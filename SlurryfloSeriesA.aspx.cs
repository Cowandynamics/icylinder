using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for SlurryfloSeriesA.
	/// </summary>
	public partial class SlurryfloSeriesA : System.Web.UI.Page
	{
		DBClass db=new DBClass();
		coder cd1=new coder();
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["User"] !=null)
			{
				if(! IsPostBack)
				{
					if(	Session["Coder"] !=null)
					{
						lblhidden.Text="";
						ArrayList blist=new ArrayList();
						ArrayList blist1=new ArrayList();
						ArrayList blist2=new ArrayList();
						ListItem litem=new ListItem();
						cd1=(coder)Session["Coder"];
						switch (cd1.Series.Trim())
						{
							case "SA":
								lblhidden.Text ="SA";
								blist =db.SelectOptions_Bore("Bore_Code", "Bore_Size","WEB_Bore_TableV1","SeriesAT","Bore_ID");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								for(int i=0;i<blist1.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
									RBLBore.Items.Add(litem);
								}
								break;
							case "AT":
								lblhidden.Text ="AT";
								blist =db.SelectOptions_Bore("Bore_Code", "Bore_Size","WEB_Bore_TableV1","SeriesAT","Bore_ID");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								for(int i=0;i<blist1.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
									RBLBore.Items.Add(litem);
								}
								break;
						
						}
								
						
						RBLBore.SelectedIndex =0;
						ListItem lite=new ListItem();
						lite=new ListItem((String.Format("MX0<br /><img alt='MX0 No mount' src='{0}'>", "mounts\\MX0.jpg")), "X0");
						RBLMount.Items.Add(lite);		
						//issue #371 start
						lite=new ListItem((String.Format("FA07<br /><img alt='M07 mount' src='{0}'>", "mounts\\MM07.jpg")), "M07");
						RBLMount.Items.Add(lite);		
						lite=new ListItem((String.Format("FA10<br /><img alt='M10 mount' src='{0}'>", "mounts\\MM10.jpg")), "M10");
						RBLMount.Items.Add(lite);		
						if(cd1.Mount==null) RBLMount.SelectedIndex=0;
						else RBLMount.SelectedValue=cd1.Mount;
						//back
						//RBLMount.SelectedIndex=0;
						//issue #371 end
						Img1stRodEnd.ImageUrl="rodends\\smallfemale.jpg";
						//issue #223 start
						if(cd1.Seal_Comp != null)
						{
							switch (cd1.Seal_Comp)
							{
								case "L":
                                    rblSeals.SelectedValue="L";
									break;
								case "N":
									rblSeals.SelectedValue="N";
									break;
							}
						}
						//issue #223 end
						if(	Session["Coder"] !=null)
						{
							cd1=(coder)Session["Coder"];
							if(cd1.Bore_Size !=null)
							{
								RBLBore.SelectedValue =cd1.Bore_Size.Trim();
							}
							if(cd1.Rod_End !=null)
							{
								if(cd1.Rod_End =="A4")
								{
									RBLRodend1.SelectedValue ="A4";
								}
							}
							
							if(cd1.Stroke !=null)
							{
								TxtStroke.Text =cd1.Stroke.Trim();
							}
						}
					}
				}
			}
			else
			{
				Response.Redirect("Login.aspx");
			}
		}
		public static bool IsNumeric(string strInteger) 
		{
			try 
			{
				int intTemp =0;
				if(strInteger.ToString().StartsWith(".") == true)
				{
					for(int i=1; i< strInteger.Length;i++)
					{
						intTemp = Int32.Parse( strInteger.Substring(i,1) );
					}
				}
				else
				{
					for(int i=0; i< strInteger.Length;i++)
					{
						if (strInteger.ToString().Substring(i,1) !=".")
						{
							intTemp = Int32.Parse( strInteger.Substring(i,1) );
						}
					}
				}
				return true;
			} 
			catch (FormatException) 
			{
				return false;
			}    
		}

		protected void TxtStroke_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtStroke.Text.ToString().Trim() !="" && IsNumeric(TxtStroke.Text.ToString().Trim()) ==true  )
			{
			
				TxtStroke.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TxtStroke.Text));
				
			}
			else
			{
				TxtStroke.Text="";
			}
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

//		private void RBLBore_SelectedIndexChanged(object sender, System.EventArgs e)
//		{
//			
//			if(	Session["Coder"] !=null)
//			{
//				RBL1stRodSize.Items.Clear();
//				ArrayList blist=new ArrayList();
//				ArrayList blist1=new ArrayList();
//				ArrayList blist2=new ArrayList();
//				ArrayList rdlist=new ArrayList();
//				ListItem litem=new ListItem();
//				cd1=(coder)Session["Coder"];
//				Img1stRodEnd.ImageUrl = "rodends\\smallfemale.jpg";
//				blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerA_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
//				blist1 =(ArrayList)blist[0];
//				blist2 =(ArrayList)blist[1];
//				for(int i=0;i<blist2.Count;i++)
//				{
//					litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
//					RBL1stRodSize.Items.Add(litem);
//				}
//				RBL1stRodSize.SelectedIndex =0;
//				ListItem li=new ListItem();
//				RBLRodend1.Items.Clear();
//				li =new ListItem("Small Female(Series A)","A4");
//				RBLRodend1.Items.Add(li);
//				RBLRodend1.SelectedIndex=0;
//
//			
//							
//			}
//		}

		protected void BtnNext_Click(object sender, System.EventArgs e)
		{
			if(RBLBore.SelectedIndex >= 0 && TxtStroke.Text.Trim() !="" )
			{
				lblstroke.Visible=false;
				cd1=new coder();
				cd1=(coder)Session["Coder"];
				cd1.Series=lblhidden.Text.Trim();
				cd1.Bore_Size =RBLBore.SelectedItem.Value.Trim();
				cd1.Stroke =TxtStroke.Text.Trim();
				cd1.Rod_End ="A4";
				ArrayList blist=new ArrayList();
				ArrayList blist1=new ArrayList();
				ArrayList blist2=new ArrayList();
				ArrayList rdlist=new ArrayList();
				ListItem litem=new ListItem();
				blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerA_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
				blist1 =(ArrayList)blist[0];
				blist2 =(ArrayList)blist[1];
				RBL1stRodSize.Items.Clear();
				for(int i=0;i<blist2.Count;i++)
				{
					litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
					RBL1stRodSize.Items.Add(litem);
				}
				RBL1stRodSize.SelectedIndex =0;
				cd1.Rod_Diamtr = RBL1stRodSize.SelectedValue.ToString();
				cd1.Cushions="8";
				cd1.CushionPosition="";
				//issue #371 start
				//back
				//cd1.Mount="X0";
				//update
				cd1.Mount=RBLMount.SelectedValue;
				//issue #371 end
				cd1.Port_Type="N";
				cd1.Port_Pos="11";
				//issue #223 start
				//back
				//cd1.MetalScrapper="GR2";
				//update
				cd1.MetalScrapper="GT3";
				//issue #223 end
				cd1.SSPistionRod="M3";
				//issue #223 start
				//back
				//cd1.Seal_Comp="L";
				//update
				cd1.Seal_Comp=rblSeals.SelectedItem.Value.ToString();
				//issue #223 end
				cd1.DoubleRod="No";
				//issue #262 start
				//cd1.CompanyID="1002";
				//issue #262 end
				Session["Coder"] =cd1;
				if(cd1.Series.Trim() =="SA")
				{
					Response.Redirect("Icylinder_page6.aspx");
				}
				else if(cd1.Series.Trim() =="AT")
				{
					Response.Redirect("Icylinder_page_Trans.aspx");
				}
			}
			else
			{
				lblstroke.Text="Please enter stroke !!";
				lblstroke.Visible=true;
			}
		}

		protected void LBBack_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Icylinder_Page1.aspx");
		}

		
	}
}
