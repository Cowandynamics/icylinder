using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for EV_SeriesA.
	/// </summary>
	public partial class Velan_SeriesA : System.Web.UI.Page
	{
		DBClass db=new DBClass();
		coder cd1=new coder();
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["User"] !=null)
			{
				if(! IsPostBack)
				{
//					TxtMinAS.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) ||(event.keyCode == 13)) {document.getElementById('"+TxtSeatingThrust.UniqueID+"').focus();return false;}}else {return true;} ");
//					TxtMinAS.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) ||(event.keyCode == 13)) {document.getElementById('"+TxtMinAS.UniqueID+"').TxtMinAS_TextChanged(sender,e);return false;}}else {return true;} ");
//					TxtSeatingThrust.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) ||(event.keyCode == 13)) {document.getElementById('"+TxtPackingFriction.UniqueID+"').focus();return false;}}else {return true;} ");
//					TxtSeatingThrust.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) ||(event.keyCode == 13)) {document.getElementById('"+TxtSeatingThrust.UniqueID+"').TxtSeatingThrust_TextChanged(sender,e);return false;}}else {return true;} ");
//					TxtPackingFriction.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) ||(event.keyCode == 13)) {document.getElementById('"+TxtSaftyFactor.UniqueID+"').focus();return false;}}else {return true;} ");
//					TxtPackingFriction.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) ||(event.keyCode == 13)) {document.getElementById('"+TxtPackingFriction.UniqueID+"').TxtPackingFriction_TextChanged(sender,e);return false;}}else {return true;} ");
//					TxtSaftyFactor.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) ||(event.keyCode == 13)) {document.getElementById('"+TxtBore.UniqueID+"').focus();return false;}}else {return true;} ");
//					TxtSaftyFactor.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) ||(event.keyCode == 13)) {document.getElementById('"+TxtSaftyFactor.UniqueID+"').TxtSaftyFactor_TextChanged(sender,e);return false;}}else {return true;} ");
//					TxtStroke.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) ||(event.keyCode == 13)) {document.getElementById('"+TxtMinAS.UniqueID+"').focus();return false;}}else {return true;} ");
//					TxtStroke.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) ||(event.keyCode == 13)) {document.getElementById('"+TxtStroke.UniqueID+"').TxtStroke_TextChanged(sender,e);return false;}}else {return true;} ");
					if(Request.QueryString["p"] !=null)
					{
						if(Request.QueryString["p"].ToString() =="new")
						{
							Session["Coder"] =null;
						}
					}
					lblpage.Visible=false;
					Lblbore.Text="";
					TxtMinAS.Text="";
					TxtPackingFriction.Text="";
					TxtSeatingThrust.Text="";
					TxtActualSafty.Text="";
					TxtBore.Text="";
					TxtStroke.Text="";
					lblhidden.Text="";
					RBLStyle.SelectedIndex=0;
					RBLStyle_SelectedIndexChanged(sender,e);
					if(Session["Coder"] !=null)
					{
						cd1=new coder();
						cd1 =(coder)Session["Coder"];
                        TxtSeatingThrust.Text=cd1.SeatingTrust.Trim();
						TxtMinAS.Text=cd1.MinAirSupply.Trim();
						TxtMinAS_TextChanged(sender,e);
						TxtSaftyFactor.Text=cd1.SeatingTrust.Trim();
						TxtSeatingThrust_TextChanged(sender,e);
						TxtPackingFriction.Text=cd1.PackingFriction.Trim();
						TxtPackingFriction_TextChanged(sender,e);
						TxtSaftyFactor.Text=cd1.SaftyFactor.Trim();
						TxtSaftyFactor_TextChanged(sender,e);
						TxtStroke.Text=cd1.Stroke.Trim();
						RBLStyle.SelectedValue=cd1.Style.Trim();
						RBLStyle_SelectedIndexChanged(sender,e);
						if(RBLStyle.SelectedIndex >0)
						{
							TxtClosingtime.Text=cd1.ClosingTime.Trim();
						}
						if(cd1.Coating !=null)
						{
							RBLPaint.SelectedValue=cd1.Coating.Trim();
						}
					}
				}
			}
			else
			{
				Response.Redirect("Login.aspx");
			}
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
		public static bool IsNumeric(string strInteger) 
		{
			try 
			{
				int intTemp =0;
				if(strInteger.ToString().StartsWith(".") == true)
				{
					for(int i=1; i< strInteger.Length;i++)
					{
						intTemp = Int32.Parse( strInteger.Substring(i,1) );
					}
				}
				else
				{
					for(int i=0; i< strInteger.Length;i++)
					{
						if (strInteger.ToString().Substring(i,1) !=".")
						{
							intTemp = Int32.Parse( strInteger.Substring(i,1) );
						}
					}
				}
				return true;
			} 
			catch (FormatException) 
			{
				return false;
			}    
		}
		public void FindBore()
		{
			if(TxtSeatingThrust.Text.Trim() !="" && TxtMinAS.Text.Trim() !="" && TxtPackingFriction.Text.Trim() !="" && TxtSaftyFactor.Text.Trim() !="")
			{
				decimal dc1,dc2,dc3,dc4,dc5,bore,b=0.00m;
				dc1=Convert.ToDecimal(TxtMinAS.Text.Trim());
				dc2=Convert.ToDecimal(TxtSeatingThrust.Text.Trim());
				dc3=Convert.ToDecimal(TxtPackingFriction.Text.Trim());
				dc4=Convert.ToDecimal(TxtSaftyFactor.Text.Trim());
				dc4= 1 + (dc4 /100);
				dc5= (( dc2 + dc3 ) *  dc4 ) / dc1;
				b= dc5 / Convert.ToDecimal(Math.PI);
				bore=Convert.ToDecimal(( Math.Sqrt(Convert.ToDouble(b))) * 2);
				bore=Decimal.Round(bore,2);
				if(bore > 0.00m && bore < 4.00m )
				{
					Lblbore.Text="H";
					TxtBore.Text="4\"";
				}
				else if(bore >= 4.00m && bore <= 5.00m )
				{
					Lblbore.Text="K";
					TxtBore.Text="5\"";
				}
				else if(bore >= 5.00m && bore <= 6.00m )
				{
					Lblbore.Text="L";
					TxtBore.Text="6\"";
				}
				else if(bore >= 6.00m && bore <= 7.00m )
				{
					Lblbore.Text="M";
					TxtBore.Text="7\"";
				}
				else if(bore >= 7.00m && bore <= 8.00m )
				{
					Lblbore.Text="N";
					TxtBore.Text="8\"";
				}
				else if(bore >= 8.00m && bore <= 10.00m )
				{
					Lblbore.Text="P";
					TxtBore.Text="10\"";
				}
				else if(bore >= 10.00m && bore <= 12.00m )
				{
					Lblbore.Text="R";
					TxtBore.Text="12\"";
				}
				else if(bore >= 12.00m && bore <= 14.00m )
				{
					Lblbore.Text="S";
					TxtBore.Text="14\"";
				}
				else if(bore >= 14.00m && bore <= 16.00m )
				{
					Lblbore.Text="T";
					TxtBore.Text="16\"";
				}
				else if(bore >= 16.00m && bore <= 18.00m )
				{
					Lblbore.Text="W";
					TxtBore.Text="18\"";
				}
				else if(bore >= 18.00m && bore <= 20.00m )
				{
					Lblbore.Text="X";
					TxtBore.Text="20\"";
				}
				else if(bore >= 20.00m && bore <= 22.00m )
				{
					Lblbore.Text="A";
					TxtBore.Text="22\"";
				}
				else if(bore >= 22.00m && bore <= 24.00m )
				{
					Lblbore.Text="Y";
					TxtBore.Text="24\"";
				}
				else if(bore >= 24.00m && bore <= 26.00m )
				{
					Lblbore.Text="B";
					TxtBore.Text="26\"";
				}
				else if(bore >= 26.00m && bore <= 28.00m )
				{
					Lblbore.Text="F";
					TxtBore.Text="28\"";
				}
				else if(bore >= 28.00m && bore <= 30.00m )
				{
					Lblbore.Text="I";
					TxtBore.Text="30\"";
				}
				else
				{
					Lblbore.Text="I";
					TxtBore.Text="30\"";
				}
				decimal cc1,cc2,cc3,cc4=0.00m;
				cc1= Convert.ToDecimal(TxtBore.Text.Replace("\"",""));
				cc2=((cc1 * cc1) / 4)  * Convert.ToDecimal(Math.PI) ;
				cc3=cc2 * dc1;
				cc4=((cc3 /( dc2 + dc3 )) -1) * 100;
				TxtActualSafty.Text=(Decimal.Round(cc4,2)).ToString();
			}
		}
		protected void BtnNext_Click(object sender, System.EventArgs e)
		{
			if(RBLStyle.SelectedIndex ==0)
			{
				if(TxtStroke.Text.Trim()!=""&& RBLStyle.SelectedIndex >-1 && TxtMinAS.Text.Trim() !="" && TxtSeatingThrust.Text.Trim() !="" && TxtPackingFriction.Text.Trim() !="" && TxtSaftyFactor.Text.Trim() !="" && TxtBore.Text.Trim() !="")
				{
					lblpage.Visible=false;
					cd1=new coder();
					cd1.Series="A";
					cd1.Bore_Size =Lblbore.Text.Trim();
					cd1.Rod_Diamtr=db.SelectValue("Rod_Code","WEB_RodSerA_TableV1",Lblbore.Text.Trim(),"1").Trim();
					cd1.Stroke =TxtStroke.Text.Trim();
					cd1.Rod_End ="A4";
					cd1.Cushions="8";
					cd1.CushionPosition="";
					cd1.Mount=RBLMount.SelectedItem.Value.ToString().Trim();
					cd1.Port_Type="N";
					cd1.Port_Pos="11";
					cd1.Seal_Comp=RBLSeal.SelectedItem.Value.ToString().Trim();
					if(RBLPaint.SelectedIndex ==1)
					{
						cd1.Coating="C1";
					}
					cd1.Style=RBLStyle.SelectedItem.Value.Trim();
					cd1.MinAirSupply=TxtMinAS.Text.Trim();
					cd1.SeatingTrust=TxtSeatingThrust.Text.ToString().Trim();
					cd1.PackingFriction=TxtPackingFriction.Text.ToString().Trim();
					cd1.SaftyFactor=TxtSaftyFactor.Text.ToString().Trim();
					cd1.ActualSaftyFactor=TxtActualSafty.Text.ToString().Trim();
					cd1.Specials="ADV";
					if(RBLStyle.SelectedIndex > 0)
					{		
						cd1.Bore_Size="Z";
						cd1.Rod_Diamtr="Z";
						cd1.Specials="YES";
						cd1.ClosingTime=TxtClosingtime.Text.Trim();
						cd1.ActualSaftyFactor=null;
					}
					Session["Coder"] =cd1;
					Response.Redirect("Velan_SeriesA_Specials.aspx");
				}
				else
				{
					lblpage.Visible=true;
				}
			}
			else if(TxtClosingtime.Text.Trim() !="")
			{
				if(TxtStroke.Text.Trim()!=""&& RBLStyle.SelectedIndex >-1 && TxtMinAS.Text.Trim() !="" && TxtSeatingThrust.Text.Trim() !="" && TxtPackingFriction.Text.Trim() !="" && TxtSaftyFactor.Text.Trim() !="" && TxtBore.Text.Trim() !="")
				{
					lblpage.Visible=false;
					cd1=new coder();
					cd1.Series="A";
					cd1.Bore_Size =Lblbore.Text.Trim();
					cd1.Rod_Diamtr=db.SelectValue("Rod_Code","WEB_RodSerA_TableV1",Lblbore.Text.Trim(),"1").Trim();
					cd1.Stroke =TxtStroke.Text.Trim();
					cd1.Rod_End ="A4";
					cd1.Cushions="8";
					cd1.CushionPosition="";
					cd1.Mount=RBLMount.SelectedItem.Value.ToString().Trim();
					cd1.Port_Type="N";
					cd1.Port_Pos="11";
					cd1.Seal_Comp=RBLSeal.SelectedItem.Value.ToString().Trim();
					if(RBLPaint.SelectedIndex ==1)
					{
						cd1.Coating="C1";
					}
					cd1.Style=RBLStyle.SelectedItem.Value.Trim();
					cd1.MinAirSupply=TxtMinAS.Text.Trim();
					cd1.SeatingTrust=TxtSeatingThrust.Text.ToString().Trim();
					cd1.PackingFriction=TxtPackingFriction.Text.ToString().Trim();
					cd1.SaftyFactor=TxtSaftyFactor.Text.ToString().Trim();
					cd1.ActualSaftyFactor=TxtActualSafty.Text.ToString().Trim();
					cd1.Specials="NO";
					if(RBLStyle.SelectedIndex > 0)
					{		
						cd1.Bore_Size="Z";
						cd1.Rod_Diamtr="Z";
						cd1.Specials="YES";
						cd1.ClosingTime=TxtClosingtime.Text.Trim();
						cd1.ActualSaftyFactor=null;
					}
					Session["Coder"] =cd1;
					Response.Redirect("Velan_SeriesA_Specials.aspx");
				}
				else
				{
					lblpage.Visible=true;
				}
			}
			else
			{
				lblpage.Visible=true;
			}
		}
			
		protected void TxtMinAS_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtMinAS.Text.ToString().Trim() !="" && IsNumeric(TxtMinAS.Text.ToString().Trim()) ==true  )
			{
			
				TxtMinAS.Text =String.Format("{0:##0}",Convert.ToDecimal(TxtMinAS.Text));
				if(Convert.ToInt32(TxtMinAS.Text) > 150)
				{
					Lblas.Visible=true;
					TxtMinAS.Text="150";
					Lblas.Text="Min Air Supply is greater than Series A capacity. The Sizing program will use 150 PSI";
				}
				else if(Convert.ToInt32(TxtMinAS.Text) < 25)
				{
					Lblas.Visible=true;
					TxtMinAS.Text="25";
					Lblas.Text="Min Air Supply is not high enough to ensure smooth operation. The Sizing program will use 25 PSI";
				}
				else
				{
					Lblas.Visible=false;
				}
				if(RBLStyle.SelectedIndex ==0)
				{
					FindBore();
				}
				else
				{
					TxtBore.Text="Custom";
					Lblbore.Text="Z";
					TxtActualSafty.Text="TBA";
				}
			}
			else
			{
				TxtMinAS.Text="";
			}
		}

		protected void TxtStroke_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtStroke.Text.ToString().Trim() !="" && IsNumeric(TxtStroke.Text.ToString().Trim()) ==true  )
			{
				TxtStroke.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TxtStroke.Text));
			}
			else
			{
				TxtStroke.Text="";
			}
		}

		protected void TxtSeatingThrust_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtSeatingThrust.Text.ToString().Trim() !="" && IsNumeric(TxtSeatingThrust.Text.ToString().Trim()) ==true  )
			{
				TxtSeatingThrust.Text =String.Format("{0:###0}",Convert.ToDecimal(TxtSeatingThrust.Text));
				if(RBLStyle.SelectedIndex ==0)
				{
					FindBore();
				}
				else
				{
					TxtBore.Text="Custom";
					Lblbore.Text="Z";
					TxtActualSafty.Text="TBA";
				}
			}
			else
			{
				TxtSeatingThrust.Text="";
			}
		}

		protected void TxtPackingFriction_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtPackingFriction.Text.ToString().Trim() !="" && IsNumeric(TxtPackingFriction.Text.ToString().Trim()) ==true  )
			{
				TxtPackingFriction.Text =String.Format("{0:###0}",Convert.ToDecimal(TxtPackingFriction.Text));
				if(RBLStyle.SelectedIndex ==0)
				{
					FindBore();
				}
				else
				{
					TxtBore.Text="Custom";
					Lblbore.Text="Z";
					TxtActualSafty.Text="TBA";
				}
			}
			else
			{
				TxtPackingFriction.Text="";
			}
		}

		protected void TxtSaftyFactor_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtSaftyFactor.Text.ToString().Trim() !="" && IsNumeric(TxtSaftyFactor.Text.ToString().Trim()) ==true  )
			{
				TxtSaftyFactor.Text =String.Format("{0:###0.00}",Convert.ToDecimal(TxtSaftyFactor.Text));
				if(RBLStyle.SelectedIndex ==0)
				{
					FindBore();
				}
				else
				{
					TxtBore.Text="Custom";
					Lblbore.Text="Z";
					TxtActualSafty.Text="TBA";
				}
			}
			else
			{
				TxtPackingFriction.Text="";
			}
		}

		protected void RBLStyle_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(RBLStyle.SelectedIndex ==0)
			{
				FindBore();
				lblclosing.Visible=false;
				lblclosing1.Visible=false;
				TxtClosingtime.Visible=false;
				RFQ1.Visible=false;
			}
			else
			{
				TxtBore.Text="Custom";
				TxtActualSafty.Text="TBA";
				Lblbore.Text="Z";
				TxtClosingtime.Text="";
                lblclosing.Visible=true;
				lblclosing1.Visible=true;
				TxtClosingtime.Visible=true;
				RFQ1.Visible=true;
			}
		}

		protected void TxtClosingtime_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtClosingtime.Text.ToString().Trim() !="" && IsNumeric(TxtClosingtime.Text.ToString().Trim()) ==true  )
			{
				TxtClosingtime.Text =String.Format("{0:###0}",Convert.ToDecimal(TxtClosingtime.Text));
			}
			else
			{
				TxtClosingtime.Text="";
			}
		}
	}
}
