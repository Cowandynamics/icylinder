<%@ Page language="c#" Codebehind="Icylinder_ML_A.aspx.cs" AutoEventWireup="True" Inherits="iCylinderV1.Icylinder_ML_A" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Icylinder_ML_A</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE style="WIDTH: 1000px; HEIGHT: 487px" id="Table1" border="0" cellSpacing="0" cellPadding="0"
				width="1000" bgColor="lightgrey">
				<TR>
					<TD height="20" width="50"></TD>
					<TD height="20" align="center"><asp:label id="Label2" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
							Font-Size="Small">Series</asp:label></TD>
					<TD height="20" width="50"></TD>
				</TR>
				<TR>
					<TD height="20" width="50"></TD>
					<TD height="20" align="center"></TD>
					<TD height="20" width="50"></TD>
				</TR>
				<TR>
					<TD width="50"></TD>
					<TD bgColor="gainsboro" align="center">
						<asp:linkbutton style="Z-INDEX: 0" id="LBBatchQuote" runat="server" Font-Size="Smaller" Font-Bold="True" onclick="LBBatchQuote_Click">Click Here for ML Series Batch Quote</asp:linkbutton>
						&nbsp;&nbsp;&nbsp;<asp:LinkButton id="LBBatchQuote_AS" runat="server" Font-Size="X-Small" Font-Bold="True" onclick="LBBatchQuote_AS_Click">Click Here for A Series Batch Quote</asp:LinkButton>
					</TD>
					<TD width="50"></TD>
				</TR>
				<TR>
					<TD height="30" width="50" align="right"></TD>
					<TD bgColor="#dcdcdc" height="30" align="center"></TD>
					<TD height="30" width="50"></TD>
				</TR>
				<TR>
					<TD height="20" width="50"></TD>
					<TD height="20" align="center"></TD>
					<TD height="20" width="50"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
