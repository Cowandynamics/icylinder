using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for HtmlQuote.
	/// </summary>
	public class HtmlQuote : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label Label58;
		protected System.Web.UI.WebControls.Label Label59;
		protected System.Web.UI.WebControls.Label Label57;
		protected System.Web.UI.WebControls.Label Label56;
		protected System.Web.UI.WebControls.Panel Panel4;
		protected System.Web.UI.WebControls.Label Label21;
		protected System.Web.UI.WebControls.Label Label51;
		protected System.Web.UI.WebControls.Label Label49;
		protected System.Web.UI.WebControls.Label Label47;
		protected System.Web.UI.WebControls.Label Label46;
		protected System.Web.UI.WebControls.Label Label45;
		protected System.Web.UI.WebControls.Label Label44;
		protected System.Web.UI.WebControls.Label Label43;
		protected System.Web.UI.WebControls.Label Label42;
		protected System.Web.UI.WebControls.Label Label33;
		protected System.Web.UI.WebControls.Image Image2;
		protected System.Web.UI.WebControls.Label Label29;
		protected System.Web.UI.WebControls.Label Label25;
		protected System.Web.UI.WebControls.Label LblTotal;
		protected System.Web.UI.WebControls.Label Lblinfo;
		protected System.Web.UI.WebControls.Label LblCurrency;
		protected System.Web.UI.WebControls.Label lblEDate;
		protected System.Web.UI.WebControls.Label lblnote;
		protected System.Web.UI.WebControls.Label LblQdate;
		protected System.Web.UI.WebControls.Label LblTerms;
		protected System.Web.UI.WebControls.Label lblCode;
		protected System.Web.UI.WebControls.Label LblDelivery;
		protected System.Web.UI.WebControls.Label LblEdates;
		protected System.Web.UI.WebControls.Label LblPBy;
		protected System.Web.UI.WebControls.Label LblQno;
		protected System.Web.UI.WebControls.Label LblShip3;
		protected System.Web.UI.WebControls.Label LblShip2;
		protected System.Web.UI.WebControls.Label LblShip1;
		protected System.Web.UI.WebControls.Label LblBill3;
		protected System.Web.UI.WebControls.Label LblBill2;
		protected System.Web.UI.WebControls.Label LblBill1;
		protected System.Web.UI.WebControls.Label LblAttn3;
		protected System.Web.UI.WebControls.Label LblAttn2;
		protected System.Web.UI.WebControls.Label LblAttn1;
		protected System.Web.UI.WebControls.Panel Panel3;
		protected System.Web.UI.WebControls.Label LblOffice;
		protected System.Web.UI.WebControls.Label LblPage;
		protected System.Web.UI.WebControls.Panel Panel1;
		protected System.Web.UI.WebControls.Label LblComments;
		protected System.Web.UI.WebControls.Panel Panel6;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Panel Panel7;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.WebControls.Label Label17;
		protected System.Web.UI.WebControls.Label Label19;
		protected System.Web.UI.WebControls.Label Label20;
		protected System.Web.UI.WebControls.Label Label22;
		protected System.Web.UI.WebControls.Label Label23;
		protected System.Web.UI.WebControls.Label Label24;
		protected System.Web.UI.WebControls.Label Label26;
		protected System.Web.UI.WebControls.Label Label27;
		protected System.Web.UI.WebControls.Label Label28;
		protected System.Web.UI.WebControls.Label Label48;
		protected System.Web.UI.WebControls.Image Image1;
		protected System.Web.UI.WebControls.Panel Panel11;
		protected System.Web.UI.WebControls.Label Label62;
		protected System.Web.UI.WebControls.Label Label66;
		protected System.Web.UI.WebControls.Label Label70;
		protected System.Web.UI.WebControls.Label Label71;
		protected System.Web.UI.WebControls.Label Label72;
		protected System.Web.UI.WebControls.Label Label73;
		protected System.Web.UI.WebControls.Label Label74;
		protected System.Web.UI.WebControls.Panel Panel12;
		protected System.Web.UI.WebControls.Label Label76;
		protected System.Web.UI.WebControls.Label Label77;
		protected System.Web.UI.WebControls.Label Label79;
		protected System.Web.UI.WebControls.Label Label80;
		protected System.Web.UI.WebControls.Label Label81;
		protected System.Web.UI.WebControls.Label Label82;
		protected System.Web.UI.WebControls.Label Label83;
		protected System.Web.UI.WebControls.Label Label84;
		protected System.Web.UI.WebControls.Label Label85;
		protected System.Web.UI.WebControls.Label Label86;
		protected System.Web.UI.WebControls.Label Label98;
		protected System.Web.UI.WebControls.Image Image3;
		protected System.Web.UI.WebControls.Panel Panel15;
		protected System.Web.UI.WebControls.Label Label103;
		protected System.Web.UI.WebControls.Label Label107;
		protected System.Web.UI.WebControls.Label Label111;
		protected System.Web.UI.WebControls.Label Label112;
		protected System.Web.UI.WebControls.Label Label113;
		protected System.Web.UI.WebControls.Label Label114;
		protected System.Web.UI.WebControls.Label Label115;
		protected System.Web.UI.WebControls.Panel Panel16;
		protected System.Web.UI.WebControls.Label Label117;
		protected System.Web.UI.WebControls.Label Label118;
		protected System.Web.UI.WebControls.Label Label120;
		protected System.Web.UI.WebControls.Label Label121;
		protected System.Web.UI.WebControls.Label Label122;
		protected System.Web.UI.WebControls.Label Label123;
		protected System.Web.UI.WebControls.Label Label124;
		protected System.Web.UI.WebControls.Label Label125;
		protected System.Web.UI.WebControls.Label Label126;
		protected System.Web.UI.WebControls.Label Label127;
		protected System.Web.UI.WebControls.Label Label139;
		protected System.Web.UI.WebControls.Image Image4;
		protected System.Web.UI.WebControls.Panel Panel19;
		protected System.Web.UI.WebControls.Label Label144;
		protected System.Web.UI.WebControls.Label Label148;
		protected System.Web.UI.WebControls.Label Label152;
		protected System.Web.UI.WebControls.Label Label153;
		protected System.Web.UI.WebControls.Label Label154;
		protected System.Web.UI.WebControls.Label Label155;
		protected System.Web.UI.WebControls.Label Label156;
		protected System.Web.UI.WebControls.Panel Panel20;
		protected System.Web.UI.WebControls.Label Label158;
		protected System.Web.UI.WebControls.Label Label159;
		protected System.Web.UI.WebControls.Label Label161;
		protected System.Web.UI.WebControls.Label Label162;
		protected System.Web.UI.WebControls.Label Label163;
		protected System.Web.UI.WebControls.Label Label164;
		protected System.Web.UI.WebControls.Label Label165;
		protected System.Web.UI.WebControls.Label Label166;
		protected System.Web.UI.WebControls.Label Label167;
		protected System.Web.UI.WebControls.Label Label168;
		protected System.Web.UI.WebControls.Label Label180;
		protected System.Web.UI.WebControls.Image Image5;
		protected System.Web.UI.WebControls.Panel Panel23;
		protected System.Web.UI.WebControls.Label Label185;
		protected System.Web.UI.WebControls.Label Label189;
		protected System.Web.UI.WebControls.Label Label193;
		protected System.Web.UI.WebControls.Label Label194;
		protected System.Web.UI.WebControls.Label Label195;
		protected System.Web.UI.WebControls.Label Label196;
		protected System.Web.UI.WebControls.Label Label197;
		protected System.Web.UI.WebControls.Panel Panel24;
		protected System.Web.UI.WebControls.Label Label199;
		protected System.Web.UI.WebControls.Label Label200;
		protected System.Web.UI.WebControls.Label Label202;
		protected System.Web.UI.WebControls.Label Label203;
		protected System.Web.UI.WebControls.Label Label204;
		protected System.Web.UI.WebControls.Label Label205;
		protected System.Web.UI.WebControls.Label Label206;
		protected System.Web.UI.WebControls.Label Label207;
		protected System.Web.UI.WebControls.Label Label208;
		protected System.Web.UI.WebControls.Label Label209;
		protected System.Web.UI.WebControls.Label Label221;
		protected System.Web.UI.WebControls.Image Image6;
		protected System.Web.UI.WebControls.Label lbloffice2;
		protected System.Web.UI.WebControls.Label lblship21;
		protected System.Web.UI.WebControls.Label lblship22;
		protected System.Web.UI.WebControls.Label lblship23;
		protected System.Web.UI.WebControls.Label lblqno2;
		protected System.Web.UI.WebControls.Label lblprep2;
		protected System.Web.UI.WebControls.Label lbledate2;
		protected System.Web.UI.WebControls.Label lbldelivery2;
		protected System.Web.UI.WebControls.Label lblcode2;
		protected System.Web.UI.WebControls.Label lblterm2;
		protected System.Web.UI.WebControls.Label lblqdate2;
		protected System.Web.UI.WebControls.Label lblnote2;
		protected System.Web.UI.WebControls.Label lblcurrency2;
		protected System.Web.UI.WebControls.Label lblinfo2;
		protected System.Web.UI.WebControls.Label lblbill21;
		protected System.Web.UI.WebControls.Label lblbill22;
		protected System.Web.UI.WebControls.Label lblbill23;
		protected System.Web.UI.WebControls.Label lblatt21;
		protected System.Web.UI.WebControls.Label lblatt22;
		protected System.Web.UI.WebControls.Label lblatt23;
		protected System.Web.UI.WebControls.Label lblcomment2;
		protected System.Web.UI.WebControls.Label lbltotalprice2;
		protected System.Web.UI.WebControls.Label lbloffice3;
		protected System.Web.UI.WebControls.Label lblship31;
		protected System.Web.UI.WebControls.Label lblship32;
		protected System.Web.UI.WebControls.Label lblship33;
		protected System.Web.UI.WebControls.Label lblqno3;
		protected System.Web.UI.WebControls.Label lblprep3;
		protected System.Web.UI.WebControls.Label lbledate3;
		protected System.Web.UI.WebControls.Label lbldelivery3;
		protected System.Web.UI.WebControls.Label lblcode3;
		protected System.Web.UI.WebControls.Label lblterms3;
		protected System.Web.UI.WebControls.Label lblqdate3;
		protected System.Web.UI.WebControls.Label lblnote3;
		protected System.Web.UI.WebControls.Label lblcurrency3;
		protected System.Web.UI.WebControls.Label lblinfo3;
		protected System.Web.UI.WebControls.Label lblbill31;
		protected System.Web.UI.WebControls.Label lblbill32;
		protected System.Web.UI.WebControls.Label lblbill33;
		protected System.Web.UI.WebControls.Label lblatt31;
		protected System.Web.UI.WebControls.Label lblatt32;
		protected System.Web.UI.WebControls.Label lblatt33;
		protected System.Web.UI.WebControls.Label lblcomment3;
		protected System.Web.UI.WebControls.Label lbltotalprice3;
		protected System.Web.UI.WebControls.Label lbloffice4;
		protected System.Web.UI.WebControls.Label llblship41;
		protected System.Web.UI.WebControls.Label llblship42;
		protected System.Web.UI.WebControls.Label llblship43;
		protected System.Web.UI.WebControls.Label lblqno4;
		protected System.Web.UI.WebControls.Label lblprep4;
		protected System.Web.UI.WebControls.Label lbledate4;
		protected System.Web.UI.WebControls.Label lbldelivery4;
		protected System.Web.UI.WebControls.Label lblcode4;
		protected System.Web.UI.WebControls.Label lblterm4;
		protected System.Web.UI.WebControls.Label lblqdate4;
		protected System.Web.UI.WebControls.Label lblnote4;
		protected System.Web.UI.WebControls.Label lblcurrency4;
		protected System.Web.UI.WebControls.Label lblinfo4;
		protected System.Web.UI.WebControls.Label lblbill41;
		protected System.Web.UI.WebControls.Label lblbill42;
		protected System.Web.UI.WebControls.Label lblbill43;
		protected System.Web.UI.WebControls.Label lblatt41;
		protected System.Web.UI.WebControls.Label lblatt42;
		protected System.Web.UI.WebControls.Label lblatt43;
		protected System.Web.UI.WebControls.Label lblcomment4;
		protected System.Web.UI.WebControls.Label lbltotalprice4;
		protected System.Web.UI.WebControls.Label lbloffice5;
		protected System.Web.UI.WebControls.Label lblship51;
		protected System.Web.UI.WebControls.Label lblship52;
		protected System.Web.UI.WebControls.Label lblship53;
		protected System.Web.UI.WebControls.Label lblqno5;
		protected System.Web.UI.WebControls.Label lblprep5;
		protected System.Web.UI.WebControls.Label lbledate5;
		protected System.Web.UI.WebControls.Label lbldelivery5;
		protected System.Web.UI.WebControls.Label lblcode5;
		protected System.Web.UI.WebControls.Label lblterm5;
		protected System.Web.UI.WebControls.Label lblqdate5;
		protected System.Web.UI.WebControls.Label lblnote5;
		protected System.Web.UI.WebControls.Label lblcurrency5;
		protected System.Web.UI.WebControls.Label lblinfo5;
		protected System.Web.UI.WebControls.Label lblbill51;
		protected System.Web.UI.WebControls.Label lblbill52;
		protected System.Web.UI.WebControls.Label lblbill53;
		protected System.Web.UI.WebControls.Label lblatt51;
		protected System.Web.UI.WebControls.Label lblatt52;
		protected System.Web.UI.WebControls.Label lblatt53;
		protected System.Web.UI.WebControls.Label lblcomment5;
		protected System.Web.UI.WebControls.Label lbltotalprice5;
		protected System.Web.UI.WebControls.Label lbloffice6;
		protected System.Web.UI.WebControls.Label lblship61;
		protected System.Web.UI.WebControls.Label lblship62;
		protected System.Web.UI.WebControls.Label lblship63;
		protected System.Web.UI.WebControls.Label lblqno6;
		protected System.Web.UI.WebControls.Label lblprep6;
		protected System.Web.UI.WebControls.Label lbledate6;
		protected System.Web.UI.WebControls.Label lbldelivery6;
		protected System.Web.UI.WebControls.Label lblcode6;
		protected System.Web.UI.WebControls.Label lblterm6;
		protected System.Web.UI.WebControls.Label lblqdate6;
		protected System.Web.UI.WebControls.Label lblnote6;
		protected System.Web.UI.WebControls.Label lblcurrency6;
		protected System.Web.UI.WebControls.Label lblinfo6;
		protected System.Web.UI.WebControls.Label lblbill61;
		protected System.Web.UI.WebControls.Label lblbill62;
		protected System.Web.UI.WebControls.Label lblbill63;
		protected System.Web.UI.WebControls.Label lblatt61;
		protected System.Web.UI.WebControls.Label lblatt62;
		protected System.Web.UI.WebControls.Label lblatt63;
		protected System.Web.UI.WebControls.Label lblcomment6;
		protected System.Web.UI.WebControls.Label lbltotalprice6;
		protected System.Web.UI.WebControls.Panel page2;
		protected System.Web.UI.WebControls.Panel page3;
		protected System.Web.UI.WebControls.Panel page4;
		protected System.Web.UI.WebControls.Panel page5;
		protected System.Web.UI.WebControls.Panel ptotal1;
		protected System.Web.UI.WebControls.Panel ptotal2;
		protected System.Web.UI.WebControls.Panel ptotal3;
		protected System.Web.UI.WebControls.Panel ptotal4;
		protected System.Web.UI.WebControls.Panel ptotal5;
		protected System.Web.UI.WebControls.Panel ptotal6;
		protected System.Web.UI.WebControls.Panel page6;
		protected System.Web.UI.WebControls.Label lblt1;
		protected System.Web.UI.WebControls.Label lblt2;
		protected System.Web.UI.WebControls.Label lblt3;
		protected System.Web.UI.WebControls.Label lblt4;
		protected System.Web.UI.WebControls.Label lblt5;
		protected System.Web.UI.WebControls.Label lblt6;
		protected System.Web.UI.WebControls.Panel Panel5;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.Label Label32;
		protected System.Web.UI.WebControls.Label Label34;
		protected System.Web.UI.WebControls.Label Label35;
		protected System.Web.UI.WebControls.Label Label36;
		protected System.Web.UI.WebControls.Label Label37;
		protected System.Web.UI.WebControls.Panel Panel8;
		protected System.Web.UI.WebControls.Label Label39;
		protected System.Web.UI.WebControls.Label Label40;
		protected System.Web.UI.WebControls.Label Label50;
		protected System.Web.UI.WebControls.Label Label52;
		protected System.Web.UI.WebControls.Label Label53;
		protected System.Web.UI.WebControls.Label Label54;
		protected System.Web.UI.WebControls.Label Label55;
		protected System.Web.UI.WebControls.Label Label60;
		protected System.Web.UI.WebControls.Label Label61;
		protected System.Web.UI.WebControls.Label Label63;
		protected System.Web.UI.WebControls.Label Label91;
		protected System.Web.UI.WebControls.Image Image7;
		protected System.Web.UI.WebControls.Panel Panel13;
		protected System.Web.UI.WebControls.Label Label96;
		protected System.Web.UI.WebControls.Label Label101;
		protected System.Web.UI.WebControls.Label Label106;
		protected System.Web.UI.WebControls.Label Label108;
		protected System.Web.UI.WebControls.Label Label109;
		protected System.Web.UI.WebControls.Label Label110;
		protected System.Web.UI.WebControls.Label Label116;
		protected System.Web.UI.WebControls.Panel Panel14;
		protected System.Web.UI.WebControls.Label Label128;
		protected System.Web.UI.WebControls.Label Label129;
		protected System.Web.UI.WebControls.Label Label131;
		protected System.Web.UI.WebControls.Label Label132;
		protected System.Web.UI.WebControls.Label Label133;
		protected System.Web.UI.WebControls.Label Label134;
		protected System.Web.UI.WebControls.Label Label135;
		protected System.Web.UI.WebControls.Label Label136;
		protected System.Web.UI.WebControls.Label Label137;
		protected System.Web.UI.WebControls.Label Label138;
		protected System.Web.UI.WebControls.Label Label160;
		protected System.Web.UI.WebControls.Image Image8;
		protected System.Web.UI.WebControls.Panel Panel21;
		protected System.Web.UI.WebControls.Label Label173;
		protected System.Web.UI.WebControls.Label Label177;
		protected System.Web.UI.WebControls.Label Label182;
		protected System.Web.UI.WebControls.Label Label183;
		protected System.Web.UI.WebControls.Label Label184;
		protected System.Web.UI.WebControls.Label Label186;
		protected System.Web.UI.WebControls.Label Label187;
		protected System.Web.UI.WebControls.Panel Panel22;
		protected System.Web.UI.WebControls.Label Label190;
		protected System.Web.UI.WebControls.Label Label191;
		protected System.Web.UI.WebControls.Label Label198;
		protected System.Web.UI.WebControls.Label Label201;
		protected System.Web.UI.WebControls.Label Label210;
		protected System.Web.UI.WebControls.Label Label211;
		protected System.Web.UI.WebControls.Label Label212;
		protected System.Web.UI.WebControls.Label Label213;
		protected System.Web.UI.WebControls.Label Label214;
		protected System.Web.UI.WebControls.Label Label215;
		protected System.Web.UI.WebControls.Label Label228;
		protected System.Web.UI.WebControls.Image Image9;
		protected System.Web.UI.WebControls.Panel Panel27;
		protected System.Web.UI.WebControls.Label Label233;
		protected System.Web.UI.WebControls.Label Label237;
		protected System.Web.UI.WebControls.Label Label241;
		protected System.Web.UI.WebControls.Label Label242;
		protected System.Web.UI.WebControls.Label Label243;
		protected System.Web.UI.WebControls.Label Label244;
		protected System.Web.UI.WebControls.Label Label245;
		protected System.Web.UI.WebControls.Panel Panel28;
		protected System.Web.UI.WebControls.Label Label247;
		protected System.Web.UI.WebControls.Label Label248;
		protected System.Web.UI.WebControls.Label Label250;
		protected System.Web.UI.WebControls.Label Label251;
		protected System.Web.UI.WebControls.Label Label252;
		protected System.Web.UI.WebControls.Label Label253;
		protected System.Web.UI.WebControls.Label Label254;
		protected System.Web.UI.WebControls.Label Label255;
		protected System.Web.UI.WebControls.Label Label256;
		protected System.Web.UI.WebControls.Label Label257;
		protected System.Web.UI.WebControls.Label Label269;
		protected System.Web.UI.WebControls.Image Image10;
		protected System.Web.UI.WebControls.Label lbloffice7;
		protected System.Web.UI.WebControls.Label lblship71;
		protected System.Web.UI.WebControls.Label lblship72;
		protected System.Web.UI.WebControls.Label lblship73;
		protected System.Web.UI.WebControls.Label lblqno7;
		protected System.Web.UI.WebControls.Label lblprep7;
		protected System.Web.UI.WebControls.Label lbledate7;
		protected System.Web.UI.WebControls.Label lbldelivery7;
		protected System.Web.UI.WebControls.Label lblcode7;
		protected System.Web.UI.WebControls.Label lblterms7;
		protected System.Web.UI.WebControls.Label lblqdate7;
		protected System.Web.UI.WebControls.Label lblnotes7;
		protected System.Web.UI.WebControls.Label lblcurrency7;
		protected System.Web.UI.WebControls.Label lblinfo7;
		protected System.Web.UI.WebControls.Label lblbill71;
		protected System.Web.UI.WebControls.Label lblbill72;
		protected System.Web.UI.WebControls.Label lblbill73;
		protected System.Web.UI.WebControls.Label lblatt71;
		protected System.Web.UI.WebControls.Label lblatt72;
		protected System.Web.UI.WebControls.Label lblatt73;
		protected System.Web.UI.WebControls.Label lblcomment7;
		protected System.Web.UI.WebControls.Label lbltotalprice7;
		protected System.Web.UI.WebControls.Label lbloffice8;
		protected System.Web.UI.WebControls.Label lblship81;
		protected System.Web.UI.WebControls.Label lblship82;
		protected System.Web.UI.WebControls.Label lblship83;
		protected System.Web.UI.WebControls.Label lblqno8;
		protected System.Web.UI.WebControls.Label lblprep8;
		protected System.Web.UI.WebControls.Label lbledate8;
		protected System.Web.UI.WebControls.Label lbldelivery8;
		protected System.Web.UI.WebControls.Label lblcode8;
		protected System.Web.UI.WebControls.Label lblterms8;
		protected System.Web.UI.WebControls.Label lblqdate8;
		protected System.Web.UI.WebControls.Label lblnotes8;
		protected System.Web.UI.WebControls.Label lblcurrency8;
		protected System.Web.UI.WebControls.Label lblinfo8;
		protected System.Web.UI.WebControls.Label lblbill81;
		protected System.Web.UI.WebControls.Label lblbill82;
		protected System.Web.UI.WebControls.Label lblbill83;
		protected System.Web.UI.WebControls.Label lblatt81;
		protected System.Web.UI.WebControls.Label lblatt82;
		protected System.Web.UI.WebControls.Label lblatt83;
		protected System.Web.UI.WebControls.Label lblcomments8;
		protected System.Web.UI.WebControls.Label lbltotalprice8;
		protected System.Web.UI.WebControls.Label lbloffice9;
		protected System.Web.UI.WebControls.Label lblatt91;
		protected System.Web.UI.WebControls.Label lblatt92;
		protected System.Web.UI.WebControls.Label lblship91;
		protected System.Web.UI.WebControls.Label lblship92;
		protected System.Web.UI.WebControls.Label lblship93;
		protected System.Web.UI.WebControls.Label lblqno91;
		protected System.Web.UI.WebControls.Label lblprep9;
		protected System.Web.UI.WebControls.Label lbledate9;
		protected System.Web.UI.WebControls.Label lbldelivery9;
		protected System.Web.UI.WebControls.Label lblcode9;
		protected System.Web.UI.WebControls.Label lblterms9;
		protected System.Web.UI.WebControls.Label lblqdate9;
		protected System.Web.UI.WebControls.Label lblnotes9;
		protected System.Web.UI.WebControls.Label lblcurrency9;
		protected System.Web.UI.WebControls.Label lblinfo9;
		protected System.Web.UI.WebControls.Label lblbill91;
		protected System.Web.UI.WebControls.Label lblbill92;
		protected System.Web.UI.WebControls.Label lblbill93;
		protected System.Web.UI.WebControls.Label lblatt93;
		protected System.Web.UI.WebControls.Label lblcomment9;
		protected System.Web.UI.WebControls.Label lbltotalprice9;
		protected System.Web.UI.WebControls.Label lbloffice10;
		protected System.Web.UI.WebControls.Label lblship101;
		protected System.Web.UI.WebControls.Label lblship102;
		protected System.Web.UI.WebControls.Label lblship103;
		protected System.Web.UI.WebControls.Label lblqno10;
		protected System.Web.UI.WebControls.Label lblprep10;
		protected System.Web.UI.WebControls.Label lbledate10;
		protected System.Web.UI.WebControls.Label lbldelivery10;
		protected System.Web.UI.WebControls.Label lblcode10;
		protected System.Web.UI.WebControls.Label lblterms10;
		protected System.Web.UI.WebControls.Label lblqdate10;
		protected System.Web.UI.WebControls.Label lblnotes10;
		protected System.Web.UI.WebControls.Label lblcurrency10;
		protected System.Web.UI.WebControls.Label lblinfo10;
		protected System.Web.UI.WebControls.Label lblbill101;
		protected System.Web.UI.WebControls.Label lblbill102;
		protected System.Web.UI.WebControls.Label lblbill103;
		protected System.Web.UI.WebControls.Label lblatt101;
		protected System.Web.UI.WebControls.Label lblatt102;
		protected System.Web.UI.WebControls.Label lblatt103;
		protected System.Web.UI.WebControls.Label lblcomment10;
		protected System.Web.UI.WebControls.Label lbltotalprice10;
		protected System.Web.UI.WebControls.Label lblt7;
		protected System.Web.UI.WebControls.Label lblt8;
		protected System.Web.UI.WebControls.Label lblt9;
		protected System.Web.UI.WebControls.Label lblt10;
		protected System.Web.UI.WebControls.Panel ptotal7;
		protected System.Web.UI.WebControls.Panel page7;
		protected System.Web.UI.WebControls.Panel ptotal8;
		protected System.Web.UI.WebControls.Panel page8;
		protected System.Web.UI.WebControls.Panel ptotal9;
		protected System.Web.UI.WebControls.Panel page9;
		protected System.Web.UI.WebControls.Panel ptotal10;
		protected System.Web.UI.WebControls.Panel page10;
		DBClass db =new DBClass();
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["User"] !=null)
			{
				RegisterClientScriptBlock("", "<script>top.window.moveTo(0,0); top.window.resizeTo(710,screen.availHeight);</script>");
				if (Request.QueryString["id"] != null)
				{
				
					page2.Visible=false;
					page3.Visible=false;
					page4.Visible=false;
					page5.Visible=false;
					page6.Visible=false;
					string qno =Request.QueryString["id"].Trim();
					Quotation quot =new Quotation();
					quot =db.SelectCustomerDetails(qno);
					if(quot.Office =="Montreal")
					{
						LblOffice.Text = "6194 Notre-Dame West<br>Montreal, Quebec, Canada H4C 1V4<br>T:(514)341-3415 F:(514)341-0249";
						lbloffice2.Text = "6194 Notre-Dame West<br>Montreal, Quebec, Canada H4C 1V4<br>T:(514)341-3415 F:(514)341-0249";
						lbloffice3.Text = "6194 Notre-Dame West<br>Montreal, Quebec, Canada H4C 1V4<br>T:(514)341-3415 F:(514)341-0249";
						lbloffice4.Text = "6194 Notre-Dame West<br>Montreal, Quebec, Canada H4C 1V4<br>T:(514)341-3415 F:(514)341-0249";
						lbloffice5.Text = "6194 Notre-Dame West<br>Montreal, Quebec, Canada H4C 1V4<br>T:(514)341-3415 F:(514)341-0249";
						lbloffice6.Text = "6194 Notre-Dame West<br>Montreal, Quebec, Canada H4C 1V4<br>T:(514)341-3415 F:(514)341-0249";
						lbloffice7.Text = "6194 Notre-Dame West<br>Montreal, Quebec, Canada H4C 1V4<br>T:(514)341-3415 F:(514)341-0249";
						lbloffice8.Text = "6194 Notre-Dame West<br>Montreal, Quebec, Canada H4C 1V4<br>T:(514)341-3415 F:(514)341-0249";
						lbloffice9.Text = "6194 Notre-Dame West<br>Montreal, Quebec, Canada H4C 1V4<br>T:(514)341-3415 F:(514)341-0249";
						lbloffice10.Text = "6194 Notre-Dame West<br>Montreal, Quebec, Canada H4C 1V4<br>T:(514)341-3415 F:(514)341-0249";
					
					}
					else
					{
						LblOffice.Text = "211 Watline Ave, S.205<br>Mississauga, Ontario, L4Z 1P3<br>T:(905)829-2910 F:(905)829-4593";
						lbloffice2.Text = "211 Watline Ave, S.205<br>Mississauga, Ontario, L4Z 1P3<br>T:(905)829-2910 F:(905)829-4593";
						lbloffice3.Text = "211 Watline Ave, S.205<br>Mississauga, Ontario, L4Z 1P3<br>T:(905)829-2910 F:(905)829-4593";
						lbloffice4.Text = "211 Watline Ave, S.205<br>Mississauga, Ontario, L4Z 1P3<br>T:(905)829-2910 F:(905)829-4593";
						lbloffice5.Text = "211 Watline Ave, S.205<br>Mississauga, Ontario, L4Z 1P3<br>T:(905)829-2910 F:(905)829-4593";
						lbloffice6.Text = "211 Watline Ave, S.205<br>Mississauga, Ontario, L4Z 1P3<br>T:(905)829-2910 F:(905)829-4593";
						lbloffice7.Text = "211 Watline Ave, S.205<br>Mississauga, Ontario, L4Z 1P3<br>T:(905)829-2910 F:(905)829-4593";
						lbloffice8.Text = "211 Watline Ave, S.205<br>Mississauga, Ontario, L4Z 1P3<br>T:(905)829-2910 F:(905)829-4593";
						lbloffice9.Text = "211 Watline Ave, S.205<br>Mississauga, Ontario, L4Z 1P3<br>T:(905)829-2910 F:(905)829-4593";
						lbloffice10.Text = "211 Watline Ave, S.205<br>Mississauga, Ontario, L4Z 1P3<br>T:(905)829-2910 F:(905)829-4593";
					}
					LblAttn1.Text = quot.AttnTo1.ToString();
					LblAttn2.Text = quot.AttnTo2.ToString();
					LblAttn3.Text = quot.AttnTo3.ToString();
					lblatt21.Text = quot.AttnTo1.ToString();
					lblatt22.Text = quot.AttnTo2.ToString();
					lblatt23.Text = quot.AttnTo3.ToString();
					lblatt31.Text = quot.AttnTo1.ToString();
					lblatt32.Text = quot.AttnTo2.ToString();
					lblatt33.Text = quot.AttnTo3.ToString();
					lblatt41.Text = quot.AttnTo1.ToString();
					lblatt42.Text = quot.AttnTo2.ToString();
					lblatt43.Text = quot.AttnTo3.ToString();
					lblatt51.Text = quot.AttnTo1.ToString();
					lblatt52.Text = quot.AttnTo2.ToString();
					lblatt53.Text = quot.AttnTo3.ToString();
					lblatt61.Text = quot.AttnTo1.ToString();
					lblatt62.Text = quot.AttnTo2.ToString();
					lblatt63.Text = quot.AttnTo3.ToString();
					lblatt71.Text = quot.AttnTo1.ToString();
					lblatt72.Text = quot.AttnTo2.ToString();
					lblatt73.Text = quot.AttnTo3.ToString();
					lblatt81.Text = quot.AttnTo1.ToString();
					lblatt82.Text = quot.AttnTo2.ToString();
					lblatt83.Text = quot.AttnTo3.ToString();
					lblatt91.Text = quot.AttnTo1.ToString();
					lblatt92.Text = quot.AttnTo2.ToString();
					lblatt93.Text = quot.AttnTo3.ToString();
					lblatt101.Text = quot.AttnTo1.ToString();
					lblatt102.Text = quot.AttnTo2.ToString();
					lblatt103.Text = quot.AttnTo3.ToString();
				
					LblBill1.Text = quot.BillTo1.ToString();
					LblBill2.Text = quot.BillTo2.ToString();
					LblBill3.Text = quot.BillTo3.ToString();
					lblbill21.Text = quot.BillTo1.ToString();
					lblbill22.Text = quot.BillTo2.ToString();
					lblbill23.Text = quot.BillTo3.ToString();
					lblbill31.Text = quot.BillTo1.ToString();
					lblbill32.Text = quot.BillTo2.ToString();
					lblbill33.Text = quot.BillTo3.ToString();
					lblbill41.Text = quot.BillTo1.ToString();
					lblbill42.Text = quot.BillTo2.ToString();
					lblbill43.Text = quot.BillTo3.ToString();
					lblbill51.Text = quot.BillTo1.ToString();
					lblbill52.Text = quot.BillTo2.ToString();
					lblbill53.Text = quot.BillTo3.ToString();
					lblbill61.Text = quot.BillTo1.ToString();
					lblbill62.Text = quot.BillTo2.ToString();
					lblbill63.Text = quot.BillTo3.ToString();
					lblbill71.Text = quot.BillTo1.ToString();
					lblbill72.Text = quot.BillTo2.ToString();
					lblbill73.Text = quot.BillTo3.ToString();
					lblbill81.Text = quot.BillTo1.ToString();
					lblbill82.Text = quot.BillTo2.ToString();
					lblbill83.Text = quot.BillTo3.ToString();
					lblbill91.Text = quot.BillTo1.ToString();
					lblbill92.Text = quot.BillTo2.ToString();
					lblbill93.Text = quot.BillTo3.ToString();
					lblbill101.Text = quot.BillTo1.ToString();
					lblbill102.Text = quot.BillTo2.ToString();
					lblbill103.Text = quot.BillTo3.ToString();
			
				
					LblShip1.Text = quot.ShipTo1.ToString();
					LblShip2.Text = quot.ShipTo2.ToString();
					LblShip3.Text = quot.ShipTo3.ToString();
					lblship21.Text = quot.ShipTo1.ToString();
					lblship22.Text = quot.ShipTo2.ToString();
					lblship23.Text = quot.ShipTo3.ToString();
					lblship31.Text = quot.ShipTo1.ToString();
					lblship32.Text = quot.ShipTo2.ToString();
					lblship33.Text = quot.ShipTo3.ToString();
					llblship41.Text = quot.ShipTo1.ToString();
					llblship42.Text = quot.ShipTo2.ToString();
					llblship43.Text = quot.ShipTo3.ToString();
					lblship51.Text = quot.ShipTo1.ToString();
					lblship52.Text = quot.ShipTo2.ToString();
					lblship53.Text = quot.ShipTo3.ToString();
					lblship61.Text = quot.ShipTo1.ToString();
					lblship62.Text = quot.ShipTo2.ToString();
					lblship63.Text = quot.ShipTo3.ToString();
					lblship71.Text = quot.ShipTo1.ToString();
					lblship72.Text = quot.ShipTo2.ToString();
					lblship73.Text = quot.ShipTo3.ToString();
					lblship81.Text = quot.ShipTo1.ToString();
					lblship82.Text = quot.ShipTo2.ToString();
					lblship83.Text = quot.ShipTo3.ToString();
					lblship91.Text = quot.ShipTo1.ToString();
					lblship92.Text = quot.ShipTo2.ToString();
					lblship93.Text = quot.ShipTo3.ToString();
					lblship101.Text = quot.ShipTo1.ToString();
					lblship102.Text = quot.ShipTo2.ToString();
					lblship103.Text = quot.ShipTo3.ToString();


					LblQno.Text = quot.QuoteNo.ToString();
					lblqno2.Text = quot.QuoteNo.ToString();
					lblqno3.Text = quot.QuoteNo.ToString();
					lblqno4.Text = quot.QuoteNo.ToString();
					lblqno5.Text = quot.QuoteNo.ToString();
					lblqno6.Text = quot.QuoteNo.ToString();
					lblqno7.Text = quot.QuoteNo.ToString();
					lblqno8.Text = quot.QuoteNo.ToString();
					lblqno91.Text = quot.QuoteNo.ToString();
					lblqno10.Text = quot.QuoteNo.ToString();

					LblQdate.Text = quot.Quotedate.ToString();
					lblqdate2.Text = quot.Quotedate.ToString();
					lblqdate3.Text = quot.Quotedate.ToString();
					lblqdate4.Text = quot.Quotedate.ToString();
					lblqdate5.Text = quot.Quotedate.ToString();
					lblqdate6.Text = quot.Quotedate.ToString();
					lblqdate7.Text = quot.Quotedate.ToString();
					lblqdate8.Text = quot.Quotedate.ToString();
					lblqdate9.Text = quot.Quotedate.ToString();
					lblqdate10.Text = quot.Quotedate.ToString();
				
					LblEdates.Text = quot.ExpiryDate.ToString();
					lbledate2.Text = quot.ExpiryDate.ToString();
					lbledate3.Text = quot.ExpiryDate.ToString();
					lbledate4.Text = quot.ExpiryDate.ToString();
					lbledate5.Text = quot.ExpiryDate.ToString();
					lbledate6.Text = quot.ExpiryDate.ToString();
					lbledate7.Text = quot.ExpiryDate.ToString();
					lbledate8.Text = quot.ExpiryDate.ToString();
					lbledate9.Text = quot.ExpiryDate.ToString();
					lbledate10.Text = quot.ExpiryDate.ToString();


					LblPBy.Text = quot.PrepairedBy.ToString();
					lblprep2.Text = quot.PrepairedBy.ToString();
					lblprep3.Text = quot.PrepairedBy.ToString();
					lblprep4.Text = quot.PrepairedBy.ToString();
					lblprep5.Text = quot.PrepairedBy.ToString();
					lblprep6.Text = quot.PrepairedBy.ToString();
					lblprep7.Text = quot.PrepairedBy.ToString();
					lblprep8.Text = quot.PrepairedBy.ToString();
					lblprep9.Text = quot.PrepairedBy.ToString();
					lblprep10.Text = quot.PrepairedBy.ToString();
				
					lblCode.Text =quot.Code.ToString();
					lblcode2.Text =quot.Code.ToString();
					lblcode3.Text =quot.Code.ToString();
					lblcode4.Text =quot.Code.ToString();
					lblcode5.Text =quot.Code.ToString();
					lblcode6.Text =quot.Code.ToString();
					lblcode7.Text =quot.Code.ToString();
					lblcode8.Text =quot.Code.ToString();
					lblcode9.Text =quot.Code.ToString();
					lblcode10.Text =quot.Code.ToString();
				
					LblTerms.Text = quot.Terms.ToString();
					lblterm2.Text = quot.Terms.ToString();
					lblterms3.Text = quot.Terms.ToString();
					lblterm4.Text = quot.Terms.ToString();
					lblterm5.Text = quot.Terms.ToString();
					lblterm6.Text = quot.Terms.ToString();
					lblterms7.Text = quot.Terms.ToString();
					lblterms8.Text = quot.Terms.ToString();
					lblterms9.Text = quot.Terms.ToString();
					lblterms10.Text = quot.Terms.ToString();

					LblDelivery.Text = quot.Delivery.ToString();
					lbldelivery2.Text = quot.Delivery.ToString();
					lbldelivery3.Text = quot.Delivery.ToString();
					lbldelivery4.Text = quot.Delivery.ToString();
					lbldelivery5.Text = quot.Delivery.ToString();
					lbldelivery6.Text = quot.Delivery.ToString();
					lbldelivery7.Text = quot.Delivery.ToString();
					lbldelivery8.Text = quot.Delivery.ToString();
					lbldelivery9.Text = quot.Delivery.ToString();
					lbldelivery10.Text = quot.Delivery.ToString();
				
					LblCurrency.Text=quot.Currency.ToString();
					lblcurrency2.Text=quot.Currency.ToString();
					lblcurrency3.Text=quot.Currency.ToString();
					lblcurrency4.Text=quot.Currency.ToString();
					lblcurrency5.Text=quot.Currency.ToString();
					lblcurrency6.Text=quot.Currency.ToString();
					lblcurrency7.Text=quot.Currency.ToString();
					lblcurrency8.Text=quot.Currency.ToString();
					lblcurrency9.Text=quot.Currency.ToString();
					lblcurrency10.Text=quot.Currency.ToString();

					if(quot.Currency.ToString() =="CN $")
					{
						lblt1.Text="Total(CAD):";
						lblt2.Text="Total(CAD):";
						lblt3.Text="Total(CAD):";
						lblt4.Text="Total(CAD):";
						lblt5.Text="Total(CAD):";
						lblt6.Text="Total(CAD):";
						lblt7.Text="Total(CAD):";
						lblt8.Text="Total(CAD):";
						lblt9.Text="Total(CAD):";
						lblt10.Text="Total(CAD):";
					}
					else
					{
						lblt1.Text="Total(USD):";
						lblt2.Text="Total(USD):";
						lblt3.Text="Total(USD):";
						lblt4.Text="Total(USD):";
						lblt5.Text="Total(USD):";
						lblt6.Text="Total(USD):";
						lblt7.Text="Total(USD):";
						lblt8.Text="Total(USD):";
						lblt9.Text="Total(USD):";
						lblt10.Text="Total(USD):";
					}
				
					lblnote.Text = quot.Note.ToString();
					lblnote2.Text = quot.Note.ToString();
					lblnote3.Text = quot.Note.ToString();
					lblnote4.Text = quot.Note.ToString();
					lblnote5.Text = quot.Note.ToString();
					lblnote6.Text = quot.Note.ToString();
					lblnotes7.Text = quot.Note.ToString();
					lblnotes8.Text = quot.Note.ToString();
					lblnotes9.Text = quot.Note.ToString();
					lblnotes10.Text = quot.Note.ToString();
				
					ShowComments(quot.QuoteNo.ToString());
					showtable(quot.QuoteNo.ToString().Trim()); 
					
				}
			}
			else
			{
				Response.Redirect("Default.htm");
			}
		}

				public void showtable(string qno)
				{   int z101cont=0;
					string head= "<TABLE id='able1' style='Z-INDEX: 101; LEFT: 0px; WIDTH: 100%; TOP: 0px; FONT-SIZE: 8pt; FONT-FAMILY: Arial;' cellSpacing='1' cellPadding='1' border='0'><TR style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Arial; TEXT-DECORATION: underline' bgColor='#c0c0c0' borderColor='black'><TD style='FONT-WEIGHT: bold; WIDTH: 135px;' noWrap align='center' >Items </TD><TD style='FONT-WEIGHT: bold;WIDTH: 320px;' noWrap align='center' >Description</TD><TD style='FONT-WEIGHT: bold;WIDTH: 50px;' noWrap align='center'>Quantity</TD><TD style='FONT-WEIGHT: bold;WIDTH: 70px;' noWrap align='center'>Price</TD><TD style='FONT-WEIGHT: bold;WIDTH:80px;' noWrap align='center'>Extension</TD></TR>";
					string end="</TR></TABLE>";	
					string body="";
					decimal totals =0.00m;
					decimal dc1 =0.00m;
					string tot="";
					decimal mprice=0.00m;
					ArrayList list=new ArrayList();
					list =db.FindItemsTOQuote(qno.ToString().Trim());
					ArrayList acclist =new ArrayList();
					acclist =db.SelectAccessory(qno.ToString().Trim());
					ArrayList Replist =new ArrayList();
					Replist =db.SelectRepairkit(qno.ToString().Trim());
					ArrayList z101list =new ArrayList();
					z101list =db.Select_Z101_ByQno(qno.ToString().Trim());
					QItems QItem=new QItems();	
					if(list.Count>0)
					{
						int lcount=0;
						lcount =1;
											
						for(int i=0;i<lcount;i++)
						{
							QItem= db.SelectAllItems(list[i].ToString(),qno.Trim());
							ArrayList applist =new ArrayList();
							ArrayList temp =new ArrayList();
							string net ="";
							if(QItem.UnitPrice.ToString().Trim() =="0.00")
							{
								net =" TBA";
							}
							else
							{
								net =String.Format("{0:$###,###.00}",Convert.ToDecimal(QItem.UnitPrice));
							}
							if(QItem.TotalPrice.ToString().Trim() =="0.00")
							{
								tot =" TBA";
							}
							else
							{
							    tot = String.Format("{0:$###,###.00}",Convert.ToDecimal(QItem.TotalPrice));
							}
				
							dc1 =Convert.ToDecimal(QItem.TotalPrice.ToString());
							mprice=Decimal.Round(dc1,2);
							body  +="<TR><TD style='WIDTH: 150px; FONT-WEIGHT: bold;' noWrap align='left'  bgColor='buttonface' colspan='2' >"+list[0].ToString()+"</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+QItem.Quantity.ToString()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+net+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+tot+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD   align='left'>"+QItem.Series.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD   align='left'>"+QItem.Bore.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD   align='left'>"+QItem.Rod.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD   align='left'>"+QItem.RodEnd.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD   align='left'>"+QItem.Cushion.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							if(QItem.CushionPos.ToString().Trim().Length >2)
							{
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD   align='left'>"+QItem.CushionPos.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							}
							if(QItem.ApplicationOpt_Id !="")
							{
								applist =db.SelectApplicationopts(QItem.Quotation_No.Trim(),QItem.PartNo.Trim());
								for(int h=0;h<applist.Count;h++)
								{
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD   align='left'>"+ applist[h].ToString()+" </TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
									
								}
							}
							string qtybrks="";
							ArrayList qtylist=new ArrayList();
							qtylist =db.SelectQtyBreaks(QItem.PartNo.ToString(),qno.Trim());
							if(qtylist.Count >0)
							{
								string h1="<TABLE id='Table10' style='Z-INDEX: 109; LEFT: 0; WIDTH: 100%; TOP: 0; FONT-SIZE: 8pt; FONT-FAMILY: Arial'cellSpacing='0' cellPadding='0' border='1' borderColor='#dcdcdc'>"
									+"<TR><TD noWrap align='center' colSpan='3' bgColor='buttonface'>Quantity Breaks</TD></TR>"
									+"<TR><TD style='WIDTH: 30%;' noWrap align='center'>Qty</TD><TD style='WIDTH: 30%;' noWrap align='center'>Unit Price</TD><TD style='WIDTH: 40%;' noWrap align='center'>Total Price</TD></TR>";
								string b1="";
								for(int p=0;p<qtylist.Count;p++)
								{
									Opts op =new Opts();
									op =(Opts)qtylist[p];
									decimal qd1,qd2,qd3=0.00m;
									qd1=Convert.ToDecimal(op.Code.Trim());
									qd2=Convert.ToDecimal(op.Free.Trim());
									qd3=qd1 * qd2;
									b1 +="<TR align='center'><TD  style='FONT-SIZE: 8pt; FONT-FAMILY: Arial' noWrap>"+op.Code.Trim()+"</TD><TD  noWrap>"+String.Format("{0:$###,###.00}",qd2)+"</TD><TD noWrap>"+String.Format("{0:$###,###.00}",qd3)+"</TD></TR>";
								}
								b1 +="</TR></TABLE>";
								qtybrks = h1.ToString()+b1.ToString();
							}

							body +="<TR><TD noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Seal.ToString()+"</TD><TD  style='WIDTH: 100%;' noWrap colSpan='3' rowspan='6'>"+qtybrks.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD   align='left'>"+QItem.Port.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD   align='left'>"+QItem.PortPos.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD   align='left'>"+QItem.Mount.ToString()+"</TD></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD   align='left'>"+QItem.Stroke.ToString()+"</TD></TD></TR>";
						
							ArrayList poplist=new ArrayList();
							if(QItem.PopularOpt_Id !="")
							{
								poplist =db.SelectPopularoptions(QItem.Quotation_No.Trim(),QItem.PartNo.Trim());
							
								for(int m=0;m<poplist.Count;m++)
								{
									string ppp="";
									if(poplist[m].ToString().Substring(0,2) =="(T")
									{
										ppp=poplist[m].ToString().Replace(",","<br>");
									}
									else
									{
										ppp=poplist[m].ToString();
									}
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD   align='left'>"+ ppp.ToString()+" </TD></TR>";
								
								}
							}
							if(QItem.Special_ID !="")
							{
								temp =db.SelectSpecial(QItem.PartNo.ToString(),qno.Trim());
								for(int j=0;j<temp.Count;j++)
								{
									string sp="";
									sp=temp[j].ToString();
                                   string p= sp.Replace(";","<br>");
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left' ></TD><TD  align='left' style='WIDTH: 318px'>"+ p.ToString()+" </TD></TR>";
								
								}
							}
						
							totals =totals + mprice;
						}
					}

					if(list.Count == 0 && z101list.Count>0)
					{
						if(z101list.Count >0)
						{
							int lcount=0;
							lcount =1;
							z101cont=1;
							for(int s =0;s<lcount;s++)
							{
								Others oth =new Others();
								oth =(Others)z101list[s];
								decimal dc2=Convert.ToDecimal(oth.UnitP.ToString());
								string listprice=String.Format("{0:$###,###.00}",dc2);
								decimal dc3=Convert.ToDecimal(oth.Price.ToString());
								string totalprice=String.Format("{0:$###,###.00}",dc3);
								body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left'  bgColor='buttonface' >"+oth.Code.ToString().Trim()+"</TD><TD bgColor='buttonface'  align='left'></TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+oth.Qty.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+listprice.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+totalprice.ToString().Trim()+"</TD></TR>";
								
								string qtybrks="";
								ArrayList qtylist=new ArrayList();
								qtylist =db.SelectQtyBreaks(oth.Code.ToString().Trim(),qno.Trim());
								if(qtylist.Count >0)
								{
									string h1="<TABLE id='Table10' style='Z-INDEX: 109; LEFT: 0; WIDTH: 100%; TOP: 0; FONT-SIZE: 8pt; FONT-FAMILY: Arial'cellSpacing='0' cellPadding='0' border='1' borderColor='#dcdcdc'>"
										+"<TR><TD noWrap align='center' colSpan='3' bgColor='buttonface'>Quantity Breaks</TD></TR>"
										+"<TR><TD style='WIDTH: 30%;' noWrap align='center'>Qty</TD><TD style='WIDTH: 30%;' noWrap align='center'>Unit Price</TD><TD style='WIDTH: 40%;' noWrap align='center'>Total Price</TD></TR>";
									string b1="";
									for(int p=0;p<qtylist.Count;p++)
									{
										Opts op =new Opts();
										op =(Opts)qtylist[p];
										decimal qd1,qd2,qd3=0.00m;
										qd1=Convert.ToDecimal(op.Code.Trim());
										qd2=Convert.ToDecimal(op.Free.Trim());
										qd3=qd1 * qd2;
										b1 +="<TR align='center'><TD  style='FONT-SIZE: 8pt; FONT-FAMILY: Arial' noWrap>"+op.Code.Trim()+"</TD><TD  noWrap>"+String.Format("{0:$###,###.00}",qd2)+"</TD><TD noWrap>"+String.Format("{0:$###,###.00}",qd3)+"</TD></TR>";
									}
									b1 +="</TR></TABLE>";
									qtybrks = h1.ToString()+b1.ToString();
								}
								string sp="";
								sp=oth.Desc.ToString().Trim();
								string zp= sp.Replace(";","<br>");
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD style='WIDTH: 318px' noWrap align='left'>"+zp.Trim()+"</TD><TD  style='WIDTH: 100%;' noWrap colSpan='3' rowspan='6'>"+qtybrks.ToString()+"</TD></TR>";
								totals =totals + dc3;
							}
					
						}
					}
					if((list.Count == 0 && z101list.Count==0 && acclist.Count>0) ||(list.Count == 0 && z101list.Count==0 && Replist.Count>0))
					{
						if(acclist.Count >0)
						{
							for(int s =0;s<acclist.Count;s++)
							{
								Others oth =new Others();
								oth =(Others)acclist[s];
								decimal dc2=Convert.ToDecimal(oth.UnitP.ToString());
								string listprice=String.Format("{0:$###,###.00}",dc2);
								decimal dc3=Convert.ToDecimal(oth.Price.ToString());
								string totalprice=String.Format("{0:$###,###.00}",dc3);
								body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left'  bgColor='buttonface' >"+oth.Code.ToString().Trim()+"</TD><TD bgColor='buttonface' noWrap align='left'>"+oth.Desc.ToString().Trim()+"</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+oth.Qty.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+listprice.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+totalprice.ToString().Trim()+"</TD></TR>";
								totals =totals + dc3;
							}
					
						}
						
						if(Replist.Count >0)
						{
		
							for(int s =0;s<Replist.Count;s++)
							{
								Others oth =new Others();
								oth =(Others)Replist[s];
								decimal dc2=Convert.ToDecimal(oth.UnitP.ToString());
								string listprice=String.Format("{0:$###,###.00}",dc2);
								decimal dc3=Convert.ToDecimal(oth.Price.ToString());
								string totalprice=String.Format("{0:$###,###.00}",dc3);
								body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left'  bgColor='buttonface' >"+oth.Code.ToString().Trim()+"</TD><TD bgColor='buttonface' noWrap align='left'>"+oth.Desc.ToString().Trim()+"("+oth.Desc1.ToString().Trim()+")</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+oth.Qty.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+listprice.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+totalprice.ToString().Trim()+"</TD></TR>";
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+oth.Desc2.ToString().Trim()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+oth.Desc3.ToString().Trim()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
								totals =totals + dc3;
							}
					
						}
					}	
					Lblinfo.Text=head + body + end;
					if(totals ==0)
					{
						LblTotal.Text = "TBA";
					}
					else
					{
						LblTotal.Text = String.Format("{0:$###,###.00}",totals);
					}
					body="";
					if(list.Count >1)
					{
						page2.Visible=true;
						ptotal1.Visible=false;
						int lcount=0;
						lcount =2;
						
						for(int i=1;i<lcount;i++)
						{
							QItem= db.SelectAllItems(list[i].ToString(),qno.Trim());
							ArrayList applist =new ArrayList();
							ArrayList temp =new ArrayList();
							string net ="";
							if(QItem.UnitPrice.ToString().Trim() =="0.00")
							{
								net =" TBA";
							}
							else
							{
								net =String.Format("{0:$###,###.00}",Convert.ToDecimal(QItem.UnitPrice));
							}
							if(QItem.TotalPrice.ToString().Trim() =="0.00")
							{
								tot =" TBA";
							}
							else
							{
								tot = String.Format("{0:$###,###.00}",Convert.ToDecimal(QItem.TotalPrice));
							}
							dc1 =Convert.ToDecimal(QItem.TotalPrice.ToString());
							mprice=Decimal.Round(dc1,2);
							body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left' colspan='2' bgColor='buttonface' >"+list[i].ToString()+"</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+QItem.Quantity.ToString()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+net+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+tot+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD   align='left'>"+QItem.Series.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD   align='left'>"+QItem.Bore.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD   align='left'>"+QItem.Rod.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD   align='left'>"+QItem.RodEnd.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD   align='left'>"+QItem.Cushion.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							if(QItem.CushionPos.ToString().Trim().Length >2)
							{
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD   align='left'>"+QItem.CushionPos.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							}
							if(QItem.ApplicationOpt_Id !="")
							{
								applist =db.SelectApplicationopts(QItem.Quotation_No.Trim(),QItem.PartNo.Trim());
								for(int h=0;h<applist.Count;h++)
								{
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD   align='left'>"+ applist[h].ToString()+" </TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
									
								}
							}
							string qtybrks="";
							ArrayList qtylist=new ArrayList();
							qtylist =db.SelectQtyBreaks(QItem.PartNo.ToString(),qno.Trim());
							if(qtylist.Count >0)
							{
								string h1="<TABLE id='Table10' style='Z-INDEX: 109; LEFT: 0; WIDTH: 100%; TOP: 0; FONT-SIZE: 8pt; FONT-FAMILY: Arial'cellSpacing='0' cellPadding='0' border='1' borderColor='#dcdcdc'>"
									+"<TR><TD noWrap align='center' colSpan='3' bgColor='buttonface'>Quantity Breaks</TD></TR>"
									+"<TR><TD style='WIDTH: 30%;' noWrap align='center'>Qty</TD><TD style='WIDTH: 30%;' noWrap align='center'>Unit Price</TD><TD style='WIDTH: 40%;' noWrap align='center'>Total Price</TD></TR>";
								string b1="";
								for(int p=0;p<qtylist.Count;p++)
								{
									Opts op =new Opts();
									op =(Opts)qtylist[p];
									decimal qd1,qd2,qd3=0.00m;
									qd1=Convert.ToDecimal(op.Code.Trim());
									qd2=Convert.ToDecimal(op.Free.Trim());
									qd3=qd1 * qd2;
									b1 +="<TR align='center'><TD  style='FONT-SIZE: 8pt; FONT-FAMILY: Arial' noWrap>"+op.Code.Trim()+"</TD><TD  noWrap>"+String.Format("{0:$###,###.00}",qd2)+"</TD><TD noWrap>"+String.Format("{0:$###,###.00}",qd3)+"</TD></TR>";
								}
								b1 +="</TR></TABLE>";
								qtybrks = h1.ToString()+b1.ToString();
							}

							body +="<TR><TD noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Seal.ToString()+"</TD><TD  style='WIDTH: 100%;' noWrap colSpan='3' rowspan='6'>"+qtybrks.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Port.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.PortPos.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Mount.ToString()+"</TD></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Stroke.ToString()+"</TD></TD></TR>";
						
							ArrayList poplist=new ArrayList();
							if(QItem.PopularOpt_Id !="")
							{
								poplist =db.SelectPopularoptions(QItem.Quotation_No.Trim(),QItem.PartNo.Trim());
							
								for(int m=0;m<poplist.Count;m++)
								{
									string ppp="";
									if(poplist[m].ToString().Substring(0,2) =="(T")
									{
										ppp=poplist[m].ToString().Replace(",","<br>");
									}
									else
									{
										ppp=poplist[m].ToString();
									}
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+ ppp.ToString()+" </TD></TR>";
								}
							}
							if(QItem.Special_ID !="")
							{
								temp =db.SelectSpecial(QItem.PartNo.ToString(),qno.Trim());
								for(int j=0;j<temp.Count;j++)
								{
									string sp="";
									sp=temp[j].ToString();
									string p= sp.Replace(";","<br>");
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left' ></TD><TD noWrap align='left' style='WIDTH: 318px'>"+ p.ToString()+" </TD></TR>";
								
								}
							}
							totals =totals + mprice;
						}
					}
					if(list.Count <= 1 && z101list.Count>0)
					{
						
						if(z101cont ==0)
						{
							page2.Visible=true;
							ptotal1.Visible=false;
							int lcount=0;
							lcount =z101cont+1;
							z101cont=2;
							for(int s =0;s<lcount;s++)
							{
								Others oth =new Others();
								oth =(Others)z101list[s];
								decimal dc2=Convert.ToDecimal(oth.UnitP.ToString());
								string listprice=String.Format("{0:$###,###.00}",dc2);
								decimal dc3=Convert.ToDecimal(oth.Price.ToString());
								string totalprice=String.Format("{0:$###,###.00}",dc3);
								body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left'  bgColor='buttonface' >"+oth.Code.ToString().Trim()+"</TD><TD bgColor='buttonface'  align='left'></TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+oth.Qty.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+listprice.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+totalprice.ToString().Trim()+"</TD></TR>";
								
								string qtybrks="";
								ArrayList qtylist=new ArrayList();
								qtylist =db.SelectQtyBreaks(oth.Code.ToString().Trim(),qno.Trim());
								if(qtylist.Count >0)
								{
									string h1="<TABLE id='Table10' style='Z-INDEX: 109; LEFT: 0; WIDTH: 100%; TOP: 0; FONT-SIZE: 8pt; FONT-FAMILY: Arial'cellSpacing='0' cellPadding='0' border='1' borderColor='#dcdcdc'>"
										+"<TR><TD noWrap align='center' colSpan='3' bgColor='buttonface'>Quantity Breaks</TD></TR>"
										+"<TR><TD style='WIDTH: 30%;' noWrap align='center'>Qty</TD><TD style='WIDTH: 30%;' noWrap align='center'>Unit Price</TD><TD style='WIDTH: 40%;' noWrap align='center'>Total Price</TD></TR>";
									string b1="";
									for(int p=0;p<qtylist.Count;p++)
									{
										Opts op =new Opts();
										op =(Opts)qtylist[p];
										decimal qd1,qd2,qd3=0.00m;
										qd1=Convert.ToDecimal(op.Code.Trim());
										qd2=Convert.ToDecimal(op.Free.Trim());
										qd3=qd1 * qd2;
										b1 +="<TR align='center'><TD  style='FONT-SIZE: 8pt; FONT-FAMILY: Arial' noWrap>"+op.Code.Trim()+"</TD><TD  noWrap>"+String.Format("{0:$###,###.00}",qd2)+"</TD><TD noWrap>"+String.Format("{0:$###,###.00}",qd3)+"</TD></TR>";
									}
									b1 +="</TR></TABLE>";
									qtybrks = h1.ToString()+b1.ToString();
								}
								string sp="";
								sp=oth.Desc.ToString().Trim();
								string zp= sp.Replace(";","<br>");
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD style='WIDTH: 318px' noWrap align='left'>"+zp.Trim()+"</TD><TD  style='WIDTH: 100%;' noWrap colSpan='3' rowspan='6'>"+qtybrks.ToString()+"</TD></TR>";
								totals =totals + dc3;
							}
					
						}
					}
					if((list.Count == 1 && acclist.Count>0) || (z101list.Count == 1 && acclist.Count>0) ||(list.Count == 1&& Replist.Count>0) ||(z101list.Count == 1&& Replist.Count>0) )
					{
						page2.Visible=true;
						ptotal1.Visible=false;
						acclist =new ArrayList();
						acclist =db.SelectAccessory(qno.ToString().Trim());
						if(acclist.Count >0)
						{
							for(int s =0;s<acclist.Count;s++)
							{
								Others oth =new Others();
								oth =(Others)acclist[s];
								decimal dc2=Convert.ToDecimal(oth.UnitP.ToString());
								string listprice=String.Format("{0:$###,###.00}",dc2);
								decimal dc3=Convert.ToDecimal(oth.Price.ToString());
								string totalprice=String.Format("{0:$###,###.00}",dc3);
								body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left'  bgColor='buttonface' >"+oth.Code.ToString().Trim()+"</TD><TD bgColor='buttonface' noWrap align='left'>"+oth.Desc.ToString().Trim()+"</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+oth.Qty.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+listprice.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+totalprice.ToString().Trim()+"</TD></TR>";
								totals =totals + dc3;
							}
					
						}
						Replist =new ArrayList();
						Replist =db.SelectRepairkit(qno.ToString().Trim());
						if(Replist.Count >0)
						{
		
							for(int s =0;s<Replist.Count;s++)
							{
								Others oth =new Others();
								oth =(Others)Replist[s];
								decimal dc2=Convert.ToDecimal(oth.UnitP.ToString());
								string listprice=String.Format("{0:$###,###.00}",dc2);
								decimal dc3=Convert.ToDecimal(oth.Price.ToString());
								string totalprice=String.Format("{0:$###,###.00}",dc3);
								body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left'  bgColor='buttonface' >"+oth.Code.ToString().Trim()+"</TD><TD bgColor='buttonface' noWrap align='left'>"+oth.Desc.ToString().Trim()+"("+oth.Desc1.ToString().Trim()+")</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+oth.Qty.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+listprice.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+totalprice.ToString().Trim()+"</TD></TR>";
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+oth.Desc2.ToString().Trim()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+oth.Desc3.ToString().Trim()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
								totals =totals + dc3;
							}
					
						}
					}
						lblinfo2.Text=head + body + end;
						lbltotalprice2.Text = String.Format("{0:$###,###.00}",totals);
					
					body="";
					if(list.Count >2)
					{
						page3.Visible=true;
						ptotal1.Visible=false;
						ptotal2.Visible=false;
						int lcount=0;
						lcount =3;
											
						for(int i=2;i<lcount;i++)
						{
							QItem= db.SelectAllItems(list[i].ToString(),qno.Trim());
							ArrayList applist =new ArrayList();
							ArrayList temp =new ArrayList();
							string net ="";
							if(QItem.UnitPrice.ToString().Trim() =="0.00")
							{
								net =" TBA";
							}
							else
							{
								net =String.Format("{0:$###,###.00}",Convert.ToDecimal(QItem.UnitPrice));
							}
							if(QItem.TotalPrice.ToString().Trim() =="0.00")
							{
								tot =" TBA";
							}
							else
							{
								tot = String.Format("{0:$###,###.00}",Convert.ToDecimal(QItem.TotalPrice));
							}
							dc1 =Convert.ToDecimal(QItem.TotalPrice.ToString());
							mprice=Decimal.Round(dc1,2);
							body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left' colspan='2' bgColor='buttonface' >"+list[i].ToString()+"</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+QItem.Quantity.ToString()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+net+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+tot+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Series.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Bore.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Rod.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.RodEnd.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Cushion.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							if(QItem.CushionPos.ToString().Trim().Length >2)
							{
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.CushionPos.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							}
							if(QItem.ApplicationOpt_Id !="")
							{
								applist =db.SelectApplicationopts(QItem.Quotation_No.Trim(),QItem.PartNo.Trim());
								for(int h=0;h<applist.Count;h++)
								{
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+ applist[h].ToString()+" </TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
									
								}
							}
							string qtybrks="";
							ArrayList qtylist=new ArrayList();
							qtylist =db.SelectQtyBreaks(QItem.PartNo.ToString(),qno.Trim());
							if(qtylist.Count >0)
							{
								string h1="<TABLE id='Table10' style='Z-INDEX: 109; LEFT: 0; WIDTH: 100%; TOP: 0; FONT-SIZE: 8pt; FONT-FAMILY: Arial'cellSpacing='0' cellPadding='0' border='1' borderColor='#dcdcdc'>"
									+"<TR><TD noWrap align='center' colSpan='3' bgColor='buttonface'>Quantity Breaks</TD></TR>"
									+"<TR><TD style='WIDTH: 30%;' noWrap align='center'>Qty</TD><TD style='WIDTH: 30%;' noWrap align='center'>Unit Price</TD><TD style='WIDTH: 40%;' noWrap align='center'>Total Price</TD></TR>";
								string b1="";
								for(int p=0;p<qtylist.Count;p++)
								{
									Opts op =new Opts();
									op =(Opts)qtylist[p];
									decimal qd1,qd2,qd3=0.00m;
									qd1=Convert.ToDecimal(op.Code.Trim());
									qd2=Convert.ToDecimal(op.Free.Trim());
									qd3=qd1 * qd2;
									b1 +="<TR align='center'><TD  style='FONT-SIZE: 8pt; FONT-FAMILY: Arial' noWrap>"+op.Code.Trim()+"</TD><TD  noWrap>"+String.Format("{0:$###,###.00}",qd2)+"</TD><TD noWrap>"+String.Format("{0:$###,###.00}",qd3)+"</TD></TR>";
								}
								b1 +="</TR></TABLE>";
								qtybrks = h1.ToString()+b1.ToString();
							}

							body +="<TR><TD noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Seal.ToString()+"</TD><TD  style='WIDTH: 100%;' noWrap colSpan='3' rowspan='6'>"+qtybrks.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Port.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.PortPos.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Mount.ToString()+"</TD></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Stroke.ToString()+"</TD></TD></TR>";
						
							ArrayList poplist=new ArrayList();
							if(QItem.PopularOpt_Id !="")
							{
								poplist =db.SelectPopularoptions(QItem.Quotation_No.Trim(),QItem.PartNo.Trim());
							
								for(int m=0;m<poplist.Count;m++)
								{
									string ppp="";
									if(poplist[m].ToString().Substring(0,2) =="(T")
									{
										ppp=poplist[m].ToString().Replace(",","<br>");
									}
									else
									{
										ppp=poplist[m].ToString();
									}
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+ ppp.ToString()+" </TD></TR>";
								}
							}
							if(QItem.Special_ID !="")
							{
								temp =db.SelectSpecial(QItem.PartNo.ToString(),qno.Trim());
								for(int j=0;j<temp.Count;j++)
								{
									string sp="";
									sp=temp[j].ToString();
									string p= sp.Replace(";","<br>");
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left' ></TD><TD noWrap align='left' style='WIDTH: 318px'>"+ p.ToString()+" </TD></TR>";
								
								}
							}
							totals =totals + mprice;
						}
						}
					if((list.Count == 2 && acclist.Count>0)  ||(list.Count == 2 && Replist.Count>0))
					{
						page3.Visible=true;
						ptotal1.Visible=false;
						ptotal2.Visible=false;
						acclist =new ArrayList();
						acclist =db.SelectAccessory(qno.ToString().Trim());
						if(acclist.Count >0)
						{
							for(int s =0;s<acclist.Count;s++)
							{
								Others oth =new Others();
								oth =(Others)acclist[s];
								decimal dc2=Convert.ToDecimal(oth.UnitP.ToString());
								string listprice=String.Format("{0:$###,###.00}",dc2);
								decimal dc3=Convert.ToDecimal(oth.Price.ToString());
								string totalprice=String.Format("{0:$###,###.00}",dc3);
								body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left'  bgColor='buttonface' >"+oth.Code.ToString().Trim()+"</TD><TD bgColor='buttonface' noWrap align='left'>"+oth.Desc.ToString().Trim()+"</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+oth.Qty.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+listprice.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+totalprice.ToString().Trim()+"</TD></TR>";
								totals =totals + dc3;
							}
					
						}
						Replist =new ArrayList();
						Replist =db.SelectRepairkit(qno.ToString().Trim());
						if(Replist.Count >0)
						{
		
							for(int s =0;s<Replist.Count;s++)
							{
								Others oth =new Others();
								oth =(Others)Replist[s];
								decimal dc2=Convert.ToDecimal(oth.UnitP.ToString());
								string listprice=String.Format("{0:$###,###.00}",dc2);
								decimal dc3=Convert.ToDecimal(oth.Price.ToString());
								string totalprice=String.Format("{0:$###,###.00}",dc3);
								body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left'  bgColor='buttonface' >"+oth.Code.ToString().Trim()+"</TD><TD bgColor='buttonface' noWrap align='left'>"+oth.Desc.ToString().Trim()+"("+oth.Desc1.ToString().Trim()+")</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+oth.Qty.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+listprice.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+totalprice.ToString().Trim()+"</TD></TR>";
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+oth.Desc2.ToString().Trim()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+oth.Desc3.ToString().Trim()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
								totals =totals + dc3;
							}
					
						}
					}
						lblinfo3.Text=head + body + end;
						lbltotalprice3.Text = String.Format("{0:$###,###.00}",totals);
					body="";
					if(list.Count >3)
					{
						page4.Visible=true;
						ptotal1.Visible=false;
						ptotal2.Visible=false;
						ptotal3.Visible=false;
						int lcount=0;
						lcount =4;
												
						for(int i=3;i<lcount;i++)
						{
							QItem= db.SelectAllItems(list[i].ToString(),qno.Trim());
							ArrayList applist =new ArrayList();
							ArrayList temp =new ArrayList();
							string net ="";
							if(QItem.UnitPrice.ToString().Trim() =="0.00")
							{
								net =" TBA";
							}
							else
							{
								net =String.Format("{0:$###,###.00}",Convert.ToDecimal(QItem.UnitPrice));
							}
							if(QItem.TotalPrice.ToString().Trim() =="0.00")
							{
								tot =" TBA";
							}
							else
							{
								tot = String.Format("{0:$###,###.00}",Convert.ToDecimal(QItem.TotalPrice));
							}
							dc1 =Convert.ToDecimal(QItem.TotalPrice.ToString());
							mprice=Decimal.Round(dc1,2);
							body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left' colspan='2' bgColor='buttonface' >"+list[i].ToString()+"</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+QItem.Quantity.ToString()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+net+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+tot+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Series.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Bore.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Rod.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.RodEnd.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Cushion.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							if(QItem.CushionPos.ToString().Trim().Length >2)
							{
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.CushionPos.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							}
							if(QItem.ApplicationOpt_Id !="")
							{
								applist =db.SelectApplicationopts(QItem.Quotation_No.Trim(),QItem.PartNo.Trim());
								for(int h=0;h<applist.Count;h++)
								{
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+ applist[h].ToString()+" </TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
									
								}
							}
							string qtybrks="";
							ArrayList qtylist=new ArrayList();
							qtylist =db.SelectQtyBreaks(QItem.PartNo.ToString(),qno.Trim());
							if(qtylist.Count >0)
							{
								string h1="<TABLE id='Table10' style='Z-INDEX: 109; LEFT: 0; WIDTH: 100%; TOP: 0; FONT-SIZE: 8pt; FONT-FAMILY: Arial'cellSpacing='0' cellPadding='0' border='1' borderColor='#dcdcdc'>"
									+"<TR><TD noWrap align='center' colSpan='3' bgColor='buttonface'>Quantity Breaks</TD></TR>"
									+"<TR><TD style='WIDTH: 30%;' noWrap align='center'>Qty</TD><TD style='WIDTH: 30%;' noWrap align='center'>Unit Price</TD><TD style='WIDTH: 40%;' noWrap align='center'>Total Price</TD></TR>";
								string b1="";
								for(int p=0;p<qtylist.Count;p++)
								{
									Opts op =new Opts();
									op =(Opts)qtylist[p];
									decimal qd1,qd2,qd3=0.00m;
									qd1=Convert.ToDecimal(op.Code.Trim());
									qd2=Convert.ToDecimal(op.Free.Trim());
									qd3=qd1 * qd2;
									b1 +="<TR align='center'><TD  style='FONT-SIZE: 8pt; FONT-FAMILY: Arial' noWrap>"+op.Code.Trim()+"</TD><TD  noWrap>"+String.Format("{0:$###,###.00}",qd2)+"</TD><TD noWrap>"+String.Format("{0:$###,###.00}",qd3)+"</TD></TR>";
								}
								b1 +="</TR></TABLE>";
								qtybrks = h1.ToString()+b1.ToString();
							}

							body +="<TR><TD noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Seal.ToString()+"</TD><TD  style='WIDTH: 100%;' noWrap colSpan='3' rowspan='6'>"+qtybrks.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Port.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.PortPos.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Mount.ToString()+"</TD></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Stroke.ToString()+"</TD></TD></TR>";
						
							ArrayList poplist=new ArrayList();
							if(QItem.PopularOpt_Id !="")
							{
								poplist =db.SelectPopularoptions(QItem.Quotation_No.Trim(),QItem.PartNo.Trim());
							
								for(int m=0;m<poplist.Count;m++)
								{
									string ppp="";
									if(poplist[m].ToString().Substring(0,2) =="(T")
									{
										ppp=poplist[m].ToString().Replace(",","<br>");
									}
									else
									{
										ppp=poplist[m].ToString();
									}
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+ ppp.ToString()+" </TD></TR>";
								}
							}
							if(QItem.Special_ID !="")
							{
								temp =db.SelectSpecial(QItem.PartNo.ToString(),qno.Trim());
								for(int j=0;j<temp.Count;j++)
								{
									string sp="";
									sp=temp[j].ToString();
									string p= sp.Replace(";","<br>");
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left' ></TD><TD noWrap align='left' style='WIDTH: 318px'>"+ p.ToString()+" </TD></TR>";
								
								}
							}
							totals =totals + mprice;
						}
						}
					if((list.Count ==3 && acclist.Count>0) ||(list.Count == 3 && Replist.Count>0))
					{
						page4.Visible=true;
						ptotal1.Visible=false;
						ptotal2.Visible=false;
						ptotal3.Visible=false;
						acclist =new ArrayList();
						acclist =db.SelectAccessory(qno.ToString().Trim());
						if(acclist.Count >0)
						{
							for(int s =0;s<acclist.Count;s++)
							{
								Others oth =new Others();
								oth =(Others)acclist[s];
								decimal dc2=Convert.ToDecimal(oth.UnitP.ToString());
								string listprice=String.Format("{0:$###,###.00}",dc2);
								decimal dc3=Convert.ToDecimal(oth.Price.ToString());
								string totalprice=String.Format("{0:$###,###.00}",dc3);
								body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left'  bgColor='buttonface' >"+oth.Code.ToString().Trim()+"</TD><TD bgColor='buttonface' noWrap align='left'>"+oth.Desc.ToString().Trim()+"</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+oth.Qty.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+listprice.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+totalprice.ToString().Trim()+"</TD></TR>";
								totals =totals + dc3;
							}
					
						}
						Replist =new ArrayList();
						Replist =db.SelectRepairkit(qno.ToString().Trim());
						if(Replist.Count >0)
						{
		
							for(int s =0;s<Replist.Count;s++)
							{
								Others oth =new Others();
								oth =(Others)Replist[s];
								decimal dc2=Convert.ToDecimal(oth.UnitP.ToString());
								string listprice=String.Format("{0:$###,###.00}",dc2);
								decimal dc3=Convert.ToDecimal(oth.Price.ToString());
								string totalprice=String.Format("{0:$###,###.00}",dc3);
								body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left'  bgColor='buttonface' >"+oth.Code.ToString().Trim()+"</TD><TD bgColor='buttonface' noWrap align='left'>"+oth.Desc.ToString().Trim()+"("+oth.Desc1.ToString().Trim()+")</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+oth.Qty.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+listprice.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+totalprice.ToString().Trim()+"</TD></TR>";
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+oth.Desc2.ToString().Trim()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+oth.Desc3.ToString().Trim()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
								totals =totals + dc3;
							}
					
						}
					}
						lblinfo4.Text=head + body + end;
						lbltotalprice4.Text = String.Format("{0:$###,###.00}",totals);
					body="";
					if(list.Count > 4)
					{
						page5.Visible=true;
						ptotal1.Visible=false;
						ptotal2.Visible=false;
						ptotal3.Visible=false;
						ptotal4.Visible=false;
						int lcount=0;
						lcount =5;
						for(int i=4;i<lcount;i++)
						{
							QItem= db.SelectAllItems(list[i].ToString(),qno.Trim());
							ArrayList applist =new ArrayList();
							ArrayList temp =new ArrayList();
							string net ="";
							if(QItem.UnitPrice.ToString().Trim() =="0.00")
							{
								net =" TBA";
							}
							else
							{
								net =String.Format("{0:$###,###.00}",Convert.ToDecimal(QItem.UnitPrice));
							}
							if(QItem.TotalPrice.ToString().Trim() =="0.00")
							{
								tot =" TBA";
							}
							else
							{
								tot = String.Format("{0:$###,###.00}",Convert.ToDecimal(QItem.TotalPrice));
							}
							dc1 =Convert.ToDecimal(QItem.TotalPrice.ToString());
							mprice=Decimal.Round(dc1,2);
							body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left' colspan='2' bgColor='buttonface' >"+list[i].ToString()+"</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+QItem.Quantity.ToString()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+net+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+tot+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Series.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Bore.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Rod.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.RodEnd.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Cushion.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							if(QItem.CushionPos.ToString().Trim().Length >2)
							{
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.CushionPos.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							}
							if(QItem.ApplicationOpt_Id !="")
							{
								applist =db.SelectApplicationopts(QItem.Quotation_No.Trim(),QItem.PartNo.Trim());
								for(int h=0;h<applist.Count;h++)
								{
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+ applist[h].ToString()+" </TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
									
								}
							}
							string qtybrks="";
							ArrayList qtylist=new ArrayList();
							qtylist =db.SelectQtyBreaks(QItem.PartNo.ToString(),qno.Trim());
							if(qtylist.Count >0)
							{
								string h1="<TABLE id='Table10' style='Z-INDEX: 109; LEFT: 0; WIDTH: 100%; TOP: 0; FONT-SIZE: 8pt; FONT-FAMILY: Arial'cellSpacing='0' cellPadding='0' border='1' borderColor='#dcdcdc'>"
									+"<TR><TD noWrap align='center' colSpan='3' bgColor='buttonface'>Quantity Breaks</TD></TR>"
									+"<TR><TD style='WIDTH: 30%;' noWrap align='center'>Qty</TD><TD style='WIDTH: 30%;' noWrap align='center'>Unit Price</TD><TD style='WIDTH: 40%;' noWrap align='center'>Total Price</TD></TR>";
								string b1="";
								for(int p=0;p<qtylist.Count;p++)
								{
									Opts op =new Opts();
									op =(Opts)qtylist[p];
									decimal qd1,qd2,qd3=0.00m;
									qd1=Convert.ToDecimal(op.Code.Trim());
									qd2=Convert.ToDecimal(op.Free.Trim());
									qd3=qd1 * qd2;
									b1 +="<TR align='center'><TD  style='FONT-SIZE: 8pt; FONT-FAMILY: Arial' noWrap>"+op.Code.Trim()+"</TD><TD  noWrap>"+String.Format("{0:$###,###.00}",qd2)+"</TD><TD noWrap>"+String.Format("{0:$###,###.00}",qd3)+"</TD></TR>";
								}
								b1 +="</TR></TABLE>";
								qtybrks = h1.ToString()+b1.ToString();
							}

							body +="<TR><TD noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Seal.ToString()+"</TD><TD  style='WIDTH: 100%;' noWrap colSpan='3' rowspan='6'>"+qtybrks.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Port.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.PortPos.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Mount.ToString()+"</TD></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Stroke.ToString()+"</TD></TD></TR>";
						
							ArrayList poplist=new ArrayList();
							if(QItem.PopularOpt_Id !="")
							{
								poplist =db.SelectPopularoptions(QItem.Quotation_No.Trim(),QItem.PartNo.Trim());
							
								for(int m=0;m<poplist.Count;m++)
								{
									string ppp="";
									if(poplist[m].ToString().Substring(0,2) =="(T")
									{
										ppp=poplist[m].ToString().Replace(",","<br>");
									}
									else
									{
										ppp=poplist[m].ToString();
									}
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+ ppp.ToString()+" </TD></TR>";
								}
							}
							if(QItem.Special_ID !="")
							{
								temp =db.SelectSpecial(QItem.PartNo.ToString(),qno.Trim());
								for(int j=0;j<temp.Count;j++)
								{
									string sp="";
									sp=temp[j].ToString();
									string p= sp.Replace(";","<br>");
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left' ></TD><TD noWrap align='left' style='WIDTH: 318px'>"+ p.ToString()+" </TD></TR>";
								
								}
							}
							totals =totals + mprice;
						}
					}
					if((list.Count == 4  && acclist.Count>0) || (list.Count == 4 && Replist.Count>0))
					{
						page5.Visible=true;
						ptotal1.Visible=false;
						ptotal2.Visible=false;
						ptotal3.Visible=false;
						ptotal4.Visible=false;
						acclist =new ArrayList();
						acclist =db.SelectAccessory(qno.ToString().Trim());
						if(acclist.Count >0)
						{
							for(int s =0;s<acclist.Count;s++)
							{
								Others oth =new Others();
								oth =(Others)acclist[s];
								decimal dc2=Convert.ToDecimal(oth.UnitP.ToString());
								string listprice=String.Format("{0:$###,###.00}",dc2);
								decimal dc3=Convert.ToDecimal(oth.Price.ToString());
								string totalprice=String.Format("{0:$###,###.00}",dc3);
								body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left'  bgColor='buttonface' >"+oth.Code.ToString().Trim()+"</TD><TD bgColor='buttonface' noWrap align='left'>"+oth.Desc.ToString().Trim()+"</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+oth.Qty.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+listprice.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+totalprice.ToString().Trim()+"</TD></TR>";
								totals =totals + dc3;
							}
					
						}
						Replist =new ArrayList();
						Replist =db.SelectRepairkit(qno.ToString().Trim());
						if(Replist.Count >0)
						{
		
							for(int s =0;s<Replist.Count;s++)
							{
								Others oth =new Others();
								oth =(Others)Replist[s];
								decimal dc2=Convert.ToDecimal(oth.UnitP.ToString());
								string listprice=String.Format("{0:$###,###.00}",dc2);
								decimal dc3=Convert.ToDecimal(oth.Price.ToString());
								string totalprice=String.Format("{0:$###,###.00}",dc3);
								body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left'  bgColor='buttonface' >"+oth.Code.ToString().Trim()+"</TD><TD bgColor='buttonface' noWrap align='left'>"+oth.Desc.ToString().Trim()+"("+oth.Desc1.ToString().Trim()+")</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+oth.Qty.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+listprice.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+totalprice.ToString().Trim()+"</TD></TR>";
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+oth.Desc2.ToString().Trim()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+oth.Desc3.ToString().Trim()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
								totals =totals + dc3;
							}
					
						}
					}
						lblinfo5.Text=head + body + end;
						lbltotalprice5.Text = String.Format("{0:$###,###.00}",totals);
					body="";
					if(list.Count > 5)
					{
						page6.Visible=true;
						ptotal1.Visible=false;
						ptotal2.Visible=false;
						ptotal3.Visible=false;
						ptotal4.Visible=false;
						ptotal5.Visible=false;
						int lcount=0;
						lcount =6;
						
						for(int i=5;i<lcount;i++)
						{
							QItem= db.SelectAllItems(list[i].ToString(),qno.Trim());
							ArrayList applist =new ArrayList();
							ArrayList temp =new ArrayList();
							string net ="";
							if(QItem.UnitPrice.ToString().Trim() =="0.00")
							{
								net =" TBA";
							}
							else
							{
								net =String.Format("{0:$###,###.00}",Convert.ToDecimal(QItem.UnitPrice));
							}
							if(QItem.TotalPrice.ToString().Trim() =="0.00")
							{
								tot =" TBA";
							}
							else
							{
								tot = String.Format("{0:$###,###.00}",Convert.ToDecimal(QItem.TotalPrice));
							}
							dc1 =Convert.ToDecimal(QItem.TotalPrice.ToString());
							mprice=Decimal.Round(dc1,2);
							body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left' colspan='2' bgColor='buttonface' >"+list[i].ToString()+"</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+QItem.Quantity.ToString()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+net+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+tot+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Series.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Bore.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Rod.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.RodEnd.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Cushion.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							if(QItem.CushionPos.ToString().Trim().Length >2)
							{
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.CushionPos.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							}
							if(QItem.ApplicationOpt_Id !="")
							{
								applist =db.SelectApplicationopts(QItem.Quotation_No.Trim(),QItem.PartNo.Trim());
								for(int h=0;h<applist.Count;h++)
								{
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+ applist[h].ToString()+" </TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
									
								}
							}
							string qtybrks="";
							ArrayList qtylist=new ArrayList();
							qtylist =db.SelectQtyBreaks(QItem.PartNo.ToString(),qno.Trim());
							if(qtylist.Count >0)
							{
								string h1="<TABLE id='Table10' style='Z-INDEX: 109; LEFT: 0; WIDTH: 100%; TOP: 0; FONT-SIZE: 8pt; FONT-FAMILY: Arial'cellSpacing='0' cellPadding='0' border='1' borderColor='#dcdcdc'>"
									+"<TR><TD noWrap align='center' colSpan='3' bgColor='buttonface'>Quantity Breaks</TD></TR>"
									+"<TR><TD style='WIDTH: 30%;' noWrap align='center'>Qty</TD><TD style='WIDTH: 30%;' noWrap align='center'>Unit Price</TD><TD style='WIDTH: 40%;' noWrap align='center'>Total Price</TD></TR>";
								string b1="";
								for(int p=0;p<qtylist.Count;p++)
								{
									Opts op =new Opts();
									op =(Opts)qtylist[p];
									decimal qd1,qd2,qd3=0.00m;
									qd1=Convert.ToDecimal(op.Code.Trim());
									qd2=Convert.ToDecimal(op.Free.Trim());
									qd3=qd1 * qd2;
									b1 +="<TR align='center'><TD  style='FONT-SIZE: 8pt; FONT-FAMILY: Arial' noWrap>"+op.Code.Trim()+"</TD><TD  noWrap>"+String.Format("{0:$###,###.00}",qd2)+"</TD><TD noWrap>"+String.Format("{0:$###,###.00}",qd3)+"</TD></TR>";
								}
								b1 +="</TR></TABLE>";
								qtybrks = h1.ToString()+b1.ToString();
							}

							body +="<TR><TD noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Seal.ToString()+"</TD><TD  style='WIDTH: 100%;' noWrap colSpan='3' rowspan='6'>"+qtybrks.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Port.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.PortPos.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Mount.ToString()+"</TD></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Stroke.ToString()+"</TD></TD></TR>";
						
							ArrayList poplist=new ArrayList();
							if(QItem.PopularOpt_Id !="")
							{
								poplist =db.SelectPopularoptions(QItem.Quotation_No.Trim(),QItem.PartNo.Trim());
							
								for(int m=0;m<poplist.Count;m++)
								{
									string ppp="";
									if(poplist[m].ToString().Substring(0,2) =="(T")
									{
										ppp=poplist[m].ToString().Replace(",","<br>");
									}
									else
									{
										ppp=poplist[m].ToString();
									}
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+ ppp.ToString()+" </TD></TR>";
								}
							}
							if(QItem.Special_ID !="")
							{
								temp =db.SelectSpecial(QItem.PartNo.ToString(),qno.Trim());
								for(int j=0;j<temp.Count;j++)
								{
									string sp="";
									sp=temp[j].ToString();
									string p= sp.Replace(";","<br>");
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left' ></TD><TD noWrap align='left' style='WIDTH: 318px'>"+ p.ToString()+" </TD></TR>";
								
								}
							}
							totals =totals + mprice;
						}
					}
					if((list.Count == 5 && acclist.Count>0) || (list.Count == 5 && Replist.Count>0))
					{
						page6.Visible=true;
						ptotal1.Visible=false;
						ptotal2.Visible=false;
						ptotal3.Visible=false;
						ptotal4.Visible=false;
						ptotal5.Visible=false;
						acclist =new ArrayList();
						acclist =db.SelectAccessory(qno.ToString().Trim());
						if(acclist.Count >0)
						{
							for(int s =0;s<acclist.Count;s++)
							{
								Others oth =new Others();
								oth =(Others)acclist[s];
								decimal dc2=Convert.ToDecimal(oth.UnitP.ToString());
								string listprice=String.Format("{0:$###,###.00}",dc2);
								decimal dc3=Convert.ToDecimal(oth.Price.ToString());
								string totalprice=String.Format("{0:$###,###.00}",dc3);
								body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left'  bgColor='buttonface' >"+oth.Code.ToString().Trim()+"</TD><TD bgColor='buttonface' noWrap align='left'>"+oth.Desc.ToString().Trim()+"</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+oth.Qty.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+listprice.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+totalprice.ToString().Trim()+"</TD></TR>";
								totals =totals + dc3;
							}
					
						}
						Replist =new ArrayList();
						Replist =db.SelectRepairkit(qno.ToString().Trim());
						if(Replist.Count >0)
						{
		
							for(int s =0;s<Replist.Count;s++)
							{
								Others oth =new Others();
								oth =(Others)Replist[s];
								decimal dc2=Convert.ToDecimal(oth.UnitP.ToString());
								string listprice=String.Format("{0:$###,###.00}",dc2);
								decimal dc3=Convert.ToDecimal(oth.Price.ToString());
								string totalprice=String.Format("{0:$###,###.00}",dc3);
								body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left'  bgColor='buttonface' >"+oth.Code.ToString().Trim()+"</TD><TD bgColor='buttonface' noWrap align='left'>"+oth.Desc.ToString().Trim()+"("+oth.Desc1.ToString().Trim()+")</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+oth.Qty.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+listprice.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+totalprice.ToString().Trim()+"</TD></TR>";
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+oth.Desc2.ToString().Trim()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+oth.Desc3.ToString().Trim()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
								totals =totals + dc3;
							}
					
						}
					}	
					lblinfo6.Text=head + body + end;
					lbltotalprice6.Text = String.Format("{0:$###,###.00}",totals);
					body="";
					if(list.Count > 6)
					{
						page7.Visible=true;
						ptotal1.Visible=false;
						ptotal2.Visible=false;
						ptotal3.Visible=false;
						ptotal4.Visible=false;
						ptotal5.Visible=false;
						ptotal6.Visible=false;
						int lcount=0;
						lcount =7;
						
						for(int i=6;i<lcount;i++)
						{
							QItem= db.SelectAllItems(list[i].ToString(),qno.Trim());
							ArrayList applist =new ArrayList();
							ArrayList temp =new ArrayList();
							string net ="";
							if(QItem.UnitPrice.ToString().Trim() =="0.00")
							{
								net =" TBA";
							}
							else
							{
								net =String.Format("{0:$###,###.00}",Convert.ToDecimal(QItem.UnitPrice));
							}
							if(QItem.TotalPrice.ToString().Trim() =="0.00")
							{
								tot =" TBA";
							}
							else
							{
								tot = String.Format("{0:$###,###.00}",Convert.ToDecimal(QItem.TotalPrice));
							}
							dc1 =Convert.ToDecimal(QItem.TotalPrice.ToString());
							mprice=Decimal.Round(dc1,2);
							body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left' colspan='2' bgColor='buttonface' >"+list[i].ToString()+"</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+QItem.Quantity.ToString()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+net+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+tot+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Series.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Bore.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Rod.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.RodEnd.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Cushion.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							if(QItem.CushionPos.ToString().Trim().Length >2)
							{
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.CushionPos.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							}
							if(QItem.ApplicationOpt_Id !="")
							{
								applist =db.SelectApplicationopts(QItem.Quotation_No.Trim(),QItem.PartNo.Trim());
								for(int h=0;h<applist.Count;h++)
								{
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+ applist[h].ToString()+" </TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
									
								}
							}
							string qtybrks="";
							ArrayList qtylist=new ArrayList();
							qtylist =db.SelectQtyBreaks(QItem.PartNo.ToString(),qno.Trim());
							if(qtylist.Count >0)
							{
								string h1="<TABLE id='Table10' style='Z-INDEX: 109; LEFT: 0; WIDTH: 100%; TOP: 0; FONT-SIZE: 8pt; FONT-FAMILY: Arial'cellSpacing='0' cellPadding='0' border='1' borderColor='#dcdcdc'>"
									+"<TR><TD noWrap align='center' colSpan='3' bgColor='buttonface'>Quantity Breaks</TD></TR>"
									+"<TR><TD style='WIDTH: 30%;' noWrap align='center'>Qty</TD><TD style='WIDTH: 30%;' noWrap align='center'>Unit Price</TD><TD style='WIDTH: 40%;' noWrap align='center'>Total Price</TD></TR>";
								string b1="";
								for(int p=0;p<qtylist.Count;p++)
								{
									Opts op =new Opts();
									op =(Opts)qtylist[p];
									decimal qd1,qd2,qd3=0.00m;
									qd1=Convert.ToDecimal(op.Code.Trim());
									qd2=Convert.ToDecimal(op.Free.Trim());
									qd3=qd1 * qd2;
									b1 +="<TR align='center'><TD  style='FONT-SIZE: 8pt; FONT-FAMILY: Arial' noWrap>"+op.Code.Trim()+"</TD><TD  noWrap>"+String.Format("{0:$###,###.00}",qd2)+"</TD><TD noWrap>"+String.Format("{0:$###,###.00}",qd3)+"</TD></TR>";
								}
								b1 +="</TR></TABLE>";
								qtybrks = h1.ToString()+b1.ToString();
							}

							body +="<TR><TD noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Seal.ToString()+"</TD><TD  style='WIDTH: 100%;' noWrap colSpan='3' rowspan='6'>"+qtybrks.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Port.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.PortPos.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Mount.ToString()+"</TD></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Stroke.ToString()+"</TD></TD></TR>";
						
							ArrayList poplist=new ArrayList();
							if(QItem.PopularOpt_Id !="")
							{
								poplist =db.SelectPopularoptions(QItem.Quotation_No.Trim(),QItem.PartNo.Trim());
							
								for(int m=0;m<poplist.Count;m++)
								{
									string ppp="";
									if(poplist[m].ToString().Substring(0,2) =="(T")
									{
										ppp=poplist[m].ToString().Replace(",","<br>");
									}
									else
									{
										ppp=poplist[m].ToString();
									}
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+ ppp.ToString()+" </TD></TR>";
								}
							}
							if(QItem.Special_ID !="")
							{
								temp =db.SelectSpecial(QItem.PartNo.ToString(),qno.Trim());
								for(int j=0;j<temp.Count;j++)
								{
									string sp="";
									sp=temp[j].ToString();
									string p= sp.Replace(";","<br>");
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left' ></TD><TD noWrap align='left' style='WIDTH: 318px'>"+ p.ToString()+" </TD></TR>";
								
								}
							}
							totals =totals + mprice;
						}
					}
					if((list.Count == 6 && acclist.Count>0) || (list.Count == 6 && Replist.Count>0))
					{
						page7.Visible=true;
						ptotal1.Visible=false;
						ptotal2.Visible=false;
						ptotal3.Visible=false;
						ptotal4.Visible=false;
						ptotal5.Visible=false;
						ptotal6.Visible=false;
						acclist =new ArrayList();
						acclist =db.SelectAccessory(qno.ToString().Trim());
						if(acclist.Count >0)
						{
							for(int s =0;s<acclist.Count;s++)
							{
								Others oth =new Others();
								oth =(Others)acclist[s];
								decimal dc2=Convert.ToDecimal(oth.UnitP.ToString());
								string listprice=String.Format("{0:$###,###.00}",dc2);
								decimal dc3=Convert.ToDecimal(oth.Price.ToString());
								string totalprice=String.Format("{0:$###,###.00}",dc3);
								body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left'  bgColor='buttonface' >"+oth.Code.ToString().Trim()+"</TD><TD bgColor='buttonface' noWrap align='left'>"+oth.Desc.ToString().Trim()+"</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+oth.Qty.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+listprice.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+totalprice.ToString().Trim()+"</TD></TR>";
								totals =totals + dc3;
							}
					
						}
						Replist =new ArrayList();
						Replist =db.SelectRepairkit(qno.ToString().Trim());
						if(Replist.Count >0)
						{
		
							for(int s =0;s<Replist.Count;s++)
							{
								Others oth =new Others();
								oth =(Others)Replist[s];
								decimal dc2=Convert.ToDecimal(oth.UnitP.ToString());
								string listprice=String.Format("{0:$###,###.00}",dc2);
								decimal dc3=Convert.ToDecimal(oth.Price.ToString());
								string totalprice=String.Format("{0:$###,###.00}",dc3);
								body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left'  bgColor='buttonface' >"+oth.Code.ToString().Trim()+"</TD><TD bgColor='buttonface' noWrap align='left'>"+oth.Desc.ToString().Trim()+"("+oth.Desc1.ToString().Trim()+")</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+oth.Qty.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+listprice.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+totalprice.ToString().Trim()+"</TD></TR>";
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+oth.Desc2.ToString().Trim()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+oth.Desc3.ToString().Trim()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
								totals =totals + dc3;
							}
					
						}
					}
						lblinfo7.Text=head + body + end;
						lbltotalprice7.Text = String.Format("{0:$###,###.00}",totals);
					body="";
					if(list.Count > 7)
					{
						page8.Visible=true;
						ptotal1.Visible=false;
						ptotal2.Visible=false;
						ptotal3.Visible=false;
						ptotal4.Visible=false;
						ptotal5.Visible=false;
						ptotal6.Visible=false;
						ptotal7.Visible=false;
						int lcount=0;
						lcount =8;
						
						for(int i=7;i<lcount;i++)
						{
							QItem= db.SelectAllItems(list[i].ToString(),qno.Trim());
							ArrayList applist =new ArrayList();
							ArrayList temp =new ArrayList();
							string net ="";
							if(QItem.UnitPrice.ToString().Trim() =="0.00")
							{
								net =" TBA";
							}
							else
							{
								net =String.Format("{0:$###,###.00}",Convert.ToDecimal(QItem.UnitPrice));
							}
							if(QItem.TotalPrice.ToString().Trim() =="0.00")
							{
								tot =" TBA";
							}
							else
							{
								tot = String.Format("{0:$###,###.00}",Convert.ToDecimal(QItem.TotalPrice));
							}
							dc1 =Convert.ToDecimal(QItem.TotalPrice.ToString());
							mprice=Decimal.Round(dc1,2);
							body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left' colspan='2' bgColor='buttonface' >"+list[i].ToString()+"</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+QItem.Quantity.ToString()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+net+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+tot+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Series.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Bore.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Rod.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.RodEnd.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Cushion.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							if(QItem.CushionPos.ToString().Trim().Length >2)
							{
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.CushionPos.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							}
							if(QItem.ApplicationOpt_Id !="")
							{
								applist =db.SelectApplicationopts(QItem.Quotation_No.Trim(),QItem.PartNo.Trim());
								for(int h=0;h<applist.Count;h++)
								{
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+ applist[h].ToString()+" </TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
									
								}
							}
							string qtybrks="";
							ArrayList qtylist=new ArrayList();
							qtylist =db.SelectQtyBreaks(QItem.PartNo.ToString(),qno.Trim());
							if(qtylist.Count >0)
							{
								string h1="<TABLE id='Table10' style='Z-INDEX: 109; LEFT: 0; WIDTH: 100%; TOP: 0; FONT-SIZE: 8pt; FONT-FAMILY: Arial'cellSpacing='0' cellPadding='0' border='1' borderColor='#dcdcdc'>"
									+"<TR><TD noWrap align='center' colSpan='3' bgColor='buttonface'>Quantity Breaks</TD></TR>"
									+"<TR><TD style='WIDTH: 30%;' noWrap align='center'>Qty</TD><TD style='WIDTH: 30%;' noWrap align='center'>Unit Price</TD><TD style='WIDTH: 40%;' noWrap align='center'>Total Price</TD></TR>";
								string b1="";
								for(int p=0;p<qtylist.Count;p++)
								{
									Opts op =new Opts();
									op =(Opts)qtylist[p];
									decimal qd1,qd2,qd3=0.00m;
									qd1=Convert.ToDecimal(op.Code.Trim());
									qd2=Convert.ToDecimal(op.Free.Trim());
									qd3=qd1 * qd2;
									b1 +="<TR align='center'><TD  style='FONT-SIZE: 8pt; FONT-FAMILY: Arial' noWrap>"+op.Code.Trim()+"</TD><TD  noWrap>"+String.Format("{0:$###,###.00}",qd2)+"</TD><TD noWrap>"+String.Format("{0:$###,###.00}",qd3)+"</TD></TR>";
								}
								b1 +="</TR></TABLE>";
								qtybrks = h1.ToString()+b1.ToString();
							}

							body +="<TR><TD noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Seal.ToString()+"</TD><TD  style='WIDTH: 100%;' noWrap colSpan='3' rowspan='6'>"+qtybrks.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Port.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.PortPos.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Mount.ToString()+"</TD></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Stroke.ToString()+"</TD></TD></TR>";
						
							ArrayList poplist=new ArrayList();
							if(QItem.PopularOpt_Id !="")
							{
								poplist =db.SelectPopularoptions(QItem.Quotation_No.Trim(),QItem.PartNo.Trim());
							
								for(int m=0;m<poplist.Count;m++)
								{
									string ppp="";
									if(poplist[m].ToString().Substring(0,2) =="(T")
									{
										ppp=poplist[m].ToString().Replace(",","<br>");
									}
									else
									{
										ppp=poplist[m].ToString();
									}
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+ ppp.ToString()+" </TD></TR>";
								}
							}
							if(QItem.Special_ID !="")
							{
								temp =db.SelectSpecial(QItem.PartNo.ToString(),qno.Trim());
								for(int j=0;j<temp.Count;j++)
								{
									string sp="";
									sp=temp[j].ToString();
									string p= sp.Replace(";","<br>");
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left' ></TD><TD noWrap align='left' style='WIDTH: 318px'>"+ p.ToString()+" </TD></TR>";
								
								}
							}
							totals =totals + mprice;
						}
					}
					if((list.Count == 7 && acclist.Count>0) || (list.Count == 7 && Replist.Count>0))
					{
						page8.Visible=true;
						ptotal1.Visible=false;
						ptotal2.Visible=false;
						ptotal3.Visible=false;
						ptotal4.Visible=false;
						ptotal5.Visible=false;
						ptotal6.Visible=false;
						ptotal7.Visible=false;
						acclist =new ArrayList();
						acclist =db.SelectAccessory(qno.ToString().Trim());
						if(acclist.Count >0)
						{
							for(int s =0;s<acclist.Count;s++)
							{
								Others oth =new Others();
								oth =(Others)acclist[s];
								decimal dc2=Convert.ToDecimal(oth.UnitP.ToString());
								string listprice=String.Format("{0:$###,###.00}",dc2);
								decimal dc3=Convert.ToDecimal(oth.Price.ToString());
								string totalprice=String.Format("{0:$###,###.00}",dc3);
								body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left'  bgColor='buttonface' >"+oth.Code.ToString().Trim()+"</TD><TD bgColor='buttonface' noWrap align='left'>"+oth.Desc.ToString().Trim()+"</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+oth.Qty.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+listprice.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+totalprice.ToString().Trim()+"</TD></TR>";
								totals =totals + dc3;
							}
					
						}
						Replist =new ArrayList();
						Replist =db.SelectRepairkit(qno.ToString().Trim());
						if(Replist.Count >0)
						{
		
							for(int s =0;s<Replist.Count;s++)
							{
								Others oth =new Others();
								oth =(Others)Replist[s];
								decimal dc2=Convert.ToDecimal(oth.UnitP.ToString());
								string listprice=String.Format("{0:$###,###.00}",dc2);
								decimal dc3=Convert.ToDecimal(oth.Price.ToString());
								string totalprice=String.Format("{0:$###,###.00}",dc3);
								body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left'  bgColor='buttonface' >"+oth.Code.ToString().Trim()+"</TD><TD bgColor='buttonface' noWrap align='left'>"+oth.Desc.ToString().Trim()+"("+oth.Desc1.ToString().Trim()+")</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+oth.Qty.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+listprice.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+totalprice.ToString().Trim()+"</TD></TR>";
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+oth.Desc2.ToString().Trim()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+oth.Desc3.ToString().Trim()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
								totals =totals + dc3;
							}
					
						}
					}
					lblinfo8.Text=head + body + end;
					lbltotalprice8.Text = String.Format("{0:$###,###.00}",totals);
					body="";
					if(list.Count > 8)
					{
						page9.Visible=true;
						ptotal1.Visible=false;
						ptotal2.Visible=false;
						ptotal3.Visible=false;
						ptotal4.Visible=false;
						ptotal5.Visible=false;
						ptotal6.Visible=false;
						ptotal7.Visible=false;
						ptotal8.Visible=false;
						int lcount=0;
						lcount =9;
						
						for(int i=8;i<lcount;i++)
						{
							QItem= db.SelectAllItems(list[i].ToString(),qno.Trim());
							ArrayList applist =new ArrayList();
							ArrayList temp =new ArrayList();
							string net ="";
							if(QItem.UnitPrice.ToString().Trim() =="0.00")
							{
								net =" TBA";
							}
							else
							{
								net =String.Format("{0:$###,###.00}",Convert.ToDecimal(QItem.UnitPrice));
							}
							if(QItem.TotalPrice.ToString().Trim() =="0.00")
							{
								tot =" TBA";
							}
							else
							{
								tot = String.Format("{0:$###,###.00}",Convert.ToDecimal(QItem.TotalPrice));
							}
							dc1 =Convert.ToDecimal(QItem.TotalPrice.ToString());
							mprice=Decimal.Round(dc1,2);
							body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left' colspan='2' bgColor='buttonface' >"+list[i].ToString()+"</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+QItem.Quantity.ToString()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+net+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+tot+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Series.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Bore.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Rod.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.RodEnd.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Cushion.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							if(QItem.CushionPos.ToString().Trim().Length >2)
							{
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.CushionPos.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							}
							if(QItem.ApplicationOpt_Id !="")
							{
								applist =db.SelectApplicationopts(QItem.Quotation_No.Trim(),QItem.PartNo.Trim());
								for(int h=0;h<applist.Count;h++)
								{
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+ applist[h].ToString()+" </TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
									
								}
							}
							string qtybrks="";
							ArrayList qtylist=new ArrayList();
							qtylist =db.SelectQtyBreaks(QItem.PartNo.ToString(),qno.Trim());
							if(qtylist.Count >0)
							{
								string h1="<TABLE id='Table10' style='Z-INDEX: 109; LEFT: 0; WIDTH: 100%; TOP: 0; FONT-SIZE: 8pt; FONT-FAMILY: Arial'cellSpacing='0' cellPadding='0' border='1' borderColor='#dcdcdc'>"
									+"<TR><TD noWrap align='center' colSpan='3' bgColor='buttonface'>Quantity Breaks</TD></TR>"
									+"<TR><TD style='WIDTH: 30%;' noWrap align='center'>Qty</TD><TD style='WIDTH: 30%;' noWrap align='center'>Unit Price</TD><TD style='WIDTH: 40%;' noWrap align='center'>Total Price</TD></TR>";
								string b1="";
								for(int p=0;p<qtylist.Count;p++)
								{
									Opts op =new Opts();
									op =(Opts)qtylist[p];
									decimal qd1,qd2,qd3=0.00m;
									qd1=Convert.ToDecimal(op.Code.Trim());
									qd2=Convert.ToDecimal(op.Free.Trim());
									qd3=qd1 * qd2;
									b1 +="<TR align='center'><TD  style='FONT-SIZE: 8pt; FONT-FAMILY: Arial' noWrap>"+op.Code.Trim()+"</TD><TD  noWrap>"+String.Format("{0:$###,###.00}",qd2)+"</TD><TD noWrap>"+String.Format("{0:$###,###.00}",qd3)+"</TD></TR>";
								}
								b1 +="</TR></TABLE>";
								qtybrks = h1.ToString()+b1.ToString();
							}

							body +="<TR><TD noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Seal.ToString()+"</TD><TD  style='WIDTH: 100%;' noWrap colSpan='3' rowspan='6'>"+qtybrks.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Port.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.PortPos.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Mount.ToString()+"</TD></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Stroke.ToString()+"</TD></TD></TR>";
						
							ArrayList poplist=new ArrayList();
							if(QItem.PopularOpt_Id !="")
							{
								poplist =db.SelectPopularoptions(QItem.Quotation_No.Trim(),QItem.PartNo.Trim());
							
								for(int m=0;m<poplist.Count;m++)
								{
									string ppp="";
									if(poplist[m].ToString().Substring(0,2) =="(T")
									{
										ppp=poplist[m].ToString().Replace(",","<br>");
									}
									else
									{
										ppp=poplist[m].ToString();
									}
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+ ppp.ToString()+" </TD></TR>";
								}
							}
							if(QItem.Special_ID !="")
							{
								temp =db.SelectSpecial(QItem.PartNo.ToString(),qno.Trim());
								for(int j=0;j<temp.Count;j++)
								{
									string sp="";
									sp=temp[j].ToString();
									string p= sp.Replace(";","<br>");
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left' ></TD><TD noWrap align='left' style='WIDTH: 318px'>"+ p.ToString()+" </TD></TR>";
								
								}
							}
							totals =totals + mprice;
						}
					}
					if((list.Count == 8 && acclist.Count>0) || (list.Count == 8 && Replist.Count>0))
					{
						page9.Visible=true;
						ptotal1.Visible=false;
						ptotal2.Visible=false;
						ptotal3.Visible=false;
						ptotal4.Visible=false;
						ptotal5.Visible=false;
						ptotal6.Visible=false;
						ptotal7.Visible=false;
						ptotal8.Visible=false;
						acclist =new ArrayList();
						acclist =db.SelectAccessory(qno.ToString().Trim());
						if(acclist.Count >0)
						{
							for(int s =0;s<acclist.Count;s++)
							{
								Others oth =new Others();
								oth =(Others)acclist[s];
								decimal dc2=Convert.ToDecimal(oth.UnitP.ToString());
								string listprice=String.Format("{0:$###,###.00}",dc2);
								decimal dc3=Convert.ToDecimal(oth.Price.ToString());
								string totalprice=String.Format("{0:$###,###.00}",dc3);
								body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left'  bgColor='buttonface' >"+oth.Code.ToString().Trim()+"</TD><TD bgColor='buttonface' noWrap align='left'>"+oth.Desc.ToString().Trim()+"</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+oth.Qty.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+listprice.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+totalprice.ToString().Trim()+"</TD></TR>";
								totals =totals + dc3;
							}
					
						}
						Replist =new ArrayList();
						Replist =db.SelectRepairkit(qno.ToString().Trim());
						if(Replist.Count >0)
						{
		
							for(int s =0;s<Replist.Count;s++)
							{
								Others oth =new Others();
								oth =(Others)Replist[s];
								decimal dc2=Convert.ToDecimal(oth.UnitP.ToString());
								string listprice=String.Format("{0:$###,###.00}",dc2);
								decimal dc3=Convert.ToDecimal(oth.Price.ToString());
								string totalprice=String.Format("{0:$###,###.00}",dc3);
								body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left'  bgColor='buttonface' >"+oth.Code.ToString().Trim()+"</TD><TD bgColor='buttonface' noWrap align='left'>"+oth.Desc.ToString().Trim()+"("+oth.Desc1.ToString().Trim()+")</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+oth.Qty.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+listprice.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+totalprice.ToString().Trim()+"</TD></TR>";
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+oth.Desc2.ToString().Trim()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+oth.Desc3.ToString().Trim()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
								totals =totals + dc3;
							}
					
						}
					}
					lblinfo9.Text=head + body + end;
					lbltotalprice9.Text = String.Format("{0:$###,###.00}",totals);
					body="";
					if(list.Count > 9)
					{
						page10.Visible=true;
						ptotal1.Visible=false;
						ptotal2.Visible=false;
						ptotal3.Visible=false;
						ptotal4.Visible=false;
						ptotal5.Visible=false;
						ptotal6.Visible=false;
						ptotal7.Visible=false;
						ptotal8.Visible=false;
						ptotal9.Visible=false;
						int lcount=0;
						lcount =10;
						
						for(int i=9;i<lcount;i++)
						{
							QItem= db.SelectAllItems(list[i].ToString(),qno.Trim());
							ArrayList applist =new ArrayList();
							ArrayList temp =new ArrayList();
							string net ="";
							if(QItem.UnitPrice.ToString().Trim() =="0.00")
							{
								net =" TBA";
							}
							else
							{
								net =String.Format("{0:$###,###.00}",Convert.ToDecimal(QItem.UnitPrice));
							}
							if(QItem.TotalPrice.ToString().Trim() =="0.00")
							{
								tot =" TBA";
							}
							else
							{
								tot = String.Format("{0:$###,###.00}",Convert.ToDecimal(QItem.TotalPrice));
							}
							dc1 =Convert.ToDecimal(QItem.TotalPrice.ToString());
							mprice=Decimal.Round(dc1,2);
							body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left' colspan='2' bgColor='buttonface' >"+list[i].ToString()+"</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+QItem.Quantity.ToString()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+net+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+tot+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Series.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Bore.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Rod.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.RodEnd.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Cushion.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							if(QItem.CushionPos.ToString().Trim().Length >2)
							{
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.CushionPos.ToString()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
							}
							if(QItem.ApplicationOpt_Id !="")
							{
								applist =db.SelectApplicationopts(QItem.Quotation_No.Trim(),QItem.PartNo.Trim());
								for(int h=0;h<applist.Count;h++)
								{
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+ applist[h].ToString()+" </TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
									
								}
							}
							string qtybrks="";
							ArrayList qtylist=new ArrayList();
							qtylist =db.SelectQtyBreaks(QItem.PartNo.ToString(),qno.Trim());
							if(qtylist.Count >0)
							{
								string h1="<TABLE id='Table10' style='Z-INDEX: 109; LEFT: 0; WIDTH: 100%; TOP: 0; FONT-SIZE: 8pt; FONT-FAMILY: Arial'cellSpacing='0' cellPadding='0' border='1' borderColor='#dcdcdc'>"
									+"<TR><TD noWrap align='center' colSpan='3' bgColor='buttonface'>Quantity Breaks</TD></TR>"
									+"<TR><TD style='WIDTH: 30%;' noWrap align='center'>Qty</TD><TD style='WIDTH: 30%;' noWrap align='center'>Unit Price</TD><TD style='WIDTH: 40%;' noWrap align='center'>Total Price</TD></TR>";
								string b1="";
								for(int p=0;p<qtylist.Count;p++)
								{
									Opts op =new Opts();
									op =(Opts)qtylist[p];
									decimal qd1,qd2,qd3=0.00m;
									qd1=Convert.ToDecimal(op.Code.Trim());
									qd2=Convert.ToDecimal(op.Free.Trim());
									qd3=qd1 * qd2;
									b1 +="<TR align='center'><TD  style='FONT-SIZE: 8pt; FONT-FAMILY: Arial' noWrap>"+op.Code.Trim()+"</TD><TD  noWrap>"+String.Format("{0:$###,###.00}",qd2)+"</TD><TD noWrap>"+String.Format("{0:$###,###.00}",qd3)+"</TD></TR>";
								}
								b1 +="</TR></TABLE>";
								qtybrks = h1.ToString()+b1.ToString();
							}

							body +="<TR><TD noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Seal.ToString()+"</TD><TD  style='WIDTH: 100%;' noWrap colSpan='3' rowspan='6'>"+qtybrks.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Port.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.PortPos.ToString()+"</TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Mount.ToString()+"</TD></TD></TR>";
							body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+QItem.Stroke.ToString()+"</TD></TD></TR>";
						
							ArrayList poplist=new ArrayList();
							if(QItem.PopularOpt_Id !="")
							{
								poplist =db.SelectPopularoptions(QItem.Quotation_No.Trim(),QItem.PartNo.Trim());
							
								for(int m=0;m<poplist.Count;m++)
								{
									string ppp="";
									if(poplist[m].ToString().Substring(0,2) =="(T")
									{
										ppp=poplist[m].ToString().Replace(",","<br>");
									}
									else
									{
										ppp=poplist[m].ToString();
									}
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+ ppp.ToString()+" </TD></TR>";
								}
							}
							if(QItem.Special_ID !="")
							{
								temp =db.SelectSpecial(QItem.PartNo.ToString(),qno.Trim());
								for(int j=0;j<temp.Count;j++)
								{
									string sp="";
									sp=temp[j].ToString();
									string p= sp.Replace(";","<br>");
									body +="<TR><TD style='WIDTH: 135px' noWrap align='left' ></TD><TD noWrap align='left' style='WIDTH: 318px'>"+ p.ToString()+" </TD></TR>";
								
								}
							}
							totals =totals + mprice;
						}
					}
					if((list.Count == 9 && acclist.Count>0) || (list.Count == 9 && Replist.Count>0))
					{
						page10.Visible=true;
						ptotal1.Visible=false;
						ptotal2.Visible=false;
						ptotal3.Visible=false;
						ptotal4.Visible=false;
						ptotal5.Visible=false;
						ptotal6.Visible=false;
						ptotal7.Visible=false;
						ptotal8.Visible=false;
						ptotal9.Visible=false;
						acclist =new ArrayList();
						acclist =db.SelectAccessory(qno.ToString().Trim());
						if(acclist.Count >0)
						{
							for(int s =0;s<acclist.Count;s++)
							{
								Others oth =new Others();
								oth =(Others)acclist[s];
								decimal dc2=Convert.ToDecimal(oth.UnitP.ToString());
								string listprice=String.Format("{0:$###,###.00}",dc2);
								decimal dc3=Convert.ToDecimal(oth.Price.ToString());
								string totalprice=String.Format("{0:$###,###.00}",dc3);
								body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left'  bgColor='buttonface' >"+oth.Code.ToString().Trim()+"</TD><TD bgColor='buttonface' noWrap align='left'>"+oth.Desc.ToString().Trim()+"</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+oth.Qty.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+listprice.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+totalprice.ToString().Trim()+"</TD></TR>";
								totals =totals + dc3;
							}
					
						}
						Replist =new ArrayList();
						Replist =db.SelectRepairkit(qno.ToString().Trim());
						if(Replist.Count >0)
						{
		
							for(int s =0;s<Replist.Count;s++)
							{
								Others oth =new Others();
								oth =(Others)Replist[s];
								decimal dc2=Convert.ToDecimal(oth.UnitP.ToString());
								string listprice=String.Format("{0:$###,###.00}",dc2);
								decimal dc3=Convert.ToDecimal(oth.Price.ToString());
								string totalprice=String.Format("{0:$###,###.00}",dc3);
								body  +="<TR><TD style='WIDTH: 135px; FONT-WEIGHT: bold;' noWrap align='left'  bgColor='buttonface' >"+oth.Code.ToString().Trim()+"</TD><TD bgColor='buttonface' noWrap align='left'>"+oth.Desc.ToString().Trim()+"("+oth.Desc1.ToString().Trim()+")</TD><TD  style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+oth.Qty.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+listprice.ToString().Trim()+"</TD><TD style='FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: Times New Roman' bgColor='buttonface' noWrap align='center'>"+totalprice.ToString().Trim()+"</TD></TR>";
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+oth.Desc2.ToString().Trim()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
								body +="<TR><TD style='WIDTH: 135px' noWrap align='left'></TD><TD  noWrap align='left'>"+oth.Desc3.ToString().Trim()+"</TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD><TD noWrap align='left'></TD></TR>";
								totals =totals + dc3;
							}
					
						}
					}
					lblinfo10.Text=head + body + end;
					lbltotalprice10.Text = String.Format("{0:$###,###.00}",totals);
				}
		
				public void ShowComments(string qno)
				{
					string head= "<TABLE id='able1' style='Z-INDEX: 101; LEFT: 0px; WIDTH: 100%; TOP: 0px; FONT-SIZE: 6pt; FONT-FAMILY: Arial;' cellSpacing='1' cellPadding='1' width='100%' border='0'><TR style='FONT-WEIGHT: bold; FONT-SIZE: 6pt; FONT-FAMILY: Times New Roman; TEXT-DECORATION: underline' bgColor='#c0c0c0' borderColor='black'><TD style=' FONT-WEIGHT: bold;' noWrap align='center'>Comments</TD></TR>";
					string end="</TR></TABLE>";	
					string body="";
					ArrayList list=new ArrayList();
					list=db.Selectcomments(qno.Trim());
					for(int p=0;p<list.Count;p++)
					{
						body +="<TR><TD  noWrap align='left' style='WIDTH: 600px ;HEIGHT :10px ;FONT-SIZE: 6pt; FONT-FAMILY: Arial;'> ->  "+ list[p].ToString()+"</TD></TR>";
					
					}
					LblComments.Text=head + body + end;
					lblcomment2.Text=head + body + end;
					lblcomment3.Text=head + body + end;
					lblcomment4.Text=head + body + end;
					lblcomment5.Text=head + body + end;
					lblcomment6.Text=head + body + end;
					lblcomment7.Text=head + body + end;
					lblcomments8.Text=head + body + end;
					lblcomment9.Text=head + body + end;
					lblcomment10.Text=head + body + end;
				}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
