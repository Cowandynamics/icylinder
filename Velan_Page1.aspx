<%@ Page language="c#" Codebehind="Velan_Page1.aspx.cs" AutoEventWireup="True" Inherits="iCylinderV1.Velan_Page1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Velan_Page1</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
  </HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 1000px; HEIGHT: 487px" cellSpacing="0" cellPadding="0"
				width="1000" bgColor="lightgrey" border="0">
				<TR>
					<TD width="50" height="20"></TD>
					<TD style="WIDT: 484px" align="center" colSpan="3" height="20"><asp:label id="Label1" runat="server" Font-Size="Small" Font-Underline="True" BorderColor="Transparent"
							BackColor="Transparent">Series-A Velan Cylinder</asp:label></TD>
					<TD width="50" height="20"></TD>
				</TR>
				<TR>
					<TD width="50"></TD>
					<TD align="center" bgColor="#dcdcdc"><asp:panel id="Panel3" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
      <TABLE id=Table11 borderColor=silver cellSpacing=0 cellPadding=0 width=299 
      border=0>
        <TR>
          <TD align=center>
<asp:label id=Label2 runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller" Font-Bold="True" ForeColor="Maroon">Series A</asp:label></TD></TR>
        <TR>
          <TD align=center>
<asp:image id=Image1 runat="server" Width="100px" Height="70px" ImageUrl="images\A.jpg"></asp:image></TD></TR>
        <TR>
          <TD align=center>
<asp:radiobuttonlist id=RBLDoubleOrSingle runat="server" BorderColor="#404040" Font-Size="Smaller" Width="200px" Height="8px" AutoPostBack="True" RepeatLayout="Flow" RepeatDirection="Horizontal" onselectedindexchanged="RBLDoubleOrSingle_SelectedIndexChanged">
											<asp:ListItem Value="No" Selected="True">Single Ended</asp:ListItem>
											<asp:ListItem Value="Yes">Double Ended</asp:ListItem>
										</asp:radiobuttonlist></TD></TR></TABLE>
						</asp:panel><asp:panel id="Panel1" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
      <TABLE id=Table3 borderColor=silver cellSpacing=0 cellPadding=0 width=299 
      border=0>
        <TR>
          <TD align=center>
<asp:label id=Label4 runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller" Font-Bold="True" ForeColor="Maroon">Bore Size</asp:label></TD></TR>
        <TR>
          <TD align=center>
<asp:radiobuttonlist id=RBLBore runat="server" Font-Size="Smaller" BorderStyle="None" AutoPostBack="True" RepeatDirection="Horizontal" RepeatColumns="6" onselectedindexchanged="RBLBore_SelectedIndexChanged"></asp:radiobuttonlist></TD></TR></TABLE>
						</asp:panel><asp:panel id="Panel2" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
      <TABLE id=Table5 style="HEIGHT: 8px" borderColor=silver cellSpacing=0 
      cellPadding=0 width=299 border=0>
        <TR>
          <TD align=center>
<asp:label id=Label5 runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller" Font-Bold="True" ForeColor="Maroon">Stroke</asp:label></TD></TR>
        <TR>
          <TD style="HEIGHT: 20px" align=center>
<asp:textbox id=TxtStroke runat="server" Font-Size="XX-Small" Width="64px" AutoPostBack="True" ontextchanged="TxtStroke_TextChanged"></asp:textbox></TD></TR>
        <TR>
          <TD style="HEIGHT: 20px" align=center>
<asp:label id=Label3 runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Size="XX-Small" ForeColor="Brown">Min= 0.00, Max= 120.00(Consult Factory, if stroke >120)</asp:label></TD></TR>
        <TR>
          <TD style="HEIGHT: 20px" align=center>
<asp:RequiredFieldValidator id=RequiredFieldValidator4 runat="server" Font-Size="8pt" ControlToValidate="TxtStroke" ErrorMessage="Stroke Required"></asp:RequiredFieldValidator></TD></TR></TABLE>
<asp:LinkButton id=BtnAdvanced runat="server" Font-Size="8pt" onclick="BtnAdvanced_Click">Click Here for Stop Tube</asp:LinkButton>
						</asp:panel><asp:panel id="PStoptube" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px"
							Visible="False">
      <TABLE id=Table6 borderColor=silver cellSpacing=0 cellPadding=0 width=299 
      border=0>
        <TR>
          <TD align=center>
<asp:RadioButtonList id=RBStrokeType runat="server" Font-Size="Smaller" Width="216px" RepeatLayout="Flow" RepeatDirection="Horizontal">
											<asp:ListItem Value="Standard" Selected="True">Standard</asp:ListItem>
											<asp:ListItem Value="Double Piston Design">Double Piston Design</asp:ListItem>
										</asp:RadioButtonList></TD></TR>
        <TR>
          <TD align=center>
<asp:label id=Label47 runat="server" BackColor="Transparent" Font-Size="Smaller" Width="128px" Height="19">Stop Tube Length :</asp:label>
<asp:TextBox id=TxtStopTube runat="server" Font-Size="XX-Small" Width="53" AutoPostBack="True" ontextchanged="TxtStopTube_TextChanged"></asp:TextBox></TD></TR>
        <TR>
          <TD align=center>
<asp:Label id=Label7 runat="server" Font-Size="Smaller" Width="128px">Enter Effective Stroke :</asp:Label>
<asp:TextBox id=TxtEffectiveStrok runat="server" Font-Size="XX-Small" Width="53px" AutoPostBack="True" ontextchanged="TxtEffectiveStrok_TextChanged"></asp:TextBox></TD></TR></TABLE>
						</asp:panel>
						<asp:panel id="Panel10" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
      <TABLE id=Table18 borderColor=silver cellSpacing=0 cellPadding=0 width=299 
      border=0>
        <TR></TR>
        <TR></TR>
        <TR></TR></TABLE>
      <TABLE id=Table19 borderColor=silver cellSpacing=0 cellPadding=0 width=299 
      border=0>
        <TR>
          <TD align=center colSpan=2>
<asp:label id=Label24 runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller" Font-Bold="True" ForeColor="Maroon">Mount</asp:label></TD></TR>
        <TR>
          <TD align=center colSpan=2>
<asp:image id=Image2 runat="server" ImageUrl="mounts/MX0.jpg"></asp:image>
<asp:Label id=Label20 runat="server" Font-Size="8pt" ForeColor="Black" Width="284px">MX0 Mount: No Mount</asp:Label></TD></TR></TABLE>
						</asp:panel></TD>
					<TD align="center" bgColor="#dcdcdc" colSpan="2">
						<TABLE id="Table12" cellSpacing="0" cellPadding="0" width="300" border="0">
							<TR>
								<TD align="center"><asp:panel id="Panel4" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
            <TABLE id=Table2 style="HEIGHT: 35px" borderColor=silver 
            cellSpacing=0 cellPadding=0 width=299 border=0>
              <TR>
                <TD align=center>
<asp:label id=Label6 runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller" Font-Bold="True" ForeColor="Maroon">Rod</asp:label></TD></TR>
              <TR>
                <TD align=center>
<asp:radiobuttonlist id=RBL1stRodSize runat="server" Font-Size="Smaller" BorderStyle="None" AutoPostBack="True" RepeatDirection="Horizontal" RepeatColumns="4"></asp:radiobuttonlist></TD></TR></TABLE>
									</asp:panel><asp:panel id="Panel5" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
            <TABLE id=Table8 borderColor=silver cellSpacing=0 cellPadding=0 
            width=299 border=0>
              <TR>
                <TD align=center>
<asp:label id=Label9 runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller" Font-Bold="True" ForeColor="Maroon">Rod End</asp:label></TD></TR>
              <TR>
                <TD align=center>
<asp:image id=Img1stRodEnd runat="server" Width="100px" Height="70px" ImageUrl="rodends\smallfemale.jpg"></asp:image></TD></TR>
              <TR>
                <TD align=center>
<asp:radiobuttonlist id=RBL1Rodend1 runat="server" Font-Size="Smaller" BorderStyle="None" Width="144px" Height="1px" RepeatLayout="Flow" RepeatDirection="Horizontal">
														<asp:ListItem Value="A4" Selected="True">Small Female (Series A)</asp:ListItem>
													</asp:radiobuttonlist></TD></TR></TABLE>
<asp:LinkButton id=LB1stRodAdvanced runat="server" Font-Size="8pt" Width="160" onclick="LB1stRodAdvanced_Click">Advanced Options</asp:LinkButton>
									</asp:panel><asp:panel id="P1stAdvanced" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px"
										Visible="False">
            <TABLE id=Table4 borderColor=silver cellSpacing=0 cellPadding=0 
            width=299 border=0>
              <TR>
                <TD>
<asp:label id=Label13 runat="server" BackColor="Transparent" Font-Size="Smaller" Width="88px" Height="19"> A Dimension :</asp:label>
<asp:textbox id=TXTThread runat="server" Font-Size="XX-Small" Width="52px" Height="20px" AutoPostBack="True" ontextchanged="TXTThread_TextChanged"></asp:textbox>
<asp:Label id=Label68 runat="server" Font-Size="XX-Small" ForeColor="SlateGray">(Min = 0.00 & Max = 12.00)</asp:Label></TD></TR>
              <TR>
                <TD>
<asp:label id=Label14 runat="server" BackColor="Transparent" Font-Size="Smaller" Width="88px" Height="19"> W  Dimension :</asp:label>
<asp:textbox id=TXTRodEx runat="server" Font-Size="XX-Small" Width="52px" Height="20px" AutoPostBack="True" ontextchanged="TXTRodEx_TextChanged"></asp:textbox>
<asp:Label id=lblw runat="server" Font-Size="XX-Small" ForeColor="SlateGray">(Min = 0.00 & Max = 24.00)</asp:Label></TD></TR></TABLE>
									</asp:panel></TD>
								<TD align="center"><asp:panel id="P2ndRod" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px"
										Visible="False">
            <TABLE id=Table7 style="HEIGHT: 35px" borderColor=silver 
            cellSpacing=0 cellPadding=0 width=299 border=0>
              <TR>
                <TD align=center>
<asp:label id=Label10 runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller" Font-Bold="True" ForeColor="Maroon">2nd Rod</asp:label></TD></TR>
              <TR>
                <TD align=center>
<asp:radiobuttonlist id=RBL2ndRodSize runat="server" Font-Size="Smaller" BorderStyle="None" AutoPostBack="True" RepeatDirection="Horizontal" RepeatColumns="4"></asp:radiobuttonlist></TD></TR></TABLE>
									</asp:panel><asp:panel id="P2ndRodnd" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px"
										Visible="False">
            <TABLE id=Table9 borderColor=silver cellSpacing=0 cellPadding=0 
            width=299 border=0>
              <TR>
                <TD align=center>
<asp:label id=Label12 runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller" Font-Bold="True" ForeColor="Maroon">2nd Rod End</asp:label></TD></TR>
              <TR>
                <TD align=center>
<asp:image id=Img2ndRodend runat="server" Width="100px" Height="70px" ImageUrl="rodends\smallfemale_double.jpg"></asp:image></TD></TR>
              <TR>
                <TD style="HEIGHT: 20px" align=center>
<asp:radiobuttonlist id=RBL2Rodend2 runat="server" Font-Size="Smaller" BorderStyle="None" Width="200px" RepeatLayout="Flow" RepeatDirection="Horizontal" RepeatColumns="2">
														<asp:ListItem Value="RA4" Selected="True">Small Female (Series A)</asp:ListItem>
													</asp:radiobuttonlist></TD></TR></TABLE>
<asp:LinkButton id=LB2ndRodAdvanced runat="server" Font-Size="8pt" Width="160px" onclick="LB2ndRodAdvanced_Click">Advanced Options</asp:LinkButton>
									</asp:panel><asp:panel id="P2ndAdvanced" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px"
										Visible="False">
            <TABLE id=Table10 borderColor=silver cellSpacing=0 cellPadding=0 
            width=299 border=0>
              <TR>
                <TD>
<asp:label id=Label18 runat="server" BackColor="Transparent" Font-Size="Smaller" Width="88px" Height="19"> A Dimension :</asp:label>
<asp:textbox id=TxtSecThreadEx runat="server" Font-Size="XX-Small" Width="53" Height="20px" AutoPostBack="True" ontextchanged="TxtSecThreadEx_TextChanged"></asp:textbox>
<asp:Label id=Label17 runat="server" Font-Size="XX-Small" ForeColor="SlateGray">(Min = 0.00 & Max = 12.00)</asp:Label></TD></TR>
              <TR>
                <TD>
<asp:label id=Label16 runat="server" BackColor="Transparent" Font-Size="Smaller" Width="88px" Height="19"> W  Dimension :</asp:label>
<asp:textbox id=TxtSecRodEx runat="server" Font-Size="XX-Small" Width="53px" Height="20px" AutoPostBack="True" ontextchanged="TxtSecRodEx_TextChanged"></asp:textbox>
<asp:Label id=lblsecminmax1 runat="server" Font-Size="XX-Small" ForeColor="SlateGray">(Min = 0.00 & Max = 24.00)</asp:Label></TD></TR></TABLE>
									</asp:panel></TD>
							</TR>
						</TABLE>
						<TABLE id="Table14" cellSpacing="0" cellPadding="0" width="300" border="0">
							<TR>
								<TD><asp:panel id="Panel6" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
            <TABLE id=Table13 borderColor=silver cellSpacing=0 cellPadding=0 
            width=299 border=0>
              <TR>
                <TD align=center>
<asp:label id=Label11 runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller" Font-Bold="True" ForeColor="Maroon">Cushion</asp:label></TD></TR>
              <TR>
                <TD align=center>
<asp:Label id=Label8 runat="server" Font-Size="Smaller" Width="128px">No Cushions</asp:Label></TD></TR></TABLE>
									</asp:panel>
									<asp:panel id="Panel7" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
            <TABLE id=Table15 borderColor=silver cellSpacing=0 cellPadding=0 
            width=299 border=0>
              <TR>
                <TD align=center>
<asp:label id=Label19 runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller" Font-Bold="True" ForeColor="Maroon">Ports</asp:label></TD></TR>
              <TR>
                <TD align=center>
<asp:Label id=Label15 runat="server" Font-Size="Smaller" Width="128px">NPT Ports</asp:Label></TD></TR></TABLE>
									</asp:panel>
									<asp:panel id="Panel9" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
            <TABLE id=Table17 borderColor=silver cellSpacing=0 cellPadding=0 
            width=299 border=0>
              <TR>
                <TD align=center>
<asp:label id=Label23 runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller" Font-Bold="True" ForeColor="Maroon">Port Positions</asp:label></TD></TR>
              <TR>
                <TD align=center>
<asp:Label id=Label22 runat="server" Font-Size="Smaller" Width="128px">Position 1 both ends</asp:Label></TD></TR></TABLE>
									</asp:panel></TD>
								<TD>
									<asp:panel id="Panel8" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
            <TABLE id=Table16 borderColor=silver cellSpacing=0 cellPadding=0 
            width=299 border=0>
              <TR>
                <TD align=center>
<asp:label id=Label21 runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller" Font-Bold="True" ForeColor="Maroon">Seals</asp:label></TD></TR>
              <TR>
                <TD align=center>
<asp:radiobuttonlist id=RBLSeal runat="server" Font-Size="Smaller" BorderStyle="None" RepeatColumns="1">
														<asp:ListItem Value="N" Selected="True">Standard Seals</asp:ListItem>
														<asp:ListItem Value="F">High Temp Seals</asp:ListItem>
														<asp:ListItem Value="L">Low temp Seals</asp:ListItem>
													</asp:radiobuttonlist></TD></TR></TABLE>
									</asp:panel></TD>
							</TR>
						</TABLE>
					</TD>
					<TD width="50"></TD>
				</TR>
				<TR>
					<TD width="50" height="20"></TD>
					<TD language="300" align="center" height="20"></TD>
					<TD align="center" colSpan="2" height="20"><asp:label id="lblhidden" runat="server" Font-Size="XX-Small" Visible="False" ></asp:label></TD>
					<TD width="50" height="20"><asp:linkbutton id="BtnNext" runat="server" Font-Size="Smaller" Font-Bold="True" onclick="BtnNext_Click">Next</asp:linkbutton></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
