using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for Test_BachQuote.
	/// </summary>
	public partial class Test_BachQuote : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void Button1_Click(object sender, System.EventArgs e)
		{
			
			if(FileUpload.Value.Trim() !="")
			{
				DBClass db=new DBClass();
				string strFile =UploadFileTemp_Customer(sender,e);
				db.Excel_Template_Save_Hdr(Request.PhysicalApplicationPath+"dbfile/"+strFile);
			}
		}
		private string UploadFileTemp_Customer(object Sender,EventArgs E)
		{
			string file="";
			if (FileUpload.PostedFile !=null) //Checking for valid file
			{	
				string name=DateTime.Today.ToShortDateString().Replace("/","").Replace(" ","");
				name += DateTime.Now.Ticks.ToString();//.Replace("/","").Replace(" ","").Replace(":","").Replace("","");
				string StrFileName = name+".xls" ;
				int IntFileSize =FileUpload.PostedFile.ContentLength;
				if (IntFileSize <=0)
				{
//					lblpage.Text="Uploading of file " + StrFileName + " failed ";
				}
				else
				{
					FileUpload.PostedFile.SaveAs(Server.MapPath("./dbfile/" + StrFileName.Trim()));
					file=StrFileName.ToString();
				}
			}
			return file;
		}

		protected void Button2_Click(object sender, System.EventArgs e)
		{
			DBClass db=new DBClass();
			string strFile =UploadFileTemp_Customer(sender,e);
			db.Select_DataFrom_ImportFile_All(Request.PhysicalApplicationPath+"dbfile/"+strFile);
		}
	}
}
