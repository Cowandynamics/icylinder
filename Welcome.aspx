<%@ Page language="c#" Codebehind="Welcome.aspx.cs" AutoEventWireup="True" Inherits="iCylinderV1.Welcome" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Welcome</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="style.css">
		<script type="text/javascript" src="Scripts/AC_RunActiveContent.js"></script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="Back1">
				<div id="Body_home1">
					<p class="title_txt"><asp:label id="Lblhi" runat="server" ForeColor="Gray" Font-Size="Smaller"></asp:label></p>
					<P class="title_txt2">I-Cylinder enables you to configure cylinder part numbers and 
						generate pricing using simple steps.<br>
						<br>
						A paper catalogue is not required.
						<br>
						<br>
						In addition, you can also request pricing for special configurations by using 
						the special request feature.</P>
				</div>
				<div style="Z-INDEX: 0" id="Body_home2" align="center"><br>
					<asp:label id="Label1" runat="server" Width="239px">Customized for:</asp:label><br>
					<asp:image id="ImgLogo" runat="server"></asp:image>
					<p class="flash_intro">
						<OBJECT title="Intro" codeBase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0"
							classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" data="data:application/x-oleobject;base64,btt80m2uzxGWuERFU1QAAGdVZlUACQAAAR8AAM4YAAAIAAIAAAAAAAgAAAAAAAgAAAAAAAgADgAAAFcAaQBuAGQAbwB3AAAACAAGAAAALQAxAAAACAAGAAAALQAxAAAACAAKAAAASABpAGcAaAAAAAgAAgAAAAAACAAGAAAALQAxAAAACAAAAAAACAACAAAAAAAIABAAAABTAGgAbwB3AEEAbABsAAAACAAEAAAAMAAAAAgABAAAADAAAAAIAAIAAAAAAAgAAAAAAAgAAgAAAAAADQAAAAAAAAAAAAAAAAAAAAAACAAEAAAAMQAAAAgABAAAADAAAAAIAAAAAAAIAAQAAAAwAAAACAAIAAAAYQBsAGwAAAAIAAwAAABmAGEAbABzAGUAAAAIAAwAAABmAGEAbABzAGUAAAAIAAQAAAAwAAAA"
							width="300" height="240" VIEWASTEXT>
							<embed src="coder.swf" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash"
								type="application/x-shockwave-flash" width="300" height="240"> </embed>
						</OBJECT>
					<p id="P1"><A id="A1" href="Icylinder_SelectV.aspx" runat="server">Click Here to Start</A>
					</p>
					<p class="title_txt1">This Program is confidential and intended for use by 
						authorized personnal possessing a valid User Id and Password.</p>
				</div>
				<div id="Body_home3"><asp:label id="lbllinks" runat="server"></asp:label></div>
			</div>
		</form>
	</body>
</HTML>
