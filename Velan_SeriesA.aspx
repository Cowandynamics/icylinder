<%@ Page language="c#" Codebehind="Velan_SeriesA.aspx.cs" AutoEventWireup="True" Inherits="iCylinderV1.Velan_SeriesA" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Velan_SeriesA</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 1000px; HEIGHT: 487px" cellSpacing="0" cellPadding="0"
				width="1000" bgColor="lightgrey" border="0">
				<TR>
					<TD></TD>
					<TD align="center" width="300"></TD>
					<TD align="center" width="300"><asp:label id="Label1" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
							Font-Size="Small">Series  A - Velan Cylinder</asp:label></TD>
					<TD align="center" width="300"></TD>
					<TD width="50"></TD>
				</TR>
				<TR>
					<TD width="56"></TD>
					<TD align="center" width="300" bgColor="#dcdcdc"><asp:panel id="P1stAdvanced" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE id="Table4" border="0" cellSpacing="0" borderColor="silver" cellPadding="0">
								<TR>
									<TD align="center">
										<asp:label id="Label7" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Style</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:radiobuttonlist id="RBLStyle" runat="server" Font-Size="Smaller" BorderStyle="None" AutoPostBack="True"
											Width="114px" RepeatColumns="1" onselectedindexchanged="RBLStyle_SelectedIndexChanged">
											<asp:ListItem Value="N" Selected="True">Double Acting</asp:ListItem>
											<asp:ListItem Value="FC">Fail Close</asp:ListItem>
											<asp:ListItem Value="FO">Fail Open</asp:ListItem>
										</asp:radiobuttonlist></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:label id="lblclosing" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Closing Time</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:textbox id="TxtClosingtime" runat="server" Font-Size="XX-Small" AutoPostBack="True" Width="64px" ontextchanged="TxtClosingtime_TextChanged"></asp:textbox>
										<asp:Label id="lblclosing1" runat="server" Font-Size="8pt" ForeColor="DodgerBlue" Width="16px">Sec</asp:Label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:RequiredFieldValidator id="RFQ1" runat="server" Font-Size="8pt" ErrorMessage="Closing Time Required" ControlToValidate="TxtClosingtime"></asp:RequiredFieldValidator></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="Panel8" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE id="Table11" border="0" cellSpacing="0" borderColor="silver" cellPadding="0">
								<TR>
									<TD align="center">
										<asp:label id="Label14" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Mount</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:radiobuttonlist id="RBLMount" runat="server" Font-Size="Smaller" BorderStyle="None" RepeatColumns="4"
											RepeatDirection="Horizontal">
											<asp:ListItem Value="X0" Selected="True">No Mount&lt;br /&gt;&lt;img alt='MX0 mount: No Mounts' src='mounts\\MX0.jpg'&gt;</asp:ListItem>
										</asp:radiobuttonlist></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="Panel5" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE id="Table8" border="0" cellSpacing="0" borderColor="silver" cellPadding="0">
								<TR>
									<TD align="center">
										<asp:label id="Label9" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Rod End</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:radiobuttonlist id="RBLRodend1" runat="server" Font-Size="Smaller" BorderStyle="None" RepeatDirection="Horizontal"
											RepeatLayout="Flow">
											<asp:ListItem Value="A4" Selected="True">Small Female (Series A)&lt;br /&gt;&lt;img alt='Small Female (Series A type)' src='rodends\\smallfemale.jpg'&gt;</asp:ListItem>
										</asp:radiobuttonlist></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="Panel4" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE style="HEIGHT: 8px" id="Table12" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0" width="299">
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:label id="Label20" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Stroke</asp:label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:textbox id="TxtStroke" runat="server" Font-Size="XX-Small" AutoPostBack="True" Width="64px" ontextchanged="TxtStroke_TextChanged"></asp:textbox></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:RequiredFieldValidator id="RequiredFieldValidator4" runat="server" Font-Size="8pt" ErrorMessage="Stroke Required"
											ControlToValidate="TxtStroke"></asp:RequiredFieldValidator></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
					<TD align="center" width="300" bgColor="#dcdcdc">
						<TABLE id="Table2" style="HEIGHT: 35px" borderColor="silver" cellSpacing="0" cellPadding="0"
							width="299" border="0">
						</TABLE>
						<asp:panel id="Panel2" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE style="HEIGHT: 8px" id="Table5" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label6" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Minimum Air Supply</asp:label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:textbox id="TxtMinAS" runat="server" Font-Size="XX-Small" AutoPostBack="True" Width="64px" ontextchanged="TxtMinAS_TextChanged"></asp:textbox>
										<asp:Label id="Label22" runat="server" Font-Size="8pt" ForeColor="DodgerBlue" Width="16px">PSI</asp:Label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:label id="Lblas" runat="server" Font-Size="9pt" BorderColor="Transparent" BackColor="Transparent"
											ForeColor="Red" Width="266px" Visible="False">Min Air Supply is greater than Series A capacity. The Sizing program will use 150 PSI</asp:label></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="Panel6" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE style="HEIGHT: 8px" id="Table13" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label12" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Seating Thrust</asp:label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:textbox id="TxtSeatingThrust" runat="server" Font-Size="XX-Small" AutoPostBack="True" Width="64px" ontextchanged="TxtSeatingThrust_TextChanged"></asp:textbox>
										<asp:Label id="Label3" runat="server" Font-Size="8pt" ForeColor="DodgerBlue" Width="16px">LBS</asp:Label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" Font-Size="8pt" ErrorMessage="Value Required"
											ControlToValidate="TxtSeatingThrust"></asp:RequiredFieldValidator></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="Panel10" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE style="HEIGHT: 8px" id="Table7" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label16" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Packing Friction</asp:label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:textbox id="TxtPackingFriction" runat="server" Font-Size="XX-Small" AutoPostBack="True"
											Width="64px" ontextchanged="TxtPackingFriction_TextChanged"></asp:textbox>
										<asp:Label id="Label4" runat="server" Font-Size="8pt" ForeColor="DodgerBlue" Width="16px">LBS</asp:Label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" Font-Size="8pt" ErrorMessage="Value Required"
											ControlToValidate="TxtPackingFriction"></asp:RequiredFieldValidator></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="Panel11" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE style="HEIGHT: 8px" id="Table15" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label21" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Safety Factor</asp:label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:textbox id="TxtSaftyFactor" runat="server" Font-Size="XX-Small" AutoPostBack="True" Width="64px" ontextchanged="TxtSaftyFactor_TextChanged"></asp:textbox>
										<asp:Label id="Label17" runat="server" Font-Size="8pt" ForeColor="DodgerBlue" Width="16px">%</asp:Label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:RequiredFieldValidator id="RequiredFieldValidator3" runat="server" Font-Size="8pt" ErrorMessage="Value Required"
											ControlToValidate="TxtSaftyFactor"></asp:RequiredFieldValidator></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="Panel12" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE style="HEIGHT: 8px" id="Table16" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0" width="299">
								<TR>
									<TD style="WIDTH: 144px" align="center">
										<asp:label id="Label24" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Bore Size</asp:label></TD>
									<TD align="center">
										<asp:label id="Label26" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Actual Safety Factor</asp:label></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 144px" align="center">
										<asp:textbox id="TxtBore" runat="server" Font-Size="XX-Small" AutoPostBack="True" Width="64px"
											ReadOnly="True"></asp:textbox></TD>
									<TD align="center">
										<asp:textbox id="TxtActualSafty" runat="server" Font-Size="XX-Small" AutoPostBack="True" Width="64px"
											ReadOnly="True"></asp:textbox>
										<asp:Label id="Label25" runat="server" Font-Size="8pt" ForeColor="DodgerBlue" Width="16px">%</asp:Label></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
					<TD align="center" width="300" bgColor="gainsboro"><asp:panel id="Panel3" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE style="HEIGHT: 35px" id="Table6" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0">
								<TR>
									<TD align="center">
										<asp:label id="Label10" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Ports</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:Label id="Label8" runat="server" Font-Size="Smaller">NPT Ports</asp:Label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:label id="Label11" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Port Positions</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:Label id="Label15" runat="server" Font-Size="Smaller">Position 1 Both ends</asp:Label></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="Panel7" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE style="HEIGHT: 35px" id="Table10" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0">
								<TR>
									<TD align="center">
										<asp:label id="Label13" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Piston Rod Material</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:Label id="Label19" runat="server" Font-Size="Smaller">1045 steel piston rod</asp:Label></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="P2ndRodnd" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE style="HEIGHT: 35px" id="Table9" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0">
								<TR>
									<TD align="center">
										<asp:label id="Label2" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Seals</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:radiobuttonlist id="RBLSeal" runat="server" Font-Size="Smaller" BorderStyle="None" RepeatColumns="1">
											<asp:ListItem Value="N" Selected="True">Standard Seals</asp:ListItem>
											<asp:ListItem Value="F">High Temp Seals</asp:ListItem>
											<asp:ListItem Value="L">Low temp Seals</asp:ListItem>
										</asp:radiobuttonlist></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="Panel1" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE style="HEIGHT: 35px" id="Table3" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0">
								<TR>
									<TD align="center">
										<asp:label id="Label5" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Paint</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:radiobuttonlist id="RBLPaint" runat="server" Font-Size="Smaller" BorderStyle="None" RepeatColumns="1">
											<asp:ListItem Value="S" Selected="True">Standard</asp:ListItem>
											<asp:ListItem Value="C1">Epoxy Paint</asp:ListItem>
										</asp:radiobuttonlist></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
					<TD width="50"></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 56px" align="right" width="56" height="30"></TD>
					<TD align="center" width="300" height="30"><asp:label id="lblhidden" runat="server" Font-Size="XX-Small" Visible="False"></asp:label></TD>
					<TD align="center" width="300" height="30"><asp:label id="lblpage" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Size="9pt"
							ForeColor="Red" Width="234px" Visible="False">Enter / Select  all values</asp:label></TD>
					<TD align="center" width="300" height="30"><asp:label id="Lblbore" runat="server" Font-Size="XX-Small" Visible="False"></asp:label></TD>
					<TD width="50" height="30"><asp:linkbutton id="BtnNext" runat="server" Font-Size="Smaller" Font-Bold="True" onclick="BtnNext_Click">Next</asp:linkbutton></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
