using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text; 
using System.Web.Mail;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for SVC_Summit_BatchQuote.
	/// </summary>
	public partial class SVC_Summit_BatchQuote : System.Web.UI.Page
	{
		protected void Page_Load(object sender, System.EventArgs e)
		{
			LblView.Text="";
			if(! IsPostBack)
			{
				ArrayList lst =new ArrayList();
				lst =(ArrayList)Session["User"];
				//lblTest.Text=lst[6].ToString().Trim();
				//issue #118 start
				//backup
				//if(lst[6].ToString().Trim() =="1002")
				//update
				if(lst[6].ToString().Trim() =="1002" || lst[6].ToString().Trim() =="1021")
				//issue #118 end
				{
					HLTemplate.NavigateUrl="crtfiles/slurryflo_template.xls";
				}
				else if(lst[6].ToString().Trim() =="1015")
				{
					//issue 325 start
					//back
					//HLTemplate.NavigateUrl="crtfiles/summit_template.xls";
					//update
					HLTemplate.NavigateUrl="crtfiles/template_1015.xls";
					//issue 325 end
				}
				//issue #243
				//back
				else if(lst[6].ToString().Trim() =="1018")
				{
					HLTemplate.NavigateUrl="crtfiles/redvalve_template.xls";
				}
				//issue #243 end
				LblUpload.Text="";
				StringBuilder disableButton = new StringBuilder();
				disableButton.Append("if (typeof(Page_ClientValidate) == 'function') { ");
				disableButton.Append("if (Page_ClientValidate() == false) { return false; }} ");
				disableButton.Append("this.value = 'Please Wait...';");
				disableButton.Append("this.disabled = true;");
				disableButton.Append(this.Page.GetPostBackEventReference(BtnQuoteGenerate));
				disableButton.Append(";"); 
				BtnQuoteGenerate.Attributes.Add("onclick", disableButton.ToString());
			}
		}
		private string UploadFileTemp_Customer(object Sender,EventArgs E)
		{
			string file="";
			if (FileUpload.PostedFile !=null) //Checking for valid file
			{	
				string name=DateTime.Today.ToShortDateString().Replace("/","").Replace(" ","");
				name += DateTime.Now.Ticks.ToString();//.Replace("/","").Replace(" ","").Replace(":","").Replace("","");
				string StrFileName = name+".xls" ;
				int IntFileSize =FileUpload.PostedFile.ContentLength;
				if (IntFileSize <=0)
				{
					lblpage.Text="Uploading of file " + StrFileName + " failed ";
				}
				else
				{
					FileUpload.PostedFile.SaveAs(Server.MapPath("./dbfile/" + StrFileName.Trim()));
					file=StrFileName.ToString();
				}
			}
			return file;
		}
		//issue #233 start
		private string Cyl_Displacement(coder PN)
		{
			string strCylDis = "TBA";
			try
			{
				DBClass db=new DBClass();
				decimal dcBore = 0m;
				decimal dcRod = 0m;
				decimal dcStroke = Convert.ToDecimal(PN.Stroke);
				dcRod = Convert.ToDecimal(db.SelectRodValue(PN.Rod_Diamtr));
				dcBore = Convert.ToDecimal(db.SelectBoreValue(PN.Bore_Size));
				decimal dcCylDis = 0m;
				decimal dcCylDisRet=0m;
				if(PN.DoubleRod == "No" && PN.FailMode != "FC" && PN.FailMode != "FO" )
				{
					dcCylDisRet=(dcBore*dcBore)*3.14m*dcStroke/4m;
				}
				dcCylDis=(dcBore*dcBore-dcRod*dcRod)*3.14m*dcStroke/4m + dcCylDisRet;
				if(PN.TandemDuplex == "TC")
				{
					dcCylDis *=2m;
				}
				dcCylDis = Math.Round(dcCylDis,0);
				strCylDis=dcCylDis.ToString();
			}
			catch (Exception ex)
			{		
				
				
			}
			return strCylDis;
		}
		//issue #233 end
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
		protected void LBUpload_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(FileUpload.Value.Trim() !="")
				{
					lblup.Text="";
					lblerr.Text="";
					if(FileUpload.PostedFile.ContentType.ToString() =="application/vnd.ms-excel" || FileUpload.PostedFile.ContentType.ToString() =="application/octet-stream")
					{
						lblpage.Text="";
						string upload="";
						if(FileUpload.Value.Trim()!="")
						{
							upload=UploadFileTemp_Customer(sender,e);
							LblUpload.Text=FileUpload.PostedFile.FileName.ToString();
						}
						if(upload.Trim() !="")
						{
							lblup.Text=upload.Trim();
							lblpage.Text="File uploaded successsfully <a href='ViewUploadedFile_SVC_Summit.aspx?file="+upload.Trim()+"' target='_blank'>Click here to View</a>";
							LblView.Text="<script language='javascript'>window.open('ViewUploadedFile_SVC_Summit.aspx?file="+upload.Trim()+"','_blank','toolbar=no,width=1000,height=600,resizable=yes,top=0,left=0')</script>";						
							DBClass db=new DBClass();
							ArrayList qlist=new ArrayList();
							qlist=db.Select_DataFrom_ImportFile_SVC_Summit(Request.PhysicalApplicationPath+"dbfile/"+lblup.Text.Trim());
							if(qlist.Count >0)
							{
								bool good=true;
								for(int i=0;i<qlist.Count;i++)
								{
									VelanClass cd1=new VelanClass();
									cd1=(VelanClass)qlist[i];
									if(cd1 !=null)
									{
										if(IsNumeric(cd1.Qty.Trim()) != true )
										{
											good =false;
										}
										if(cd1.Style.ToString().Trim() == "")
										{
											good =false;
										}
										if(cd1.PneumaticHydraulic.ToString().Trim() =="")
										{
											good =false;
										}										
										if(IsNumeric(cd1.Stroke.Trim()) != true)
										{
											good =false;
										}
										if(IsNumeric(cd1.Bore.Trim()) != true)
										{
											if(IsNumeric(cd1.Thrust.Trim()) != true)
											{
												good =false;
											}
											if(IsNumeric(cd1.SaftyFactor.Trim()) != true)
											{
												good =false;
											}
											if(IsNumeric(cd1.MinAirSupply.Trim()) != true)
											{
												good =false;
											}		
										}
										else
										{
											if(Convert.ToDecimal(cd1.Bore.ToString()) <4.00m  && cd1.Bore.ToUpper().StartsWith("SERIES A"))
											{
												good=false;
											}
										}
//										if(db.SelectValue("Seal_Code","WEB_Seal_TableV1","Seal_Type",cd1.Seals.ToString().Trim()).Trim() =="")
//										{
//											good =false;
//										}										
										//issue #215 start
//										if(cd1.RodWiper.ToString().Trim() =="")
//										{
//											good =false;
//										}
										//issue #215 end
										if(cd1.PneumaticHydraulic.StartsWith("Series AS"))
										{
											if(IsNumeric(cd1.Thrust.Trim()) != true || IsNumeric(cd1.BTO.Trim()) != true || IsNumeric(cd1.ETO.Trim()) != true || IsNumeric(cd1.PackingFriction.Trim()) != true)
											{
												good =false;
											}
											
										}
									}
								}
								if(good ==false)
								{
                                    //issue #263 start
									//backup
									//lblerr.Text="Error in excel file Please <a href='ViewUploadedFile.aspx?file="+upload.Trim()+"' target='_blank'>Click here to View</a> the error in uploaded file.";
									//update
									lblerr.Text="Error in excel file Please <a href='ViewUploadedFile_SVC_Summit.aspx?file="+upload.Trim()+"' target='_blank'>Click here to View</a> the error in uploaded file.";
									//issue #263 end
									lblup.Text="";
									lblpage.Text="";
								}
							}
						}
					}
					else
					{
						lblpage.Text="Please select a excel file!!!";
					}
				}
				else
				{
					lblpage.Text="Please select a excel file!!!";
				}
			}
			catch(Exception ex)
			{
				string ee=ex.Message;
				lblup.Text="";
				LblUpload.Text="";
				lblpage.Text="";
				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Not a valid import file')</script>";
			}
		}

		protected void BtnQuoteGenerate_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(lblup.Text.Trim() !="")
				{
					lblspecsheet.Text="";
					ArrayList lst =new ArrayList();
					lst =(ArrayList)Session["User"];
					DBClass db=new DBClass();
					ArrayList qlist=new ArrayList();
					//db
					//if(lst[6].ToString().Trim() !="1018" )
					//{
					qlist=db.Select_DataFrom_ImportFile_SVC_Summit(Request.PhysicalApplicationPath+"dbfile/"+lblup.Text.Trim());
					//issue #243 start
					//}
					//////Red Valve Start
					//back
//					if(lst[6].ToString().Trim() =="1018" )
//					{
//						qlist=db.Select_DataFrom_ImportFile_RedValve(Request.PhysicalApplicationPath+"dbfile/"+lblup.Text.Trim());
//					}
					//issue #243 end
					//////Red Valve End
				
					if(qlist.Count >0)
					{
						ArrayList list1 = new ArrayList();
						ArrayList list2=new ArrayList();
						list1=db.SelectContactdetails(lst[6].ToString().Trim(),lst[5].ToString().Trim());
						list2=db.SelectCompanyterms(lst[5].ToString().Trim(),lst[6].ToString().Trim());
						Quotation quot =new Quotation();
						string company="";
						if(list1[16].ToString().Trim() =="0")
						{
							company ="Montreal";
						}
						else
						{
							company ="Mississauga";
						}
						quot.Office= company.Trim();
						quot.Customer=list1[0].ToString();
						quot.Contact=lst[0].ToString().Trim();
						quot.AttnTo1=lst[0].ToString().Trim();
						quot.AttnTo2="P:"+list1[12].ToString()+" F:"+list1[13].ToString();
						quot.AttnTo3=list1[14].ToString();
						quot.BillTo1=list1[0].ToString();
						quot.BillTo2=list1[1].ToString()+", "+list1[2].ToString();
						quot.BillTo3=list1[3].ToString()+", "+list1[4].ToString()+", "+list1[5].ToString();
						quot.ShipTo1=list1[6].ToString();
						quot.ShipTo2=list1[7].ToString()+", "+list1[8].ToString();
						quot.ShipTo3=list1[9].ToString()+", "+list1[10].ToString()+", "+list1[11].ToString();
						quot.QuoteNo=Qno(lst[3].ToString().Trim());
						quot.Quotedate=DateTime.Now.ToShortDateString();
						quot.ExpiryDate=DateTime.Now.AddDays(30).ToShortDateString();;
						quot.PrepairedBy=lst[0].ToString().Trim();
						quot.Code=list2[0].ToString();
						if(list2[4].ToString().Trim()=="0")
						{
							quot.Langu="English";
						}
						else
						{
							quot.Langu="French";
						}					
						quot.Terms=list2[2].ToString();
						quot.Delivery="TBA";
						if(list2[1].ToString().Trim()=="0")
						{
							quot.Currency="CN $";
						}
						else
						{
							quot.Currency="US $";
						}
						quot.Note=TxtProjectNo.Text.ToString().Trim();
						quot.FinishedDate ="  ";
						quot.CowanQno= "  "; 
						quot.Items=quot.QuoteNo.ToString();
						quot.CompanyID=list2[5].ToString();
						string upload="";
						lblspecsheet.Text="";
						if(FileUploadSpec.Value.Trim() !="")
						{
							if(FileUploadSpec.PostedFile.ContentType.ToString() =="application/pdf")
							{										
								upload=UploadFileSpec(sender,e);
								lblstatus.Text="true";
								lblspecsheet.Text= Server.MapPath(".") + "/specsheet/"+upload.ToString();
							}
						}
						bool hydro=false;
						quot.SpecSheet=upload.Trim();
						ArrayList Implist =new ArrayList();
						for(int i=0;i<qlist.Count;i++)
						{
							bool mlprice=false;
							bool cyltype=true;
							lblmgrp.Text="";
							lbltable.Text="";
							lblstatus.Text="";
							VelanClass cod=new VelanClass();
							cod=(VelanClass)qlist[i];
							coder PN=new coder();
							//redvalve start
							PN.Series="";
							PN.Rod_End ="";
							PN.Port_Type="";
							PN.Seal_Comp="";
							PN.Port_Pos="";
							PN.Cushions="";
							PN.CushionPosition="";
							PN.SSPistionRod="";
							PN.CarbonFibBarrel="";
							PN.TandemDuplex="";
							PN.AllSS="";
							PN.Transducer="";
							PN.Bore_Size="";
							PN.Rod_Diamtr="";
							//redvalve end
							if(cod !=null)
							{
								if(cod.PneumaticHydraulic.ToString().ToUpper().Trim() =="SERIES A - STANDARD")
								{
									cyltype=true;
									PN.Series="A";
									PN.Rod_End ="A4";
									PN.Port_Type="N";
									PN.Seal_Comp="L";
									PN.Port_Pos="11";
									PN.Cushions="8";
									PN.CushionPosition="";
									PN.SSPistionRod="";					
									PN.CarbonFibBarrel="";
									PN.TandemDuplex="";
									PN.AllSS="";
									PN.Transducer="";
								}
									//issue #137 start
								else if(cod.PneumaticHydraulic.ToString().ToUpper().Trim() =="SERIES AS")
								{
									cyltype=true;
									PN.Series="AS";
									PN.Rod_End ="A4";
									PN.Port_Type="N";
									PN.Seal_Comp="N";
									PN.Port_Pos="1";
									PN.Cushions="8";
									PN.CushionPosition="";
									//PN.SSPistionRod="M3";
									PN.ETO=cod.ETO;
									PN.Stroke=cod.Stroke;
									PN.Mount = "X3";
									cod.AirPressure=cod.MinAirSupply;
									PN.MinAirSupply=cod.MinAirSupply;
									PN.PackingFriction=String.Format("{0:#.##}",Convert.ToDecimal(cod.PackingFriction));
									blCanisterPN blcanister = new blCanisterPN();
									switch (cod.Style)
									{
										case "Fail Close":
											PN.Style="FC";
											cod.FailMode="FC";
											PN.FailMode="FC";
											blcanister = db.ASSeries_Select_FC(Convert.ToDecimal(cod.Thrust),Convert.ToDecimal(cod.Stroke), Convert.ToDecimal(cod.MinAirSupply),Convert.ToDecimal(cod.PackingFriction));
											if(blcanister==null)
											{
												throw new System.ArgumentException("There is no match for canister size!");
											}
											PN.CanisterNo=blcanister.CanisterNo;
											PN.ETC = String.Format("{0:#.##}",Convert.ToDecimal(blcanister.ETC));
											PN.ETO=String.Format("{0:#.##}",Convert.ToDecimal(blcanister.ETO));
											PN.BTO=String.Format("{0:#.##}",Convert.ToDecimal(blcanister.BTO));
											PN.BTC=String.Format("{0:#.##}",Convert.ToDecimal(blcanister.BTC));
											//					cod.Stroke=blcanister.Stroke;
											PN.TandemDuplex=blcanister.Tandem.Replace("S","");
											string strTest = PN.TandemDuplex.Replace("S","");
											PN.SpringRate=String.Format("{0:#.##}",Convert.ToDecimal(blcanister.SpringRate));;
											PN.Preload = String.Format("{0:#.##}",Convert.ToDecimal(blcanister.Preload));
											string strBoreSession =db.Select_BoreCode_ByBoreValue(blcanister.CylinderBoreValue.TrimEnd('0').TrimEnd('.'));
											string[] strsBoreSession = strBoreSession.Split('|');
											blcanister.CylinderBoreCode=strsBoreSession[0];
											blcanister.CylinderBoreSize=strsBoreSession[1];
											string strRodSession = db.Select_RodDependency_ByBore(blcanister.CylinderBoreCode);
											string[] strsRodSession = strRodSession.Split('|');
											blcanister.CylinderRodCode=strsRodSession[0];
											blcanister.CylinderRodSize=strsRodSession[1];
											PN.Bore_Size=blcanister.CylinderBoreCode;
											PN.Rod_Diamtr=blcanister.CylinderRodCode;
											PN.EtcValveTrust=blcanister.EtcValveTrust;
											break;
										case "Fail Open":
											PN.Style="FC";
											cod.FailMode="FO";
											cod.AirPressure=cod.MinAirSupply;
											PN.FailMode="FO";
											blcanister = db.ASSeries_Select_FO(Convert.ToDecimal(cod.Thrust),Convert.ToDecimal(cod.ETO),Convert.ToDecimal(cod.Stroke), Convert.ToDecimal(cod.AirPressure), Convert.ToDecimal(cod.BTO));
											if(blcanister==null)
											{
												throw new System.ArgumentException("There is no match for canister size!");
											}
											PN.CanisterNo=blcanister.CanisterNo;
											PN.ETC = String.Format("{0:#.##}",Convert.ToDecimal(blcanister.ETC));
											PN.ETO=String.Format("{0:#.##}",Convert.ToDecimal(blcanister.ETO));
											PN.BTO=String.Format("{0:#.##}",Convert.ToDecimal(blcanister.BTO));
											PN.BTC=String.Format("{0:#.##}",Convert.ToDecimal(blcanister.BTC));
											//cod.Stroke=blcanister.Stroke;
											PN.TandemDuplex=blcanister.Tandem.Replace("S","");
											PN.SpringRate=String.Format("{0:#.##}",Convert.ToDecimal(blcanister.SpringRate));;
											PN.Preload = String.Format("{0:#.##}",Convert.ToDecimal(blcanister.Preload));
											PN.EtcValveTrust=cod.ETC;
											strBoreSession =db.Select_BoreCode_ByBoreValue(blcanister.CylinderBoreValue.TrimEnd('0').TrimEnd('.'));
											strsBoreSession = strBoreSession.Split('|');
											blcanister.CylinderBoreCode=strsBoreSession[0];
											blcanister.CylinderBoreSize=strsBoreSession[1];
											strRodSession = db.Select_RodDependency_ByBore(blcanister.CylinderBoreCode);
											strsRodSession = strRodSession.Split('|');
											blcanister.CylinderRodCode=strsRodSession[0];
											blcanister.CylinderRodSize=strsRodSession[1];
											PN.Bore_Size=blcanister.CylinderBoreCode;
											PN.Rod_Diamtr=blcanister.CylinderRodCode;
											PN.EtcValveTrust=blcanister.EtcValveTrust;
											break;
									}
							    
									PN.CarbonFibBarrel="";
									PN.AllSS="";
									PN.Transducer="";
									string pindex=db.SelectPriceIndex("AS").ToString();
									if(cod.RodWiper.ToString().Trim().ToUpper() =="YES")
									{
										PN.MetalScrapper="GT3";
										PN.Specials="SPECIAL";
									}
									else
									{
										PN.MetalScrapper="";
									}
								}
									//issue #137 end
								else if(cod.PneumaticHydraulic.ToString().ToUpper().Trim() =="SERIES A - WITH 316 PISTON ROD")
								{
									cyltype=true;
									PN.Series="A";
									PN.Rod_End ="A4";
									PN.Port_Type="N";
									PN.Seal_Comp="L";
									PN.Port_Pos="11";
									PN.Cushions="8";
									PN.CushionPosition="";
									PN.SSPistionRod="M3";
									PN.CarbonFibBarrel="";
									PN.TandemDuplex="";
									PN.AllSS="";
									PN.Transducer="";
								}
								else if(cod.PneumaticHydraulic.ToString().ToUpper().Trim() =="SERIES AC - CORROSION RESISTANT")
								{
									cyltype=true;
									PN.Series="AC";
									PN.Rod_End ="A4";
									PN.Port_Type="N";
									PN.Seal_Comp="L";
									PN.Port_Pos="11";
									PN.Cushions="8";
									PN.CushionPosition="";
									PN.SSPistionRod="";
									PN.CarbonFibBarrel="";
									PN.TandemDuplex="";
									PN.AllSS="";
									PN.Transducer="";
								}
								else if(cod.PneumaticHydraulic.ToString().ToUpper().Trim() =="SERIES AC - WITH STAINLESS STEEL BARREL")
								{
									cyltype=true;
									PN.Series="AC";
									PN.Rod_End ="A4";
									PN.Port_Type="N";
									PN.Seal_Comp="L";
									PN.Port_Pos="11";
									PN.Cushions="8";
									PN.CushionPosition="";
									PN.SSPistionRod="";
									PN.CarbonFibBarrel="M8";
									PN.TandemDuplex="";
									PN.AllSS="";
									PN.Transducer="";
								}
								else if(cod.PneumaticHydraulic.ToString().ToUpper().Trim() =="SERIES AT - STANDARD")
								{
									cyltype=true;
									PN.Series="AT";
									PN.Rod_End ="A4";
									PN.Port_Type="N";
									PN.Seal_Comp="L";
									PN.Port_Pos="11";
									PN.Cushions="8";
									PN.CushionPosition="";
									PN.SSPistionRod="";
									PN.CarbonFibBarrel="";
									PN.TandemDuplex="";
									PN.AllSS="";
									PN.Transducer="";
								}
								else if(cod.PneumaticHydraulic.ToString().ToUpper().Trim() =="SERIES AT - WITH ALL STAINLESS STEEL OPTION")
								{
									cyltype=true;
									PN.Series="AT";
									PN.Rod_End ="A4";
									PN.Port_Type="N";
									PN.Seal_Comp="L";
									PN.Port_Pos="11";
									PN.Cushions="8";
									PN.CushionPosition="";
									PN.SSPistionRod="";
									PN.CarbonFibBarrel="";
									PN.TandemDuplex="";
									PN.AllSS="C5";
									PN.Transducer="";
								}
								else if(cod.PneumaticHydraulic.ToString().ToUpper().Trim() =="SERIES AT - ALL STAINLESS & CARBON FIBRE BARREL")
								{
									cyltype=true;
									PN.Series="AT";
									PN.Rod_End ="A4";
									PN.Port_Type="N";
									PN.Seal_Comp="L";
									PN.Port_Pos="11";
									PN.Cushions="8";
									PN.CushionPosition="";
									PN.SSPistionRod="";
									PN.CarbonFibBarrel="";
									PN.TandemDuplex="";
									PN.AllSS="C6";
									PN.Transducer="";
								}
								else if(cod.PneumaticHydraulic.ToString().ToUpper().Trim() =="SERIES ML - STANDARD")
								{
									hydro=true;
									cyltype=false;
									PN.Series="ML";
									PN.Rod_End ="N4";
									PN.Port_Type="S";
									PN.Seal_Comp="L";
									PN.Port_Pos="11";
									PN.Cushions="8";
									PN.CushionPosition="";
									PN.SSPistionRod="";
									PN.CarbonFibBarrel="";
									PN.TandemDuplex="";
									PN.AllSS="";
									mlprice=true;
									PN.Transducer="";
									//issue #215 start
									cod.RodWiper="YES";
									//issue #215 end
								}
								else if(cod.PneumaticHydraulic.ToString().ToUpper().Trim() =="SERIES ML - WITH 17-4 PISTON ROD")
								{
									hydro=true;
									cyltype=false;
									PN.Series="ML";
									PN.Rod_End ="N4";
									PN.Port_Type="S";
									PN.Seal_Comp="L";
									PN.Port_Pos="11";
									PN.Cushions="8";
									PN.CushionPosition="";
									PN.SSPistionRod="M4";
									PN.CarbonFibBarrel="";
									PN.TandemDuplex="";
									PN.AllSS="";
									mlprice=true;
									PN.Transducer="";
									//issue #215 start
									//PN.MetalScrapper="GT3";
									cod.RodWiper="YES";
									//issue #215 end
								}
								else if(cod.PneumaticHydraulic.ToString().ToUpper().Trim() =="SERIES L - WITH PROBE")
								{
									hydro=true;
									cyltype=false;
									PN.Series="L";
									PN.Rod_End ="N4";
									PN.Port_Type="S";
									PN.Seal_Comp="L";
									PN.Port_Pos="11";
									PN.Cushions="8";
									PN.CushionPosition="";
									PN.SSPistionRod="";
									PN.CarbonFibBarrel="";
									PN.TandemDuplex="";
									PN.AllSS="";
									//issue #215 start
									//back
									PN.Transducer="TB";
									//update
                                    //PN.Transducer="";
									cod.RodWiper="YES";
									//PN.MetalScrapper="GT3";
									//issue #215 end
								}
									/////Red Valve Start
							
								else if(cod.PneumaticHydraulic.ToString().Trim() =="Pneumatic On/Off")
								{
									//if(lst[6].ToString().Trim() =="1018" )
									//{
									cyltype=true;
									PN.Series="A";
									PN.Rod_End ="A4";
									PN.Port_Type="N";
									if(cod.Seals == "Standard seals")PN.Seal_Comp="N";
									if(cod.Seals == "Low temperature seals")PN.Seal_Comp="L";
									if(cod.Seals == "Fluorocarbon Seals")PN.Seal_Comp="F";
									//PN.Seal_Comp="N";
									PN.Port_Pos="11";
									PN.Cushions="8";
									PN.CushionPosition="";
									PN.SSPistionRod="";
									PN.CarbonFibBarrel="";
									PN.TandemDuplex="";
									PN.AllSS="";
									PN.Transducer="";
                                

								
									//}
								}
								else if(cod.PneumaticHydraulic.ToString().Trim() =="Pneumatic Modulating")
								{
									//if(lst[6].ToString().Trim() =="1018" )
									//{
									cyltype=true;
									PN.Series="AT";
									PN.Rod_End ="A4";
									PN.Port_Type="N";
									//if(cod.Seals == "Standard")PN.Seal_Comp="N";
									//if(cod.Seals == "Low Temp")PN.Seal_Comp="L";
									//if(cod.Seals == "High Temp")PN.Seal_Comp="H";
									PN.Seal_Comp="N";
									PN.Port_Pos="11";
									PN.Cushions="8";
									PN.CushionPosition="";
									PN.SSPistionRod="";
									PN.CarbonFibBarrel="";
									PN.TandemDuplex="";
									PN.AllSS="C6";
									PN.Transducer="";


									//}
								}
								else if(cod.PneumaticHydraulic.ToString().Trim() =="Hydraulic On/Off")
								{
									//if(lst[6].ToString().Trim() =="1018" )
									//{
									hydro=true;
									cyltype=false;
									PN.Series="ML";
									PN.Rod_End ="N4";
									PN.Port_Type="S";
									//if(cod.Seals == "Standard")PN.Seal_Comp="N";
									//if(cod.Seals == "Low Temp")PN.Seal_Comp="L";
									//if(cod.Seals == "High Temp")PN.Seal_Comp="H";
									PN.Seal_Comp="N";
									PN.Port_Pos="11";
									PN.Cushions="8";
									PN.CushionPosition="";
									PN.SSPistionRod="M4";
									PN.CarbonFibBarrel="";
									PN.TandemDuplex="";
									PN.AllSS="";
									mlprice=true;
									PN.Transducer="";
                                    

									
									//}
								}
								else if(cod.PneumaticHydraulic.ToString().Trim() =="Hydraulic Modulating" && Convert.ToDecimal(cod.Bore.Trim()) <= 7 )
								{
									//if(lst[6].ToString().Trim() =="1018" )
									//{
									hydro=true;
									cyltype=false;
									PN.Series="L";
									PN.Rod_End ="N4";
									PN.Port_Type="S";
									//if(cod.Seals == "Standard")PN.Seal_Comp="N";
									//if(cod.Seals == "Low Temp")PN.Seal_Comp="L";
									//if(cod.Seals == "High Temp")PN.Seal_Comp="H";
									PN.Seal_Comp="N";
									PN.Port_Pos="11";
									PN.Cushions="8";
									PN.CushionPosition="";
									PN.SSPistionRod="";
									PN.CarbonFibBarrel="";
									PN.TandemDuplex="";
									PN.AllSS="";
									PN.Transducer="TB";
									//}
								}
								else if(cod.PneumaticHydraulic.ToString().Trim() =="Hydraulic Modulating" && Convert.ToDecimal(cod.Bore.Trim()) >= 7 )
								{
									//if(lst[6].ToString().Trim() =="1018" )
									//{
									hydro=true;
									cyltype=false;
									PN.Series="ML";
									PN.Rod_End ="N4";
									PN.Port_Type="S";
									//if(cod.Seals == "Standard")PN.Seal_Comp="N";
									//if(cod.Seals == "Low Temp")PN.Seal_Comp="L";
									//if(cod.Seals == "High Temp")PN.Seal_Comp="H";
									PN.Seal_Comp="N";
									PN.Port_Pos="11";
									PN.Cushions="8";
									PN.CushionPosition="";
									PN.SSPistionRod="M4";
									PN.CarbonFibBarrel="";
									PN.TandemDuplex="";
									PN.AllSS="";
									mlprice=true;
									PN.Transducer="";
									//}
								}
								/////Red Valve Ende
								PN.Stroke =String.Format("{0:##0.00}",Convert.ToDecimal(cod.Stroke.Trim()));
								//							if(cod.Seals.Trim() !="")
								//							{
								//								PN.Seal_Comp=db.SelectValue("Seal_Code","WEB_Seal_TableV1","Seal_Type",cod.Seals.ToString().Trim()).Trim();	
								//							}
								//							else
								//							{
								//								PN.Seal_Comp="L";
								//							}
								//pnappopt
								if(cod.RodWiper.ToString().Trim().ToUpper() =="YES")
								{
									//issue #137 start
									//back
//									if(cyltype ==true)
//									{
//										PN.MetalScrapper="GT2";
//									}
//									else
//									{
//										PN.MetalScrapper="GR2";
//									}
									//update
									PN.MetalScrapper="GT3";
									//issue #137 end
								}
								else
								{
									PN.MetalScrapper="";
								}
								PN.Specials="";
								if(cod.RodWiper.ToString().Trim().ToUpper() =="YES" && PN.Series=="AS")
								{
									PN.MetalScrapper="GT3";
									PN.Specials="SPECIAL";
								}
								if(cod.Style.ToString().ToUpper().Trim() =="DOUBLE ACTING")
								{
									PN.Style="N";
								}							
									//style
									//issue #137 start
									//backup
									//							else if(cod.Style.ToString().ToUpper().Trim() =="FAIL CLOSE")
									//update
								else if(cod.Style.ToString().ToUpper().Trim() =="FAIL CLOSE" && PN.Series.ToUpper().Trim() !="AS")
									//issue #137 end
								{
									PN.Style="FC";
									PN.Specials="SPECIAL";
								}
									//issue #137 start
									//backup
									//else if(cod.Style.ToString().ToUpper().Trim() =="FAIL OPEN")
									//update
								else if(cod.Style.ToString().ToUpper().Trim() =="FAIL OPEN" && PN.Series.ToUpper().Trim() !="AS")
									//issue #137 end
								{
									PN.Style="FO";
									PN.Specials="SPECIAL";
								}
									//issue #137 start
									//backup
									//else
									//update
								else if(PN.Series.ToUpper().Trim() !="AS")
									//issue #137 end
								{
									PN.Style="N";
								}									
								PN.SecRodDiameter="";
								PN.SecRodEnd="";
								PN.DoubleRod="No";	
								PN.Qty=cod.Qty.ToString().Trim();								
								decimal bore=0.00m;
								///Red Valve Start
								string boresize="";
								///Red Valve End
								//bore
								//issue #137 start
								//back
								//if(cod.Bore.ToString().Trim() !="")
								//update
								if(cod.Bore.ToString().Trim() !="" && PN.Series.ToUpper().Trim() !="AS")
									//issue #137 end
								{
									bore=Convert.ToDecimal(cod.Bore.ToString());
									boresize=cod.Bore.ToString();
									if(cyltype ==true)
									{
										PN.Mount="X0";							
									}
									else
									{
										//									if(bore >=0.0m && bore <=8m)
										//									{
										PN.Mount="E5";	
										//									}
										//									else
										//									{
										//										PN.Mount="F5";	
										//									}
									}
								
									string boreval="";
									boreval=db.SelectValue("Bore_Code","WEB_Bore_TableV1","BoreValue",bore.ToString());
									if(boreval.Trim() !="")
									{
										PN.Bore_Size=boreval.ToString().Trim();
									}
									//boredependency
									if(PN.Series.Trim()=="L")
									{
										string brval="";
										brval=db.SelectValue("BoreValue","WEB_Bore_TableV1","Bore_Code",PN.Bore_Size.Trim());
										decimal bdc=0;
										bdc=Convert.ToDecimal(brval);
										if(bdc >4 && bdc <=6)
										{
											mlprice=true;
										}
										if(bdc >6)
										{
											PN.Series="ML";
											PN.Transducer="TB";
											mlprice =true;
										}
									}
									//rod
									if(PN.Series.Trim() =="A" || PN.Series.Trim() =="AC" || PN.Series.Trim() =="AT"  )
									{
										PN.Rod_Diamtr=db.SelectValue("Rod_Code","WEB_RodSerA_TableV1",PN.Bore_Size,"1").Trim();
									}
									else if(PN.Series.Trim() =="ML")
									{
										PN.Rod_Diamtr=db.SelectValue_Rotork("Rod_Code","WEB_RodSerM_TableV1",PN.Bore_Size,"1","RodValue").Trim();									
									}
									else if(PN.Series.Trim() =="L")
									{
										PN.Rod_Diamtr=db.SelectValue_Rotork("Rod_Code","WEB_RodSerL_TableV1",PN.Bore_Size,"1","RodValue").Trim();									
									}
									if(PN.Bore_Size.Trim()!="" || PN.Rod_Diamtr.Trim()!="")
									{									
										if(PN.Series.Trim() =="A")
										{
											APricing(PN,false);										
										}
										else if(PN.Series.Trim() =="AC")
										{
											ACPricing(PN,false);
										}
										else if(PN.Series.Trim() =="AT")
										{
											ATPricing(PN);
										}
										else if(PN.Series.Trim() =="ML" || mlprice ==true )
										{
											MLPricing(PN);
										}
										else if(PN.Series.Trim() =="L" && mlprice ==false )
										{
											LPricing(PN);
										}
											//pricing
										else if(PN.Series.Trim() =="AS" )
										{
											
											//ASPricing(PN);
										}
										else
										{
											lbltotal.Text ="0.00";
											lblstatus.Text ="true";
										}
									}
									else
									{
										lbltotal.Text ="0.00";
										lblstatus.Text ="true";
										PN.Bore_Size="Z";
										PN.Rod_Diamtr="Z";
									}
								}
									//boreend
									//seriesafail
									//issue #137 start
								else if(PN.Series.ToUpper().Trim() =="AS")
								{
									lbltotal.Text ="0.00";
									ASPricing(PN);
								}
									//issue #137 end
								else
								{
									if(cod.MinAirSupply.ToString().Trim() !="")
									{
										PN.MinAirSupply=cod.MinAirSupply.ToString().Trim();
									}
									if(cod.Thrust.ToString().Trim() !="")
									{
										PN.SeatingTrust=cod.Thrust.ToString().Trim();
									}
									if(cod.SaftyFactor.ToString().Trim() !="")
									{
										PN.SaftyFactor=cod.SaftyFactor.ToString().Trim();
									}
									double op=0;
									double th=0;
									double sf=0;
									double area=0;
									op=Convert.ToDouble(PN.MinAirSupply.ToString().Trim());
									th=Convert.ToDouble(PN.SeatingTrust.ToString().Trim());
									sf=Convert.ToDouble(PN.SaftyFactor.ToString().Trim());
									area=(th * (1 + sf/100)/ op);
									if(area >0)
									{
										ArrayList alist1=new ArrayList();
										ArrayList alist2=new ArrayList();
										if(PN.Series.Trim()=="A" || PN.Series.Trim()=="AC")
										{
											PN.Mount="X0";
											decimal stdprice=0;
											decimal tamdemprice=0;
											alist1=db.Select_BoreByBoreArea("WEB_SVC_Summit_AAC_Area_TableV1","Standard_Area",area.ToString().Trim());
											alist2=db.Select_BoreByBoreArea("WEB_SVC_Summit_AAC_Area_TableV1","Tandem_Area",area.ToString().Trim());								
											if(alist1.Count ==3 && alist2.Count ==3)
											{
												PN.Bore_Size=alist1[0].ToString();
												PN.Rod_Diamtr=alist1[1].ToString();
												if(PN.Series.Trim()=="A" )
												{
													stdprice=APricing(PN,false);
												}
												else if(PN.Series.Trim()=="AC")
												{
													stdprice=ACPricing(PN,false);
												}										
												PN.Bore_Size=alist2[0].ToString();
												PN.Rod_Diamtr=alist2[1].ToString();
												if(PN.Series.Trim()=="A" )
												{
													tamdemprice=APricing(PN,true);
												}
												else if(PN.Series.Trim()=="AC")
												{
													tamdemprice=ACPricing(PN,true);
												}															
												if(lblstatus.Text.Trim() !="true")
												{
													if(stdprice > tamdemprice)
													{
														PN.Bore_Size=alist2[0].ToString();
														PN.Rod_Diamtr=alist2[1].ToString();
														if(PN.Series.Trim()=="A" )
														{
															APricing(PN,true);
														}
														else if(PN.Series.Trim()=="AC")
														{
															ACPricing(PN,true);
														}
													}
													else
													{
														PN.Bore_Size=alist1[0].ToString();
														PN.Rod_Diamtr=alist1[1].ToString();
														if(PN.Series.Trim()=="A" )
														{
															APricing(PN,false);
														}
														else if(PN.Series.Trim()=="AC")
														{
															ACPricing(PN,false);
														}
													}
												}
											}
											else
											{
												PN.Bore_Size="Z";
												PN.Rod_Diamtr="Z";
												lbltotal.Text ="0.00";
												lblstatus.Text ="true";
											}
										}
										else if(PN.Series.Trim()=="AT")
										{
											PN.Mount="X0";
											alist1=db.Select_BoreByBoreArea("WEB_SVC_Summit_AAC_Area_TableV1","Standard_Area",area.ToString().Trim());
											if(alist1.Count ==3)
											{
												PN.Bore_Size=alist1[0].ToString();
												PN.Rod_Diamtr=alist1[1].ToString();
												if(PN.Bore_Size.Trim()!="" || PN.Rod_Diamtr.Trim()!="")
												{
													ATPricing(PN);
												}											
											}
										}
										else 
										{
											alist1=db.Select_BoreByBoreArea("WEB_SVC_Summit_ML_Area_TableV1","Standard_Area",area.ToString().Trim());
											if(alist1.Count ==3)
											{
												PN.Bore_Size=alist1[0].ToString();
												PN.Rod_Diamtr=alist1[1].ToString();
												boresize=db.SelectValue("Bore","WEB_SVC_Summit_ML_Area_TableV1","BCode",PN.Bore_Size.ToString().Trim());
												if(boresize !="")
												{
													bore=Convert.ToDecimal(boresize);
													//												if(bore >=0.0m && bore <=8m)
													//												{
													PN.Mount="E5";	
													//												}
													//												else
													//												{
													//													PN.Mount="F5";	
													//												}
												}
												if(PN.Bore_Size.Trim()!="" || PN.Rod_Diamtr.Trim()!="")
												{
													//boredependency
													//issue #215 start
													if(PN.Series.Trim()=="L")
													{
														string brval="";
														brval=db.SelectValue("BoreValue","WEB_Bore_TableV1","Bore_Code",PN.Bore_Size.Trim());
														decimal bdc=0;
														bdc=Convert.ToDecimal(brval);
														if(bdc >4)
														{
															PN.Series="ML";
															PN.Transducer="TB";
															mlprice =true;
														}
													}
													//issue #215 end
													if(PN.Series.Trim() =="ML")
													{
														MLPricing(PN);
													}											
													else if(PN.Series.Trim() =="L")
													{
														LPricing(PN);
													}
													else
													{
														lbltotal.Text ="0.00";
														lblstatus.Text ="true";
													}
												}
												else
												{
													lbltotal.Text ="0.00";
													lblstatus.Text ="true";
												}
											}
										}
										double ctps=0;		
										double ctpl=0;	
										double ba=0;
										double br=0;
										if(cyltype ==true)
										{
											string strba=db.SelectValue("BA","WEB_SVC_Summit_AAC_Area_TableV1","BCode",PN.Bore_Size.ToString().Trim());
											if(strba !="")
											{
												ba=Convert.ToDouble(strba.Trim());
											}
											string strbr=db.SelectValue("BR","WEB_SVC_Summit_AAC_Area_TableV1","BCode",PN.Bore_Size.ToString().Trim());
											if(strbr !="")
											{
												br=Convert.ToDouble(strbr.Trim());
											}
											ctps= (op) * ba;									
											ctpl= (op) * (ba -br);
										}
										else
										{
											string strba=db.SelectValue("BA","WEB_SVC_Summit_ML_Area_TableV1","BCode",PN.Bore_Size.ToString().Trim());
											if(strba !="")
											{
												ba=Convert.ToDouble(strba.Trim());
											}
											string strbr=db.SelectValue("BR","WEB_SVC_Summit_ML_Area_TableV1","BCode",PN.Bore_Size.ToString().Trim());
											if(strbr !="")
											{
												br=Convert.ToDouble(strbr.Trim());
											}
											ctps= (op) * ba;									
											ctpl= (op) * (ba -br);
										}
										PN.CylinderThrustPush=Decimal.Round(Convert.ToDecimal(ctps),0).ToString();
										PN.CylinderThrustPull=Decimal.Round(Convert.ToDecimal(ctpl),0).ToString();
										PN.SafetyFactorPush=Decimal.Round(Convert.ToDecimal(((ctps/th)-1)*100),0).ToString();
										PN.SafetyFactorPull=Decimal.Round(Convert.ToDecimal(((ctpl/th)-1)*100),0).ToString();
									}
									else
									{
										PN.Bore_Size="Z";
										PN.Rod_Diamtr="Z";
										if(cyltype ==true)
										{
											PN.Mount="X0";							
										}
										else
										{
											//										if(bore >=0.0m && bore <=8m)
											//										{
											PN.Mount="E5";	
											//										}
											//										else
											//										{
											//											PN.Mount="F5";	
											//										}
										}
										lbltotal.Text ="0.00";
										lblstatus.Text ="true";
									}
								}
								//boreend
								//specialstart
								//issue #137 start
								//backup
								//if(PN.Style.ToString().Trim() !="N")
								//update
								if(PN.Style.ToString().Trim() !="N" && PN.Series.ToUpper().Trim() !="AS")
									//issue #137 end
								{
									PN.Bore_Size="Z";
									PN.Rod_Diamtr="Z";
									if(cyltype ==true)
									{
										PN.Mount="X0";							
									}
									else
									{
										//									if(bore >=0.0m && bore <=8m)
										//									{
										PN.Mount="E5";	
										//									}
										//									else
										//									{
										//										PN.Mount="F5";	
										//									}
									}
								}		
								//numenclature		
								PN.PNO =PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+
									PN.Rod_End.ToString().Trim()+PN.Cushions.ToString().Trim()+PN.CushionPosition.ToString().Trim()+PN.MetalScrapper.ToString().Trim()+
									PN.Seal_Comp.ToString().Trim()+PN.Port_Type.ToString().Trim()+PN.Port_Pos.ToString().Trim()+
									PN.Mount.ToString().Trim()+PN.Stroke.ToString().Trim()+PN.SSPistionRod.ToString().Trim()+PN.CarbonFibBarrel.ToString().Trim()+PN.Transducer.ToString().Trim();
								LblPartNo.Text=PN.PNO.ToString();							
								//issue #137 start
								if(PN.Series == "AS")
								{
									PN.PNO =PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+
										PN.Rod_End.ToString().Trim()+PN.MetalScrapper.ToString().Trim()+PN.Seal_Comp.ToString().Trim()+PN.Port_Type.ToString().Trim()+PN.Port_Pos.ToString().Trim()+
										PN.Mount.ToString().Trim()+PN.Stroke.ToString().Trim()+PN.TandemDuplex+PN.FailMode.Replace("FO","O").Replace("FC","C")+PN.CanisterNo+"-"+PN.Preload;
									LblPartNo.Text=PN.PNO.ToString();	
								}
								//issue #137 end
								try
								{
									string st="";
									if(Convert.ToDecimal(PN.Stroke.ToString()) > 120.00m)
									{
										st="For pricing please consult factory";
										lblstatus.Text="true";
									}
									//rodenddim
									string rd=PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim();
									string rodenddim ="";
									if(db.SelectRRD(rd).Trim() !="")
									{
										rodenddim =" KK = "+db.SelectRRD(rd);
									}
									//issue #137 start
									if(PN.Series=="AS")
									{
										string sr=PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim();
										string kk=db.SelectValue("RodEnd_Dimension","RodEndDiamension_TableV1","RR_Code",sr.Trim());
										if(kk.ToString().Trim() !="")
										{
											PN.RodendDim =" KK="+kk.Trim();
										}
										else PN.RodendDim="";
									}
									//issue #137 end
									//portsize
									string portsiz="";					
									string table="";
									//issue #137 start
									//backup
									//if(PN.Series.Trim().Substring(0,1) =="A" ||PN.Series.Trim().Substring(0,1) =="S")
									//update
									if((PN.Series.Trim().Substring(0,1) =="A" ||PN.Series.Trim().Substring(0,1) =="S") && PN.Series!="AS" )
									{
										table ="WEB_SeriesAPortSize_TableV1";
									}
									else if(PN.Series.Trim() =="ML" || PN.Series.Trim() =="L")
									{
										table ="WEB_SeriesMMLLRPortSize_TableV1";
									}
										//issue #137 start
									else if(PN.Series.Trim() =="AS")
									{
										table ="SeriesASPortSize_TableV1";
									}
									//issue #137 end
									if(db.SelectPortSizeNo(PN.Bore_Size.Trim(),PN.Port_Type.Trim(),table.Trim()) !="")
									{
										portsiz ="#"+db.SelectPortSizeNo(PN.Bore_Size.Trim(),PN.Port_Type.Trim(),table.Trim());
									}
									//initquote
									QItems Qitem =new QItems();
									Qitem.ItemNo =quot.QuoteNo.ToString();
									Qitem.S_Code=PN.Series.Trim();
									Qitem.Series=db.SelectValue("Series_Name","WEB_Series_TableV1","Series_Code",PN.Series.ToString().Trim()).ToString();
									Qitem.S_Price="0.00";
									Qitem.B_Code=PN.Bore_Size.ToString().Trim();
									Qitem.Bore=db.SelectValue("Bore_Size","WEB_Bore_TableV1","Bore_Code",PN.Bore_Size.ToString().Trim()).ToString()+" Bore Size";
									Qitem.B_Price="0.00";
									Qitem.R_Code=PN.Rod_Diamtr.ToString().Trim();
									Qitem.Rod=db.SelectValue("Rod_Size","WEB_RodSerZ_TableV1","Rod_Code",PN.Rod_Diamtr.ToString().Trim())+" Rod Size";
									Qitem.R_Price="0.00";
									Qitem.Stroke_Code=PN.Stroke.ToString().Trim();
									Qitem.Stroke="Stroke = "+PN.Stroke.ToString()+"\""+st.ToString();
									Qitem.Stroke_Price="0.00";
									Qitem.M_code=PN.Mount.ToString().Trim();
									Qitem.Mount=db.SelectValue("Mount_Type","WEB_Mount"+PN.Series.Trim()+"_TableV1","Mount_Code",PN.Mount.ToString().Trim()).ToString();
									Qitem.M_Price="0.00";
									Qitem.RE_Code=PN.Rod_End.ToString().Trim();
									Qitem.RodEnd=db.SelectValue("RodEnd_Shape","WEB_RodEndKK_TableV1","RodEnd_Code",PN.Rod_End.ToString().Trim()).ToString()+rodenddim.ToString();
									Qitem.RE_Price="0.00";
									Qitem.Cu_Code=PN.Cushions.ToString().Trim();
									Qitem.Cushion=db.SelectValue("Cushion_type","WEB_Cushion_TableV1","Cushion_Code",PN.Cushions.ToString()).ToString();
									Qitem.Cu_Price="0.00";
									Qitem.CushionPos_Code=PN.CushionPosition.ToString().Trim();
									Qitem.CushionPos=db.SelectValue("CushionPos_Pos","WEB_CushionPos_TableV1","CushionPos_Code",PN.CushionPosition.ToString().Trim()).ToString();
									Qitem.CushionPos_Price="0.00";
									Qitem.Sel_Code=PN.Seal_Comp.ToString().Trim();
									Qitem.Seal=db.SelectValue("Seal_Type","WEB_Seal_TableV1","Seal_Code",PN.Seal_Comp.ToString().Trim()).ToString();
									Qitem.Sel_Price="0.00";
									Qitem.Port_Code=PN.Port_Type.ToString().Trim();
									Qitem.Port=portsiz.Trim()+" "+ db.SelectValue("PortType_Type","WEB_portType_TableV1","PortType_Code",PN.Port_Type.ToString().Trim()).ToString();
									Qitem.Port_Price="0.00";
									Qitem.PP_Code=PN.Port_Pos.ToString().Trim();
									Qitem.PortPos=db.SelectValue("PortPos_Position","WEB_PortPosition_TableV1","PortPos_Code",PN.Port_Pos.ToString().Trim()).ToString();
									Qitem.PP_Price="0.00";
									Qitem.Quantity=cod.Qty.ToString();
									//discount
									//issue #137 start
									//backup
									//if(Qitem.S_Code.Trim()=="A" || Qitem.S_Code.Trim()=="AC" || Qitem.S_Code.Trim()=="AT" )
									//update
									if(Qitem.S_Code.Trim()=="A" || Qitem.S_Code.Trim()=="AC" || Qitem.S_Code.Trim()=="AT" || Qitem.S_Code.Trim()=="AS" )
										//issue #137 end
									{
										Qitem.Discount="0";
									}
									else
									{
										if(PN.Series =="ML")
										{
											decimal qty=0;
											qty=Convert.ToDecimal(PN.Qty.ToString());
											if(qty >0 && qty <=2)
											{
												Qitem.Discount="35";
											}
											else if(qty >=3 && qty <=5)
											{
												Qitem.Discount="37";
											}
											else if(qty >=6 && qty <=10)
											{
												Qitem.Discount="40";
											}
											else if(qty >10)
											{
												Qitem.Discount="45";
											}
										}
										else
										{
											Qitem.Discount="35";
										}
									}
									//customer
									Qitem.Cusomer_ID=quot.Customer.Trim();
									Qitem.Quotation_No=quot.QuoteNo.Trim();
									Qitem.Q_Date=quot.Quotedate.Trim();
									Qitem.User_ID=lst[0].ToString();
									Qitem.PriceList="0";
									Qitem.DWG ="";
									string dwgnotes="";
									//drawings
									if(Qitem.B_Code.Trim() !="Z" &&  Qitem.R_Code.Trim() !="Z" )
									{
										if(PN.CylinderThrustPush !=null)
										{
											dwgnotes +="  Cylinder Thrust Push : "+PN.CylinderThrustPush.ToString().Trim()+",";
										}
										if(PN.CylinderThrustPull !=null)
										{
											dwgnotes +="  Cylinder Thrust Pull : "+PN.CylinderThrustPull.ToString().Trim()+",";
										}
										if(PN.SafetyFactorPush !=null)
										{
											dwgnotes +="  Safety Factor Push : "+PN.SafetyFactorPush.ToString().Trim()+"%,";
										}
										if(PN.SafetyFactorPull !=null)
										{
											dwgnotes +="  Safety Factor Pull : "+PN.SafetyFactorPull.ToString().Trim()+"%";
										}
									}
									Qitem.SpecialReq=dwgnotes.ToString();
									Qitem.Note=cod.Notes.ToString();
									string commadd="";
									decimal index =0.00m;
									if(Qitem.S_Code.Trim() !="")
									{
										commadd="WEB_Series"+Qitem.S_Code.Trim()+"CommAdders_TableV1";
										string ind="";
										if(PN.Bore_Size.ToString().Trim()=="C" || PN.Bore_Size.ToString().Trim()=="D" || PN.Bore_Size.ToString().Trim()=="E")
										{			
											ind=db.SelectPriceIndex("MLS").ToString();	
										}
										else
										{
											ind=db.SelectPriceIndex(Qitem.S_Code.Trim()).ToString();	
										}									
										index =Convert.ToDecimal(ind.Trim());
									}
									if(PN.Specials !=null)
									{
										if(PN.Specials.ToString().ToUpper().Trim() =="SPECIAL")
										{
											decimal total =0.00m;
											bool result1=false;
											string sp ="";
											int tt=Convert.ToInt32(db.SelectLastSpNo());
											sp=(tt+1).ToString();
											//numenclature
											PN.PNO ="Z"+PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim()+
												PN.Cushions.ToString().Trim()+PN.Mount.ToString().Trim()+"/Z"+sp.ToString();
											LblPartNo.Text ="Z"+PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim()+
												PN.Cushions.ToString().Trim()+PN.Mount.ToString().Trim()+"/Z"+sp.ToString();
											//issue #137 start
											if(PN.Series == "AS")
											{
												PN.PNO ="Z"+PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim()+
													PN.Mount.ToString().Trim()+ PN.TandemDuplex+PN.FailMode.Replace("FO","O").Replace("FC","C")+PN.CanisterNo+"-"+"/Z"+sp.ToString();
												LblPartNo.Text ="Z"+PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim()+
													PN.Mount.ToString().Trim()+ PN.TandemDuplex+PN.FailMode.Replace("FO","O").Replace("FC","C")+PN.CanisterNo+"-"+"/Z"+sp.ToString();
											}
											//issue #137 end
											if(PN.Style !=null)
											{
												if(PN.Style.Trim() !="")
												{
													if(PN.Series != "AS")
													{
														if(PN.Style.Trim() =="FC" || PN.Style.Trim() =="FO")
														{
															total =0.00m;
															if(total ==0)
															{
																result1 =true;
															}
															string des="";

															des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Style.Trim());
															string sp1 ="";
															int tt1=Convert.ToInt32(db.SelectLastSpNo());
															sp1=(tt1+1).ToString();
															db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Style.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"1");
															db.InsertSpecialCount(sp1.Trim());
														}
													}
													else if(PN.Series == "AS")
													{
														total =0.00m;
														string des="";

														des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.MetalScrapper.Trim());
														total =db.SelectAddersPrice("GT2",commadd.Trim(),PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
														if(total ==0)
														{
															result1 =true;
														}
														else lbltotal.Text = Convert.ToString(Convert.ToDecimal(lbltotal.Text)+total);
														string sp1 ="";
														int tt1=Convert.ToInt32(db.SelectLastSpNo());
														sp1=(tt1+1).ToString();
														db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.MetalScrapper.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"1");
														db.InsertSpecialCount(sp1.Trim());
													}
												}
											}								
											if(result1 ==true)
											{
												lblstatus.Text= "true";
											}
										}
										else
										{
											Qitem.Special_ID="";
										}
									}
									else
									{
										Qitem.Special_ID="";
									}
									//appopt
									decimal app=0.00m;
									if(PN.MetalScrapper.ToString().Trim() !="" && PN.Series!="AS")
									{
										if(PN.MetalScrapper.ToString().Trim() =="GR2")
										{
											//
											if(PN.Seal_Comp.Trim() =="F")
											{
												app =db.SelectAddersPrice("FGR2",commadd.Trim(),PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());											
											}
												// issue #79
											else
											{	
												app =db.SelectAddersPrice("GR2",commadd.Trim(),PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());											
											}
										}
										//issue #137 start
										if(PN.MetalScrapper.ToString().Trim() =="GT2")
										{
											app =db.SelectAddersPrice("GT2",commadd.Trim(),PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());											
											
										}
										//issue #137 end
										//issue #215 start
										if(PN.MetalScrapper.ToString().Trim() =="GT3")
										{
											app =db.SelectAddersPrice("GT3",commadd.Trim(),PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());											
											
										}
										//issue #215 end
										if(app ==0)
										{
											lblstatus.Text ="true";
										}
										string desc="";
										desc=db.SelectAppopts(PN.MetalScrapper.ToString().Trim());
										db.InsertItemApplicationOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.MetalScrapper.ToString(),desc.ToString(),"0.00");
										Qitem.ApplicationOpt_Id=quot.QuoteNo.Trim()+LblPartNo.Text.Trim();
									}
									else
									{
										Qitem.ApplicationOpt_Id="";
									}	
									//popopt
									decimal pop=0.00m;
									if(PN.CarbonFibBarrel.ToString().Trim() !="" || PN.SSPistionRod.ToString().Trim() !="" || PN.TandemDuplex.ToString().Trim() !="" || PN.Transducer.ToString().Trim() !="" )
									{
										if(PN.CarbonFibBarrel.ToString().Trim() =="M8")
										{
											string desc="";
											desc=db.SelectPopopts(PN.CarbonFibBarrel.Trim());
											db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.CarbonFibBarrel.ToString(),desc.ToString(),"0.00");
										}
										if(PN.SSPistionRod.ToString().Trim() !="")
										{
											string desc="";
											desc=db.SelectPopopts(PN.SSPistionRod.Trim());
											db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.SSPistionRod.ToString(),desc.ToString(),"0.00");
										}
										if(PN.TandemDuplex.ToString().Trim() !="")
										{										
											string desc="";
											desc=db.SelectPopopts(PN.TandemDuplex.Trim());
											db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.TandemDuplex.ToString(),desc.ToString(),"0.00");
										}	
										if(PN.Transducer.ToString().Trim() !="")
										{
											decimal transducer=0;
											if(PN.Series.Trim()=="L" || PN.Series.Trim()=="ML")
											{										
												decimal tbbase=0;
												decimal tbstroke=0;
												tbbase =db.SelectAddersPrice("TB_Base","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
												tbstroke=db.SelectAddersPrice_Transducer("TB_Series"+PN.Series.Trim(),PN.Stroke.ToString().Trim());
												transducer=tbbase +tbstroke;
												//transducer =transducer + transducer * (index /100);
												pop=transducer;
											}
											if(transducer ==0)
											{
												lblstatus.Text ="true";
											}
										
											string desc="";
											desc=db.SelectPopopts(PN.Transducer.Trim());
											db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.Transducer.ToString(),desc.ToString(),"0.00");
										}
										Qitem.PopularOpt_Id=quot.QuoteNo.Trim()+LblPartNo.Text.Trim();
									}								
									else
									{
										Qitem.PopularOpt_Id="";
									}
									//aspopopt
									//issue #137 start
									if(PN.Series=="AS")
									{
										decimal dcCanisterPrice = Convert.ToDecimal(db.SelectOneValueByAllinfo("CanPrice","AS_PRELOAD_SPACER",
											"CanisterNo",PN.CanisterNo,
											"Preload",PN.Preload).ToString());
										pop += dcCanisterPrice;
										if(dcCanisterPrice ==0)
										{
											lblstatus.Text ="true";
										}
										string strCanisterDesc = Create_CanisterDesc(PN.FailMode,PN.CanisterNo,PN.Preload,PN.BTO,PN.ETO,PN.ETC,PN.BTC);
										db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.FailMode.ToString(),strCanisterDesc,dcCanisterPrice.ToString());
										string strCanisterIns = db.InsertCanisters(quot.QuoteNo,PN.PNO,PN.FailMode,PN.CanisterNo,PN.Preload,PN.ETC, PN.ETO,PN.BTO,PN.MinAirSupply,PN.PackingFriction,PN.SpringRate,PN.EtcValveTrust,dcCanisterPrice.ToString(),"",PN.BTC,"0");
										Qitem.PopularOpt_Id=quot.QuoteNo.Trim()+LblPartNo.Text.Trim();
								
									}
									//issue #137 end
									//issue #215 start
//									if(PN.Series=="ML")
//									{
										string pindex="0";
										pindex=db.SelectPriceIndex(PN.Series).ToString();
										app=app*(1+Convert.ToDecimal(pindex)/100m);
										pop=pop*(1+Convert.ToDecimal(pindex)/100m);
//									}
									//issue #215 end
									decimal dcc2=0.00m;
									dcc2 =Convert.ToDecimal(lbltotal.Text);		
									//issue #315 start
									//issue #79
									dcc2 =dcc2 +app+pop;
									//dcc2 =dcc2 +pop;
									decimal dc1 ,dc3,dctotal=0.00m;
									decimal curency=1.00m;
									if(quot.Currency.ToString().Trim() =="CN $")	
									{	
										dctotal=dcc2 *curency;
										lbltotal.Text=dctotal.ToString();
										dc1=Convert.ToDecimal(cod.Qty.ToString());
										dc3= dc1 * dctotal ;
									}
									else
									{
										curency=Convert.ToDecimal(db.SelectValue("USD_Q","Dollar_ExchangeRate_TableV1","Slno","1"));
										dctotal=dcc2 *curency;
										lbltotal.Text=dctotal.ToString();
										dc1=Convert.ToDecimal(cod.Qty.ToString());
										dc3= dc1 * dctotal ;
									}
									if(lblspecsheet.Text.Trim() !="")
									{
										lblstatus.Text="true";
									}
									if(lblstatus.Text.ToLower().Trim() =="true" || dc3 > 100000)
									{
										Qitem.UnitPrice ="0.00";
										Qitem.TotalPrice="0.00";
									}
									else
									{
										Qitem.UnitPrice=lbltotal.Text.ToString();
										Qitem.TotalPrice=dc3.ToString();
									}
									Qitem.Special_ID="";
									Qitem.PartNo=LblPartNo.Text.ToString();
									//weight
									if(PN.Series.Trim() !="")
									{
										string weight="";
										decimal c1,c2,c22,c3=0.00m;
										decimal c4=0.00m;
										decimal c5=0.00m;
										if(PN.Series.Trim()== "A" )
										{
											//issue weight start backup
											//										if(PN.DoubleRod.ToUpper().Trim() =="YES")
											//										{
											//											c1=db.SelectAddersPrice("DWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
											//											c2=db.SelectAddersPrice("DStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
											//											c3=Convert.ToDecimal(PN.Stroke.Trim());
											//										}
											//										else
											//										{
											//											c1=db.SelectAddersPrice("SWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
											//											c2=db.SelectAddersPrice("SStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
											//											c3=Convert.ToDecimal(PN.Stroke.Trim());
											//										}
											//										c4=c1 + (c2 * c3);
											//										c4=Decimal.Round(c4,0);
											//issue weight start backup
											//issue weight start update
											if(PN.TandemDuplex ==null && PN.DoubleRod.ToUpper().Trim() =="NO")
											{
												c1=db.SelectAddersPrice("SWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												c2=db.SelectAddersPrice("SStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												c4=c1+c2*Convert.ToDecimal(PN.Stroke.Trim());
												c4=Decimal.Round(c4,0);
											}
											if(PN.TandemDuplex ==null && PN.DoubleRod.ToUpper().Trim() =="YES")
											{
												c1=db.SelectAddersPrice("DWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												c2=db.SelectAddersPrice("DStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												c4=c1+c2*Convert.ToDecimal(PN.Stroke.Trim());
												c4=Decimal.Round(c4,0);
											}
											if(PN.TandemDuplex !=null && PN.DoubleRod.ToUpper().Trim() =="NO")
											{
												c1=db.SelectAddersPrice("SWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												c2=db.SelectAddersPrice("SStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												c22=db.SelectAddersPrice("DStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												c4=c1*1.75m+(c2+c22)*Convert.ToDecimal(PN.Stroke.Trim());
												c4=Decimal.Round(c4,0);
											}
											if(PN.TandemDuplex !=null && PN.DoubleRod.ToUpper().Trim() =="YES")
											{
												c1=db.SelectAddersPrice("DWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												c22=db.SelectAddersPrice("DStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												c4=c1*1.75m+2m*c22*Convert.ToDecimal(PN.Stroke.Trim());
												c4=Decimal.Round(c4,0);
											}
											//issue weight end update
										}
											//issue #137 start
										else if(PN.Series.Trim()== "AS")
										{
											string strTandem ="";
											if(PN.TandemDuplex=="TC") strTandem="YES";
											else strTandem="NO";
											c4=CylWeight(PN.Series,PN.Bore_Size, PN.Rod_Diamtr, PN.CanisterNo, PN.Stroke,PN.FailMode,strTandem);
										}
											//issue #137 end
										else if(PN.Series.Trim()== "M" || PN.Series.Trim()== "ML" || PN.Series.Trim()== "L")
										{
											if(PN.Bore_Size.Trim() =="P" || PN.Bore_Size.Trim() =="R" || PN.Bore_Size.Trim() =="S" || PN.Bore_Size.Trim() =="T" || PN.Bore_Size.Trim() =="W" || PN.Bore_Size.Trim() =="X")
											{
												if(PN.DoubleRod.ToUpper().Trim() =="YES")
												{
													c1=db.SelectAddersPrice("DWeight","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													c2=db.SelectAddersPrice("DStroke","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										
													if(PN.Mount.Substring(0,2) =="T1" || PN.Mount.Substring(0,2) =="T2")
													{
														c5=db.SelectAddersPrice("SWeight1","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													else if(PN.Mount.Substring(0,2) =="E5" || PN.Mount.Substring(0,2) =="E6" || PN.Mount.Substring(0,2) =="T4")
													{
														c5=db.SelectAddersPrice("SWeight2","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													else if(PN.Mount.Substring(0,2) =="F5" || PN.Mount.Substring(0,2) =="F6" )
													{
														c5=db.SelectAddersPrice("SWeight3","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													else if(PN.Mount.Substring(0,2) =="P1" || PN.Mount.Substring(0,2) =="S2" || PN.Mount.Substring(0,2) =="S3")
													{
														c5=db.SelectAddersPrice("SWeight4","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													else
													{
														c5=db.SelectAddersPrice("SWeight1","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													c3=Convert.ToDecimal(PN.Stroke.Trim());
													c4= c1 + c5 + (c2 * c3);
													c4=Decimal.Round(c4,0);	
												}
												else
												{
													c2=db.SelectAddersPrice("SStroke","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										
													if(PN.Mount.Substring(0,2) =="T1" || PN.Mount.Substring(0,2) =="T2")
													{
														c5=db.SelectAddersPrice("SWeight1","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													else if(PN.Mount.Substring(0,2) =="E5" || PN.Mount.Substring(0,2) =="E6" || PN.Mount.Substring(0,2) =="T4")
													{
														c5=db.SelectAddersPrice("SWeight2","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													else if(PN.Mount.Substring(0,2) =="F5" || PN.Mount.Substring(0,2) =="F6" )
													{
														c5=db.SelectAddersPrice("SWeight3","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													else if(PN.Mount.Substring(0,2) =="P1" || PN.Mount.Substring(0,2) =="S2" || PN.Mount.Substring(0,2) =="S3")
													{
														c5=db.SelectAddersPrice("SWeight4","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													else
													{
														c5=db.SelectAddersPrice("SWeight1","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}	
													c3=Convert.ToDecimal(PN.Stroke.Trim());
													c4=c5 + (c2 * c3);
													c4=Decimal.Round(c4,0);								
												}
											}
											else if(PN.Bore_Size.Trim() !="P" && PN.Bore_Size.Trim() !="R" && PN.Bore_Size.Trim() !="S" && PN.Bore_Size.Trim() !="T" && PN.Bore_Size.Trim() !="W" && PN.Bore_Size.Trim() !="X")
											{
												if( PN.DoubleRod.ToUpper().Trim() =="YES" )
												{
													if(PN.Mount.Substring(0,1) =="P" || PN.Mount.Substring(0,1) =="T" || PN.Mount.Substring(0,1) =="E" || PN.Mount.Substring(0,1) =="S")
													{
														c1=db.SelectAddersPrice("DWeight2","WEB_SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													else
													{
														c1=db.SelectAddersPrice("DWeight1","SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													c2=db.SelectAddersPrice("DStroke","WEB_SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												}
												else
												{
													if(PN.Mount.Substring(0,1) =="P" || PN.Mount.Substring(0,1) =="T" || PN.Mount.Substring(0,1) =="E" || PN.Mount.Substring(0,1) =="S")
													{
														c1=db.SelectAddersPrice("SWeight2","WEB_SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													else
													{
														c1=db.SelectAddersPrice("SWeight1","SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													c2=db.SelectAddersPrice("SStroke","WEB_SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												}
												c3=Convert.ToDecimal(PN.Stroke.Trim());
												c4=c1 + (c2 * c3);
												c4=Decimal.Round(c4,0);
											}
										}
										if(c4 !=0)
										{
											weight="Approximate cylinder weight (does not include accessories)= "+c4.ToString()+" LBS";
											//issue #233 start
											weight +=";The Cylinder Displacement = " + Cyl_Displacement(PN) + " in" + Convert.ToChar(0179).ToString();
											//issue #233 end
										}
										else
										{
											weight="";
										}
										Qitem.Weight=weight.ToString();
									}								
									//insertquote
									string strd="";
									strd= db.InsertItems(Qitem);
									//issue #137 start
									//backup
									if(strd.ToString().Trim() =="1")
										//update
										if(strd.ToString().Trim() =="1" && PN.Series!="AS")
											//issue #137 end
										{
											if(PN.Style !=null)
											{
												if(PN.Style.ToString().Trim() !="")
												{
													Opts opts=new Opts();
													string ff="";
													if(PN.Style.Trim() =="FC")
													{
														ff="Style: Fail Close\r\n";
													}
													else if(PN.Style.Trim() =="FO")
													{
														ff="Style: Fail Open\r\n";
													}
													else
													{
														ff="Style: Double Acting \r\n";
													}										
													if(PN.MinAirSupply !=null)
													{
														ff +="  Min Supply: "+PN.MinAirSupply.ToString().Trim()+" PSI Min\r\n";
													}
													if(cod.Bore.ToString().Trim() =="")
													{												
														if(PN.SeatingTrust !=null)
														{
															ff +="  Requested Thrust : "+PN.SeatingTrust.ToString().Trim()+"\r\n";
														}												
														if(PN.SaftyFactor !=null)
														{
															ff +="  Requested Safety Factor : "+cod.SaftyFactor.ToString().Trim()+"% \r\n";
														}			
														if(Qitem.B_Code.Trim() !="Z" && Qitem.R_Code.Trim() !="Z")
														{
															if(PN.CylinderThrustPush !=null)
															{
																ff +="  Cylinder Thrust Push : "+PN.CylinderThrustPush.ToString().Trim()+"\r\n";
															}
															if(PN.CylinderThrustPull !=null)
															{
																ff +="  Cylinder Thrust Pull : "+PN.CylinderThrustPull.ToString().Trim()+"\r\n";
															}
															if(PN.SafetyFactorPush !=null)
															{
																ff +="  Safety Factor Push : "+PN.SafetyFactorPush.ToString().Trim()+"% \r\n";
															}
															if(PN.SafetyFactorPull !=null)
															{
																ff +="  Safety Factor Pull : "+PN.SafetyFactorPull.ToString().Trim()+"% \r\n";
															}
														}
													}											
													opts.Opt =ff.Trim();
													opts.Code=quot.QuoteNo.Trim();
													string temp=db.Insertcomments(opts);
												}
											}
											Implist.Add(lblstatus.Text.ToString().Trim());								
										}
								}
								catch( Exception ex)
								{
									string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
									s=s.Replace("'"," ");
									LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
								}
							}
						}
						bool pricestatus=true;
						for(int p=0;p<Implist.Count;p++)
						{
							if(Implist[p].ToString().Trim() =="true")
							{
								pricestatus=false;
							}
						}
						if(pricestatus ==true)
						{
							quot.Finish="1";
							string str =db.SelectCustomerQno(quot.QuoteNo.ToString().Trim());
							if(str.ToString() !="")
							{
								string s =db.UpdateCustomerQuote(quot,str.Trim());
							}
							else
							{
								string s =db.InsertCustomerQuote(quot);
							}
							db.UpdateCustomerQuote_UpLoadFile(lblup.Text.Trim(),str.ToString());
							lblstatus.Text="";
							Response.Redirect("ManageQ.aspx?id="+quot.QuoteNo.Trim()+"&pn=9999");		
						}
						else
						{
							PSend.Visible=true;
							Session["Quote"] = quot;
							quot.Finish="0";
							if(TxtProjectNo.Text.Trim() =="")
							{
								quot.Note ="This Quote require assitance from the factory.Please Contact Cowan Dynamics";
							}
							string str =db.SelectCustomerQno(quot.QuoteNo.ToString().Trim());
							if(str.ToString() !="")
							{
								string s =db.UpdateCustomerQuote(quot,str.Trim());
							}
							else       
							{
								string s =db.InsertCustomerQuote(quot);
							}
							db.UpdateCustomerQuote_UpLoadFile(lblup.Text.Trim(),str.ToString());
							lblstatus.Text="";
						}	
						if(hydro ==true)
						{
							Opts opts=new Opts();
							string ff="Pricing is quantity sensitive. Quantities shown to be ordered & shipped at one time.";
							opts.Opt =ff.Trim();
							opts.Code=quot.QuoteNo.Trim();
							string temp=db.Insertcomments(opts);
						}
					}
					else
					{
						LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Please fix the error in the uploaded file !!!')</script>";
					}
				}
				else
				{
					LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Please Upload a file/Please fix the error in the uploaded file !!!')</script>";
				}
			}
			catch( Exception ex)
			{
				lblup.Text="";
				LblUpload.Text="";
				lblpage.Text="";
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ");
				s=s.Replace("'"," ");
				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}

		//issue #137 start
		private decimal CylWeight(string series, string bore,string rod,string mount,string stk,string strFailMode,string tandem)
		{
			decimal c1 = 0;
			decimal c2=0;
			decimal c3=0;
			decimal c4=0;
			decimal cs = 0;
			DBClass db=new DBClass();
			if( series.Trim()== "AS")
			{
				if(tandem.ToUpper().Trim()=="NO")
				{
					c1=db.SelectOneValueByAllinfo("SWeight1","SeriesAs_Weight_TableV1","BoreSize",bore.Trim(),"1","1");
					c2=db.SelectOneValueByAllinfo("SStroke","SeriesAs_Weight_TableV1","BoreSize",bore.Trim(),"1","1");
					cs=db.SelectOneValueByAllinfo("Weight","SeriesAS_Weight_Canister_TableV1","BoreSize",mount.Trim(),"FailMode",strFailMode);
					c4=c1+c2*Convert.ToDecimal(stk.Trim()) +cs;
					c4=Decimal.Round(c4,0);
				}
				if(tandem.ToUpper().Trim()=="YES")
				{
					c1=db.SelectOneValueByAllinfo("DWeight1","SeriesAs_Weight_TableV1","BoreSize",bore.Trim(),"1","1");
					c2=db.SelectOneValueByAllinfo("DStroke","SeriesAs_Weight_TableV1","BoreSize",bore.Trim(),"1","1");
					cs=db.SelectOneValueByAllinfo("Weight","SeriesAS_Weight_Canister_TableV1","BoreSize",mount.Trim(),"FailMode",strFailMode);
					c4=c1+c2*Convert.ToDecimal(stk.Trim()) +cs;
					c4=Decimal.Round(c4,0);
				}
			}
			return c4;
		}
		public decimal ASPricing(coder PN)
		{
			decimal total=0.00m;
			try
			{				
				    DBClass db=new DBClass();	
					string pindex="0";
					//pindex=db.SelectPriceIndex("OEMA").ToString();
					pindex=db.SelectPriceIndex("AS").ToString();
				    string comtable="SeriesAS_OEM_CommAdders_TableV1";//"WEB_SeriesAS_OEM_CommAdders_TableV1";					
					if(PN.TandemDuplex=="TC")
					{
						lblmgrp.Text="ASP_Base";
						lbltable.Text="ASPrice_Tandem_TableV1";//"WEB_ADPrice_Tandem_TableV1"
					}	
					else
					{
						lblmgrp.Text="ASP_Base";
						lbltable.Text="ASPrice_Master_TableV1";//"WEB_APrice_OEM_TableV1"
					}
					decimal strokeprice =0.00m;
					decimal stroke =0.00m;
					decimal mountprice =0.00m;
					decimal sealprice=0.00m;
					decimal ssprodprice=0.00m;
					decimal mrprice=0.00m;
					decimal temp=0;
					decimal index =0.00m;
					index =Convert.ToDecimal(pindex.Trim());
					
					//stroke price 
					stroke =Convert.ToDecimal(PN.Stroke.ToString().Trim());
					//strokeprice =db.SelectStrokeA(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					strokeprice =db.SelectOneValueByAllinfo("ASP_StrokePerInch",lbltable.Text.Trim(),"Bore_Size",PN.Bore_Size.ToString().Trim(),"Rod_Size",PN.Rod_Diamtr.ToString().Trim());
					strokeprice =Decimal.Round(strokeprice,2);				
					temp =stroke * strokeprice;			
					temp =temp + temp * (index /100);
					strokeprice =Decimal.Round(temp,2);
					if(strokeprice ==0)
					{
						lblstatus.Text ="true";
					}

					//mount price		
					temp=0;
					temp =db.SelectMountPrice(lblmgrp.Text.ToString(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					mountprice =temp + temp * (index /100);
					mountprice=Decimal.Round(mountprice,2); 				
					
					if(PN.Mount.Trim().Substring(0,1) =="I" || PN.Mount.Trim().Substring(0,1) =="M" )
					{
						decimal a1=0.00m;
						a1 =Convert.ToDecimal(db.SelectValue("Price","ASprice_IsoMssAdder_TableV1","Mount",PN.Mount.Trim()));
						if(a1 >0)
						{
							a1 =a1 + a1 * (index /100);
							a1 =Decimal.Round(a1,2);
							mountprice +=a1;
						}

					
					}
					
					if(mountprice ==0)
					{
						lblstatus.Text ="true";
					}

					//seal price
					temp=0;
					if(PN.Seal_Comp.ToString().Trim() =="L")
					{
						//temp =db.SelectAddersPrice("SL",comtable.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());		
						
						if(lbltable.Text.Trim() =="ASPrice_Tandem_TableV1")
						{
							temp =db.SelectOneValueByAllinfo("TSL",comtable.Trim(),"BoreSize",PN.Bore_Size.ToString().Trim(),"RodSize",PN.Rod_Diamtr.ToString().Trim());
						}
						else
						{
							temp =db.SelectOneValueByAllinfo("SL",comtable.Trim(),"BoreSize",PN.Bore_Size.ToString().Trim(),"RodSize",PN.Rod_Diamtr.ToString().Trim());
						}
						
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}
					else if(PN.Seal_Comp.ToString().Trim() =="F")
					{
						//temp =db.SelectAddersPrice("SF",comtable.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());		
						temp =db.SelectOneValueByAllinfo("SF",comtable.Trim(),"BoreSize",PN.Bore_Size.ToString().Trim(),"RodSize",PN.Rod_Diamtr.ToString().Trim());
					
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}
					sealprice =temp + temp * (index /100);	
					sealprice=Decimal.Round(sealprice,2);						

					//ssprod price
					//metal rod scrapper price
					//rodend
					decimal rodend=0.00m;
					if(PN.Rod_End.ToString().Substring(0,1) !="N" &&PN.Rod_End.ToString().Trim().Substring(0,1) !="A")
					{
						rodend =db.SelectAddersPrice("MetricRodEnd",comtable.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
						if(rodend ==0)
						{
							lblstatus.Text ="true";
						}
					}
					rodend=Decimal.Round(rodend,2);
				
				    //issue #315 start
				    total=strokeprice + mountprice + sealprice +rodend ;
					lbltotal.Text=total.ToString();
				return total;
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				return total;
			}
		}
		//issue #137 end
		//canisternum 
		private string Create_CanisterDesc(string failmode, string canisterno, string preload, string bto, string eto, string etc , string btc)
		{
			string strFailMode="";
			try
			{
				switch(failmode)
				{
					case "FC":		
						//strFailMode="CAN" + canisterno + failmode+ preload +"; " + "Fail Close. ETC= "+preload+" lbs, BTC= "+btc+" lbs ; BTO= " +bto +" lbs, ETO= "+eto+" lbs";
						strFailMode="CAN" + canisterno + failmode.Replace("FO","O").Replace("FC","C")+ preload +"; " + "Fail Close. ETC= "+preload+" lbs, BTC= "+btc+" lbs ; BTO= " +bto +" lbs, ETO= "+eto+" lbs";
						break;
					case "FO":
						//strFailMode="CAN" + canisterno + failmode+ preload +"; " + "Fail Open. ETO= "+eto +" lbs, BTO= " +bto +" lbs; ETC= " +etc+" lbs, BTC= "+btc+" lbs";
						strFailMode="CAN" + canisterno + failmode.Replace("FO","O").Replace("FC","C")+ preload +"; " + "Fail Open. ETO= "+eto +" lbs, BTO= " +bto +" lbs; ETC= " +etc+" lbs, BTC= "+btc+" lbs";
						break;
				}
			}
			catch{}
			return strFailMode;
		}
		protected void LBSend_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(Page.IsValid)
				{
					DBClass db=new DBClass();
					ArrayList lst =new ArrayList();
					lst =(ArrayList)Session["User"];
					Quotation quote=new Quotation();
					quote =(Quotation)Session["Quote"];
					ArrayList lst1 =new ArrayList();
					lst1 =(ArrayList)Session["User"];
                    ArrayList cclist = new ArrayList();

                    string to = "";
                    if (quote.CompanyID.ToString().Trim() == "1015")
                    {
                        to = "jenny.l@cowandynamics.com";
                        cclist.Add("dtaranu@cowandynamics.com");
                        cclist.Add("jbehara@cowandynamics.com");
                    }                    
                    else if (quote.CompanyID.ToString().Trim() == "1002" || quote.CompanyID.ToString().Trim() == "1021")
                    //issue #118 end
                    {
                        to = "dtaranu@cowandynamics.com";
                        cclist.Add("jenny.l@cowandynamics.com");
                        cclist.Add("jbehara@cowandynamics.com");
                    }
                    else
                    {
                        to = "jenny.l@cowandynamics.com";
                        cclist.Add("dtaranu@cowandynamics.com");
                        cclist.Add("jbehara@cowandynamics.com");
                    }

                    string body =
                        "<hr color='#FF0000'>Bonjour, <br> " + lst1[0].ToString() + " a une nouvelle demande de prix:<br>"
                        + "<p><TABLE id='Table1' borderColor='#0000ff' cellSpacing='1' cellPadding='1' align='left' border='1'>"
                        + "<TR><TD noWrap>Date de demande :</TD><TD noWrap>" + quote.Quotedate.Trim() + "</TD></TR>"
                        + "<TR><TD noWrap>No de demande :</TD><TD noWrap>" + quote.QuoteNo.ToString().Trim() + "</TD></TR>"
                        + "<TR><TD noWrap>Special Request Note :</TD><TD noWrap>" + TxtSpecialReq.Text.Trim() + "</TD></TR></TABLE></p>"
                        + "<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>Clic sur le lien ci-dessous � r�pondre: <a href='http://172.16.0.252/'>Quote System</a>"
                        + "<br><br><hr> Merci <br>I-Cylinder<br><hr color='#FF0000'><br>"
                        + "<hr color='#FF0000'>Hi, <br> " + lst1[0].ToString() + " has entered a new request for a quote:<br>"
                        + "<p><TABLE id='Table1' borderColor='#0000ff' cellSpacing='1' cellPadding='1' align='left' border='1'>"
                        + "<TR><TD noWrap>Quote Date :</TD><TD noWrap>" + quote.Quotedate.Trim() + "</TD></TR>"
                        + "<TR><TD noWrap>Quote No :</TD><TD noWrap>" + quote.QuoteNo.ToString().Trim() + "</TD></TR>"
                        + "<TR><TD noWrap>Special Request Note :</TD><TD style='width:200' noWrap>" + TxtSpecialReq.Text.Trim() + "</TD></TR></TABLE></p>"
                        + "<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>Please log into <a href='http://172.16.0.252/'>Quote System</a> to complete the Quote."
                        + "<br><br><hr> Thanks <br>I-Cylinder<br><hr color='#FF0000'>"
                        + MailService.Get_Admin();
                    string attachment = "";
                    if (lblspecsheet.Text.Trim() != "")
                    {
                        attachment = lblspecsheet.Text.ToString().Trim();
                    }
                    MailService.SendMail(
                              "dtaranu@cowandynamics.com",
                              cclist,
                              "[" + DDLPriority.SelectedItem.Text.Trim() + "] New Quote from " + quote.Customer.Trim(),
                              body,
                              attachment,
                              DDLPriority.SelectedIndex == 1
                              );
					db.Insert_NewRFQ_ICYL(lst[0].ToString().Trim(),quote.QuoteNo.ToString(),quote.Customer.ToString()
						,lst[0].ToString().Trim(),TxtSpecialReq.Text.ToString(),"",DateTime.Today.ToShortDateString(),DateTime.Today.AddDays(0).ToShortDateString()
						,"Select","0","1.00");
					Response.Redirect("Result.aspx?id="+quote.QuoteNo.Trim());
				}
			}
			catch (Exception ex)
			{
				lblerr.Text=ex.Message;
			}
		}
		public string Qno(string use)
		{
			try
			{
				DBClass db=new DBClass();
				string qno ="";
				string count = "";
				string usr=use.ToUpper();
				string st= usr.Substring(0,1);
				string s1=DateTime.Today.Month.ToString();
				count=db.SelectValue("QuoteNo","WEB_QuoteCount_TableV1","SLNo","1");
				if(count.Length !=0)
				{
					int lst=Convert.ToInt32(count.Substring(5));
				
				
					string s=DateTime.Today.Month.ToString();
					if(s.Length ==1)
					{
						s="0"+DateTime.Today.Month.ToString();
					}
					else
					{
						s=DateTime.Today.Month.ToString();
					}
					if(count !="")
					{
						if(count.Substring(1,2).Equals(DateTime.Today.Year.ToString().Substring(2)))
						{
							if(count.Substring(3,2).Equals(s))
							{
								lst++;
								string num="";
								if(lst.ToString().Length ==4)
								{
									num=lst.ToString();
								}
								else if(lst.ToString().Length ==3)
								{
									num="0"+lst.ToString();
								}
								else if(lst.ToString().Length ==2)
								{
									num="00"+lst.ToString();
								}
								else 
								{
									num="000"+lst.ToString();
								}
							
								if(s1.Length ==1)
								{
									s1="0"+DateTime.Today.Month.ToString();
								}
								else
								{
									s1=DateTime.Today.Month.ToString();
								}
								qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+num.ToString();
							}
							else
							{
						
								if(s1.Length ==1)
								{
									s1="0"+DateTime.Today.Month.ToString();
								}
								else
								{
									s1=DateTime.Today.Month.ToString();
								}
								qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+"0001";
							}
	
						}
						else
						{
						
							if(s1.Length ==1)
							{
								s1="0"+DateTime.Today.Month.ToString();
							}
							else
							{
								s1=DateTime.Today.Month.ToString();
							}
							qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+"0001";
						}
					}
					else
					{
					
						if(s1.Length ==1)
						{
							s1="0"+DateTime.Today.Month.ToString();
						}
						else
						{
							s1=DateTime.Today.Month.ToString();
						}
						qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+"0001";
					}
				}
				else
				{
					qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+"0001";
				}
				string sav=db.InsertQuoteNo(qno);
				db.InsertQuoteCount(qno);
				return qno;
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				return null;
			}		
		}
		
		public decimal APricing(coder PN,bool tandem)
		{
			decimal total=0.00m;
			try
			{				
				if(PN.Style.ToString().Trim()=="N")
				{
					DBClass db=new DBClass();	
					string pindex="0";
					pindex=db.SelectPriceIndex("OEMA").ToString();
					string comtable="WEB_SeriesA_OEM_CommAdders_TableV1";					
					if(tandem == true)
					{
						lblmgrp.Text="AP_Base";
						lbltable.Text="WEB_ADPrice_Tandem_TableV1";
					}	
					else
					{
						lblmgrp.Text="AP_Base";
						lbltable.Text="WEB_APrice_OEM_TableV1";
					}
					decimal strokeprice =0.00m;
					decimal stroke =0.00m;
					decimal mountprice =0.00m;
					decimal sealprice=0.00m;
					decimal ssprodprice=0.00m;
					decimal mrprice=0.00m;
					decimal temp=0;
					decimal index =0.00m;
					index =Convert.ToDecimal(pindex.Trim());
					
					//stroke price 
					stroke =Convert.ToDecimal(PN.Stroke.ToString().Trim());
					strokeprice =db.SelectStrokeA(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					strokeprice =Decimal.Round(strokeprice,2);				
					temp =stroke * strokeprice;			
					temp =temp + temp * (index /100);
					strokeprice =Decimal.Round(temp,2);
					if(strokeprice ==0)
					{
						lblstatus.Text ="true";
					}

					//mount price		
					temp=0;
					temp =db.SelectMountPrice(lblmgrp.Text.ToString(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					mountprice =temp + temp * (index /100);
					mountprice=Decimal.Round(mountprice,2); 				
					if(mountprice ==0)
					{
						lblstatus.Text ="true";
					}

					//seal price
					temp=0;
					if(PN.Seal_Comp.ToString().Trim() =="L")
					{
						temp =db.SelectAddersPrice("SL",comtable.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());		
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}
					else if(PN.Seal_Comp.ToString().Trim() =="F")
					{
						temp =db.SelectAddersPrice("SF",comtable.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());		
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}
					sealprice =temp + temp * (index /100);	
					sealprice=Decimal.Round(sealprice,2);						

					//ssprod price
					temp=0;
					if(PN.SSPistionRod.ToString().Trim() =="M3")
					{
						decimal t1=0;
						decimal t2=0;
						t1 =db.SelectAddersPrice("M3Base",comtable.Trim(),PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
						t2 =db.SelectAddersPrice("M3PerInch",comtable.Trim(),PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
						temp =t1 + (t2 * stroke); 	
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}		
					ssprodprice =temp + temp * (index /100);	
					ssprodprice=Decimal.Round(ssprodprice,2);					

					//metal rod scrapper price
					temp=0;
					if(PN.MetalScrapper.ToString().Trim() =="GR2")
					{
						temp =db.SelectAddersPrice("GR2",comtable.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());		
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}				
					mrprice =temp + temp * (index /100);	
					mrprice=Decimal.Round(mrprice,2);	

					//issue #315 start
					total=strokeprice + mountprice + sealprice +ssprodprice +mrprice ;
					lbltotal.Text=total.ToString();
				}
				else
				{
					lbltotal.Text="0.00";
				}
				return total;
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				return total;
			}
		}
		public decimal ACPricing(coder PN,bool tandem)
		{
			decimal total=0.00m;
			try
			{				
				if(PN.Style.ToString().Trim()=="N")
				{
					DBClass db=new DBClass();	
					string pindex="0";
					pindex=db.SelectPriceIndex("AC").ToString();
					string comtable="WEB_SeriesAC_CommAdders_TableV1";					
					if(tandem == true)
					{
						lblmgrp.Text="AP_Base";
						lbltable.Text="WEB_ACPrice_Tandem_TableV1";
					}	
					else
					{
						lblmgrp.Text="AP_Base";
						lbltable.Text="WEB_ACPrice_Master_TableV1";
					}
					decimal strokeprice =0.00m;
					decimal stroke =0.00m;
					decimal mountprice =0.00m;
					decimal sealprice=0.00m;
					decimal barrelprice=0.00m;
					decimal mrprice=0;
					decimal temp=0;
					decimal index =0.00m;
					index =Convert.ToDecimal(pindex.Trim());
					
					//stroke price 
					stroke =Convert.ToDecimal(PN.Stroke.ToString().Trim());
					strokeprice =db.SelectStrokeA(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					strokeprice =Decimal.Round(strokeprice,2);				
					temp =stroke * strokeprice;			
					temp =temp + temp * (index /100);
					strokeprice =Decimal.Round(temp,2);
					if(strokeprice ==0)
					{
						lblstatus.Text ="true";
					}

					//mount price		
					temp=0;
					temp =db.SelectMountPrice(lblmgrp.Text.ToString(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					mountprice =temp + temp * (index /100);
					mountprice=Decimal.Round(mountprice,2); 				
					if(mountprice ==0)
					{
						lblstatus.Text ="true";
					}

					//seal price
					temp=0;
					if(PN.Seal_Comp.ToString().Trim() =="L")
					{
						temp =db.SelectAddersPrice("SL",comtable.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());		
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}
					else if(PN.Seal_Comp.ToString().Trim() =="F")
					{
						temp =db.SelectAddersPrice("SF",comtable.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());	
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}
					temp =temp + temp * (index /100);	
					sealprice=Decimal.Round(temp,2);					

					//ss barrel price
					temp=0;
					if(PN.CarbonFibBarrel.ToString().Trim() =="M8")
					{
						decimal t1=0;
						decimal t2=0;
						t1 =db.SelectAddersPrice("M8Base",comtable.Trim(),PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
						t2 =db.SelectAddersPrice("M8PerInch",comtable.Trim(),PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
						temp =t1 + (t2 * stroke); 
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}		
					temp =temp + temp * (index /100);	
					barrelprice=Decimal.Round(temp,2);	
					
					//metal rod scrapper price
					temp=0;
					if(PN.MetalScrapper.ToString().Trim() =="GR2")
					{
						temp =db.SelectAddersPrice("GR2",comtable.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());	
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}				
					mrprice =temp + temp * (index /100);	
					mrprice=Decimal.Round(mrprice,2);						
					
					total=strokeprice + mountprice + sealprice + barrelprice + mrprice;
					lbltotal.Text=total.ToString();
				}
				else
				{
					lbltotal.Text="0.00";
				}
				return total;
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				return total;
			}
		}		
		public decimal MLPricing(coder PN)
		{
			decimal total=0.00m;
			try
			{				
				if(PN.Style.ToString().Trim()=="N")
				{
					DBClass db=new DBClass();	
					string pindex="0";
					pindex=db.SelectPriceIndex("ML").ToString();
					string comtable="WEB_SeriesMLCommAdders_TableV1";					
					lblmgrp.Text="MLSMount_Grp";
					lbltable.Text="WEB_MLPrice_Master_TableV1";
				
					decimal strokeprice =0.00m;
					decimal stroke =0.00m;
					decimal mountprice =0.00m;
					decimal cushionprice =0.00m;
					decimal sealprice=0.00m;
					decimal ssprodprice=0.00m;
					decimal mrprice=0;
					decimal temp=0;
					decimal index =0.00m;
					if(PN.Bore_Size.ToString().Trim()=="C" || PN.Bore_Size.ToString().Trim()=="D" || PN.Bore_Size.ToString().Trim()=="E")
					{			
						index =Convert.ToDecimal(db.SelectPriceIndex("MLS").ToString());
					}
					else
					{
						index =Convert.ToDecimal(db.SelectPriceIndex("ML").ToString());
					}

					decimal discount =0.00m;
					decimal qty=0;
					qty=Convert.ToDecimal(PN.Qty.ToString());
					if(qty >0 && qty <=2)
					{
						discount=35;
					}
					else if(qty >=3 && qty <=5)
					{
						discount=37;
					}
					else if(qty >=6 && qty <=10)
					{
						discount=40;
					}
					else if(qty >10)
					{
						discount=45;
					}

					//stroke price 
					stroke =Convert.ToDecimal(PN.Stroke.ToString().Trim());
					strokeprice =db.SelectStrokePriceML(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					strokeprice =Decimal.Round(strokeprice,2);				
					temp =stroke * strokeprice;			
					temp =temp + temp * (index /100);
					temp =temp *(1 - (discount/100)); 
					strokeprice =Decimal.Round(temp,2);
					if(strokeprice ==0)
					{
						lblstatus.Text ="true";
					}

					//mount price		
					temp=0;
					string mgrp ="";
					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
					temp =db.SelectMountPriceML(mgrp.ToString(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					temp =temp + temp * (index /100);
					temp =temp *(1 - (discount/100)); 
					mountprice=Decimal.Round(temp,2); 				
					if(mountprice ==0)
					{
						lblstatus.Text ="true";
					}

					//cushion price
					temp=0;					
					temp =db.SelectCushionPriceML(lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());						
					temp =temp + temp * (index /100);	
					if(PN.Cushions.ToString() =="5")
					{
						temp =temp * 2;
						temp = temp * (1 - (discount/100));		
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}
					else if(PN.Cushions.ToString() =="8")
					{
						temp=0;
					}
					else
					{
						temp = temp*(1 - (discount/100)); 	
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}			
					cushionprice =Decimal.Round(temp,2);					

					//seal price
					temp=0;
//					if(PN.Seal_Comp.ToString().Trim() =="L")
//					{
//						temp =db.SelectAddersPrice("SL",comtable.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());		
//						if(temp ==0)
//						{
//							lblstatus.Text ="true";
//						}
//					}
					if(PN.Seal_Comp.ToString().Trim() =="F")
					{
						temp =db.SelectAddersPrice("SF",comtable.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}
					temp =temp + temp * (index /100);	
					sealprice=Decimal.Round(temp,2);	
					
					//ssprod price
					temp=0;
					if(PN.SSPistionRod.ToString().Trim() =="M4")
					{
						decimal t1=0;
						decimal t2=0;
						t1 =db.SelectAddersPrice("M4Base",comtable.Trim(),PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
						t2 =db.SelectAddersPrice("M4PerInch",comtable.Trim(),PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
						temp =t1 + (t2 * stroke); 			
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}		
					temp =temp + temp * (index /100);	
					ssprodprice=Decimal.Round(temp,2);						
					
					//metal rod scrapper price
					temp=0;
					if(PN.MetalScrapper.ToString().Trim() =="GR2")
					{
						temp =db.SelectAddersPrice("GR2",comtable.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());			
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}				
					mrprice =temp + temp * (index /100);	
					mrprice=Decimal.Round(mrprice,2);	
					
					total=strokeprice + mountprice + cushionprice + sealprice + ssprodprice + mrprice;
					lbltotal.Text=total.ToString();
				}
				else
				{
					lbltotal.Text="0.00";
				}
				return total;
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s=s.Replace("'"," ");
				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				return total;
			}
		}
		public decimal LPricing(coder PN)
		{
			decimal total=0.00m;
			try
			{				
				if(PN.Style.ToString().Trim()=="N")
				{
					DBClass db=new DBClass();	
					string pindex="0";
					pindex=db.SelectPriceIndex("L").ToString();
					string comtable="WEB_SeriesLCommAdders_TableV1";					
					lblmgrp.Text="LMount_Grp";
					lbltable.Text="WEB_LPrice_Master_TableV1";
				
					decimal strokeprice =0.00m;
					decimal stroke =0.00m;
					decimal mountprice =0.00m;
					decimal sealprice=0.00m;
					decimal mrprice=0;
					decimal temp=0;
					decimal index =0.00m;
					index =Convert.ToDecimal(pindex.Trim());

					decimal discount =0.00m;
					decimal qty=0;
					qty=Convert.ToDecimal(PN.Qty.ToString());
					if(qty >0 && qty <=2)
					{
						discount=35;
					}
					else if(qty >=3 && qty <=5)
					{
						discount=37;
					}
					else if(qty >=6 && qty <=10)
					{
						discount=40;
					}
					else if(qty >10)
					{
						discount=45;
					}

					//stroke price 
					stroke =Convert.ToDecimal(PN.Stroke.ToString().Trim());
					strokeprice =db.SelectStrokePriceL(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					strokeprice =Decimal.Round(strokeprice,2);				
					temp =stroke * strokeprice;			
					temp =temp + temp * (index /100);
					temp =temp *(1 - (discount/100)); 
					strokeprice =Decimal.Round(temp,2);
					if(strokeprice ==0)
					{
						lblstatus.Text ="true";
					}

					//mount price		
					temp=0;
					string mgrp ="";
					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
					temp =db.SelectMountPrice(mgrp.ToString(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					temp =temp + temp * (index /100);
					temp =temp *(1 - (discount/100)); 
					mountprice=Decimal.Round(temp,2); 				
					if(mountprice ==0)
					{
						lblstatus.Text ="true";
					}
				
					//seal price
					temp=0;
//					if(PN.Seal_Comp.ToString().Trim() =="L")
//					{
//						temp =db.SelectAddersPrice("SL",comtable.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());		
//						if(temp ==0)
//						{
//							lblstatus.Text ="true";
//						}
//					}
					if(PN.Seal_Comp.ToString().Trim() =="F")
					{
						temp =db.SelectAddersPrice("SF",comtable.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());	
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}
					temp =temp + temp * (index /100);	
					sealprice=Decimal.Round(temp,2);								

					//metal rod scrapper price
					temp=0;
					if(PN.MetalScrapper.ToString().Trim() =="GR2")
					{
						temp =db.SelectAddersPrice("GR2",comtable.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());	
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}				
					mrprice =temp + temp * (index /100);	
					mrprice=Decimal.Round(mrprice,2);	
					
					total=strokeprice + mountprice + sealprice+mrprice ;
					lbltotal.Text=total.ToString();
				}
				else
				{
					lbltotal.Text="0.00";
				}
				return total;
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				return total;
			}
		}
		public decimal ATPricing(coder PN)
		{
			decimal total=0.00m;
			try
			{				
				if(PN.Style.ToString().Trim()=="N")
				{
					DBClass db=new DBClass();	
					string pindex="0";
					pindex=db.SelectPriceIndex("AT").ToString();
					lblmgrp.Text="AP_Base";
					lbltable.Text="WEB_ATPrice_SVC_TableV1";
					string comtable="WEB_SVCSeriesATCommAdders_TableV1";

					decimal strokeprice =0.00m;
					decimal stroke =0.00m;
					decimal mountprice =0.00m;
					decimal sealprice=0.00m;
					decimal ssprice=0.00m;
					decimal sscarbonprice=0.00m;
					decimal mrprice=0;
					decimal temp=0;
					decimal index =0.00m;
					index =Convert.ToDecimal(pindex.Trim());
					
					//stroke price 
					stroke =Convert.ToDecimal(PN.Stroke.ToString().Trim());
					strokeprice =db.SelectStrokeA(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					strokeprice =Decimal.Round(strokeprice,2);				
					temp =stroke * strokeprice;			
					temp =temp + temp * (index /100);
					strokeprice =Decimal.Round(temp,2);
					if(strokeprice ==0)
					{
						lblstatus.Text ="true";
					}

					//mount price		
					temp=0;
					temp =db.SelectMountPrice(lblmgrp.Text.ToString(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					mountprice =temp + temp * (index /100);
					mountprice=Decimal.Round(mountprice,2); 				
					if(mountprice ==0)
					{
						lblstatus.Text ="true";
					}

					//seal price
					temp=0;
					if(PN.Seal_Comp.ToString().Trim() =="L")
					{
						temp =db.SelectAddersPrice("SL",comtable.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());	
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}
					else if(PN.Seal_Comp.ToString().Trim() =="F")
					{
						temp =db.SelectAddersPrice("SF",comtable.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());	
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}
					sealprice =temp + temp * (index /100);	
					sealprice=Decimal.Round(sealprice,2);					

					//all ss price
					temp=0;
					if(PN.AllSS.ToString().Trim() =="C5")
					{
						temp =0; 
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}		
					ssprice =temp + temp * (index /100);	
					ssprice=Decimal.Round(ssprice,2);						

					//all ss carbon price
					temp=0;
					if(PN.AllSS.ToString().Trim() =="C6")
					{
						temp =0;	
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}		
					sscarbonprice =temp + temp * (index /100);	
					sscarbonprice=Decimal.Round(sscarbonprice,2);						

					//metal rod scrapper price
					temp=0;
					if(PN.MetalScrapper.ToString().Trim() =="GR2")
					{
						temp =db.SelectAddersPrice("GR2",comtable.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());	
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}				
					mrprice =temp + temp * (index /100);	
					mrprice=Decimal.Round(mrprice,2);						

					total=strokeprice + mountprice + sealprice +ssprice +sscarbonprice +mrprice;
					lbltotal.Text=total.ToString();
				}
				else
				{
					lbltotal.Text="0.00";
				}
				return total;
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				return total;
			}
		}

		//		public void MLPricin1g(coder PN)
//		{
//			try
//			{
//				DBClass db=new DBClass();
//				lblmgrp.Text="MLSMount_Grp";
//				lbltable.Text="WEB_MLPrice_Master_TableV1";
//				if(PN.DoubleRod.ToString().Trim()=="Yes")
//				{
//					lblmgrp.Text="MLDMount_Grp";
//					lbltable.Text="WEB_MLDPrice_Master_TableV1";						
//				}
//				else
//				{
//					lblmgrp.Text="MLSMount_Grp";
//					lbltable.Text="WEB_MLPrice_Master_TableV1";
//				}
//				//stroke price
//				decimal sp =0.00m;
//				decimal st =0.00m;
//				decimal sprice =0.00m;
//				decimal mprice =0.00m;
//				decimal cprice =0.00m;
//				decimal seal=0.00m;
//				decimal rodend=0.00m;
//
//				st =Convert.ToDecimal(PN.Stroke);
//				sp =db.SelectStrokePriceML(lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
//				sp =Decimal.Round(sp,2);
//				decimal temp =sp * st;
//				decimal index =0.00m;
//				index =Convert.ToDecimal(db.SelectPriceIndex("ML").ToString());
//				temp =temp + temp * (index /100);
//				decimal discount =0.00m;
//				ArrayList lst=new ArrayList();
//				lst=(ArrayList)Session["User"];
//				string disco=db.SelectDiscount(lst[1].ToString().Trim(),"ML");
//				discount=Convert.ToDecimal(disco.ToString());
//				sprice =temp*(1 - (discount/100)); 
//				sprice =Decimal.Round(sprice,2);
//				if(sprice ==0)
//				{
//					lblstatus.Text ="True";
//				}
//				//mount price
//				string mgrp ="";
//				decimal mp =0.00m;
//				try
//				{
//					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
//					mp =db.SelectMountPriceML(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
//				}
//				catch(Exception ex) 
//				{
//					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
//				}
//				mp =mp + mp * (index /100);
//				mp= mp*(1 - (discount/100)); 
//				mprice = Decimal.Round(mp,2);
//				if(mprice ==0)
//				{
//					lblstatus.Text ="True";
//				}
//				// cushin price
//				decimal cp =0.00m;
//				try
//				{
//					cp =db.SelectCushionPriceML(lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());					
//				}
//				catch(Exception ex) 
//				{
//					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
//				}
//				cp =cp + cp * (index /100);	
//				if(PN.Cushions.ToString() =="5")
//				{
//					decimal temp1 =cp * 2;
//					temp1 = temp1*(1 - (discount/100));
//					cprice =Decimal.Round(temp1,2);
//					if(cprice ==0)
//					{
//						lblstatus.Text ="True";
//					}					
//				}
//				else if(PN.Cushions.ToString() =="8")
//				{
//					cprice =0.00m;
//					cprice =Decimal.Round(cprice,2);
//				}
//				else
//				{
//					cprice=cp;
//					cprice = cprice*(1 - (discount/100)); 
//					cprice =Decimal.Round(cprice,2);
//					if(cprice ==0)
//					{
//						lblstatus.Text ="True";
//					}
//				}			
//				cprice =Decimal.Round(cprice,2);				
//				if(PN.Seal_Comp.ToString().Trim() =="L")
//				{
//					seal =db.SelectAddersPrice("SL","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
//					if(seal ==0)
//					{
//						lblstatus.Text ="True";
//					}				
//				}
//				else if(PN.Seal_Comp.ToString().Trim() =="F")
//				{
//					seal =db.SelectAddersPrice("SF","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
//					if(seal ==0)
//					{
//						lblstatus.Text ="True";
//					}
//				}
//				else if(PN.Seal_Comp.ToString().Trim()=="W")
//				{
//					seal =db.SelectAddersPrice("SW","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
//					if(seal ==0)
//					{
//						lblstatus.Text ="True";
//					}
//				}
//				else if(PN.Seal_Comp.ToString().Trim() =="E")
//				{
//					seal =db.SelectAddersPrice("SE","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
//					if(seal ==0)
//					{
//						lblstatus.Text ="True";
//					}
//				}
//				seal = Decimal.Round(seal,2);
//				if(PN.Rod_End.Trim().Substring(0,1) !="N" && PN.Rod_End.Trim().Substring(0,1) !="A")
//				{
//					rodend =db.SelectAddersPrice("MetricRodEnd","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
//					if(rodend ==0)
//					{
//						lblstatus.Text ="True";
//					}
//				}
//				seal =seal + seal * (index /100);
//				rodend =rodend + rodend * (index /100);
//				decimal total=0.00m;
//				total=sprice + mprice +cprice +seal + rodend ;
//				lbltotal.Text=total.ToString();
//			}
//			catch(Exception ex)
//			{
//				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
//				s.Replace("'"," ");
//				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
//			}				
//		}

		public static bool IsNumeric(string strInteger) 
		{
			try 
			{
				if(strInteger.Trim() =="")
				{
					return false;
				}
				else
				{
					int intTemp =0;
					if(strInteger.ToString().StartsWith(".") == true)
					{
						for(int i=1; i< strInteger.Length;i++)
						{
							intTemp = Int32.Parse( strInteger.Substring(i,1) );
						}
					}
					else
					{
						for(int i=0; i< strInteger.Length;i++)
						{
							if (strInteger.ToString().Substring(i,1) !=".")
							{
								intTemp = Int32.Parse( strInteger.Substring(i,1) );
							}
						}
					}
					return true;
				}				
			} 
			catch (FormatException) 
			{
				return false;
			}    
		}
		private string UploadFileSpec(object Sender,EventArgs E)
		{
			string file="";
			if (FileUploadSpec.PostedFile !=null) //Checking for valid file
			{	
				string name=DateTime.Today.ToShortDateString().Replace("/","").Replace(" ","");
				name += DateTime.Now.Ticks.ToString();//.Replace("/","").Replace(" ","").Replace(":","").Replace("","");
				string StrFileName = name+".pdf" ;
				int IntFileSize =FileUploadSpec.PostedFile.ContentLength;
				if (IntFileSize <=0)
				{
					lblpage.Text="Uploading of file " + StrFileName + " failed ";
				}
				else
				{
					FileUploadSpec.PostedFile.SaveAs(Server.MapPath("./specsheet/" + StrFileName.Trim()));
					file=StrFileName.ToString();
				}
			}
			return file;
		}
	}
}
