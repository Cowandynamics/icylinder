using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for EV_SeriesA_Specials.
	/// </summary>
	public partial class EV_SeriesA_Specials : System.Web.UI.Page
	{
		coder cd1=new coder();
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["User"] !=null)
			{
				if(! IsPostBack)
				{
					PanelTB.Enabled =true;
					lblpage.Visible=false;
					lblhidden.Text="";
					lblclassi.Visible=false;
					TxtClassification.Visible=false;
					
					if(Session["Coder"] !=null)
					{
						cd1=new coder();
						cd1 =(coder)Session["Coder"];
						if(cd1.Style.ToString().Trim() =="N")
						{
							ListItem li=new ListItem();
							li=new ListItem("Select Type","Select Type");
							DDLSVType.Items.Add(li);
							li=new ListItem("S1","General Purpose 5 Way 2 pos solenoid valve with 12 VDC coil. Energize coil to close valve");
							DDLSVType.Items.Add(li);
							li=new ListItem("S2","General Purpose 5 Way 2 pos solenoid valve with 120 VAC coil. Energize coil to close valve");
							DDLSVType.Items.Add(li);
							li=new ListItem("S3","Asco General Purpose 4 Way 2 pos solenoid valve with 120 VAC coil. Energize coil to close valve");
							DDLSVType.Items.Add(li);
							li=new ListItem("S4","Stainless Steel Solenoid Valve");
							DDLSVType.Items.Add(li);
							li=new ListItem("S5","Hazardous Area Solenoid Valve");
							DDLSVType.Items.Add(li);
						}
						else
						{
							ListItem li=new ListItem();
							li=new ListItem("Select Type","Select Type");
							DDLSVType.Items.Add(li);
							li=new ListItem("FS1","General Purpose 3 Way 2 pos solenoid valve with 12 VDC coil. Energize coil to close valve");
							DDLSVType.Items.Add(li);
							li=new ListItem("FS2","General Purpose 3 Way 2 pos solenoid valve with 120 VAC coil. Energize coil to close valve");
							DDLSVType.Items.Add(li);
							li=new ListItem("FS3","Asco General Purpose 3 Way 2 pos solenoid valve with 120 VAC coil. Energize coil to close valve");
							DDLSVType.Items.Add(li);
							li=new ListItem("FS4","Stainless Steel Solenoid Valve");
							DDLSVType.Items.Add(li);
							li=new ListItem("FS5","Hazardous Area Solenoid Valve");
							DDLSVType.Items.Add(li);
						}
						RBLPSOption.SelectedIndex=0;
						RBLPSOption_SelectedIndexChanged(sender,e);
						if(cd1.ManualOverride !=null)
						{
							if(cd1.ManualOverride.Trim() !="")
							{
								RBLMO.SelectedValue=cd1.ManualOverride.Trim();
							}
						}
						if(cd1.ReedSwitch !=null)
						{
							if(cd1.ReedSwitch.Trim() !="")
							{
								RBLRS.SelectedValue=cd1.ReedSwitch.Trim();
							}
						}
						if(cd1.Positioner !=null)
						{
							if(cd1.Positioner.Trim() !="")
							{
								RBLPSOption.SelectedIndex=1;
								RBLPSOption_SelectedIndexChanged(sender,e);
								TxtPPartNo.Text=cd1.PositionerPno.ToString().Trim();
							}
						}
						if(cd1.FilterRegulator !=null)
						{
							if(cd1.FilterRegulator.Trim() !="")
							{
								RBLFR.SelectedValue=cd1.FilterRegulator.Trim();
								RBLFR_SelectedIndexChanged(sender,e);
							}
						}
						if(cd1.Solenoid !=null)
						{
							if(cd1.Solenoid.Trim() !="")
							{
								DBClass db=new DBClass();
								RBLPSOption.SelectedIndex=0;
								RBLPSOption_SelectedIndexChanged(sender,e);
								ListItem li=new ListItem();
								li.Value=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.Solenoid.Trim());
								li.Text=cd1.Solenoid.Trim();
								DDLSVType.SelectedValue =li.Value;
								DDLSVType_SelectedIndexChanged(sender,e);
								TxtClassification.Text=cd1.SolenoidClass.ToString().Trim();			
							}
							else
							{
								if(cd1.Positioner.ToString().Trim() =="")
								{
									RBLPSOption.SelectedIndex=2;
									RBLPSOption_SelectedIndexChanged(sender,e);
								}
							}
						}
						 
						if(cd1.FlowControl !=null)
						{
							if(cd1.FlowControl.Trim() !="")
							{
								RBLFC.SelectedValue=cd1.FlowControl.Trim();
								RBLFC_SelectedIndexChanged(sender,e);
							}
						}
						if(cd1.Tubing !=null)
						{
							if(cd1.Tubing.Trim() !="")
							{
								RBLTubing.SelectedValue=cd1.Tubing.Trim();
							}
						}
					}
					
				}
			}
			else
			{
				Response.Redirect("Login.aspx");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void RBLPSOption_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lblclassi.Visible=false;
			TxtClassification.Visible=false;
			LblSVDesc.Text="";
			if(RBLPSOption.SelectedIndex ==0)
			{
				PanelPr.Enabled=false;
				PanelSV.Enabled=true;
				PanelTB.Enabled=true;
				PanelFC.Enabled=true;
				PanelFR.Enabled=true;
				RBLFR.SelectedIndex=2;
				lblppartno.Visible=false;
				TxtPPartNo.Visible=false;
				RequiredFieldValidator1.Visible=false;
				DDLSVType.SelectedIndex= 1;
				DDLSVType_SelectedIndexChanged(sender,e);
			}
			else if(RBLPSOption.SelectedIndex ==1)
			{
				PanelPr.Enabled=true;
				PanelSV.Enabled=false;
				PanelTB.Enabled=true;
				PanelFC.Enabled=false;
				PanelFR.Enabled=true;
				RBLFC.SelectedIndex=2;
				DDLSVType.SelectedIndex=0;
				DDLSVType_SelectedIndexChanged(sender,e);
				lblppartno.Visible=true;
				TxtPPartNo.Visible=true;
				RequiredFieldValidator1.Visible=true;
				RBLPositioner.SelectedIndex=0;
			}
			else if(RBLPSOption.SelectedIndex ==2)
			{
				PanelPr.Enabled=false;
				PanelSV.Enabled=false;
				PanelTB.Enabled=false;
				PanelFC.Enabled=false;
				PanelFR.Enabled=false;
				RBLFC.SelectedIndex=2;
				RBLFR.SelectedIndex=2;
				DDLSVType.SelectedIndex=0;
				DDLSVType_SelectedIndexChanged(sender,e);
				lblppartno.Visible=false;
				TxtPPartNo.Visible=false;
				RequiredFieldValidator1.Visible=false;
			}
		}
		
		protected void DDLSVType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(DDLSVType.SelectedIndex >0)
			{
				LblSVDesc.Text=DDLSVType.SelectedItem.Value.Trim();
				PanelTB.Enabled=true;
				if(DDLSVType.SelectedIndex ==5)
				{
					lblclassi.Visible=true;
					TxtClassification.Visible=true;
					TxtClassification.Text="";
					RequiredFieldValidator2.Visible=true;
				}
				else
				{
					RequiredFieldValidator2.Visible=false;
					lblclassi.Visible=false;
					TxtClassification.Visible=false;
					TxtClassification.Text="";
					if(RBLPSOption.SelectedIndex == 2 && RBLFR.SelectedIndex == 2 &&  RBLFC.SelectedIndex ==2 )
					{
						PanelTB.Enabled=false;
					}
					else
					{
						PanelTB.Enabled=true;
					}
				}
			}
		}

		protected void RBLFC_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(RBLFC.SelectedIndex > -1 && RBLFC.SelectedIndex < 2)
			{
				PanelTB.Enabled=true;
			}
			else if(RBLPSOption.SelectedIndex == 2 && RBLFR.SelectedIndex == 2 &&  RBLFC.SelectedIndex ==2 )
			{
				PanelTB.Enabled=false;
			}
			else
			{
				PanelTB.Enabled=true;
			}
		}

		protected void RBLFR_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(RBLFR.SelectedIndex > -1 && RBLFR.SelectedIndex < 2)
			{
				PanelTB.Enabled=true;
			}
			else if(RBLPSOption.SelectedIndex == 2 && RBLFR.SelectedIndex == 2 &&  RBLFC.SelectedIndex ==2 )
			{
				PanelTB.Enabled=false;
			}
			else
			{
				PanelTB.Enabled=true;
			}
		}

		protected void BtnNext_Click(object sender, System.EventArgs e)
		{
			if(Page.IsValid)
			{
				if(Session["Coder"] !=null)
				{
					cd1=new coder();
					cd1 =(coder)Session["Coder"];
					cd1.Specials="";
					Session["Coder"] =null;
					if(RBLPSOption.SelectedIndex ==1)
					{
						cd1.Positioner=RBLPositioner.SelectedItem.Value.ToString().Trim();
						cd1.PositionerPno=TxtPPartNo.Text.Trim();
						cd1.Specials="YES";
					}
					else 
					{
						cd1.Positioner="";
						cd1.PositionerPno="";
					}
					if(RBLPSOption.SelectedIndex ==0)
					{
						if(DDLSVType.SelectedIndex >0)
						{
							cd1.Solenoid=DDLSVType.SelectedItem.Text.Trim();
							cd1.SolenoidClass=TxtClassification.Text.Trim();
							cd1.Specials="YES";
						}
						else 
						{
							cd1.SolenoidClass="";
							cd1.Solenoid="";
						}
					}
					else
					{
						cd1.SolenoidClass="";
						cd1.Solenoid="";
					}
					if(RBLPSOption.SelectedIndex ==2)
					{
						cd1.SolenoidClass="";
						cd1.Solenoid="";
						cd1.Positioner="";
						cd1.PositionerPno="";
					}
					if(RBLFC.SelectedIndex > -1 && RBLFC.SelectedIndex < 2)
					{
						cd1.FlowControl=RBLFC.SelectedItem.Value.ToString().Trim();
						cd1.Specials="YES";
					}
					else
					{
						cd1.FlowControl="";
					}
					if(RBLFR.SelectedIndex > -1 && RBLFR.SelectedIndex < 2)
					{
						cd1.FilterRegulator=RBLFR.SelectedItem.Value.ToString().Trim();
						cd1.Specials="YES";
					}
					else
					{
						cd1.FilterRegulator="";
					}
					if(RBLMO.SelectedIndex ==0)
					{
						cd1.ManualOverride=RBLMO.SelectedItem.Value.ToString().Trim();
						cd1.Specials="YES";
						cd1.SecRodDiameter="D"+cd1.Rod_Diamtr.Trim()+"2";
						cd1.SecRodEnd="RA4";
						cd1.DoubleRod="Yes";
					}
					else
					{
						if(cd1.DoubleRod.ToString().Trim() =="No")
						{
							cd1.SecRodDiameter="";
							cd1.SecRodEnd="";
							cd1.DoubleRod="No";
						}
						cd1.ManualOverride="";
					}
					if(RBLRS.SelectedIndex ==0)
					{
						cd1.ReedSwitch=RBLRS.SelectedItem.Value.ToString().Trim();
						cd1.MagnetRing="P2";
						cd1.Specials="YES";
					}
					else
					{
						cd1.ReedSwitch="";
						cd1.MagnetRing="";
					}
					if(PanelTB.Enabled ==true)
					{
						if(RBLTubing.SelectedIndex >-1)
						{
							cd1.Tubing=RBLTubing.SelectedItem.Value.ToString().Trim();
							cd1.Specials="YES";
						}
						else
						{
							cd1.Tubing="";
						}
					}
					else
					{
						cd1.Tubing="";
					}
					cd1.CompanyID="1003";
					Session["Coder"] =cd1;
					Response.Redirect("Icylinder_Page6.aspx");
				}
				else
				{
					lblpage.Visible=true;
				}
			}
		}

		protected void BtnBack_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("EV_SeriesA.aspx");
		}

		protected void RBLPositioner_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(RBLPositioner.SelectedIndex ==0)
			{
				lblppartno.Visible=true;
				TxtPPartNo.Visible=true;
				RequiredFieldValidator1.Visible=true;
			}
			else
			{
				lblppartno.Visible=false;
				TxtPPartNo.Visible=false;
				RequiredFieldValidator1.Visible=false;
			}
		}

	

	}
}
