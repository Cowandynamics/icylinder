<%@ Page language="c#" Codebehind="Login.aspx.cs" AutoEventWireup="True" Inherits="iCylinderV1.Default" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>UserLogin</title>
		<meta name="vs_snapToGrid" content="False">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="style.css">
		<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
		</script>
	</HEAD>
	<body>
		<form id="UserLogin" method="post" runat="server">
			<div id="Back1">
				<div id="login-02"></div>
				<div id="login-03" align="center">
					<p class="title_txt2"><strong>Welcome to I-Cylinder,</strong><br>
						<br>
						A Unique Tool For Hydaulic &amp; Pneumatic Cylinder Customization &amp; Quote 
						Management.<br>
						<br>
						Powered By.<br>
						<br>
						<IMG alt="Logo" src="images/Cowan_Logo.gif">
					</p>
				</div>
				<div id="login-04"></div>
				<div id="login-05"></div>
				<div id="login-06">
					<TABLE style="WIDTH: 304px; HEIGHT: 118px" id="Table1" border="0" cellSpacing="0" cellPadding="1"
						width="304" align="center">
						<TR>
							<TD style="HEIGHT: 9px" colSpan="3" align="center"><asp:label id="Label3" runat="server" Font-Size="Smaller" ForeColor="White"> Login</asp:label></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 9px" width="80"><asp:label id="Label1" runat="server" Font-Size="XX-Small" ForeColor="White">User Name</asp:label></TD>
							<TD style="WIDTH: 204px; HEIGHT: 9px" width="204"><asp:textbox id="txtUserName" runat="server" Font-Size="Smaller" Width="208px"></asp:textbox></TD>
							<TD style="HEIGHT: 9px"><asp:requiredfieldvalidator id="rfvUserName" runat="server" ErrorMessage="User name is required" ControlToValidate="txtUserName">*</asp:requiredfieldvalidator><asp:regularexpressionvalidator id="RegularExpressionValidator1" runat="server" Width="3px" ErrorMessage="Enter a valid user name."
									ControlToValidate="txtUserName" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:regularexpressionvalidator></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 73px; HEIGHT: 9px"><asp:label id="Label2" runat="server" Font-Size="XX-Small" ForeColor="White">Password</asp:label></TD>
							<TD style="WIDTH: 204px; HEIGHT: 9px"><asp:textbox id="txtPassword" runat="server" Font-Size="Smaller" Width="208px" TextMode="Password">test</asp:textbox></TD>
							<TD style="HEIGHT: 9px"><asp:requiredfieldvalidator id="rfvPassword" runat="server" ErrorMessage="Password is required" ControlToValidate="txtPassword"
									Enabled="False">*</asp:requiredfieldvalidator></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 7px" colSpan="3" align="center"><asp:button id="btnLogin" runat="server" Text="Login" onclick="btnLogin_Click"></asp:button><INPUT style="WIDTH: 46px; HEIGHT: 24px; FONT-SIZE: 12px" id="Clear" value="Clear" type="reset"></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 3px" colSpan="3" align="center"><asp:label id="Label4" runat="server" Font-Size="XX-Small" ForeColor="White">Forget  Password?</asp:label><asp:linkbutton id="LBForgotPass" runat="server" Font-Size="XX-Small" onclick="LBForgotPass_Click">Click Here.</asp:linkbutton></TD>
						</TR>
						<TR>
							<TD colSpan="3" align="center"></TD>
						</TR>
					</TABLE>
				</div>
				<div id="login-07"></div>
				<div id="login-08"><asp:label style="TEXT-ALIGN: center" id="lblMessage" runat="server" Font-Size="X-Small" ForeColor="Red"
						Width="627px"></asp:label><asp:validationsummary id="ValidationSummary1" runat="server" Font-Size="X-Small" Width="627px" Height="38px"></asp:validationsummary></div>
			</div>
		</form>
	</body>
</HTML>
