using System;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for QItems.
	/// </summary>
	public class QItems
	{
		public QItems()
		{
			//
			// TODO: Add constructor logic here
			//
			dwg="";
			units="imperial";
		}
		private string itemno;
		private string partno;
		private string scode;
		private string series;
		private string sprice;
		private string bcode;
		private string bore;
		private string bprice;
		private string rcode;
		private string rod;
		private string rprice;

		private string appoptid;
		private string strokcode;
		private string stroke;
		private string strokprice;
		private string mcode;
		private string mount;
		private string mprice;
		private string recode;
		private string rodend;
		private string reprice;
		private string cucode;
		private string cushion;
		private string cuprice;
		private string cuposcode;
		private string cuppos;
		private string cupprice;
		private string selcode;
		private string seal;
		private string selprice;
		private string portcode;
		private string port;
		private string portprice;
		private string ppcode;
		private string portpos;
		private string ppprice;
	
		private string popoptid;
		private string specialid;
		private string qty;
		private string discount;
		private string unitprice;
		private string totalprice;
		private string cusomerid;
		private string quotatonno;
		private string qdate;
		private string userid;
		private string pricelist;
		private string specialreq;
		private string note;
		private string wt;
		private string dwg;
		//issue #242 start
		private string units;
		public string Units
		{
			get
			{
				return units;
			}
			set
			{
				units = value;
			}
		}	
		//issue #242 end
		public string PriceList
		{
			get
			{
				return pricelist;
			}
			set
			{
				pricelist = value;
			}
		}	
		public string DWG
		{
			get
			{
				return dwg;
			}
			set
			{
				dwg = value;
			}
		}	
		public string User_ID
		{
			get
			{
				return userid;
			}
			set
			{
				userid = value;
			}
		}
		public string Q_Date
		{
			get
			{
				return qdate;
			}
			set
			{
				qdate = value;
			}
		}
		public string Quotation_No
		{
			get
			{
				return quotatonno;
			}
			set
			{
				quotatonno = value;
			}
		}

		public string Cusomer_ID
		{
			get
			{
				return cusomerid;
			}
			set
			{
				cusomerid = value;
			}
		}		
		public string TotalPrice
		{
			get
			{
				return totalprice;
			}
			set
			{
				totalprice = value;
			}
		}
		public string UnitPrice
		{
			get
			{
				return unitprice;
			}
			set
			{
				unitprice = value;
			}
		}
		public string Discount
		{
			get
			{
				return discount;
			}
			set
			{
				discount = value;
			}
		}
		
		public string Quantity
		{
			get
			{
				return qty;
			}
			set
			{
				qty = value;
			}
		}		
		public string Special_ID
		{
			get
			{
				return specialid;
			}
			set
			{
				specialid = value;
			}
		}
		public string PopularOpt_Id
		{
			get
			{
				return popoptid;
			}
			set
			{
				popoptid = value;
			}
		}
		public string PP_Price
			   {
				   get
				   {
					   return ppprice;
				   }
				   set
				   {
					   ppprice = value;
				   }
			   }
		public string PortPos
		{
			get
			{
				return portpos;
			}
			set
			{
				portpos = value;
			}
		}
		public string PP_Code
		{
			get
			{
				return ppcode;
			}
			set
			{
				ppcode = value;
			}
		}
		public string Port_Price
		{
			get
			{
				return portprice;
			}
			set
			{
				portprice = value;
			}
		}
		public string Port_Code
		{
			get
			{
				return portcode;
			}
			set
			{
				portcode = value;
			}
		}
		public string Port
		{
			get
			{
				return port;
			}
			set
			{
				port = value;
			}
		}
		public string Sel_Price
		{
			get
			{
				return selprice;
			}
			set
			{
				selprice = value;
			}
		}
		public string Seal	
		{
			get
			{
				return seal ;
			}
			set
			{
				seal = value;
			}
		}
		public string Sel_Code
		{
			get
			{
				return selcode ;
			}
			set
			{
				selcode = value;
			}
		}
		public string CushionPos_Price
		{
			get
			{
				return cupprice;
			}
			set
			{
				cupprice = value;
			}
		}
		public string ApplicationOpt_Id
		{
			get
			{
				return appoptid;
			}
			set
			{
				appoptid = value;
			}
		}
		public string CushionPos
		{
			get
			{
				return cuppos;
			}
			set
			{
				cuppos = value;
			}
		}
		public string CushionPos_Code
		{
			get
			{
				return cuposcode;
			}
			set
			{
				cuposcode=value;
			}
		}
		public string Cu_Price
		{
			get
			{
				return cuprice;
			}
			set
			{
				cuprice = value;
			}
		}
		public string Cushion
		{
			get
			{
				return cushion;
			}
			set
			{
				cushion = value;
			}
		}
		public string Cu_Code
		{
			get
			{
				return cucode;
			}
			set
			{
				cucode = value;
			}
		}
		public string RE_Price
		{
			get
			{
				return reprice;
			}
			set
			{
				reprice = value;
			}
		}
		public string RodEnd
		{
			get
			{
				return rodend;
			}
			set
			{
				rodend  = value;
			}
		}
		public string RE_Code
		{
			get
			{
				return recode;
			}
			set
			{
				recode  = value;
			}
		}
		public string M_Price
		{
			get
			{
				return mprice;
			}
			set
			{
				mprice  = value;
			}
		}
		public string Mount
		{
			get
			{
				return mount;
			}
			set
			{
				mount  = value;
			}
		}
		public string M_code
		{
			get
			{
				return mcode;
			}
			set
			{
				mcode  = value;
			}
		}
		public string Stroke_Price
		{
			get
			{
				return strokprice;
			}
			set
			{
				strokprice  = value;
			}
		}
		public string Stroke
		{
			get
			{
				return stroke;
			}
			set
			{
				stroke  = value;
			}
		}
		public string Stroke_Code
		{
			get
			{
				return strokcode;
			}
			set
			{
				strokcode  = value;
			}
		}
		
		public string R_Price
		{
			get
			{
				return rprice;
			}
			set
			{
				rprice  = value;
			}
		}
		public string Rod
		{
			get
			{
				return rod;
			}
			set
			{
				rod  = value;
			}
		}
		public string R_Code
		{
			get
			{
				return rcode;
			}
			set
			{
				rcode  = value;
			}
		}
		public string B_Price
		{
			get
			{
				return bprice;
			}
			set
			{
				bprice  = value;
			}
		}
		public string Bore
		{
			get
			{
				return bore;
			}
			set
			{
				bore  = value;
			}
		}
		public string B_Code
		{
			get
			{
				return bcode;
			}
			set
			{
				bcode  = value;
			}
		}
		public string S_Price
		{
			get
			{
				return sprice;
			}
			set
			{
				sprice  = value;
			}
		}
		public string Series
		{
			get
			{
				return series;
			}
			set
			{
				series  = value;
			}
		}
		public string S_Code
		{
			get
			{
				return scode;
			}
			set
			{
				scode  = value;
			}
		}
		public string PartNo
		{
			get
			{
				return partno;
			}
			set
			{
				partno  = value;
			}
		}
		public string ItemNo
		{
			get
			{
				return itemno;
			}
			set
			{
				itemno  = value;
			}
		}
		public string SpecialReq
		{
			get
			{
				return specialreq;
			}
			set
			{
				specialreq  = value;
			}
		}
		public string Note
		{
			get
			{
				return note;
			}
			set
			{
				note  = value;
			}
		}
		public string Weight
		{
			get
			{
				return wt;
			}
			set
			{
				wt  = value;
			}
		}
	}
}
