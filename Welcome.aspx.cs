using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net; 
using System.Security;
namespace iCylinderV1
{
	/// <summary>
	/// Summary description for Welcome.
	/// </summary>
	public partial class Welcome : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.HyperLink HyperLink1;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Button BtnStart;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.HyperLink HyperLink2;
		protected System.Web.UI.WebControls.HyperLink HyperLink3;
		protected System.Web.UI.WebControls.HyperLink HyperLink6;
		protected System.Web.UI.WebControls.Panel PnlSpReg;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if(Session["User"] !=null)
			{
				ArrayList lst =new ArrayList();
				lst =(ArrayList)Session["User"];
				Lblhi.Text="Hi  "+lst[0].ToString()+",";
				ImgLogo.ImageUrl=lst[6].ToString().Trim()+".gif";
				if(lst[6].ToString().Trim() =="1000")
				{
					lbllinks.Text="<p class='title_txt'><strong>Links:</strong><ol>"
						+"<li><a href='http://www.cowandynamics.com/include/get.php?nodeid=1890' target='_blank'>Spring Return Pneumatic Actuator</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=75' target='_blank'>A Series Pneumatic Actuators</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=1907' target='_blank'>AT Series Pneumatic Actuators with Transducer </a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=593' target='_blank'>Series ML</a>"
						//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_L_Series_Catalogue.pdf' target='_blank'>L Series Servo Cylinder.</a>"
						+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_R_Series_Catalogue.pdf' target='_blank'>R Series Mill Type.</a>"
						+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_N_Series_Catalogue.pdf' target='_blank'>N Series Medium Duty Hydraulic.</a>"
						+"</li></ol>";
					try
					{
						string ipAddress = IpAddress(); 
						System.Security.Principal.WindowsPrincipal wp = new System.Security.Principal.WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent()); 
						string user = wp.Identity.Name; 
						StreamWriter wrtr = new StreamWriter (Server.MapPath( "visitors.log" ), true ); 
						wrtr.WriteLine( DateTime .Now.ToString() + " | " + lst[0].ToString().Trim()  + " | " + lst[1].ToString().Trim() + " | " + ipAddress.ToString()+ " | " + user.ToString()); 
						wrtr.Close(); 
					}
					catch(Exception ex)
					{
						string sss=ex.Message.ToString();
					}
				}
				else if(lst[6].ToString().Trim() =="1001" || lst[6].ToString().Trim() =="1007" || lst[6].ToString().Trim() =="1019" || lst[6].ToString().Trim() =="1020")
				{
					lbllinks.Text="<p class='title_txt'><strong>Links:</strong><ol>"
						+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_P_Series_Catalogue.pdf' target='_blank'>P Series NFPA Pneumatic.</a>"
						//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_A_Series_Catalogue.pdf' target='_blank'>A Series Pneumatic Actuators.</a>"
						+"<li><a href='http://www.cowandynamics.com/include/get.php?nodeid=1890' target='_blank'>Spring Return Pneumatic Actuator</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=75' target='_blank'>A Series Pneumatic Actuators</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=1907' target='_blank'>AT Series Pneumatic Actuators with Transducer </a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=593' target='_blank'>Series ML</a>"
						//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_L_Series_Catalogue.pdf' target='_blank'>L Series Servo Cylinder.</a>"
						+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_R_Series_Catalogue.pdf' target='_blank'>R Series Mill Type.</a>"
						+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_N_Series_Catalogue.pdf' target='_blank'>N Series Medium Duty Hydraulic.</a>"
						+"</li></ol>";
					try
					{
						string ipAddress = IpAddress(); 
						System.Security.Principal.WindowsPrincipal wp = new System.Security.Principal.WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent()); 
						string user = wp.Identity.Name; 
						StreamWriter wrtr = new StreamWriter (Server.MapPath( "visitors.log" ), true ); 
						wrtr.WriteLine( DateTime .Now.ToString() + " | " + lst[0].ToString().Trim()  + " | " + lst[1].ToString().Trim() + " | " + ipAddress.ToString()+ " | " + user.ToString()); 
						wrtr.Close(); 
					}
					catch(Exception ex)
					{
						string sss=ex.Message.ToString();
					}
				}
				else if(lst[6].ToString().Trim() =="999")
				{
					lbllinks.Text="<p class='title_txt'><strong>Links:</strong><ol>"
						+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_P_Series_Catalogue.pdf' target='_blank'>P Series NFPA Pneumatic.</a>"
						//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_A_Series_Catalogue.pdf' target='_blank'>A Series Pneumatic Actuators.</a>"
						+"<li><a href='http://www.cowandynamics.com/include/get.php?nodeid=1890' target='_blank'>Spring Return Pneumatic Actuator</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=75' target='_blank'>A Series Pneumatic Actuators</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=1907' target='_blank'>AT Series Pneumatic Actuators with Transducer </a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=593' target='_blank'>Series ML</a>"
						//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_L_Series_Catalogue.pdf' target='_blank'>L Series Servo Cylinder.</a>"
						+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_R_Series_Catalogue.pdf' target='_blank'>R Series Mill Type.</a>"
						+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_N_Series_Catalogue.pdf' target='_blank'>N Series Medium Duty Hydraulic.</a>"
						+"</li></ol>";
					try
					{
						string ipAddress = IpAddress(); 
						System.Security.Principal.WindowsPrincipal wp = new System.Security.Principal.WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent()); 
						string user = wp.Identity.Name; 
						StreamWriter wrtr = new StreamWriter (Server.MapPath( "visitors.log" ), true ); 
						wrtr.WriteLine( DateTime .Now.ToString() + " | " + lst[0].ToString().Trim()  + " | " + lst[1].ToString().Trim() + " | " + ipAddress.ToString()+ " | " + user.ToString()); 
						wrtr.Close();
					}
					catch(Exception ex)
					{
						string sss=ex.Message.ToString();
					}
				}
				//issue #118 start
				//backup
				else if(lst[6].ToString().Trim() =="1002" )
				//update
				//else if(lst[6].ToString().Trim() =="1002" || lst[6].ToString().Trim() =="1021" )
				//issue #118 end
				{
					lbllinks.Text="<p class='title_txt'><strong>Links:</strong><ol>"
						//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_A_Series_Catalogue.pdf' target='_blank'>A Series Pneumatic Actuators.</a>"
						+"<li><a href='http://www.cowandynamics.com/include/get.php?nodeid=1890' target='_blank'>Spring Return Pneumatic Actuator</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=75' target='_blank'>A Series Pneumatic Actuators</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=1907' target='_blank'>AT Series Pneumatic Actuators with Transducer </a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=593' target='_blank'>Series ML</a>"
						+"</li></ol>";
					try
					{
						string ipAddress = IpAddress(); 
						System.Security.Principal.WindowsPrincipal wp = new System.Security.Principal.WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent()); 
						string user = wp.Identity.Name; 
						StreamWriter wrtr = new StreamWriter (Server.MapPath( "visitors.log" ), true ); 
						wrtr.WriteLine( DateTime .Now.ToString() + " | " + lst[0].ToString().Trim()  + " | " + lst[1].ToString().Trim() + " | " + ipAddress.ToString()+ " | " + user.ToString()); 
						wrtr.Close(); 
					}
					catch(Exception ex)
					{
						string sss=ex.Message.ToString();
					}	
					Response.Redirect("BacklogReport.aspx");
				}
				else if(lst[6].ToString().Trim() =="1015" )
				{
					lbllinks.Text="<p class='title_txt'><strong>Links:</strong><ol>"
						//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_A_Series_Catalogue.pdf' target='_blank'>A Series Pneumatic Actuators.</a>"
						+"<li><a href='http://www.cowandynamics.com/include/get.php?nodeid=1890' target='_blank'>Spring Return Pneumatic Actuator</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=75' target='_blank'>A Series Pneumatic Actuators</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=1907' target='_blank'>AT Series Pneumatic Actuators with Transducer </a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=593' target='_blank'>Series ML</a>"
						+"</li></ol>";
					try
					{
						string ipAddress = IpAddress(); 
						System.Security.Principal.WindowsPrincipal wp = new System.Security.Principal.WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent()); 
						string user = wp.Identity.Name; 
						StreamWriter wrtr = new StreamWriter (Server.MapPath( "visitors.log" ), true ); 
						wrtr.WriteLine( DateTime .Now.ToString() + " | " + lst[0].ToString().Trim()  + " | " + lst[1].ToString().Trim() + " | " + ipAddress.ToString()+ " | " + user.ToString()); 
						wrtr.Close(); 
					}
					catch(Exception ex)
					{
						string sss=ex.Message.ToString();
					}	
					Response.Redirect("BacklogReport.aspx");
				}
				else if(lst[6].ToString().Trim() =="1016" || lst[6].ToString().Trim() =="1026")
				{
					lbllinks.Text="<p class='title_txt'><strong>Links:</strong><ol>"
						//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_A_Series_Catalogue.pdf' target='_blank'>A Series Pneumatic Actuators.</a>"
						+"<li><a href='http://www.cowandynamics.com/include/get.php?nodeid=1890' target='_blank'>Spring Return Pneumatic Actuator</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=75' target='_blank'>A Series Pneumatic Actuators</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=1907' target='_blank'>AT Series Pneumatic Actuators with Transducer </a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=593' target='_blank'>Series ML</a>"
						//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_L_Series_Catalogue.pdf' target='_blank'>L Series Servo Cylinder.</a>"
						//issue #264 start
						+"<li><a href='http://www.cowan-cylinder.com/icylinder_instructions_wey.pdf' target='_blank'>User Manual</a>"
						//issue #264 end
						+"</li></ol>";
					try
					{
						string ipAddress = IpAddress(); 
						System.Security.Principal.WindowsPrincipal wp = new System.Security.Principal.WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent()); 
						string user = wp.Identity.Name; 
						StreamWriter wrtr = new StreamWriter (Server.MapPath( "visitors.log" ), true ); 
						wrtr.WriteLine( DateTime .Now.ToString() + " | " + lst[0].ToString().Trim()  + " | " + lst[1].ToString().Trim() + " | " + ipAddress.ToString()+ " | " + user.ToString()); 
						wrtr.Close(); 
					}
					catch(Exception ex)
					{
						string sss=ex.Message.ToString();
					}	
				}
				//issue #267 start
				else if(lst[6].ToString().Trim() =="1018" )
				{
					lbllinks.Text="<p class='title_txt'><strong>Links:</strong><ol>"
						+"<li><a href='http://www.cowandynamics.com/include/get.php?nodeid=1890' target='_blank'>Spring Return Pneumatic Actuator</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=75' target='_blank'>A Series Pneumatic Actuators</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=1907' target='_blank'>AT Series Pneumatic Actuators with Transducer </a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=593' target='_blank'>Series ML</a>"
						+"<li><a href='http://www.cowan-cylinder.com/icylinder_instructions_red.pdf' target='_blank'>User Manual</a>"
						+"</li></ol>";
					try
					{
						string ipAddress = IpAddress(); 
						System.Security.Principal.WindowsPrincipal wp = new System.Security.Principal.WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent()); 
						string user = wp.Identity.Name; 
						StreamWriter wrtr = new StreamWriter (Server.MapPath( "visitors.log" ), true ); 
						wrtr.WriteLine( DateTime .Now.ToString() + " | " + lst[0].ToString().Trim()  + " | " + lst[1].ToString().Trim() + " | " + ipAddress.ToString()+ " | " + user.ToString()); 
						wrtr.Close(); 
					}
					catch(Exception ex)
					{
						string sss=ex.Message.ToString();
					}	
				}
				//issue #267 end
				else if(lst[6].ToString().Trim() =="1017" )
				{
					lbllinks.Text="<p class='title_txt'><strong>Links:</strong><ol>"
						//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_A_Series_Catalogue.pdf' target='_blank'>A Series Pneumatic Actuators.</a>"
						+"<li><a href='http://www.cowandynamics.com/include/get.php?nodeid=1890' target='_blank'>Spring Return Pneumatic Actuator</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=75' target='_blank'>A Series Pneumatic Actuators</a>"
						//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_AT_Series_Catalogue.pdf' target='_blank'>AT Series Pneumatic Actuators with Transducer.</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=1907' target='_blank'>AT Series Pneumatic Actuators with Transducer </a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=593' target='_blank'>Series ML</a>"
						//issue #264 start
						+"<li><a href='http://www.cowan-cylinder.com/icylinder_SISTAG.pdf' target='_blank'>User Manual</a>"
						//issue #264 end
						+"</li></ol>";
					try
					{
						string ipAddress = IpAddress(); 
						System.Security.Principal.WindowsPrincipal wp = new System.Security.Principal.WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent()); 
						string user = wp.Identity.Name; 
						StreamWriter wrtr = new StreamWriter (Server.MapPath( "visitors.log" ), true ); 
						wrtr.WriteLine( DateTime .Now.ToString() + " | " + lst[0].ToString().Trim()  + " | " + lst[1].ToString().Trim() + " | " + ipAddress.ToString()+ " | " + user.ToString()); 
						wrtr.Close(); 
					}
					catch(Exception ex)
					{
						string sss=ex.Message.ToString();
					}	
				}
				else if(lst[6].ToString().Trim() =="1003")
				{
					lbllinks.Text="<p class='title_txt'><strong>Links:</strong><ol>"
						//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_A_Series_Catalogue.pdf' target='_blank'>A Series Pneumatic Actuators.</a>"
						+"<li><a href='http://www.cowandynamics.com/include/get.php?nodeid=1890' target='_blank'>Spring Return Pneumatic Actuator</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=75' target='_blank'>A Series Pneumatic Actuators</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=1907' target='_blank'>AT Series Pneumatic Actuators with Transducer </a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=593' target='_blank'>Series ML</a>"
						+"</li></ol>";
					try
					{
						string ipAddress = IpAddress(); 
						System.Security.Principal.WindowsPrincipal wp = new System.Security.Principal.WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent()); 
						string user = wp.Identity.Name; 
						StreamWriter wrtr = new StreamWriter (Server.MapPath( "visitors.log" ), true ); 
						wrtr.WriteLine( DateTime .Now.ToString() + " | " + lst[0].ToString().Trim()  + " | " + lst[1].ToString().Trim() + " | " + ipAddress.ToString()+ " | " + user.ToString()); 
						wrtr.Close(); 
					}
					catch(Exception ex)
					{
						string sss=ex.Message.ToString();
					}
					
				}
				else if(lst[6].ToString().Trim() =="1004")
				{
					lbllinks.Text="<p class='title_txt'><strong>Links:</strong><ol>"
						//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_A_Series_Catalogue.pdf' target='_blank'>A Series Pneumatic Actuators.</a>"
						+"<li><a href='http://www.cowandynamics.com/include/get.php?nodeid=1890' target='_blank'>Spring Return Pneumatic Actuator</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=75' target='_blank'>A Series Pneumatic Actuators</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=1907' target='_blank'>AT Series Pneumatic Actuators with Transducer </a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=593' target='_blank'>Series ML</a>"
						+"</li></ol>";
					try
					{
						string ipAddress = IpAddress(); 
						System.Security.Principal.WindowsPrincipal wp = new System.Security.Principal.WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent()); 
						string user = wp.Identity.Name; 
						StreamWriter wrtr = new StreamWriter (Server.MapPath( "visitors.log" ), true ); 
						wrtr.WriteLine( DateTime .Now.ToString() + " | " + lst[0].ToString().Trim()  + " | " + lst[1].ToString().Trim() + " | " + ipAddress.ToString()+ " | " + user.ToString()); 
						wrtr.Close(); 
					}
					catch(Exception ex)
					{
						string sss=ex.Message.ToString();
					}
					
				}
				else if(lst[6].ToString().Trim() =="1005")
				{
					lbllinks.Text="<p class='title_txt'><strong>Links:</strong><ol>"
						//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_A_Series_Catalogue.pdf' target='_blank'>A Series Pneumatic Actuators.</a>"
						+"<li><a href='http://www.cowandynamics.com/include/get.php?nodeid=1890' target='_blank'>Spring Return Pneumatic Actuator</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=75' target='_blank'>A Series Pneumatic Actuators</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=1907' target='_blank'>AT Series Pneumatic Actuators with Transducer </a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=593' target='_blank'>Series ML</a>"
						+"</li></ol>";
					try
					{
						string ipAddress = IpAddress(); 
						System.Security.Principal.WindowsPrincipal wp = new System.Security.Principal.WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent()); 
						string user = wp.Identity.Name; 
						StreamWriter wrtr = new StreamWriter (Server.MapPath( "visitors.log" ), true ); 
						wrtr.WriteLine( DateTime .Now.ToString() + " | " + lst[0].ToString().Trim()  + " | " + lst[1].ToString().Trim() + " | " + ipAddress.ToString()+ " | " + user.ToString()); 
						wrtr.Close(); 
					}
					catch(Exception ex)
					{
						string sss=ex.Message.ToString();
					}
					
				}
				else if(lst[6].ToString().Trim() =="1006")
				{
					lbllinks.Text="<p class='title_txt'><strong>Links:</strong><ol>"
						//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_A_Series_Catalogue.pdf' target='_blank'>A Series Pneumatic Actuators.</a>"
						+"<li><a href='http://www.cowandynamics.com/include/get.php?nodeid=1890' target='_blank'>Spring Return Pneumatic Actuator</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=75' target='_blank'>A Series Pneumatic Actuators</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=1907' target='_blank'>AT Series Pneumatic Actuators with Transducer </a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=593' target='_blank'>Series ML</a>"
						+"</li></ol>";
					try
					{
						string ipAddress = IpAddress(); 
						System.Security.Principal.WindowsPrincipal wp = new System.Security.Principal.WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent()); 
						string user = wp.Identity.Name; 
						StreamWriter wrtr = new StreamWriter (Server.MapPath( "visitors.log" ), true ); 
						wrtr.WriteLine( DateTime .Now.ToString() + " | " + lst[0].ToString().Trim()  + " | " + lst[1].ToString().Trim() + " | " + ipAddress.ToString()+ " | " + user.ToString()); 
						wrtr.Close(); 
					}
					catch(Exception ex)
					{
						string sss=ex.Message.ToString();
					}
					
				}
				else if(lst[6].ToString().Trim() =="1008")
				{
					lbllinks.Text="<p class='title_txt'><strong>Links:</strong><ol>"
						+"<li><a href='http://www.cowandynamics.com/include/get.php?nodeid=1890' target='_blank'>Spring Return Pneumatic Actuator</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=75' target='_blank'>A Series Pneumatic Actuators</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=1907' target='_blank'>AT Series Pneumatic Actuators with Transducer </a>"
						+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_P_Series_Catalogue.pdf' target='_blank'>P Series NFPA Pneumatic.</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=593' target='_blank'>Series ML</a>"
						+"</li></ol>";
					try
					{
						string ipAddress = IpAddress(); 
						System.Security.Principal.WindowsPrincipal wp = new System.Security.Principal.WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent()); 
						string user = wp.Identity.Name; 
						StreamWriter wrtr = new StreamWriter (Server.MapPath( "visitors.log" ), true ); 
						wrtr.WriteLine( DateTime .Now.ToString() + " | " + lst[0].ToString().Trim()  + " | " + lst[1].ToString().Trim() + " | " + ipAddress.ToString()+ " | " + user.ToString()); 
						wrtr.Close(); 
					}
					catch(Exception ex)
					{
						string sss=ex.Message.ToString();
					}
				}
				else if(lst[6].ToString().Trim() =="1009")
				{
					lbllinks.Text="<p class='title_txt'><strong>Links:</strong><ol>"
						//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_A_Series_Catalogue.pdf' target='_blank'>A Series Pneumatic Actuators.</a>"
						+"<li><a href='http://www.cowandynamics.com/include/get.php?nodeid=1890' target='_blank'>Spring Return Pneumatic Actuator</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=75' target='_blank'>A Series Pneumatic Actuators</a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=1907' target='_blank'>AT Series Pneumatic Actuators with Transducer </a>"
						+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=593' target='_blank'>Series ML</a>"
						+"</li></ol>";
					try
					{
						string ipAddress = IpAddress(); 
						System.Security.Principal.WindowsPrincipal wp = new System.Security.Principal.WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent()); 
						string user = wp.Identity.Name; 
						StreamWriter wrtr = new StreamWriter (Server.MapPath( "visitors.log" ), true ); 
						wrtr.WriteLine( DateTime .Now.ToString() + " | " + lst[0].ToString().Trim()  + " | " + lst[1].ToString().Trim() + " | " + ipAddress.ToString()+ " | " + user.ToString()); 
						wrtr.Close(); 
					}
					catch(Exception ex)
					{
						string sss=ex.Message.ToString();
					}
				}
					//#588
				//else if(lst[6].ToString().Trim() =="1012" || lst[6].ToString().Trim() =="1013"  || lst[6].ToString().Trim() =="1027" || lst[6].ToString().Trim() =="1028" || lst[6].ToString().Trim() =="1029" || lst[6].ToString().Trim() =="1030")
				else if(lst[6].ToString().Trim() =="1012" || lst[6].ToString().Trim() =="1013"  || lst[6].ToString().Trim() =="1030" )
				{
					try
					{
						string ipAddress = IpAddress(); 
						System.Security.Principal.WindowsPrincipal wp = new System.Security.Principal.WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent()); 
						string user = wp.Identity.Name; 
						StreamWriter wrtr = new StreamWriter (Server.MapPath( "visitors.log" ), true ); 
						wrtr.WriteLine( DateTime .Now.ToString() + " | " + lst[0].ToString().Trim()  + " | " + lst[1].ToString().Trim() + " | " + ipAddress.ToString()+ " | " + user.ToString()); 
						wrtr.Close(); 
					}
					catch(Exception ex)
					{
						string sss=ex.Message.ToString();
					}
					Response.Redirect("BacklogReport.aspx");					
				}
			}
			else
			{
				Response.Redirect("Login.aspx");
			}
		}
		

		private string IpAddress() 
		{ 
			string strIpAddress; 
			strIpAddress = Request.ServerVariables[ "HTTP_X_FORWARDED_FOR" ]; 
			if (strIpAddress == null ) 
			{
				strIpAddress = Request.ServerVariables[ "REMOTE_ADDR" ]; 
			}
			else if (strIpAddress == "" ) 
			{
				strIpAddress = Request.ServerVariables[ "REMOTE_ADDR" ]; 
			}
				return strIpAddress; 

		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		
	}
}
