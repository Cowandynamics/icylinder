using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Web.Mail;
using System.IO;
using System.Net;
using System.Security.Principal;
namespace iCylinderV1
{
	/// <summary>
	/// Summary description for PNOAdvanced.
	/// </summary>
	public class PNOAdvanced : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.DropDownList DDLSeries;
		protected System.Web.UI.WebControls.DropDownList DDLBore;
		protected System.Web.UI.WebControls.DropDownList DDLRod;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.TextBox TxtStroke;
		protected System.Web.UI.WebControls.Label Label40;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator1;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.DropDownList DDLMount;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.DropDownList DDLRodEnd;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.DropDownList DDLCushions;
		protected System.Web.UI.WebControls.Label Label18;
		protected System.Web.UI.WebControls.DropDownList DDLSeal;
		protected System.Web.UI.WebControls.Label Label21;
		protected System.Web.UI.WebControls.DropDownList DDLPorttype;
		protected System.Web.UI.WebControls.Label Label22;
		protected System.Web.UI.WebControls.DropDownList DDLPortPos;
		protected System.Web.UI.WebControls.Button BTNGenPartNo;
		protected System.Web.UI.WebControls.Button BtnReset;
		protected System.Web.UI.WebControls.Label lbl;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.TextBox TXTRodEx;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.TextBox TXTThread;
		protected System.Web.UI.WebControls.Label Label19;
		protected System.Web.UI.WebControls.DropDownList DDLDiameterSec;
		protected System.Web.UI.WebControls.Label Label20;
		protected System.Web.UI.WebControls.DropDownList DDLSecRodEnd;
		protected System.Web.UI.WebControls.Label Label23;
		protected System.Web.UI.WebControls.TextBox TxtSecRodEx;
		protected System.Web.UI.WebControls.Label Label24;
		protected System.Web.UI.WebControls.TextBox TxtSecThreadEx;
		protected System.Web.UI.WebControls.Label Label38;
		protected System.Web.UI.WebControls.Label Label35;
		protected System.Web.UI.WebControls.Label Label36;
		protected System.Web.UI.WebControls.DropDownList DDLCoating;
		protected System.Web.UI.WebControls.Label Label27;
		protected System.Web.UI.WebControls.DropDownList DDLCushionPosition;
		protected System.Web.UI.WebControls.Label Label43;
		protected System.Web.UI.WebControls.Label Label41;
		protected System.Web.UI.WebControls.Label Label45;
		protected System.Web.UI.WebControls.TextBox TXTIntere;
		protected System.Web.UI.WebControls.Label Label47;
		protected System.Web.UI.WebControls.Label Label49;
		protected System.Web.UI.WebControls.Label Label50;
		protected System.Web.UI.WebControls.Label Label54;
		protected System.Web.UI.WebControls.Label Label55;
		protected System.Web.UI.WebControls.RadioButtonList RBLMagnet;
		//protected JLovell.WebControls.StaticPostBackPosition StaticPostBackPosition1;
		DBClass db =new DBClass();
		protected System.Web.UI.WebControls.Label lblbore;
		protected System.Web.UI.WebControls.Label Label30;
		protected System.Web.UI.WebControls.Label Label48;
		protected System.Web.UI.WebControls.Label Label17;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.WebControls.RadioButtonList RBLDoubleRod;
		protected System.Web.UI.WebControls.Label Label37;
		protected System.Web.UI.WebControls.RadioButtonList RBLTieRod;
		protected System.Web.UI.WebControls.Label Label52;
		protected System.Web.UI.WebControls.Label Label51;
		protected System.Web.UI.WebControls.RadioButtonList RBDrain;
		protected System.Web.UI.WebControls.Label lblbor;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator7;
		protected System.Web.UI.WebControls.CompareValidator CompareValidator1;
		protected System.Web.UI.WebControls.TextBox TxtStopTube;
		protected System.Web.UI.WebControls.DropDownList DDLSSpistinrod;
		protected System.Web.UI.WebControls.Panel Panel1;
		protected System.Web.UI.WebControls.Panel PanelSeal;
		protected System.Web.UI.WebControls.Panel PanelRod;
		protected System.Web.UI.WebControls.Panel PanelGland;
		protected System.Web.UI.WebControls.Panel PanelPiston;
		protected System.Web.UI.WebControls.Panel PanelMount;
		protected System.Web.UI.WebControls.Panel PanelStroke;
		protected System.Web.UI.WebControls.Panel PanelOther;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label LblInfo;
		protected System.Web.UI.WebControls.DropDownList DDLMetalScrappeer;
		protected System.Web.UI.WebControls.DropDownList DDLPistonSealConfig;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator2;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator3;
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator3;
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator2;
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator4;
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator5;
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator6;
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator7;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.TextBox TxtEffectiveStrok;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator4;
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator8;
		protected System.Web.UI.WebControls.DropDownList DDLRodSeal;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.DropDownList DDLTandomConfig;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.TextBox TxtSecondStroke;
		protected System.Web.UI.WebControls.Panel PNTandem;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.RadioButtonList RBStrokeType;
		protected System.Web.UI.WebControls.Label Label25;
		protected System.Web.UI.WebControls.DropDownList DDLBushing;
		protected System.Web.UI.WebControls.CheckBox CBTandem;
		protected System.Web.UI.WebControls.CheckBox CbOther;
		protected System.Web.UI.WebControls.CheckBox CBGland;
		protected System.Web.UI.WebControls.CheckBox CBPiston;
		protected System.Web.UI.WebControls.CheckBox CBMount;
		protected System.Web.UI.WebControls.CheckBox CBStroke;
		protected System.Web.UI.WebControls.CheckBox CBSeal;
		protected System.Web.UI.WebControls.CheckBox CBRod;
		protected System.Web.UI.WebControls.DropDownList DDLBarrel;
		protected System.Web.UI.WebControls.Label Label26;
		protected System.Web.UI.WebControls.Label LBLPNo;
		protected System.Web.UI.WebControls.Label Label29;
		protected System.Web.UI.WebControls.Panel Panel2;
		protected System.Web.UI.WebControls.Label Label28;
		protected System.Web.UI.WebControls.Label Label31;
		protected System.Web.UI.WebControls.Label Label32;
		protected System.Web.UI.WebControls.Label Label39;
		protected System.Web.UI.WebControls.Label Label42;
		protected System.Web.UI.WebControls.Button BtnGeneratePrice;
		protected System.Web.UI.WebControls.Label Label46;
		protected System.Web.UI.WebControls.TextBox TxtSpecialReq;
		protected System.Web.UI.WebControls.Label Label53;
		protected System.Web.UI.WebControls.Label Label56;
		protected System.Web.UI.WebControls.Panel PnlSpReg;
		protected System.Web.UI.WebControls.RadioButtonList RBSpecialReq;
		protected System.Web.UI.WebControls.TextBox TxtQty;
		protected System.Web.UI.WebControls.Label lblmgrp;
		protected System.Web.UI.WebControls.Label lbltable;
		protected System.Web.UI.WebControls.Label lbltotal;
		protected System.Web.UI.WebControls.Label lblgenerateprice;
		protected System.Web.UI.WebControls.Label lblqno;
		protected System.Web.UI.WebControls.Label lblstroke;
		protected System.Web.UI.WebControls.Label lblstatus;
		protected System.Web.UI.WebControls.Label LBLSeries;
		protected System.Web.UI.WebControls.Panel Panel3;
		protected System.Web.UI.WebControls.CheckBox CBPort;
		protected System.Web.UI.WebControls.Label Label44;
		protected System.Web.UI.WebControls.Label Label33;
		protected System.Web.UI.WebControls.DropDownList DDLPortSize;
		protected System.Web.UI.WebControls.Label Label34;
		protected System.Web.UI.WebControls.DropDownList DDLAirbleeds;
		protected System.Web.UI.WebControls.Panel PanelPort;
		protected System.Web.UI.WebControls.RequiredFieldValidator RFVtandem;
		protected System.Web.UI.WebControls.Panel Panel4;
		protected System.Web.UI.WebControls.Label Label57;
		protected System.Web.UI.WebControls.Label Label58;
		protected System.Web.UI.WebControls.DropDownList DDLPriority;
		protected System.Web.UI.WebControls.Button BtnSendMail;
		protected System.Web.UI.WebControls.Panel PPriority;
		protected System.Web.UI.WebControls.Label lblD;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.TextBox TxtBBCap;
		protected System.Web.UI.WebControls.Label lblbbc;
		protected System.Web.UI.WebControls.Label Label65;
		protected System.Web.UI.WebControls.Panel PnlBBC;
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator11;
		protected System.Web.UI.WebControls.TextBox TxtBBHead;
		protected System.Web.UI.WebControls.Label lblbbh;
		protected System.Web.UI.WebControls.Label lblbb;
		protected System.Web.UI.WebControls.Panel PnlBBH;
		protected System.Web.UI.WebControls.Label lblxi;
		protected System.Web.UI.WebControls.Panel PnlXI;
		protected System.Web.UI.WebControls.Label Label68;
		protected System.Web.UI.WebControls.Label lblw;
		protected System.Web.UI.WebControls.Label lblwd;
		protected System.Web.UI.WebControls.Label Label64;
		protected System.Web.UI.WebControls.Label lblx;
		protected System.Web.UI.WebControls.Label lblbbcc;
		protected System.Web.UI.WebControls.Label lblbbhh;
		protected System.Web.UI.WebControls.Label lblSecstroke;
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator10;
		protected System.Web.UI.WebControls.Label Label59;
		PartNumber PNo=new PartNumber();
		protected System.Web.UI.WebControls.Label lblQty;
		protected System.Web.UI.WebControls.Label dwg;
		protected System.Web.UI.WebControls.LinkButton LbPreview;
		protected System.Web.UI.WebControls.Label Label60;
		protected System.Web.UI.HtmlControls.HtmlInputFile UploadDwg;
		protected System.Web.UI.WebControls.Label lblfilename;
		protected System.Web.UI.WebControls.CheckBox cbupload;
		protected System.Web.UI.WebControls.Label lblfile;
		protected System.Web.UI.WebControls.CheckBox CBTransducer;
		protected System.Web.UI.WebControls.Label Label61;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator5;
		protected System.Web.UI.WebControls.DropDownList DDLTransducer;
		protected System.Web.UI.WebControls.Label Label66;
		protected System.Web.UI.WebControls.Label lblohm;
		protected System.Web.UI.WebControls.Label Label62;
		protected System.Web.UI.WebControls.Label Label67;
		protected System.Web.UI.WebControls.RadioButtonList RBLRemote;
		protected System.Web.UI.WebControls.RadioButtonList RBLFittings;
		protected System.Web.UI.WebControls.Panel PNTransducer;
		protected System.Web.UI.WebControls.TextBox TxtTransducer;
		protected System.Web.UI.WebControls.Label lblminmaxohm;
		protected System.Web.UI.WebControls.Label lblressist;
		decimal round=0.004m;
		private void Page_Load(object sender, System.EventArgs e)
		{
			
			//issue #137 start
			try
			{
				if( Session["User"] !=null)
				{
					LblInfo.Text="";	
					dwg.Text="";
					if(CBRod.Checked ==false)
					{
						PanelRod.Visible=false;
					}
					if(CBStroke.Checked == false)
					{
						PanelStroke.Visible=false;
					}
					if(CBSeal.Checked == false)
					{
						PanelSeal.Visible=false;
					}
					if(CBPort.Checked == false)
					{
						PanelPort.Visible=false;
					}
					if(CBMount.Checked == false)
					{	
						PanelMount.Visible=false;
					}
					if(CBGland.Checked == false)
					{					
						PanelGland.Visible=false;
					}
					if(CBPiston.Checked == false)
					{			
						PanelPiston.Visible=false;
					}
					if(CbOther.Checked ==false)
					{				
						PanelOther.Visible=false;
					}
					if(CBTandem.Checked ==false)
					{				
						PNTandem.Visible=false;
					}
					if(CBTransducer.Checked ==false)
					{				
						PNTransducer.Visible=false;
					}
					DDLSeries.ForeColor =Color.Black;
					DDLBore.ForeColor =Color.Black;
					DDLRod.ForeColor =Color.Black;
					DDLRodEnd.ForeColor =Color.Black;
					DDLMount.ForeColor =Color.Black;
					DDLPorttype.ForeColor =Color.Black;
					DDLCushions.ForeColor =Color.Black;
					DDLSeal.ForeColor =Color.Black;
					LblInfo.Text="";
					if(!IsPostBack)
					{
						//					string ipAddress = IpAddress(); 
						//					System.Security.Principal.WindowsPrincipal wp = new System.Security.Principal.WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent()); 
						//					string user = wp.Identity.Name; 
						//					string hostName = Dns.GetHostByAddress(ipAddress).HostName.ToString(); 
						//					StreamWriter wrtr = new StreamWriter (Server.MapPath( "visitors.log" ), true ); 
						//					wrtr.WriteLine( DateTime .Now.ToString() + " | " + ipAddress + " | " + user + " | " + hostName + " | " + Request.Url.ToString()); 
						//					wrtr.Close(); 
						BtnGeneratePrice.Visible=true;
						PnlSpReg.Visible =false;
						CBRod.Enabled=false;
						CBGland.Enabled=false;
						CBMount.Enabled=false;
						CbOther.Enabled=false;
						CBPiston.Enabled=false;
						CBStroke.Enabled=false;
						CBSeal.Enabled=false;
						CBPort.Enabled=false;
						CBTandem.Enabled=false;
						CBTransducer.Enabled=false;
						if(Session["User"] !=null)
						{
							ArrayList selist =new ArrayList();
							selist =(ArrayList)Session["User"];
							//						ArrayList clist =new ArrayList();
							//						clist = db.SelectContactName(selist[5].ToString()); 
							ArrayList list = new ArrayList();
							list=db.SelectSeries();
							ArrayList slist =new ArrayList();
							slist=db.SelectAvailableSeries_details(selist[6].ToString().Trim(),selist[5].ToString().Trim());
							if(slist[0].ToString() !="")
							{
								DDLSeries.Items.Add(list[0].ToString());
							}
							if(slist[1].ToString() !="")
							{
								DDLSeries.Items.Add(list[1].ToString());
							}
							if(slist[2].ToString() !="")
							{
								DDLSeries.Items.Add(list[2].ToString());
							}
							if(slist[3].ToString() !="")
							{
								DDLSeries.Items.Add(list[3].ToString());
							}
							if(slist[4].ToString() !="")
							{
								DDLSeries.Items.Add(list[4].ToString());
							}
							if(slist[5].ToString() !="")
							{
								DDLSeries.Items.Add(list[5].ToString());
							}
							if(slist[6].ToString() !="")
							{
								DDLSeries.Items.Add(list[6].ToString());
							}
							if(slist[7].ToString() !="")
							{
								DDLSeries.Items.Add(list[7].ToString());
							}
							if(slist[8].ToString() !="")
							{
								DDLSeries.Items.Add(list[8].ToString());
							}
							if(slist[9].ToString() !="")
							{
								DDLSeries.Items.Add(list[9].ToString());
							}
							if(slist[10].ToString() !="")
							{
								DDLSeries.Items.Add(list[10].ToString());
							}
							if(slist[11].ToString() !="")
							{
								DDLSeries.Items.Add(list[11].ToString());
							}
							if(slist[12].ToString() !="")
							{
								DDLSeries.Items.Add(list[12].ToString());
							}
							//add R5 start
							if(slist[13].ToString() !="")
							{
								DDLSeries.Items.Add(list[13].ToString());
							}
							string strSeries = list[14].ToString();
							if(slist[14].ToString() !="")
							{
								DDLSeries.Items.Add(list[14].ToString());
							}
							//add R5 enad
						}
						else
						{
							ArrayList list = new ArrayList();
							list=db.SelectSeries();
							for (int i=0;i<= list.Count -1;i++)
							{
								DDLSeries.Items.Add(list[i].ToString());
							}

						}
						if(Session["Command"] !=null && ! IsPostBack)
						{
							QItems qitems =new QItems();
							Quotation quot =new Quotation();
							ArrayList  pn=(ArrayList)Session["Command"];
							qitems =db.SelectItemsToManage(pn[2].ToString().Trim(),pn[1].ToString().Trim());
							//issue #137 start
							if(qitems.S_Code=="AS") throw new Exception("This feature for AS Series is under construction");
							//issue #137 end
							ListItem litem=new ListItem();
							litem.Text =db.SelectValue("Series_Name","WEB_Series_TableV1","Series_Code",qitems.S_Code.ToString().Trim());
							DDLSeries.SelectedIndex =DDLSeries.Items.IndexOf(litem);
							DDLSeries_SelectedIndexChanged(sender ,e);
					
							litem.Text =db.SelectValue("Bore_Size","WEB_Bore_TableV1","Bore_Code",qitems.B_Code.ToString().Trim());
							DDLBore.SelectedIndex =DDLBore.Items.IndexOf(litem);
							DDLBore_SelectedIndexChanged(sender ,e);

							litem.Text =db.SelectValue("Rod_Size","WEB_RodSerZ_TableV1","Rod_Code",qitems.R_Code.ToString().Trim());
							DDLRod.SelectedIndex =DDLRod.Items.IndexOf(litem);
							DDLRod_SelectedIndexChanged(sender ,e);

							litem.Text =db.SelectValue("RodEnd_Shape","WEB_RodEndKK_TableV1","RodEnd_Code",qitems.RE_Code.ToString().Trim());
							DDLRodEnd.SelectedIndex =DDLRodEnd.Items.IndexOf(litem);
			
							litem.Text =db.SelectValue("Cushion_type","WEB_Cushion_TableV1","Cushion_Code",qitems.Cu_Code.ToString().Trim());
							DDLCushions.SelectedIndex =DDLCushions.Items.IndexOf(litem);
							DDLCushions_SelectedIndexChanged(sender ,e);
					
							if(DDLSeries.SelectedItem.Text !="A")
							{
								litem.Text =db.SelectValue("CushionPos_Pos","WEB_CushionPos_TableV1","CushionPos_Code",qitems.CushionPos_Code.ToString().Trim());
								DDLCushionPosition.SelectedIndex =DDLCushionPosition.Items.IndexOf(litem);
							}
							
							litem.Text =db.SelectValue("Seal_Type","WEB_Seal_TableV1","Seal_Code",qitems.Sel_Code.ToString().Trim());
							DDLSeal.SelectedIndex =DDLSeal.Items.IndexOf(litem);
				
					
							litem.Text =db.SelectValue("PortType_Type","WEB_portType_TableV1","PortType_Code",qitems.Port_Code.ToString().Trim());
							DDLPorttype.SelectedIndex =DDLPorttype.Items.IndexOf(litem);
							DDLPorttype_SelectedIndexChanged(sender,e);

							litem.Text =db.SelectValue("PortPos_Position","WEB_PortPosition_TableV1","PortPos_Code",qitems.PP_Code.ToString().Trim());
							DDLPortPos.SelectedIndex =DDLPortPos.Items.IndexOf(litem);
					

							litem.Text =db.SelectValue("Mount_Type","WEB_Mount"+qitems.S_Code.Trim()+"_TableV1","Mount_Code",qitems.M_code.ToString().Trim());
							DDLMount.SelectedIndex =DDLMount.Items.IndexOf(litem);
							DDLMount_SelectedIndexChanged(sender,e);

							TxtStroke.Text=qitems.Stroke_Code.Trim();

							if(qitems.ApplicationOpt_Id !="")
							{
								ArrayList applicatlist=new ArrayList(); 
								Opts opt =new Opts();
								applicatlist =db.SelectAopts(qitems.Quotation_No.Trim(),qitems.PartNo.Trim());
								for(int i=0;i<applicatlist.Count;i++)
								{
									opt =(Opts)applicatlist[i];
									ListItem litem1=new ListItem();
									if(opt.Code.ToString().Trim() !="" )
									{
										if(opt.Code.ToString().Trim().Substring(0,1) =="G")
										{
											if(CBGland.Enabled !=true || CBGland.Checked !=true )
											{
												CBGland.Enabled =true;
												CBGland.Checked =true;
												CBGland_CheckedChanged(sender,e);
											}
											if(opt.Code.ToString().Trim().Length ==3)
											{
												//issue #104 start
												//backup
												//if(opt.Code.ToString().Trim() =="GR1" || opt.Code.ToString().Trim() =="GR2")
												//update
												if(opt.Code.ToString().Trim() =="GR1" || opt.Code.ToString().Trim() =="GR2" || opt.Code.ToString().Trim() =="GT3")
													//issue #104 end
												{
										
													litem1.Text =db.SelectValue("MetalScrapper","WEB_MetalScrapper_TableV1","MS_Code",opt.Code.ToString().Trim());
													DDLMetalScrappeer.SelectedIndex =DDLMetalScrappeer.Items.IndexOf(litem1);
										
												}
																
											}
											else if(opt.Code.ToString().Trim().Length ==2)
											{
												if(opt.Code.ToString().Trim() =="G1")
												{
													RBDrain.SelectedIndex =0;										
												}
												else if(opt.Code.ToString().Trim() !="G1")
												{
													litem1.Text =db.SelectValue("Bush_Type","WEB_GlandBushing_TableV1","Bush_Code",opt.Code.ToString().Trim());
													DDLBushing.SelectedIndex =DDLBushing.Items.IndexOf(litem1);								
												}
								
											}
										}
										else if(opt.Code.ToString().Trim().Substring(0,1) =="P")
										{
											if(CBPiston.Enabled !=true || CBPiston.Checked !=true )
											{
												CBPiston.Enabled =true;
												CBPiston.Checked =true;
												CBPiston_CheckedChanged(sender,e);
											}
											if(opt.Code.ToString().Trim().Length ==2)
											{
												if(opt.Code.ToString().Trim() =="P2")
												{
													RBLMagnet.SelectedIndex =0;
												}
												else 
												{
													litem1.Text =db.SelectValue("SealConf_Conf","WEB_PistonSealConf_TableV1","SealConf_Code",opt.Code.ToString().Trim());
													DDLPistonSealConfig.SelectedIndex =DDLPistonSealConfig.Items.IndexOf(litem1);
												}
											}
																									
										}
										else if(opt.Code.ToString().Trim().Substring(0,1) =="S")
										{
											if(CBSeal.Enabled !=true || CBSeal.Checked !=true )
											{
												CBSeal.Enabled =true;
												CBSeal.Checked =true;
												CBSeal_CheckedChanged(sender,e);
											}
											if(opt.Code.ToString().Trim().Length ==2)
											{
												litem1.Text =db.SelectValue("SealConf_Conf","WEB_RodSealConf_TableV1","SealConf_Code",opt.Code.ToString().Trim());
												DDLRodSeal.SelectedIndex =DDLRodSeal.Items.IndexOf(litem1);
											}
										}
										else if(opt.Code.ToString().Trim().Substring(0,1) =="D" || opt.Code.ToString().Trim().Substring(0,1) =="R")
										{
											if(CBRod.Enabled !=true || CBRod.Checked !=true )
											{
												CBRod.Enabled =true;
												CBRod.Checked =true;
												CBRod_CheckedChanged(sender,e);
											}
											if(RBLDoubleRod.SelectedIndex !=0)
											{
												RBLDoubleRod.SelectedIndex =0;
												RBLDoubleRod_SelectedIndexChanged(sender,e);
											}
											if(opt.Code.ToString().Trim().Length ==3)
											{
												if(opt.Code.ToString().Trim().Substring(0,1) =="D" )
												{
													litem1.Text =db.SelectValue("SecRod_Diam","WEB_SecRod_TableV1","SecRod_Code",opt.Code.ToString().Trim());
													DDLDiameterSec.SelectedIndex =DDLDiameterSec.Items.IndexOf(litem1);
															
												}
												else if(opt.Code.ToString().Trim().Substring(0,1) =="R" )
												{
													litem1.Text =db.SelectValue("SecRodEnd_Type","WEB_SecRodEnd_TableV1","SecRodEnd_Code",opt.Code.ToString().Trim());
													DDLSecRodEnd.SelectedIndex =DDLSecRodEnd.Items.IndexOf(litem1);
												}
											}

										}
										else if(opt.Code.ToString().Trim().Substring(0,2) =="W1")
										{
											if(CBRod.Enabled !=true || CBRod.Checked !=true )
											{
												CbOther.Enabled =true;
												CbOther.Checked =true;
												CbOther_CheckedChanged(sender,e);
											}
											litem1.Text =db.SelectValue("Coating_Type","WEB_Coating_TableV1","Coating_Code",opt.Code.ToString().Trim());
											DDLCoating.SelectedIndex =DDLCoating.Items.IndexOf(litem1);
										}
								
									}
								}
							}
							if(qitems.PopularOpt_Id !="")
							{
								Opts opt =new Opts();
								ArrayList Popularlist =new ArrayList();
								Popularlist =db.SelectPopts(qitems.Quotation_No.Trim(),qitems.PartNo.Trim());
								for(int k=0;k<Popularlist.Count ;k++)
								{
									opt =(Opts)Popularlist[k];
									ListItem litem1=new ListItem();
									if(opt.Code.ToString().Trim() !="")
									{
										if(opt.Code.ToString().Trim().Substring(0,1) =="A")
										{
											if(CBRod.Enabled !=true || CBRod.Checked !=true )
											{
												CBRod.Enabled =true;
												CBRod.Checked =true;
												CBRod_CheckedChanged(sender,e);
											}
											if(opt.Code.ToString().Trim().Substring(0,2) =="AD")
											{
												if(RBLDoubleRod.SelectedIndex !=0)
												{
													RBLDoubleRod.SelectedIndex =0;
													RBLDoubleRod_SelectedIndexChanged(sender,e);
												}
												TxtSecThreadEx.Text =opt.Code.ToString().Trim().Substring(2);
											}
											else 
											{
												TXTThread.Text =opt.Code.ToString().Trim().Substring(1);
											}

										}
										else if(opt.Code.ToString().Trim().Substring(0,1) =="C")
										{
											if(CbOther.Enabled !=true || CbOther.Checked !=true )
											{
												CbOther.Enabled =true;
												CbOther.Checked =true;
												CbOther_CheckedChanged(sender,e);
											}
											if(opt.Code.ToString().Trim().Substring(0,1) =="C")
											{
												litem1.Text =db.SelectValue("Coating_Type","WEB_Coating_TableV1","Coating_Code",opt.Code.ToString().Trim());
												DDLCoating.SelectedIndex =DDLCoating.Items.IndexOf(litem1);
											}
										}
										else if(opt.Code.ToString().Trim().Substring(0,1) =="M")
										{
											if(opt.Code.ToString().Trim().Substring(0,2) =="M2")
											{
												if(CBRod.Enabled !=true || CBRod.Checked !=true )
												{
													CbOther.Enabled =true;
													CbOther.Checked =true;
													CbOther_CheckedChanged(sender,e);
												}
												RBLTieRod.SelectedIndex =0;
											}
											else if(opt.Code.ToString().Trim().Substring(0,2) =="M1" || opt.Code.ToString().Trim().Substring(0,2) =="M6")
											{
												if(CbOther.Enabled !=true || CbOther.Checked !=true )
												{
													CbOther.Enabled =true;
													CbOther.Checked =true;
													CbOther_CheckedChanged(sender,e);
												}
												litem1.Text =db.SelectValue("Description","WEB_BarrelMaterial_TableV1","Popular_Code",opt.Code.ToString().Trim());
												DDLBarrel.SelectedIndex =DDLBarrel.Items.IndexOf(litem1);
											}
											else if(opt.Code.ToString().Trim().Substring(0,2) =="M3" || opt.Code.ToString().Trim().Substring(0,2) =="M4" || opt.Code.ToString().Trim().Substring(0,2) =="M5")
											{
												if(CBRod.Enabled !=true || CBRod.Checked !=true )
												{
													CBRod.Enabled =true;
													CBRod.Checked =true;
													CBRod_CheckedChanged(sender,e);
												}
												litem1.Text =db.SelectValue("SSPRod_Type","WEB_SSPRod_TableV1","SSPRod_Code",opt.Code.ToString().Trim());
												DDLSSpistinrod.SelectedIndex =DDLSSpistinrod.Items.IndexOf(litem1);
											}
										}
										else if(opt.Code.ToString().Trim().Substring(0,1) =="P")
										{
											if(CBPort.Enabled !=true || CBPort.Checked !=true )
											{
												CBPort.Enabled =true;
												CBPort.Checked =true;
												CBPort_CheckedChanged(sender,e);
											}
											
											if(opt.Code.ToString().Trim().Substring(0,1) =="P")
											{
												litem1.Text =db.SelectValue("AirBleed_Type","WEB_AirBleed_TableV1","AirBleed_Code",opt.Code.ToString().Trim());
												DDLAirbleeds.SelectedIndex =DDLAirbleeds.Items.IndexOf(litem1);
											}
										}
										else if(opt.Code.ToString().Trim().Substring(0,2) =="ST")
										{
											if(CBStroke.Enabled !=true || CBStroke.Checked !=true )
											{
												CBStroke.Enabled =true;
												CBStroke.Checked =true;
												CBStroke_CheckedChanged(sender,e);
											}
											
											if(opt.Code.ToString().Trim().Substring(0,2) =="ST")
											{
												RBStrokeType.SelectedIndex =0;
												TxtStopTube.Text =opt.Code.ToString().Trim().Substring(2);
												decimal dc1,dc2,dc3=0.00m;
												dc1=Convert.ToDecimal(TxtStopTube.Text);
												dc2=Convert.ToDecimal(TxtStroke.Text);
												dc3=dc2 - dc1;
												TxtEffectiveStrok.Text=dc3.ToString();
											}
										}
										else if(opt.Code.ToString().Trim().Substring(0,1) =="D")
										{
											if(opt.Code.ToString().Trim().Substring(0,2) =="DS")
											{
												if(CBStroke.Enabled !=true || CBStroke.Checked !=true )
												{
													CBStroke.Enabled =true;
													CBStroke.Checked =true;
													CBStroke_CheckedChanged(sender,e);
												}
												RBStrokeType.SelectedIndex =1;
												TxtStopTube.Text =opt.Code.ToString().Trim().Substring(3);
												decimal dc1,dc2,dc3=0.00m;
												dc1=Convert.ToDecimal(TxtStopTube.Text);
												dc2=Convert.ToDecimal(TxtStroke.Text);
												dc3=dc2 - dc1;
												TxtEffectiveStrok.Text=dc3.ToString();
											}
											else if(opt.Code.ToString().Trim().Substring(0,2) =="DC")
											{
												if(CBTandem.Enabled !=true || CBTandem.Checked !=true )
												{
													CBTandem.Enabled =true;
													CBTandem.Checked =true;
													TxtSecondStroke.Enabled=true;
													CBTandem_CheckedChanged(sender,e);
												}
												DDLTandomConfig.SelectedIndex =1;
												TxtSecondStroke.Text =opt.Code.ToString().Trim().Substring(2);
											}
									
										}
										else if(opt.Code.ToString().Trim().Substring(0,1) =="W")
										{
											if(opt.Code.ToString().Trim().Substring(0,2) =="WD")
											{
												if(CBRod.Enabled !=true || CBRod.Checked !=true )
												{
													CBRod.Enabled =true;
													CBRod.Checked =true;
													CBRod_CheckedChanged(sender,e);
												}
												if(RBLDoubleRod.SelectedIndex !=0)
												{
													RBLDoubleRod.SelectedIndex =0;
													RBLDoubleRod_SelectedIndexChanged(sender,e);
												}
												TxtSecRodEx.Text =opt.Code.ToString().Trim().Substring(2);
											}
											else 
											{
												TXTRodEx.Text =opt.Code.ToString().Trim().Substring(1);
											}
										}
										else if(opt.Code.ToString().Trim().Substring(0,2) =="XI")
										{
											CBMount.Enabled=true;
											CBMount.Checked=true;	
											PanelMount.Visible=true;
											PnlXI.Visible=true;
											PnlBBC.Visible=false;
											PnlBBH.Visible=false;
											TxtBBCap.Visible =false;
											TxtBBHead.Visible=false;
											TXTIntere.Visible=true;
											lblxi.Visible=true;
											lblbb.Visible=false;
											lblbbc.Visible=false;
											lblbbh.Visible=false;
											TXTIntere.Text=opt.Code.ToString().Trim().Substring(2);
										}
										else if(opt.Code.ToString().Trim().Substring(0,2) =="TC")
										{
											if(CBTandem.Enabled !=true || CBTandem.Checked !=true )
											{
												CBTandem.Enabled =true;
												CBTandem.Checked =true;
												CBTandem_CheckedChanged(sender,e);
											}
											DDLTandomConfig.SelectedIndex =0;
										}
										else if(opt.Code.ToString().Trim().Substring(0,2) =="BC")
										{
											if(CBTandem.Enabled !=true || CBTandem.Checked !=true )
											{
												CBTandem.Enabled =true;
												CBTandem.Checked =true;
												TxtSecondStroke.Enabled=true;
												CBTandem_CheckedChanged(sender,e);
											}
											DDLTandomConfig.SelectedIndex =2;
											TxtSecondStroke.Text =opt.Code.ToString().Trim().Substring(2);
										}
										else if(opt.Code.ToString().Trim().Substring(0,2) =="BB")
										{
										
											if(opt.Code.ToString().Trim().Substring(0,3) =="BBC")
											{
												CBMount.Enabled=true;
												CBMount.Checked=true;	
												PanelMount.Visible=true;
												PnlXI.Visible=false;
												PnlBBC.Visible=true;
												PnlBBH.Visible=false;
												TxtBBCap.Visible =true;
												TxtBBHead.Visible=false;
												TXTIntere.Visible=false;
												lblxi.Visible=false;
												lblbb.Visible=true;
												lblbbc.Visible=true;
												lblbbh.Visible=false;
												TxtBBCap.Text =opt.Code.ToString().Trim().Substring(3);
											}
											else if(opt.Code.ToString().Trim().Substring(0,3) =="BBH")
											{
												CBMount.Enabled=true;
												CBMount.Checked=true;	
												PanelMount.Visible=true;
												PnlBBC.Visible=false;
												PnlBBH.Visible=true;
												TxtBBCap.Visible =false;
												TxtBBHead.Visible=true;
												TXTIntere.Visible=false;
												lblxi.Visible=false;
												lblbb.Visible=true;
												lblbbc.Visible=false;
												lblbbh.Visible=true;
												TxtBBHead.Text =opt.Code.ToString().Trim().Substring(3);
											}
											if(qitems.M_code.Trim() =="X1")
											{
												CBMount.Enabled=true;
												CBMount.Checked=true;	
												PanelMount.Visible=true;
												PnlXI.Visible=false;
												PnlBBC.Visible=true;
												PnlBBH.Visible=true;
												TxtBBCap.Visible =true;
												TxtBBHead.Visible=true;
												TXTIntere.Visible=false;
												lblxi.Visible=false;
												lblbb.Visible=true;
												lblbbc.Visible=true;
												lblbbh.Visible=true;
											}
										}
									}
								}
							}
							litem.Text =db.SelectValue("Mount_Type","WEB_MountZ_TableV1","Mount_Code",qitems.M_code.ToString().Trim());
							DDLMount.SelectedIndex =DDLMount.Items.IndexOf(litem);
							DDLMount_SelectedIndexChanged(sender,e);
						}
					}
				}
				else
				{
					Response.Redirect("Login.aspx");
				}
			}
			catch( Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				
			}
		}
		private string IpAddress() 
		{ 
			string strIpAddress; 
			strIpAddress = Request.ServerVariables[ "HTTP_X_FORWARDED_FOR" ]; 
			if (strIpAddress == null ) 
				strIpAddress = Request.ServerVariables[ "REMOTE_ADDR" ]; 
			return strIpAddress; 

		}		

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.DDLSeries.SelectedIndexChanged += new System.EventHandler(this.DDLSeries_SelectedIndexChanged);
			this.DDLBore.SelectedIndexChanged += new System.EventHandler(this.DDLBore_SelectedIndexChanged);
			this.DDLRod.SelectedIndexChanged += new System.EventHandler(this.DDLRod_SelectedIndexChanged);
			this.TxtStroke.TextChanged += new System.EventHandler(this.TxtStroke_TextChanged);
			this.DDLMount.SelectedIndexChanged += new System.EventHandler(this.DDLMount_SelectedIndexChanged);
			this.DDLPorttype.SelectedIndexChanged += new System.EventHandler(this.DDLPorttype_SelectedIndexChanged);
			this.DDLCushions.SelectedIndexChanged += new System.EventHandler(this.DDLCushions_SelectedIndexChanged);
			this.CBRod.CheckedChanged += new System.EventHandler(this.CBRod_CheckedChanged);
			this.TXTThread.TextChanged += new System.EventHandler(this.TXTThread_TextChanged);
			this.TXTRodEx.TextChanged += new System.EventHandler(this.TXTRodEx_TextChanged);
			this.RBLDoubleRod.SelectedIndexChanged += new System.EventHandler(this.RBLDoubleRod_SelectedIndexChanged);
			this.DDLDiameterSec.SelectedIndexChanged += new System.EventHandler(this.DDLDiameterSec_SelectedIndexChanged);
			this.TxtSecThreadEx.TextChanged += new System.EventHandler(this.TxtSecThreadEx_TextChanged);
			this.TxtSecRodEx.TextChanged += new System.EventHandler(this.TxtSecRodEx_TextChanged);
			this.CBStroke.CheckedChanged += new System.EventHandler(this.CBStroke_CheckedChanged);
			this.TxtStopTube.TextChanged += new System.EventHandler(this.TxtStopTube_TextChanged);
			this.TxtEffectiveStrok.TextChanged += new System.EventHandler(this.TxtEffectiveStrok_TextChanged);
			this.CBSeal.CheckedChanged += new System.EventHandler(this.CBSeal_CheckedChanged);
			this.CBPort.CheckedChanged += new System.EventHandler(this.CBPort_CheckedChanged);
			this.CBMount.CheckedChanged += new System.EventHandler(this.CBMount_CheckedChanged);
			this.TXTIntere.TextChanged += new System.EventHandler(this.TXTIntere_TextChanged);
			this.TxtBBHead.TextChanged += new System.EventHandler(this.TxtBBHead_TextChanged);
			this.TxtBBCap.TextChanged += new System.EventHandler(this.TxtBBCap_TextChanged);
			this.CBPiston.CheckedChanged += new System.EventHandler(this.CBPiston_CheckedChanged);
			this.CBGland.CheckedChanged += new System.EventHandler(this.CBGland_CheckedChanged);
			this.CbOther.CheckedChanged += new System.EventHandler(this.CbOther_CheckedChanged);
			this.CBTandem.CheckedChanged += new System.EventHandler(this.CBTandem_CheckedChanged);
			this.DDLTandomConfig.SelectedIndexChanged += new System.EventHandler(this.DDLTandomConfig_SelectedIndexChanged);
			this.TxtSecondStroke.TextChanged += new System.EventHandler(this.TxtSecondStroke_TextChanged);
			this.CBTransducer.CheckedChanged += new System.EventHandler(this.CBTransducer_CheckedChanged);
			this.DDLTransducer.SelectedIndexChanged += new System.EventHandler(this.DDLTransducer_SelectedIndexChanged);
			this.BTNGenPartNo.Click += new System.EventHandler(this.BTNGenPartNo_Click);
			this.RBSpecialReq.SelectedIndexChanged += new System.EventHandler(this.RBSpecialReq_SelectedIndexChanged);
			this.LbPreview.Click += new System.EventHandler(this.LbPreview_Click);
			this.BtnGeneratePrice.Click += new System.EventHandler(this.BtnGeneratePrice_Click);
			this.BtnSendMail.Click += new System.EventHandler(this.BtnSendMail_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void DDLSeries_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(DDLSeries.SelectedIndex >0)
			{
				CBRod.Enabled=true;
				CBGland.Enabled=true;
				CbOther.Enabled=true;
				CBPiston.Enabled=true;
				CBStroke.Enabled=true;
				CBSeal.Enabled=true;
				CBPort.Enabled=true;
				CBTandem.Enabled=true;
				CBTransducer.Enabled=true;
				LBLPNo.Text="";
				LblInfo.Text="";
				lblgenerateprice.Text="";
				LBLSeries.Text="";
				CBGland.Checked=false;
				CBMount.Checked=false;
				CBPiston.Checked=false;
				CBRod.Checked=false;
				CBSeal.Checked=false;
				CBStroke.Checked=false;
				CbOther.Checked=false;
				CBTandem.Checked=false;
				CBTransducer.Checked=false;
				CBPort.Checked	=false;
				//issue #98 end
				Session["CushionDeletedText"]="";
				Session["CushionDeletedValue"]="";
				//issue #98 start	
				if(CBRod.Checked ==false)
				{
					PanelRod.Visible=false;
				}
				if(CBStroke.Checked == false)
				{
					PanelStroke.Visible=false;
				}
				if(CBSeal.Checked == false)
				{
					PanelSeal.Visible=false;
				}
				if(CBMount.Checked == false)
				{		
					PanelMount.Visible=false;
				}
				if(CBGland.Checked == false)
				{
					PanelGland.Visible=false;
				}
				if(CBPiston.Checked == false)
				{
					PanelPiston.Visible=false;
				}
				if(CbOther.Checked ==false)
				{
					PanelOther.Visible=false;
				}
				if(CBTandem.Checked ==false)
				{				
					PNTandem.Visible=false;
				}
				if(CBTransducer.Checked ==false)
				{				
					PNTransducer.Visible=false;
				}
				if(CBPort.Checked ==false)
				{				
					PanelPort.Visible=false;
				}
				CBTransducer.Enabled=false;		
				ArrayList selist =new ArrayList();
				selist =(ArrayList)Session["User"];
				string str ="";
				string s="";
				str=db.SelectSeriesCode(DDLSeries.SelectedItem.ToString());
				lblD.Text =db.SelectDiscount(selist[1].ToString(),str.Trim());
				ArrayList list = new ArrayList();
				switch (str.Trim())					   
				{
					case "PA":
						s="SeriesPA"; 
						CBSeal.Enabled=true;
						RBLMagnet.Enabled=true;
						RBDrain.Enabled=false;
						CBMount.Enabled=false;
						break;
					case "PS":
						s="SeriesPS"; 
						CBSeal.Enabled=true;
						RBLMagnet.Enabled=true;
						RBDrain.Enabled=false;
						CBMount.Enabled=false;
						break;
					case "PC":
						s="SeriesPC";
						CBSeal.Enabled=true;
						RBLMagnet.Enabled=true;
						RBDrain.Enabled=false;
						CBMount.Enabled=false;
						break;
					case "M":
						s="SeriesM";
						CBSeal.Enabled=true;
						RBLMagnet.Enabled=false;
						RBDrain.Enabled=true;
						CBMount.Enabled=false;
						break;
					case "N":
						s="SeriesN"; 
						CBSeal.Enabled=true;
						RBLMagnet.Enabled=false;
						RBDrain.Enabled=true;
						CBMount.Enabled=false;
						break;
					case "L":
						s="SeriesL"; 
						CBSeal.Enabled=true;
						RBLMagnet.Enabled=false;
						RBDrain.Enabled=true;
						CBMount.Enabled=false;
						CBTandem.Enabled=false;
						break;
					case "ML":
						s="SeriesML"; 
						CBSeal.Enabled=true;
						RBLMagnet.Enabled=false;
						RBDrain.Enabled=true;
						CBMount.Enabled=false;
						break;
					case "MH":
						s="SeriesMH"; 
						CBSeal.Enabled=true;
						RBLMagnet.Enabled=false;
						RBDrain.Enabled=true;
						CBMount.Enabled=false;
						break;
					case "R":
						s="SeriesR";
						CBSeal.Enabled=true;
						RBLMagnet.Enabled=false;
						RBDrain.Enabled=true;
						CBMount.Enabled=false;
						break;
					case "RP":
						s="SeriesRP";
						CBSeal.Enabled=true;
						RBLMagnet.Enabled=false;
						RBDrain.Enabled=true;
						CBMount.Enabled=false;
						break;
					case "A":
						s="SeriesA"; 
						CBSeal.Enabled=true;
						RBLMagnet.Enabled=true;
						RBDrain.Enabled=false;
						CBMount.Enabled=false;									
						break;
					case "AT":
						s="SeriesAT"; 
						CBSeal.Enabled=true;
						RBLMagnet.Enabled=true;
						RBDrain.Enabled=false;
						CBMount.Enabled=false;
						CBTransducer.Enabled=true;							
						break;
					case "SA":
						s="SeriesA"; 
						CBSeal.Enabled=true;
						RBLMagnet.Enabled=true;
						RBDrain.Enabled=false;
						CBMount.Enabled=false;					
						break;
						//UMI start
					case "R5":
						s="SeriesR5"; 
						CBSeal.Enabled=true;
						RBLMagnet.Enabled=true;
						RBDrain.Enabled=false;
						CBMount.Enabled=false;					
						break;
						//UMI End
				}
				list =db.SelectBore(s);
				DDLBore.Items.Clear();
				DDLBore.Items.Add("Select Bore Size");
				if(str.Trim() !="SA")
				{
					for (int i=0;i<= list.Count -1;i++)
					{
						DDLBore.Items.Add(list[i].ToString());
					}
				}
				else
				{
					for (int i=2;i<= 10;i++)
					{
						DDLBore.Items.Add(list[i].ToString());
					}
				}
				lbl.Text=s.ToString();
				ArrayList list2 = new ArrayList();
				DDLCushions.Items.Clear();
				DDLCushions.Items.Add("Select Cushions");
				list2=db.SelectCushion(lbl.Text.Trim());
				for (int i=0;i<= list2.Count -1;i++)
				{
					DDLCushions.Items.Add(list2[i].ToString());
				}
				DDLCushions.SelectedIndex =1;
				DDLCushions_SelectedIndexChanged(sender,e);
				ArrayList list7 = new ArrayList();
				list7=db.SelectSeal(lbl.Text.Trim());
				DDLSeal.Items.Clear();
				DDLSeal.Items.Add("Select Seal");
				for (int i=0;i<= list7.Count -1;i++)
				{
					DDLSeal.Items.Add(list7[i].ToString());
				}
				if(str.Trim() !="SA")
				{
					DDLSeal.SelectedIndex =1;
				}
				else
				{	
					DDLSeal.SelectedIndex =3;	
				}
				ArrayList list21= new ArrayList();
				DDLPorttype.Items.Clear();
				DDLPorttype.Items.Add("Select Ports");
				list21=db.SelectPortType(lbl.Text.Trim());
				for (int i=0;i<= list21.Count -1;i++)
				{
					DDLPorttype.Items.Add(list21[i].ToString());
				}
				//UMI Start
				//if(lbl.Text.Trim() !="SeriesA")
				//{
					//if(lbl.Text.Trim() =="SeriesPA" || lbl.Text.Trim() =="SeriesPS" || lbl.Text.Trim() =="SeriesPC")
					//{
						//DDLPorttype.SelectedIndex =2;
					//}
					//else
					//{
						//DDLPorttype.SelectedIndex =1;
					//}
				//}
                //UMI End
				if(lbl.Text.Trim() =="SeriesPA" || lbl.Text.Trim() =="SeriesPS" || lbl.Text.Trim() =="SeriesPC" || lbl.Text.Trim() =="SeriesRP" || lbl.Text.Trim() =="SeriesA" || lbl.Text.Trim() =="SeriesAT")
				{
					try
					{
						DDLPorttype.SelectedIndex =Get_Index_DDLPorttype("NPT Ports");
					}
					catch
					{
						DDLPorttype.SelectedIndex =1;
					}
				}
				else if (lbl.Text.Trim() =="SeriesR5" || lbl.Text.Trim() =="SeriesM" || lbl.Text.Trim() =="SeriesN" || lbl.Text.Trim() =="SeriesL" || lbl.Text.Trim() =="SeriesML" || lbl.Text.Trim() =="SeriesR")
				{
					try
					{
						DDLPorttype.SelectedIndex =Get_Index_DDLPorttype("SAE Ports");
					}
					catch
					{
                      DDLPorttype.SelectedIndex =1;
					}
				}
				else
				{
					DDLPorttype.SelectedIndex =1;
				}
				DDLPorttype_SelectedIndexChanged(sender,e);				
				//				DDLCushionPosition.Items.Clear();
				//				DDLPortPos.Items.Clear();
				//				DDLMount.Items.Clear();
				TxtStroke.Text="";
	
			}

		}
		int Get_Index_DDLPorttype(string defaultvalue)
		{
			int Index=0;
			int counter=0;
			foreach(ListItem i in DDLPorttype.Items)
			{
				if(i.Text.Trim()==defaultvalue) Index=counter;
                counter+=1;
			}
			return Index;
		}

		private void DDLBore_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(DDLBore.SelectedIndex >0)
			{
				
				string str ="";
				string tab = "";
				str=db.SelectBoreCode(DDLBore.SelectedItem.ToString());
				lblbor.Text = str.ToString();
				ArrayList list = new ArrayList();
				switch (lbl.Text.Trim())					   
				{
					case "SeriesPA":
						tab ="WEB_RodSerP_TableV1";
						list =db.SelectRod(tab,str); break;
					case "SeriesPS":
						tab ="WEB_RodSerP_TableV1";
						list =db.SelectRod(tab,str); break;
					case "SeriesPC":
						tab ="WEB_RodSerP_TableV1";
						list =db.SelectRod(tab,str); break;
					case "SeriesM":
						tab ="WEB_RodSerM_TableV1";
						list =db.SelectRod(tab,str); break;
					case "SeriesN":
						tab ="WEB_RodSerN_TableV1";
						list =db.SelectRod(tab,str); break;
					case "SeriesL":
						tab ="WEB_RodSerL_TableV1";
						list =db.SelectRod(tab,str); break;
					case "SeriesML":
						tab ="WEB_RodSerM_TableV1";
						list =db.SelectRod(tab,str); break;
					case "SeriesMH":
						tab ="WEB_RodSerM_TableV1";
						list =db.SelectRod(tab,str); break;
					case "SeriesR":
						tab ="WEB_RodSerR_TableV1";
						list =db.SelectRod(tab,str); break;
					case "SeriesRP":
						tab ="WEB_RodSerRP_TableV1";
						list =db.SelectRod(tab,str); break;
					case "SeriesA":
						tab ="WEB_RodSerA_TableV1";
						list =db.SelectRod(tab,str); break;
					case "SeriesAT":
						tab ="WEB_RodSerAT_TableV1";
						list =db.SelectRod(tab,str); 
						CBTransducer.Checked=true;
						CBTransducer_CheckedChanged(sender,e);
						break;
					case "SeriesR5":
						tab ="WEB_RodSerR5_TableV1";
						list =db.SelectRod(tab,str); break;
				}			
				DDLRod.Items.Clear();
				DDLRod.Items.Add("Select Rod Size");
				for (int i=0;i<= list.Count -1;i++)
				{
					DDLRod.Items.Add(list[i].ToString());
				}
				DDLDiameterSec.Items.Clear();
				DDLDiameterSec.Items.Add("Select Second Rod Size");
				for (int i=0;i<= list.Count -1;i++)
				{
					DDLDiameterSec.Items.Add(list[i].ToString());
				}
				if(lbl.Text.ToString().Trim() =="SeriesPC")
				{
					string cd1="";
					cd1=db.SelectCode("Bore_Code","WEB_Bore_TableV1","Bore_Size",DDLBore.SelectedItem.ToString());
					if(cd1.ToString().Trim() =="C" || cd1.ToString().Trim() =="D" || cd1.ToString().Trim() =="E")
					{
						ArrayList list2 = new ArrayList();
						DDLCushions.Items.Clear();
						DDLCushions.Items.Add("Select Cushions");
						list2=db.SelectCushion(lbl.Text.Trim()+"1");
						for (int i=0;i<= list2.Count -1;i++)
						{
							DDLCushions.Items.Add(list2[i].ToString());
						}
						DDLCushions.SelectedIndex =1;
						DDLCushions_SelectedIndexChanged(sender,e);
					}
				}
				//issue #98 start
				ListItem lstItemCushionDeleted=null;
				string strBoreValue= DDLBore.SelectedIndex.ToString();
				strBoreValue=DDLBore.SelectedItem.Text;
				strBoreValue=DDLBore.SelectedItem.Value;
				if(DDLBore.SelectedItem.Value!="3 1/4\"" && DDLBore.SelectedItem.Value!="4\"")
				{
					for(int i=0;i<DDLCushions.Items.Count;i++)
					{
						strBoreValue=DDLCushions.Items[i].Value;
						strBoreValue=DDLCushions.Items[i].Text;
						if (DDLCushions.Items[i].Text.Trim()=="High Impact Cushions")
						{
							Session["CushionDeletedText"]=DDLCushions.Items[i].Text;
							Session["CushionDeletedValue"]=DDLCushions.Items[i].Value;
							DDLCushions.Items.RemoveAt(i);
						}
					}
					strBoreValue="Right";
				}
				else if(DDLBore.SelectedItem.Value=="3 1/4\"" || DDLBore.SelectedItem.Value=="4\"")
				{
					if(Session["CushionDeletedText"]!=null && Session["CushionDeletedValue"]!=null)
					{
						if(Session["CushionDeletedText"].ToString()!="" && Session["CushionDeletedValue"].ToString()!="")
						{
							if(DDLCushions.Items.FindByText("High Impact Cushions")==null)
							{
								lstItemCushionDeleted = new ListItem();
								lstItemCushionDeleted.Text = (string)Session["CushionDeletedText"];
								lstItemCushionDeleted.Value = (string)Session["CushionDeletedValue"];
								DDLCushions.Items.Add(lstItemCushionDeleted);
							}
						}
					}
				}
				//issue #98 end
			}
		}
		private void BTNGenPartNo_Click(object sender, System.EventArgs e)
		{
			//issue #90 start
			decimal dcStrokeThr = 59.99m;
			//issue #90 end
			if(DDLSeries.SelectedIndex < 1 || DDLBore.SelectedIndex < 1 || DDLRod.SelectedIndex < 1 ||DDLRodEnd.SelectedIndex < 1 || DDLMount.SelectedIndex < 1 ||DDLPorttype.SelectedIndex < 1 || DDLCushions.SelectedIndex < 1 ||DDLSeal.SelectedIndex < 1 )
			{
			
				LblInfo.Text="Select all basic options !!!";
				if(DDLSeries.SelectedIndex < 1 )
				{
					DDLSeries.ForeColor =Color.Red;
				}
				if(DDLBore.SelectedIndex < 1 )
				{
					DDLBore.ForeColor =Color.Red;
				}
				if(DDLRod.SelectedIndex < 1 )
				{
					DDLRod.ForeColor =Color.Red;
				}
				if(DDLRodEnd.SelectedIndex < 1 )
				{
					DDLRodEnd.ForeColor =Color.Red;
				}
				if(DDLMount.SelectedIndex < 1 )
				{
					DDLMount.ForeColor =Color.Red;
				}
				if(DDLPorttype.SelectedIndex < 1 )
				{
					DDLPorttype.ForeColor =Color.Red;
				}
				if(DDLCushions.SelectedIndex < 1 )
				{
					DDLCushions.ForeColor =Color.Red;
				}
				if(DDLSeal.SelectedIndex < 1 )
				{
					DDLSeal.ForeColor =Color.Red;
				}
			}
			else
			{
				lblgenerateprice.Visible=false;
				BtnGeneratePrice.Visible=true;
								
				PNo.Series =db.SelectSeriesCode(DDLSeries.SelectedItem.ToString());
				PNo.Bore_Size=db.SelectCode("Bore_Code","WEB_Bore_TableV1","Bore_Size",DDLBore.SelectedItem.ToString());
				PNo.Rod_Diamtr=db.SelectCode("Rod_Code","WEB_RodSerZ_TableV1","Rod_Size",DDLRod.SelectedItem.ToString());
				PNo.Rod_End=db.SelectCode("RodEnd_Code","WEB_RodEndKK_TableV1","RodEnd_Shape",DDLRodEnd.SelectedItem.ToString());
				PNo.Mount=db.SelectCode("Mount_Code","WEB_Mount"+PNo.Series.Trim()+"_TableV1","Mount_Type",DDLMount.SelectedItem.ToString());	
				PNo.Stroke=TxtStroke.Text.ToString().Trim();
				//issue #90 start
				PNo.TieRodeCentSupp="";
				if(IsNumeric(TxtStroke.Text)==true)
				{
					if( Convert.ToDecimal(TxtStroke.Text)> dcStrokeThr)
					{
						if(PNo.Series.Trim() =="PA" || PNo.Series.Trim() =="PC" || PNo.Series.Trim() =="PS")
						{
							PNo.TieRodeCentSupp="CS";
								
						}
						else PNo.TieRodeCentSupp="";
					}
					else PNo.TieRodeCentSupp="";
				}
				//issue #90 end
				PNo.Cushions=db.SelectCode("Cushion_Code","WEB_Cushion_TableV1","Cushion_type",DDLCushions.SelectedItem.ToString());
				if(DDLCushionPosition.Items.Count <1)
				{
					PNo.CushionPosition = "";
				}	
				else	
				{
					PNo.CushionPosition=db.SelectCode("CushionPos_Code","WEB_CushionPos_TableV1","CushionPos_Pos",DDLCushionPosition.SelectedItem.ToString());	
				}
				PNo.Seal_Comp=db.SelectCode("Seal_Code","WEB_Seal_TableV1","Seal_Type",DDLSeal.SelectedItem.ToString());	
				PNo.Port_Type=db.SelectCode("PortType_Code","WEB_portType_TableV1","PortType_Type",DDLPorttype.SelectedItem.ToString());	
				PNo.Port_Pos=db.SelectCode("PortPos_Code","WEB_PortPosition_TableV1","PortPos_Position",DDLPortPos.SelectedItem.ToString());	
			

				if(PanelRod.Visible == false)
				{
					PNo.RodEx="";
					PNo.ThreadEx="";
					PNo.SSPistionRod="";
					PNo.SecRodDiameter="";
					PNo.SecRodEnd="";
					PNo.SecRodEx="";
					PNo.SecRodThreadx="";
				}

				else if(PanelRod.Visible == true)
				{
					if(TXTRodEx.Text.ToString().Trim() !="")
					{
						PNo.RodEx="W"+TXTRodEx.Text.ToString().Trim();
					}
					else
					{
						PNo.RodEx="";
					}
					if(TXTThread.Text.ToString().Trim() !="")
					{
						PNo.ThreadEx="A"+TXTThread.Text.ToString().Trim();
					}
					else
					{
						PNo.ThreadEx="";
					}
					
					if(DDLSSpistinrod.SelectedItem.Text.Trim() == "Standard")
					{
						PNo.SSPistionRod="";
					}
					else
					{
						PNo.SSPistionRod=db.SelectCode("SSPRod_Code","WEB_SSPRod_TableV1","SSPRod_Type",DDLSSpistinrod.SelectedItem.ToString());	
					}		
				
					if(RBLDoubleRod.SelectedIndex ==0)
					{
						if(DDLDiameterSec.SelectedIndex !=0)
						{
							PNo.SecRodDiameter=db.SelectCode("SecRod_Code","WEB_SecRod_TableV1","SecRod_Diam",DDLDiameterSec.SelectedItem.ToString());	
					
						}
						else
						{
							PNo.SecRodDiameter="";
						}
						if(DDLSecRodEnd.SelectedIndex !=-1)
						{
							PNo.SecRodEnd=db.SelectCode("SecRodEnd_Code","WEB_SecRodEnd_TableV1","SecRodEnd_Type",DDLSecRodEnd.SelectedItem.ToString());	
					
						}
						else
						{
							PNo.SecRodEnd="";
						}
						if(TxtSecRodEx.Text.ToString().Trim() !="")
						{
							PNo.SecRodEx="WD"+TxtSecRodEx.Text.ToString().Trim();
						}
						else
						{
							PNo.SecRodEx="";
						}
						if(TxtSecThreadEx.Text.ToString().Trim() !="")
						{
							PNo.SecRodThreadx="AD"+TxtSecThreadEx.Text.ToString().Trim();
						}
						else
						{
							PNo.SecRodThreadx="";
						}
					}
					else if(RBLDoubleRod.SelectedIndex ==1)
					{
						PNo.SecRodDiameter="";	
						PNo.SecRodEnd="";
						PNo.SecRodEx="";
						PNo.SecRodThreadx="";
					}
				
			
				}
				if(PanelMount.Visible ==false)
				{
					PNo.Trunnions="";
					PNo.BBC="";
					PNo.BBH="";
				}
				else if(PanelMount.Visible ==true)
				{
					if(TXTIntere.Text.ToString().Trim() !="")
					{
						PNo.Trunnions="XI"+TXTIntere.Text.ToString().Trim();
					}
					else
					{
						PNo.Trunnions="";
					}
					if(TxtBBCap.Text.ToString().Trim() !="")
					{
						PNo.BBC="BBC"+TxtBBCap.Text.ToString().Trim();
					}
					else
					{
						PNo.BBC="";
					}
					if(TxtBBHead.Text.ToString().Trim() !="")
					{
						PNo.BBH="BBH"+TxtBBHead.Text.ToString().Trim();
					}
					else
					{
						PNo.BBH="";
					}
				}
			
				if(PanelStroke.Visible==false)
				{
					PNo.StopTube="" ;
				
				}
				else if(PanelStroke.Visible==true)
				{
					if(RBStrokeType.SelectedIndex ==0)
					{
						if(TxtStopTube.Text.Trim() !="")
						{
							PNo.StopTube="ST"+TxtStopTube.Text.ToString().Trim();
						}
					}
					else
					{
						if(TxtStopTube.Text.Trim() !="")
						{
							PNo.StopTube="DST"+TxtStopTube.Text.ToString().Trim();
						}
					}
				}
			
				if(PanelGland.Visible==false)
				{
					PNo.MetalScrapper="";
					PNo.GlandDrain="";
					PNo.Bushing_Conf="";
				}
				else if(PanelGland.Visible==true)
				{
					PNo.MetalScrapper="";
					PNo.GlandDrain="";
					PNo.Bushing_Conf="";
					if(DDLMetalScrappeer.SelectedIndex !=0)
					{
						PNo.MetalScrapper= db.SelectCode("MS_Code","WEB_MetalScrapper_TableV1","MetalScrapper",DDLMetalScrappeer.SelectedItem.ToString());	
					}
					if(RBDrain.SelectedIndex ==0)
					{
						PNo.GlandDrain= RBDrain.SelectedItem.Value.ToString();
					}
					if(DDLBushing.SelectedIndex !=0)
					{
						PNo.Bushing_Conf= db.SelectCode("Bush_Code","WEB_GlandBushing_TableV1","Bush_Type",DDLBushing.SelectedItem.ToString());	
					}
				}

				if(PanelSeal.Visible==false)
				{
					PNo.RodSeal_Conf="";
				
				}
				else if(PanelSeal.Visible==true)
				{
 
					PNo.RodSeal_Conf="";

					if(DDLRodSeal.SelectedIndex !=0)
					{
						PNo.RodSeal_Conf=db.SelectCode("SealConf_Code","WEB_RodSealConf_TableV1","SealConf_Conf",DDLRodSeal.SelectedItem.ToString());
				
					}
				}

				if(PanelPort.Visible==false)
				{
					PNo.PortSize="";
					PNo.AirBleed="";
				}
				else 	if(PanelPort.Visible==true)
				{
					PNo.AirBleed="";
					if(DDLAirbleeds.Items.Count >0 && DDLAirbleeds.SelectedIndex !=0)
					{
						PNo.AirBleed=db.SelectCode("AirBleed_Code","WEB_AirBleed_TableV1","AirBleed_Type",DDLAirbleeds.SelectedItem.ToString());	
					}
					if(DDLPortSize.SelectedIndex !=0)
					{
						PNo.PortSize=db.SelectCode("PortSize_Code","WEB_PortSize_TableV1","PortSize_Type",DDLPortSize.SelectedItem.ToString());	
		
					}
					else
					{
						PNo.PortSize="";
					}	
				}
				if(PanelPiston.Visible==false)
				{
					PNo.PistonSeal_Conf ="";
					PNo.MagnetRing="";
				}
				else if(PanelPiston.Visible ==true)
				{
					PNo.PistonSeal_Conf ="";
					PNo.MagnetRing="";
					if(DDLPistonSealConfig.SelectedIndex > 0)
					{
						PNo.PistonSeal_Conf=db.SelectCode("SealConf_Code","WEB_PistonSealConf_TableV1","SealConf_Conf",DDLPistonSealConfig.SelectedItem.ToString());
					}
					if(RBLMagnet.SelectedIndex==0)
					{
						PNo.MagnetRing=RBLMagnet.SelectedItem.Value.ToString();
					}
				}
				if(PanelOther.Visible==false)
				{
					PNo.Coating="";
					PNo.SSTieRod="";
					PNo.CarbonFibBarrel="";
				}
				else
				{
					if(DDLCoating.Items.Count>0)
					{
						PNo.Coating=db.SelectCode("Coating_Code","WEB_Coating_TableV1","Coating_Type",DDLCoating.SelectedItem.ToString());	
					}
					else
					{
						PNo.Coating="";
					}
					PNo.SSTieRod="";
					PNo.CarbonFibBarrel="";
					if(RBLTieRod.SelectedIndex==0)
					{
						PNo.SSTieRod=RBLTieRod.SelectedItem.Value.ToString();
					}
					if(DDLBarrel.SelectedIndex > 0)
					{
						PNo.CarbonFibBarrel=db.SelectCode("Popular_Code","WEB_BarrelMaterial_TableV1","Description",DDLBarrel.SelectedItem.Text.ToString().Trim());
					}

				}
				if(RBLDoubleRod.SelectedIndex ==0 && PanelRod.Visible == true)
				{
					PNo.DoubleRod ="Yes";
				}
				else
				{
					PNo.DoubleRod ="No";
				}


				if(PNTandem.Visible ==false)
				{
					PNo.TandemDuplex ="";
				}
				else
				{
					if(DDLTandomConfig.SelectedIndex ==0)
					{
						PNo.TandemDuplex =DDLTandomConfig.SelectedItem.Value.ToString();
					}
					if(DDLTandomConfig.SelectedIndex >0)
					{
						PNo.TandemDuplex =DDLTandomConfig.SelectedItem.Value.ToString()+TxtSecondStroke.Text.Trim();
					}
				
				}
				if(PNTransducer.Visible ==false)
				{
					PNo.Transducer ="";
				}
				else
				{
					if(DDLTransducer.SelectedItem.Value.ToString().Trim().Substring(0,2) !="T1" && DDLTransducer.SelectedItem.Value.ToString().Trim().Substring(0,2) !="T2")
					{
						string trans="";
						trans =DDLTransducer.SelectedItem.Value.ToString().Trim();
						if(RBLRemote.SelectedIndex ==0)
						{
							PNo.Transducer=trans.Insert(2,"R");									
						}
						else
						{
							if(RBLFittings.SelectedIndex ==1)
							{
								PNo.Transducer=trans.Insert(2,"S");
							}
							else
							{
								PNo.Transducer=trans.Trim();
							}
						}
					}
					else if(DDLTransducer.SelectedItem.Value.ToString().Trim().Substring(0,2) =="T1" || DDLTransducer.SelectedItem.Value.ToString().Trim().Substring(0,2) =="T2")
					{
						string trans="";
						if(TxtTransducer.Text.Trim() !="10")
						{
							trans=TxtTransducer.Text.Trim();
						}
						string tr="";
						tr =DDLTransducer.SelectedItem.Value.ToString().Trim()+trans.Trim();
						if(RBLRemote.SelectedIndex ==0)
						{
							PNo.Transducer=tr.Insert(2,"R");									
						}
						else
						{
							if(RBLFittings.SelectedIndex ==1)
							{
								PNo.Transducer=tr.Insert(2,"S");
							}
							else
							{
								PNo.Transducer=tr.Trim();
							}
						}
					}
				}

				if(PNo.SecRodDiameter.Trim() !="")
				{
					PNo.Application_Opt.Add(PNo.SecRodDiameter);
				}
				
				if(PNo.GlandDrain.Trim() !="")
				{
					PNo.Application_Opt.Add(PNo.GlandDrain);	
				}
				if(PNo.Bushing_Conf.Trim() !="")
				{
					PNo.Application_Opt.Add(PNo.Bushing_Conf);	
				}
				if(PNo.MetalScrapper.Trim() !="")
				{
					PNo.Application_Opt.Add(PNo.MetalScrapper);	 
				}
				if(PNo.PistonSeal_Conf.Trim() !="")
				{
					if(PNo.PistonSeal_Conf.Trim() =="P1")
					{
						PNo.Application_Opt.Add(PNo.PistonSeal_Conf);
					}
				}
				if(PNo.MagnetRing.Trim() !="")
				{
					PNo.Application_Opt.Add(PNo.MagnetRing);	
				}
				if(PNo.PistonSeal_Conf.Trim() !="")
				{
					if(PNo.PistonSeal_Conf.Trim() !="P1")
					{
						PNo.Application_Opt.Add(PNo.PistonSeal_Conf);
					}
				}
				if(PNo.SecRodEnd.Trim() !="")
				{
					PNo.Application_Opt.Add(PNo.SecRodEnd);	 
				}
				if(PNo.RodSeal_Conf.Trim() !="")
				{
					PNo.Application_Opt.Add(PNo.RodSeal_Conf);	 
				}
				if(PNo.Coating.Trim() !="")
				{
					if(PNo.Coating.Substring(0,1)=="W")
					{
						PNo.Popular_Opt.Add(PNo.Coating); 
					}
						 
				}
				//issue #90 start
				if(PNo.TieRodeCentSupp.Trim() !="")
				{
					PNo.Application_Opt.Add(PNo.TieRodeCentSupp);	 
				}
				//issue #90 end 
				if(PNo.ThreadEx.Trim() !="")
				{
					PNo.Popular_Opt.Add(PNo.ThreadEx);	 
				}
				if(PNo.SecRodThreadx.Trim() !="")
				{
					PNo.Popular_Opt.Add(PNo.SecRodThreadx);	 
				}
				if(PNo.BBC.Trim() !="")
				{
					PNo.Popular_Opt.Add(PNo.BBC);	 
				}
				if(PNo.BBH.Trim() !="")
				{
					PNo.Popular_Opt.Add(PNo.BBH);	 
				}
				if(PNo.TandemDuplex.Trim() !=""  )
				{
					if(PNo.TandemDuplex.Substring(0,1)=="B")
					{
						PNo.Popular_Opt.Add(PNo.TandemDuplex);	 
					}
				}
				if(PNo.Coating.Trim() !="")
				{
					if(PNo.Coating.Substring(0,1)!="W")
					{
						PNo.Popular_Opt.Add(PNo.Coating); 
					}
						 
				}
				if(PNo.TandemDuplex.Trim() !=""  )
				{
					if(PNo.TandemDuplex.Substring(0,1)=="D")
					{
						PNo.Popular_Opt.Add(PNo.TandemDuplex);	 
					}
				}
				if(PNo.StopTube!=null && PNo.StopTube.Trim() !="")
				{
					if(PNo.StopTube.Substring(0,1)=="D")
					{
						PNo.Popular_Opt.Add(PNo.StopTube);	 
					}
				}
				if(PNo.CarbonFibBarrel.Trim() !="")
				{
					if(PNo.CarbonFibBarrel.Substring(0,2)=="M1")
					{
						PNo.Popular_Opt.Add(PNo.CarbonFibBarrel);	 
					}
				}	
				if(PNo.SSTieRod.Trim() !="")
				{
					PNo.Popular_Opt.Add(PNo.SSTieRod);	 
				}
				if(PNo.SSPistionRod.Trim() !="")
				{
					PNo.Popular_Opt.Add(PNo.SSPistionRod);	 
				}
				if(PNo.CarbonFibBarrel.Trim() !="")
				{
					if(PNo.CarbonFibBarrel.Substring(0,2)=="M6")
					{
						PNo.Popular_Opt.Add(PNo.CarbonFibBarrel);	 
					}
				}	
				if(PNo.AirBleed.Trim() !="")
				{
					PNo.Popular_Opt.Add(PNo.AirBleed);	 
				}
				if(PNo.PortSize.Trim() !="")
				{
					PNo.Popular_Opt.Add(PNo.PortSize);	 
				}
				if(PNo.StopTube!=null && PNo.StopTube.Trim() !="")
				{
					if(PNo.StopTube.Substring(0,1)=="S")
					{
						PNo.Popular_Opt.Add(PNo.StopTube);	 
					}
				}
				if(PNo.TandemDuplex.Trim() !=""  )
				{
					if(PNo.TandemDuplex.Substring(0,1)=="T")
					{
						PNo.Popular_Opt.Add(PNo.TandemDuplex);	 
					}
				}
				if(PNo.RodEx.Trim() !="")
				{
					PNo.Popular_Opt.Add(PNo.RodEx);	 
				}
				if(PNo.SecRodEx.Trim() !="")
				{
					PNo.Popular_Opt.Add(PNo.SecRodEx);	 
				}
				if(PNo.Trunnions.Trim() !="")
				{
					PNo.Popular_Opt.Add(PNo.Trunnions);	 
				}
				if(PNo.Transducer.Trim() !="")
				{
					PNo.Popular_Opt.Add(PNo.Transducer); 
				}
				string appoptcode="";
				for(int a=0;a<PNo.Application_Opt.Count;a++)
				{
					appoptcode +=PNo.Application_Opt[a].ToString();
				}
				string popoptcode="";
				for(int p=0;p<PNo.Popular_Opt.Count;p++)
				{
					popoptcode +=PNo.Popular_Opt[p].ToString();
				}
				string series="";
				if(PNo.Series.Trim() =="SA")
				{
					series="A";
				}
				else
				{
					series=PNo.Series.Trim();
				}
				PNo.PNO =series.ToString().Trim()+PNo.Bore_Size.ToString().Trim()+PNo.Rod_Diamtr.ToString().Trim()+
					PNo.Rod_End.ToString().Trim()+PNo.Cushions.ToString().Trim()+PNo.CushionPosition.ToString().Trim()+
					appoptcode.Trim()+ PNo.Seal_Comp.ToString().Trim()+PNo.Port_Type.ToString().Trim()+PNo.Port_Pos.ToString().Trim()+
					PNo.Mount.ToString().Trim()+PNo.Stroke.ToString().Trim()+popoptcode.Trim();		
			
				Session["PartNo"]= PNo;
				if(Session["PartNo"] != null)
				{
					PartNumber PN= (PartNumber)Session["PartNo"];
					
					string stri="";
					for (int k=0;k<PNo.Application_Opt.Count; k++)
					{
						stri +=(PNo.Application_Opt[k].ToString().Trim());
					}
					
					decimal f =120.00m;
					string st="";
					if(Convert.ToDecimal(PNo.Stroke.ToString()) > f)
					{
						st="For pricing please consult factory";
					}
					string strn="";
					for (int t=0;t<PNo.Popular_Opt.Count; t++)
					{
						strn +=(PNo.Popular_Opt[t].ToString().Trim());
					}
					LBLPNo.Text=PNo.PNO.ToString();
					string sto ="";
					if(PNo.StopTube!=null && PNo.StopTube.ToString() !="")
					{
						decimal a =0.00m;
						decimal b =0.00m;
						decimal c =0.00m;
						string ss="";
						if(PNo.StopTube.Trim().Substring(0,3) =="DST")
						{
							ss=PNo.StopTube.Trim().Substring(3);
						}
						else
						{
							ss=PNo.StopTube.Trim().Substring(2);
						}
						a =Convert.ToDecimal(ss);
						b= Convert.ToDecimal(PNo.Stroke.ToString());
						c= b - a;
						sto =" (Effective Stroke ="+ c.ToString()+"\")";
					}
					string s=PNo.Rod_Diamtr.ToString().Trim()+PNo.Rod_End.ToString().Trim();
					string rodenddim ="";
					if(db.SelectRRD(s).Trim() !="")
					{
						rodenddim =" KK = "+db.SelectRRD(s);
					}
					string portsiz="";
					
					string table="";
					if(PNo.Series.Trim().Substring(0,1) =="A" || PNo.Series.Trim().Substring(0,1) =="S")
					{
						table ="WEB_SeriesAPortSize_TableV1";
					}
					else if(PNo.Series.Trim().Substring(0,1) =="P" || PNo.Series.Trim().Substring(0,1) =="N")
					{
						table ="WEB_SeriesPNPortSize_TableV1";
					}
					else if(PNo.Series.Trim().Substring(0,1) =="M" || PNo.Series.Trim().Substring(0,1) =="L" || PNo.Series.Trim().Substring(0,1) =="R")
					{
						table ="WEB_SeriesMMLLRPortSize_TableV1";
					}
					if(PNo.Series.Trim() !="Z")
					{
						if(db.SelectPortSizeNo(PNo.Bore_Size.Trim(),PNo.Port_Type.Trim(),table.Trim()) !="")
						{
							portsiz ="#"+db.SelectPortSizeNo(PNo.Bore_Size.Trim(),PNo.Port_Type.Trim(),table.Trim());
						}
					}
					string series1="";
					if(PNo.Series.Trim() =="SA")
					{
						series1="A";
					}
					else
					{
						series1=PNo.Series.Trim();
					}
					string head="<TABLE id=Table19 style= 'Z-INDEX: 203; LEFT: 0px; POSITION: relative; TOP: 0px;FONT-SIZE: 8pt; FONT-FAMILY: Arial;'  cellSpacing=1 cellPadding=1 width=101% border=1><TR><TD style= 'FONT-SIZE: 8pt; FONT-FAMILY: Arial;'ForeColor='Black' align=center bgColor=Gray>Code</TD><TD style= 'FONT-SIZE: 8pt; FONT-FAMILY: Arial;' bgColor=Gray>Descriptions</TD></TR>";
					string 	str ="";
					str ="<TR><TD>"+series1.Trim()+"</TD><TD>"+ db.SelectValue("Series_Name","WEB_Series_TableV1","Series_Code",PNo.Series.Trim())+"</TD></TR>";	
					str += "<TR><TD>"+PNo.Bore_Size+"</TD><TD>"+ db.SelectValue("Bore_Size","WEB_Bore_TableV1","Bore_Code",PNo.Bore_Size.ToString().Trim())+" Bore Size</TD></TR>";	
					str += "<TR><TD>"+PNo.Rod_Diamtr+"</TD><TD>"+db.SelectValue("Rod_Size","WEB_RodSerZ_TableV1","Rod_Code",PNo.Rod_Diamtr.ToString().Trim())+" Rod Size</TD></TR>";	
					str +="<TR><TD>"+PNo.Rod_End+"</TD><TD>"+db.SelectValue("RodEnd_Shape","WEB_RodEndKK_TableV1","RodEnd_Code",PNo.Rod_End.ToString().Trim())+rodenddim.ToString()+"</TD></TR>";	
					str +="<TR><TD>"+PNo.Cushions+"</TD><TD>"+db.SelectValue("Cushion_type","WEB_Cushion_TableV1","Cushion_Code",PNo.Cushions.ToString())+"</TD></TR>";	
					if(PNo.CushionPosition !="")
					{
						str +="<TR><TD>"+PNo.CushionPosition+"</TD><TD>"+db.SelectValue("CushionPos_Pos","WEB_CushionPos_TableV1","CushionPos_Code",PNo.CushionPosition.ToString().Trim())+"</TD></TR>";		
					}
					
						
					if(PNo.SecRodDiameter !="")
					{
						str +="<TR><TD>"+PNo.SecRodDiameter+"</TD><TD>"+db.SelectValue("SecRod_Diam","WEB_SecRod_TableV1","SecRod_Code",PNo.SecRodDiameter.ToString().Trim())+"Second Rod Diameter</TD></TR>";		
					}
					if(PNo.GlandDrain !="")
					{
						str +="<TR><TD>"+PNo.GlandDrain+"</TD><TD>Gland Drain</TD></TR>";		
					}
					if(PNo.Bushing_Conf !="")
					{
						str +="<TR><TD>"+PNo.Bushing_Conf+"</TD><TD>"+db.SelectValue("Bush_Type","WEB_GlandBushing_TableV1","Bush_Code",PNo.Bushing_Conf.ToString().Trim())+"</TD></TR>";	
					}
					if(PNo.MetalScrapper !="")
					{
						str +="<TR><TD>"+PNo.MetalScrapper+"</TD><TD>"+db.SelectValue("MetalScrapper","WEB_MetalScrapper_TableV1","MS_Code",PNo.MetalScrapper.ToString().Trim())+" Metal Scrapper</TD></TR>";
					}
					if(PNo.PistonSeal_Conf !="")
					{
						if(PNo.PistonSeal_Conf =="P1")
						{
							str +="<TR><TD>"+PNo.PistonSeal_Conf+"</TD><TD>"+db.SelectValue("SealConf_Conf","WEB_PistonSealConf_TableV1","SealConf_Code",PNo.PistonSeal_Conf.ToString().Trim())+"</TD></TR>";		
						}
					}
					if(PNo.MagnetRing !="")
					{
						str +="<TR><TD>"+PNo.MagnetRing+"</TD><TD>Magnet on piston</TD></TR>";		
					}
					if(PNo.PistonSeal_Conf !="")
					{
						if(PNo.PistonSeal_Conf !="P1")
						{
							str +="<TR><TD>"+PNo.PistonSeal_Conf+"</TD><TD>"+db.SelectValue("SealConf_Conf","WEB_PistonSealConf_TableV1","SealConf_Code",PNo.PistonSeal_Conf.ToString().Trim())+"</TD></TR>";		
						}
					}
					if(PNo.SecRodEnd !="")
					{
						str +="<TR><TD>"+PNo.SecRodEnd+"</TD><TD>"+db.SelectValue("SecRodEnd_Type","WEB_SecRodEnd_TableV1","SecRodEnd_Code",PNo.SecRodEnd.ToString().Trim())+"</TD></TR>";		
					}
					
					if(PNo.RodSeal_Conf !="")
					{
						str +="<TR><TD>"+PNo.RodSeal_Conf+"</TD><TD>"+db.SelectValue("SealConf_Conf","WEB_RodSealConf_TableV1","SealConf_Code",PNo.RodSeal_Conf.ToString().Trim())+"</TD></TR>";		
					}
					if(PNo.Coating !="")
					{
						if(PNo.Coating.Substring(0,1)=="W")
						{
							str +="<TR><TD >"+PNo.Coating+"</TD><TD >"+db.SelectValue("Coating_Type","WEB_Coating_TableV1","Coating_Code",PNo.Coating.ToString().Trim())+"</TD></TR>";		
						}
					}
					//issue #90 start
					if(PNo.TieRodeCentSupp !="")
					{
						str +="<TD>"+PNo.TieRodeCentSupp+"</TD><TD>"+db.SelectValue("Description","WEB_ApplicationOpt_TableV1","Application_Code",PNo.TieRodeCentSupp.ToString().Trim())+"</TD></TR>";		
							
					}
					//issue #90 end
					str +="<TR><TD>"+PNo.Seal_Comp+"</TD><TD>"+db.SelectValue("Seal_Type","WEB_Seal_TableV1","Seal_Code",PNo.Seal_Comp.ToString().Trim())+"</TD></TR>";	
					str +="<TR><TD>"+PNo.Port_Type+"</TD><TD>"+portsiz.Trim()+db.SelectValue("PortType_Type","WEB_portType_TableV1","PortType_Code",PNo.Port_Type.ToString().Trim())+"</TD></TR>";	
					str +="<TR><TD>"+PNo.Port_Pos+"</TD><TD>"+db.SelectValue("PortPos_Position","WEB_PortPosition_TableV1","PortPos_Code",PNo.Port_Pos.ToString().Trim())+"</TD></TR>";		
					str +="<TR><TD>"+PNo.Mount+"</TD><TD>"+db.SelectValue("Mount_Type","WEB_Mount"+PNo.Series.Trim()+"_TableV1","Mount_Code",PNo.Mount.ToString().Trim())+"</TD></TR>";	
					str +="<TR><TD>"+PNo.Stroke+"</TD><TD>"+"Stroke = "+PNo.Stroke.ToString()+"\""+sto.ToString()+st.ToString()+"</TD></TR>";	
						
				
					if(PNo.ThreadEx !="")
					{
						str +="<TR><TD>"+PNo.ThreadEx+"</TD><TD>Dim \"A\" ="+PNo.ThreadEx.Substring(1) +"\"</TD></TR>";		
					}
					if(PNo.SecRodThreadx !="")
					{
						str +="<TR><TD>"+PNo.SecRodThreadx+"</TD><TD>Dim \"AD\" ="+PNo.SecRodThreadx.Substring(2) +"\"</TD></TR>";		
					}
					if(PNo.BBC !="")
					{
						str +="<TR><TD>"+PNo.BBC+"</TD><TD>Special Tie Rod Extension(Cap End)\"BB\" ="+PNo.BBC.Substring(3) +"\"</TD></TR>";		
					}
					if(PNo.BBH!="")
					{
						str +="<TR><TD>"+PNo.BBH+"</TD><TD>Special Tie Rod Extension(Head End)\"BB\" ="+PNo.BBH.Substring(3) +"\"</TD></TR>";		
					}
					if(PNo.TandemDuplex !="")
					{
						if(PNo.TandemDuplex.Trim().Substring(0,2) =="BC")
						{
							string tan="";
							string ss="";
							ss=PNo.TandemDuplex.Trim().Substring(2)+"\"";
							tan =PNo.TandemDuplex.ToString().Trim().Substring(0,2);
							str +="<TR><TD>"+PNo.TandemDuplex+"</TD><TD>"+db.SelectValue("Description","WEB_popular_Opt_TableV1","Popular_Code",tan.Trim())+ss.Trim()+"</TD></TR>";	
						}
					}
					if(PNo.Coating !="")
					{
						if(PNo.Coating.Substring(0,1)!="W")
						{
							str +="<TR><TD>"+PNo.Coating+"</TD><TD>"+db.SelectValue("Coating_Type","WEB_Coating_TableV1","Coating_Code",PNo.Coating.ToString().Trim())+"</TD></TR>";		
						}
					}
					if(PNo.TandemDuplex !="")
					{
						if(PNo.TandemDuplex.Substring(0,2) =="DC")
						{
							string tan="";
							string ss="";
							ss=PNo.TandemDuplex.Trim().Substring(2)+"\"";
							tan =PNo.TandemDuplex.ToString().Trim().Substring(0,2);
							str +="<TR><TD>"+PNo.TandemDuplex+"</TD><TD>"+db.SelectValue("Description","WEB_popular_Opt_TableV1","Popular_Code",tan.Trim())+ss.Trim()+"</TD></TR>";	
						}
					}
					if(PNo.StopTube!=null && PNo.StopTube !="")
					{
						if(PNo.StopTube.Substring(0,1) =="D")
						{
							string ss="";
							if(PNo.StopTube.Trim().Substring(0,3) =="DST")
							{
								ss="Stop Tube Double Piston Design DST ="+PNo.StopTube.Trim().Substring(3);
							}
							else
							{
								ss="Stop Tube ="+PNo.StopTube.Trim().Substring(2);
							}
							str +="<TR><TD>"+PNo.StopTube+"</TD><TD>"+ss.Trim() +"\"</TD></TR>";		
						}
					}
					if(PNo.CarbonFibBarrel !="")
					{
						if(PNo.CarbonFibBarrel =="M1")
						{
							str +="<TR><TD>"+PNo.CarbonFibBarrel+"</TD><TD>"+db.SelectValue("Description","WEB_BarrelMaterial_TableV1","Popular_Code",PNo.CarbonFibBarrel.ToString().Trim())+"</TD></TR>";		
						}
					}
					if(PNo.SSTieRod !="")
					{
						str +="<TR><TD>"+PNo.SSTieRod+"</TD><TD>Stainless Steel Tie Rod </TD></TR>";		
					}
					if(PNo.SSPistionRod !="")
					{
						str +="<TR><TD>"+PNo.SSPistionRod+"</TD><TD>"+db.SelectValue("SSPRod_Type","WEB_SSPRod_TableV1","SSPRod_Code",PNo.SSPistionRod.ToString().Trim())+"</TD></TR>";		
					}
					if(PNo.CarbonFibBarrel !="")
					{
						if(PNo.CarbonFibBarrel =="M6")
						{
							str +="<TR><TD >"+PNo.CarbonFibBarrel+"</TD><TD >"+db.SelectValue("Description","WEB_BarrelMaterial_TableV1","Popular_Code",PNo.CarbonFibBarrel.ToString().Trim())+"</TD></TR>";		
						}
					}
					if(PNo.AirBleed !="")
					{
						str +="<TR><TD>"+PNo.AirBleed+"</TD><TD>"+db.SelectValue("AirBleed_Type","WEB_AirBleed_TableV1","AirBleed_Code",PNo.AirBleed.ToString().Trim())+"</TD></TR>";		
					}
					if(PNo.PortSize !="")
					{
						str +="<TR><TD>"+PNo.PortSize+"</TD><TD>"+db.SelectValue("PortSize_Type","WEB_PortSize_TableV1","PortSize_Code",PNo.PortSize.ToString().Trim())+"</TD></TR>";		
					}
					if(PNo.StopTube!=null && PNo.StopTube !="")
					{
						if(PNo.StopTube.Substring(0,1) =="S")
						{
							string ss="";
							if(PNo.StopTube.Trim().Substring(0,3) =="DST")
							{
								ss="Stop Tube Double Piston Design DST ="+PNo.StopTube.Trim().Substring(3);
							}
							else
							{
								ss="Stop Tube ="+PNo.StopTube.Trim().Substring(2);
							}
							str +="<TR><TD>"+PNo.StopTube+"</TD><TD>"+ss.Trim() +"\"</TD></TR>";		
						}
					}
					if(PNo.TandemDuplex !="")
					{
						if(PNo.TandemDuplex.Substring(0,2) =="TC")
						{
							str +="<TR><TD>"+PNo.TandemDuplex+"</TD><TD>"+db.SelectValue("Description","WEB_popular_Opt_TableV1","Popular_Code",PNo.TandemDuplex.Trim())+"</TD></TR>";	
						}
					}
					if(PNo.RodEx !="")
					{
						str +="<TR><TD>"+PNo.RodEx+"</TD><TD>Dim \"W\" ="+PNo.RodEx.Substring(1) +"\"</TD></TR>";		
					}
					if(PNo.SecRodEx !="")
					{
						str +="<TR><TD>"+PNo.SecRodEx+"</TD><TD>Dim \"WD\" ="+PNo.SecRodEx.Substring(2) +"\"</TD></TR>";		
					}
					if(PNo.Trunnions !="")
					{
						str +="<TR><TD>"+PNo.Trunnions+"</TD><TD>Intermediate Trunnions XI="+ PNo.Trunnions.Trim().Substring(2)+"\"</TD></TR>";		
					}
					if(PNo.Transducer !="")
					{
						string tt="";	
						if(PNo.Transducer.Length >2)
						{
							if(PNo.Transducer.ToString().Trim().Substring(2,1) =="S")
							{
								ArrayList lst=new ArrayList();
								lst=db.Select_TransducerDisc(PNo.Transducer.ToString().Substring(0,3));		
								for(int h=0;h<lst.Count;h++)
								{
									tt +=lst[h].ToString();
									if(lst[h].ToString()!=null && h < lst.Count -1)
									{
										tt +=", ";
									}
								}
								if(PNo.Transducer.ToString().Trim().Length >3)
								{
									tt =tt.Replace("10K",PNo.Transducer.ToString().Substring(3)+"K");
								}
							}
							else if(PNo.Transducer.ToString().Trim().Substring(2,1) =="R")
							{
								ArrayList lst=new ArrayList();
								lst=db.Select_TransducerDisc(PNo.Transducer.ToString().Substring(0,3));		
								for(int h=0;h<lst.Count;h++)
								{
									tt +=lst[h].ToString();
									if(lst[h].ToString()!=null && h < 3)
									{
										tt +=", ";
									}
								}
								if(PNo.Transducer.ToString().Trim().Length >3)
								{
									tt =tt.Replace("10K",PNo.Transducer.ToString().Substring(3)+"K");
								}
							}
							else if(PNo.Transducer.ToString().Trim().Substring(1,1) =="1" ||PNo.Transducer.ToString().Trim().Substring(1,1) =="2"
								||PNo.Transducer.ToString().Trim().Substring(1,1) =="3" || PNo.Transducer.ToString().Trim().Substring(1,1) =="4"
								|| PNo.Transducer.ToString().Trim().Substring(1,1) =="5")
							{
								ArrayList lst=new ArrayList();
								lst=db.Select_TransducerDisc(PNo.Transducer.ToString().Substring(0,2));			
								for(int h=0;h<lst.Count;h++)
								{
									tt +=lst[h].ToString();
									if(lst[h].ToString()!=null && h < lst.Count -1)
									{
										tt +=", ";
									}
								}
								if(PNo.Transducer.ToString().Trim().Length >2)
								{
									tt =tt.Replace("10K",PNo.Transducer.ToString().Substring(2)+"K");
								}
							}
						}
						else
						{
							tt="";
							ArrayList lst=new ArrayList();
							lst=db.Select_TransducerDisc(PNo.Transducer.ToString().Substring(0,2));			
							for(int h=0;h<lst.Count;h++)
							{
								tt +=lst[h].ToString();
								if(lst[h].ToString()!=null && h < lst.Count -1)
								{
									tt +=", ";
								}
							}
						}
						str +="<TD>"+PNo.Transducer+"</TD><TD>"+tt.Trim()+"</TD></TR>";		
					}
					str+="</TABLE>";	
					LBLSeries.Text=head.ToString()+str.ToString();
				}
			}
		}


		private void CBRod_CheckedChanged(object sender, System.EventArgs e)
		{
			
			if(CBRod.Checked ==false)
			{
				PanelRod.Visible=false;
								
			}
			else if(CBRod.Checked ==true)
			{
				PanelRod.Visible=true;
				RBLDoubleRod.SelectedIndex=1;
				DDLDiameterSec.Enabled=false;
				DDLSecRodEnd.Enabled=false;
				TxtSecRodEx.Enabled=false;
				TxtSecThreadEx.Enabled=false;
				ArrayList list4 = new ArrayList();
				list4=db.SelectSSProd(lbl.Text.Trim());
				DDLSSpistinrod.Items.Clear();
				DDLSSpistinrod.Items.Add("Standard");
				for (int i=0;i<= list4.Count -1;i++)
				{
					DDLSSpistinrod.Items.Add(list4[i].ToString());
				}
				if(DDLSeries.SelectedItem.Text.Trim() =="A - Slurryflo Pneumatic Actuator")
				{
					DDLSSpistinrod.SelectedIndex=1;
				}
				string b =db.SelectCode("Bore_Code","WEB_Bore_TableV1","Bore_Size",DDLBore.SelectedItem.ToString());
				string r=db.SelectCode("Rod_Code","WEB_RodSerZ_TableV1","Rod_Size",DDLRod.SelectedItem.ToString());
				decimal wdim=db.SelectAddersPrice("V","WEB_Min_Values_TableV1",b.Trim(),r.Trim());
				decimal min=0.00m;
				min =wdim + round;
				min =Decimal.Round(min,2);
				lblw.Text ="(Min = "+min.ToString()+" & Max = 24.00)";
			}
			 
		
		
		}

		private void CBStroke_CheckedChanged(object sender, System.EventArgs e)
		{
			if(CBStroke.Checked == false)
			{
				PanelStroke.Visible=false;
			}
			else if(CBStroke.Checked == true)
			{
				PanelStroke.Visible=true;
			}
			else
			{		
				PanelStroke.Visible=false;
			}
		
		}
		private void CBSeal_CheckedChanged(object sender, System.EventArgs e)
		{
			if(CBSeal.Checked == false)
			{
				PanelSeal.Visible=false;
			}
			else if(CBSeal.Checked == true)
			{
				PanelSeal.Visible=true;
				ArrayList list6 = new ArrayList();
				list6=db.SelectRodSealConf(lbl.Text.Trim());
				DDLRodSeal.Items.Clear();
				DDLRodSeal.Items.Add("Standard");
				for (int i=0;i<= list6.Count -1;i++)
				{
					DDLRodSeal.Items.Add(list6[i].ToString());
				}
			
				
			}
		
		}

		

		private void CBMount_CheckedChanged(object sender, System.EventArgs e)
		{
			if(DDLMount.SelectedIndex>0)
			{
				if(db.SelectCode("Mount_Code","WEB_MountZ_TableV1","Mount_Type",DDLMount.SelectedItem.ToString().Trim()) =="T4")
				{
					CBMount.Enabled=true;
					CBMount.Checked=true;	
					PanelMount.Visible=true;
					PnlXI.Visible=true;
					PnlBBC.Visible=false;
					PnlBBH.Visible=false;
					TxtBBCap.Visible =false;
					TxtBBHead.Visible=false;
					TXTIntere.Visible=true;
					lblxi.Visible=true;
					lblbb.Visible=false;
					lblbbc.Visible=false;
					lblbbh.Visible=false;
					decimal xi =0.00m;
					string s ="";
					if(DDLSeries.SelectedItem.Text.Trim() =="A - Slurryflo Pneumatic Actuator")
					{
						s="A";
					}
					else
					{
						s=	db.SelectSeriesCode(DDLSeries.SelectedItem.ToString());
					}
					string b =db.SelectCode("Bore_Code","WEB_Bore_TableV1","Bore_Size",DDLBore.SelectedItem.ToString());
					string r=db.SelectCode("Rod_Code","WEB_RodSerZ_TableV1","Rod_Size",DDLRod.SelectedItem.ToString());
					decimal wdim =0.00m;
					wdim=db.SelectAddersPrice("W","WEB_Min_Values_TableV1",b.Trim(),r.Trim());
					if(s.Trim().Substring(0,1) =="M")
					{
						xi=db.SelectAddersPrice("XIM","WEB_Min_Values_TableV1",b.Trim(),r.Trim());
					}
					else
					{
						xi=db.SelectAddersPrice("XIP","WEB_Min_Values_TableV1",b.Trim(),r.Trim());
					}
					decimal x1,x2,x3,x4,x5=0.00m;
					if(TXTRodEx.Text.Trim() !="")
					{
						decimal rodx=0.00m;
						rodx =Convert.ToDecimal(TXTRodEx.Text.Trim());
						x1= xi + (rodx - wdim);
							 
					}
					else
					{
						x1 = xi  - wdim;
					}
					x3 =Convert.ToDecimal(TxtStroke.Text.Trim());
					x2 = x1 + x3;
						
					x4 = x1 + round;
					x4 =Decimal.Round(x4,2);
					x5 = x2 - round;
					x5 =Decimal.Round(x5,2);

					lblx.Text ="(Min = "+x4.ToString()+" & Max = "+x5.ToString()+")";
					
				}
				else if(db.SelectCode("Mount_Code","WEB_MountZ_TableV1","Mount_Type",DDLMount.SelectedItem.ToString().Trim()) =="X1" )
				{
					CBMount.Enabled=true;
					CBMount.Checked=true;	
					PanelMount.Visible=true;
					PnlXI.Visible=false;
					PnlBBC.Visible=true;
					PnlBBH.Visible=true;
					TxtBBCap.Visible =true;
					TxtBBHead.Visible=true;
					TXTIntere.Visible=false;
					lblxi.Visible=false;
					lblbb.Visible=true;
					lblbbc.Visible=true;
					lblbbh.Visible=true;
					string b =db.SelectCode("Bore_Code","WEB_Bore_TableV1","Bore_Size",DDLBore.SelectedItem.ToString());
					string r=db.SelectCode("Rod_Code","WEB_RodSerZ_TableV1","Rod_Size",DDLRod.SelectedItem.ToString());
					decimal wdim=db.SelectAddersPrice("K","WEB_Min_Values_TableV1",b.Trim(),r.Trim());
					decimal min=0.00m;
					min = wdim + round;
					min =Decimal.Round(min,2);
					lblbbcc.Text ="(Min = "+min.ToString()+" & Max = 12.00)";
					lblbbhh.Text ="(Min = "+min.ToString()+" & Max = 12.00)";
				
				}
				else if(db.SelectCode("Mount_Code","WEB_MountZ_TableV1","Mount_Type",DDLMount.SelectedItem.ToString().Trim()) =="X2" )
				{
					CBMount.Enabled=true;
					CBMount.Checked=true;	
					PanelMount.Visible=true;
					PnlXI.Visible=false;
					PnlBBC.Visible=true;
					PnlBBH.Visible=false;
					TxtBBCap.Visible =true;
					TxtBBHead.Visible=false;
					TXTIntere.Visible=false;
					lblxi.Visible=false;
					lblbb.Visible=true;
					lblbbc.Visible=true;
					lblbbh.Visible=false;
					string b =db.SelectCode("Bore_Code","WEB_Bore_TableV1","Bore_Size",DDLBore.SelectedItem.ToString());
					string r=db.SelectCode("Rod_Code","WEB_RodSerZ_TableV1","Rod_Size",DDLRod.SelectedItem.ToString());
					decimal wdim=db.SelectAddersPrice("K","WEB_Min_Values_TableV1",b.Trim(),r.Trim());
					decimal min=0.00m;
					min = wdim + round;
					min =Decimal.Round(min,2);
					lblbbcc.Text ="(Min = "+min.ToString()+" & Max = 12.00)";
				
				}
				else if(db.SelectCode("Mount_Code","WEB_MountZ_TableV1","Mount_Type",DDLMount.SelectedItem.ToString().Trim()) =="X3" )
				{
					CBMount.Enabled=true;
					CBMount.Checked=true;	
					PanelMount.Visible=true;
					PnlBBC.Visible=false;
					PnlBBH.Visible=true;
					TxtBBCap.Visible =false;
					TxtBBHead.Visible=true;
					TXTIntere.Visible=false;
					lblxi.Visible=false;
					lblbb.Visible=true;
					lblbbc.Visible=false;
					lblbbh.Visible=true;
					string b =db.SelectCode("Bore_Code","WEB_Bore_TableV1","Bore_Size",DDLBore.SelectedItem.ToString());
					string r=db.SelectCode("Rod_Code","WEB_RodSerZ_TableV1","Rod_Size",DDLRod.SelectedItem.ToString());
					decimal wdim=db.SelectAddersPrice("K","WEB_Min_Values_TableV1",b.Trim(),r.Trim());
					decimal min=0.00m;
					min = wdim + round;
					min =Decimal.Round(min,2);
					lblbbhh.Text ="(Min = "+min.ToString()+" & Max = 12.00)";
				}
				else
				{
					CBMount.Enabled=false;
					CBMount.Checked=false;
					PanelMount.Visible=false;
				}
				TxtBBCap.Text="";
				TxtBBHead.Text="";
				TXTIntere.Text="";
			}
			else
			{
				PanelMount.Visible=false;
			}
		}

		private void CBPiston_CheckedChanged(object sender, System.EventArgs e)
		{
			if(CBPiston.Checked == false)
			{
			
				PanelPiston.Visible=false;
			}
			else if(CBPiston.Checked == true)
			{
				PanelPiston.Visible=true;
				ArrayList list22= new ArrayList();
				DDLPistonSealConfig.Items.Clear();
				DDLPistonSealConfig.Items.Add("Standard");
				list22=db.SelectPistonSealConf(lbl.Text.Trim());
				for (int i=0;i<= list22.Count -1;i++)
				{
					DDLPistonSealConfig.Items.Add(list22[i].ToString());
				}
				
			}
		}

		private void CBGland_CheckedChanged(object sender, System.EventArgs e)
		{
		
			if(CBGland.Checked == false)
			{
				PanelGland.Visible=false;
				DDLMetalScrappeer.Items.Clear();
			}
			
			else if(CBGland.Checked == true)
			{
					
				PanelGland.Visible=true;
				
				ArrayList list22= new ArrayList();
				DDLMetalScrappeer.Items.Clear();
				DDLMetalScrappeer.Items.Add("None");
				list22=db.MetalScrap(lbl.Text.Trim());
				for (int i=0;i<= list22.Count -1;i++)
				{
					DDLMetalScrappeer.Items.Add(list22[i].ToString());
				}
				if(DDLSeries.SelectedItem.Text.Trim() =="A - Slurryflo Pneumatic Actuator")
				{
					DDLMetalScrappeer.SelectedIndex=1;
				}
				ArrayList list2= new ArrayList();
				DDLBushing.Items.Clear();
				DDLBushing.Items.Add("Standard");
				list2=db.SelectGlandBushing(lbl.Text.Trim());
				for (int i=0;i<= list2.Count -1;i++)
				{
					DDLBushing.Items.Add(list2[i].ToString());
				}
			}
		}
	

		private void RBLDoubleRod_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(RBLDoubleRod.SelectedIndex ==1)
			{
		
				DDLDiameterSec.Enabled=false;
				DDLSecRodEnd.Enabled=false;
				TxtSecRodEx.Enabled=false;
				TxtSecThreadEx.Enabled=false;
				DDLDiameterSec.Items.Clear();
				DDLSecRodEnd.Items.Clear();
				TxtSecRodEx.Text="";
				TxtSecThreadEx.Text="";
				ArrayList list1 = new ArrayList();
				list1 =db.SelectMount(lbl.Text.Trim(),lblbor.Text.Trim(),"WEB_Mount"+lbl.Text.Replace("Series","").Trim()+"_TableV1");
				DDLMount.Items.Clear();
				DDLMount.Items.Add("Select Mount");
				for (int i=0;i<= list1.Count -1;i++)
				{
					DDLMount.Items.Add(list1[i].ToString());
				}
				if(lbl.Text =="SeriesN")
				{
					if(DDLBore.SelectedItem.Text.Trim() =="1 1/2\"" && DDLRod.SelectedItem.Text.Trim() =="1\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="2\"" && DDLRod.SelectedItem.Text.Trim() =="1 3/8\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="2 1/2\"" && DDLRod.SelectedItem.Text.Trim() =="5/8\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="2 1/2\"" && DDLRod.SelectedItem.Text.Trim() =="1 3/4\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="3 1/4\"" && DDLRod.SelectedItem.Text.Trim() =="1 3/4\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="3 1/4\"" && DDLRod.SelectedItem.Text.Trim() =="2\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text .Trim()=="4\"" && DDLRod.SelectedItem.Text.Trim() =="1\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="4\"" && DDLRod.SelectedItem.Text .Trim()=="2\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="4\"" && DDLRod.SelectedItem.Text.Trim() =="2 1/2\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="5\"" && DDLRod.SelectedItem.Text.Trim() =="1\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="5\"" && DDLRod.SelectedItem.Text.Trim() =="1 3/8\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="5\"" && DDLRod.SelectedItem.Text.Trim() =="2 1/2\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="6\"" && DDLRod.SelectedItem.Text.Trim() =="1 3/8\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="6\"" && DDLRod.SelectedItem.Text.Trim() =="2 1/2\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="6\"" && DDLRod.SelectedItem.Text.Trim() =="3\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
				}
			}
			else if(RBLDoubleRod.SelectedIndex ==0)
			{

				DDLDiameterSec.Enabled=true;
				DDLSecRodEnd.Enabled=true;
				TxtSecRodEx.Enabled=true;
				TxtSecThreadEx.Enabled=true;
				DDLDiameterSec.Items.Clear();
				DDLDiameterSec.Items.Add("Select Second Rod Size");
				for (int i=1;i<= DDLRod.Items.Count -1;i++)
				{
					DDLDiameterSec.Items.Add(DDLRod.Items[i].Text.ToString());
				}
				ArrayList list1 = new ArrayList();
				string rodend=db.SelectCode("Rod_Code","WEB_RodSerZ_TableV1","Rod_Size",DDLRod.SelectedItem.ToString());
				list1=db.SelectSecRodEnd(rodend);
				DDLSecRodEnd.DataSource =list1;
				DDLSecRodEnd.DataBind();
				ArrayList list11 = new ArrayList();
				//681
				//back
				//list11 =db.SelectDMount(lbl.Text.Trim(),lblbor.Text.Trim()+"D","WEB_Mount"+lbl.Text.Replace("Series","").Trim()+"_TableV1");
				//update
				list11 =db.SelectDMount(lbl.Text.Trim()+"D",lblbor.Text.Trim(),"WEB_Mount"+lbl.Text.Replace("Series","").Trim()+"_TableV1");
				DDLMount.Items.Clear();
				DDLMount.Items.Add("Select Mount");
				for (int i=0;i<= list11.Count -1;i++)
				{
					DDLMount.Items.Add(list11[i].ToString());
				}
				if(lbl.Text =="SeriesN")
				{
					if(DDLBore.SelectedItem.Text.Trim() =="1 1/2\"" && DDLRod.SelectedItem.Text.Trim() =="1\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="2\"" && DDLRod.SelectedItem.Text.Trim() =="1 3/8\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="2 1/2\"" && DDLRod.SelectedItem.Text.Trim() =="5/8\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="2 1/2\"" && DDLRod.SelectedItem.Text.Trim() =="1 3/4\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="3 1/4\"" && DDLRod.SelectedItem.Text.Trim() =="1 3/4\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="3 1/4\"" && DDLRod.SelectedItem.Text.Trim() =="2\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text .Trim()=="4\"" && DDLRod.SelectedItem.Text.Trim() =="1\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="4\"" && DDLRod.SelectedItem.Text .Trim()=="2\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="4\"" && DDLRod.SelectedItem.Text.Trim() =="2 1/2\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="5\"" && DDLRod.SelectedItem.Text.Trim() =="1\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="5\"" && DDLRod.SelectedItem.Text.Trim() =="1 3/8\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="5\"" && DDLRod.SelectedItem.Text.Trim() =="2 1/2\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="6\"" && DDLRod.SelectedItem.Text.Trim() =="1 3/8\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="6\"" && DDLRod.SelectedItem.Text.Trim() =="2 1/2\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="6\"" && DDLRod.SelectedItem.Text.Trim() =="3\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
				}
			
			}
			else
			{
		
				DDLDiameterSec.Enabled=false;
				DDLSecRodEnd.Enabled=false;
				TxtSecRodEx.Enabled=false;
				TxtSecThreadEx.Enabled=false;
				DDLDiameterSec.Items.Clear();
				DDLSecRodEnd.Items.Clear();
				TxtSecRodEx.Text="";
				TxtSecThreadEx.Text="";
			}
		}

		private void DDLMount_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(DDLMount.SelectedIndex>0)
			{
				DDLCushions_SelectedIndexChanged(sender,e);
				if(db.SelectCode("Mount_Code","WEB_MountZ_TableV1","Mount_Type",DDLMount.SelectedItem.ToString().Trim()) =="T4" || db.SelectCode("Mount_Code","WEB_MountZ_TableV1","Mount_Type",DDLMount.SelectedItem.ToString().Trim()) =="TR4")
				{
					CBMount.Enabled=true;
					CBMount.Checked=true;	
					PanelMount.Visible=true;
					PnlXI.Visible=true;
					PnlBBC.Visible=false;
					PnlBBH.Visible=false;
					TxtBBCap.Visible =false;
					TxtBBHead.Visible=false;
					TXTIntere.Visible=true;
					lblxi.Visible=true;
					lblbb.Visible=false;
					lblbbc.Visible=false;
					lblbbh.Visible=false;
					decimal xi =0.00m;
					string s ="";
					if(DDLSeries.SelectedItem.Text.Trim() =="A - Slurryflo Pneumatic Actuator")
					{
						s="A";
					}
					else
					{
						s=db.SelectSeriesCode(DDLSeries.SelectedItem.ToString());
					}
					string b =db.SelectCode("Bore_Code","WEB_Bore_TableV1","Bore_Size",DDLBore.SelectedItem.ToString());
					string r=db.SelectCode("Rod_Code","WEB_RodSerZ_TableV1","Rod_Size",DDLRod.SelectedItem.ToString());
					decimal wdim =0.00m;
					wdim=db.SelectAddersPrice("W","WEB_Min_Values_TableV1",b.Trim(),r.Trim());
					if(s.Trim().Substring(0,1) =="M")
					{
						xi=db.SelectAddersPrice("XIM","WEB_Min_Values_TableV1",b.Trim(),r.Trim());
					}
					else
					{
						xi=db.SelectAddersPrice("XIP","WEB_Min_Values_TableV1",b.Trim(),r.Trim());
					}
					decimal x1,x2,x4,x5=0.00m;
					decimal x3=0.00m;
					if(TXTRodEx.Text.Trim() !="")
					{
						decimal rodx=0.00m;
						rodx =Convert.ToDecimal(TXTRodEx.Text.Trim());
						x1= xi + (rodx - wdim);
							 
					}
					else
					{
						x1 = xi  - wdim;
					}
					if(TxtStroke.Text.Trim() !="")
					{
						x3 =Convert.ToDecimal(TxtStroke.Text.Trim());
					}
					x2 = x1 + x3;
						
					x4 = x1 + round;
					x4 =Decimal.Round(x4,2);
					x5 = x2 - round;
					x5 =Decimal.Round(x5,2);

					lblx.Text ="(Min = "+x4.ToString()+" & Max = "+x5.ToString()+")";
					
				}
				else if(db.SelectCode("Mount_Code","WEB_MountZ_TableV1","Mount_Type",DDLMount.SelectedItem.ToString().Trim()) =="X1" )
				{
					CBMount.Enabled=true;
					CBMount.Checked=true;	
					PanelMount.Visible=true;
					PnlXI.Visible=false;
					PnlBBC.Visible=true;
					PnlBBH.Visible=true;
					TxtBBCap.Visible =true;
					TxtBBHead.Visible=true;
					TXTIntere.Visible=false;
					lblxi.Visible=false;
					lblbb.Visible=true;
					lblbbc.Visible=true;
					lblbbh.Visible=true;
					string b =db.SelectCode("Bore_Code","WEB_Bore_TableV1","Bore_Size",DDLBore.SelectedItem.ToString());
					string r=db.SelectCode("Rod_Code","WEB_RodSerZ_TableV1","Rod_Size",DDLRod.SelectedItem.ToString());
					decimal wdim=db.SelectAddersPrice("K","WEB_Min_Values_TableV1",b.Trim(),r.Trim());
					decimal min=0.00m;
					min = wdim + round;
					min =Decimal.Round(min,2);
					lblbbcc.Text ="(Min = "+min.ToString()+" & Max = 12.00)";
					lblbbhh.Text ="(Min = "+min.ToString()+" & Max = 12.00)";
				
				}
				else if(db.SelectCode("Mount_Code","WEB_MountZ_TableV1","Mount_Type",DDLMount.SelectedItem.ToString().Trim()) =="X2" )
				{
					CBMount.Enabled=true;
					CBMount.Checked=true;	
					PanelMount.Visible=true;
					PnlXI.Visible=false;
					PnlBBC.Visible=true;
					PnlBBH.Visible=false;
					TxtBBCap.Visible =true;
					TxtBBHead.Visible=false;
					TXTIntere.Visible=false;
					lblxi.Visible=false;
					lblbb.Visible=true;
					lblbbc.Visible=true;
					lblbbh.Visible=false;
					string b =db.SelectCode("Bore_Code","WEB_Bore_TableV1","Bore_Size",DDLBore.SelectedItem.ToString());
					string r=db.SelectCode("Rod_Code","WEB_RodSerZ_TableV1","Rod_Size",DDLRod.SelectedItem.ToString());
					decimal wdim=db.SelectAddersPrice("K","WEB_Min_Values_TableV1",b.Trim(),r.Trim());
					decimal min=0.00m;
					min = wdim + round;
					min =Decimal.Round(min,2);
					lblbbcc.Text ="(Min = "+min.ToString()+" & Max = 12.00)";
				
				}
				else if(db.SelectCode("Mount_Code","WEB_MountZ_TableV1","Mount_Type",DDLMount.SelectedItem.ToString().Trim()) =="X3" )
				{
					CBMount.Enabled=true;
					CBMount.Checked=true;	
					PanelMount.Visible=true;
					PnlBBC.Visible=false;
					PnlBBH.Visible=true;
					TxtBBCap.Visible =false;
					TxtBBHead.Visible=true;
					TXTIntere.Visible=false;
					lblxi.Visible=false;
					lblbb.Visible=true;
					lblbbc.Visible=false;
					lblbbh.Visible=true;
					string b =db.SelectCode("Bore_Code","WEB_Bore_TableV1","Bore_Size",DDLBore.SelectedItem.ToString());
					string r=db.SelectCode("Rod_Code","WEB_RodSerZ_TableV1","Rod_Size",DDLRod.SelectedItem.ToString());
					decimal wdim=db.SelectAddersPrice("K","WEB_Min_Values_TableV1",b.Trim(),r.Trim());
					decimal min=0.00m;
					min = wdim + round;
					min =Decimal.Round(min,2);
					lblbbhh.Text ="(Min = "+min.ToString()+" & Max = 12.00)";
				}
				else
				{
					CBMount.Enabled=false;
					CBMount.Checked=false;
					PanelMount.Visible=false;
				}
				TxtBBCap.Text="";
				TxtBBHead.Text="";
				TXTIntere.Text="";
			}
			else
			{
				PanelMount.Visible=false;
			}
		}
		private void CbOther_CheckedChanged(object sender, System.EventArgs e)
		{
			
			if(CbOther.Checked == false)
			{
				
				PanelOther.Visible=false;
			}
			else if(CbOther.Checked == true)
			{
				
				PanelOther.Visible=true;
				ArrayList list12 = new ArrayList();
				list12=db.SelectCoating(lbl.Text.ToString().Trim());
				DDLCoating.Items.Clear();
				DDLCoating.Items.Add("None");
				for (int i=0;i<= list12.Count -1;i++)
				{
					DDLCoating.Items.Add(list12[i].ToString());
				}
				ArrayList list2 = new ArrayList();
				list2=db.SelectBarrelMaterial(lbl.Text.ToString().Trim());
				DDLBarrel.Items.Clear();
				DDLBarrel.Items.Add("Standard");
				for (int i=0;i<= list2.Count -1;i++)
				{
					DDLBarrel.Items.Add(list2[i].ToString());
				}
					
			}
			
		}

		private void DDLCushions_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(DDLMount.Items.Count >1)
			{
				DDLCushionPosition.Items.Clear();
				string mcod="";
				if(DDLMount.SelectedIndex >0)
				{
					mcod=db.SelectCode("Mount_Code","WEB_MountZ_TableV1","Mount_Type",DDLMount.SelectedItem.ToString().Trim());
				}
				ArrayList list = new ArrayList();
				list=db.SelectCushionPos();
				if(DDLCushions.SelectedItem.Text.Trim().Equals("No Cushions"))
				{
					DDLCushionPosition.Items.Clear();
				}
				else if(DDLCushions.SelectedItem.Text.Trim().Equals("Cushioned Both Ends") )
				{
					for (int i=4;i<= list.Count -1;i++)
					{
						DDLCushionPosition.Items.Add(list[i].ToString());
					}
					if(mcod.Trim() =="T1" || mcod.Trim() =="T1+")
					{
						DDLCushionPosition.SelectedIndex=11;
					
					}
					else if(mcod.Trim() =="T2" || mcod.Trim() =="T2+")
					{
						DDLCushionPosition.SelectedIndex=8;
					}
					else if(mcod.Trim() =="S3")
					{
						DDLCushionPosition.SelectedIndex=2;
					}
					else if(mcod.Trim() =="S4")
					{
						DDLCushionPosition.SelectedIndex=1;
					}
					else
					{
						DDLCushionPosition.SelectedIndex=1;
					}

				}
				else if(DDLCushions.SelectedItem.Text.Trim().Equals("Head End Cushion"))
				{
					for (int i=0;i<=3;i++)
					{
						DDLCushionPosition.Items.Add(list[i].ToString());
					}
					if(mcod.Trim() =="T1" || mcod.Trim() =="T1+")
					{
						DDLCushionPosition.SelectedIndex=2;
					
					}
					else if(mcod.Trim() =="T2" || mcod.Trim() =="T2+")
					{
						DDLCushionPosition.SelectedIndex=1;
					}
					else if(mcod.Trim() =="S3")
					{
						DDLCushionPosition.SelectedIndex=2;
					}
					else if(mcod.Trim() =="S4")
					{
						DDLCushionPosition.SelectedIndex=1;
					}
					else
					{
						DDLCushionPosition.SelectedIndex=1;
					}			
					
					
				}
				else if(DDLCushions.SelectedItem.Text.Trim().Equals("Cap End Cushion"))
				{
					for (int i=0;i<=3;i++)
					{
						DDLCushionPosition.Items.Add(list[i].ToString());
					}
					if(mcod.Trim() =="T1" || mcod.Trim() =="T1+")
					{
						DDLCushionPosition.SelectedIndex=1;
					
					}
					else if(mcod.Trim() =="T2" || mcod.Trim() =="T2+")
					{
						DDLCushionPosition.SelectedIndex=2;
					}
					else if(mcod.Trim() =="S3")
					{
						DDLCushionPosition.SelectedIndex=2;
					}
					else if(mcod.Trim() =="S4")
					{
						DDLCushionPosition.SelectedIndex=1;
					}
					else
					{
						DDLCushionPosition.SelectedIndex=1;
					}					
					
				}
				else
				{
					DDLCushionPosition.Items.Clear();
				}
			}
			else
			{
				DDLCushionPosition.Items.Clear();

				ArrayList list = new ArrayList();
				list=db.SelectCushionPos();
				if(DDLCushions.SelectedItem.Text.Trim().Equals("No Cushions"))
				{
					DDLCushionPosition.Items.Clear();
				}
				else if(DDLCushions.SelectedItem.Text.Trim().Equals("Cushioned Both Ends"))
				{
					for (int i=4;i<= list.Count -1;i++)
					{
						DDLCushionPosition.Items.Add(list[i].ToString());
					}

					DDLCushionPosition.SelectedIndex=1;
				}
				else if(DDLCushions.SelectedItem.Text.Trim().Equals("Head End Cushion") || DDLCushions.SelectedItem.Text.Trim().Equals("Cap End Cushion"))
				{
					for (int i=0;i<=3;i++)
					{
						DDLCushionPosition.Items.Add(list[i].ToString());
					}
					string mcod=db.SelectCode("Mount_Code","WEB_MountZ_TableV1","Mount_Type",DDLMount.SelectedItem.ToString().Trim());
					if(mcod.ToString() =="T1" ||mcod.ToString() =="T2"||mcod.ToString() =="E5"||mcod.ToString() =="E6"||mcod.ToString() =="S3")
					{
						DDLCushionPosition.SelectedIndex=2;
					}
					else
					{
						DDLCushionPosition.SelectedIndex=1;
					}
					
				}
				else
				{
					DDLCushionPosition.Items.Clear();
				}
			}
		}

		private void DDLPorttype_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(DDLPorttype.SelectedIndex >0)
			{
				ArrayList list11 = new ArrayList();
				list11=db.SelectPortPos();
				//issue #680 start
				//list11=db.SelectOptionsWithCode("PortPos_Code","PortPos_Position","PortPosition_TableV1",lbl.Text.Trim(),"PortPos_SLNO");
				//issue #680 end
				DDLPortPos.DataSource =list11;
				DDLPortPos.DataBind();
				DDLPortPos.SelectedIndex =0;
			}
		}

		
		private void DDLRod_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(DDLRod.SelectedIndex >0)
			{
				string rodend=db.SelectCode("Rod_Code","WEB_RodSerZ_TableV1","Rod_Size",DDLRod.SelectedItem.ToString());
				ArrayList list2 = new ArrayList();
				list2=db.SelectRodEnd(rodend,lbl.Text.Trim());
				DDLRodEnd.Items.Clear();
				DDLRodEnd.Items.Add("Select Rod End");
				for (int i=0;i<= list2.Count -1;i++)
				{
					DDLRodEnd.Items.Add(list2[i].ToString());
				}
				DDLRodEnd.SelectedIndex= 1;
				if(DDLSeries.SelectedItem.Text.Trim() !="A - Pneumatic 150 PSI Linear Valve Actuator")
				{
					DDLRodEnd.Items.Remove("Female rod end (Series A type)");
				}
				if(DDLSeries.SelectedItem.Text.Trim() =="A - Slurryflo Pneumatic Actuator")
				{
					DDLRodEnd.SelectedItem.Text="Female rod end (Series A type)";
				}
				string b =db.SelectCode("Bore_Code","WEB_Bore_TableV1","Bore_Size",DDLBore.SelectedItem.ToString());
				string r=db.SelectCode("Rod_Code","WEB_RodSerZ_TableV1","Rod_Size",DDLRod.SelectedItem.ToString());
				decimal wdim=db.SelectAddersPrice("V","WEB_Min_Values_TableV1",b.Trim(),r.Trim());
				decimal min=0.00m;
				min = wdim + round;
				min =Decimal.Round(min,2);
				lblw.Text ="(Min = "+wdim.ToString()+" & Max = 24.00)";
				ArrayList list1 = new ArrayList();
				list1 =db.SelectMount(lbl.Text.Trim(),lblbor.Text.Trim(),"WEB_Mount"+lbl.Text.Replace("Series","").Trim()+"_TableV1");
				DDLMount.Items.Clear();
				DDLMount.Items.Add("Select Mount");
				for (int i=0;i<= list1.Count -1;i++)
				{
					DDLMount.Items.Add(list1[i].ToString());
				}
				if(DDLSeries.SelectedItem.Text.Trim() =="A - Slurryflo Pneumatic Actuator")
				{
					DDLMount.SelectedIndex=1;
					CBRod.Checked=true;
					CBRod_CheckedChanged(sender,e);
					CBGland.Checked=true;
					CBGland_CheckedChanged(sender,e);
				}
				if(lbl.Text =="SeriesN")
				{
					if(DDLBore.SelectedItem.Text.Trim() =="1 1/2\"" && DDLRod.SelectedItem.Text.Trim() =="1\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="2\"" && DDLRod.SelectedItem.Text.Trim() =="1 3/8\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="2 1/2\"" && DDLRod.SelectedItem.Text.Trim() =="5/8\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="2 1/2\"" && DDLRod.SelectedItem.Text.Trim() =="1 3/4\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="3 1/4\"" && DDLRod.SelectedItem.Text.Trim() =="1 3/4\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="3 1/4\"" && DDLRod.SelectedItem.Text.Trim() =="2\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text .Trim()=="4\"" && DDLRod.SelectedItem.Text.Trim() =="1\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="4\"" && DDLRod.SelectedItem.Text .Trim()=="2\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="4\"" && DDLRod.SelectedItem.Text.Trim() =="2 1/2\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="5\"" && DDLRod.SelectedItem.Text.Trim() =="1\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="5\"" && DDLRod.SelectedItem.Text.Trim() =="1 3/8\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="5\"" && DDLRod.SelectedItem.Text.Trim() =="2 1/2\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="6\"" && DDLRod.SelectedItem.Text.Trim() =="1 3/8\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="6\"" && DDLRod.SelectedItem.Text.Trim() =="2 1/2\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
					else if(DDLBore.SelectedItem.Text.Trim() =="6\"" && DDLRod.SelectedItem.Text.Trim() =="3\"" )
					{
						DDLMount.Items.Remove("ME5 mount: Head rectangular");
					}
				}
				//issue #98 start
				string strtest ="";
				ListItem lstItemCushionDeleted=null;
				string strBoreValue= DDLBore.SelectedIndex.ToString();
				strBoreValue=DDLBore.SelectedItem.Text;
				strBoreValue=DDLBore.SelectedItem.Value;
				if((DDLBore.SelectedItem.Value!="3 1/4\"" && DDLBore.SelectedItem.Value!="4\"") || ((DDLBore.SelectedItem.Value=="3 1/4\"" || DDLBore.SelectedItem.Value=="4\"")&&DDLRod.SelectedItem.Value!="1\"" && DDLRod.SelectedItem.Value!="1 3/8\""))
				{
					for(int i=0;i<DDLCushions.Items.Count;i++)
					{
						strtest=DDLCushions.Items[i].Value;
						strtest=DDLCushions.Items[i].Text;
						if (DDLCushions.Items[i].Value=="High Impact Cushions")
						{
							Session["CushionDeletedText"]=DDLCushions.Items[i].Text;
							Session["CushionDeletedValue"]=DDLCushions.Items[i].Value;
							DDLCushions.Items.RemoveAt(i);
						}
					}
					strBoreValue="Right";
				}
				else
				{
					if(Session["CushionDeletedText"]!=null && Session["CushionDeletedValue"]!=null)
					{
						if(Session["CushionDeletedText"].ToString()!="" && Session["CushionDeletedValue"].ToString()!="")
						{
							if(DDLCushions.Items.FindByValue("High Impact Cushions")==null)
							{
								lstItemCushionDeleted = new ListItem();
								lstItemCushionDeleted.Text = (string)Session["CushionDeletedText"];
								lstItemCushionDeleted.Value = (string)Session["CushionDeletedValue"];
								DDLCushions.Items.Add(lstItemCushionDeleted);
							}
						}
					}
				}
				//issue #98 end
			}
		}

		private void TxtEffectiveStrok_TextChanged(object sender, System.EventArgs e)
		{
			if(IsNumeric(TxtEffectiveStrok.Text.ToString().Trim()) ==true )
			{
			
				TxtEffectiveStrok.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TxtEffectiveStrok.Text));
			}
			else
			{
				TxtEffectiveStrok.Text="";
			}
			if(TxtEffectiveStrok.Text.ToString().Trim() !="" && TxtStopTube.Text.ToString().Trim() !="")
			{
				decimal dc1,dc2,dc3=0.00m;
				dc1=Convert.ToDecimal(TxtStopTube.Text);
				dc2=Convert.ToDecimal(TxtEffectiveStrok.Text);
				dc3 =dc1 +dc2;
				TxtStroke.Text =String.Format("{0:##0.00}", dc3);
			}
		}

		private void TxtStroke_TextChanged(object sender, System.EventArgs e)
		{ 
		 
			if(TxtStroke.Text.ToString().Trim() !="" && IsNumeric(TxtStroke.Text.ToString().Trim()) ==true  )
			{
			
				TxtStroke.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TxtStroke.Text));
				lblSecstroke.Text="( Min = 0.00 & Max  <= "+TxtStroke.Text.Trim()+")";
			}
			else
			{
				TxtStroke.Text="";
			}
		}

		private void TXTThread_TextChanged(object sender, System.EventArgs e)
		{
			if(TXTThread.Text.ToString().Trim() !="" && IsNumeric(TXTThread.Text.ToString().Trim()) ==true )
			{
			
				TXTThread.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TXTThread.Text));
			}
			else
			{
				TXTThread.Text="";
			}
			
		}

		private void TXTRodEx_TextChanged(object sender, System.EventArgs e)
		{
			if(TXTRodEx.Text.ToString().Trim() !="" && IsNumeric(TXTRodEx.Text.ToString().Trim()) ==true )
			{
			
				TXTRodEx.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TXTRodEx.Text));
				if(PnlXI.Visible ==true)
				{
					decimal xi =0.00m;
					string s ="";
					if(DDLSeries.SelectedItem.Text.Trim() =="A - Slurryflo Pneumatic Actuator")
					{
						s="A";
					}
					else
					{
						s=db.SelectSeriesCode(DDLSeries.SelectedItem.ToString());
					}
					string b =db.SelectCode("Bore_Code","WEB_Bore_TableV1","Bore_Size",DDLBore.SelectedItem.ToString());
					string r=db.SelectCode("Rod_Code","WEB_RodSerZ_TableV1","Rod_Size",DDLRod.SelectedItem.ToString());
					decimal wdim =0.00m;
					wdim=db.SelectAddersPrice("W","WEB_Min_Values_TableV1",b.Trim(),r.Trim());
					if(s.Trim().Substring(0,1) =="M")
					{
						xi=db.SelectAddersPrice("XIM","WEB_Min_Values_TableV1",b.Trim(),r.Trim());
					}
					else
					{
						xi=db.SelectAddersPrice("XIP","WEB_Min_Values_TableV1",b.Trim(),r.Trim());
					}
					decimal x1,x2,x3,x4,x5=0.00m;
					if(TXTRodEx.Text.Trim() !="")
					{
						decimal rodx=0.00m;
						rodx =Convert.ToDecimal(TXTRodEx.Text.Trim());
						x1= xi + (rodx - wdim);
							 
					}
					else
					{
						x1 = xi  - wdim;
					}
					x3 =Convert.ToDecimal(TxtStroke.Text.Trim());
					x2 = x1 + x3;
						
					x4 = x1 + round;
					x4 =Decimal.Round(x4,2);
					x5 = x2 - round;
					x5 =Decimal.Round(x5,2);

					lblx.Text ="(Min = "+x4.ToString()+"& Max = "+x5.ToString()+")";
				}	
			}
			else
			{
				TXTRodEx.Text="";
			}
			
		}

		private void TxtSecThreadEx_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtSecThreadEx.Text.ToString().Trim() !="" && IsNumeric(TxtSecThreadEx.Text.ToString().Trim()) ==true )
			{
			
				TxtSecThreadEx.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TxtSecThreadEx.Text));
			}
			else
			{
				TxtSecThreadEx.Text="";
			}
		}

		private void TxtSecRodEx_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtSecRodEx.Text.ToString().Trim() !="" && IsNumeric(TxtSecRodEx.Text.ToString().Trim()) ==true )
			{
			
				TxtSecRodEx.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TxtSecRodEx.Text));
			}
			else
			{
				TxtSecRodEx.Text="";
			}
			
		}

		private void TxtStopTube_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtStopTube.Text.ToString().Trim() !="" && IsNumeric(TxtStopTube.Text.ToString().Trim()) ==true )
			{
			
				TxtStopTube.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TxtStopTube.Text));
			}
			else
			{
				TxtStopTube.Text="";
			}
			if(TxtEffectiveStrok.Text.ToString().Trim() !="" && TxtStopTube.Text.ToString().Trim() !="")
			{
				decimal dc1,dc2,dc3=0.00m;
				dc1=Convert.ToDecimal(TxtStopTube.Text);
				dc2=Convert.ToDecimal(TxtEffectiveStrok.Text);
				dc3 =dc1 +dc2;
				TxtStroke.Text =String.Format("{0:##0.00}", dc3);
			}
		}

		private void TXTIntere_TextChanged(object sender, System.EventArgs e)
		{
			if(TXTIntere.Text.ToString().Trim() !="" && IsNumeric(TXTIntere.Text.ToString().Trim()) ==true )
			{
			
				TXTIntere.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TXTIntere.Text));
			}
			else
			{
				TXTIntere.Text="";
			}
		}
		public static bool IsNumeric(string strInteger) 
		{
			try 
			{
					int intTemp =0;
				if(strInteger.ToString().StartsWith(".") == true)
				{
					for(int i=1; i< strInteger.Length;i++)
					{
						intTemp = Int32.Parse( strInteger.Substring(i,1) );
					}
				}
				else
				{
					for(int i=0; i< strInteger.Length;i++)
					{
						if (strInteger.ToString().Substring(i,1) !=".")
						{
							intTemp = Int32.Parse( strInteger.Substring(i,1) );
						}
					}
				}
				return true;
			} 
			catch (FormatException) 
			{
				return false;
			}    
		}

		//issue #233 start
		private string Cyl_Displacement(PartNumber PN)
		{
			string strCylDis = "TBA";
			try
			{
				DBClass db=new DBClass();
				decimal dcBore = 0m;
				decimal dcRod = 0m;
				decimal dcStroke = Convert.ToDecimal(PN.Stroke);
				dcRod = Convert.ToDecimal(db.SelectRodValue(PN.Rod_Diamtr));
				dcBore = Convert.ToDecimal(db.SelectBoreValue(PN.Bore_Size));
				decimal dcCylDis = 0m;
				decimal dcCylDisRet=0m;
				if(PN.DoubleRod == "No")
				{
					dcCylDisRet=(dcBore*dcBore)*3.14m*dcStroke/4m;
				}
				dcCylDis=(dcBore*dcBore-dcRod*dcRod)*3.14m*dcStroke/4m + dcCylDisRet;
				if(PN.TandemDuplex == "TC")
				{
					dcCylDis *=2m;
				}
				dcCylDis = Math.Round(dcCylDis,0);
				strCylDis=dcCylDis.ToString();
			}
			catch (Exception ex)
			{		
				
				
			}
			return strCylDis;
		}
		//issue #233 end
		private void TxtSecondStroke_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtSecondStroke.Text.ToString().Trim() !="" && IsNumeric(TxtSecondStroke.Text.ToString().Trim()) ==true )
			{
			
				TxtSecondStroke.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TxtSecondStroke.Text));
			}
			else
			{
				TxtSecondStroke.Text="";
			}
		}

		private void CBTandem_CheckedChanged(object sender, System.EventArgs e)
		{
			
			if(CBTandem.Checked == false)
			{
				
				PNTandem.Visible=false;
			}
			else if(CBTandem.Checked == true)
			{
				
				PNTandem.Visible=true;
				TxtSecondStroke.Enabled=false;
						
			}
			
		}

		private void DDLTandomConfig_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(DDLTandomConfig.SelectedIndex ==0)
			{
				TxtSecondStroke.Enabled =false;
				RFVtandem.Enabled =false;
			}
			if(DDLTandomConfig.SelectedIndex > 0)
			{
				TxtSecondStroke.Enabled = true;
				RFVtandem.Enabled =true;
			}
		}

		private void RBSpecialReq_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(RBSpecialReq.SelectedIndex ==1)
			{
				PnlSpReg.Visible =false;
				TxtSpecialReq.Text="";
			}
			else
			{
				PnlSpReg.Visible =true;
			}
		}

		private void BtnGeneratePrice_Click(object sender, System.EventArgs e)
		{
			if(TxtQty.Text.Trim() !=""  && Session["PartNo"] !=null)
			{
				lblQty.Visible =false;
				ArrayList lst =new ArrayList();
				lst =(ArrayList)Session["User"];
				PartNumber PN= (PartNumber)Session["PartNo"];								
				if(PN.Series.Trim() =="PA")
				{
					//issue #644 start
					if(lst[6].ToString().Trim() =="1030")
					{
						PAPricing_SMC();
					}
					else
						//issue #644 end
					PAPricing();
				}
				else if(PN.Series.Trim() =="PS")
				{
					//issue #644 start
					if(lst[6].ToString().Trim() =="1030")
					{
						PSPricing_SMC();
					}
					else
					//issue #644 end
					PSPricing();
				}
				else if(PN.Series.Trim() =="PC")
				{
					//issue #644 start
					if(lst[6].ToString().Trim() =="1030")
					{
						PCPricing_SMC();
					}
					else
					//issue #644 end
					PCPricing();
				}
				else if(PN.Series.Trim() =="A")
				{
					APricing();
				}
				else if(PN.Series.Trim() =="AT")
				{
					ATPricing();
				}
				else if(PN.Series.Trim() =="SA")
				{
					SAPricing();
				}
				else if(PN.Series.Trim() =="M")
				{
					MPricing();
				}
				else if(PN.Series.Trim() =="ML")
				{
					MLPricing();
				}
				else if(PN.Series.Trim() =="L")
				{
					LPricing();
				}
				else if(PN.Series.Trim() =="N")
				{
					NPricing();
				}
				else
				{
					lbltotal.Text ="0.00";
					lblstroke.Text ="0.00";
					lblstatus.Text ="true";
				}

				try
				{
					ArrayList list1 = new ArrayList();
					ArrayList list2=new ArrayList();
					list1=db.SelectContactdetails(lst[6].ToString().Trim(),lst[5].ToString().Trim());
					list2=db.SelectCompanyterms(lst[5].ToString().Trim(),lst[6].ToString().Trim());
					Quotation quot =new Quotation();
					string company="";
					if(list1[16].ToString().Trim() =="0")
					{
						company ="Montreal";
					}
					else
					{
						company ="Oakville";
					}
					quot.Office= company.Trim();
					quot.Customer=list1[0].ToString();
					quot.Contact=lst[0].ToString().Trim();
					quot.AttnTo1=lst[0].ToString().Trim();
					quot.AttnTo2="P:"+list1[12].ToString()+" F:"+list1[13].ToString();
					quot.AttnTo3=list1[14].ToString();
					quot.BillTo1=list1[0].ToString();
					quot.BillTo2=list1[1].ToString()+", "+list1[2].ToString();
					quot.BillTo3=list1[3].ToString()+", "+list1[4].ToString()+", "+list1[5].ToString();
					quot.ShipTo1=list1[6].ToString();
					quot.ShipTo2=list1[7].ToString()+", "+list1[8].ToString();
					quot.ShipTo3=list1[9].ToString()+", "+list1[10].ToString()+", "+list1[11].ToString();
					quot.QuoteNo=Qno(lst[3].ToString().Trim());
					quot.Quotedate=DateTime.Now.ToShortDateString();
					quot.ExpiryDate=DateTime.Now.AddDays(30).ToShortDateString();;
					quot.PrepairedBy=lst[0].ToString().Trim();
					quot.Code=list2[0].ToString();
					if(list2[4].ToString().Trim()=="0")
					{
						quot.Langu="English";
					}
					else
					{
						quot.Langu="French";
					}
					
					quot.Terms=list2[2].ToString();
					quot.Delivery="TBA";
					if(list2[1].ToString().Trim()=="0")
					{
						quot.Currency="CN $";
					}
					else
					{
						quot.Currency="US $";
					}
					quot.Note="";
					quot.FinishedDate ="  ";
					quot.CowanQno= "  "; 
					quot.Items=quot.QuoteNo.ToString();
					quot.CompanyID=list2[5].ToString();
					quot.SpecSheet="";
					decimal f =120.00m;
					string st="";
					if(Convert.ToDecimal(PN.Stroke.ToString()) > f)
					{
						st="For pricing please consult factory";
						lblstatus.Text="true";
					}
					string strn="";
					for (int t=0;t<PN.Popular_Opt.Count; t++)
					{
						strn +=(PN.Popular_Opt[t].ToString().Trim());
					}
					LBLPNo.Text=PN.PNO.ToString();
					string sto ="";
					if(PN.StopTube.ToString() !="")
					{
						decimal a =0.00m;
						decimal b =0.00m;
						decimal c =0.00m;
						string ss="";
						if(PN.StopTube.Trim().Substring(0,3) =="DST")
						{
							ss=PN.StopTube.Trim().Substring(3);
						}
						else
						{
							ss=PN.StopTube.Trim().Substring(2);
						}
						a =Convert.ToDecimal(ss);
						b= Convert.ToDecimal(PN.Stroke.ToString());
						c= b - a;
						sto =" (Effective Stroke ="+ c.ToString()+"\")";
					}
					string rd=PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim();
					string rodenddim ="";
					if(db.SelectRRD(rd).Trim() !="")
					{
						rodenddim =" KK = "+db.SelectRRD(rd);
					}
					string portsiz="";
					
					string table="";
					if(PN.Series.Trim().Substring(0,1) =="A" || PN.Series.Trim().Substring(0,1) =="S")
					{
						table ="WEB_SeriesAPortSize_TableV1";
					}
					else if(PN.Series.Trim().Substring(0,1) =="P" || PN.Series.Trim().Substring(0,1) =="N")
					{
						table ="WEB_SeriesPNPortSize_TableV1";
					}
					else if(PN.Series.Trim().Substring(0,1) =="M" || PN.Series.Trim().Substring(0,1) =="L" || PN.Series.Trim().Substring(0,1) =="R")
					{
						table ="WEB_SeriesMMLLRPortSize_TableV1";
					}
					if(PN.Series.Trim() !="Z")
					{
						if(db.SelectPortSizeNo(PN.Bore_Size.Trim(),PN.Port_Type.Trim(),table.Trim()) !="")
						{
							portsiz ="#"+db.SelectPortSizeNo(PN.Bore_Size.Trim(),PN.Port_Type.Trim(),table.Trim());
						}
					}
					QItems Qitem =new QItems();
					Qitem.ItemNo =quot.QuoteNo.ToString();
					Qitem.S_Code=PN.Series.ToString().Trim();
					Qitem.Series=db.SelectValue("Series_Name","WEB_Series_TableV1","Series_Code",PN.Series.ToString().Trim()).ToString();
					Qitem.S_Price="";
					Qitem.B_Code=PN.Bore_Size.ToString().Trim();
					Qitem.Bore=db.SelectValue("Bore_Size","WEB_Bore_TableV1","Bore_Code",PN.Bore_Size.ToString().Trim()).ToString()+" Bore Size";
					Qitem.B_Price="";
					Qitem.R_Code=PN.Rod_Diamtr.ToString().Trim();
					Qitem.Rod=db.SelectValue("Rod_Size","WEB_RodSerZ_TableV1","Rod_Code",PN.Rod_Diamtr.ToString().Trim())+" Rod Size";
					Qitem.R_Price="";
					Qitem.Stroke_Code=PN.Stroke.ToString().Trim();
					Qitem.Stroke="Stroke = "+PN.Stroke.ToString()+"\""+sto.ToString()+st.ToString();
					Qitem.Stroke_Price="";
					Qitem.M_code=PN.Mount.ToString().Trim();
					Qitem.Mount=db.SelectValue("Mount_Type","WEB_Mount"+Qitem.S_Code.Trim()+"_TableV1","Mount_Code",PN.Mount.ToString().Trim()).ToString();
					Qitem.M_Price="";
					Qitem.RE_Code=PN.Rod_End.ToString().Trim();
					Qitem.RodEnd=db.SelectValue("RodEnd_Shape","WEB_RodEndKK_TableV1","RodEnd_Code",PN.Rod_End.ToString().Trim()).ToString()+rodenddim.ToString();
					Qitem.RE_Price="";
					Qitem.Cu_Code=PN.Cushions.ToString().Trim();
					Qitem.Cushion=db.SelectValue("Cushion_type","WEB_Cushion_TableV1","Cushion_Code",PN.Cushions.ToString()).ToString();
					Qitem.Cu_Price="";
					Qitem.CushionPos_Code=PN.CushionPosition.ToString().Trim();
					Qitem.CushionPos=db.SelectValue("CushionPos_Pos","WEB_CushionPos_TableV1","CushionPos_Code",PN.CushionPosition.ToString().Trim()).ToString();
					Qitem.CushionPos_Price="";
					Qitem.Sel_Code=PN.Seal_Comp.ToString().Trim();
					Qitem.Seal=db.SelectValue("Seal_Type","WEB_Seal_TableV1","Seal_Code",PN.Seal_Comp.ToString().Trim()).ToString();
					Qitem.Sel_Price="";
					Qitem.Port_Code=PN.Port_Type.ToString().Trim();
					Qitem.Port=portsiz.Trim()+" "+ db.SelectValue("PortType_Type","WEB_portType_TableV1","PortType_Code",PN.Port_Type.ToString().Trim()).ToString();
					Qitem.Port_Price="";
					Qitem.PP_Code=PN.Port_Pos.ToString().Trim();
					Qitem.PortPos=db.SelectValue("PortPos_Position","WEB_PortPosition_TableV1","PortPos_Code",PN.Port_Pos.ToString().Trim()).ToString();
					Qitem.PP_Price="";
					Qitem.Quantity=TxtQty.Text.ToString();
					Qitem.Discount=lblD.Text;
					
					Qitem.Cusomer_ID=quot.Customer.Trim();
					Qitem.Quotation_No=quot.QuoteNo.Trim();
					Qitem.Q_Date=quot.Quotedate.Trim();
					Qitem.User_ID=lst[0].ToString();
					Qitem.PriceList="0";
					string upload="";
					if(UploadDwg.Value.Trim()!="")
					{
						if(UploadDwg.PostedFile.ContentType.ToString() =="application/pdf")
						{
							upload=UploadFile(sender,e);
							lblstatus.Text ="true";
						}
						else
						{
							LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Please select a PDF file!!!')</script>";
						}
					}
					else if(cbupload.Checked ==true && lblfile.Text.Trim() !="")
					{
						string source=Server.MapPath("./temp_drawing/" + lblfile.Text.Trim());
						string destination=Server.MapPath("./icyl_drawing/" + lblfile.Text.Trim());
						System.IO.File.Copy(source,destination,true);
						upload=lblfile.Text.Trim();
						lblstatus.Text ="true";
					}
					lblfile.Text=upload.ToString();
					Qitem.DWG=upload.ToString();
					if(TxtSpecialReq.Text.Trim() !="")
					{
						Qitem.SpecialReq =TxtSpecialReq.Text;
					}
					else
					{
						Qitem.SpecialReq ="";
					}
					string commadd="";
					if(lbl.Text.Trim() !="")
					{
						commadd="WEB_"+lbl.Text.Trim();
					}
					//issue #644 start
					if(lst[6].ToString().Trim() =="1030" && (Qitem.S_Code=="PA" || Qitem.S_Code=="PC" ||Qitem.S_Code=="PS" ))
					{
						switch(Qitem.S_Code)
						{
							case "PA":
								commadd="SMCSeries"+Qitem.S_Code.Trim();
								break;
							case "PC":
								commadd="SMCSeries"+Qitem.S_Code.Trim();
								break;
							case "PS":
								commadd="SMCSeries"+Qitem.S_Code.Trim();
								break;
						}

					}
					//issue #644 end
					decimal appoptprice=0.00m;
					decimal pindex =0.00m;
					pindex =Convert.ToDecimal(db.SelectPriceIndex(lbl.Text.Trim().Substring(6)));
					if(PN.Application_Opt.Count >0)
					{
						for(int cont=0;cont < PN.Application_Opt.Count; cont++)
						{
							bool price=false;
							decimal app=0.00m;
							if(PN.Application_Opt[cont].ToString().Trim() =="G1")
							{
								app =db.SelectAddersPrice("G1",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								if(app ==0)
								{
									price =true;
								}
							}
							else if(PN.Application_Opt[cont].ToString().Trim() =="G2")
							{
								app =db.SelectAddersPrice("G2",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								if(app ==0)
								{
									price =true;
								}
							}
							else if(PN.Application_Opt[cont].ToString().Trim() =="G3")
							{
								app =db.SelectAddersPrice("G3",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								if(app ==0)
								{
									price =true;
								}
							}
							else if(PN.Application_Opt[cont].ToString().Trim() =="G4")
							{
								app =db.SelectAddersPrice("G4",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								if(app ==0)
								{
									price =true;
								}
							}
							else if(PN.Application_Opt[cont].ToString().Trim() =="G5")
							{
								app =db.SelectAddersPrice("G5",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								if(app ==0)
								{
									price =true;
								}
							}
							else if(PN.Application_Opt[cont].ToString().Trim() =="G6")
							{
								app =db.SelectAddersPrice("G6",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								if(app ==0)
								{
									price =true;
								}
							}
							else if(PN.Application_Opt[cont].ToString().Trim() =="G7")
							{
								app =db.SelectAddersPrice("G7",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								if(app ==0)
								{
									price =true;
								}
							}
							else if(PN.Application_Opt[cont].ToString().Trim() =="GR1")
							{
								app =db.SelectAddersPrice("GR1",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								if(app ==0)
								{
									price =true;
								}
							}
							else if(PN.Application_Opt[cont].ToString().Trim() =="GR2")
							{
								if(DDLSeal.SelectedItem.Text.Trim() =="F")
								{
									app =db.SelectAddersPrice("FGR2",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									if(app ==0)
									{
										price =true;
									}
								}
								else
								{	
									app =db.SelectAddersPrice("GR2",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									if(app ==0)
									{
										price =true;
									}
								}
							}
							//issue #104 start
							else if(PN.Application_Opt[cont].ToString().Trim() =="GT3")
							{
								app =db.SelectAddersPrice("GT3",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								if(app ==0)
								{
									price =true;
								}
							}
								//issue #104 end
							else if(PN.Application_Opt[cont].ToString().Trim() =="P1")
							{
								app =db.SelectAddersPrice("P1",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								if(app ==0)
								{
									price =true;
								}
							}
							else if(PN.Application_Opt[cont].ToString().Trim() =="P2")
							{
								app =db.SelectAddersPrice("P2",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								if(app ==0)
								{
									price =true;
								}
							}
							else if(PN.Application_Opt[cont].ToString().Trim() =="P3")
							{
								app =db.SelectAddersPrice("P3",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								if(app ==0)
								{
									price =true;
								}
							}
							else if(PN.Application_Opt[cont].ToString().Trim() =="P4")
							{
								app =db.SelectAddersPrice("P4",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								if(app ==0)
								{
									price =true;
								}
							}
							else if(PN.Application_Opt[cont].ToString().Trim() =="S1")
							{
								app =db.SelectAddersPrice("S1",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								if(app ==0)
								{
									price =true;
								}
							}
							
							else if(PN.Application_Opt[cont].ToString().Trim() =="S2")
							{
								app =db.SelectAddersPrice("S2",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								if(app ==0)
								{
									price =true;
								}
							}
							else if(PN.Application_Opt[cont].ToString().Trim() =="S3")
							{
								app =db.SelectAddersPrice("S3",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								if(app ==0)
								{
									price =true;
								}
							}
							else if(PN.Application_Opt[cont].ToString().Trim() =="S4")
							{
								app =db.SelectAddersPrice("S4",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								if(app ==0)
								{
									price =true;
								}
							}
							else if(PN.Application_Opt[cont].ToString().Trim() =="S5")
							{
								app =db.SelectAddersPrice("S5",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								if(app ==0)
								{
									price =true;
								}
							}
							else if(PN.Application_Opt[cont].ToString().Substring(0,2) =="RM" || PN.Application_Opt[cont].ToString().Substring(0,2) =="RX" || PN.Application_Opt[cont].ToString().Substring(0,2) =="RY")
							{							
								
								app =db.SelectAddersPrice("MetricRodEnd",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								if(app ==0)
								{
									price =true;
								}
							}
							else if(PN.Application_Opt[cont].ToString().Trim() =="W1")
							{
								decimal t1,t2,t3=0.00m;
								t1 =db.SelectAddersPrice("M3Base",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								t2 =db.SelectAddersPrice("M3PerInch",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								t3 = Convert.ToDecimal(TxtStroke.Text.Trim());
								app= (t1 + (t2 * t3)+ 275);
								if(app ==0  || app ==275)
								{
									price =true;
								}
							}
							//issue #90 start
							else if(PN.Application_Opt[cont].ToString().Trim()=="CS")
							{							
								//app= 391m;
								app =db.SelectAddersPrice_ByBore("CS",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim());
								app=Math.Round(app,2);
							}
							//isue #90 end
							if(app ==0 && price ==true)
							{
								lblstatus.Text = "true";
									
							}	
							appoptprice +=app;
							
						}
							
						ArrayList temp1 =new ArrayList();
						for(int p=0;p< PN.Application_Opt.Count;p++)
						{
							temp1.Add(db.SelectAppopts(PN.Application_Opt[p].ToString().Trim()));
						}
						for(int q=0;q< temp1.Count;q++)
						{
							db.InsertItemApplicationOpts(quot.QuoteNo.Trim(),LBLPNo.Text.Trim(), PN.Application_Opt[q].ToString(),temp1[q].ToString(),"0.00");
						}
						
						Qitem.ApplicationOpt_Id=quot.QuoteNo.Trim()+LBLPNo.Text.Trim();
					}
					else
					{
						Qitem.ApplicationOpt_Id="";
					}
					decimal pop=0.00m;
					if(PN.Popular_Opt.Count >0)
					{
						for(int cont=0;cont < PN.Popular_Opt.Count; cont++)
						{
							decimal total =0.00m;
							decimal t1,t2,t3 =0.00m;
							bool result1=false;
							if(PN.Popular_Opt[cont].ToString().Substring(0,1) =="A")
							{
								if(PN.Popular_Opt[cont].ToString().Substring(0,2) =="AD")
								{
									t1 =db.SelectAddersPrice("A",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									t2=Convert.ToDecimal(PN.Popular_Opt[cont].ToString().Substring(2).ToString());
									total = t1 * t2;
									if(total ==0)
									{
										result1 =true;
									}
								}
								else
								{
									t1 =db.SelectAddersPrice("A",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									t2 =Convert.ToDecimal(PN.Popular_Opt[cont].ToString().Substring(1).ToString());
									total = t1 * t2;
									if(total ==0)
									{
										result1 =true;
									}
								}
							}
							else if(PN.Popular_Opt[cont].ToString().Substring(0,1) =="W" && PN.Popular_Opt[cont].ToString().Length  > 2 )
							{
								if(PN.Popular_Opt[cont].ToString().Substring(0,2) =="WD")
								{
									t1 =db.SelectAddersPrice("W",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									t2 =Convert.ToDecimal(PN.Popular_Opt[cont].ToString().Substring(2).ToString());
									total = t1 * t2;
									if(total ==0)
									{
										result1 =true;
									}
								}
								else
								{
									t1=db.SelectAddersPrice("W",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									t2=Convert.ToDecimal(PN.Popular_Opt[cont].ToString().Substring(1).ToString());
									total = t1 * t2;
									if(total ==0)
									{
										result1 =true;
									}
								}
							}
							else if(PN.Popular_Opt[cont].ToString().Substring(0,2) =="BB" && PN.Popular_Opt[cont].ToString().Length  > 3 )
							{
								if(PN.Popular_Opt[cont].ToString().Substring(0,3) =="BBC")
								{								
									t1=db.SelectAddersPrice("BBBase",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									t2=db.SelectAddersPrice("BBPerInch",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									t3=Convert.ToDecimal(PN.Popular_Opt[cont].ToString().Substring(3));
									total = t1 +( t2 * t3);
									if(total ==0)
									{
										result1 =true;
									}
								}
								else
								{
									t1=db.SelectAddersPrice("BBBase",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									t2=db.SelectAddersPrice("BBPerInch",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									t3=Convert.ToDecimal(PN.Popular_Opt[cont].ToString().Substring(3));
									total = t1 +( t2 * t3);
									if(total ==0)
									{
										result1 =true;
									}
								}
								
							}
							else if(PN.Popular_Opt[cont].ToString().Substring(0,1) =="S")
							{
								PNo.StopTube =PN.Popular_Opt[cont].ToString();
								t1=db.SelectAddersPrice("STBase",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								t2=db.SelectAddersPrice("STPerInch",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								t3=Convert.ToDecimal(PN.Popular_Opt[cont].ToString().Substring(2).ToString());
								total = t1 +( t2 * t3);
								if(total ==0)
								{
									result1 =true;
								}
							}
							else if(PN.Popular_Opt[cont].ToString().Substring(0,2) =="DS")
							{
								PNo.StopTube =PN.Popular_Opt[cont].ToString();
								t1 =db.SelectAddersPrice("DSTBase",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								total =t1;
								if(total ==0)
								{
									result1 =true;
								}
							}
							else if(PN.Popular_Opt[cont].ToString().Trim().Substring(0,1) =="T" && PN.Popular_Opt[cont].ToString().Trim().Substring(0,2) !="TC")
							{
								if(PN.Popular_Opt[cont].ToString().Length >2)
								{
									if(PN.Popular_Opt[cont].ToString().Trim().Substring(2,1) =="S")
									{
										decimal ts1=0.00m;
										decimal ts2=0.00m;
										decimal ts3=0.00m;
										decimal ts4=0.00m;
										decimal strk=0.00m;
										decimal res=0.00m;
										strk=Convert.ToDecimal(PN.Stroke.Trim());
										ts1=db.SelectAddersPrice(PN.Popular_Opt[cont].ToString().Substring(0,3),commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										if(PN.Popular_Opt[cont].ToString().Substring(0,3) =="T1S" ||PN.Popular_Opt[cont].ToString().Substring(0,2) =="T1")
										{
											ts2=db.SelectAddersPrice("T1Adder",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										}
										else
										{
											ts2=db.SelectAddersPrice("TAdder",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										}
										if(PN.Popular_Opt[cont].ToString().Substring(2,1) =="S")
										{
											ts3=db.SelectAddersPrice("TSBase",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
											ts4=db.SelectAddersPrice("TSPerInch",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
											res=ts3 + (ts4 * strk);
										}
										total =ts1 + ts2 +res;

									}
									else if(PN.Popular_Opt[cont].ToString().Trim().Substring(2,1) =="R")
									{
									
										decimal ts1=0.00m;
										decimal ts2=0.00m;
										decimal strk=0.00m;
										decimal res=0.00m;
										strk=Convert.ToDecimal(PN.Stroke.Trim());
										ts1=db.SelectAddersPrice(PN.Popular_Opt[cont].ToString().Substring(0,2),commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										if(PN.Popular_Opt[cont].ToString().Substring(0,3) =="T1R" ||PN.Popular_Opt[cont].ToString().Substring(0,2) =="T1")
										{
											ts2=db.SelectAddersPrice("T1Adder",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										}
										else
										{
											ts2=db.SelectAddersPrice("TAdder",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										}
										res=144.35m;
										total =ts1 + ts2 - res;
									}
									else if(PN.Popular_Opt[cont].ToString().Trim().Substring(1,1) =="1" || PN.Popular_Opt[cont].ToString().Trim().Substring(1,1) =="2"
										||PN.Popular_Opt[cont].ToString().ToString().Trim().Substring(1,1) =="3" || PN.Popular_Opt[cont].ToString().Trim().Substring(1,1) =="4"
										|| PN.Popular_Opt[cont].ToString().Trim().Substring(1,1) =="5")
									{
										decimal ts1=0.00m;
										decimal ts2=0.00m;
										ts1=db.SelectAddersPrice(PN.Popular_Opt[cont].ToString().Substring(0,2),commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										if(PN.Popular_Opt[cont].ToString().Substring(0,2) =="T1")
										{
											ts2=db.SelectAddersPrice("T1Adder",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										}
										else
										{
											ts2=db.SelectAddersPrice("TAdder",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										}
										total =ts1 + ts2;

									}
								}
								else
								{
									decimal ts1=0.00m;
									decimal ts2=0.00m;
									ts1=db.SelectAddersPrice(PN.Popular_Opt[cont].ToString().Substring(0,2),commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									if(PN.Popular_Opt[cont].ToString().Substring(0,2) =="T1")
									{
										ts2=db.SelectAddersPrice("T1Adder",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else
									{
										ts2=db.SelectAddersPrice("TAdder",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									total =ts1 + ts2;
								}								
								if(total ==0)
								{
									result1 =true;
								}
							}
							else
							{
								if(PN.Popular_Opt[cont].ToString() =="M1")
								{
									t1 =db.SelectAddersPrice("M1Base",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									t2 =db.SelectAddersPrice("M1PerInch",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									t3 =Convert.ToDecimal(TxtStroke.Text.Trim());
									total =-(t1 + (t2 * t3)); 
									if(total ==0)
									{
										result1 =true;
									}
								}
								else if(PN.Popular_Opt[cont].ToString() =="M2")
								{
									t1 =db.SelectAddersPrice("M2Base",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									t2 =db.SelectAddersPrice("M2PerInch",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									t3 =Convert.ToDecimal(TxtStroke.Text.Trim());
									total =t1 + (t2 * t3); 
									if(total ==0)
									{
										result1 =true;
									}
								}
								else if(PN.Popular_Opt[cont].ToString() =="M3")
								{
									t1 =db.SelectAddersPrice("M3Base",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									t2 =db.SelectAddersPrice("M3PerInch",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									t3 =Convert.ToDecimal(TxtStroke.Text.Trim());
									total =t1 + (t2 * t3); 
									if(total ==0)
									{
										result1 =true;
									}
								
								}
								else if(PN.Popular_Opt[cont].ToString() =="M4")
								{
									t1 =db.SelectAddersPrice("M4Base",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									t2 =db.SelectAddersPrice("M4PerInch",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									t3 =Convert.ToDecimal(TxtStroke.Text.Trim());
									total =t1 + (t2 * t3); 
									if(total ==0)
									{
										result1 =true;
									}
								}
								else if(PN.Popular_Opt[cont].ToString() =="M5")
								{
									t1 =db.SelectAddersPrice("M5Base",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									t2 =db.SelectAddersPrice("M5PerInch",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									t3 =Convert.ToDecimal(TxtStroke.Text.Trim());
									total =t1 + (t2 * t3); 
									if(total ==0)
									{
										result1 =true;
									}
								}
								else if(PN.Popular_Opt[cont].ToString() =="M7")
								{
									t1 =db.SelectAddersPrice("M7Base",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									t2 =db.SelectAddersPrice("M7PerInch",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									t3 =Convert.ToDecimal(TxtStroke.Text.Trim());
									total =t1 + (t2 * t3); 
									if(total ==0)
									{
										result1 =true;
									}
								}
								else if(PN.Popular_Opt[cont].ToString().Substring(0,1) =="P")
								{
									t1 =db.SelectAddersPrice("A"+PN.Popular_Opt[cont].ToString().Trim(),commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									total =t1; 
									if(total ==0)
									{
										result1 =true;
									}
								}
								
							}
							if(result1 ==true)
							{
								lblstatus.Text ="true";
							}
							pop +=total;
						}
						decimal d1,d2=0.00m;
						//total price
						d1 =Convert.ToDecimal(lbltotal.Text);
						//issue #104 start
						//backup
						//appoptprice =appoptprice + appoptprice * (pindex /100);
						//pop =pop + pop * (pindex /100);
						//d2 =d1 + appoptprice + pop;
						//issue #104 end
						//issue #645
						//backup
						//lbltotal.Text=d2.ToString();
						//issue #645 end
						decimal totaltandem =0.00m;
						for(int cont=0;cont < PN.Popular_Opt.Count; cont++)
						{
							
							//							decimal t1,t2,t3,t4,t5 =0.00m;
							bool result1=false;
							if(PN.Popular_Opt[cont].ToString().Substring(0,2) =="DC")
							{
								//								decimal duplex=0.00m;
								//								if(PN.Series.ToString().Trim()  =="PA" || PN.Series.ToString().Trim()  =="PC" || PN.Series.ToString().Trim() =="PS")
								//								{
								//									duplex =db.SelectAddersPrice("DC",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								//									if(duplex ==0)
								//									{
								result1 =true;
								//									}
								//								}
								//								t1 = Convert.ToDecimal(lbltotal.Text); 
								//								t2 =  Convert.ToDecimal(lblstroke.Text); 
								//								t3 = Convert.ToDecimal(TxtStroke.Text);
								//								t4 =t2 / t3;
								//								t5 = t4 * (Convert.ToDecimal(PN.Popular_Opt[cont].ToString().Substring(2).ToString()));
								//								totaltandem =t1 - t2 + t5 + duplex;
								//								if(totaltandem ==0)
								//								{
								//									result1 =true;
								//								}
								if(result1 ==true)
								{
									lblstatus.Text ="true";
								}
							}
							else if(PN.Popular_Opt[cont].ToString().Substring(0,2) =="BC")
							{
								//								decimal back=0.00m;
								//								decimal tt4=0.00m;
								//								if(PN.Series.ToString().Trim() =="PA" || PN.Series.ToString().Trim() =="PC" || PN.Series.ToString().Trim() =="PS")
								//								{
								//									back =db.SelectAddersPrice("BC",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								//									if(back ==0)
								//									{
								result1 =true;
								//									}
								//									if(PN.Mount.ToString().Trim() == "T4")
								//									{
								//										tt4=db.SelectAddersPrice("BCT4",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								//										if(tt4 ==0)
								//										{
								//											result1 =true;
								//										}
								//									}
								//								}
								//								t1 = Convert.ToDecimal(lbltotal.Text); 
								//								t2 =  Convert.ToDecimal(lblstroke.Text); 
								//								t3 = Convert.ToDecimal(TxtStroke.Text);
								//								t4 =t2 / t3;
								//								t5 = t4 * (Convert.ToDecimal(PN.Popular_Opt[cont].ToString().Substring(2).ToString()));
								//								totaltandem =t1 - t2 + t5+ back +tt4;
								//							
								//								if(totaltandem ==0)
								//								{
								//									result1 =true;
								//								}
								if(result1 ==true)
								{
									lblstatus.Text ="true";
								}
							}
							else if(PN.Popular_Opt[cont].ToString().Substring(0,2) =="TC")
							{
								//								decimal tand=0.00m;
								//								if(PN.Series.ToString().Trim() =="PA" || PN.Series.ToString().Trim() =="PS"|| PN.Series.ToString().Trim() =="PC")
								//								{
								//									if(PN.Series.ToString().Trim() !="PC" )
								//									{
								//										tand =db.SelectAddersPrice("TC",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
								//										if(tand ==0)
								//										{
								result1 =true;
								//										}
								//									}
								//								}
								//								t1 = Convert.ToDecimal(lbltotal.Text); 
								//								totaltandem =t1 + tand;
								//								
								//								if(totaltandem ==0)
								//								{
								//									result1 =true;
								//								}
								if(result1 ==true)
								{
									lblstatus.Text ="true";
								}
							}
								
						}
						decimal dcc1,dcc2=0.00m;
						//total price
						dcc1 =Convert.ToDecimal(lbltotal.Text);
						dcc2 =dcc1 + totaltandem;
						lbltotal.Text=dcc2.ToString();

						ArrayList temp2 =new ArrayList();
						for(int p=0;p<PN.Popular_Opt.Count;p++)
						{
							if(PN.Popular_Opt[p].ToString().Substring(0,1) =="A")
							{
								if(PN.Popular_Opt[p].ToString().Substring(0,2) =="AD")
								{
									temp2.Add("Second thread extension AD="+PN.Popular_Opt[p].ToString().Substring(2).ToString()+"\"");
								}
								else
								{
									temp2.Add("Thread extension A="+PN.Popular_Opt[p].ToString().Substring(1).ToString()+"\"");
								}
							}
							else if(PN.Popular_Opt[p].ToString().Substring(0,1) =="W" && PN.Popular_Opt[p].ToString().Length  > 2)
							{
								if(PN.Popular_Opt[p].ToString().Substring(0,2) =="WD")
								{
									temp2.Add("Second rod extension WD="+PN.Popular_Opt[p].ToString().Substring(2).ToString()+"\"");
								}
								else
								{
									temp2.Add("Rod extension W="+PN.Popular_Opt[p].ToString().Substring(1).ToString()+"\"");
								}
							}
							else if(PN.Popular_Opt[p].ToString().Substring(0,2) =="BB" && PN.Popular_Opt[p].ToString().Length  > 3)
							{
								if(PN.Popular_Opt[p].ToString().Substring(0,3) =="BBC")
								{
									temp2.Add("Special Tie rod Extension (Cap End)BB="+PN.Popular_Opt[p].ToString().Substring(3).ToString()+"\"");
								}
								else
								{
									temp2.Add("Special Tie rod Extension (Head End)BB="+PN.Popular_Opt[p].ToString().Substring(3).ToString()+"\"");
								}
							}
							else if(PN.Popular_Opt[p].ToString().Substring(0,1) =="S")
							{
										
								temp2.Add("Stop Tube ST="+PN.Popular_Opt[p].ToString().Substring(2).ToString()+"\"");
										
							}
							else if(PN.Popular_Opt[p].ToString().Substring(0,2) =="DS")
							{
								
								temp2.Add("Stop Tube Double Piston Design DST="+PN.Popular_Opt[p].ToString().Substring(3).ToString()+"\"");
								
							}
							else if(PN.Popular_Opt[p].ToString().Substring(0,2) =="DC")
							{
								
								temp2.Add("Duplex Inline.(Rods not Attached) Second Stroke DC= "+PN.Popular_Opt[p].ToString().Substring(2).ToString()+"\"");
								
							}
							else if(PN.Popular_Opt[p].ToString().Substring(0,2) =="BC")
							{
								
								temp2.Add("Back To Back Cylinder. Second Stroke BC= "+PN.Popular_Opt[p].ToString().Substring(2).ToString()+"\"");
								
							}
							else if(PN.Popular_Opt[p].ToString().Substring(0,1) =="X")
							{
										
								temp2.Add("Intermediate trunnions XI="+PN.Popular_Opt[p].ToString().Substring(2).ToString()+"\"");
										
							}							
							else if(PN.Popular_Opt[p].ToString().Substring(0,1) =="T" && PN.Popular_Opt[p].ToString().Substring(0,2) !="TC" )
							{
								string tt="";	
								if(PN.Popular_Opt[p].ToString().Length >2)
								{
									if(PN.Popular_Opt[p].ToString().Trim().Substring(2,1) =="S")
									{									
										ArrayList trlist=new ArrayList();
										trlist=db.SelectTransducerDisc(PN.Popular_Opt[p].ToString().Substring(0,3));		
										for(int h=0;h<trlist.Count;h++)
										{
											tt +=trlist[h].ToString();
											if(trlist[h].ToString()!=null && h < trlist.Count -1)
											{
												tt +=", ";
											}
										}
										if(PN.Popular_Opt[p].ToString().Trim().Length >3)
										{
											tt =tt.Replace("10K",PN.Popular_Opt[p].ToString().Substring(3)+"K");
										}
									}
									else if(PN.Popular_Opt[p].ToString().Trim().Substring(2,1) =="R")
									{
									
										ArrayList trlist=new ArrayList();
										trlist=db.SelectTransducerDisc(PN.Popular_Opt[p].ToString().Substring(0,3));		
										for(int h=0;h<3;h++)
										{
											tt +=trlist[h].ToString();
											if(trlist[h].ToString()!=null && h < 3)
											{
												tt +=", ";
											}
										}
										if(PN.Popular_Opt[p].ToString().Trim().Length >3)
										{
											tt =tt.Replace("10K",PN.Popular_Opt[p].ToString().Substring(3)+"K");
										}
									}
									if(PN.Popular_Opt[p].ToString().Trim().Substring(2,1) !="S" && PN.Popular_Opt[p].ToString().Trim().Substring(2,1) =="R")
									{
										if(PN.Popular_Opt[p].ToString().Trim().Substring(1,1) =="1" ||PN.Popular_Opt[p].ToString().Trim().Substring(1,1) =="2"
											||PN.Popular_Opt[p].ToString().Trim().Substring(1,1) =="3" || PN.Popular_Opt[p].ToString().Trim().Substring(1,1) =="4"
											|| PN.Popular_Opt[p].ToString().Trim().Substring(1,1) =="5")
										{
										
											ArrayList trlist=new ArrayList();
											trlist=db.SelectTransducerDisc(PN.Popular_Opt[p].ToString().Substring(0,2));		
											for(int h=0;h<trlist.Count;h++)
											{
												tt +=trlist[h].ToString();
												if(trlist[h].ToString()!=null && h < trlist.Count -1)
												{
													tt +=", ";
												}
											}
											if(PN.Popular_Opt[p].ToString().Trim().Length >2)
											{
												tt =tt.Replace("10K",PN.Popular_Opt[p].ToString().Substring(2)+"K");
											}
										}
									}
								}
								else
								{
									ArrayList trlist=new ArrayList();
									trlist=db.SelectTransducerDisc(PN.Popular_Opt[p].ToString().Substring(0,2));		
									for(int h=0;h<trlist.Count;h++)
									{
										tt +=trlist[h].ToString();
										if(trlist[h].ToString()!=null && h < trlist.Count -1)
										{
											tt +=", ";
										}
									}
								}
								temp2.Add(tt.Trim());
							}
							else
							{
								temp2.Add(db.SelectPopopts(PN.Popular_Opt[p].ToString().Trim()));
							}
							
						}						
						for(int t=0;t< temp2.Count;t++)
						{
							db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LBLPNo.Text.Trim(), PN.Popular_Opt[t].ToString(),temp2[t].ToString(),"0.00");
						}
						Qitem.PopularOpt_Id=quot.QuoteNo.Trim()+LBLPNo.Text.Trim();
					}
					else
					{
						Qitem.PopularOpt_Id="";
					}
					if(TxtSpecialReq.Text.ToString().Trim() !="" )
					{
						lblstatus.Text ="true";
					}
								
					//total price
					decimal dc1 ,dc2,dc3=0.00m;
					dc1=Convert.ToDecimal(TxtQty.Text.ToString());
					dc2=Convert.ToDecimal(lbltotal.Text.ToString());
					//total price
					//issue #104 start
					appoptprice =appoptprice + appoptprice * (pindex /100);
					pop =pop + pop * (pindex /100);
					//issue #315 start
					if(PN.Series.ToString().Trim()=="A" || PN.Series.ToString().Trim()=="AC" || PN.Series.ToString().Trim()=="AT" || PN.Series.ToString().Trim()=="AS")
					{
						decimal discount = discount=Convert.ToDecimal(lblD.Text);
						appoptprice = appoptprice*(1 - (discount/100));
						pop = pop*(1 - (discount/100));
						
					}
					//total price
					//issue #315 end
					dc2 =dc2 + appoptprice + pop;
					//issue #648 start
                    lbltotal.Text = dc2.ToString();
					//issue #648
					//issue #104 end
					dc3= dc1 * dc2 ;
					if(lblstatus.Text.ToLower().Trim() =="true")
					{
						Qitem.UnitPrice ="0.00";
						Qitem.TotalPrice="0.00";
					}
					else
					{
						Qitem.UnitPrice=lbltotal.Text.ToString();
						Qitem.TotalPrice=dc3.ToString();
					}
					Qitem.Special_ID="";
					Qitem.PartNo=LBLPNo.Text.ToString();
					string strd="";
					if(PN.Series.Trim() !="")
					{
						string weight="";
						decimal c1,c2,c3,c22,c5=0.00m;
						decimal c4=0.00m;
						//issue weight  start backup
                        //if(PN.Series.Trim()== "A" || PN.Series.Trim()== "AT")
                        //issue weight  end backup
						//issue weight start update
						if(PN.TandemDuplex =="" && PN.DoubleRod.ToUpper().Trim() =="NO")
						{
							c1=db.SelectAddersPrice("SWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
							c2=db.SelectAddersPrice("SStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
							c4=c1+c2*Convert.ToDecimal(PN.Stroke.Trim());
							c4=Decimal.Round(c4,0);
						}
						if(PN.TandemDuplex =="" && PN.DoubleRod.ToUpper().Trim() =="YES")
						{
							c1=db.SelectAddersPrice("DWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
							c2=db.SelectAddersPrice("DStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
							c4=c1+c2*Convert.ToDecimal(PN.Stroke.Trim());
							c4=Decimal.Round(c4,0);
						}
						if(PN.TandemDuplex !="" && PN.DoubleRod.ToUpper().Trim() =="NO")
						{
							c1=db.SelectAddersPrice("SWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
							c2=db.SelectAddersPrice("SStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
							c22=db.SelectAddersPrice("DStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
							c4=c1*1.75m+(c2+c22)*Convert.ToDecimal(PN.Stroke.Trim());
							c4=Decimal.Round(c4,0);
						}
						if(PN.TandemDuplex !="" && PN.DoubleRod.ToUpper().Trim() =="YES")
						{
							c1=db.SelectAddersPrice("DWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
							c22=db.SelectAddersPrice("DStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
							c4=c1*1.75m+2m*c22*Convert.ToDecimal(PN.Stroke.Trim());
							c4=Decimal.Round(c4,0);
						}
						//issue weight end update
						if(PN.Series.Trim()== "AT")
						{
							if(PN.DoubleRod.Trim() =="Yes")
							{
								c1=db.SelectAddersPrice("DWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								c2=db.SelectAddersPrice("DStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								c3=Convert.ToDecimal(PN.Stroke.Trim());
							}
							else
							{
								c1=db.SelectAddersPrice("SWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								c2=db.SelectAddersPrice("SStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								c3=Convert.ToDecimal(PN.Stroke.Trim());
							}
							c4=c1 + (c2 * c3);
							c4=Decimal.Round(c4,0);
						}
						else if(PN.Series.Trim()== "M" || PN.Series.Trim()== "ML" || PN.Series.Trim()== "L")
						{
							if(PN.Bore_Size.Trim() =="P" || PN.Bore_Size.Trim() =="R" || PN.Bore_Size.Trim() =="S" || PN.Bore_Size.Trim() =="T" || PN.Bore_Size.Trim() =="W" || PN.Bore_Size.Trim() =="X")
							{
								if(PN.DoubleRod.Trim() =="Yes")
								{
									c1=db.SelectAddersPrice("DWeight","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									c2=db.SelectAddersPrice("DStroke","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										
									if(PN.Mount.Substring(0,2) =="T1" || PN.Mount.Substring(0,2) =="T2")
									{
										c5=db.SelectAddersPrice("SWeight1","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else if(PN.Mount.Substring(0,2) =="E5" || PN.Mount.Substring(0,2) =="E6" || PN.Mount.Substring(0,2) =="T4")
									{
										c5=db.SelectAddersPrice("SWeight2","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else if(PN.Mount.Substring(0,2) =="F5" || PN.Mount.Substring(0,2) =="F6" )
									{
										c5=db.SelectAddersPrice("SWeight3","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else if(PN.Mount.Substring(0,2) =="P1" || PN.Mount.Substring(0,2) =="S2" || PN.Mount.Substring(0,2) =="S3")
									{
										c5=db.SelectAddersPrice("SWeight4","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else
									{
										c5=db.SelectAddersPrice("SWeight1","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									c3=Convert.ToDecimal(PN.Stroke.Trim());
									c4= c1 + c5 + (c2 * c3);
									c4=Decimal.Round(c4,0);	
								}
								else
								{
									c2=db.SelectAddersPrice("SStroke","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										
									if(PN.Mount.Substring(0,2) =="T1" || PN.Mount.Substring(0,2) =="T2")
									{
										c5=db.SelectAddersPrice("SWeight1","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else if(PN.Mount.Substring(0,2) =="E5" || PN.Mount.Substring(0,2) =="E6" || PN.Mount.Substring(0,2) =="T4")
									{
										c5=db.SelectAddersPrice("SWeight2","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else if(PN.Mount.Substring(0,2) =="F5" || PN.Mount.Substring(0,2) =="F6" )
									{
										c5=db.SelectAddersPrice("SWeight3","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else if(PN.Mount.Substring(0,2) =="P1" || PN.Mount.Substring(0,2) =="S2" || PN.Mount.Substring(0,2) =="S3")
									{
										c5=db.SelectAddersPrice("SWeight4","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else
									{
										c5=db.SelectAddersPrice("SWeight1","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}	
									c3=Convert.ToDecimal(PN.Stroke.Trim());
									c4=c5 + (c2 * c3);
									c4=Decimal.Round(c4,0);								
								}
							}
							else if(PN.Bore_Size.Trim() !="P" && PN.Bore_Size.Trim() !="R" && PN.Bore_Size.Trim() !="S" && PN.Bore_Size.Trim() !="T" && PN.Bore_Size.Trim() !="W" && PN.Bore_Size.Trim() !="X")
							{
								if(PN.DoubleRod.Trim() =="Yes")
								{
									if(PN.Mount.Substring(0,1) =="P" || PN.Mount.Substring(0,1) =="T" || PN.Mount.Substring(0,1) =="E" || PN.Mount.Substring(0,1) =="S")
									{
										c1=db.SelectAddersPrice("DWeight2","WEB_SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else
									{
										c1=db.SelectAddersPrice("DWeight1","WEB_SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									c2=db.SelectAddersPrice("DStroke","WEB_SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								else
								{
									if(PN.Mount.Substring(0,1) =="P" || PN.Mount.Substring(0,1) =="T" || PN.Mount.Substring(0,1) =="E" || PN.Mount.Substring(0,1) =="S")
									{
										c1=db.SelectAddersPrice("SWeight2","WEB_SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else
									{
										c1=db.SelectAddersPrice("SWeight1","WEB_SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									c2=db.SelectAddersPrice("SStroke","WEB_SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								c3=Convert.ToDecimal(PN.Stroke.Trim());
								c4=c1 + (c2 * c3);
								c4=Decimal.Round(c4,0);
							}
						}
						else if(PN.Series.Trim()== "PS" || PN.Series.Trim()== "N" || PN.Series.Trim()== "PC")
						{
							if(PN.DoubleRod.Trim() =="Yes")
							{
								if(PN.Mount.Substring(0,1) =="P" || PN.Mount.Substring(0,1) =="T" || PN.Mount.Substring(0,1) =="E" || PN.Mount.Substring(0,1) =="S")
								{
									c1=db.SelectAddersPrice("DWeight2","WEB_SeriesPneumatics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								else
								{
									c1=db.SelectAddersPrice("DWeight1","WEB_SeriesPneumatics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								c2=db.SelectAddersPrice("DStroke","WEB_SeriesPneumatics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
							}
							else
							{
								if(PN.Mount.Substring(0,1) =="P" || PN.Mount.Substring(0,1) =="T" || PN.Mount.Substring(0,1) =="E" || PN.Mount.Substring(0,1) =="S")
								{
									c1=db.SelectAddersPrice("SWeight2","WEB_SeriesPneumatics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								else
								{
									c1=db.SelectAddersPrice("SWeight1","WEB_SeriesPneumatics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								c2=db.SelectAddersPrice("SStroke","WEB_SeriesPneumatics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
							}
							c3=Convert.ToDecimal(PN.Stroke.Trim());
							c4=c1 + c2 * c3;
							c4=Decimal.Round(c4,0);
						}
						else if(PN.Series.Trim()== "PA")
						{
							decimal c6=0.00m;
							if(PN.DoubleRod.Trim() =="Yes")
							{
								c1=db.SelectAddersPrice("DWeight","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								c2=db.SelectAddersPrice("SWeight1","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								c5=db.SelectAddersPrice("DStroke","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								if(PN.Mount.Substring(0,1) =="X")
								{
									c3=db.SelectAddersPrice("SWeight1","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								else if(PN.Mount.Substring(0,2) =="F1" || PN.Mount.Substring(0,2) =="F2")
								{
									c3=db.SelectAddersPrice("SWeight2","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								else if(PN.Mount.Substring(0,2) =="F5" || PN.Mount.Substring(0,2) =="F6")
								{
									c3=db.SelectAddersPrice("SWeight3","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								else if(PN.Mount.Substring(0,2) =="T1" || PN.Mount.Substring(0,2) =="T2")
								{
									c3=db.SelectAddersPrice("SWeight4","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								else if(PN.Mount.Substring(0,2) =="P1" || PN.Mount.Substring(0,2) =="P3")
								{
									c3=db.SelectAddersPrice("SWeight5","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								else if(PN.Mount.Substring(0,2) =="S1" || PN.Mount.Substring(0,2) =="S7")
								{
									c3=db.SelectAddersPrice("SWeight6","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								else if(PN.Mount.Substring(0,2) =="T4" )
								{
									c3=db.SelectAddersPrice("SWeight7","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								else if(PN.Mount.Substring(0,2) =="P4" )
								{
									c3=db.SelectAddersPrice("SWeight8","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								else if(PN.Mount.Substring(0,2) =="S2" )
								{
									c3=db.SelectAddersPrice("SWeight9","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								else if(PN.Mount.Substring(0,2) =="P2" )
								{
									c3=db.SelectAddersPrice("SWeight10","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								else
								{
									c3=db.SelectAddersPrice("DWeight","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								c6=Convert.ToDecimal(PN.Stroke.Trim());
								c4=(c3 - c2)+ c1 + (c5 * c6);
								c4=Decimal.Round(c4,0);
							}
							else
							{
								c5=db.SelectAddersPrice("SStroke","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								if(PN.Mount.Substring(0,1) =="X")
								{
									c3=db.SelectAddersPrice("SWeight1","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								else if(PN.Mount.Substring(0,2) =="F1" || PN.Mount.Substring(0,2) =="F2")
								{
									c3=db.SelectAddersPrice("SWeight2","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								else if(PN.Mount.Substring(0,2) =="F5" || PN.Mount.Substring(0,2) =="F6")
								{
									c3=db.SelectAddersPrice("SWeight3","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								else if(PN.Mount.Substring(0,2) =="T1" || PN.Mount.Substring(0,2) =="T2")
								{
									c3=db.SelectAddersPrice("SWeight4","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								else if(PN.Mount.Substring(0,2) =="P1" || PN.Mount.Substring(0,2) =="P3")
								{
									c3=db.SelectAddersPrice("SWeight5","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								else if(PN.Mount.Substring(0,2) =="S1" || PN.Mount.Substring(0,2) =="S7")
								{
									c3=db.SelectAddersPrice("SWeight6","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								else if(PN.Mount.Substring(0,2) =="T4" )
								{
									c3=db.SelectAddersPrice("SWeight7","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								else if(PN.Mount.Substring(0,2) =="P4" )
								{
									c3=db.SelectAddersPrice("SWeight8","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								else if(PN.Mount.Substring(0,2) =="S2" )
								{
									c3=db.SelectAddersPrice("SWeight9","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								else if(PN.Mount.Substring(0,2) =="P2" )
								{
									c3=db.SelectAddersPrice("SWeight10","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								else
								{
									c3=db.SelectAddersPrice("DWeight","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								c6=Convert.ToDecimal(PN.Stroke.Trim());
								c4= c3 + (c5 * c6);
								c4=Decimal.Round(c4,0);
							}
						}
						if(c4 !=0)
						{
							weight="Approximate cylinder weight (does not include specials)= "+c4.ToString()+" LBS";
							//issue #233 start
							weight +=";The Cylinder Displacement = " + Cyl_Displacement(PN) + " in" + Convert.ToChar(0179).ToString();
							//issue #233 end
						}
						else
						{
							weight="";
						}
						Qitem.Weight=weight.ToString();
					}
					Qitem.Note="";
					strd= db.InsertItems(Qitem);
					if(strd.ToString().Trim() =="1")
					{
						if(lblstatus.Text.ToLower().Trim() !="true" && TxtSpecialReq.Text.ToString().Trim() ==""  )
						{
							lblgenerateprice.Text ="Your Quote is created !!";
							lblqno.Text=quot.QuoteNo.Trim();
							BtnGeneratePrice.Visible=false;
							if(TxtSpecialReq.Text.ToString().Trim() !="" || lblstatus.Text.ToLower().Trim() =="true")
							{ 
								quot.Finish="0";
							}
							else
							{
								quot.Finish="1";
							}
							string str =db.SelectCustomerQno(quot.QuoteNo.ToString().Trim());
							if(str.ToString() !="")
							{
								string s =db.UpdateCustomerQuote(quot,str.Trim());
							}
							else
							{
								string s =db.InsertCustomerQuote(quot);
							}
							lblstatus.Text="";
							Session["PartNo"]=null;
							Session["Accessories"]=null;
							LblInfo.Text="<script language='javascript'>window.open('print.aspx?id="+quot.QuoteNo.Trim()+"&type=icylinder','blank','toolbar=yes,width=800,height=800,resizable=yes,top=0,left=0')</script>";
							if(Qitem.S_Code.Trim() =="A" || Qitem.S_Code.Trim() =="ML" || Qitem.S_Code.Substring(0,1) =="P"  || Qitem.S_Code.Substring(0,1) =="N")
							{
								dwg.Text="<script language='javascript'>window.open('Drawings.aspx?qno="+quot.QuoteNo.Trim()+"&pno="+Qitem.PartNo.ToString().Trim()+"','_blank','toolbar=yes,width=1000,height=800,resizable=yes,top=0,left=0')</script>";
							}
						}
						else
						{
							PPriority.Visible=true;
							Session["Quote"] = quot;
							Session["Part"] =Qitem;
							if(TxtSpecialReq.Text.ToString().Trim() !="" ||  lblstatus.Text.ToLower().Trim() =="true")
							{ 
								quot.Finish="0";
								quot.Note ="This Quote require assitance from the factory.Please Contact Cowan Dynamics";
								if(TxtSpecialReq.Text.ToString().Trim() !="" )
								{
									Opts opts=new Opts();
									opts.Code=quot.QuoteNo.Trim();
									opts.Opt =TxtSpecialReq.Text.ToString().Trim();
									string temp=db.Insertcomments(opts);
								}
							}
							else
							{
								quot.Finish="1";
							}
							string str =db.SelectCustomerQno(quot.QuoteNo.ToString().Trim());
							if(str.ToString() !="")
							{
								string s =db.UpdateCustomerQuote(quot,str.Trim());
							}
							else
							{
								string s =db.InsertCustomerQuote(quot);
							}
							lblstatus.Text="";
							BtnGeneratePrice.Visible=false;
							
						}
					}
				}
				catch( Exception ex)
				{
					string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
					s.Replace("'"," ");
					LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
					lblgenerateprice.Text=ex.Message.ToString();
				}
				lblgenerateprice.Visible =true;
			}
			else
			{
				lblQty.Visible =true;
			}
		}
		private string GetDomain( string email )
		{
			int index = email.IndexOf( '@' );
			return email.Substring( index + 1 );
		}

		public string Qno(string use)
		{
			try
			{
				string qno ="";
				string count = "";
				string usr=use.ToUpper();
				string st= usr.Substring(0,1);
				string s1=DateTime.Today.Month.ToString();
				count=db.SelectLastQno();
				if(count.Length !=0)
				{
					int lst=Convert.ToInt32(count.Substring(5));
				
				
					string s=DateTime.Today.Month.ToString();
					if(s.Length ==1)
					{
						s="0"+DateTime.Today.Month.ToString();
					}
					else
					{
						s=DateTime.Today.Month.ToString();
					}
					if(count !="")
					{
						if(count.Substring(1,2).Equals(DateTime.Today.Year.ToString().Substring(2)))
						{
							if(count.Substring(3,2).Equals(s))
							{
								lst++;
								string num="";
								if(lst.ToString().Length ==4)
								{
									num=lst.ToString();
								}
								else if(lst.ToString().Length ==3)
								{
									num="0"+lst.ToString();
								}
								else if(lst.ToString().Length ==2)
								{
									num="00"+lst.ToString();
								}
								else 
								{
									num="000"+lst.ToString();
								}
							
								if(s1.Length ==1)
								{
									s1="0"+DateTime.Today.Month.ToString();
								}
								else
								{
									s1=DateTime.Today.Month.ToString();
								}
								qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+num.ToString();
							}
							else
							{
						
								if(s1.Length ==1)
								{
									s1="0"+DateTime.Today.Month.ToString();
								}
								else
								{
									s1=DateTime.Today.Month.ToString();
								}
								qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+"0001";
							}
	
						}
						else
						{
						
							if(s1.Length ==1)
							{
								s1="0"+DateTime.Today.Month.ToString();
							}
							else
							{
								s1=DateTime.Today.Month.ToString();
							}
							qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+"0001";
						}
					}
					else
					{
					
						if(s1.Length ==1)
						{
							s1="0"+DateTime.Today.Month.ToString();
						}
						else
						{
							s1=DateTime.Today.Month.ToString();
						}
						qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+"0001";
					}
				}
				else
				{
					qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+"0001";
				}
				string sav=db.InsertQuoteNo(qno);
				db.InsertQuoteCount(qno);
				return qno;
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				return null;
			}
		
		}

		private void CBPort_CheckedChanged(object sender, System.EventArgs e)
		{
			try
			{
				if(CBPort.Checked == false)
				{
					PanelPort.Visible=false;
				}
				else if(CBPort.Checked == true)
				{
					PanelPort.Visible=true;

					ArrayList list8 = new ArrayList();
					list8=db.SelectPortSize(lbl.Text.ToString().Trim(),DDLPorttype.SelectedIndex.ToString());
					DDLPortSize.Items.Clear();
					DDLPortSize.Items.Add("Select Port Size");
					for (int i=0;i<= list8.Count -1;i++)
					{
						DDLPortSize.Items.Add(list8[i].ToString());
					}
				 
					ArrayList list9 = new ArrayList();
					list9=db.SelectAirBleeds(lbl.Text.ToString().Trim());
					DDLAirbleeds.Items.Clear();
					DDLAirbleeds.Items.Add("None");
					for (int i=0;i<= list9.Count -1;i++)
					{
						DDLAirbleeds.Items.Add(list9[i].ToString());
					}
				
				}
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}
		public void MLPricing()
		{
			try
			{
				lblmgrp.Text="MLSMount_Grp";
				lbltable.Text="WEB_MLPrice_Master_TableV1";

				PartNumber PN = (PartNumber)Session["PartNo"];
				if(PN.Application_Opt.Count >0 || PN.Popular_Opt.Count >0)
				{
					for(int p =0;p<PN.Application_Opt.Count;p++)
					{
						if(PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="D" || PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="R" )
						{
							lblmgrp.Text="MLDMount_Grp";
							lbltable.Text="WEB_MLDPrice_Master_TableV1";
						
						}
						
					}
					for(int p =0;p<PN.Popular_Opt.Count;p++)
					{
						if(PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="AD" || PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="WD" )
						{
							lblmgrp.Text="MLDMount_Grp";
							lbltable.Text="WEB_MLDPrice_Master_TableV1";
						}
					}
				}
				else
				{
					lblmgrp.Text="MLSMount_Grp";
					lbltable.Text="WEB_MLPrice_Master_TableV1";
				}
				//stroke price
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal cprice =0.00m;
				decimal seal=0.00m;
				decimal rodend=0.00m;

				st =Convert.ToDecimal(PN.Stroke);
				sp =db.SelectStrokePriceML(lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				sp =Decimal.Round(sp,2);
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(db.SelectPriceIndex("ML").ToString());
				//issue #680 start
				if((PN.Bore_Size.ToString().Trim() =="C" || PN.Bore_Size.ToString().Trim() =="D" || PN.Bore_Size.ToString().Trim() =="E"))
				{
					index =Convert.ToDecimal(db.SelectPriceIndex("MLS").ToString());
				}
				//issue #680 end
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100)); 
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text =sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
				//mount price
				string mgrp ="";
				decimal mp =0.00m;
				try
				{
					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
					mp =db.SelectMountPriceML(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}
				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100)); 
				mprice = Decimal.Round(mp,2);
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}
				// cushin price
				decimal cp =0.00m;
				try
				{
					cp =db.SelectCushionPriceML(lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}
				cp =cp + cp * (index /100);
				
				
				if(PN.Cushions.ToString() =="5")
				{
					decimal temp1 =cp * 2;
					temp1 = temp1*(1 - (discount/100));
					cprice =Decimal.Round(temp1,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
					
				}
				else if(PN.Cushions.ToString() =="8")
				{
					cprice =0.00m;
					cprice =Decimal.Round(cprice,2);
				}
				else
				{
					cprice=cp;
					cprice = cprice*(1 - (discount/100)); 
					cprice =Decimal.Round(cprice,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
			
				cprice =Decimal.Round(cprice,2);
				
				if(PN.Seal_Comp.ToString().Trim() =="L")
				{
					seal =db.SelectAddersPrice("SL","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				
				}
				else if(PN.Seal_Comp.ToString().Trim() =="F")
				{
					seal =db.SelectAddersPrice("SF","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim()=="W")
				{
					seal =db.SelectAddersPrice("SW","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="E")
				{
					seal =db.SelectAddersPrice("SE","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				seal = Decimal.Round(seal,2);
				seal =seal + seal * (index /100);
				if(PN.Rod_End.Trim().Substring(0,1) !="N" && PN.Rod_End.Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="True";
					}
				}
				rodend =rodend + rodend * (index /100);
				decimal total=0.00m;
				total=sprice + mprice +cprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
				
		}
		public void PAPricing()
		{
			try
			{
				PartNumber PN = (PartNumber)Session["PartNo"];
				if(PN.Application_Opt.Count >0 || PN.Popular_Opt.Count >0)
				{
					for(int p =0;p<PN.Application_Opt.Count;p++)
					{
						if(PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="D" || PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="R" )
						{
							char[] asst ;
							asst = PN.Bore_Size.ToString().ToCharArray(0,1);
							if( (int)asst[0] ==78)
							{
								lblmgrp.Text="PAD8MountSMC_Grp";
								lbltable.Text="WEB_PAPriceMaster_D8_TableV1";
							}
							else
							{
								lblmgrp.Text="PADMountSMC_Grp";
								lbltable.Text="WEB_PAPriceMaster_D_TableV1";
							}
						}
					}
					for(int p =0;p<PN.Popular_Opt.Count;p++)
					{
						if(PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="AD" || PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="WD" )
						{
							char[] asst ;
							asst = asst = PN.Bore_Size.ToString().ToCharArray(0,1);
							if( (int)asst[0] ==78)
							{
								lblmgrp.Text="PAD8MountSMC_Grp";
								lbltable.Text="WEB_PAPriceMaster_D8_TableV1";
							}
							else
							{
								lblmgrp.Text="PADMountSMC_Grp";
								lbltable.Text="WEB_PAPriceMaster_D_TableV1";
							}							
						}
					}
				}
				else
				{
					char[] asst ;
					asst = PN.Bore_Size.ToString().ToCharArray(0,1);
					if( (int)asst[0] ==78)
					{
						lblmgrp.Text="PAS8MountSMC_Grp";
						lbltable.Text="WEB_PAPriceMaster_SP_TableV1";
					}
					else
					{
						lblmgrp.Text="PASMountSMC_Grp";
						lbltable.Text="WEB_PAPriceMaster_TableV1";
					}
				}
				//stroke price
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal cprice =0.00m;
				decimal seal=0.00m;
				decimal rodend=0.00m;

				st =Convert.ToDecimal(PN.Stroke);
				try
				{
					sp =db.SelectStrokeSMCPricePA(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}
				sp =Decimal.Round(sp,2);
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(db.SelectPriceIndex("PA").ToString());
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100)); 
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text =sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
				//mount price
				decimal mp =0.00m;
				string mgrp ="";
				try
				{ 
					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
					mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}

				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100)); 
				
				mprice= Decimal.Round(mp,2);
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}

				// cushin price
				string col ="";
				if(PN.Cushions.ToString().Trim() == "2" || PN.Cushions.ToString().Trim() == "3" || PN.Cushions.ToString().Trim() == "4")
				{
					col="PACushion_Per_InchN";
				} 
				else
				{
					col="PACushion_Per_InchA";
				}
				decimal cp =0.00m;
				cp =db.SelectCushionPrice(col.ToString(), lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				cp =cp + cp * (index /100);
				
				if(PN.Cushions.ToString().Trim() =="5" || PN.Cushions.ToString() =="2")
				{
					decimal temp1 =cp * 2;
					temp1 = temp1*(1 - (discount/100)); 
					cprice =Decimal.Round(temp1,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Cushions.ToString().Trim() =="8")
				{
					cprice =0.00m;
					cprice =Decimal.Round(cprice,2);
				}
				else
				{
					cprice=cp;
					cprice = cprice*(1 - (discount/100)); 
					cprice =Decimal.Round(cprice,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				cprice =Decimal.Round(cprice,2);

				
				if(PN.Seal_Comp.ToString().Trim() =="L")
				{
					seal =db.SelectAddersPrice("SL","WEB_SeriesPACommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim()  =="F")
				{
					seal =db.SelectAddersPrice("SF","WEB_SeriesPACommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				seal =Decimal.Round(seal,2);
				seal =seal + seal * (index /100);

				if(PN.Rod_End.ToString().Trim().Substring(0,1) !="N" && PN.Rod_End.ToString().Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd","WEB_SeriesPACommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="True";
					}
				}
				rodend =Decimal.Round(rodend,2);
				rodend =rodend + rodend * (index /100);
				decimal total=0.00m;
				total=sprice + mprice +cprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}
		//issue #644 start
		public void PAPricing_SMC()
		{
			try
			{
				PartNumber PN = (PartNumber)Session["PartNo"];
				if(PN.Application_Opt.Count >0 || PN.Popular_Opt.Count >0)
				{
					for(int p =0;p<PN.Application_Opt.Count;p++)
					{
						if(PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="D" || PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="R" )
						{
							char[] asst ;
							asst = PN.Bore_Size.ToString().ToCharArray(0,1);
							if( (int)asst[0] ==78)
							{
								lblmgrp.Text="PAD8MountSMC_Grp";
								lbltable.Text="PAPriceSMC_D8_Table";
							}
							else
							{
								lblmgrp.Text="PADMountSMC_Grp";
								lbltable.Text="PAPriceSMC_D_Table";
							}
						}
					}
					for(int p =0;p<PN.Popular_Opt.Count;p++)
					{
						if(PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="AD" || PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="WD" )
						{
							char[] asst ;
							asst = asst = PN.Bore_Size.ToString().ToCharArray(0,1);
							if( (int)asst[0] ==78)
							{
								lblmgrp.Text="PAD8MountSMC_Grp";
								lbltable.Text="PAPriceSMC_D8_Table";
							}
							else
							{
								lblmgrp.Text="PADMountSMC_Grp";
								lbltable.Text="PAPriceSMC_D_Table";
							}							
						}
					}
				}
				else
				{
					char[] asst ;
					asst = PN.Bore_Size.ToString().ToCharArray(0,1);
					if( (int)asst[0] ==78)
					{
						lblmgrp.Text="PAS8MountSMC_Grp";
						lbltable.Text="PAPriceSMC_SP_Table";
					}
					else
					{
						lblmgrp.Text="PASMountSMC_Grp";
						lbltable.Text="PAPriceSMC_Table";
					}
				}
				//stroke price
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal cprice =0.00m;
				decimal seal=0.00m;
				decimal rodend=0.00m;

				st =Convert.ToDecimal(PN.Stroke);
				try
				{
					sp =db.SelectStrokeSMCPricePA(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}
				sp =Decimal.Round(sp,2);
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(db.SelectPriceIndex_SMC("PA").ToString());
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100)); 
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text =sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
				//mount price
				decimal mp =0.00m;
				string mgrp ="";
				try
				{ 
					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
					mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}

				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100)); 
				
				mprice= Decimal.Round(mp,2);
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}

				// cushin price
				string col ="";
				if(PN.Cushions.ToString().Trim() == "2" || PN.Cushions.ToString().Trim() == "3" || PN.Cushions.ToString().Trim() == "4")
				{
					col="PACushion_Per_InchN";
				} 
				else
				{
					col="PACushion_Per_InchA";
				}
				decimal cp =0.00m;
				cp =db.SelectCushionPrice(col.ToString(), lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				cp =cp + cp * (index /100);
				
				if(PN.Cushions.ToString().Trim() =="5" || PN.Cushions.ToString() =="2")
				{
					decimal temp1 =cp * 2;
					temp1 = temp1*(1 - (discount/100)); 
					cprice =Decimal.Round(temp1,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Cushions.ToString().Trim() =="8")
				{
					cprice =0.00m;
					cprice =Decimal.Round(cprice,2);
				}
				else
				{
					cprice=cp;
					cprice = cprice*(1 - (discount/100)); 
					cprice =Decimal.Round(cprice,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				cprice =Decimal.Round(cprice,2);

				
				if(PN.Seal_Comp.ToString().Trim() =="L")
				{
					//seal =db.SelectAddersPrice("SL","SMCSeriesPACommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					seal =db.SelectAddersPrice("L","SMCSeriesPACommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim()  =="F")
				{
					//seal =db.SelectAddersPrice("SF","SMCSeriesPACommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					seal =db.SelectAddersPrice("F","SMCSeriesPACommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				seal =Decimal.Round(seal,2);
				seal =seal + seal * (index /100);

				if(PN.Rod_End.ToString().Trim().Substring(0,1) !="N" && PN.Rod_End.ToString().Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd","SMCSeriesPACommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="True";
					}
				}
				rodend =Decimal.Round(rodend,2);
				rodend =rodend + rodend * (index /100);
				decimal total=0.00m;
				total=sprice + mprice +cprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}
		//issue #644 end
		public void PSPricing()
		{
			try
			{
				lblmgrp.Text="PSSMountSMC_Grp";
				lbltable.Text="WEB_PSPriceMaster_TableV1";

				PartNumber PN = (PartNumber)Session["PartNo"];
				if(PN.Application_Opt.Count >0 || PN.Popular_Opt.Count >0)
				{
					for(int p =0;p<PN.Application_Opt.Count;p++)
					{
						if(PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="D" || PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="R" )
						{
							char[] asst ;
							asst = PN.Bore_Size.ToString().ToCharArray(0,1);
							if( (int)asst[0] >= 78)
							{
								lblmgrp.Text="PSDMountSMC_GrpSp";
								lbltable.Text="WEB_PSPriceMaster_DSP_TableV1";
							}
							else
							{
								lblmgrp.Text="PSDMountSMC_Grp";
								lbltable.Text="WEB_PSPriceMaster_D_TableV1";
							}
						}
					
					}
					for(int p =0;p<PN.Popular_Opt.Count;p++)
					{
						if(PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="AD" || PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="WD" )
						{
							char[] asst ;
							asst = asst = PN.Bore_Size.ToString().ToCharArray(0,1);
							if( (int)asst[0] >= 78)
							{
								lblmgrp.Text="PSDMountSMC_GrpSp";
								lbltable.Text="WEB_PSPriceMaster_DSP_TableV1";
							}
							else
							{
								lblmgrp.Text="PSDMountSMC_Grp";
								lbltable.Text="WEB_PSPriceMaster_D_TableV1";
							}
						}
					}
				}
				else
				{
					char[] asst ;
					asst = asst = PN.Bore_Size.ToString().ToCharArray(0,1);
					if( (int)asst[0] >=78)
					{
						lblmgrp.Text="PSSMountSMC_GrpSp";
						lbltable.Text="WEB_PSPriceMaster_SP_TableV1";
					}
					else
					{
						lblmgrp.Text="PSSMountSMC_Grp";
						lbltable.Text="WEB_PSPriceMaster_TableV1";
					}
					
				}
				if(PN.Mount.ToString().Trim() =="MT1+" )
				{
					lblmgrp.Text="PSSMountMT1_Grp";
					lbltable.Text="WEB_PSPriceSMC_MT1_TableV1";
				}
				
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal cprice =0.00m;	
				decimal seal=0.00m;

				st =Convert.ToDecimal(PN.Stroke);
				sp =db.SelectStrokeSMCPricePS(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				sp =Decimal.Round(sp,2);
				
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(db.SelectPriceIndex("PS").ToString());
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100));
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text =sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
				//mount price
				decimal mp =0.00m;
				string mgrp ="";
				try
				{
					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
					mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}
				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100));
				
				mprice=Decimal.Round(mp,2);
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}

				// cushin price
				string col ="";
				if(PN.Cushions.Trim() == "2" || PN.Cushions.Trim() == "3" || PN.Cushions.Trim() == "4")
				{
					col="CushionPerInchN";
				} 
				else
				{
					col="CushionPerInchA";
				}
				decimal cp =0.00m;
				cp =db.SelectCushionPrice(col.ToString(), lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				cp =cp + cp * (index /100);
				
				if(PN.Cushions.Trim() =="5" || PN.Cushions.Trim() =="2")
				{
					decimal temp1 =cp * 2;
					temp1 = temp1*(1 - (discount/100));
					cprice =Decimal.Round(temp1,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Cushions.Trim() =="8")
				{
					cprice =0.00m;
					cprice =Decimal.Round(cprice,2);
				}
				else
				{
					cprice=cp;
					cprice = cprice*(1 - (discount/100)); 
					cprice =Decimal.Round(cprice,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				cprice =Decimal.Round(cprice,2);
				
				if(PN.Seal_Comp.Trim()  =="L")
				{
					seal =db.SelectAddersPrice("SL","SWEB_eriesPSCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.Trim()  =="F")
				{
					seal =db.SelectAddersPrice("SF","WEB_SeriesPSCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				seal =Decimal.Round(seal,2);
				seal =seal + seal * (index /100);
				decimal rodend=0.00m;
				if(PN.Rod_End.Trim().Substring(0,1) !="N" && PN.Rod_End.Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd","WEB_SeriesPSCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="True";
					}
				}
				rodend =Decimal.Round(rodend,2);
				rodend =rodend + rodend * (index /100);
				decimal total=0.00m;
				total=sprice + mprice +cprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}
		//issue #644 start
		public void PSPricing_SMC()
		{
			try
			{
				lblmgrp.Text="PSSMountSMC_Grp";
				lbltable.Text="PSPriceSMC_Table";

				PartNumber PN = (PartNumber)Session["PartNo"];
				if(PN.Application_Opt.Count >0 || PN.Popular_Opt.Count >0)
				{
					for(int p =0;p<PN.Application_Opt.Count;p++)
					{
						if(PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="D" || PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="R" )
						{
							char[] asst ;
							asst = PN.Bore_Size.ToString().ToCharArray(0,1);
							if( (int)asst[0] >= 78)
							{
								lblmgrp.Text="PSDMountSMC_GrpSp";
								lbltable.Text="PSPriceSMC_DSP_Table";
							}
							else
							{
								lblmgrp.Text="PSDMountSMC_Grp";
								lbltable.Text="PSPriceSMC_D_Table";
							}
						}
					
					}
					for(int p =0;p<PN.Popular_Opt.Count;p++)
					{
						if(PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="AD" || PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="WD" )
						{
							char[] asst ;
							asst = asst = PN.Bore_Size.ToString().ToCharArray(0,1);
							if( (int)asst[0] >= 78)
							{
								lblmgrp.Text="PSDMountSMC_GrpSp";
								lbltable.Text="PSPriceSMC_DSP_Table";
							}
							else
							{
								lblmgrp.Text="PSDMountSMC_Grp";
								lbltable.Text="PSPriceSMC_D_Table";
							}
						}
					}
				}
				else
				{
					char[] asst ;
					asst = asst = PN.Bore_Size.ToString().ToCharArray(0,1);
					if( (int)asst[0] >=78)
					{
						lblmgrp.Text="PSSMountSMC_GrpSp";
						lbltable.Text="PSPriceSMC_SP_Table";
					}
					else
					{
						lblmgrp.Text="PSSMountSMC_Grp";
						lbltable.Text="PSPriceSMC_Table";
					}
					
				}
				if(PN.Mount.ToString().Trim() =="MT1+" )
				{
					lblmgrp.Text="PSSMountMT1_Grp";
					lbltable.Text="PSPriceSMC_MT1_Table";
				}
				
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal cprice =0.00m;	
				decimal seal=0.00m;

				st =Convert.ToDecimal(PN.Stroke);
				sp =db.SelectStrokeSMCPricePS(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				sp =Decimal.Round(sp,2);
				
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(db.SelectPriceIndex_SMC("PS").ToString());
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100));
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text =sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
				//mount price
				decimal mp =0.00m;
				string mgrp ="";
				try
				{
					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
					mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}
				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100));
				
				mprice=Decimal.Round(mp,2);
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}

				// cushin price
				string col ="";
				if(PN.Cushions.Trim() == "2" || PN.Cushions.Trim() == "3" || PN.Cushions.Trim() == "4")
				{
					col="CushionPerInchN";
				} 
				else
				{
					col="CushionPerInchA";
				}
				decimal cp =0.00m;
				cp =db.SelectCushionPrice(col.ToString(), lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				cp =cp + cp * (index /100);
				
				if(PN.Cushions.Trim() =="5" || PN.Cushions.Trim() =="2")
				{
					decimal temp1 =cp * 2;
					temp1 = temp1*(1 - (discount/100));
					cprice =Decimal.Round(temp1,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Cushions.Trim() =="8")
				{
					cprice =0.00m;
					cprice =Decimal.Round(cprice,2);
				}
				else
				{
					cprice=cp;
					cprice = cprice*(1 - (discount/100)); 
					cprice =Decimal.Round(cprice,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				cprice =Decimal.Round(cprice,2);
				
				if(PN.Seal_Comp.Trim()  =="L")
				{
					//seal =db.SelectAddersPrice("SL","SMCSeriesPSCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					seal =db.SelectAddersPrice("L","SMCSeriesPSCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.Trim()  =="F")
				{
					//seal =db.SelectAddersPrice("SF","SMCSeriesPSCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					seal =db.SelectAddersPrice("F","SMCSeriesPSCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				seal =Decimal.Round(seal,2);
				seal =seal + seal * (index /100);
				decimal rodend=0.00m;
				if(PN.Rod_End.Trim().Substring(0,1) !="N" && PN.Rod_End.Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd","SMCSeriesPSCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="True";
					}
				}
				rodend =Decimal.Round(rodend,2);
				rodend =rodend + rodend * (index /100);
				decimal total=0.00m;
				total=sprice + mprice +cprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}
		//issue #644 end

		public void PCPricing()
		{
			try
			{
				PartNumber PN = (PartNumber)Session["PartNo"];
				if(PN.Application_Opt.Count >0 || PN.Popular_Opt.Count >0)
				{
					for(int p =0;p<PN.Application_Opt.Count;p++)
					{
						if(PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="D" || PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="R" )
						{
							lblstatus.Text ="True";
							return;
						}
				
					}
					for(int p =0;p<PN.Popular_Opt.Count;p++)
					{
						if(PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="AD" || PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="WD" )
						{
							lblstatus.Text ="True";
							return;
						}
					}
				}
				lblmgrp.Text="PCMount_Grp";
				lbltable.Text="WEB_PCPriceSMC_Table";
				//stroke price
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal cprice =0.00m;
				st =Convert.ToDecimal(PN.Stroke);
				try
				{
					sp =db.SelectStrokePriceL(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}

				sp =Decimal.Round(sp,2);
				
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(db.SelectPriceIndex("PC").ToString());
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100)); 
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text=sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
				//mount price
				decimal mp =0.00m;
				string mgrp ="";
				try
				{ 
					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
					mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}

				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100)); 
				
				mprice=Decimal.Round(mp,2);
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}
				// cushin price
				string col ="";
				if(PN.Cushions.Trim() == "2" || PN.Cushions.Trim() == "3" || PN.Cushions.Trim() == "4")
				{
					col="CushionPerInchN";
				} 
				else
				{
					col="CushionPerInchA";
				}
				decimal cp =0.00m;
				cp =db.SelectCushionPrice(col.ToString(), lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				cp =cp + cp * (index /100);
				
				if(PN.Cushions.Trim() =="5" || PN.Cushions.Trim() =="2")
				{
					decimal temp1 =cp * 2;
					temp1 = temp1*(1 - (discount/100)); 
					cprice =Decimal.Round(temp1,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Cushions.Trim() =="8")
				{
					cprice =0.00m;
					cprice =Decimal.Round(cprice,2);
				}
				else
				{
					cprice=cp;
					cprice = cprice*(1 - (discount/100)); 
					cprice =Decimal.Round(cprice,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				cprice =Decimal.Round(cprice,2);
				decimal seal=0.00m;
				if(PN.Seal_Comp.Trim() =="L")
				{
					seal =db.SelectAddersPrice("SL","WEB_SeriesPCCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.Trim() =="F")
				{
					seal =db.SelectAddersPrice("SF","WEB_SeriesPCCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				
				}
				seal =Decimal.Round(seal,2);
				seal =seal + seal * (index /100);
				decimal rodend=0.00m;
				if(PN.Rod_End.Trim().Substring(0,1) !="N" && PN.Rod_End.Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd","WEB_SeriesPCCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="True";
					}
				}
				rodend =Decimal.Round(rodend,2);
				rodend =rodend + rodend * (index /100);
				decimal total=0.00m;
				total=sprice + mprice +cprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		
		}
		//issue #644 start
		public void PCPricing_SMC()
		{
			try
			{
				PartNumber PN = (PartNumber)Session["PartNo"];
				if(PN.Application_Opt.Count >0 || PN.Popular_Opt.Count >0)
				{
					for(int p =0;p<PN.Application_Opt.Count;p++)
					{
						if(PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="D" || PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="R" )
						{
							lblstatus.Text ="True";
							return;
						}
				
					}
					for(int p =0;p<PN.Popular_Opt.Count;p++)
					{
						if(PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="AD" || PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="WD" )
						{
							lblstatus.Text ="True";
							return;
						}
					}
				}
				lblmgrp.Text="PCMount_Grp";
				//issue #644 update
				lbltable.Text="PCPriceSMC_Table";
				//stroke price
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal cprice =0.00m;
				st =Convert.ToDecimal(PN.Stroke);
				try
				{
					sp =db.SelectStrokePriceL(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}

				sp =Decimal.Round(sp,2);
				
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(db.SelectPriceIndex_SMC("PC").ToString());
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100)); 
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text=sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
				//mount price
				decimal mp =0.00m;
				string mgrp ="";
				try
				{ 
					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
					mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}

				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100)); 
				
				mprice=Decimal.Round(mp,2);
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}
				// cushin price
				string col ="";
				if(PN.Cushions.Trim() == "2" || PN.Cushions.Trim() == "3" || PN.Cushions.Trim() == "4")
				{
					col="CushionPerInchN";
				} 
				else
				{
					col="CushionPerInchA";
				}
				decimal cp =0.00m;
				cp =db.SelectCushionPrice(col.ToString(), lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				cp =cp + cp * (index /100);
				
				if(PN.Cushions.Trim() =="5" || PN.Cushions.Trim() =="2")
				{
					decimal temp1 =cp * 2;
					temp1 = temp1*(1 - (discount/100)); 
					cprice =Decimal.Round(temp1,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Cushions.Trim() =="8")
				{
					cprice =0.00m;
					cprice =Decimal.Round(cprice,2);
				}
				else
				{
					cprice=cp;
					cprice = cprice*(1 - (discount/100)); 
					cprice =Decimal.Round(cprice,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				cprice =Decimal.Round(cprice,2);
				decimal seal=0.00m;
				if(PN.Seal_Comp.Trim() =="L")
				{
					//seal =db.SelectAddersPrice("SL","SMCSeriesPCCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					seal =db.SelectAddersPrice("L","SMCSeriesPCCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.Trim() =="F")
				{
					//seal =db.SelectAddersPrice("SF","SMCSeriesPCCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					seal =db.SelectAddersPrice("F","SMCSeriesPCCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				
				}
				seal =Decimal.Round(seal,2);
				seal =seal + seal * (index /100);
				decimal rodend=0.00m;
				if(PN.Rod_End.Trim().Substring(0,1) !="N" && PN.Rod_End.Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd","SMCSeriesPCCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="True";
					}
				}
				rodend =Decimal.Round(rodend,2);
				rodend =rodend + rodend * (index /100);
				decimal total=0.00m;
				total=sprice + mprice +cprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		
		}
		//issue #644 end
		public void MPricing()
		{
			try
			{
				lblmgrp.Text="MLSMount_Grp";
				lbltable.Text="WEB_MLPrice_Master_TableV1";
				PartNumber PN = (PartNumber)Session["PartNo"];
				if(PN.Application_Opt.Count >0 || PN.Popular_Opt.Count >0)
				{
					for(int p =0;p<PN.Application_Opt.Count;p++)
					{
						if(PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="D" || PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="R" )
						{
							lblmgrp.Text="MLDMount_Grp";
							lbltable.Text="WEB_MLDPrice_Master_TableV1";
						
						}
						
					}
					for(int p =0;p<PN.Popular_Opt.Count;p++)
					{
						if(PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="AD" || PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="WD" )
						{
							lblmgrp.Text="MLDMount_Grp";
							lbltable.Text="WEB_MLDPrice_Master_TableV1";
						}
					}
				}
				else
				{
					lblmgrp.Text="MLSMount_Grp";
					lbltable.Text="WEB_MLPrice_Master_TableV1";
				}
				//stroke price
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal cprice =0.00m;
				decimal seal=0.00m;
				decimal rodend=0.00m;

				st =Convert.ToDecimal(PN.Stroke);
				sp =db.SelectStrokePriceML(lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				sp =Decimal.Round(sp,2);
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(db.SelectPriceIndex("ML").ToString());
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100)); 
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text =sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
				//mount price
				string mgrp ="";
				decimal mp =0.00m;
				try
				{
					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
					mp =db.SelectMountPriceML(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}
				decimal mlm=4.00m;
				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100)); 
				mp=mp*(1 - (mlm/100)); 
				mprice = Decimal.Round(mp,2);
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}
				// cushin price
				decimal cp =0.00m;
				try
				{
					cp =db.SelectCushionPriceML(lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}
				cp =cp + cp * (index /100);
				
				
				if(PN.Cushions.ToString() =="5")
				{
					decimal temp1 =cp * 2;
					temp1 = temp1*(1 - (discount/100));
					cprice =Decimal.Round(temp1,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
					
				}
				else if(PN.Cushions.ToString() =="8")
				{
					cprice =0.00m;
					cprice =Decimal.Round(cprice,2);
				}
				else
				{
					cprice=cp;
					cprice = cprice*(1 - (discount/100)); 
					cprice =Decimal.Round(cprice,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
			
				cprice =Decimal.Round(cprice,2);
				
				if(PN.Seal_Comp.ToString().Trim() =="L")
				{
					seal =db.SelectAddersPrice("SL","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				
				}
				else if(PN.Seal_Comp.ToString().Trim() =="F")
				{
					seal =db.SelectAddersPrice("SF","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim()=="W")
				{
					seal =db.SelectAddersPrice("SW","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="E")
				{
					seal =db.SelectAddersPrice("SE","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				seal = Decimal.Round(seal,2);
				seal =seal + seal * (index /100);
				if(PN.Rod_End.Trim().Substring(0,1) !="N" && PN.Rod_End.Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="True";
					}
				}
				rodend =rodend + rodend * (index /100);
				decimal total=0.00m;
				total=sprice + mprice +cprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
				
		}
		public void NPricing()
		{
			try
			{
				PartNumber PN = (PartNumber)Session["PartNo"];
				if(PN.Application_Opt.Count >0 || PN.Popular_Opt.Count >0)
				{
					for(int p =0;p<PN.Application_Opt.Count;p++)
					{
						if(PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="D" || PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="R" )
						{
							lblstatus.Text ="True";
							return;
						}
				
					}
					for(int p =0;p<PN.Popular_Opt.Count;p++)
					{
						if(PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="AD" || PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="WD" )
						{
							lblstatus.Text ="True";
							return;
						}
					}
				}
				lblmgrp.Text="NMount_Grp";
				lbltable.Text="WEB_NPrice_Master_TableV1";
		
				//stroke price
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal cprice =0.00m;
				decimal seal=0.00m;
				st =Convert.ToDecimal(PN.Stroke);
				sp =db.SelectStrokePriceL(lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				sp =Decimal.Round(sp,2);
				
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(db.SelectPriceIndex("N").ToString());
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100)); 
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text=sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
				//mount price
				string mgrp ="";
				decimal mp =0.00m;
				try
				{
					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
					if(mgrp.ToString() !="" || mgrp.ToString() !=null)
					{
						mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					}
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}
				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100)); 
				
				mprice=Decimal.Round(mp,2);
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}
				// cushin price
				decimal cp =0.00m;
				try
				{
					cp =db.SelectCushionPrice("CushionPerEnd",lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}
				cp =cp + cp * (index /100);
				
				if(PN.Cushions.ToString().Trim() =="5")
				{
					decimal temp1 =cp * 2;
					temp1 = temp1*(1 - (discount/100));
					cprice =Decimal.Round(temp1,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Cushions.ToString().Trim() =="8")
				{
					cprice =0.00m;
					cprice =Decimal.Round(cprice,2);
				}
				else
				{
					cprice=cp;
					cprice = cprice*(1 - (discount/100)); 
					cprice =Decimal.Round(cprice,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				cprice =Decimal.Round(cprice,2);
				
				if(PN.Seal_Comp.ToString().Trim() =="L")
				{
					seal =db.SelectAddersPrice("SL","WEB_SeriesNCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="F")
				{
					seal =db.SelectAddersPrice("SF","WEB_SeriesNCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="W")
				{
					seal =db.SelectAddersPrice("SW","WEB_SeriesNCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="E")
				{
					seal =db.SelectAddersPrice("SE","WEB_SeriesNCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				seal =Decimal.Round(seal,2);
				seal =seal + seal * (index /100);
				decimal rodend=0.00m;
				if(PN.Rod_End.ToString().Trim().Substring(0,1) !="N" && PN.Rod_End.ToString().Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd","WEB_SeriesNCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="True";
					}
				}
				rodend =Decimal.Round(rodend,2);
				rodend =rodend + rodend * (index /100);
				decimal total=0.00m;
				total=sprice + mprice +cprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
				
		}
		public void LPricing()
		{
			try
			{
				PartNumber PN = (PartNumber)Session["PartNo"];
				if(PN.Application_Opt.Count >0 || PN.Popular_Opt.Count >0)
				{
					for(int p =0;p<PN.Application_Opt.Count;p++)
					{
						if(PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="D" || PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="R" )
						{
							lblstatus.Text ="True";
							return;
						}
				
					}
					for(int p =0;p<PN.Popular_Opt.Count;p++)
					{
						if(PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="AD" || PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="WD" )
						{
							lblstatus.Text ="True";
							return;
						}
					}
				}
				lblmgrp.Text="LMount_Grp";
				lbltable.Text="WEB_LPrice_Master_TableV1";
				//stroke price
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;

				st =Convert.ToDecimal(PN.Stroke);
				sp =db.SelectStrokeLprice(lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				sp =Decimal.Round(sp,2);
				
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(db.SelectPriceIndex("L").ToString());
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100)); 
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text=sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
			
				//mount price
				string mgrp ="";
				decimal mp =0.00m;
				try
				{
					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
					if(mgrp.ToString() !="" || mgrp.ToString() !=null)
					{
						mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					}
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}
				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100)); 
				
				mprice=Decimal.Round(mp,2);
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}
		
				decimal total=0.00m;
				total=sprice + mprice ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}
		public void MHPricing()
		{}
		public void RPricing()
		{}
		public void APricing()
		{
			try
			{
				lblmgrp.Text="AP_Base";
				lbltable.Text="WEB_APrice_Master_TableV1";
				PartNumber PN = (PartNumber)Session["PartNo"];
				if(PN.Application_Opt.Count >0 || PN.Popular_Opt.Count >0)
				{
					for(int p =0;p<PN.Application_Opt.Count;p++)
					{
						if(PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="D" || PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="R" )
						{
							lblmgrp.Text="AP_Base";
							lbltable.Text="WEB_ADPrice_Master_TableV1";
						}
				
					}
					for(int p =0;p<PN.Popular_Opt.Count;p++)
					{
						if(PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="AD" || PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="WD" )
						{
							lblmgrp.Text="AP_Base";
							lbltable.Text="WEB_ADPrice_Master_TableV1";
						}
					}
				}
				else
				{
					lblmgrp.Text="AP_Base";
					lbltable.Text="WEB_APrice_Master_TableV1";
				}
								
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal seal=0.00m;

				st =Convert.ToDecimal(PN.Stroke);
				sp =db.SelectStrokeA(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				sp =Decimal.Round(sp,2);
				
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(db.SelectPriceIndex("A").ToString());
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100));
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text=sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
				//mount price
				decimal mp =0.00m;
				string mgrp = lblmgrp.Text.ToString();
				mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100)); 
				
				mprice=Decimal.Round(mp,2);
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}

				if(PN.Seal_Comp.ToString().Trim() =="L")
				{
					seal =db.SelectAddersPrice("SL","WEB_SeriesACommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="F")
				{
					seal =db.SelectAddersPrice("SF","WEB_SeriesACommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="E")
				{
					seal =db.SelectAddersPrice("SE","WEB_SeriesACommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				seal=Decimal.Round(seal,2);
				seal =seal + seal * (index /100);
				decimal rodend=0.00m;
				if(PN.Rod_End.ToString().Substring(0,1) !="N" &&PN.Rod_End.ToString().Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd","WEB_SeriesACommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="True";
					}
				}
				rodend=Decimal.Round(rodend,2);
				rodend =rodend + rodend * (index /100);
				//issue #315 start
				seal = seal*(1 - (discount/100));
				seal = Decimal.Round(seal,2);
				rodend = rodend*(1 - (discount/100));
				rodend = Decimal.Round(rodend,2);
				//issue #315 end
				decimal total=0.00m;
				total=sprice + mprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}

		public void ATPricing()
		{
			try
			{
				ArrayList lst =new ArrayList();
				lst =(ArrayList)Session["User"];
				string comtable="";
				//issue #118 start
				//backup
				//if(lst[6].ToString().Trim() =="1002")
				//update
				if(lst[6].ToString().Trim() =="1002" || lst[6].ToString().Trim() =="1021")
				//issue #118 end
				{
					lblmgrp.Text="AP_Base";
					lbltable.Text="WEB_ATPrice_SVC_TableV1";
					comtable="WEB_SVCSeriesATCommAdders_TableV1";
				}
				else
				{
					lblmgrp.Text="AP_Base";
					lbltable.Text="WEB_ATPrice_Master_TableV1";
					comtable="WEB_SeriesATCommAdders_TableV1";
				}				
				PartNumber PN = (PartNumber)Session["PartNo"];										
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal seal=0.00m;

				st =Convert.ToDecimal(PN.Stroke);
				sp =db.SelectStrokeA(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				sp =Decimal.Round(sp,2);
				
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(db.SelectPriceIndex("AT").ToString());
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				lblD.Text=db.SelectDiscount(lst[1].ToString().Trim(),"AT");
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100));
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text=sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
				//mount price
				decimal mp =0.00m;
				string mgrp = lblmgrp.Text.ToString();
				mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100)); 
				
				mprice=Decimal.Round(mp,2);
				if(PN.Mount.Trim().Substring(0,1) =="I" || PN.Mount.Trim().Substring(0,1) =="M" )
				{
					decimal a1=0.00m;
					a1 =db.SelectISSMSSPrice(PN.Mount.Trim());
					if(a1 >0)
					{
						a1 =a1 + a1 * (index /100);
						a1= a1*(1 - (discount/100)); 
						a1 =Decimal.Round(a1,2);
						mprice +=a1;
					}
				}
				else if(PN.Mount.Trim().Substring(0,2) =="GR" )
				{
					decimal a1=0.00m;
					a1 =db.SelectGRPrice(PN.Mount.Trim());
					if(a1 >0)
					{
						a1 =a1 + a1 * (index /100);
						a1= a1*(1 - (discount/100)); 
						a1 =Decimal.Round(a1,2);
						mprice +=a1;
					}
				}
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}

				if(PN.Seal_Comp.ToString().Trim() =="L")
				{
					seal =db.SelectAddersPrice("SL",comtable,PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="F")
				{
					seal =db.SelectAddersPrice("SF",comtable,PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="E")
				{
					seal =db.SelectAddersPrice("SE",comtable,PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				seal=Decimal.Round(seal,2);

				decimal rodend=0.00m;
				if(PN.Rod_End.ToString().Substring(0,1) !="N" &&PN.Rod_End.ToString().Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd",comtable,PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="True";
					}
				}
				rodend=Decimal.Round(rodend,2);
				seal =seal + seal * (index /100);
				rodend =rodend + rodend * (index /100);
				//issue #315 start
				seal = seal*(1 - (discount/100));
				seal = Decimal.Round(seal,2);
				rodend = rodend*(1 - (discount/100));
				rodend = Decimal.Round(rodend,2);
				//issue #315 end
				decimal total=0.00m;
				total=sprice + mprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s=s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}
		public void SAPricing()
		{
			try
			{
				lblmgrp.Text="BasePrice";
				lbltable.Text="WEB_APrice_SVC_TableV1";
				PartNumber PN= (PartNumber)Session["PartNo"];
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal seal=0.00m;

				st =Convert.ToDecimal(PN.Stroke);
				sp =db.SelectStrokeA(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				sp =Decimal.Round(sp,2);
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(db.SelectPriceIndex("SA").ToString());
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				ArrayList lst=new ArrayList();
				lst=(ArrayList)Session["User"];
				lblD.Text=db.SelectDiscount(lst[1].ToString().Trim(),"SA");
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100));
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text=sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
				//mount price
				decimal mp =0.00m;
				string mgrp = lblmgrp.Text.ToString();
				mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100)); 
				
				mprice=Decimal.Round(mp,2);
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}

				if(PN.Seal_Comp.ToString().Trim() =="L")
				{
					seal =db.SelectAddersPrice("SL","WEB_SVCSeriesATCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="F")
				{
					seal =db.SelectAddersPrice("SF","WEB_SVCSeriesATCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="E")
				{
					seal =db.SelectAddersPrice("SE","WEB_SVCSeriesATCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				seal=Decimal.Round(seal,2);

				decimal rodend=0.00m;
				if(PN.Rod_End.ToString().Substring(0,1) !="N" &&PN.Rod_End.ToString().Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd","WEB_SVCSeriesATCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="True";
					}
				}
				rodend=Decimal.Round(rodend,2);
				seal =seal + seal * (index /100);
				rodend =rodend + rodend * (index /100);
				decimal total=0.00m;
				total=sprice + mprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}
		private void BtnSendMail_Click(object sender, System.EventArgs e)
		{
			if(Page.IsValid)
			{
				BtnGeneratePrice.Visible=true;
				QItems qitem=new QItems();
				qitem =(QItems)Session["Part"];
				Quotation quote=new Quotation();
				quote =(Quotation)Session["Quote"];

				ArrayList lst1 =new ArrayList();
				lst1 =(ArrayList)Session["User"];
                ArrayList cclist = new ArrayList();
				if(qitem.Cusomer_ID.Trim()=="Reseau CB/DIV. CAN Bearings (Qu�bec)" || qitem.Cusomer_ID.Trim()=="Engrenage Provincial Inc")
                {
                    cclist.Add("clemire@cowandynamics.com");
                    cclist.Add("jbehara@cowandynamics.com");
                }
                else
                {
                    cclist.Add("jbehara@cowandynamics.com");
                }
                string body =
                    "<hr color='#FF0000'>Bonjour, <br> " + lst1[0].ToString() + " a une nouvelle demande de prix:"
                    + "<TABLE id='Table1' borderColor='#0000ff' cellSpacing='1' cellPadding='1' align='left' border='1'>"
                    + "<TR><TD noWrap>Date de demande :</TD><TD noWrap>" + quote.Quotedate.Trim() + "</TD></TR>"
                    + "<TR><TD noWrap>No de demande :</TD><TD noWrap>" + quote.QuoteNo.ToString().Trim() + "</TD></TR>"
                    + "<TR><TD noWrap>Part No :</TD><TD noWrap>" + qitem.PartNo.ToString().Trim() + "</TD></TR>"
                    + "<TR><TD noWrap>Special Request Note :</TD><TD noWrap>" + TxtSpecialReq.Text.Trim() + "</TD></TR></TABLE>"
                    + "<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>Clic sur le lien ci-dessous � r�pondre: <a href='http://172.16.0.253:9000/'>Quote System</a>"
                    + "<br><br><hr> Merci <br>I-Cylinder<br><hr color='#FF0000'><br>"
                    + "<hr color='#FF0000'>Hi, <br> " + lst1[0].ToString() + " has entered a new request for a quote:"
                    + "<TABLE id='Table1' borderColor='#0000ff' cellSpacing='1' cellPadding='1' align='left' border='1'>"
                    + "<TR><TD noWrap>Quote Date :</TD><TD noWrap>" + quote.Quotedate.Trim() + "</TD></TR>"
                    + "<TR><TD noWrap>Quote No :</TD><TD noWrap>" + quote.QuoteNo.ToString().Trim() + "</TD></TR>"
                    + "<TR><TD noWrap>Part No :</TD><TD noWrap>" + qitem.PartNo.ToString().Trim() + "</TD></TR>"
                    + "<TR><TD noWrap>Special Request Note :</TD><TD noWrap>" + TxtSpecialReq.Text.Trim() + "</TD></TR></TABLE>"
                    + "<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>Please log into <a href='http://172.16.0.253:9000/'>Quote System</a> to complete the Quote."
                    + "<br><br><hr> Thanks <br>I-Cylinder<br><hr color='#FF0000'>"
                    + MailService.Get_Admin();
                string attachment = "";
                if (qitem.DWG.Trim() != "")
                {
                    attachment = Request.PhysicalApplicationPath + "icyl_drawing/" + qitem.DWG.ToString().Trim();
                }
                MailService.SendMail(
                              "dtaranu@cowandynamics.com",
                              cclist,
                              "[" + DDLPriority.SelectedItem.Text.Trim() + "] New Quote from " + quote.Customer.Trim(),
                              body,
                              attachment,
                              DDLPriority.SelectedIndex == 1);
                
				Session["PartNo"]=null;
				Session["Accessories"]=null;
				Response.Redirect("Result.aspx?id="+quote.QuoteNo.Trim());	
			}
		}
		private void DDLDiameterSec_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(DDLDiameterSec.SelectedIndex >0)
			{
				string b =db.SelectCode("Bore_Code","WEB_Bore_TableV1","Bore_Size",DDLBore.SelectedItem.ToString());
				string r=db.SelectCode("Rod_Code","WEB_RodSerZ_TableV1","Rod_Size",DDLDiameterSec.SelectedItem.ToString());
				decimal wdim=db.SelectAddersPrice("V","WEB_Min_Values_TableV1",b.Trim(),r.Trim());
				decimal min=0.00m;
				min =wdim + round;
				min =Decimal.Round(min,2);
				lblwd.Text ="(Min = "+min.ToString()+" & Max = 24.00)";
			} 
		}

		private void TxtBBHead_TextChanged(object sender, System.EventArgs e)
		{
			try
			{
				if(TxtBBHead.Text.ToString().Trim() !="" && IsNumeric(TxtBBHead.Text.ToString().Trim()) ==true )
				{
			
					TxtBBHead.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TxtBBHead.Text));
				}
				else
				{
					TxtBBHead.Text="";
				}
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}

		private void TxtBBCap_TextChanged(object sender, System.EventArgs e)
		{
			try
			{
				if(TxtBBCap.Text.ToString().Trim() !="" && IsNumeric(TxtBBCap.Text.ToString().Trim()) ==true )
				{
			
					TxtBBCap.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TxtBBCap.Text));
				}
				else
				{
					TxtBBCap.Text="";
				}
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}

		private string UploadFile(object Sender,EventArgs E)
		{
			string file="";
			if (UploadDwg.PostedFile !=null) //Checking for valid file
			{	
				string StrFileName ="ICyl"+DateTime.Now.Ticks.ToString()+"_CustomerDwg.pdf" ;
				string StrFileType = UploadDwg.PostedFile.ContentType ;
				int IntFileSize =UploadDwg.PostedFile.ContentLength;
				if (IntFileSize <=0)
				{
					lblstatus.Text="Uploading of file " + StrFileName + " failed ";
				}
				else
				{
					UploadDwg.PostedFile.SaveAs(Server.MapPath("./icyl_drawing/" + StrFileName));
					file=StrFileName.ToString();
				}
			}
			return file;
		}
		private string UploadFileTemp(object Sender,EventArgs E)
		{
			string file="";
			if (UploadDwg.PostedFile !=null) //Checking for valid file
			{	
				string StrFileName = "ICyl"+DateTime.Now.Ticks.ToString()+"_CustomerDwg.pdf" ;
				string StrFileType = UploadDwg.PostedFile.ContentType ;
				int IntFileSize =UploadDwg.PostedFile.ContentLength;
				if (IntFileSize <=0)
				{
					lblstatus.Text="Uploading of file " + StrFileName + " failed ";
				}
				else
				{
					UploadDwg.PostedFile.SaveAs(Server.MapPath("./temp_drawing/" + StrFileName));
					lblfilename.Text=UploadDwg.Value.ToString();
					file=StrFileName.ToString();
				}
			}
			return file;
		}
		private void LbPreview_Click(object sender, System.EventArgs e)
		{
			if(UploadDwg.Value.Trim() !="")
			{
				if(UploadDwg.PostedFile.ContentType.ToString() =="application/pdf")
				{
					lblstatus.Text="";
					string upload="";
					if(UploadDwg.Value.Trim()!="")
					{
						upload=UploadFileTemp(sender,e);
						cbupload.Visible=true;
					}
					if(upload.Trim() !="")
					{
						lblfile.Text=upload.Trim();
						LblInfo.Text="<script language='javascript'>window.open('temp_drawing/"+upload.Trim()+"','_blank','toolbar=yes,width=1000,height=750,resizable=yes,top=0,left=0')</script>";						
					}
				}
				else
				{
					LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Please select a PDF file!!!')</script>";
				}
			}
			else
			{
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Please select a file to preview!!!')</script>";
			}
		}

		private void CBTransducer_CheckedChanged(object sender, System.EventArgs e)
		{
			if(CBTransducer.Checked == false)
			{
				
				PNTransducer.Visible=false;
			}
			else if(CBTransducer.Checked == true)
			{				
				PNTransducer.Visible=true;
				ListItem litem=new ListItem();
				ArrayList list = new ArrayList();
				ArrayList blist1 = new ArrayList();
				ArrayList blist2 = new ArrayList();
				list=db.SelectOptionsWithCode("Code","Transducer","WEB_Transducer_TableV1",db.SelectBoreCode(DDLBore.SelectedItem.Text.ToString()),"Slno");
				DDLTransducer.Items.Clear();
				blist1=new ArrayList();
				blist2=new ArrayList();
				blist1 =(ArrayList)list[0];
				blist2 =(ArrayList)list[1];
				for(int k=0;k <blist1.Count;k++)
				{
					litem =new ListItem(blist2[k].ToString().Trim(),blist1[k].ToString().Trim());
					DDLTransducer.Items.Add(litem);
				}
				TxtTransducer.Text="10";
				DDLTransducer.SelectedIndex=0;
			}
		}

		private void DDLTransducer_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(DDLTransducer.SelectedIndex >1)
			{
				TxtTransducer.Text="";
				TxtTransducer.Visible=false;
				lblminmaxohm.Visible=false;
				lblressist.Visible=false;
				lblohm.Visible=false;
			}
			else
			{
				TxtTransducer.Enabled=true;
				TxtTransducer.Text="10";
				TxtTransducer.Visible=true;
				lblminmaxohm.Visible=true;
				lblressist.Visible=true;
				lblohm.Visible=true;
			}
		}

	}
	
}

