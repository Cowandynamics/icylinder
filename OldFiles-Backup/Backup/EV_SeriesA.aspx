<%@ Page language="c#" Codebehind="EV_SeriesA.aspx.cs" AutoEventWireup="false" Inherits="iCylinderV1.EV_SeriesA" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>EV_SeriesA</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE style="WIDTH: 1000px; HEIGHT: 487px" id="Table1" border="0" cellSpacing="0" cellPadding="0"
				width="1000" bgColor="lightgrey">
				<TR>
					<TD height="20" width="50"></TD>
					<TD style="WIDT: 484px" height="20" align="center"></TD>
					<TD height="20" align="center"><asp:label id="lbls" runat="server" Font-Size="Small" Font-Underline="True" BorderColor="Transparent"
							BackColor="Transparent" Font-Bold="True" ForeColor="#C00000">Series  A - EVR Cylinder</asp:label></TD>
					<TD height="20" align="center"></TD>
					<TD height="20" width="50"></TD>
				</TR>
				<TR>
					<TD width="50"></TD>
					<TD language="300" bgColor="#dcdcdc" width="299" align="center"><asp:panel id="Panel9" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
							<TABLE style="HEIGHT: 8px" id="Table14" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label18" runat="server" ForeColor="Maroon" Font-Bold="True" BackColor="Transparent"
											BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller">Valve Size</asp:label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:DropDownList id="DDLValveSize" runat="server" Font-Size="8pt" AutoPostBack="True" Width="57px">
											<asp:ListItem Value="Select" Selected="True">Select</asp:ListItem>
											<asp:ListItem Value="1">1</asp:ListItem>
											<asp:ListItem Value="1.5">1.5</asp:ListItem>
											<asp:ListItem Value="2">2</asp:ListItem>
											<asp:ListItem Value="2.5">2.5</asp:ListItem>
											<asp:ListItem Value="3">3</asp:ListItem>
											<asp:ListItem Value="4">4</asp:ListItem>
											<asp:ListItem Value="5">5</asp:ListItem>
											<asp:ListItem Value="6">6</asp:ListItem>
											<asp:ListItem Value="8">8</asp:ListItem>
											<asp:ListItem Value="10">10</asp:ListItem>
											<asp:ListItem Value="12">12</asp:ListItem>
											<asp:ListItem Value="14">14</asp:ListItem>
											<asp:ListItem Value="16">16</asp:ListItem>
											<asp:ListItem Value="18">18</asp:ListItem>
											<asp:ListItem Value="20">20</asp:ListItem>
											<asp:ListItem Value="24">24</asp:ListItem>
										</asp:DropDownList></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:Label style="Z-INDEX: 0" id="Lblvalve" runat="server" Font-Size="Smaller" Visible="False"></asp:Label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:label id="Lblval" runat="server" ForeColor="Red" BackColor="Transparent" BorderColor="Transparent"
											Font-Size="9pt" Width="234px" Visible="False">Select valve size before entering other values</asp:label></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="PanelLP" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
							<TABLE style="HEIGHT: 8px" id="Table13" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label12" runat="server" ForeColor="Maroon" Font-Bold="True" BackColor="Transparent"
											BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller">Line Pressure</asp:label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:textbox id="TxtLinearP" runat="server" Font-Size="XX-Small" AutoPostBack="True" Width="64px"
											MaxLength="3"></asp:textbox>&nbsp;psi</TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:label id="Lbllp" runat="server" ForeColor="Red" BackColor="Transparent" BorderColor="Transparent"
											Font-Size="9pt" Width="206px" Visible="False">Line Pressure is higher than standard. Consult EVR Engineering.</asp:label></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="PanelMAS" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px"
							style="Z-INDEX: 0">
							<TABLE style="HEIGHT: 8px" id="Table5" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label6" runat="server" ForeColor="Maroon" Font-Bold="True" BackColor="Transparent"
											BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller">Minimum Air Supply</asp:label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:textbox id="TxtMinAS" runat="server" Font-Size="XX-Small" AutoPostBack="True" Width="64px"
											MaxLength="3"></asp:textbox>&nbsp;psi</TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:label id="Lblas" runat="server" ForeColor="Red" BackColor="Transparent" BorderColor="Transparent"
											Font-Size="9pt" Width="266px" Visible="False">Min Air Supply is greater than Series A capacity. The Sizing program will use 150 PSI</asp:label></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel style="Z-INDEX: 0" id="PanelT" runat="server" BorderColor="LightGray" BorderStyle="Solid"
							BorderWidth="1px">
							<TABLE style="HEIGHT: 8px" id="Table15" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label16" runat="server" ForeColor="Maroon" Font-Bold="True" BackColor="Transparent"
											BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller">Seating Thrust</asp:label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:textbox id="TxtThrust" runat="server" Font-Size="XX-Small" AutoPostBack="True" Width="64px"></asp:textbox>&nbsp;lbs</TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:label id="lblthrust" runat="server" ForeColor="Red" BackColor="Transparent" BorderColor="Transparent"
											Font-Size="9pt" Width="266px" Visible="False"></asp:label></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="Panel4" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
							<TABLE style="HEIGHT: 8px" id="Table12" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label3" runat="server" ForeColor="Maroon" Font-Bold="True" BackColor="Transparent"
											BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller">Funnel Sleeve/Double Wall/ Pre Pinch Option</asp:label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 17px" align="center">
										<asp:radiobuttonlist id="RBLFunnel" runat="server" Font-Size="Smaller" BorderStyle="None" AutoPostBack="True"
											Width="95px" RepeatLayout="Flow" Height="1px" RepeatDirection="Horizontal">
											<asp:ListItem Value="yes">Yes</asp:ListItem>
											<asp:ListItem Value="No" Selected="True">No</asp:ListItem>
										</asp:radiobuttonlist></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:label id="Label20" runat="server" ForeColor="Maroon" Font-Bold="True" BackColor="Transparent"
											BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller">Stroke</asp:label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:textbox id="TxtStroke" runat="server" Font-Size="XX-Small" AutoPostBack="True" Width="64px"
											ReadOnly="True"></asp:textbox></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:label id="lblstroke" runat="server" ForeColor="Red" BackColor="Transparent" BorderColor="Transparent"
											Font-Size="9pt" Width="234px" Visible="False"></asp:label></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
					<TD bgColor="#dcdcdc" width="299" align="center">
						<TABLE style="HEIGHT: 35px" id="Table2" border="0" cellSpacing="0" borderColor="silver"
							cellPadding="0" width="299">
						</TABLE>
						<asp:panel id="Panel10" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
							<TABLE style="HEIGHT: 35px" id="Table7" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0">
								<TR>
									<TD align="center">
										<asp:label id="Label22" runat="server" ForeColor="Maroon" Font-Bold="True" BackColor="Transparent"
											BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller">Cylinder Type</asp:label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 16px" align="center">
										<asp:radiobuttonlist id="RBLCylType" runat="server" Font-Size="Smaller" BorderStyle="None" RepeatDirection="Horizontal"
											RepeatColumns="2"></asp:radiobuttonlist></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="PanelM" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
							<TABLE style="HEIGHT: 35px" id="Table11" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label14" runat="server" ForeColor="Maroon" Font-Bold="True" BackColor="Transparent"
											BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller">Mount</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:radiobuttonlist id="RBLMount" runat="server" Font-Size="Smaller" BorderStyle="None" AutoPostBack="True"
											RepeatColumns="2"></asp:radiobuttonlist></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:DropDownList id="DDLMount" runat="server" Font-Size="8pt" Width="294px" Visible="False"></asp:DropDownList></TD>
								</TR>
							</TABLE>
							<asp:label style="Z-INDEX: 0" id="lblmount" runat="server" ForeColor="Red" BackColor="Transparent"
								BorderColor="Transparent" Font-Size="9pt" Width="234px" Visible="False">No MSS mount available !! </asp:label>
						</asp:panel><asp:panel id="Panel5" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
							<TABLE id="Table8" border="0" cellSpacing="0" borderColor="silver" cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label9" runat="server" ForeColor="Maroon" Font-Bold="True" BackColor="Transparent"
											BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller">Rod End</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:radiobuttonlist id="RBLRodend1" runat="server" Font-Size="Smaller" BorderStyle="None" Width="229px"
											RepeatLayout="Flow" Height="21px" RepeatDirection="Horizontal">
											<asp:ListItem Value="A4" Selected="True">Small Female (Series A)</asp:ListItem>
										</asp:radiobuttonlist>
										<asp:image style="Z-INDEX: 0" id="Img1stRodEnd" runat="server" Width="100px" Height="70px"></asp:image></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
					<TD bgColor="gainsboro" width="299" align="center"><asp:panel id="Panel3" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
							<TABLE style="HEIGHT: 35px" id="Table6" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0">
								<TR>
									<TD align="center">
										<asp:label id="Label10" runat="server" ForeColor="Maroon" Font-Bold="True" BackColor="Transparent"
											BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller">Ports</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:Label id="Label8" runat="server" Font-Size="Smaller">NPT Ports</asp:Label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:label id="Label11" runat="server" ForeColor="Maroon" Font-Bold="True" BackColor="Transparent"
											BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller">Port Positions</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:Label id="Label15" runat="server" Font-Size="Smaller">Position 1 Both ends</asp:Label></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="Panel7" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
							<TABLE style="HEIGHT: 35px" id="Table10" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0">
								<TR>
									<TD align="center">
										<asp:label id="Label13" runat="server" ForeColor="Maroon" Font-Bold="True" BackColor="Transparent"
											BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller">Piston Rod Material</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:radiobuttonlist id="RBLPRod" runat="server" Font-Size="Smaller" BorderStyle="None" RepeatColumns="1">
											<asp:ListItem Selected="True">1045 steel piston rod</asp:ListItem>
											<asp:ListItem Value="M3">316 Stainless steel piston rod</asp:ListItem>
										</asp:radiobuttonlist></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="P2ndRodnd" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
							<TABLE style="HEIGHT: 35px" id="Table9" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0">
								<TR>
									<TD align="center">
										<asp:label id="Label2" runat="server" ForeColor="Maroon" Font-Bold="True" BackColor="Transparent"
											BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller">Seals</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:Label id="Label17" runat="server" Font-Size="Smaller">Standard Seals</asp:Label></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="P1stAdvanced" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
							<TABLE style="HEIGHT: 35px" id="Table4" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0">
								<TR>
									<TD align="center">
										<asp:label id="Label7" runat="server" ForeColor="Maroon" Font-Bold="True" BackColor="Transparent"
											BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller">Style</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:radiobuttonlist id="RBLStyle" runat="server" Font-Size="Smaller" BorderStyle="None" Width="127px"
											RepeatColumns="1">
											<asp:ListItem Value="N" Selected="True">Double Acting</asp:ListItem>
											<asp:ListItem Value="FC">Fail Close</asp:ListItem>
											<asp:ListItem Value="FO">Fail Open</asp:ListItem>
										</asp:radiobuttonlist></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="Panel1" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
							<TABLE style="HEIGHT: 35px" id="Table3" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0">
								<TR>
									<TD align="center">
										<asp:label id="Label5" runat="server" ForeColor="Maroon" Font-Bold="True" BackColor="Transparent"
											BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller">Paint</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:radiobuttonlist id="RBLPaint" runat="server" Font-Size="Smaller" BorderStyle="None" Width="127px"
											RepeatColumns="1">
											<asp:ListItem Value="S" Selected="True">Standard</asp:ListItem>
											<asp:ListItem Value="C1">Epoxy Paint</asp:ListItem>
										</asp:radiobuttonlist></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
					<TD width="50"></TD>
				</TR>
				<TR>
					<TD height="30" width="50" align="right">
						<asp:LinkButton style="Z-INDEX: 0" id="LBBack" runat="server" Font-Size="Smaller" Font-Bold="True">Back</asp:LinkButton></TD>
					<TD language="300" height="30" align="center"><asp:label id="lblhidden" runat="server" Font-Size="XX-Small" Visible="False"></asp:label></TD>
					<TD height="30" align="center"><asp:label id="lblpage" runat="server" Font-Size="9pt" BorderColor="Transparent" BackColor="Transparent"
							ForeColor="Red" Width="234px" Visible="False">Enter / Select  all values</asp:label></TD>
					<TD height="30" align="center"><asp:label id="Lblp1" runat="server" Font-Size="XX-Small" Visible="False"></asp:label>
						<asp:label style="Z-INDEX: 0" id="lblfstyle" runat="server" Font-Size="XX-Small" Visible="False"></asp:label></TD>
					<TD height="30" width="50"><asp:linkbutton id="BtnNext" runat="server" Font-Size="Smaller" Font-Bold="True">Next</asp:linkbutton></TD>
				</TR>
				<TR>
					<TD height="20" width="50"></TD>
					<TD language="300" height="20" align="center"></TD>
					<TD height="20" align="center"></TD>
					<TD height="20" align="center"></TD>
					<TD height="20" width="50"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
