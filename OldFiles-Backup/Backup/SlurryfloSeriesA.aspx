<%@ Page language="c#" Codebehind="SlurryfloSeriesA.aspx.cs" AutoEventWireup="false" Inherits="iCylinderV1.SlurryfloSeriesA" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>SlurryfloSeriesA</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 1000px; HEIGHT: 487px" cellSpacing="0" cellPadding="0"
				width="1000" bgColor="lightgrey" border="0">
				<TR>
					<TD width="50" height="20"></TD>
					<TD style="WIDT: 484px" align="center" height="20"></TD>
					<TD align="center" height="20">
						<asp:label id="Label1" runat="server" Font-Size="Small" Font-Underline="True" BorderColor="Transparent"
							BackColor="Transparent">Series  A -Slurryflo Acruators</asp:label></TD>
					<TD align="center" height="20"></TD>
					<TD width="50" height="20"></TD>
				</TR>
				<TR>
					<TD width="50"></TD>
					<TD language="300" align="center" bgColor="#dcdcdc" width="299">
						<asp:Panel id="Panel1" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
							<TABLE id="Table3" border="0" cellSpacing="0" borderColor="silver" cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label4" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
											Font-Size="Smaller" ForeColor="Maroon" Font-Bold="True">Bore Size</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:radiobuttonlist id="RBLBore" runat="server" Font-Size="Smaller" BorderStyle="None" RepeatDirection="Horizontal"
											RepeatColumns="3"></asp:radiobuttonlist></TD>
								</TR>
							</TABLE>
						</asp:Panel>
						<asp:Panel id="Panel2" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
							<TABLE style="HEIGHT: 8px" id="Table5" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label5" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
											Font-Size="Smaller" ForeColor="Maroon" Font-Bold="True">Stroke</asp:label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:textbox id="TxtStroke" runat="server" Font-Size="XX-Small" Width="64px" AutoPostBack="True"></asp:textbox></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:label id="Label3" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Size="XX-Small"
											ForeColor="DimGray">Min= 0.00, Max= 120.00(Consult Factory, if stroke >120)</asp:label></TD>
								</TR>
							</TABLE>
							<asp:label id="lblstroke" runat="server" BackColor="Transparent" BorderColor="Transparent"
								Font-Size="X-Small" ForeColor="Red" Visible="False"></asp:label>
						</asp:Panel>
						<asp:radiobuttonlist id="RBL1stRodSize" runat="server" Font-Size="Smaller" BorderStyle="None" RepeatColumns="4"
							RepeatDirection="Horizontal" AutoPostBack="True" Visible="False"></asp:radiobuttonlist></TD>
					<TD align="center" bgColor="#dcdcdc" width="299">
						<TABLE id="Table2" style="HEIGHT: 35px" borderColor="silver" cellSpacing="0" cellPadding="0"
							width="299" border="0">
						</TABLE>
						<asp:Panel id="Panel8" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE style="HEIGHT: 35px" id="Table11" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label14" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
											Font-Size="Smaller" ForeColor="Maroon" Font-Bold="True">Mount</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:radiobuttonlist id="RBLMount" runat="server" Font-Size="Smaller" BorderStyle="None" RepeatDirection="Horizontal"
											RepeatColumns="4" Width="47px" AutoPostBack="True"></asp:radiobuttonlist></TD>
								</TR>
							</TABLE>
						</asp:Panel>
						<asp:Panel id="Panel5" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE id="Table8" border="0" cellSpacing="0" borderColor="silver" cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label9" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
											Font-Size="Smaller" ForeColor="Maroon" Font-Bold="True">Rod End</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:image id="Img1stRodEnd" runat="server" Width="100px" Height="70px"></asp:image></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:radiobuttonlist id="RBLRodend1" runat="server" Font-Size="Smaller" BorderStyle="None" RepeatDirection="Horizontal"
											Width="144px" AutoPostBack="True" Height="1px" RepeatLayout="Flow">
											<asp:ListItem Value="A4" Selected="True">Small Female (Series A)</asp:ListItem>
										</asp:radiobuttonlist></TD>
								</TR>
							</TABLE>
						</asp:Panel></TD>
					<TD align="center" bgColor="gainsboro" width="299">
						<asp:Panel id="Panel7" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE style="HEIGHT: 35px" id="Table10" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0">
								<TR>
									<TD align="center">
										<asp:label id="Label13" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
											Font-Size="Smaller" ForeColor="Maroon" Font-Bold="True">Piston Rod Material</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:Label id="Label19" runat="server" Font-Size="Smaller">316 Stainless steel piston rod</asp:Label></TD>
								</TR>
							</TABLE>
						</asp:Panel>
						<asp:Panel id="P1stAdvanced" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE style="HEIGHT: 35px" id="Table4" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0">
								<TR>
									<TD align="center">
										<asp:label id="Label7" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
											Font-Size="Smaller" ForeColor="Maroon" Font-Bold="True">Cushion</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:Label id="Label16" runat="server" Font-Size="Smaller">None</asp:Label></TD>
								</TR>
							</TABLE>
						</asp:Panel>
						<asp:Panel id="P2ndRodnd" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE style="HEIGHT: 35px" id="Table9" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0">
								<TR>
									<TD align="center">
										<asp:label id="Label2" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
											Font-Size="Smaller" ForeColor="Maroon" Font-Bold="True">Seals</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:Label id="Label17" runat="server" Font-Size="Smaller" Visible="False">Low Temp Seals</asp:Label></TD>
								</TR>
							</TABLE>
							<asp:RadioButtonList id="rblSeals" runat="server" Font-Size="Smaller" RepeatDirection="Horizontal" Width="230px"
								Height="20px">
								<asp:ListItem Value="L" Selected="True">Low Temp Seals</asp:ListItem>
								<asp:ListItem Value="N">Standard Seals</asp:ListItem>
							</asp:RadioButtonList>
						</asp:Panel>
						<asp:Panel id="Panel6" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE style="HEIGHT: 35px" id="Table7" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0">
								<TR>
									<TD align="center">
										<asp:label id="Label12" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
											Font-Size="Smaller" ForeColor="Maroon" Font-Bold="True">Metallic Rod Scrapper</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:Label id="Label18" runat="server" Font-Size="Smaller">Severe Service</asp:Label></TD>
								</TR>
							</TABLE>
						</asp:Panel>
						<asp:Panel id="Panel3" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE style="HEIGHT: 35px" id="Table6" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0">
								<TR>
									<TD align="center">
										<asp:label id="Label10" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
											Font-Size="Smaller" ForeColor="Maroon" Font-Bold="True">Ports</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:Label id="Label8" runat="server" Font-Size="Smaller">NPT Ports</asp:Label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:label id="Label11" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
											Font-Size="Smaller" ForeColor="Maroon" Font-Bold="True">Port Positions</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:Label id="Label15" runat="server" Font-Size="Smaller">Position 1 Both ends</asp:Label></TD>
								</TR>
							</TABLE>
						</asp:Panel></TD>
					<TD width="50"></TD>
				</TR>
				<TR>
					<TD align="right" width="50" height="30">
						<asp:LinkButton id="LBBack" runat="server" Font-Size="Smaller" Font-Bold="True">Back</asp:LinkButton></TD>
					<TD language="300" align="center" height="30"></TD>
					<TD align="center" height="30"></TD>
					<TD align="center" height="30"></TD>
					<TD width="50" height="30">
						<asp:LinkButton id="BtnNext" runat="server" Font-Size="Smaller" Font-Bold="True">Next</asp:LinkButton></TD>
				</TR>
				<TR>
					<TD width="50" height="20"></TD>
					<TD language="300" align="center" height="20"></TD>
					<TD align="center" height="20">
						<asp:label id="lblhidden" runat="server" Font-Size="XX-Small" Visible="False"></asp:label></TD>
					<TD align="center" height="20"></TD>
					<TD width="50" height="20"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
