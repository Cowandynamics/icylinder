<%@ Page language="c#" Codebehind="Icylinder_PageAccP.aspx.cs" AutoEventWireup="false" Inherits="iCylinderV1.Icylinder_PageAccP" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Icylinder_PageAccP</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
  </HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 1000px; HEIGHT: 487px" cellSpacing="0" cellPadding="0"
				width="1000" bgColor="lightgrey" border="0">
				<TR>
					<TD width="50" height="20"></TD>
					<TD align="center" height="20">
						<asp:label id="Label1" runat="server" Font-Size="Smaller" Font-Underline="True" BackColor="Transparent"
							Width="229px" Font-Bold="True" Height="19"> Cap End Accessories</asp:label></TD>
					<TD width="50" height="20"></TD>
				</TR>
				<TR>
					<TD width="50"></TD>
					<TD align="center" bgColor="gainsboro">
						<TABLE id="Table3" style="WIDTH: 238px; HEIGHT: 333px" borderColor="gray" cellSpacing="0"
							cellPadding="0" width="238" border="1">
							<TR>
								<TD align="center">
									<TABLE id="Table24" style="WIDTH: 624px; HEIGHT: 65px" borderColor="whitesmoke" cellSpacing="0"
										cellPadding="0" width="624" border="1">
										<TR>
											<TD>
												<asp:Panel id="PanelRodeye" runat="server" Enabled="False">
                  <TABLE id=Table16 cellSpacing=0 cellPadding=0 width=300 
                  border=0>
                    <TR>
                      <TD style="WIDTH: 251px">
                        <TABLE id=Table17 style="WIDTH: 192px; HEIGHT: 91px" 
                        cellSpacing=0 cellPadding=0 width=192 border=0>
                          <TR>
                            <TD style="HEIGHT: 8px">
<asp:CheckBox id=CBRodeye runat="server" Font-Size="Smaller" AutoPostBack="True" Text="Rod Eye"></asp:CheckBox></TD></TR>
                          <TR>
                            <TD>
<asp:DropDownList id=DDLRodeye runat="server" Width="190px" Font-Size="XX-Small" AutoPostBack="True"></asp:DropDownList></TD></TR>
                          <TR>
                            <TD></TD></TR></TABLE></TD>
                      <TD align=right width=110>
<asp:Image id=ImgRodeye runat="server" ImageUrl="accessories\rodeye.jpg"></asp:Image></TD></TR></TABLE>
												</asp:Panel></TD>
											<TD>
												<asp:Panel id="PanelRodClevis" runat="server" Enabled="False">
                  <TABLE id=Table6 cellSpacing=0 cellPadding=0 width=300 
                  border=0>
                    <TR>
                      <TD style="WIDTH: 251px">
                        <TABLE id=Table12 style="WIDTH: 192px; HEIGHT: 91px" 
                        cellSpacing=0 cellPadding=0 width=192 border=0>
                          <TR>
                            <TD style="HEIGHT: 11px">
<asp:CheckBox id=CBRodclevis runat="server" Font-Size="Smaller" AutoPostBack="True" Text="Rod Clevis"></asp:CheckBox></TD></TR>
                          <TR>
                            <TD>
<asp:DropDownList id=DDLRodclevis runat="server" Width="190px" Font-Size="XX-Small" AutoPostBack="True"></asp:DropDownList></TD></TR>
                          <TR>
                            <TD></TD></TR></TABLE></TD>
                      <TD align=right width=110>
<asp:Image id=ImgRodclevis runat="server" ImageUrl="accessories\rodclevis.jpg"></asp:Image></TD></TR></TABLE>
												</asp:Panel></TD>
											<TD>
												<asp:Panel id="PanelPivotPin" runat="server" Enabled="False">
                  <TABLE id=Table4 cellSpacing=0 cellPadding=0 width=300 
                  border=0>
                    <TR>
                      <TD style="WIDTH: 251px">
                        <TABLE id=Table2 style="WIDTH: 192px; HEIGHT: 91px" 
                        cellSpacing=0 cellPadding=0 width=192 border=0>
                          <TR>
                            <TD style="HEIGHT: 9px">
<asp:CheckBox id=CBPivotpin runat="server" Font-Size="Smaller" AutoPostBack="True" Text="Pivot Pin"></asp:CheckBox></TD></TR>
                          <TR>
                            <TD>
<asp:DropDownList id=DDLPivotpin runat="server" Width="190px" Font-Size="XX-Small" AutoPostBack="True"></asp:DropDownList></TD></TR>
                          <TR>
                            <TD></TD></TR></TABLE></TD>
                      <TD align=right width=110>
<asp:Image id=ImgPivotpin runat="server" ImageUrl="accessories\pivotpin.jpg"></asp:Image></TD></TR></TABLE>
												</asp:Panel></TD>
										</TR>
										<TR>
											<TD>
												<asp:Panel id="PanelEyeBracket" runat="server" Enabled="False">
                  <TABLE id=Table13 cellSpacing=0 cellPadding=0 width=300 
                  border=0>
                    <TR>
                      <TD style="WIDTH: 251px">
                        <TABLE id=Table14 style="WIDTH: 192px; HEIGHT: 91px" 
                        cellSpacing=0 cellPadding=0 width=192 border=0>
                          <TR>
                            <TD style="HEIGHT: 1px">
<asp:CheckBox id=CBEyeb runat="server" Font-Size="Smaller" AutoPostBack="True" Text="Eye Bracket"></asp:CheckBox></TD></TR>
                          <TR>
                            <TD>
<asp:DropDownList id=DDLEyeb runat="server" Width="190px" Font-Size="XX-Small" AutoPostBack="True"></asp:DropDownList></TD></TR>
                          <TR>
                            <TD></TD></TR></TABLE></TD>
                      <TD align=right width=110>
<asp:Image id=ImgEyeb runat="server" ImageUrl="accessories\eyebracket.jpg"></asp:Image></TD></TR></TABLE>
												</asp:Panel></TD>
											<TD>
												<asp:Panel id="PanelClevisBracket" runat="server" Enabled="False">
                  <TABLE id=Table15 cellSpacing=0 cellPadding=0 width=300 
                  border=0>
                    <TR>
                      <TD style="WIDTH: 251px">
                        <TABLE id=Table18 style="WIDTH: 192px; HEIGHT: 91px" 
                        cellSpacing=0 cellPadding=0 width=192 border=0>
                          <TR>
                            <TD style="HEIGHT: 5px">
<asp:CheckBox id=CBClevisbracket runat="server" Font-Size="Smaller" AutoPostBack="True" Text="Clevis Bracket"></asp:CheckBox></TD></TR>
                          <TR>
                            <TD>
<asp:DropDownList id=DDLClevisbracket runat="server" Width="190px" Font-Size="XX-Small" AutoPostBack="True"></asp:DropDownList></TD></TR>
                          <TR>
                            <TD></TD></TR></TABLE></TD>
                      <TD align=right width=110>
<asp:Image id=ImgClevisbracket runat="server" ImageUrl="accessories\clevisbracket.jpg"></asp:Image></TD></TR></TABLE>
												</asp:Panel></TD>
											<TD>
												<asp:Panel id="PanelLinearCoupler" runat="server" Enabled="False">
                  <TABLE id=Table9 cellSpacing=0 cellPadding=0 width=300 
                  border=0>
                    <TR>
                      <TD style="WIDTH: 251px">
                        <TABLE id=Table10 style="WIDTH: 192px; HEIGHT: 91px" 
                        cellSpacing=0 cellPadding=0 width=192 border=0>
                          <TR>
                            <TD style="HEIGHT: 8px">
<asp:CheckBox id=CBLinearcoupler runat="server" Font-Size="Smaller" AutoPostBack="True" Text="Linear Alignment Coupler"></asp:CheckBox></TD></TR>
                          <TR>
                            <TD>
<asp:DropDownList id=DDLLinearcoupler runat="server" Width="190px" Font-Size="XX-Small" AutoPostBack="True"></asp:DropDownList></TD></TR>
                          <TR>
                            <TD></TD></TR></TABLE></TD>
                      <TD align=right width=110>
<asp:Image id=ImgLinearcoupler runat="server" ImageUrl="accessories\linearcoupler.jpg"></asp:Image></TD></TR></TABLE>
												</asp:Panel></TD>
										</TR>
										<TR>
											<TD>
												<asp:Panel id="PanelSphericalRodEye" runat="server" Enabled="False">
                  <TABLE id=Table19 cellSpacing=0 cellPadding=0 width=300 
                  border=0>
                    <TR>
                      <TD style="WIDTH: 251px">
                        <TABLE id=Table20 style="WIDTH: 192px; HEIGHT: 91px" 
                        cellSpacing=0 cellPadding=0 width=192 border=0>
                          <TR>
                            <TD style="HEIGHT: 13px">
<asp:CheckBox id=CBSphericalrodeye runat="server" Font-Size="Smaller" AutoPostBack="True" Text="Spherical Rod Eye"></asp:CheckBox></TD></TR>
                          <TR>
                            <TD>
<asp:DropDownList id=DDLSphericalrodeye runat="server" Width="190px" Font-Size="XX-Small" AutoPostBack="True"></asp:DropDownList></TD></TR>
                          <TR>
                            <TD></TD></TR></TABLE></TD>
                      <TD align=right width=110>
<asp:Image id=ImgSphericalrodeye runat="server" ImageUrl="accessories\sphericalrodeye.jpg"></asp:Image></TD></TR></TABLE>
												</asp:Panel></TD>
											<TD>
												<asp:Panel id="PanelSphericalClevisBracket" runat="server" Enabled="False">
                  <TABLE id=Table8 cellSpacing=0 cellPadding=0 width=300 
                  border=0>
                    <TR>
                      <TD style="WIDTH: 251px">
                        <TABLE id=Table11 style="WIDTH: 192px; HEIGHT: 91px" 
                        cellSpacing=0 cellPadding=0 width=192 border=0>
                          <TR>
                            <TD style="HEIGHT: 10px">
<asp:CheckBox id=CBSphericalclevisbracket runat="server" Font-Size="Smaller" AutoPostBack="True" Text="Spherical Clevis Bracket"></asp:CheckBox></TD></TR>
                          <TR>
                            <TD>
<asp:DropDownList id=DDLSphericalclevisbracket runat="server" Width="190px" Font-Size="XX-Small" AutoPostBack="True"></asp:DropDownList></TD></TR>
                          <TR>
                            <TD></TD></TR></TABLE></TD>
                      <TD align=right width=110>
<asp:Image id=ImgSphericalclevisbraket runat="server" ImageUrl="accessories\sphericalclevisbracket.jpg"></asp:Image></TD></TR></TABLE>
												</asp:Panel></TD>
											<TD>
												<asp:Panel id="PanelSphericalPivotpin" runat="server" Enabled="False">
                  <TABLE id=Table5 cellSpacing=0 cellPadding=0 width=300 
                  border=0>
                    <TR>
                      <TD style="WIDTH: 251px">
                        <TABLE id=Table7 style="WIDTH: 192px; HEIGHT: 91px" 
                        cellSpacing=0 cellPadding=0 width=192 border=0>
                          <TR>
                            <TD style="HEIGHT: 18px">
<asp:CheckBox id=CBSpericalpp runat="server" Font-Size="Smaller" AutoPostBack="True" Text="Spherical Pivot Pin"></asp:CheckBox></TD></TR>
                          <TR>
                            <TD>
<asp:DropDownList id=DDLSphericalpp runat="server" Width="190px" Font-Size="XX-Small" AutoPostBack="True"></asp:DropDownList></TD></TR>
                          <TR>
                            <TD></TD></TR></TABLE></TD>
                      <TD align=right width=110>
<asp:Image id=ImgSphericalpp runat="server" ImageUrl="accessories\sphericalpivotpin.jpg"></asp:Image></TD></TR></TABLE>
												</asp:Panel></TD>
										</TR>
									</TABLE>
									<asp:Label id="lblinfo" runat="server" Font-Size="Smaller" ForeColor="Red"></asp:Label>
								</TD>
							</TR>
						</TABLE>
						<asp:Label id="lblkk" runat="server" Visible="False"></asp:Label>
						<asp:Label id="lblmalefemale" runat="server" Visible="False"></asp:Label></TD>
					<TD width="50"></TD>
				</TR>
				<TR>
					<TD align="right" width="50" height="30">
						<asp:LinkButton id="LBHome" runat="server" Font-Size="Smaller" Width="1px" Font-Bold="True">Back</asp:LinkButton></TD>
					<TD align="center" bgColor="#dcdcdc" height="30">
						<asp:Label id="Label2" runat="server" Font-Size="Smaller" ForeColor="#404040"> Select the cap end accessories for your cylinder and click next.</asp:Label></TD>
					<TD width="50" height="30">
						<asp:LinkButton id="LBNext" runat="server" Font-Size="Smaller" Width="1px" Font-Bold="True">Next</asp:LinkButton></TD>
				</TR>
				<TR>
					<TD width="50" height="20"></TD>
					<TD align="center" height="20"></TD>
					<TD width="50" height="20"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
