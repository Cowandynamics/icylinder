using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
namespace iCylinderV1
{
	/// <summary>
	/// Summary description for UserLogin.
	/// </summary>
	public class Default : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.TextBox txtUserName;
		protected System.Web.UI.WebControls.RequiredFieldValidator rfvUserName;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.TextBox txtPassword;
		protected System.Web.UI.WebControls.RequiredFieldValidator rfvPassword;
		protected System.Web.UI.WebControls.Label lblMessage;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.LinkButton LBForgotPass;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.Button btnLogin;
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator1;
		DBClass db=new DBClass();
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			Page.RegisterStartupScript("SetInitialFocus", "<script>document.getElementById('" + txtUserName.ClientID + "').focus();</script>");
			
			if (Session["User"]!=null)
			{
				Session.Clear();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
			this.LBForgotPass.Click += new System.EventHandler(this.LBForgotPass_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnLogin_Click(object sender, System.EventArgs e)
		{
			rfvPassword.Enabled =true;
			lblMessage.Text="";
			if(Page.IsValid)
			{
				
				ArrayList list =new ArrayList();
				list=db.SelectUser(txtUserName.Text.ToString().Trim(),txtPassword.Text.ToString().Trim());
				if(list.Count ==1)
				{
					string s=list[0].ToString().Replace("'"," ");
					s.Replace("\r\n"," ");
					lblMessage.Text ="<script language='javascript'> window.alert('"+s+"')</script>";
				}
				else if(list.Count > 1)
				{
					Session["User"]=list;
					DateTime dt=Convert.ToDateTime(list[4]);
					if(dt.AddDays(90) < DateTime.Today)
					{
						Response.Redirect("Profile.aspx");
					}
					else
					{
						//issue #341 start
						DateTime dtUpdatePrice = new DateTime(2015,06,02);
						if(DateTime.Now<dtUpdatePrice)
						{
							string strMessage = "Dear iCylinder User,\\r\\n" +
								"As you are aware, our industry has been challenged with rising costs on all fronts.\\r\\n"+
								"While we have attempted to offset those additional costs to minimize the effect to our customers, we have reached a point where we must pass on a small portion of those increases:\\r\\n"+
								"Effective June 1st there will be a 4% price increase on cylinders and accessories.\\r\\n"+
								"Repair kits will increase by 5%.\\r\\n"+
								"iCylinder will be updated on June 1st. \\r\\n";
//							string sJavaScript = "<script language=javascript>\n";        
//							sJavaScript += "var agree;\n";        
//							sJavaScript += "agree = confirm('"+strMessage+"');\n";        
//							sJavaScript += "if(agree)\n";        
//							sJavaScript += "window.location = \"Welcome.aspx\";\n";        
//							sJavaScript += "</script>";      
//							Response.Write(sJavaScript);
							string sJavaScript = "<script language=javascript>\n";        
							sJavaScript += "alert('"+strMessage+"');\n";        
							sJavaScript += "window.location = \"Welcome.aspx\";\n";        
							sJavaScript += "</script>";      
							Response.Write(sJavaScript);
						}
						else
						{
							Response.Redirect("Welcome.aspx");
						}
					}
				}
				else
				{
					rfvPassword.Enabled =false;
					lblMessage.Text="<script language='javascript'> " + Environment.NewLine +" window.alert('Invalid Login Try again !')</script>";
				}
			}
		}

		private void LBForgotPass_Click(object sender, System.EventArgs e)
		{
			rfvPassword.Enabled =false;
			if(txtUserName.Text.Trim() !="" )
			{
				lblMessage.Text="";
				ArrayList elist=new ArrayList();
				elist=db.SelectUser_ValidEmail(txtUserName.Text.Trim());
				if(elist.Count >=1)
				{
					string guidResult = System.Guid.NewGuid().ToString();
					guidResult = guidResult.Replace("-", string.Empty);
					guidResult=guidResult.Substring(0,6);
					string s =db.ResetPassword(guidResult.Trim(),txtUserName.Text.Trim());
						if(s.Trim() =="1")
						{
							MailMessage mail=new MailMessage();
							SmtpMail.SmtpServer ="k2smtpout.secureserver.net"; 
							//mail.From= "admin@cowandynamics.com";
							mail.From= "admin@cowandynamics.com";
							mail.To =txtUserName.Text.Trim();
							mail.Bcc="admin@cowandynamics.com";
							mail.Cc="jbehara@cowandynamics.com";
							mail.BodyFormat =MailFormat.Html;
							mail.Subject=  "Temporary password for i-Cylinder";
							mail.Priority =MailPriority.High;
							mail.Body ="Hi,<Br> Your temporary password is -<br>"+guidResult.Trim()+"<Br>Thanks<br> Admin";
							//issue #582 start
							mail.Body += csSignature.Get_Admin();
							//issue #582 end
							SmtpMail.Send(mail);
//							Message message = new Message();
//							message.From.Email = "admin@cowandynamics.com";
//							message.To.Add( txtUserName.Text.Trim());
//							message.Bcc.Add( "jbehara@cowandynamics.com" );
//							message.Bcc.Add( "admin@cowandynamics.com" );
//							message.Subject ="Temporary password for i-Cylinder";
//							message.Charset = System.Text.Encoding.GetEncoding("iso-8859-7");
//							message.Priority =Priority.Highest;
//							message.BodyHtml ="Hi,<Br> Your temporary password is -<br>"+guidResult.Trim()+"<Br>Thanks<br> Admin";
//							Smtp.Send( message, "smtpout.secureserver.net", 80, GetDomain( message.From.Email ),SmtpAuthentication.Login,"admin@cowandynamics.com","hockey13" );
							lblMessage.Text="New password is emailed to you";
						}
						else
						{
							lblMessage.Text="Error try again later!!";
						}
				}
				else
				{
					lblMessage.Text="You are not a valid user!!";
				}
			}
			else
			{
				lblMessage.Text="Please enter user name!!";
			}
		}
		private string GetDomain( string email )
		{
			int index = email.IndexOf( '@' );
			return email.Substring( index + 1 );
		}
	}
}
