using System;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for Others.
	/// </summary>
	public class Others
	{
		public Others()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		private string ids;
		private string code;
		private string desc;
		private string desc1;
		private string desc2;
		private string desc3;
		private string qty;
		private string dicount;
		private string unitp;
		private string price;
		public string Ids
		{
			get
			{
				return ids ;
			}
			set
			{
				ids  = value;
			}
		}

		public string Code
		{
			get
			{
				return code;
			}
			set
			{
				code = value;
			}
		}

		public string Desc
		{
			get
			{
				return desc;
			}
			set
			{
				desc = value;
			}
		}
		public string Desc1
		{
			get
			{
				return desc1;
			}
			set
			{
				desc1 = value;
			}
		}
		public string Desc2
		{
			get
			{
				return desc2;
			}
			set
			{
				desc2 = value;
			}
		}
		public string Desc3
		{
			get
			{
				return desc3;
			}
			set
			{
				desc3 = value;
			}
		}
		public string Qty
		{
			get
			{
				return qty;
			}
			set
			{
				qty = value;
			}
		}
		public string UnitP
		{
			get
			{
				return unitp;
			}
			set
			{
				unitp = value;
			}
		}
		public string Discount
		{
			get
			{
				return dicount;
			}
			set
			{
				dicount = value;
			}
		}
		public string Price
		{
			get
			{
				return price;
			}
			set
			{
				price = value;
			}
		}
	}
}
