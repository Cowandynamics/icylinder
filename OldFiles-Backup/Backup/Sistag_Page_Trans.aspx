<%@ Page language="c#" Codebehind="Sistag_Page_Trans.aspx.cs" AutoEventWireup="false" Inherits="iCylinderV1.Sistag_Page_Trans" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Icylinder_page_Trans</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 1000px; HEIGHT: 487px" cellSpacing="0" cellPadding="0"
				width="1000" bgColor="#d3d3d3" border="0">
				<TR>
					<TD width="50" height="20"></TD>
					<TD align="center" height="20">
						<asp:label id="Label2" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
							Font-Size="Small" ForeColor="Black">Transducer Options</asp:label></TD>
					<TD width="50" height="20"></TD>
				</TR>
				<TR>
					<TD width="50"></TD>
					<TD bgColor="#dcdcdc">
						<TABLE id="Table2" borderColor="silver" cellSpacing="0" cellPadding="0" width="902" border="1">
							<TR>
								<TD align="center">
									<asp:RadioButtonList id="RBLTrans" runat="server" Font-Size="Smaller" RepeatColumns="3" RepeatDirection="Horizontal"
										AutoPostBack="True"></asp:RadioButtonList></TD>
							</TR>
							<TR>
								<TD align="center" height="20">
									<TABLE id="Table3" cellSpacing="0" cellPadding="0" border="0">
										<TR>
											<TD style="WIDTH: 104px">
												<asp:label id="Label1" runat="server" Font-Size="Smaller">Remote Postioner:</asp:label></TD>
											<TD>
												<asp:radiobuttonlist id="RBLRemote" runat="server" Font-Size="9pt" AutoPostBack="True" RepeatDirection="Horizontal"
													Width="73px" Height="8px">
													<asp:ListItem Value="Y">Yes</asp:ListItem>
													<asp:ListItem Value="N" Selected="True">No</asp:ListItem>
												</asp:radiobuttonlist></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD align="center" height="20">
									<asp:radiobuttonlist id="RBLFittings" runat="server" Font-Size="9pt" Height="16px" RepeatDirection="Horizontal">
										<asp:ListItem Value="S" Selected="True">Stainless Steel fittings &amp; tubing</asp:ListItem>
									</asp:radiobuttonlist></TD>
							</TR>
						</TABLE>
					</TD>
					<TD width="50"></TD>
				</TR>
				<TR>
					<TD align="right" width="50" height="30">
						<asp:LinkButton id="LBBack" runat="server" Font-Size="Smaller" Font-Bold="True">Back</asp:LinkButton></TD>
					<TD align="center" bgColor="#dcdcdc" height="30">
						<asp:label id="lblxi" runat="server" Font-Size="Smaller" Visible="False">Resistance</asp:label>
						<asp:textbox id="Txtxi" runat="server" Font-Size="XX-Small" AutoPostBack="True" Width="30px"
							Visible="False" MaxLength="2">10</asp:textbox>
						<asp:label id="omega" runat="server" BackColor="Transparent" Width="8px" Height="19" Visible="False">&Omega;</asp:label>
						<asp:label id="lbrange" runat="server" ForeColor="Red" Font-Size="Smaller" Visible="False">(5 < range> 99)</asp:label></TD>
					<TD width="50" height="30">
						<asp:LinkButton id="BtnNext" runat="server" Font-Size="Smaller" Font-Bold="True">Next</asp:LinkButton></TD>
				</TR>
				<TR>
					<TD width="50" height="20"></TD>
					<TD align="center" height="20">
						<asp:label id="lblseries" runat="server" BackColor="Transparent" BorderColor="Transparent"
							Font-Size="XX-Small" ForeColor="Red"></asp:label></TD>
					<TD width="50" height="20"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
