using System;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for blCanisterPN.
	/// </summary>
	public class blCanisterPN
	{
		public blCanisterPN()
		{
			//
			// TODO: Add constructor logic here
			//
			eto="";
		    etc="";
		    bto="";
		    canisterno="";
		    canisterboresize="";
		    springrate="";
		    cylinderboreno="";
		    cylinderboresize="";
		    tandem="";
			cylinderrodsize="";
			stroke="";
		    failmode="";
			cylinderseries="";
			cylinderborecode="";
            cylinderborevalue="";
			cylinderrodcode="";
			airpressure="";
			packingfriction="0";
			price="";
			btc="";
		}
		private string eto;
		private string etc;
		private string bto;
		private string canisterno;
		private string canisterboresize;
		private string springrate;
		private string cylinderboreno;
		private string cylinderboresize;
		private string cylinderborevalue;
		private string cylinderborecode;
		private string cylinderrodsize;
		private string cylinderrodcode;
		private string tandem;
		private string stroke;
		private string cylinderseries;
		private string failmode;
        private string preload;
		private string airpressure;
		private string packingfriction;
		private string etcvalvetrust;
		private string price;
		private string btc;
		public string BTC
		{
			get
			{
				return btc;
			}
			set
			{
				btc = value;
			}
		}
		public string Price
		{
			get
			{
				return price;
			}
			set
			{
				price = value;
			}
		}
		public string EtcValveTrust
		{
			get
			{
				return etcvalvetrust;
			}
			set
			{
				etcvalvetrust = value;
			}
		}
		public string PackingFriction
		{
			get
			{
				return packingfriction;
			}
			set
			{
				packingfriction = value;
			}
		}
		public string AirPressure
		{
			get
			{
				return airpressure;
			}
			set
			{
				airpressure = value;
			}
		}
		public string Preload
		{
			get
			{
				return preload;
			}
			set
			{
				preload = value;
			}
		}
		public string CylinderBoreValue
		{
			get
			{
				return cylinderborevalue;
			}
			set
			{
				cylinderborevalue = value;
			}
		}
		public string BTO
		{
			get
			{
				return bto;
			}
			set
			{
				bto = value;
			}
		}
		public string CylinderBoreCode
		{
			get
			{
				return cylinderborecode;
			}
			set
			{
				cylinderborecode = value;
			}
		}
		public string CylinderRodCode
		 {
			 get
			 {
				 return cylinderrodcode;
			 }
			 set
			 {
				 cylinderrodcode = value;
			 }
		 }
		public string CylinderSeries
		{
			get
			{
				return cylinderseries;
			}
			set
			{
				cylinderseries = value;
			}
		}
		public string Stroke
		{
			get
			{
				return stroke;
			}
			set
			{
				stroke = value;
			}
		}
		public string CylinderRodSize
		{
			get
			{
				return cylinderrodsize;
			}
			set
			{
				cylinderrodsize = value;
			}
		}
		public string ETO
		{
			get
			{
				return eto;
			}
			set
			{
				eto = value;
			}
		}
		public string ETC
		{
			get
			{
				return etc;
			}
			set
			{
				etc = value;
			}
		}
		public string CanisterNo
		 {
			 get
			 {
				 return canisterno;
			 }
			 set
			 {
				 canisterno = value;
			 }
		 }
		public string CanisterBoreSize
		  {
			  get
			  {
				  return canisterboresize;
			  }
			  set
			  {
				  canisterboresize = value;
			  }
		  }
		public string SpringRate
		   {
			   get
			   {
				   return springrate;
			   }
			   set
			   {
				   springrate = value;
			   }
		   }
		public string CylinderBoreNo
			{
				get
				{
					return cylinderboreno;
				}
				set
				{
					cylinderboreno = value;
				}
			}
		public string CylinderBoreSize
			 {
				 get
				 {
					 return cylinderboresize;
				 }
				 set
				 {
					 cylinderboresize = value;
				 }
			 }
		public string Tandem
			  {
				  get
				  {
					  return tandem;
				  }
				  set
				  {
					  tandem = value;
				  }
			  }
		public string FailMode
			   {
				   get
				   {
					   return failmode;
				   }
				   set
				   {
					   failmode = value;
				   }
			   }
		
		
	}
}
