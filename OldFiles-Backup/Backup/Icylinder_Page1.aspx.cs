using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net;
using System.Security.Principal;
namespace iCylinderV1
{
	/// <summary>
	/// Summary description for Icylinder_Page1.
	/// </summary>
	public class Icylinder_Page1 : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.LinkButton BtnNext;
		protected System.Web.UI.WebControls.Label lblhidden;
		protected System.Web.UI.WebControls.Label lblstroke;
		protected System.Web.UI.WebControls.RadioButtonList RBLDoubleOrSingle;
		protected System.Web.UI.WebControls.RadioButtonList RBLSeries;
		protected System.Web.UI.WebControls.Label Label2;
		DBClass db=new DBClass();
		protected System.Web.UI.WebControls.LinkButton LBHome;
		protected System.Web.UI.WebControls.LinkButton LBBatchQuote;
		protected System.Web.UI.WebControls.LinkButton LBBatchQuote_AS; 
		public coder cd1=new coder();
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["User"] !=null)
			{
				if(! IsPostBack)
				{
					if(Session["Coder"] !=null)
					{
						Session["Coder"] =null;
					}
					ArrayList blist1=new ArrayList();
					ArrayList blist2=new ArrayList();
					ArrayList selist =new ArrayList();
					selist =(ArrayList)Session["User"];
					if(selist[6].ToString().Trim() =="1018" )
					{
						Response.Redirect("SVC_Summit_BatchQuote.aspx");
					}
					
					//issue #118 start
					//backup
					//if(selist[6].ToString().Trim() =="1002" || selist[6].ToString().Trim() =="1015" || selist[6].ToString().Trim() =="1018" )
					if(selist[6].ToString().Trim() =="1002" || selist[6].ToString().Trim() =="1015" || selist[6].ToString().Trim() =="1018" || selist[6].ToString().Trim() =="998" )
					//update
//					if(selist[6].ToString().Trim() =="1002" || selist[6].ToString().Trim() =="1015" || selist[6].ToString().Trim() =="1018" || selist[6].ToString().Trim() =="1021" )
					//issue #118 end
					{
						LBBatchQuote.Visible=true;
					}
					else
					{
						LBBatchQuote.Visible=false;
					}
					//issue #215 start
					if(selist[6].ToString().Trim() =="1015" || selist[6].ToString().Trim() =="998")
					{
						LBBatchQuote_AS.Visible=true;
					}
					else
					{
						LBBatchQuote_AS.Visible=false;
					}
					//issue #215 end
//					ArrayList clist =new ArrayList();
//					clist = db.SelectContactName(selist[5].ToString()); 
					ArrayList list = new ArrayList();
					list=db.SelectSeries_WithCode();
					blist1 =(ArrayList)list[0];
					blist2 =(ArrayList)list[1];
					ArrayList slist =new ArrayList();
					//umi start
					string company = selist[6].ToString().Trim();
					string user = selist[5].ToString().Trim();
					//umi end
					slist=db.SelectAvailableSeries_details(selist[6].ToString().Trim(),selist[5].ToString().Trim());
					ListItem litem=new ListItem();
					for(int k=0;k <blist1.Count;k++)
					{
						string strtest = slist[k].ToString() + blist2[k].ToString().Trim() + k;
						if(slist[k].ToString() !="")
						{
							//issue #118 start
							//backup
							//if((selist[6].ToString().Trim() =="1002" || selist[6].ToString().Trim() =="1015" || selist[6].ToString().Trim() =="1018") && blist2[k].ToString().Trim()=="L - Hydraulic 3000 PSI Servo Cylinder")
							//update
							if((selist[6].ToString().Trim() =="1002" || selist[6].ToString().Trim() =="1015" || selist[6].ToString().Trim() =="1018" || selist[6].ToString().Trim() =="1021") && blist2[k].ToString().Trim()=="L - Hydraulic 3000 PSI Servo Cylinder")
							//issue #118 end
							{
								litem =new ListItem((String.Format("Series L - with Probe<br /><img  align='middle' alt='Series L - with Probe' src='{0}'>", "images\\"+blist1[k].ToString().Trim()+".jpg")), blist1[k].ToString().Trim());
							}
							else
							{
								litem =new ListItem((String.Format(blist2[k].ToString().Trim()+"<br /><img  align='middle' alt='"+blist2[k].ToString().Trim() +"' src='{0}'>", "images\\"+blist1[k].ToString().Trim()+".jpg")), blist1[k].ToString().Trim());
							}
							RBLSeries.Items.Add(litem);
						}
					}
					RBLSeries.SelectedIndex =0;
					if(Session["Coder"] !=null)
					{
						cd1=(coder)Session["Coder"];
						if(cd1.Series !=null && cd1.Series !="")
						{
							RBLSeries.SelectedValue = cd1.Series.Trim(); 
						}
						if(cd1.DoubleRod !=null && cd1.DoubleRod !="")
						{
							if(cd1.DoubleRod =="Yes")
							{
								RBLDoubleOrSingle.SelectedIndex =1;
							}
							else
							{
								RBLDoubleOrSingle.SelectedIndex =0;
							}
						}
					}
					
				}
			}
			else
			{
				Response.Redirect("Login.aspx");
			}

		}

		private string IpAddress() 
		{ 
			string strIpAddress; 
			strIpAddress = Request.ServerVariables[ "HTTP_X_FORWARDED_FOR" ]; 
			if (strIpAddress == null ) 
				strIpAddress = Request.ServerVariables[ "REMOTE_ADDR" ]; 
			return strIpAddress; 

		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.LBBatchQuote.Click += new System.EventHandler(this.LBBatchQuote_Click);
			this.LBBatchQuote_AS.Click += new System.EventHandler(this.LBBatchQuote_AS_Click);
			this.RBLSeries.SelectedIndexChanged += new System.EventHandler(this.RBLSeries_SelectedIndexChanged);
			this.BtnNext.Click += new System.EventHandler(this.BtnNext_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void BtnNext_Click(object sender, System.EventArgs e)
		{

			if(RBLSeries.SelectedIndex >= 0 && RBLDoubleOrSingle.SelectedIndex >=0)
			{
			
				Session["Coder"] =null;
				if(RBLDoubleOrSingle.SelectedIndex == 0)
				{
					cd1.DoubleRod = "No";
				}
				else
				{
					cd1.DoubleRod = "Yes";
				}
				cd1.Series =RBLSeries.SelectedItem.Value.ToString().Trim();
				if(cd1.Series.Trim()=="L")
				{
					cd1.Transducer="TB";
				}
				else
				{
					cd1.Transducer="";
				}
				Session["Coder"] =cd1;
					
				if(cd1.Series.Trim() =="SA")
				{
					Response.Redirect("SlurryfloSeriesA.aspx");
				}
				else if(cd1.Series.Trim() =="AT")
				{
					Response.Redirect("SlurryfloSeriesA.aspx");
				}
				else
				{
					Response.Redirect("Icylinder_page2.aspx");
				}
			}
			else
			{
				lblstroke.Text="Please enter/select all the options";
				lblstroke.Visible=true;
			}
		}

		private void RBLSeries_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(RBLSeries.SelectedValue.Trim() =="SA" ||RBLSeries.SelectedValue.Trim() =="AT")
			{
				RBLDoubleOrSingle.Enabled=false;
				RBLDoubleOrSingle.SelectedIndex=0;
			}
			else
			{
				RBLDoubleOrSingle.Enabled=true;
			}
		}

		private void LBBatchQuote_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("SVC_Summit_BatchQuote.aspx");
		}

		private void LBBatchQuote_AS_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Velan_BachQuote.aspx");
		}
	}
}
