using System;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for CrystalReportClass.
	/// </summary>
	public class CrystalReportClass
	{
		public CrystalReportClass()
		{
		 units="imperial";
		}
		private string quote;
		private string partno;
		private string desc;
		private string qty;
		private string unitprice;
		private string price;
		private string qtybrks1;
		private string qtybrks2;
		private string qtybrks3;
		private string itemprice;
		private string discount;
		private string note;
		private string weight;
		//issue #242 start
		private string units;
		public string Units
		{
			get
			{
				return units ;
			}
			set
			{
				units  = value;
			}
		}//issue #242 end
		public string QuoteNo
		{
			get
			{
				return quote ;
			}
			set
			{
				quote  = value;
			}
		}

		public string PartNo
		{
			get
			{
				return partno;
			}
			set
			{
				partno = value;
			}
		}

		public string Desc
		{
			get
			{
				return desc;
			}
			set
			{
				desc = value;
			}
		}
		public string Qty
		{
			get
			{
				return qty;
			}
			set
			{
				qty = value;
			}
		}
		public string UnitPrice
		{
			get
			{
				return unitprice;
			}
			set
			{
				unitprice = value;
			}
		}
	
		public string Price
		{
			get
			{
				return price;
			}
			set
			{
				price = value;
			}
		}
		public string Qtybreaks_qty
		{
			get
			{
				return qtybrks1;
			}
			set
			{
				qtybrks1 = value;
			}
		}
		public string Qtybreaks_uprice
		{
			get
			{
				return qtybrks2;
			}
			set
			{
				qtybrks2 = value;
			}
		}
		public string Qtybreaks_tprice
		{
			get
			{
				return qtybrks3;
			}
			set
			{
				qtybrks3 = value;
			}
		}
		public string Itemprice
		{
			get
			{
				return itemprice;
			}
			set
			{
				itemprice = value;
			}
		}
		public string Discount
		{
			get
			{
				return discount;
			}
			set
			{
				discount = value;
			}
		}
		public string Note
		{
			get
			{
				return note;
			}
			set
			{
				note = value;
			}
		}
		public string Weight
		{
			get
			{
				return weight;
			}
			set
			{
				weight = value;
			}
		}
	}
	
}
