<%@ Page language="c#" Codebehind="HtmlQuote.aspx.cs" AutoEventWireup="false" Inherits="iCylinderV1.HtmlQuote" uiCulture="en-CB"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>HtmlQuote</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="HtmlQuote" method="post" runat="server">
			<asp:panel id="Panel3" style="Z-INDEX: 102; LEFT: 0px; POSITION: absolute; TOP: 0px" runat="server"
				Width="660px" Height="904px">
				<DIV style="WIDTH: 666px; BORDER-BOTTOM: darkgray 38px solid; POSITION: relative; HEIGHT: 908px"
					ms_positioning="GridLayout">
					<asp:Image id="Image2" style="Z-INDEX: 101; LEFT: 1px; POSITION: absolute; TOP: 2px" runat="server"
						Height="39px" Width="252px" ImageUrl="Cowan_Logo.jpg"></asp:Image>
					<asp:Label id="LblOffice" style="Z-INDEX: 102; LEFT: 13px; POSITION: absolute; TOP: 45px" runat="server"
						Height="38px" Width="202px" Font-Size="XX-Small" Font-Names="Times New Roman" Font-Italic="True"></asp:Label>
					<DIV style="Z-INDEX: 103; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 214px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label33" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Ship To :</asp:label>
						<asp:label id="LblShip1" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="LblShip2" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="LblShip3" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<DIV style="Z-INDEX: 104; LEFT: 397px; WIDTH: 268px; POSITION: absolute; TOP: 38px; HEIGHT: 240px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="LblQno" style="Z-INDEX: 101; LEFT: 97px; POSITION: absolute; TOP: 7px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="LblPBy" style="Z-INDEX: 102; LEFT: 97px; POSITION: absolute; TOP: 73px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="LblEdates" style="Z-INDEX: 103; LEFT: 97px; POSITION: absolute; TOP: 51px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="LblDelivery" style="Z-INDEX: 104; LEFT: 97px; POSITION: absolute; TOP: 139px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblCode" style="Z-INDEX: 105; LEFT: 97px; POSITION: absolute; TOP: 95px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="LblTerms" style="Z-INDEX: 106; LEFT: 97px; POSITION: absolute; TOP: 117px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="LblQdate" style="Z-INDEX: 107; LEFT: 97px; POSITION: absolute; TOP: 29px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblnote" style="Z-INDEX: 108; LEFT: 97px; POSITION: absolute; TOP: 183px; TEXT-ALIGN: center"
							runat="server" Height="49px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="Label42" style="Z-INDEX: 109; LEFT: 6px; POSITION: absolute; TOP: 183px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Note :</asp:label>
						<asp:label id="Label43" style="Z-INDEX: 110; LEFT: 6px; POSITION: absolute; TOP: 161px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Currency :</asp:label>
						<asp:label id="Label44" style="Z-INDEX: 111; LEFT: 6px; POSITION: absolute; TOP: 139px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Delivery :</asp:label>
						<asp:label id="Label45" style="Z-INDEX: 112; LEFT: 6px; POSITION: absolute; TOP: 117px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Terms :</asp:label>
						<asp:label id="Label46" style="Z-INDEX: 113; LEFT: 6px; POSITION: absolute; TOP: 95px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Code :</asp:label>
						<asp:label id="Label47" style="Z-INDEX: 114; LEFT: 6px; POSITION: absolute; TOP: 73px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Prepared By :</asp:label>
						<asp:label id="lblEDate" style="Z-INDEX: 115; LEFT: 6px; POSITION: absolute; TOP: 51px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Expiry Date :</asp:label>
						<asp:label id="Label49" style="Z-INDEX: 116; LEFT: 6px; POSITION: absolute; TOP: 29px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Quote Date :</asp:label>
						<asp:label id="LblCurrency" style="Z-INDEX: 117; LEFT: 97px; POSITION: absolute; TOP: 161px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="Label51" style="Z-INDEX: 118; LEFT: 6px; POSITION: absolute; TOP: 7px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Quote No:</asp:label></DIV>
					<asp:label id="Label21" style="Z-INDEX: 105; LEFT: 397px; POSITION: absolute; TOP: 1px; TEXT-ALIGN: center"
						runat="server" Width="268px" Font-Size="X-Large" Font-Names="Times New Roman" BorderStyle="None"
						Font-Bold="True" BackColor="DarkGray">Price Quotation</asp:label>
					<asp:panel id="Panel4" style="Z-INDEX: 106; LEFT: 0px; POSITION: absolute; TOP: 280px" runat="server"
						Height="482px" Width="665px" BorderWidth="2px" BorderStyle="Solid">
						<asp:Label id="Lblinfo" runat="server" Font-Size="Smaller" Font-Names="Arial"></asp:Label>
					</asp:panel>
					<asp:label id="Label56" style="Z-INDEX: 107; LEFT: 20px; POSITION: absolute; TOP: 872px" runat="server"
						Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">All prices are net.</asp:label>
					<asp:label id="Label57" style="Z-INDEX: 108; LEFT: 20px; POSITION: absolute; TOP: 886px" runat="server"
						Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">F.O.B. Montreal, Quebec. All taxes are extra.</asp:label>
					<asp:label id="Label59" style="Z-INDEX: 109; LEFT: 433px; POSITION: absolute; TOP: 888px" runat="server"
						Width="216px" Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">F.A.B. Montreal, Quebec. Taxes en sus</asp:label>
					<asp:label id="Label58" style="Z-INDEX: 110; LEFT: 552px; POSITION: absolute; TOP: 872px" runat="server"
						Width="97px" Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">Les prix sont net.</asp:label>
					<DIV style="Z-INDEX: 111; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 147px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label29" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Bill To :</asp:label>
						<asp:label id="LblBill1" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="LblBill2" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="LblBill3" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<DIV style="Z-INDEX: 112; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 80px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label25" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Attn To :</asp:label>
						<asp:label id="LblAttn1" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="LblAttn2" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="LblAttn3" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<asp:Label id="LblPage" style="Z-INDEX: 113; LEFT: 301px; POSITION: absolute; TOP: 886px" runat="server"
						Width="52px" Font-Size="Smaller" Font-Names="Times New Roman" BackColor="DarkGray">Page : 1</asp:Label>
					<asp:Panel id="Panel1" style="Z-INDEX: 114; LEFT: 0px; POSITION: absolute; TOP: 760px" runat="server"
						Height="90px" Width="665px" BorderWidth="2px" BorderStyle="Solid">
						<asp:Label id="LblComments" runat="server"></asp:Label>
					</asp:Panel></DIV>
			</asp:panel>&nbsp;
			<asp:panel id="ptotal1" style="Z-INDEX: 101; LEFT: 0px; POSITION: absolute; TOP: 848px" runat="server"
				Width="665px" Height="23px" BorderStyle="Solid" BorderWidth="2px" BackColor="#E0E0E0">
				<asp:Label id="lblt1" style="TEXT-ALIGN: right" runat="server" Width="464px" BackColor="Transparent">Total (CAD) :</asp:Label>
				<asp:Label id="LblTotal" style="TEXT-ALIGN: right" runat="server" Width="192px" BackColor="Transparent"></asp:Label>
			</asp:panel><asp:panel id="page2" style="Z-INDEX: 103; LEFT: 0px; POSITION: absolute; TOP: 912px" runat="server"
				Visible="False">
				<DIV style="WIDTH: 666px; BORDER-BOTTOM: darkgray 38px solid; POSITION: relative; HEIGHT: 908px"
					ms_positioning="GridLayout">
					<asp:Image id="Image1" style="Z-INDEX: 101; LEFT: 1px; POSITION: absolute; TOP: 2px" runat="server"
						Height="39px" Width="252px" ImageUrl="Cowan_Logo.jpg"></asp:Image>
					<asp:Label id="lbloffice2" style="Z-INDEX: 102; LEFT: 13px; POSITION: absolute; TOP: 45px"
						runat="server" Height="38px" Width="202px" Font-Size="XX-Small" Font-Names="Times New Roman"
						Font-Italic="True"></asp:Label>
					<DIV style="Z-INDEX: 103; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 214px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label48" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Ship To :</asp:label>
						<asp:label id="lblship21" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblship22" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="lblship23" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<DIV style="Z-INDEX: 104; LEFT: 397px; WIDTH: 268px; POSITION: absolute; TOP: 38px; HEIGHT: 240px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="lblqno2" style="Z-INDEX: 101; LEFT: 97px; POSITION: absolute; TOP: 7px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblprep2" style="Z-INDEX: 102; LEFT: 97px; POSITION: absolute; TOP: 73px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lbledate2" style="Z-INDEX: 103; LEFT: 97px; POSITION: absolute; TOP: 51px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lbldelivery2" style="Z-INDEX: 104; LEFT: 97px; POSITION: absolute; TOP: 139px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblcode2" style="Z-INDEX: 105; LEFT: 97px; POSITION: absolute; TOP: 95px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblterm2" style="Z-INDEX: 106; LEFT: 97px; POSITION: absolute; TOP: 117px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblqdate2" style="Z-INDEX: 107; LEFT: 97px; POSITION: absolute; TOP: 29px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblnote2" style="Z-INDEX: 108; LEFT: 97px; POSITION: absolute; TOP: 183px; TEXT-ALIGN: center"
							runat="server" Height="49px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="Label28" style="Z-INDEX: 109; LEFT: 6px; POSITION: absolute; TOP: 183px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Note :</asp:label>
						<asp:label id="Label27" style="Z-INDEX: 110; LEFT: 6px; POSITION: absolute; TOP: 161px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Currency :</asp:label>
						<asp:label id="Label26" style="Z-INDEX: 111; LEFT: 6px; POSITION: absolute; TOP: 139px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Delivery :</asp:label>
						<asp:label id="Label24" style="Z-INDEX: 112; LEFT: 6px; POSITION: absolute; TOP: 117px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Terms :</asp:label>
						<asp:label id="Label23" style="Z-INDEX: 113; LEFT: 6px; POSITION: absolute; TOP: 95px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Code :</asp:label>
						<asp:label id="Label22" style="Z-INDEX: 114; LEFT: 6px; POSITION: absolute; TOP: 73px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Prepared By :</asp:label>
						<asp:label id="Label20" style="Z-INDEX: 115; LEFT: 6px; POSITION: absolute; TOP: 51px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Expiry Date :</asp:label>
						<asp:label id="Label19" style="Z-INDEX: 116; LEFT: 6px; POSITION: absolute; TOP: 29px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Quote Date :</asp:label>
						<asp:label id="lblcurrency2" style="Z-INDEX: 117; LEFT: 97px; POSITION: absolute; TOP: 161px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="Label17" style="Z-INDEX: 118; LEFT: 6px; POSITION: absolute; TOP: 7px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Quote No:</asp:label></DIV>
					<asp:label id="Label16" style="Z-INDEX: 105; LEFT: 397px; POSITION: absolute; TOP: 1px; TEXT-ALIGN: center"
						runat="server" Width="268px" Font-Size="X-Large" Font-Names="Times New Roman" BorderStyle="None"
						Font-Bold="True" BackColor="DarkGray">Price Quotation</asp:label>
					<asp:panel id="Panel7" style="Z-INDEX: 106; LEFT: 0px; POSITION: absolute; TOP: 280px" runat="server"
						Height="482px" Width="665px" BorderWidth="2px" BorderStyle="Solid">
						<asp:Label id="lblinfo2" runat="server" Font-Size="Smaller" Font-Names="Arial"></asp:Label>
					</asp:panel>
					<asp:label id="Label14" style="Z-INDEX: 108; LEFT: 20px; POSITION: absolute; TOP: 872px" runat="server"
						Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">All prices are net.</asp:label>
					<asp:label id="Label13" style="Z-INDEX: 109; LEFT: 20px; POSITION: absolute; TOP: 886px" runat="server"
						Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">F.O.B. Montreal, Quebec. All taxes are extra.</asp:label>
					<asp:label id="Label12" style="Z-INDEX: 110; LEFT: 433px; POSITION: absolute; TOP: 888px" runat="server"
						Width="216px" Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">F.A.B. Montreal, Quebec. Taxes en sus</asp:label>
					<asp:label id="Label11" style="Z-INDEX: 111; LEFT: 552px; POSITION: absolute; TOP: 872px" runat="server"
						Width="97px" Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">Les prix sont net.</asp:label>
					<DIV style="Z-INDEX: 112; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 147px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label10" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Bill To :</asp:label>
						<asp:label id="lblbill21" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblbill22" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="lblbill23" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<DIV style="Z-INDEX: 113; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 80px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label6" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Attn To :</asp:label>
						<asp:label id="lblatt21" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblatt22" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="lblatt23" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<asp:Label id="Label2" style="Z-INDEX: 114; LEFT: 301px; POSITION: absolute; TOP: 886px" runat="server"
						Width="52px" Font-Size="Smaller" Font-Names="Times New Roman" BackColor="DarkGray">Page : 2</asp:Label>
					<asp:Panel id="Panel6" style="Z-INDEX: 115; LEFT: 0px; POSITION: absolute; TOP: 760px" runat="server"
						Height="90px" Width="665px" BorderWidth="2px" BorderStyle="Solid">
						<asp:Label id="lblcomment2" runat="server"></asp:Label>
					</asp:Panel>
					<asp:panel id="ptotal2" style="Z-INDEX: 115; LEFT: 0px; POSITION: absolute; TOP: 848px" runat="server"
						Height="23px" Width="665px" BorderWidth="2px" BorderStyle="Solid" BackColor="#E0E0E0">
						<asp:Label id="lblt2" style="TEXT-ALIGN: right" runat="server" Width="464px" BackColor="Transparent">Total (CAD) :</asp:Label>
						<asp:Label id="lbltotalprice2" style="TEXT-ALIGN: right" runat="server" Width="192px" BackColor="Transparent"></asp:Label>
					</asp:panel></DIV>
			</asp:panel><asp:panel id="page3" style="Z-INDEX: 104; LEFT: 0px; POSITION: absolute; TOP: 1824px" runat="server"
				Width="48px" Visible="False">
				<DIV style="WIDTH: 666px; BORDER-BOTTOM: darkgray 38px solid; POSITION: relative; HEIGHT: 908px"
					ms_positioning="GridLayout">
					<asp:Image id="Image3" style="Z-INDEX: 101; LEFT: 1px; POSITION: absolute; TOP: 2px" runat="server"
						Height="39px" Width="252px" ImageUrl="Cowan_Logo.jpg"></asp:Image>
					<asp:Label id="lbloffice3" style="Z-INDEX: 102; LEFT: 13px; POSITION: absolute; TOP: 45px"
						runat="server" Height="38px" Width="202px" Font-Size="XX-Small" Font-Names="Times New Roman"
						Font-Italic="True"></asp:Label>
					<DIV style="Z-INDEX: 103; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 214px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label98" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Ship To :</asp:label>
						<asp:label id="lblship31" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblship32" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="lblship33" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<DIV style="Z-INDEX: 104; LEFT: 397px; WIDTH: 268px; POSITION: absolute; TOP: 38px; HEIGHT: 240px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="lblqno3" style="Z-INDEX: 101; LEFT: 97px; POSITION: absolute; TOP: 7px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblprep3" style="Z-INDEX: 102; LEFT: 97px; POSITION: absolute; TOP: 73px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lbledate3" style="Z-INDEX: 103; LEFT: 97px; POSITION: absolute; TOP: 51px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lbldelivery3" style="Z-INDEX: 104; LEFT: 97px; POSITION: absolute; TOP: 139px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblcode3" style="Z-INDEX: 105; LEFT: 97px; POSITION: absolute; TOP: 95px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblterms3" style="Z-INDEX: 106; LEFT: 97px; POSITION: absolute; TOP: 117px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblqdate3" style="Z-INDEX: 107; LEFT: 97px; POSITION: absolute; TOP: 29px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblnote3" style="Z-INDEX: 108; LEFT: 97px; POSITION: absolute; TOP: 183px; TEXT-ALIGN: center"
							runat="server" Height="49px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="Label86" style="Z-INDEX: 109; LEFT: 6px; POSITION: absolute; TOP: 183px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Note :</asp:label>
						<asp:label id="Label85" style="Z-INDEX: 110; LEFT: 6px; POSITION: absolute; TOP: 161px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Currency :</asp:label>
						<asp:label id="Label84" style="Z-INDEX: 111; LEFT: 6px; POSITION: absolute; TOP: 139px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Delivery :</asp:label>
						<asp:label id="Label83" style="Z-INDEX: 112; LEFT: 6px; POSITION: absolute; TOP: 117px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Terms :</asp:label>
						<asp:label id="Label82" style="Z-INDEX: 113; LEFT: 6px; POSITION: absolute; TOP: 95px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Code :</asp:label>
						<asp:label id="Label81" style="Z-INDEX: 114; LEFT: 6px; POSITION: absolute; TOP: 73px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Prepared By :</asp:label>
						<asp:label id="Label80" style="Z-INDEX: 115; LEFT: 6px; POSITION: absolute; TOP: 51px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Expiry Date :</asp:label>
						<asp:label id="Label79" style="Z-INDEX: 116; LEFT: 6px; POSITION: absolute; TOP: 29px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Quote Date :</asp:label>
						<asp:label id="lblcurrency3" style="Z-INDEX: 117; LEFT: 97px; POSITION: absolute; TOP: 161px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="Label77" style="Z-INDEX: 118; LEFT: 6px; POSITION: absolute; TOP: 7px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Quote No:</asp:label></DIV>
					<asp:label id="Label76" style="Z-INDEX: 105; LEFT: 397px; POSITION: absolute; TOP: 1px; TEXT-ALIGN: center"
						runat="server" Width="268px" Font-Size="X-Large" Font-Names="Times New Roman" BorderStyle="None"
						Font-Bold="True" BackColor="DarkGray">Price Quotation</asp:label>
					<asp:panel id="Panel12" style="Z-INDEX: 106; LEFT: 0px; POSITION: absolute; TOP: 280px" runat="server"
						Height="482px" Width="665px" BorderWidth="2px" BorderStyle="Solid">
						<asp:Label id="lblinfo3" runat="server" Font-Size="Smaller" Font-Names="Arial"></asp:Label>
					</asp:panel>
					<asp:label id="Label74" style="Z-INDEX: 108; LEFT: 20px; POSITION: absolute; TOP: 872px" runat="server"
						Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">All prices are net.</asp:label>
					<asp:label id="Label73" style="Z-INDEX: 109; LEFT: 20px; POSITION: absolute; TOP: 886px" runat="server"
						Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">F.O.B. Montreal, Quebec. All taxes are extra.</asp:label>
					<asp:label id="Label72" style="Z-INDEX: 110; LEFT: 433px; POSITION: absolute; TOP: 888px" runat="server"
						Width="216px" Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">F.A.B. Montreal, Quebec. Taxes en sus</asp:label>
					<asp:label id="Label71" style="Z-INDEX: 111; LEFT: 552px; POSITION: absolute; TOP: 872px" runat="server"
						Width="97px" Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">Les prix sont net.</asp:label>
					<DIV style="Z-INDEX: 112; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 147px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label70" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Bill To :</asp:label>
						<asp:label id="lblbill31" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblbill32" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="lblbill33" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<DIV style="Z-INDEX: 113; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 80px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label66" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Attn To :</asp:label>
						<asp:label id="lblatt31" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblatt32" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="lblatt33" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<asp:Label id="Label62" style="Z-INDEX: 114; LEFT: 301px; POSITION: absolute; TOP: 886px" runat="server"
						Width="52px" Font-Size="Smaller" Font-Names="Times New Roman" BackColor="DarkGray">Page : 3</asp:Label>
					<asp:Panel id="Panel11" style="Z-INDEX: 115; LEFT: 0px; POSITION: absolute; TOP: 760px" runat="server"
						Height="90px" Width="665px" BorderWidth="2px" BorderStyle="Solid">
						<asp:Label id="lblcomment3" runat="server"></asp:Label>
					</asp:Panel>
					<asp:panel id="ptotal3" style="Z-INDEX: 115; LEFT: 0px; POSITION: absolute; TOP: 848px" runat="server"
						Height="23px" Width="665px" BorderWidth="2px" BorderStyle="Solid" BackColor="#E0E0E0">
						<asp:Label id="lblt3" style="TEXT-ALIGN: right" runat="server" Width="464px" BackColor="Transparent">Total (CAD) :</asp:Label>
						<asp:Label id="lbltotalprice3" style="TEXT-ALIGN: right" runat="server" Width="192px" BackColor="Transparent"></asp:Label>
					</asp:panel></DIV>
			</asp:panel><asp:panel id="page4" style="Z-INDEX: 105; LEFT: 0px; POSITION: absolute; TOP: 2736px" runat="server"
				Visible="False">
				<DIV style="WIDTH: 666px; BORDER-BOTTOM: darkgray 38px solid; POSITION: relative; HEIGHT: 908px"
					ms_positioning="GridLayout">
					<asp:Image id="Image4" style="Z-INDEX: 101; LEFT: 1px; POSITION: absolute; TOP: 2px" runat="server"
						Height="39px" Width="252px" ImageUrl="Cowan_Logo.jpg"></asp:Image>
					<asp:Label id="lbloffice4" style="Z-INDEX: 102; LEFT: 13px; POSITION: absolute; TOP: 45px"
						runat="server" Height="38px" Width="202px" Font-Size="XX-Small" Font-Names="Times New Roman"
						Font-Italic="True"></asp:Label>
					<DIV style="Z-INDEX: 103; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 214px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label139" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Ship To :</asp:label>
						<asp:label id="llblship41" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="llblship42" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px"
							runat="server" Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="llblship43" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px"
							runat="server" Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label></DIV>
					<DIV style="Z-INDEX: 104; LEFT: 397px; WIDTH: 268px; POSITION: absolute; TOP: 38px; HEIGHT: 240px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="lblqno4" style="Z-INDEX: 101; LEFT: 97px; POSITION: absolute; TOP: 7px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblprep4" style="Z-INDEX: 102; LEFT: 97px; POSITION: absolute; TOP: 73px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lbledate4" style="Z-INDEX: 103; LEFT: 97px; POSITION: absolute; TOP: 51px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lbldelivery4" style="Z-INDEX: 104; LEFT: 97px; POSITION: absolute; TOP: 139px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblcode4" style="Z-INDEX: 105; LEFT: 97px; POSITION: absolute; TOP: 95px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblterm4" style="Z-INDEX: 106; LEFT: 97px; POSITION: absolute; TOP: 117px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblqdate4" style="Z-INDEX: 107; LEFT: 97px; POSITION: absolute; TOP: 29px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblnote4" style="Z-INDEX: 108; LEFT: 97px; POSITION: absolute; TOP: 183px; TEXT-ALIGN: center"
							runat="server" Height="49px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="Label127" style="Z-INDEX: 109; LEFT: 6px; POSITION: absolute; TOP: 183px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Note :</asp:label>
						<asp:label id="Label126" style="Z-INDEX: 110; LEFT: 6px; POSITION: absolute; TOP: 161px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Currency :</asp:label>
						<asp:label id="Label125" style="Z-INDEX: 111; LEFT: 6px; POSITION: absolute; TOP: 139px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Delivery :</asp:label>
						<asp:label id="Label124" style="Z-INDEX: 112; LEFT: 6px; POSITION: absolute; TOP: 117px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Terms :</asp:label>
						<asp:label id="Label123" style="Z-INDEX: 113; LEFT: 6px; POSITION: absolute; TOP: 95px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Code :</asp:label>
						<asp:label id="Label122" style="Z-INDEX: 114; LEFT: 6px; POSITION: absolute; TOP: 73px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Prepared By :</asp:label>
						<asp:label id="Label121" style="Z-INDEX: 115; LEFT: 6px; POSITION: absolute; TOP: 51px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Expiry Date :</asp:label>
						<asp:label id="Label120" style="Z-INDEX: 116; LEFT: 6px; POSITION: absolute; TOP: 29px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Quote Date :</asp:label>
						<asp:label id="lblcurrency4" style="Z-INDEX: 117; LEFT: 97px; POSITION: absolute; TOP: 161px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="Label118" style="Z-INDEX: 118; LEFT: 6px; POSITION: absolute; TOP: 7px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Quote No:</asp:label></DIV>
					<asp:label id="Label117" style="Z-INDEX: 105; LEFT: 397px; POSITION: absolute; TOP: 1px; TEXT-ALIGN: center"
						runat="server" Width="268px" Font-Size="X-Large" Font-Names="Times New Roman" BorderStyle="None"
						Font-Bold="True" BackColor="DarkGray">Price Quotation</asp:label>
					<asp:panel id="Panel16" style="Z-INDEX: 106; LEFT: 0px; POSITION: absolute; TOP: 280px" runat="server"
						Height="482px" Width="665px" BorderWidth="2px" BorderStyle="Solid">
						<asp:Label id="lblinfo4" runat="server" Font-Size="Smaller" Font-Names="Arial"></asp:Label>
					</asp:panel>
					<asp:label id="Label115" style="Z-INDEX: 108; LEFT: 20px; POSITION: absolute; TOP: 872px" runat="server"
						Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">All prices are net.</asp:label>
					<asp:label id="Label114" style="Z-INDEX: 109; LEFT: 20px; POSITION: absolute; TOP: 886px" runat="server"
						Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">F.O.B. Montreal, Quebec. All taxes are extra.</asp:label>
					<asp:label id="Label113" style="Z-INDEX: 110; LEFT: 433px; POSITION: absolute; TOP: 888px"
						runat="server" Width="216px" Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">F.A.B. Montreal, Quebec. Taxes en sus</asp:label>
					<asp:label id="Label112" style="Z-INDEX: 111; LEFT: 552px; POSITION: absolute; TOP: 872px"
						runat="server" Width="97px" Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">Les prix sont net.</asp:label>
					<DIV style="Z-INDEX: 112; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 147px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label111" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Bill To :</asp:label>
						<asp:label id="lblbill41" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblbill42" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="lblbill43" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<DIV style="Z-INDEX: 113; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 80px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label107" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Attn To :</asp:label>
						<asp:label id="lblatt41" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblatt42" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="lblatt43" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<asp:Label id="Label103" style="Z-INDEX: 114; LEFT: 301px; POSITION: absolute; TOP: 886px"
						runat="server" Width="52px" Font-Size="Smaller" Font-Names="Times New Roman" BackColor="DarkGray">Page : 4</asp:Label>
					<asp:Panel id="Panel15" style="Z-INDEX: 115; LEFT: 0px; POSITION: absolute; TOP: 760px" runat="server"
						Height="90px" Width="665px" BorderWidth="2px" BorderStyle="Solid">
						<asp:Label id="lblcomment4" runat="server"></asp:Label>
					</asp:Panel>
					<asp:panel id="ptotal4" style="Z-INDEX: 115; LEFT: 0px; POSITION: absolute; TOP: 848px" runat="server"
						Height="23px" Width="665px" BorderWidth="2px" BorderStyle="Solid" BackColor="#E0E0E0">
						<asp:Label id="lblt4" style="TEXT-ALIGN: right" runat="server" Width="464px" BackColor="Transparent">Total (CAD) :</asp:Label>
						<asp:Label id="lbltotalprice4" style="TEXT-ALIGN: right" runat="server" Width="192px" BackColor="Transparent"></asp:Label>
					</asp:panel></DIV>
			</asp:panel><asp:panel id="page5" style="Z-INDEX: 106; LEFT: 0px; POSITION: absolute; TOP: 3648px" runat="server"
				Visible="False">
				<DIV style="WIDTH: 666px; BORDER-BOTTOM: darkgray 38px solid; POSITION: relative; HEIGHT: 908px"
					ms_positioning="GridLayout">
					<asp:Image id="Image5" style="Z-INDEX: 101; LEFT: 1px; POSITION: absolute; TOP: 2px" runat="server"
						Height="39px" Width="252px" ImageUrl="Cowan_Logo.jpg"></asp:Image>
					<asp:Label id="lbloffice5" style="Z-INDEX: 102; LEFT: 13px; POSITION: absolute; TOP: 45px"
						runat="server" Height="38px" Width="202px" Font-Size="XX-Small" Font-Names="Times New Roman"
						Font-Italic="True"></asp:Label>
					<DIV style="Z-INDEX: 103; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 214px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label180" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Ship To :</asp:label>
						<asp:label id="lblship51" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblship52" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="lblship53" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<DIV style="Z-INDEX: 104; LEFT: 397px; WIDTH: 268px; POSITION: absolute; TOP: 38px; HEIGHT: 240px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="lblqno5" style="Z-INDEX: 101; LEFT: 97px; POSITION: absolute; TOP: 7px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblprep5" style="Z-INDEX: 102; LEFT: 97px; POSITION: absolute; TOP: 73px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lbledate5" style="Z-INDEX: 103; LEFT: 97px; POSITION: absolute; TOP: 51px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lbldelivery5" style="Z-INDEX: 104; LEFT: 97px; POSITION: absolute; TOP: 139px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblcode5" style="Z-INDEX: 105; LEFT: 97px; POSITION: absolute; TOP: 95px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblterm5" style="Z-INDEX: 106; LEFT: 97px; POSITION: absolute; TOP: 117px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblqdate5" style="Z-INDEX: 107; LEFT: 97px; POSITION: absolute; TOP: 29px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblnote5" style="Z-INDEX: 108; LEFT: 97px; POSITION: absolute; TOP: 183px; TEXT-ALIGN: center"
							runat="server" Height="49px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="Label168" style="Z-INDEX: 109; LEFT: 6px; POSITION: absolute; TOP: 183px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Note :</asp:label>
						<asp:label id="Label167" style="Z-INDEX: 110; LEFT: 6px; POSITION: absolute; TOP: 161px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Currency :</asp:label>
						<asp:label id="Label166" style="Z-INDEX: 111; LEFT: 6px; POSITION: absolute; TOP: 139px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Delivery :</asp:label>
						<asp:label id="Label165" style="Z-INDEX: 112; LEFT: 6px; POSITION: absolute; TOP: 117px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Terms :</asp:label>
						<asp:label id="Label164" style="Z-INDEX: 113; LEFT: 6px; POSITION: absolute; TOP: 95px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Code :</asp:label>
						<asp:label id="Label163" style="Z-INDEX: 114; LEFT: 6px; POSITION: absolute; TOP: 73px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Prepared By :</asp:label>
						<asp:label id="Label162" style="Z-INDEX: 115; LEFT: 6px; POSITION: absolute; TOP: 51px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Expiry Date :</asp:label>
						<asp:label id="Label161" style="Z-INDEX: 116; LEFT: 6px; POSITION: absolute; TOP: 29px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Quote Date :</asp:label>
						<asp:label id="lblcurrency5" style="Z-INDEX: 117; LEFT: 97px; POSITION: absolute; TOP: 161px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="Label159" style="Z-INDEX: 118; LEFT: 6px; POSITION: absolute; TOP: 7px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Quote No:</asp:label></DIV>
					<asp:label id="Label158" style="Z-INDEX: 105; LEFT: 397px; POSITION: absolute; TOP: 1px; TEXT-ALIGN: center"
						runat="server" Width="268px" Font-Size="X-Large" Font-Names="Times New Roman" BorderStyle="None"
						Font-Bold="True" BackColor="DarkGray">Price Quotation</asp:label>
					<asp:panel id="Panel20" style="Z-INDEX: 106; LEFT: 0px; POSITION: absolute; TOP: 280px" runat="server"
						Height="482px" Width="665px" BorderWidth="2px" BorderStyle="Solid">
						<asp:Label id="lblinfo5" runat="server" Font-Size="Smaller" Font-Names="Arial"></asp:Label>
					</asp:panel>
					<asp:label id="Label156" style="Z-INDEX: 108; LEFT: 20px; POSITION: absolute; TOP: 872px" runat="server"
						Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">All prices are net.</asp:label>
					<asp:label id="Label155" style="Z-INDEX: 109; LEFT: 20px; POSITION: absolute; TOP: 886px" runat="server"
						Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">F.O.B. Montreal, Quebec. All taxes are extra.</asp:label>
					<asp:label id="Label154" style="Z-INDEX: 110; LEFT: 433px; POSITION: absolute; TOP: 888px"
						runat="server" Width="216px" Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">F.A.B. Montreal, Quebec. Taxes en sus</asp:label>
					<asp:label id="Label153" style="Z-INDEX: 111; LEFT: 552px; POSITION: absolute; TOP: 872px"
						runat="server" Width="97px" Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">Les prix sont net.</asp:label>
					<DIV style="Z-INDEX: 112; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 147px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label152" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Bill To :</asp:label>
						<asp:label id="lblbill51" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblbill52" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="lblbill53" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<DIV style="Z-INDEX: 113; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 80px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label148" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Attn To :</asp:label>
						<asp:label id="lblatt51" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblatt52" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="lblatt53" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<asp:Label id="Label144" style="Z-INDEX: 114; LEFT: 301px; POSITION: absolute; TOP: 886px"
						runat="server" Width="52px" Font-Size="Smaller" Font-Names="Times New Roman" BackColor="DarkGray">Page : 5</asp:Label>
					<asp:Panel id="Panel19" style="Z-INDEX: 115; LEFT: 0px; POSITION: absolute; TOP: 760px" runat="server"
						Height="90px" Width="665px" BorderWidth="2px" BorderStyle="Solid">
						<asp:Label id="lblcomment5" runat="server"></asp:Label>
					</asp:Panel>
					<asp:panel id="ptotal5" style="Z-INDEX: 115; LEFT: 0px; POSITION: absolute; TOP: 848px" runat="server"
						Height="23px" Width="665px" BorderWidth="2px" BorderStyle="Solid" BackColor="#E0E0E0">
						<asp:Label id="lblt5" style="TEXT-ALIGN: right" runat="server" Width="464px" BackColor="Transparent">Total (CAD) :</asp:Label>
						<asp:Label id="lbltotalprice5" style="TEXT-ALIGN: right" runat="server" Width="192px" BackColor="Transparent"></asp:Label>
					</asp:panel></DIV>
			</asp:panel><asp:panel id="page6" style="Z-INDEX: 107; LEFT: 0px; POSITION: absolute; TOP: 4560px" runat="server"
				Visible="False">
				<DIV style="WIDTH: 666px; BORDER-BOTTOM: darkgray 38px solid; POSITION: relative; HEIGHT: 908px"
					ms_positioning="GridLayout">
					<asp:Image id="Image6" style="Z-INDEX: 101; LEFT: 1px; POSITION: absolute; TOP: 2px" runat="server"
						Height="39px" Width="252px" ImageUrl="Cowan_Logo.jpg"></asp:Image>
					<asp:Label id="lbloffice6" style="Z-INDEX: 102; LEFT: 13px; POSITION: absolute; TOP: 45px"
						runat="server" Height="38px" Width="202px" Font-Size="XX-Small" Font-Names="Times New Roman"
						Font-Italic="True"></asp:Label>
					<DIV style="Z-INDEX: 103; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 214px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label221" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Ship To :</asp:label>
						<asp:label id="lblship61" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblship62" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="lblship63" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<DIV style="Z-INDEX: 104; LEFT: 397px; WIDTH: 268px; POSITION: absolute; TOP: 38px; HEIGHT: 240px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="lblqno6" style="Z-INDEX: 101; LEFT: 97px; POSITION: absolute; TOP: 7px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblprep6" style="Z-INDEX: 102; LEFT: 97px; POSITION: absolute; TOP: 73px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lbledate6" style="Z-INDEX: 103; LEFT: 97px; POSITION: absolute; TOP: 51px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lbldelivery6" style="Z-INDEX: 104; LEFT: 97px; POSITION: absolute; TOP: 139px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblcode6" style="Z-INDEX: 105; LEFT: 97px; POSITION: absolute; TOP: 95px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblterm6" style="Z-INDEX: 106; LEFT: 97px; POSITION: absolute; TOP: 117px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblqdate6" style="Z-INDEX: 107; LEFT: 97px; POSITION: absolute; TOP: 29px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblnote6" style="Z-INDEX: 108; LEFT: 97px; POSITION: absolute; TOP: 183px; TEXT-ALIGN: center"
							runat="server" Height="49px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="Label209" style="Z-INDEX: 109; LEFT: 6px; POSITION: absolute; TOP: 183px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Note :</asp:label>
						<asp:label id="Label208" style="Z-INDEX: 110; LEFT: 6px; POSITION: absolute; TOP: 161px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Currency :</asp:label>
						<asp:label id="Label207" style="Z-INDEX: 111; LEFT: 6px; POSITION: absolute; TOP: 139px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Delivery :</asp:label>
						<asp:label id="Label206" style="Z-INDEX: 112; LEFT: 6px; POSITION: absolute; TOP: 117px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Terms :</asp:label>
						<asp:label id="Label205" style="Z-INDEX: 113; LEFT: 6px; POSITION: absolute; TOP: 95px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Code :</asp:label>
						<asp:label id="Label204" style="Z-INDEX: 114; LEFT: 6px; POSITION: absolute; TOP: 73px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Prepared By :</asp:label>
						<asp:label id="Label203" style="Z-INDEX: 115; LEFT: 6px; POSITION: absolute; TOP: 51px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Expiry Date :</asp:label>
						<asp:label id="Label202" style="Z-INDEX: 116; LEFT: 6px; POSITION: absolute; TOP: 29px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Quote Date :</asp:label>
						<asp:label id="lblcurrency6" style="Z-INDEX: 117; LEFT: 97px; POSITION: absolute; TOP: 161px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="Label200" style="Z-INDEX: 118; LEFT: 6px; POSITION: absolute; TOP: 7px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Quote No:</asp:label></DIV>
					<asp:label id="Label199" style="Z-INDEX: 105; LEFT: 397px; POSITION: absolute; TOP: 1px; TEXT-ALIGN: center"
						runat="server" Width="268px" Font-Size="X-Large" Font-Names="Times New Roman" BorderStyle="None"
						Font-Bold="True" BackColor="DarkGray">Price Quotation</asp:label>
					<asp:panel id="Panel24" style="Z-INDEX: 106; LEFT: 0px; POSITION: absolute; TOP: 280px" runat="server"
						Height="482px" Width="665px" BorderWidth="2px" BorderStyle="Solid">
						<asp:Label id="lblinfo6" runat="server" Font-Size="Smaller" Font-Names="Arial"></asp:Label>
					</asp:panel>
					<asp:label id="Label197" style="Z-INDEX: 108; LEFT: 20px; POSITION: absolute; TOP: 872px" runat="server"
						Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">All prices are net.</asp:label>
					<asp:label id="Label196" style="Z-INDEX: 109; LEFT: 20px; POSITION: absolute; TOP: 886px" runat="server"
						Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">F.O.B. Montreal, Quebec. All taxes are extra.</asp:label>
					<asp:label id="Label195" style="Z-INDEX: 110; LEFT: 433px; POSITION: absolute; TOP: 888px"
						runat="server" Width="216px" Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">F.A.B. Montreal, Quebec. Taxes en sus</asp:label>
					<asp:label id="Label194" style="Z-INDEX: 111; LEFT: 552px; POSITION: absolute; TOP: 872px"
						runat="server" Width="97px" Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">Les prix sont net.</asp:label>
					<DIV style="Z-INDEX: 112; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 147px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label193" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Bill To :</asp:label>
						<asp:label id="lblbill61" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblbill62" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="lblbill63" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<DIV style="Z-INDEX: 113; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 80px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label189" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Attn To :</asp:label>
						<asp:label id="lblatt61" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblatt62" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="lblatt63" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<asp:Label id="Label185" style="Z-INDEX: 114; LEFT: 301px; POSITION: absolute; TOP: 886px"
						runat="server" Width="52px" Font-Size="Smaller" Font-Names="Times New Roman" BackColor="DarkGray">Page : 6</asp:Label>
					<asp:Panel id="Panel23" style="Z-INDEX: 115; LEFT: 0px; POSITION: absolute; TOP: 760px" runat="server"
						Height="90px" Width="665px" BorderWidth="2px" BorderStyle="Solid">
						<asp:Label id="lblcomment6" runat="server"></asp:Label>
					</asp:Panel>
					<asp:panel id="ptotal6" style="Z-INDEX: 115; LEFT: 0px; POSITION: absolute; TOP: 848px" runat="server"
						Height="23px" Width="665px" BorderWidth="2px" BorderStyle="Solid" BackColor="#E0E0E0">
						<asp:Label id="lblt6" style="TEXT-ALIGN: right" runat="server" Width="464px" BackColor="Transparent">Total (CAD) :</asp:Label>
						<asp:Label id="lbltotalprice6" style="TEXT-ALIGN: right" runat="server" Width="192px" BackColor="Transparent"></asp:Label>
					</asp:panel></DIV>
			</asp:panel><asp:panel id="page7" style="Z-INDEX: 108; LEFT: 0px; POSITION: absolute; TOP: 5472px" runat="server"
				Visible="False">
				<DIV style="WIDTH: 666px; BORDER-BOTTOM: darkgray 38px solid; POSITION: relative; HEIGHT: 908px"
					ms_positioning="GridLayout">
					<asp:Image id="Image7" style="Z-INDEX: 101; LEFT: 1px; POSITION: absolute; TOP: 2px" runat="server"
						Height="39px" Width="252px" ImageUrl="Cowan_Logo.jpg"></asp:Image>
					<asp:Label id="lbloffice7" style="Z-INDEX: 102; LEFT: 13px; POSITION: absolute; TOP: 45px"
						runat="server" Height="38px" Width="202px" Font-Size="XX-Small" Font-Names="Times New Roman"
						Font-Italic="True"></asp:Label>
					<DIV style="Z-INDEX: 103; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 214px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label91" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Ship To :</asp:label>
						<asp:label id="lblship71" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblship72" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="lblship73" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<DIV style="Z-INDEX: 104; LEFT: 397px; WIDTH: 268px; POSITION: absolute; TOP: 38px; HEIGHT: 240px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="lblqno7" style="Z-INDEX: 101; LEFT: 97px; POSITION: absolute; TOP: 7px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblprep7" style="Z-INDEX: 102; LEFT: 97px; POSITION: absolute; TOP: 73px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lbledate7" style="Z-INDEX: 103; LEFT: 97px; POSITION: absolute; TOP: 51px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lbldelivery7" style="Z-INDEX: 104; LEFT: 97px; POSITION: absolute; TOP: 139px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblcode7" style="Z-INDEX: 105; LEFT: 97px; POSITION: absolute; TOP: 95px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblterms7" style="Z-INDEX: 106; LEFT: 97px; POSITION: absolute; TOP: 117px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblqdate7" style="Z-INDEX: 107; LEFT: 97px; POSITION: absolute; TOP: 29px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblnotes7" style="Z-INDEX: 108; LEFT: 97px; POSITION: absolute; TOP: 183px; TEXT-ALIGN: center"
							runat="server" Height="49px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="Label63" style="Z-INDEX: 109; LEFT: 6px; POSITION: absolute; TOP: 183px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Note :</asp:label>
						<asp:label id="Label61" style="Z-INDEX: 110; LEFT: 6px; POSITION: absolute; TOP: 161px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Currency :</asp:label>
						<asp:label id="Label60" style="Z-INDEX: 111; LEFT: 6px; POSITION: absolute; TOP: 139px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Delivery :</asp:label>
						<asp:label id="Label55" style="Z-INDEX: 112; LEFT: 6px; POSITION: absolute; TOP: 117px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Terms :</asp:label>
						<asp:label id="Label54" style="Z-INDEX: 113; LEFT: 6px; POSITION: absolute; TOP: 95px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Code :</asp:label>
						<asp:label id="Label53" style="Z-INDEX: 114; LEFT: 6px; POSITION: absolute; TOP: 73px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Prepared By :</asp:label>
						<asp:label id="Label52" style="Z-INDEX: 115; LEFT: 6px; POSITION: absolute; TOP: 51px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Expiry Date :</asp:label>
						<asp:label id="Label50" style="Z-INDEX: 116; LEFT: 6px; POSITION: absolute; TOP: 29px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Quote Date :</asp:label>
						<asp:label id="lblcurrency7" style="Z-INDEX: 117; LEFT: 97px; POSITION: absolute; TOP: 161px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="Label40" style="Z-INDEX: 118; LEFT: 6px; POSITION: absolute; TOP: 7px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Quote No:</asp:label></DIV>
					<asp:label id="Label39" style="Z-INDEX: 105; LEFT: 397px; POSITION: absolute; TOP: 1px; TEXT-ALIGN: center"
						runat="server" Width="268px" Font-Size="X-Large" Font-Names="Times New Roman" BorderStyle="None"
						Font-Bold="True" BackColor="DarkGray">Price Quotation</asp:label>
					<asp:panel id="Panel8" style="Z-INDEX: 106; LEFT: 0px; POSITION: absolute; TOP: 280px" runat="server"
						Height="482px" Width="665px" BorderWidth="2px" BorderStyle="Solid">
						<asp:Label id="lblinfo7" runat="server" Font-Size="Smaller" Font-Names="Arial"></asp:Label>
					</asp:panel>
					<asp:label id="Label37" style="Z-INDEX: 108; LEFT: 20px; POSITION: absolute; TOP: 872px" runat="server"
						Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">All prices are net.</asp:label>
					<asp:label id="Label36" style="Z-INDEX: 109; LEFT: 20px; POSITION: absolute; TOP: 886px" runat="server"
						Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">F.O.B. Montreal, Quebec. All taxes are extra.</asp:label>
					<asp:label id="Label35" style="Z-INDEX: 110; LEFT: 433px; POSITION: absolute; TOP: 888px" runat="server"
						Width="216px" Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">F.A.B. Montreal, Quebec. Taxes en sus</asp:label>
					<asp:label id="Label34" style="Z-INDEX: 111; LEFT: 552px; POSITION: absolute; TOP: 872px" runat="server"
						Width="97px" Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">Les prix sont net.</asp:label>
					<DIV style="Z-INDEX: 112; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 147px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label32" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Bill To :</asp:label>
						<asp:label id="lblbill71" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblbill72" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="lblbill73" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<DIV style="Z-INDEX: 113; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 80px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label15" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Attn To :</asp:label>
						<asp:label id="lblatt71" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblatt72" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="lblatt73" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<asp:Label id="Label5" style="Z-INDEX: 114; LEFT: 301px; POSITION: absolute; TOP: 886px" runat="server"
						Width="52px" Font-Size="Smaller" Font-Names="Times New Roman" BackColor="DarkGray">Page : 7</asp:Label>
					<asp:Panel id="Panel5" style="Z-INDEX: 115; LEFT: 0px; POSITION: absolute; TOP: 760px" runat="server"
						Height="90px" Width="665px" BorderWidth="2px" BorderStyle="Solid">
						<asp:Label id="lblcomment7" runat="server"></asp:Label>
					</asp:Panel>
					<asp:panel id="ptotal7" style="Z-INDEX: 115; LEFT: 0px; POSITION: absolute; TOP: 848px" runat="server"
						Height="23px" Width="665px" BorderWidth="2px" BorderStyle="Solid" BackColor="#E0E0E0">
						<asp:Label id="lblt7" style="TEXT-ALIGN: right" runat="server" Width="464px" BackColor="Transparent">Total (CAD) :</asp:Label>
						<asp:Label id="lbltotalprice7" style="TEXT-ALIGN: right" runat="server" Width="192px" BackColor="Transparent"></asp:Label>
					</asp:panel></DIV>
			</asp:panel><asp:panel id="page8" style="Z-INDEX: 110; LEFT: 0px; POSITION: absolute; TOP: 6384px" runat="server"
				Visible="False">
				<DIV style="WIDTH: 666px; BORDER-BOTTOM: darkgray 38px solid; POSITION: relative; HEIGHT: 908px"
					ms_positioning="GridLayout">
					<asp:Image id="Image8" style="Z-INDEX: 101; LEFT: 1px; POSITION: absolute; TOP: 2px" runat="server"
						Height="39px" Width="252px" ImageUrl="Cowan_Logo.jpg"></asp:Image>
					<asp:Label id="lbloffice8" style="Z-INDEX: 102; LEFT: 13px; POSITION: absolute; TOP: 45px"
						runat="server" Height="38px" Width="202px" Font-Size="XX-Small" Font-Names="Times New Roman"
						Font-Italic="True"></asp:Label>
					<DIV style="Z-INDEX: 103; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 214px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label160" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Ship To :</asp:label>
						<asp:label id="lblship81" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblship82" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="lblship83" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<DIV style="Z-INDEX: 104; LEFT: 397px; WIDTH: 268px; POSITION: absolute; TOP: 38px; HEIGHT: 240px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="lblqno8" style="Z-INDEX: 101; LEFT: 97px; POSITION: absolute; TOP: 7px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblprep8" style="Z-INDEX: 102; LEFT: 97px; POSITION: absolute; TOP: 73px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lbledate8" style="Z-INDEX: 103; LEFT: 97px; POSITION: absolute; TOP: 51px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lbldelivery8" style="Z-INDEX: 104; LEFT: 97px; POSITION: absolute; TOP: 139px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblcode8" style="Z-INDEX: 105; LEFT: 97px; POSITION: absolute; TOP: 95px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblterms8" style="Z-INDEX: 106; LEFT: 97px; POSITION: absolute; TOP: 117px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblqdate8" style="Z-INDEX: 107; LEFT: 97px; POSITION: absolute; TOP: 29px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblnotes8" style="Z-INDEX: 108; LEFT: 97px; POSITION: absolute; TOP: 183px; TEXT-ALIGN: center"
							runat="server" Height="49px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="Label138" style="Z-INDEX: 109; LEFT: 6px; POSITION: absolute; TOP: 183px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Note :</asp:label>
						<asp:label id="Label137" style="Z-INDEX: 110; LEFT: 6px; POSITION: absolute; TOP: 161px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Currency :</asp:label>
						<asp:label id="Label136" style="Z-INDEX: 111; LEFT: 6px; POSITION: absolute; TOP: 139px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Delivery :</asp:label>
						<asp:label id="Label135" style="Z-INDEX: 112; LEFT: 6px; POSITION: absolute; TOP: 117px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Terms :</asp:label>
						<asp:label id="Label134" style="Z-INDEX: 113; LEFT: 6px; POSITION: absolute; TOP: 95px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Code :</asp:label>
						<asp:label id="Label133" style="Z-INDEX: 114; LEFT: 6px; POSITION: absolute; TOP: 73px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Prepared By :</asp:label>
						<asp:label id="Label132" style="Z-INDEX: 115; LEFT: 6px; POSITION: absolute; TOP: 51px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Expiry Date :</asp:label>
						<asp:label id="Label131" style="Z-INDEX: 116; LEFT: 6px; POSITION: absolute; TOP: 29px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Quote Date :</asp:label>
						<asp:label id="lblcurrency8" style="Z-INDEX: 117; LEFT: 97px; POSITION: absolute; TOP: 161px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="Label129" style="Z-INDEX: 118; LEFT: 6px; POSITION: absolute; TOP: 7px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Quote No:</asp:label></DIV>
					<asp:label id="Label128" style="Z-INDEX: 105; LEFT: 397px; POSITION: absolute; TOP: 1px; TEXT-ALIGN: center"
						runat="server" Width="268px" Font-Size="X-Large" Font-Names="Times New Roman" BorderStyle="None"
						Font-Bold="True" BackColor="DarkGray">Price Quotation</asp:label>
					<asp:panel id="Panel14" style="Z-INDEX: 106; LEFT: 0px; POSITION: absolute; TOP: 280px" runat="server"
						Height="482px" Width="665px" BorderWidth="2px" BorderStyle="Solid">
						<asp:Label id="lblinfo8" runat="server" Font-Size="Smaller" Font-Names="Arial"></asp:Label>
					</asp:panel>
					<asp:label id="Label116" style="Z-INDEX: 108; LEFT: 20px; POSITION: absolute; TOP: 872px" runat="server"
						Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">All prices are net.</asp:label>
					<asp:label id="Label110" style="Z-INDEX: 109; LEFT: 20px; POSITION: absolute; TOP: 886px" runat="server"
						Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">F.O.B. Montreal, Quebec. All taxes are extra.</asp:label>
					<asp:label id="Label109" style="Z-INDEX: 110; LEFT: 433px; POSITION: absolute; TOP: 888px"
						runat="server" Width="216px" Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">F.A.B. Montreal, Quebec. Taxes en sus</asp:label>
					<asp:label id="Label108" style="Z-INDEX: 111; LEFT: 552px; POSITION: absolute; TOP: 872px"
						runat="server" Width="97px" Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">Les prix sont net.</asp:label>
					<DIV style="Z-INDEX: 112; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 147px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label106" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Bill To :</asp:label>
						<asp:label id="lblbill81" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblbill82" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="lblbill83" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<DIV style="Z-INDEX: 113; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 80px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label101" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Attn To :</asp:label>
						<asp:label id="lblatt81" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblatt82" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="lblatt83" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<asp:Label id="Label96" style="Z-INDEX: 114; LEFT: 301px; POSITION: absolute; TOP: 886px" runat="server"
						Width="52px" Font-Size="Smaller" Font-Names="Times New Roman" BackColor="DarkGray">Page : 8</asp:Label>
					<asp:Panel id="Panel13" style="Z-INDEX: 115; LEFT: 0px; POSITION: absolute; TOP: 760px" runat="server"
						Height="90px" Width="665px" BorderWidth="2px" BorderStyle="Solid">
						<asp:Label id="lblcomments8" runat="server"></asp:Label>
					</asp:Panel>
					<asp:panel id="ptotal8" style="Z-INDEX: 115; LEFT: 0px; POSITION: absolute; TOP: 848px" runat="server"
						Height="23px" Width="665px" BorderWidth="2px" BorderStyle="Solid" BackColor="#E0E0E0">
						<asp:Label id="lblt8" style="TEXT-ALIGN: right" runat="server" Width="464px" BackColor="Transparent">Total (CAD) :</asp:Label>
						<asp:Label id="lbltotalprice8" style="TEXT-ALIGN: right" runat="server" Width="192px" BackColor="Transparent"></asp:Label>
					</asp:panel></DIV>
			</asp:panel><asp:panel id="page9" style="Z-INDEX: 111; LEFT: 0px; POSITION: absolute; TOP: 7296px" runat="server"
				Visible="False">
				<DIV style="WIDTH: 666px; BORDER-BOTTOM: darkgray 38px solid; POSITION: relative; HEIGHT: 908px"
					ms_positioning="GridLayout">
					<asp:Image id="Image9" style="Z-INDEX: 101; LEFT: 1px; POSITION: absolute; TOP: 2px" runat="server"
						Height="39px" Width="252px" ImageUrl="Cowan_Logo.jpg"></asp:Image>
					<asp:Label id="lbloffice9" style="Z-INDEX: 102; LEFT: 13px; POSITION: absolute; TOP: 45px"
						runat="server" Height="38px" Width="202px" Font-Size="XX-Small" Font-Names="Times New Roman"
						Font-Italic="True"></asp:Label>
					<DIV style="Z-INDEX: 103; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 214px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label228" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Ship To :</asp:label>
						<asp:label id="lblship91" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblship92" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="lblship93" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<DIV style="Z-INDEX: 104; LEFT: 397px; WIDTH: 268px; POSITION: absolute; TOP: 38px; HEIGHT: 240px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="lblqno91" style="Z-INDEX: 101; LEFT: 97px; POSITION: absolute; TOP: 7px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblprep9" style="Z-INDEX: 102; LEFT: 97px; POSITION: absolute; TOP: 73px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lbledate9" style="Z-INDEX: 103; LEFT: 97px; POSITION: absolute; TOP: 51px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lbldelivery9" style="Z-INDEX: 104; LEFT: 97px; POSITION: absolute; TOP: 139px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblcode9" style="Z-INDEX: 105; LEFT: 97px; POSITION: absolute; TOP: 95px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblterms9" style="Z-INDEX: 106; LEFT: 97px; POSITION: absolute; TOP: 117px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblqdate9" style="Z-INDEX: 107; LEFT: 97px; POSITION: absolute; TOP: 29px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblnotes9" style="Z-INDEX: 108; LEFT: 97px; POSITION: absolute; TOP: 183px; TEXT-ALIGN: center"
							runat="server" Height="49px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="Label215" style="Z-INDEX: 109; LEFT: 6px; POSITION: absolute; TOP: 183px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Note :</asp:label>
						<asp:label id="Label214" style="Z-INDEX: 110; LEFT: 6px; POSITION: absolute; TOP: 161px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Currency :</asp:label>
						<asp:label id="Label213" style="Z-INDEX: 111; LEFT: 6px; POSITION: absolute; TOP: 139px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Delivery :</asp:label>
						<asp:label id="Label212" style="Z-INDEX: 112; LEFT: 6px; POSITION: absolute; TOP: 117px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Terms :</asp:label>
						<asp:label id="Label211" style="Z-INDEX: 113; LEFT: 6px; POSITION: absolute; TOP: 95px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Code :</asp:label>
						<asp:label id="Label210" style="Z-INDEX: 114; LEFT: 6px; POSITION: absolute; TOP: 73px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Prepared By :</asp:label>
						<asp:label id="Label201" style="Z-INDEX: 115; LEFT: 6px; POSITION: absolute; TOP: 51px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Expiry Date :</asp:label>
						<asp:label id="Label198" style="Z-INDEX: 116; LEFT: 6px; POSITION: absolute; TOP: 29px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Quote Date :</asp:label>
						<asp:label id="lblcurrency9" style="Z-INDEX: 117; LEFT: 97px; POSITION: absolute; TOP: 161px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="Label191" style="Z-INDEX: 118; LEFT: 6px; POSITION: absolute; TOP: 7px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Quote No:</asp:label></DIV>
					<asp:label id="Label190" style="Z-INDEX: 105; LEFT: 397px; POSITION: absolute; TOP: 1px; TEXT-ALIGN: center"
						runat="server" Width="268px" Font-Size="X-Large" Font-Names="Times New Roman" BorderStyle="None"
						Font-Bold="True" BackColor="DarkGray">Price Quotation</asp:label>
					<asp:panel id="Panel22" style="Z-INDEX: 106; LEFT: 0px; POSITION: absolute; TOP: 280px" runat="server"
						Height="482px" Width="665px" BorderWidth="2px" BorderStyle="Solid">
						<asp:Label id="lblinfo9" runat="server" Font-Size="Smaller" Font-Names="Arial"></asp:Label>
					</asp:panel>
					<asp:label id="Label187" style="Z-INDEX: 108; LEFT: 20px; POSITION: absolute; TOP: 872px" runat="server"
						Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">All prices are net.</asp:label>
					<asp:label id="Label186" style="Z-INDEX: 109; LEFT: 20px; POSITION: absolute; TOP: 886px" runat="server"
						Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">F.O.B. Montreal, Quebec. All taxes are extra.</asp:label>
					<asp:label id="Label184" style="Z-INDEX: 110; LEFT: 433px; POSITION: absolute; TOP: 888px"
						runat="server" Width="216px" Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">F.A.B. Montreal, Quebec. Taxes en sus</asp:label>
					<asp:label id="Label183" style="Z-INDEX: 111; LEFT: 552px; POSITION: absolute; TOP: 872px"
						runat="server" Width="97px" Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">Les prix sont net.</asp:label>
					<DIV style="Z-INDEX: 112; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 147px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label182" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Bill To :</asp:label>
						<asp:label id="lblbill91" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblbill92" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="lblbill93" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<DIV style="Z-INDEX: 113; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 80px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label177" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Attn To :</asp:label>
						<asp:label id="lblatt91" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblatt92" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="lblatt93" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<asp:Label id="Label173" style="Z-INDEX: 114; LEFT: 301px; POSITION: absolute; TOP: 886px"
						runat="server" Width="52px" Font-Size="Smaller" Font-Names="Times New Roman" BackColor="DarkGray">Page : 9</asp:Label>
					<asp:Panel id="Panel21" style="Z-INDEX: 115; LEFT: 0px; POSITION: absolute; TOP: 760px" runat="server"
						Height="90px" Width="665px" BorderWidth="2px" BorderStyle="Solid">
						<asp:Label id="lblcomment9" runat="server"></asp:Label>
					</asp:Panel>
					<asp:panel id="ptotal9" style="Z-INDEX: 115; LEFT: 0px; POSITION: absolute; TOP: 848px" runat="server"
						Height="23px" Width="665px" BorderWidth="2px" BorderStyle="Solid" BackColor="#E0E0E0">
						<asp:Label id="lblt9" style="TEXT-ALIGN: right" runat="server" Width="464px" BackColor="Transparent">Total (CAD) :</asp:Label>
						<asp:Label id="lbltotalprice9" style="TEXT-ALIGN: right" runat="server" Width="192px" BackColor="Transparent"></asp:Label>
					</asp:panel></DIV>
			</asp:panel><asp:panel id="page10" style="Z-INDEX: 112; LEFT: 0px; POSITION: absolute; TOP: 8208px" runat="server"
				Visible="False">
				<DIV style="WIDTH: 666px; BORDER-BOTTOM: darkgray 38px solid; POSITION: relative; HEIGHT: 908px"
					ms_positioning="GridLayout">
					<asp:Image id="Image10" style="Z-INDEX: 101; LEFT: 1px; POSITION: absolute; TOP: 2px" runat="server"
						Height="39px" Width="252px" ImageUrl="Cowan_Logo.jpg"></asp:Image>
					<asp:Label id="lbloffice10" style="Z-INDEX: 102; LEFT: 13px; POSITION: absolute; TOP: 45px"
						runat="server" Height="38px" Width="202px" Font-Size="XX-Small" Font-Names="Times New Roman"
						Font-Italic="True"></asp:Label>
					<DIV style="Z-INDEX: 103; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 214px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label269" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Ship To :</asp:label>
						<asp:label id="lblship101" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblship102" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px"
							runat="server" Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblship103" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px"
							runat="server" Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label></DIV>
					<DIV style="Z-INDEX: 104; LEFT: 397px; WIDTH: 268px; POSITION: absolute; TOP: 38px; HEIGHT: 240px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="lblqno10" style="Z-INDEX: 101; LEFT: 97px; POSITION: absolute; TOP: 7px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblprep10" style="Z-INDEX: 102; LEFT: 97px; POSITION: absolute; TOP: 73px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lbledate10" style="Z-INDEX: 103; LEFT: 97px; POSITION: absolute; TOP: 51px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lbldelivery10" style="Z-INDEX: 104; LEFT: 97px; POSITION: absolute; TOP: 139px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblcode10" style="Z-INDEX: 105; LEFT: 97px; POSITION: absolute; TOP: 95px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblterms10" style="Z-INDEX: 106; LEFT: 97px; POSITION: absolute; TOP: 117px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblqdate10" style="Z-INDEX: 107; LEFT: 97px; POSITION: absolute; TOP: 29px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblnotes10" style="Z-INDEX: 108; LEFT: 97px; POSITION: absolute; TOP: 183px; TEXT-ALIGN: center"
							runat="server" Height="49px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="Label257" style="Z-INDEX: 109; LEFT: 6px; POSITION: absolute; TOP: 183px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Note :</asp:label>
						<asp:label id="Label256" style="Z-INDEX: 110; LEFT: 6px; POSITION: absolute; TOP: 161px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Currency :</asp:label>
						<asp:label id="Label255" style="Z-INDEX: 111; LEFT: 6px; POSITION: absolute; TOP: 139px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Delivery :</asp:label>
						<asp:label id="Label254" style="Z-INDEX: 112; LEFT: 6px; POSITION: absolute; TOP: 117px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Terms :</asp:label>
						<asp:label id="Label253" style="Z-INDEX: 113; LEFT: 6px; POSITION: absolute; TOP: 95px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Code :</asp:label>
						<asp:label id="Label252" style="Z-INDEX: 114; LEFT: 6px; POSITION: absolute; TOP: 73px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Prepared By :</asp:label>
						<asp:label id="Label251" style="Z-INDEX: 115; LEFT: 6px; POSITION: absolute; TOP: 51px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Expiry Date :</asp:label>
						<asp:label id="Label250" style="Z-INDEX: 116; LEFT: 6px; POSITION: absolute; TOP: 29px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Quote Date :</asp:label>
						<asp:label id="lblcurrency10" style="Z-INDEX: 117; LEFT: 97px; POSITION: absolute; TOP: 161px; TEXT-ALIGN: center"
							runat="server" Height="20px" Width="163px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="Label248" style="Z-INDEX: 118; LEFT: 6px; POSITION: absolute; TOP: 7px; TEXT-ALIGN: right"
							runat="server" Height="20px" Width="88px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid">Quote No:</asp:label></DIV>
					<asp:label id="Label247" style="Z-INDEX: 105; LEFT: 397px; POSITION: absolute; TOP: 1px; TEXT-ALIGN: center"
						runat="server" Width="268px" Font-Size="X-Large" Font-Names="Times New Roman" BorderStyle="None"
						Font-Bold="True" BackColor="DarkGray">Price Quotation</asp:label>
					<asp:panel id="Panel28" style="Z-INDEX: 106; LEFT: 0px; POSITION: absolute; TOP: 280px" runat="server"
						Height="482px" Width="665px" BorderWidth="2px" BorderStyle="Solid">
						<asp:Label id="lblinfo10" runat="server" Font-Size="Smaller" Font-Names="Arial"></asp:Label>
					</asp:panel>
					<asp:label id="Label245" style="Z-INDEX: 108; LEFT: 20px; POSITION: absolute; TOP: 872px" runat="server"
						Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">All prices are net.</asp:label>
					<asp:label id="Label244" style="Z-INDEX: 109; LEFT: 20px; POSITION: absolute; TOP: 886px" runat="server"
						Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">F.O.B. Montreal, Quebec. All taxes are extra.</asp:label>
					<asp:label id="Label243" style="Z-INDEX: 110; LEFT: 433px; POSITION: absolute; TOP: 888px"
						runat="server" Width="216px" Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">F.A.B. Montreal, Quebec. Taxes en sus</asp:label>
					<asp:label id="Label242" style="Z-INDEX: 111; LEFT: 552px; POSITION: absolute; TOP: 872px"
						runat="server" Width="97px" Font-Size="X-Small" Font-Names="Times New Roman" BackColor="DarkGray">Les prix sont net.</asp:label>
					<DIV style="Z-INDEX: 112; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 147px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label241" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Bill To :</asp:label>
						<asp:label id="lblbill101" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblbill102" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px"
							runat="server" Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label>
						<asp:label id="lblbill103" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px"
							runat="server" Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman"
							BorderWidth="1px" BorderStyle="Solid"></asp:label></DIV>
					<DIV style="Z-INDEX: 113; LEFT: 0px; WIDTH: 330px; POSITION: absolute; TOP: 80px; HEIGHT: 65px; BACKGROUND-COLOR: buttonface"
						ms_positioning="GridLayout">
						<asp:label id="Label237" style="Z-INDEX: 145; LEFT: 14px; POSITION: absolute; TOP: 8px" runat="server"
							Font-Size="Smaller" Font-Names="Times New Roman">Attn To :</asp:label>
						<asp:label id="lblatt101" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 8px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid" Font-Bold="True"></asp:label>
						<asp:label id="lblatt102" style="Z-INDEX: 145; LEFT: 70px; POSITION: absolute; TOP: 24px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label>
						<asp:label id="lblatt103" style="Z-INDEX: 144; LEFT: 70px; POSITION: absolute; TOP: 40px" runat="server"
							Height="10px" Width="250px" Font-Size="Smaller" Font-Names="Times New Roman" BorderWidth="1px"
							BorderStyle="Solid"></asp:label></DIV>
					<asp:Label id="Label233" style="Z-INDEX: 114; LEFT: 301px; POSITION: absolute; TOP: 886px"
						runat="server" Width="52px" Font-Size="Smaller" Font-Names="Times New Roman" BackColor="DarkGray">Page : 10</asp:Label>
					<asp:Panel id="Panel27" style="Z-INDEX: 115; LEFT: 0px; POSITION: absolute; TOP: 760px" runat="server"
						Height="90px" Width="665px" BorderWidth="2px" BorderStyle="Solid">
						<asp:Label id="lblcomment10" runat="server"></asp:Label>
					</asp:Panel>
					<asp:panel id="ptotal10" style="Z-INDEX: 115; LEFT: 0px; POSITION: absolute; TOP: 848px" runat="server"
						Height="23px" Width="665px" BorderWidth="2px" BorderStyle="Solid" BackColor="#E0E0E0">
						<asp:Label id="lblt10" style="TEXT-ALIGN: right" runat="server" Width="464px" BackColor="Transparent">Total (CAD) :</asp:Label>
						<asp:Label id="lbltotalprice10" style="TEXT-ALIGN: right" runat="server" Width="192px" BackColor="Transparent"></asp:Label>
					</asp:panel></DIV>
			</asp:panel></form>
	</body>
</HTML>
