<%@ Page language="c#" Codebehind="EV_SeriesA_Specials.aspx.cs" AutoEventWireup="false" Inherits="iCylinderV1.EV_SeriesA_Specials" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>EV_SeriesA_Specials</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 1000px; HEIGHT: 487px" cellSpacing="0" cellPadding="0"
				width="1000" bgColor="lightgrey" border="0">
				<TR>
					<TD width="50" height="20"></TD>
					<TD style="WIDT: 484px" align="center" height="20"></TD>
					<TD align="center" height="20"><asp:label id="Label1" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
							Font-Size="Small">Series  A - EVR Cylinder Special Options</asp:label></TD>
					<TD align="center" height="20"></TD>
					<TD width="50" height="20"></TD>
				</TR>
				<TR>
					<TD width="50"></TD>
					<TD language="300" align="center" width="299" bgColor="#dcdcdc"><asp:panel id="Panel4" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE id="Table6" border="0" cellSpacing="0" borderColor="silver" cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label5" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" ForeColor="Maroon" Font-Bold="True">Select One Option</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:RadioButtonList id="RBLPSOption" runat="server" Font-Size="Smaller" AutoPostBack="True" RepeatDirection="Horizontal">
											<asp:ListItem Value="Solenoid" Selected="True">Solenoid</asp:ListItem>
											<asp:ListItem Value="Positioner">Positioner</asp:ListItem>
											<asp:ListItem Value="None">None</asp:ListItem>
										</asp:RadioButtonList></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="PanelSV" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE id="Table8" border="0" cellSpacing="0" borderColor="silver" cellPadding="0" width="299">
								<TR>
									<TD style="WIDTH: 99px" align="center">
										<asp:label id="Label9" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" ForeColor="Maroon" Font-Bold="True" Width="84px">Solenoid Valve</asp:label></TD>
									<TD align="left">
										<asp:DropDownList id="DDLSVType" runat="server" Font-Size="8pt" AutoPostBack="True" Width="89px"></asp:DropDownList></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 99px" align="center"></TD>
									<TD align="left">
										<asp:image id="Image3" runat="server" ImageUrl="specials/sv.gif"></asp:image></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 99px" align="center">
										<asp:Label id="Label7" runat="server" Font-Size="9pt" ForeColor="Black">Description :</asp:Label></TD>
									<TD align="left">
										<asp:Label id="LblSVDesc" runat="server" Font-Size="8pt" ForeColor="Black"></asp:Label></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 99px" align="center">
										<asp:Label id="lblclassi" runat="server" Font-Size="9pt">Classification :</asp:Label></TD>
									<TD align="left">
										<asp:textbox id="TxtClassification" runat="server" Font-Size="XX-Small" Width="168px"></asp:textbox></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 99px" align="center"></TD>
									<TD align="left">
										<asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" Font-Size="8pt" Width="160px" ControlToValidate="TxtClassification"
											ErrorMessage="Enter Classification" Visible="False"></asp:RequiredFieldValidator></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="PanelPr" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE style="HEIGHT: 35px" id="Table11" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0" width="299">
								<TR>
									<TD style="WIDTH: 100px" align="center">
										<asp:label id="Label14" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" ForeColor="Maroon" Font-Bold="True">Positioner</asp:label></TD>
									<TD align="left">
										<asp:RadioButtonList style="Z-INDEX: 0" id="RBLPositioner" runat="server" Font-Size="Smaller" AutoPostBack="True"
											RepeatDirection="Horizontal" RepeatLayout="Flow">
											<asp:ListItem Value="PO" Selected="True">Special</asp:ListItem>
											<asp:ListItem Value="PMVEP5">PMV-EP5</asp:ListItem>
										</asp:RadioButtonList></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 99px" align="center">
										<asp:Label id="Label6" runat="server" Font-Size="8pt" ForeColor="Gray">Positioner supplied by EVR and mounted by Cowan.</asp:Label></TD>
									<TD align="left">
										<asp:image id="Image2" runat="server" ImageUrl="specials/pr.gif"></asp:image></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 99px" align="center">
										<asp:Label id="lblppartno" runat="server" Font-Size="9pt">Specify Positioner :</asp:Label></TD>
									<TD align="left">
										<asp:textbox id="TxtPPartNo" runat="server" Font-Size="XX-Small" Width="168px"></asp:textbox></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 99px" align="center"></TD>
									<TD align="left">
										<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" Font-Size="8pt" Width="160px" ControlToValidate="TxtPPartNo"
											ErrorMessage="Enter Positioner Part No."></asp:RequiredFieldValidator></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
					<TD align="center" width="299" bgColor="#dcdcdc">
						<TABLE id="Table2" style="HEIGHT: 35px" borderColor="silver" cellSpacing="0" cellPadding="0"
							width="299" border="0">
						</TABLE>
						<asp:panel id="PanelFC" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
							<TABLE style="HEIGHT: 35px" id="Table3" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label2" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" ForeColor="Maroon" Font-Bold="True">Flow Control</asp:label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 120px" align="center">
										<asp:image id="Image4" runat="server" ImageUrl="specials/fc.gif"></asp:image></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:RadioButtonList id="RBLFC" runat="server" Font-Size="Smaller" AutoPostBack="True">
											<asp:ListItem Value="F1">Inline Flow Controls</asp:ListItem>
											<asp:ListItem Value="F2">Inline Stainless Flow Controls</asp:ListItem>
											<asp:ListItem Selected="True">None</asp:ListItem>
										</asp:RadioButtonList></TD>
								</TR>
							</TABLE>
						</asp:panel>
						<asp:panel id="PanelFR" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
							<TABLE style="HEIGHT: 35px" id="Table4" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label3" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" ForeColor="Maroon" Font-Bold="True">Filter Regulator</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:image id="Image5" runat="server" ImageUrl="specials/fr.gif"></asp:image></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:RadioButtonList id="RBLFR" runat="server" Font-Size="Smaller" AutoPostBack="True">
											<asp:ListItem Value="FR1">General Purpose Filter Regulator</asp:ListItem>
											<asp:ListItem Value="FR2">Stainless Steel Filter Regulator</asp:ListItem>
											<asp:ListItem Selected="True">None</asp:ListItem>
										</asp:RadioButtonList></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
					<TD align="center" width="299" bgColor="gainsboro"><asp:panel id="Panel9" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE style="HEIGHT: 8px" id="Table14" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label18" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" ForeColor="Maroon" Font-Bold="True">Manual Override</asp:label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:image id="ImageMO" runat="server" ImageUrl="specials/mo.gif"></asp:image></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:RadioButtonList id="RBLMO" runat="server" Font-Size="Smaller" RepeatDirection="Horizontal">
											<asp:ListItem Value="MO">Yes</asp:ListItem>
											<asp:ListItem Selected="True">No</asp:ListItem>
										</asp:RadioButtonList></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="Panel6" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE style="HEIGHT: 8px" id="Table13" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label12" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" ForeColor="Maroon" Font-Bold="True">Reed Switch</asp:label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:image id="Image1" runat="server" ImageUrl="specials/rs.gif"></asp:image></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:RadioButtonList id="RBLRS" runat="server" Font-Size="Smaller" RepeatDirection="Horizontal">
											<asp:ListItem Value="RS">Yes</asp:ListItem>
											<asp:ListItem Selected="True">No</asp:ListItem>
										</asp:RadioButtonList></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="PanelTB" runat="server" BorderColor="LightGray" BorderWidth="1px" BorderStyle="Solid">
							<TABLE id="Table5" border="0" cellSpacing="0" borderColor="silver" cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label4" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" ForeColor="Maroon" Font-Bold="True">Tubing & Fittings</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:RadioButtonList id="RBLTubing" runat="server" Font-Size="Smaller">
											<asp:ListItem Value="T1" Selected="True">Copper Tubing &amp; Brass Fittings</asp:ListItem>
											<asp:ListItem Value="T2">Stainless Steel Tubing &amp; Fittings</asp:ListItem>
										</asp:RadioButtonList></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
					<TD width="50"></TD>
				</TR>
				<TR>
					<TD align="right" width="50" height="30"><asp:linkbutton id="BtnBack" runat="server" Font-Size="Smaller" Font-Bold="True">Back</asp:linkbutton></TD>
					<TD language="300" align="center" height="30"><asp:label id="lblhidden" runat="server" Font-Size="XX-Small" Visible="False"></asp:label></TD>
					<TD align="center" height="30"><asp:label id="lblpage" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Size="9pt"
							ForeColor="Red" Width="234px" Visible="False">Enter / Select  all values</asp:label></TD>
					<TD align="center" height="30"></TD>
					<TD width="50" height="30"><asp:linkbutton id="BtnNext" runat="server" Font-Size="Smaller" Font-Bold="True">Next</asp:linkbutton></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
