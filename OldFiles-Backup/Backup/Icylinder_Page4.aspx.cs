using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for Icylinder_Page4.
	/// </summary>
	public class Icylinder_Page4 : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblseries;
		protected System.Web.UI.WebControls.LinkButton BtnNext;
		protected System.Web.UI.WebControls.LinkButton LBBack;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.RadioButtonList RBLCC2;
		protected System.Web.UI.WebControls.RadioButtonList RBLCC1;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.RadioButtonList RBLCusions;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.RadioButtonList RBLPP2;
		protected System.Web.UI.WebControls.RadioButtonList RBLPP1;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.RadioButtonList RBLPorts;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label lbl;
		protected System.Web.UI.WebControls.Label lblxi;
		protected System.Web.UI.WebControls.RadioButtonList RBLSeal;
		coder cd1=new coder();
		protected System.Web.UI.WebControls.LinkButton LBAdvanced;
		DBClass db=new DBClass();
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["User"] !=null)
			{
				if(! IsPostBack)
				{
					if(	Session["Coder"] !=null)
					{
						ArrayList blist=new ArrayList();
						ArrayList blist1=new ArrayList();
						ArrayList blist2=new ArrayList();
						ListItem litem=new ListItem();
						cd1=(coder)Session["Coder"];
						switch (cd1.Series.Trim())
						{
							case "A":
								blist =db.SelectOptions("PortType_Code", "PortType_Type","WEB_PortType_TableV1","SeriesA","PortType_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLPorts.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLPorts.Items.Add(litem);
								}
								RBLPorts.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Cushion_Code","Cushion_Type","WEB_Cushion_TableV1","SeriesA","Cushion_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								
								RBLCusions.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLCusions.Items.Add(litem);
								}
								RBLCusions.SelectedIndex =0;

								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Seal_Code","Seal_Type","WEB_Seal_TableV1","SeriesA","Seal_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLSeal.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLSeal.Items.Add(litem);
								}
								RBLSeal.SelectedIndex =0;
								break;
							case "PA":
								blist =db.SelectOptions("PortType_Code", "POrtType_Type","WEB_PortType_TableV1","SeriesPA","PortType_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLPorts.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLPorts.Items.Add(litem);
								}
								RBLPorts.SelectedIndex =1;


								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Cushion_Code","Cushion_Type","WEB_Cushion_TableV1","SeriesPA","Cushion_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLCusions.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLCusions.Items.Add(litem);
								}
								RBLCusions.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Seal_Code","Seal_Type","WEB_Seal_TableV1","SeriesPA","Seal_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLSeal.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLSeal.Items.Add(litem);
								}
								RBLSeal.SelectedIndex =0;
								break;
							case "PS":
								blist =db.SelectOptions("PortType_Code", "POrtType_Type","WEB_PortType_TableV1","SeriesPS","PortType_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLPorts.Items.Clear();
							
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLPorts.Items.Add(litem);
								}
								RBLPorts.SelectedIndex =1;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Cushion_Code","Cushion_Type","WEB_Cushion_TableV1","SeriesPS","Cushion_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLCusions.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLCusions.Items.Add(litem);
								}
								RBLCusions.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Seal_Code","Seal_Type","WEB_Seal_TableV1","SeriesPS","Seal_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLSeal.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLSeal.Items.Add(litem);
								}
								RBLSeal.SelectedIndex =0;
								break;
							case "PC":
								blist =db.SelectOptions("PortType_Code", "POrtType_Type","WEB_PortType_TableV1","SeriesPC","PortType_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLPorts.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLPorts.Items.Add(litem);
								}
								RBLPorts.SelectedIndex =1;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								if(cd1.Bore_Size.ToString().Trim() =="C" || cd1.Bore_Size.ToString().Trim() =="D" || cd1.Bore_Size.ToString().Trim() =="E")
								{
									blist =db.SelectOptions("Cushion_Code","Cushion_Type","WEB_Cushion_TableV1","SeriesPC1","Cushion_SLNO");
									blist1 =(ArrayList)blist[0];
									blist2 =(ArrayList)blist[1];
								}
								else
								{
									blist =db.SelectOptions("Cushion_Code","Cushion_Type","WEB_Cushion_TableV1","SeriesPC","Cushion_SLNO");
									blist1 =(ArrayList)blist[0];
									blist2 =(ArrayList)blist[1];
								}
								RBLCusions.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLCusions.Items.Add(litem);
								}
								RBLCusions.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Seal_Code","Seal_Type","WEB_Seal_TableV1","SeriesPC","Seal_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLSeal.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLSeal.Items.Add(litem);
								}
								RBLSeal.SelectedIndex =0;
								break;
							case "N":
								blist =db.SelectOptions("PortType_Code", "POrtType_Type","WEB_PortType_TableV1","SeriesN","PortType_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLPorts.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLPorts.Items.Add(litem);
								}
								RBLPorts.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Cushion_Code","Cushion_Type","WEB_Cushion_TableV1","SeriesN","Cushion_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLCusions.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLCusions.Items.Add(litem);
								}
								RBLCusions.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Seal_Code","Seal_Type","WEB_Seal_TableV1","SeriesN","Seal_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLSeal.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLSeal.Items.Add(litem);
								}
								RBLSeal.SelectedIndex =0;
								break;
							case "M":
								blist =db.SelectOptions("PortType_Code", "POrtType_Type","WEB_PortType_TableV1","SeriesM","PortType_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLPorts.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLPorts.Items.Add(litem);
								}
								RBLPorts.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Cushion_Code","Cushion_Type","WEB_Cushion_TableV1","SeriesM","Cushion_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLCusions.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLCusions.Items.Add(litem);
								}
								RBLCusions.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Seal_Code","Seal_Type","WEB_Seal_TableV1","SeriesM","Seal_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLSeal.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLSeal.Items.Add(litem);
								}
								RBLSeal.SelectedIndex =0;
								break;
							case "ML":
								blist =db.SelectOptions("PortType_Code", "POrtType_Type","WEB_PortType_TableV1","SeriesML","PortType_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLPorts.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLPorts.Items.Add(litem);
								}
								RBLPorts.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Cushion_Code","Cushion_Type","WEB_Cushion_TableV1","SeriesML","Cushion_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLCusions.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLCusions.Items.Add(litem);
								}
								RBLCusions.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Seal_Code","Seal_Type","WEB_Seal_TableV1","SeriesML","Seal_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLSeal.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLSeal.Items.Add(litem);
								}
								RBLSeal.SelectedIndex =0;
								break;
							case "R":
								blist =db.SelectOptions("PortType_Code", "POrtType_Type","WEB_PortType_TableV1","SeriesR","PortType_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLPorts.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLPorts.Items.Add(litem);
								}
								RBLPorts.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Cushion_Code","Cushion_Type","WEB_Cushion_TableV1","SeriesR","Cushion_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLCusions.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLCusions.Items.Add(litem);
								}
								RBLCusions.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Seal_Code","Seal_Type","WEB_Seal_TableV1","SeriesR","Seal_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLSeal.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLSeal.Items.Add(litem);
								}
								RBLSeal.SelectedIndex =0;
								break;
							case "RP":
								blist =db.SelectOptions("PortType_Code", "POrtType_Type","WEB_PortType_TableV1","SeriesRP","PortType_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLPorts.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLPorts.Items.Add(litem);
								}
								RBLPorts.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Cushion_Code","Cushion_Type","WEB_Cushion_TableV1","SeriesRP","Cushion_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLCusions.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLCusions.Items.Add(litem);
								}
								RBLCusions.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Seal_Code","Seal_Type","WEB_Seal_TableV1","SeriesRP","Seal_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLSeal.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLSeal.Items.Add(litem);
								}
								RBLSeal.SelectedIndex =0;
								break;
							case "L":
								blist =db.SelectOptions("PortType_Code", "POrtType_Type","WEB_PortType_TableV1","SeriesL","PortType_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLPorts.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLPorts.Items.Add(litem);
								}
								RBLPorts.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Cushion_Code","Cushion_Type","WEB_Cushion_TableV1","SeriesL","Cushion_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLCusions.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLCusions.Items.Add(litem);
								}
								RBLCusions.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Seal_Code","Seal_Type","WEB_Seal_TableV1","SeriesL","Seal_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLSeal.Items.Clear();
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString());
									RBLSeal.Items.Add(litem);
								}
								RBLSeal.SelectedIndex =0;
								break;
						}
						if(cd1.Series.Trim() !="L")
						{
							if(cd1.Mount.Trim() =="T2")
							{
								ListItem li=new ListItem();
								RBLPP2.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLPP2.Items.Add(li);
								RBLPP2.SelectedIndex =RBLPP2.Items.IndexOf(li);
								li= new ListItem("Position 3","3");
								RBLPP2.Items.Add(li);
								RBLPP1.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLPP1.Items.Add(li);
								RBLPP1.SelectedIndex =RBLPP1.Items.IndexOf(li);
								li= new ListItem("Position 2","2");
								RBLPP1.Items.Add(li);
								li= new ListItem("Position 3","3");
								RBLPP1.Items.Add(li);
								li= new ListItem("Position 4","4");
								RBLPP1.Items.Add(li);
								
								RBLCC2.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLCC2.Items.Add(li);
								li= new ListItem("Position 3","3");
								RBLCC2.Items.Add(li);
								RBLCC2.SelectedIndex =RBLCC2.Items.IndexOf(li);
								RBLCC1.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLCC1.Items.Add(li);
								li= new ListItem("Position 2","2");
								RBLCC1.Items.Add(li);
								li= new ListItem("Position 3","3");
								RBLCC1.Items.Add(li);
								RBLCC1.SelectedIndex =RBLCC1.Items.IndexOf(li);
								li= new ListItem("Position 4","4");
								RBLCC1.Items.Add(li);
								
							}
							else if(cd1.Mount.Trim() =="T1")
							{
								ListItem li=new ListItem();
								RBLPP1.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLPP1.Items.Add(li);
								RBLPP1.SelectedIndex =RBLPP1.Items.IndexOf(li);
								li= new ListItem("Position 3","3");
								RBLPP1.Items.Add(li);
								RBLPP2.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLPP2.Items.Add(li);
								RBLPP2.SelectedIndex =RBLPP2.Items.IndexOf(li);
								li= new ListItem("Position 2","2");
								RBLPP2.Items.Add(li);
								li= new ListItem("Position 3","3");
								RBLPP2.Items.Add(li);
								li= new ListItem("Position 4","4");
								RBLPP2.Items.Add(li);

								RBLCC1.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLCC1.Items.Add(li);
								li= new ListItem("Position 3","3");
								RBLCC1.Items.Add(li);
								RBLCC1.SelectedIndex =RBLCC1.Items.IndexOf(li);
								RBLCC2.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLCC2.Items.Add(li);
								li= new ListItem("Position 2","2");
								RBLCC2.Items.Add(li);
								li= new ListItem("Position 3","3");
								RBLCC2.Items.Add(li);
								RBLCC2.SelectedIndex =RBLCC2.Items.IndexOf(li);
								li= new ListItem("Position 4","4");
								RBLCC2.Items.Add(li);
							}
							else if(cd1.Mount.Trim() =="S4")
							{
								ListItem li=new ListItem();
								RBLPP1.Items.Clear();
								RBLPP2.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLPP1.Items.Add(li);
								RBLPP2.Items.Add(li);
								RBLPP1.SelectedIndex =RBLPP1.Items.IndexOf(li);
								RBLPP2.SelectedIndex =RBLPP2.Items.IndexOf(li);
								li= new ListItem("Position 2","2");
								RBLPP1.Items.Add(li);
								RBLPP2.Items.Add(li);
								li= new ListItem("Position 4","4");
								RBLPP1.Items.Add(li);
								RBLPP2.Items.Add(li);

								RBLCC1.Items.Clear();
								RBLCC2.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLCC1.Items.Add(li);
								RBLCC2.Items.Add(li);
								li= new ListItem("Position 2","2");
								RBLCC1.Items.Add(li);
								RBLCC2.Items.Add(li);
								RBLCC1.SelectedIndex =RBLCC1.Items.IndexOf(li);
								RBLCC2.SelectedIndex =RBLCC2.Items.IndexOf(li);
								li= new ListItem("Position 4","4");
								RBLCC1.Items.Add(li);
								RBLCC2.Items.Add(li);
								
							}
							else if(cd1.Mount.Trim() =="S3")
							{
								ListItem li=new ListItem();
								RBLPP1.Items.Clear();
								RBLPP2.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLPP1.Items.Add(li);
								RBLPP2.Items.Add(li);
								RBLPP1.SelectedIndex =RBLPP1.Items.IndexOf(li);
								RBLPP2.SelectedIndex =RBLPP2.Items.IndexOf(li);
								li= new ListItem("Position 3","3");
								RBLPP1.Items.Add(li);
								RBLPP2.Items.Add(li);

								RBLCC1.Items.Clear();
								RBLCC2.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLCC1.Items.Add(li);
								RBLCC2.Items.Add(li);
								li= new ListItem("Position 3","3");
								RBLCC1.Items.Add(li);
								RBLCC2.Items.Add(li);
								RBLCC1.SelectedIndex =RBLCC1.Items.IndexOf(li);
								RBLCC2.SelectedIndex =RBLCC2.Items.IndexOf(li);
							}
							else 
							{
								ListItem li=new ListItem();
								RBLPP1.Items.Clear();
								RBLPP2.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLPP1.Items.Add(li);
								RBLPP2.Items.Add(li);
								RBLPP1.SelectedIndex =RBLPP1.Items.IndexOf(li);
								RBLPP2.SelectedIndex =RBLPP2.Items.IndexOf(li);
								li= new ListItem("Position 2","2");
								RBLPP1.Items.Add(li);
								RBLPP2.Items.Add(li);
								li= new ListItem("Position 3","3");
								RBLPP1.Items.Add(li);
								RBLPP2.Items.Add(li);
								li= new ListItem("Position 4","4");
								RBLPP1.Items.Add(li);
								RBLPP2.Items.Add(li);

								RBLCC1.Items.Clear();
								RBLCC2.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLCC1.Items.Add(li);
								RBLCC2.Items.Add(li);
								li= new ListItem("Position 2","2");
								RBLCC1.Items.Add(li);
								RBLCC2.Items.Add(li);
								RBLCC1.SelectedIndex =RBLCC1.Items.IndexOf(li);
								RBLCC2.SelectedIndex =RBLCC2.Items.IndexOf(li);
								li= new ListItem("Position 3","3");
								RBLCC1.Items.Add(li);
								RBLCC2.Items.Add(li);
								li= new ListItem("Position 4","4");
								RBLCC1.Items.Add(li);
								RBLCC2.Items.Add(li);
							}
						}
						else
						{
							if(cd1.Mount.Trim() =="T2")
							{
								ListItem li=new ListItem();
								RBLPP2.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLPP2.Items.Add(li);
								RBLPP2.SelectedIndex =RBLPP2.Items.IndexOf(li);
								li= new ListItem("Position 3","3");
								RBLPP2.Items.Add(li);
								li= new ListItem("Manifold Mount","M");
								RBLPP2.Items.Add(li);
								RBLPP1.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLPP1.Items.Add(li);
								RBLPP1.SelectedIndex =RBLPP1.Items.IndexOf(li);
								li= new ListItem("Position 2","2");
								RBLPP1.Items.Add(li);
								li= new ListItem("Position 3","3");
								RBLPP1.Items.Add(li);
								li= new ListItem("Position 4","4");
								RBLPP1.Items.Add(li);
								li= new ListItem("Manifold Mount","M");
								RBLPP1.Items.Add(li);
								
								RBLCC2.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLCC2.Items.Add(li);
								li= new ListItem("Position 3","3");
								RBLCC2.Items.Add(li);
								RBLCC2.SelectedIndex =RBLCC2.Items.IndexOf(li);
								RBLCC1.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLCC1.Items.Add(li);
								li= new ListItem("Position 2","2");
								RBLCC1.Items.Add(li);
								li= new ListItem("Position 3","3");
								RBLCC1.Items.Add(li);
								RBLCC1.SelectedIndex =RBLCC1.Items.IndexOf(li);
								li= new ListItem("Position 4","4");
								RBLCC1.Items.Add(li);
								
							}
							else if(cd1.Mount.Trim() =="T1")
							{
								ListItem li=new ListItem();
								RBLPP1.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLPP1.Items.Add(li);
								RBLPP1.SelectedIndex =RBLPP1.Items.IndexOf(li);
								li= new ListItem("Position 3","3");
								RBLPP1.Items.Add(li);
								li= new ListItem("Manifold Mount","M");
								RBLPP1.Items.Add(li);
								RBLPP2.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLPP2.Items.Add(li);
								RBLPP2.SelectedIndex =RBLPP2.Items.IndexOf(li);
								li= new ListItem("Position 2","2");
								RBLPP2.Items.Add(li);
								li= new ListItem("Position 3","3");
								RBLPP2.Items.Add(li);
								li= new ListItem("Position 4","4");
								RBLPP2.Items.Add(li);
								li= new ListItem("Manifold Mount","M");
								RBLPP2.Items.Add(li);

								RBLCC1.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLCC1.Items.Add(li);
								li= new ListItem("Position 3","3");
								RBLCC1.Items.Add(li);
								RBLCC1.SelectedIndex =RBLCC1.Items.IndexOf(li);
								RBLCC2.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLCC2.Items.Add(li);
								li= new ListItem("Position 2","2");
								RBLCC2.Items.Add(li);
								li= new ListItem("Position 3","3");
								RBLCC2.Items.Add(li);
								RBLCC2.SelectedIndex =RBLCC2.Items.IndexOf(li);
								li= new ListItem("Position 4","4");
								RBLCC2.Items.Add(li);
							}
							else if(cd1.Mount.Trim() =="S4")
							{
								ListItem li=new ListItem();
								RBLPP1.Items.Clear();
								RBLPP2.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLPP1.Items.Add(li);
								RBLPP2.Items.Add(li);
								RBLPP1.SelectedIndex =RBLPP1.Items.IndexOf(li);
								RBLPP2.SelectedIndex =RBLPP2.Items.IndexOf(li);
								li= new ListItem("Position 2","2");
								RBLPP1.Items.Add(li);
								RBLPP2.Items.Add(li);
								li= new ListItem("Position 4","4");
								RBLPP1.Items.Add(li);
								RBLPP2.Items.Add(li);
								li= new ListItem("Manifold Mount","M");
								RBLPP1.Items.Add(li);
								RBLPP2.Items.Add(li);

								RBLCC1.Items.Clear();
								RBLCC2.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLCC1.Items.Add(li);
								RBLCC2.Items.Add(li);
								li= new ListItem("Position 2","2");
								RBLCC1.Items.Add(li);
								RBLCC2.Items.Add(li);
								RBLCC1.SelectedIndex =RBLCC1.Items.IndexOf(li);
								RBLCC2.SelectedIndex =RBLCC2.Items.IndexOf(li);
								li= new ListItem("Position 4","4");
								RBLCC1.Items.Add(li);
								RBLCC2.Items.Add(li);
								
							}
							else if(cd1.Mount.Trim() =="S3")
							{
								ListItem li=new ListItem();
								RBLPP1.Items.Clear();
								RBLPP2.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLPP1.Items.Add(li);
								RBLPP2.Items.Add(li);
								RBLPP1.SelectedIndex =RBLPP1.Items.IndexOf(li);
								RBLPP2.SelectedIndex =RBLPP2.Items.IndexOf(li);
								li= new ListItem("Position 3","3");
								RBLPP1.Items.Add(li);
								RBLPP2.Items.Add(li);
								li= new ListItem("Manifold Mount","M");
								RBLPP1.Items.Add(li);
								RBLPP2.Items.Add(li);

								RBLCC1.Items.Clear();
								RBLCC2.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLCC1.Items.Add(li);
								RBLCC2.Items.Add(li);
								li= new ListItem("Position 3","3");
								RBLCC1.Items.Add(li);
								RBLCC2.Items.Add(li);
								RBLCC1.SelectedIndex =RBLCC1.Items.IndexOf(li);
								RBLCC2.SelectedIndex =RBLCC2.Items.IndexOf(li);
							}
							else 
							{
								ListItem li=new ListItem();
								RBLPP1.Items.Clear();
								RBLPP2.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLPP1.Items.Add(li);
								RBLPP2.Items.Add(li);
								RBLPP1.SelectedIndex =RBLPP1.Items.IndexOf(li);
								RBLPP2.SelectedIndex =RBLPP2.Items.IndexOf(li);
								li= new ListItem("Position 2","2");
								RBLPP1.Items.Add(li);
								RBLPP2.Items.Add(li);
								li= new ListItem("Position 3","3");
								RBLPP1.Items.Add(li);
								RBLPP2.Items.Add(li);
								li= new ListItem("Position 4","4");
								RBLPP1.Items.Add(li);
								RBLPP2.Items.Add(li);
								li= new ListItem("Manifold Mount","M");
								RBLPP1.Items.Add(li);
								RBLPP2.Items.Add(li);

								RBLCC1.Items.Clear();
								RBLCC2.Items.Clear();
								li= new ListItem("Position 1","1");
								RBLCC1.Items.Add(li);
								RBLCC2.Items.Add(li);
								li= new ListItem("Position 2","2");
								RBLCC1.Items.Add(li);
								RBLCC2.Items.Add(li);
								RBLCC1.SelectedIndex =RBLCC1.Items.IndexOf(li);
								RBLCC2.SelectedIndex =RBLCC2.Items.IndexOf(li);
								li= new ListItem("Position 3","3");
								RBLCC1.Items.Add(li);
								RBLCC2.Items.Add(li);
								li= new ListItem("Position 4","4");
								RBLCC1.Items.Add(li);
								RBLCC2.Items.Add(li);
							}
						}
						if(cd1.Series.Trim() =="A" || cd1.Series.Trim() =="L")
						{
							RBLCC1.Items.Clear();
							RBLCC2.Items.Clear();
							ListItem li=new ListItem();
							li= new ListItem("NA","");
							RBLCC1.Items.Add(li);
							RBLCC2.Items.Add(li);
							RBLCC1.SelectedIndex =RBLCC1.Items.IndexOf(li);
							RBLCC2.SelectedIndex =RBLCC2.Items.IndexOf(li);
							RBLPP1.SelectedIndex=0;
							RBLPP2.SelectedIndex=0;
						}
						if(Session["Coder"] !=null)
						{
							cd1 =(coder)Session["Coder"];
							if(cd1.Port_Type !=null)
							{
								RBLPorts.SelectedValue =cd1.Port_Type.Trim();
							}
							if(cd1.Port_Pos !=null)
							{
								RBLPP1.SelectedValue =cd1.Port_Pos.Trim().Substring(0,1);
								RBLPP2.SelectedValue =cd1.Port_Pos.Trim().Substring(1,1);
							}
							if(cd1.Cushions !=null)
							{
								RBLCusions.SelectedValue =cd1.Cushions.Trim();
								RBLCusions_SelectedIndexChanged(sender,e);
							}
							if(cd1.CushionPosition !=null)
							{
								if(cd1.CushionPosition.Trim().Length ==3)
								{
									RBLCC1.SelectedValue =cd1.CushionPosition.Trim().Substring(1,1);
									RBLPP2.SelectedValue =cd1.CushionPosition.Trim().Substring(2,1);
								}
								else if(cd1.CushionPosition.Trim().Length ==2)
								{
									if(RBLCC1.Items.Count > 1)
									{
										RBLCC1.SelectedValue =cd1.CushionPosition.Trim().Substring(1,1);
									}
									if(RBLCC2.Items.Count > 1)
									{
										RBLCC2.SelectedValue =cd1.CushionPosition.Trim().Substring(1,1);
									}
								}

							}
							
							if(cd1.Seal_Comp !=null)
							{
								RBLSeal.SelectedValue =cd1.Seal_Comp.Trim();
							}
							
							//issue #98 start
							if((cd1.Bore_Size.Trim()!="G" && cd1.Bore_Size.Trim()!="H") || ((cd1.Bore_Size.Trim()=="G" || cd1.Bore_Size.Trim()=="H")&&(cd1.Rod_Diamtr!="E"&&cd1.Rod_Diamtr!="G")))
							{
								if (RBLCusions.Items.FindByValue("9")!=null)
								{
									RBLCusions.Items.Remove(RBLCusions.Items.FindByValue("9"));
								}
							}
							//issue #98 end
						}
					
					}
				}
			}
			else
			{
				Response.Redirect("Login.aspx");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.RBLPP1.SelectedIndexChanged += new System.EventHandler(this.RBLPP1_SelectedIndexChanged);
			this.RBLPP2.SelectedIndexChanged += new System.EventHandler(this.RBLPP2_SelectedIndexChanged);
			this.RBLCusions.SelectedIndexChanged += new System.EventHandler(this.RBLCusions_SelectedIndexChanged);
			this.RBLCC1.SelectedIndexChanged += new System.EventHandler(this.RBLCC1_SelectedIndexChanged);
			this.RBLCC2.SelectedIndexChanged += new System.EventHandler(this.RBLCC2_SelectedIndexChanged);
			this.LBBack.Click += new System.EventHandler(this.LBBack_Click);
			this.LBAdvanced.Click += new System.EventHandler(this.LBAdvanced_Click);
			this.BtnNext.Click += new System.EventHandler(this.BtnNext_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void RBLCusions_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lbl.Visible =false;
			if(RBLCusions.SelectedItem.Value.Trim() =="8" || RBLCusions.SelectedItem.Value.Trim() =="2"
				|| RBLCusions.SelectedItem.Value.Trim() =="3" ||RBLCusions.SelectedItem.Value.Trim() =="4"
				|| RBLCusions.SelectedItem.Value.Trim() =="1" || RBLCusions.SelectedItem.Value.Trim() =="9")
			{
				ListItem li=new ListItem();
				RBLCC1.Items.Clear();
				RBLCC2.Items.Clear();
				li= new ListItem("NA","");
				RBLCC1.Items.Add(li);
				RBLCC2.Items.Add(li);
				RBLCC1.SelectedIndex =RBLCC1.Items.IndexOf(li);
				RBLCC2.SelectedIndex =RBLCC2.Items.IndexOf(li);

			}
			else if(RBLCusions.SelectedItem.Value.Trim() =="5")
			{
				ListItem li=new ListItem();
				RBLCC1.Items.Clear();
				RBLCC2.Items.Clear();
				li= new ListItem("Position 1","1");
				RBLCC1.Items.Add(li);
				RBLCC2.Items.Add(li);
				li= new ListItem("Position 2","2");
				RBLCC1.Items.Add(li);
				RBLCC2.Items.Add(li);
				RBLCC1.SelectedIndex =RBLCC1.Items.IndexOf(li);
				RBLCC2.SelectedIndex =RBLCC2.Items.IndexOf(li);
				li= new ListItem("Position 3","3");
				RBLCC1.Items.Add(li);
				RBLCC2.Items.Add(li);
				li= new ListItem("Position 4","4");
				RBLCC1.Items.Add(li);
				RBLCC2.Items.Add(li);
				
			}
			else if(RBLCusions.SelectedItem.Value.Trim() =="6")
			{
				ListItem li=new ListItem();
				RBLCC1.Items.Clear();
				
				li= new ListItem("Position 1","1");
				RBLCC1.Items.Add(li);
				li= new ListItem("Position 2","2");
				RBLCC1.Items.Add(li);
				RBLCC1.SelectedIndex =RBLCC1.Items.IndexOf(li);
				li= new ListItem("Position 3","3");
				RBLCC1.Items.Add(li);
				li= new ListItem("Position 4","4");
				RBLCC1.Items.Add(li);
				RBLCC2.Items.Clear();
				li= new ListItem("NA","");
				RBLCC2.Items.Add(li);
				RBLCC2.SelectedIndex =RBLCC2.Items.IndexOf(li);
			}
			else if(RBLCusions.SelectedItem.Value.Trim() =="7")
			{
				ListItem li=new ListItem();
				RBLCC1.Items.Clear();
				li= new ListItem("NA","");
				RBLCC1.Items.Add(li);
				RBLCC1.SelectedIndex =RBLCC1.Items.IndexOf(li);
				RBLCC2.Items.Clear();
				li= new ListItem("Position 1","1");
				RBLCC2.Items.Add(li);
				li= new ListItem("Position 2","2");
				RBLCC2.Items.Add(li);
				RBLCC2.SelectedIndex =RBLCC2.Items.IndexOf(li);
				li= new ListItem("Position 3","3");
				RBLCC2.Items.Add(li);
				li= new ListItem("Position 4","4");
				RBLCC2.Items.Add(li);
					
			}

			if(RBLCC1.SelectedIndex ==0 && RBLPP1.SelectedIndex ==0)
			{
				
				lbl.Visible =true;
				if(RBLCC1.Items.Count >1)
				{
					RBLCC1.SelectedIndex =1;
				}
				else
				{
					RBLCC1.SelectedIndex =0;
				}
			}
			else if(RBLCC1.SelectedIndex ==1 && RBLPP1.SelectedIndex ==1)
			{
				
				lbl.Visible =true;
				if(RBLCC1.Items.Count >2)
				{
					RBLCC1.SelectedIndex =2;
				}
				else
				{
					RBLCC1.SelectedIndex =0;	
				}
			} 
			else if(RBLCC1.SelectedIndex ==2 && RBLPP1.SelectedIndex ==2)
			{
				
				lbl.Visible =true;
				if(RBLCC1.Items.Count >3)
				{
					RBLCC1.SelectedIndex =3;
				}
				else
				{
					RBLCC1.SelectedIndex =1;	
				}
			} 
			else if(RBLCC1.SelectedIndex ==3 && RBLPP1.SelectedIndex ==3)
			{
				
				lbl.Visible =true;
				RBLCC1.SelectedIndex =2;	
				
			} 
			else
			{
				lbl.Visible =false;
			}

			if(RBLCC2.SelectedIndex ==0 && RBLPP2.SelectedIndex ==0)
			{
				
				lbl.Visible =true;
				if(RBLCC2.Items.Count >1)
				{
					RBLCC2.SelectedIndex =1;
				}
				else
				{
					RBLCC2.SelectedIndex =0;
				}
			}
			else if(RBLCC2.SelectedIndex ==1 && RBLPP2.SelectedIndex ==1)
			{
				
				lbl.Visible =true;
				if(RBLCC2.Items.Count >2)
				{
					RBLCC2.SelectedIndex =2;
				}
				else
				{
					RBLCC2.SelectedIndex =0;	
				}
			} 
			else if(RBLCC2.SelectedIndex ==2 && RBLPP2.SelectedIndex ==2)
			{
				
				lbl.Visible =true;
				if(RBLCC2.Items.Count >3)
				{
					RBLCC2.SelectedIndex =3;
				}
				else
				{
					RBLCC2.SelectedIndex =1;	
				}
			} 
			else if(RBLCC2.SelectedIndex ==3 && RBLPP2.SelectedIndex ==3)
			{
				
				lbl.Visible =true;
				RBLCC2.SelectedIndex =2;	
				
			} 
			else
			{
				lbl.Visible =false;
			}

		}

		private void RBLPP1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lbl.Visible =false;
			if(RBLPP1.SelectedIndex ==0)
			{
				if(RBLCC1.Items.Count >1)
				{
					if(RBLCC1.SelectedIndex ==0 )
					{
						RBLCC1.SelectedIndex =1;
					}
				}
				else
				{
					RBLCC1.SelectedIndex =0;
				}
			}
			else
				if(RBLPP1.SelectedIndex ==1)
			{
				if(RBLCC1.Items.Count >2)
				{
					if(RBLCC1.SelectedIndex ==1)
					{
						RBLCC1.SelectedIndex =2;
					}
				}
				else
				{
					RBLCC1.SelectedIndex =0;
				}
			}
			else
				if(RBLPP1.SelectedIndex ==2)
			{
				if(RBLCC1.Items.Count >3)
				{
					if(RBLCC1.SelectedIndex ==2)
					{
						RBLCC1.SelectedIndex =3;
					}
				}
				else if(RBLCC1.Items.Count >2)
				{
					if(RBLCC1.SelectedIndex ==2)
					{
						RBLCC1.SelectedIndex =1;
					}
				}
				else
				{
					RBLCC1.SelectedIndex =0;
				}
			}
			else
				if(RBLPP1.SelectedIndex ==3)
			{
				if(RBLCC1.Items.Count >3)
				{
					if(RBLCC1.SelectedIndex ==3)
					{
						RBLCC1.SelectedIndex =2;
					}
				}
				else
				{
					RBLCC1.SelectedIndex =0;
				}
			}
			if(RBLPP1.SelectedItem.Value.Trim() =="M" && RBLPP2.SelectedItem.Value.Trim() =="M")
			{
				RBLPP2.SelectedIndex =(RBLPP2.SelectedIndex  -1);
			}
			
		}

		private void RBLPP2_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lbl.Visible =false;
			if(RBLPP2.SelectedIndex ==0)
			{
				if(RBLCC2.Items.Count >1)
				{
					if(RBLCC2.SelectedIndex ==0 )
					{
						RBLCC2.SelectedIndex =1;
					}
				}
				else
				{
					RBLCC2.SelectedIndex =0;
				}
			}
			else
				if(RBLPP2.SelectedIndex ==1)
			{
				if(RBLCC2.Items.Count >2)
				{
					if(RBLCC2.SelectedIndex ==1)
					{
						RBLCC2.SelectedIndex =2;
					}
				}
				else
				{
					RBLCC2.SelectedIndex =0;
				}
			}
			else
				if(RBLPP2.SelectedIndex ==2)
			{
				if(RBLCC2.Items.Count >3)
				{
					if(RBLCC2.SelectedIndex ==2)
					{
						RBLCC2.SelectedIndex =3;
					}
				}
				else if(RBLCC2.Items.Count >2)
				{
					if(RBLCC2.SelectedIndex ==2)
					{
						RBLCC2.SelectedIndex =1;
					}
				}
				else
				{
					RBLCC2.SelectedIndex =0;
				}
			}
			else
				if(RBLPP2.SelectedIndex ==3)
			{
				if(RBLCC2.Items.Count >3)
				{
					if(RBLCC2.SelectedIndex ==3)
					{
						RBLCC2.SelectedIndex =2;
					}
				}
				else
				{
					RBLCC2.SelectedIndex =0;
				}
			}
			if(RBLPP1.SelectedItem.Value.Trim() =="M" && RBLPP2.SelectedItem.Value.Trim() =="M")
			{
				RBLPP1.SelectedIndex =(RBLPP1.SelectedIndex  -1);
			}
		}

		private void RBLCC1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(RBLCC1.SelectedIndex ==0 && RBLPP1.SelectedIndex ==0)
			{
				lbl.Text="Please select a diffrent position other than port position";
				lbl.Visible =true;
				if(RBLCC1.Items.Count >1)
				{
					RBLCC1.SelectedIndex =1;
				}
				else
				{
					RBLCC1.SelectedIndex =0;
				}
			}
			else if(RBLCC1.SelectedIndex ==1 && RBLPP1.SelectedIndex ==1)
			{
				lbl.Text="Please select a diffrent position other than port position";
				lbl.Visible =true;
				if(RBLCC1.Items.Count >2)
				{
					RBLCC1.SelectedIndex =2;
				}
				else
				{
					RBLCC1.SelectedIndex =0;	
				}
			} 
			else if(RBLCC1.SelectedIndex ==2 && RBLPP1.SelectedIndex ==2)
			{
				lbl.Text="Please select a diffrent position other than port position";
				lbl.Visible =true;
				if(RBLCC1.Items.Count >3)
				{
					RBLCC1.SelectedIndex =3;
				}
				else
				{
					RBLCC1.SelectedIndex =1;	
				}
			} 
			else if(RBLCC1.SelectedIndex ==3 && RBLPP1.SelectedIndex ==3)
			{
				lbl.Text="Please select a diffrent position other than port position";
				lbl.Visible =true;
				RBLCC1.SelectedIndex =2;	
				
			} 
			else
			{
				lbl.Visible =false;
			}

		}

		private void RBLCC2_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(RBLCC2.SelectedIndex ==0 && RBLPP2.SelectedIndex ==0)
			{
				lbl.Text="Please select a diffrent position other than port position";
				lbl.Visible =true;
				if(RBLCC2.Items.Count >1)
				{
					RBLCC2.SelectedIndex =1;
				}
				else
				{
					RBLCC2.SelectedIndex =0;
				}
			}
			else if(RBLCC2.SelectedIndex ==1 && RBLPP2.SelectedIndex ==1)
			{
				lbl.Text="Please select a diffrent position other than port position";
				lbl.Visible =true;
				if(RBLCC2.Items.Count >2)
				{
					RBLCC2.SelectedIndex =2;
				}
				else
				{
					RBLCC2.SelectedIndex =0;	
				}
			} 
			else if(RBLCC2.SelectedIndex ==2 && RBLPP2.SelectedIndex ==2)
			{
				lbl.Text="Please select a diffrent position other than port position";
				lbl.Visible =true;
				if(RBLCC2.Items.Count >3)
				{
					RBLCC2.SelectedIndex =3;
				}
				else
				{
					RBLCC2.SelectedIndex =1;	
				}
			} 
			else if(RBLCC2.SelectedIndex ==3 && RBLPP2.SelectedIndex ==3)
			{
				lbl.Text="Please select a diffrent position other than port position";
				lbl.Visible =true;
				RBLCC2.SelectedIndex =2;	
				
			} 
			else
			{
				lbl.Visible =false;
			}

		}

		private void LBBack_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Icylinder_page3.aspx");
		}

		private void BtnNext_Click(object sender, System.EventArgs e)
		{
			if(RBLPorts.SelectedIndex !=-1 && RBLCusions.SelectedIndex !=-1 && 
				RBLCC1.SelectedIndex !=-1 && RBLCC2.SelectedIndex !=-1 && RBLPP1.SelectedIndex !=-1 
				&& RBLPP2.SelectedIndex !=-1  && RBLSeal.SelectedIndex !=-1)
			{
				cd1=new coder();
				cd1 =(coder)Session["Coder"];
				Session["Coder"] =null;
				cd1.Port_Type =RBLPorts.SelectedItem.Value.Trim();
				cd1.Port_Pos =RBLPP1.SelectedItem.Value.Trim() + RBLPP2.SelectedItem.Value.Trim();
				cd1.Cushions =RBLCusions.SelectedItem.Value.Trim();
				if(cd1.Cushions.Trim() !="8" && cd1.Cushions.Trim() !="9" && cd1.Cushions.Trim() !="1" && cd1.Cushions.Trim() !="2" &&
					cd1.Cushions.Trim() !="3" && cd1.Cushions.Trim() !="4")
				{
					cd1.CushionPosition ="C"+RBLCC1.SelectedItem.Value.Trim() + RBLCC2.SelectedItem.Value.Trim();
				}
				else
				{
					cd1.CushionPosition ="";
				}
				cd1.Seal_Comp=RBLSeal.SelectedItem.Value.Trim();
				Session["Coder"] =cd1;
				Response.Redirect("Icylinder_PageAcc.aspx");
			}
		}

		private void LBAdvanced_Click(object sender, System.EventArgs e)
		{
			if(RBLPorts.SelectedIndex !=-1 && RBLCusions.SelectedIndex !=-1 && 
				RBLCC1.SelectedIndex !=-1 && RBLCC2.SelectedIndex !=-1 && RBLPP1.SelectedIndex !=-1 
				&& RBLPP2.SelectedIndex !=-1  && RBLSeal.SelectedIndex !=-1)
			{
				cd1=new coder();
				cd1 =(coder)Session["Coder"];
				Session["Coder"] =null;
				cd1.Port_Type =RBLPorts.SelectedItem.Value.Trim();
				cd1.Port_Pos =RBLPP1.SelectedItem.Value.Trim() + RBLPP2.SelectedItem.Value.Trim();
				cd1.Cushions =RBLCusions.SelectedItem.Value.Trim();
				if(cd1.Cushions.Trim() !="8" && cd1.Cushions.Trim() !="9" && cd1.Cushions.Trim() !="1" && cd1.Cushions.Trim() !="2" &&
					cd1.Cushions.Trim() !="3" && cd1.Cushions.Trim() !="4")
				{
					cd1.CushionPosition ="C"+RBLCC1.SelectedItem.Value.Trim() + RBLCC2.SelectedItem.Value.Trim();
				}
				else
				{
					cd1.CushionPosition ="";
				}
				cd1.Seal_Comp=RBLSeal.SelectedItem.Value.Trim();
				Session["Coder"] =cd1;
				Response.Redirect("Icylinder_Page5.aspx");
			}
		}
	}
}
