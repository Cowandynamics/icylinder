using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Text;
namespace iCylinderV1
{
	/// <summary>
	/// Summary description for WayValve_BatchQuote.
	/// </summary>
	public class WayValve_BatchQuote : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblspecsheet;
		protected System.Web.UI.WebControls.Label LblPartNo;
		protected System.Web.UI.WebControls.Label lblstatus;
		protected System.Web.UI.WebControls.Label lbltable;
		protected System.Web.UI.WebControls.Label lbltotal;
		protected System.Web.UI.WebControls.Label lblmgrp;
		protected System.Web.UI.WebControls.Label LblView;
		protected System.Web.UI.WebControls.Label lblup;
		protected System.Web.UI.WebControls.LinkButton LBSend;
		protected System.Web.UI.WebControls.TextBox TxtSpecialReq;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.DropDownList DDLPriority;
		protected System.Web.UI.WebControls.Label Label58;
		protected System.Web.UI.WebControls.Label Label57;
		protected System.Web.UI.WebControls.Panel PSend;
		protected System.Web.UI.WebControls.Label lblerr;
		protected System.Web.UI.WebControls.LinkButton BtnQuoteGenerate;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Panel Panel2;
		protected System.Web.UI.WebControls.Label lblpage;
		protected System.Web.UI.WebControls.LinkButton LBUpload;
		protected System.Web.UI.WebControls.Label LblUpload;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.TextBox TxtProjectNo;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.HyperLink HyperLink1;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Panel Panel1;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.HtmlControls.HtmlInputFile FileUpload;
		protected System.Web.UI.HtmlControls.HtmlInputFile FileUploadSpec;
	
		public string Qno(string use)
		{
			try
			{
				DBClass db=new DBClass();
				string qno ="";
				string count = "";
				string usr=use.ToUpper();
				string st= usr.Substring(0,1);
				string s1=DateTime.Today.Month.ToString();
				count=db.SelectValue("QuoteNo","WEB_QuoteCount_TableV1","SLNo","1");
				if(count.Length !=0)
				{
					int lst=Convert.ToInt32(count.Substring(5));
				
				
					string s=DateTime.Today.Month.ToString();
					if(s.Length ==1)
					{
						s="0"+DateTime.Today.Month.ToString();
					}
					else
					{
						s=DateTime.Today.Month.ToString();
					}
					if(count !="")
					{
						if(count.Substring(1,2).Equals(DateTime.Today.Year.ToString().Substring(2)))
						{
							if(count.Substring(3,2).Equals(s))
							{
								lst++;
								string num="";
								if(lst.ToString().Length ==4)
								{
									num=lst.ToString();
								}
								else if(lst.ToString().Length ==3)
								{
									num="0"+lst.ToString();
								}
								else if(lst.ToString().Length ==2)
								{
									num="00"+lst.ToString();
								}
								else 
								{
									num="000"+lst.ToString();
								}
							
								if(s1.Length ==1)
								{
									s1="0"+DateTime.Today.Month.ToString();
								}
								else
								{
									s1=DateTime.Today.Month.ToString();
								}
								qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+num.ToString();
							}
							else
							{
						
								if(s1.Length ==1)
								{
									s1="0"+DateTime.Today.Month.ToString();
								}
								else
								{
									s1=DateTime.Today.Month.ToString();
								}
								qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+"0001";
							}
	
						}
						else
						{
						
							if(s1.Length ==1)
							{
								s1="0"+DateTime.Today.Month.ToString();
							}
							else
							{
								s1=DateTime.Today.Month.ToString();
							}
							qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+"0001";
						}
					}
					else
					{
					
						if(s1.Length ==1)
						{
							s1="0"+DateTime.Today.Month.ToString();
						}
						else
						{
							s1=DateTime.Today.Month.ToString();
						}
						qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+"0001";
					}
				}
				else
				{
					qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+"0001";
				}
				string sav=db.InsertQuoteNo(qno);
				db.InsertQuoteCount(qno);
				return qno;
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s=s.Replace("'"," ");
				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				return null;
			}		
		}
		
		public void APricing(coder PN)
		{
			try
			{
				DBClass db=new DBClass();
				string commn="";				
				ArrayList lst =new ArrayList();
				lst =(ArrayList)Session["User"];
				string ind="0";
				ind=db.SelectPriceIndex("A").ToString();
				decimal index =0.00m;
				index =Convert.ToDecimal(ind.Trim());				
				commn="WEB_SeriesACommAdders_TableV1";
				lblmgrp.Text="AP_Base";
				lbltable.Text="WEB_APrice_Master_TableV1";
				if(PN.DoubleRod.ToString().Trim() =="Yes")
				{
					lblmgrp.Text="AP_Base";
					lbltable.Text="WEB_ADPrice_Master_TableV1";
				}				
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal seal=0.00m;

				st =Convert.ToDecimal(PN.Stroke);
				sp =db.SelectStrokeA(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				sp =Decimal.Round(sp,2);
				
				decimal temp =sp * st;		
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				discount=Convert.ToDecimal(db.SelectDiscount(lst[1].ToString().Trim(),"A"));
				sprice =temp*(1 - (discount/100));
				sprice =Decimal.Round(sprice,2);
				//				lblstroke.Text=sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="true";
				}
				//mount price
				decimal mp =0.00m;
				string mgrp = lblmgrp.Text.ToString();
				mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100));
				mprice=Decimal.Round(mp,2); 
				if(PN.Mount.Trim() =="NX1" ||PN.Mount.Trim() =="NX3")
				{
					mprice=mprice + 5.00m;
				}
				if(PN.Mount.Trim().Substring(0,1) =="I" || PN.Mount.Trim().Substring(0,1) =="M" )
				{
					decimal a1=0.00m;
					a1 =db.SelectISSMSSPrice(PN.Mount.Trim());
					if(a1 >0)
					{
						a1 =a1 + a1 * (index /100);
						a1= a1*(1 - (discount/100)); 
						a1 =Decimal.Round(a1,2);
						mprice +=a1;
					}
				}
				else if(PN.Mount.Trim().Substring(0,2) =="GR" )
				{
					decimal a1=0.00m;
					a1 =db.SelectGRPrice(PN.Mount.Trim());
					if(a1 >0)
					{
						a1 =a1 + a1 * (index /100);
						a1= a1*(1 - (discount/100)); 
						a1 =Decimal.Round(a1,2);
						mprice +=a1;
					}
				}
				if(mprice ==0)
				{
					lblstatus.Text ="true";
				}
				if(PN.Seal_Comp.ToString().Trim() =="L")
				{
					seal =db.SelectAddersPrice("SL",commn.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="true";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="F")
				{
					seal =db.SelectAddersPrice("SF",commn.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="true";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="E")
				{
					seal =db.SelectAddersPrice("SE",commn.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="true";
					}
				}
				seal=Decimal.Round(seal,2);

				decimal rodend=0.00m;
				if(PN.Rod_End.ToString().Substring(0,1) !="N" &&PN.Rod_End.ToString().Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd",commn.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="true";
					}
				}				
				rodend=Decimal.Round(rodend,2);
				seal =seal + seal * (index /100);
				rodend =rodend + rodend * (index /100);
				//issue #315 start
				seal = seal*(1 - (discount/100));
				seal = Decimal.Round(seal,2);
				rodend = rodend*(1 - (discount/100));
				rodend = Decimal.Round(rodend,2);
				//issue #315 end
				decimal total=0.00m;
				total=sprice + mprice +seal + rodend  ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}
		public decimal MLPricing(coder PN)
		{
			decimal total=0.00m;
			try
			{				
				if(PN.Style.ToString().Trim()=="N")
				{
					DBClass db=new DBClass();	
					string pindex="0";
					pindex=db.SelectPriceIndex("ML").ToString();
					//issue #264 start
					//back
					//string comtable="WEB_SeriesML_CommAdders_TableV1";					
					//update
					string comtable="WEB_SeriesMLCommAdders_TableV1";					
					//issue #264 end
					lblmgrp.Text="MLSMount_Grp";
					lbltable.Text="WEB_MLPrice_Master_TableV1";
				
					decimal strokeprice =0.00m;
					decimal stroke =0.00m;
					decimal mountprice =0.00m;
					decimal cushionprice =0.00m;
					decimal sealprice=0.00m;
					decimal temp=0;
					decimal index =0.00m;
					
					if(PN.Bore_Size.ToString().Trim()=="C" || PN.Bore_Size.ToString().Trim()=="D" || PN.Bore_Size.ToString().Trim()=="E")
					{			
						index =Convert.ToDecimal(db.SelectPriceIndex("MLS").ToString());
					}
					else
					{
						index =Convert.ToDecimal(db.SelectPriceIndex("ML").ToString());
					}

					decimal discount =0.00m;
					decimal qty=0;
					qty=Convert.ToDecimal(PN.Qty.ToString());
					if(qty >0 && qty <=2)
					{
						discount=35;
					}
					else if(qty >=3 && qty <=5)
					{
						discount=37;
					}
					else if(qty >=6 && qty <=10)
					{
						discount=40;
					}
					else if(qty >10)
					{
						discount=45;
					}

					//stroke price 
					stroke =Convert.ToDecimal(PN.Stroke.ToString().Trim());
					strokeprice =db.SelectStrokePriceML(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					strokeprice =Decimal.Round(strokeprice,2);				
					temp =stroke * strokeprice;			
					temp =temp + temp * (index /100);
					temp =temp *(1 - (discount/100)); 
					strokeprice =Decimal.Round(temp,2);
					if(strokeprice ==0)
					{
						lblstatus.Text ="true";
					}

					//mount price		
					temp=0;
					string mgrp ="";
					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
					temp =db.SelectMountPriceML(mgrp.ToString(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					temp =temp + temp * (index /100);
					temp =temp *(1 - (discount/100)); 
					mountprice=Decimal.Round(temp,2); 				
					if(mountprice ==0)
					{
						lblstatus.Text ="true";
					}

					//cushion price
					temp=0;					
					temp =db.SelectCushionPriceML(lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());						
					temp =temp + temp * (index /100);	
					if(PN.Cushions.ToString() =="5")
					{
						temp =temp * 2;
						temp = temp * (1 - (discount/100));		
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}
					else if(PN.Cushions.ToString() =="8")
					{
						temp=0;
					}
					else
					{
						temp = temp*(1 - (discount/100)); 	
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}			
					cushionprice =Decimal.Round(temp,2);					

					//seal price
					temp=0;
					if(PN.Seal_Comp.ToString().Trim() =="L")
					{
						temp =db.SelectAddersPrice("SL",comtable.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());		
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}
					else if(PN.Seal_Comp.ToString().Trim() =="F")
					{
						temp =db.SelectAddersPrice("SF",comtable.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}
					temp =temp + temp * (index /100);	
					sealprice=Decimal.Round(temp,2);	
					
					total=strokeprice + mountprice + cushionprice + sealprice;
					lbltotal.Text=total.ToString();
				}
				else
				{
					lbltotal.Text="0.00";
				}
				return total;
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				return total;
			}
		}
		public decimal LPricing(coder PN)
		{
			decimal total=0.00m;
			try
			{				
				if(PN.Style.ToString().Trim()=="N")
				{
					DBClass db=new DBClass();	
					string pindex="0";
					pindex=db.SelectPriceIndex("L").ToString();
					string comtable="WEB_SeriesLCommAdders_TableV1";					
					lblmgrp.Text="LMount_Grp";
					lbltable.Text="WEB_LPrice_Master_TableV1";
				
					decimal strokeprice =0.00m;
					decimal stroke =0.00m;
					decimal mountprice =0.00m;
					decimal sealprice=0.00m;
					decimal temp=0;
					decimal index =0.00m;
					index =Convert.ToDecimal(pindex.Trim());

					decimal discount =0.00m;
					decimal qty=0;
					qty=Convert.ToDecimal(PN.Qty.ToString());
					if(qty >0 && qty <=2)
					{
						discount=35;
					}
					else if(qty >=3 && qty <=5)
					{
						discount=37;
					}
					else if(qty >=6 && qty <=10)
					{
						discount=40;
					}
					else if(qty >10)
					{
						discount=45;
					}

					//stroke price 
					stroke =Convert.ToDecimal(PN.Stroke.ToString().Trim());
					strokeprice =db.SelectStrokePriceL(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					strokeprice =Decimal.Round(strokeprice,2);				
					temp =stroke * strokeprice;			
					temp =temp + temp * (index /100);
					temp =temp *(1 - (discount/100)); 
					strokeprice =Decimal.Round(temp,2);
					if(strokeprice ==0)
					{
						lblstatus.Text ="true";
					}

					//mount price		
					temp=0;
					string mgrp ="";
					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
					temp =db.SelectMountPrice(mgrp.ToString(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					temp =temp + temp * (index /100);
					temp =temp *(1 - (discount/100)); 
					mountprice=Decimal.Round(temp,2); 				
					if(mountprice ==0)
					{
						lblstatus.Text ="true";
					}
				
					//seal price
					temp=0;
					if(PN.Seal_Comp.ToString().Trim() =="L")
					{
						temp =db.SelectAddersPrice("SL",comtable.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());		
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}
					else if(PN.Seal_Comp.ToString().Trim() =="F")
					{
						temp =db.SelectAddersPrice("SF",comtable.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());	
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}
					temp =temp + temp * (index /100);	
					sealprice=Decimal.Round(temp,2);	
					
					total=strokeprice + mountprice + sealprice ;
					lbltotal.Text=total.ToString();
				}
				else
				{
					lbltotal.Text="0.00";
				}
				return total;
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				return total;
			}
		}
		//		public void MLPricing(coder PN, string qtydiscount)
//		{
//			try
//			{
//				DBClass db=new DBClass();
//				lblmgrp.Text="MLSMount_Grp";
//				lbltable.Text="WEB_MLPrice_Master_TableV1";
//				if(PN.DoubleRod.ToString().Trim()=="Yes")
//				{
//					lblmgrp.Text="MLDMount_Grp";
//					lbltable.Text="WEB_MLDPrice_Master_TableV1";						
//				}
//				else
//				{
//					lblmgrp.Text="MLSMount_Grp";
//					lbltable.Text="WEB_MLPrice_Master_TableV1";
//				}
//				string allind="";
//				allind=db.SelectPriceIndex("ADDER").ToString();
//				decimal addinx=0.00m;
//				addinx=Convert.ToDecimal(allind);
//
//				//stroke price
//				decimal sp =0.00m;
//				decimal st =0.00m;
//				decimal sprice =0.00m;
//				decimal mprice =0.00m;
//				decimal cprice =0.00m;
//				decimal seal=0.00m;
//				decimal rodend=0.00m;
//
//				st =Convert.ToDecimal(PN.Stroke);
//				sp =db.SelectStrokePriceML(lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
//				sp =Decimal.Round(sp,2);
//				decimal temp =sp * st;
//				decimal index =0.00m;
//				index =Convert.ToDecimal(db.SelectPriceIndex("ML").ToString());
//				temp =temp + temp * (index /100);
//				decimal discount =0.00m;
//				discount=Convert.ToDecimal(qtydiscount.ToString());
//				sprice =temp*(1 - (discount/100)); 
//				sprice =Decimal.Round(sprice,2);
//				if(sprice ==0)
//				{
//					lblstatus.Text ="true";
//				}
//				//mount price
//				
//				decimal mp =0.00m;
//				try
//				{
//					string mgrp ="";
//					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
//					mp =db.SelectMountPriceML(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
//				}
//				catch(Exception ex) 
//				{
//					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
//				}
//				mp =mp + mp * (index /100);
//				mp= mp*(1 - (discount/100)); 
//				mprice = Decimal.Round(mp,2);
//				if(mprice ==0)
//				{
//					lblstatus.Text ="true";
//				}
//				// cushin price
//				decimal cp =0.00m;
//				try
//				{
//					cp =db.SelectCushionPriceML(lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());					
//				}
//				catch(Exception ex) 
//				{
//					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
//				}
//				cp =cp + cp * (index /100);	
//				if(PN.Cushions.ToString() =="5")
//				{
//					decimal temp1 =cp * 2;
//					temp1 = temp1*(1 - (discount/100));
//					cprice =Decimal.Round(temp1,2);
//					if(cprice ==0)
//					{
//						lblstatus.Text ="true";
//					}					
//				}
//				else if(PN.Cushions.ToString() =="8")
//				{
//					cprice =0.00m;
//					cprice =Decimal.Round(cprice,2);
//				}
//				else
//				{
//					cprice=cp;
//					cprice = cprice*(1 - (discount/100)); 
//					cprice =Decimal.Round(cprice,2);
//					if(cprice ==0)
//					{
//						lblstatus.Text ="true";
//					}
//				}			
//				cprice =Decimal.Round(cprice,2);				
//				if(PN.Seal_Comp.ToString().Trim() =="L")
//				{
//					seal =db.SelectAddersPrice("SL","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
//					if(seal ==0)
//					{
//						lblstatus.Text ="true";
//					}				
//				}
//				else if(PN.Seal_Comp.ToString().Trim() =="F")
//				{
//					seal =db.SelectAddersPrice("SF","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
//					if(seal ==0)
//					{
//						lblstatus.Text ="true";
//					}
//				}
//				else if(PN.Seal_Comp.ToString().Trim()=="W")
//				{
//					seal =db.SelectAddersPrice("SW","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
//					if(seal ==0)
//					{
//						lblstatus.Text ="true";
//					}
//				}
//				else if(PN.Seal_Comp.ToString().Trim() =="E")
//				{
//					seal =db.SelectAddersPrice("SE","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
//					if(seal ==0)
//					{
//						lblstatus.Text ="true";
//					}
//				}
//				seal = Decimal.Round(seal,2);
//				if(PN.Rod_End.Trim().Substring(0,1) !="N" && PN.Rod_End.Trim().Substring(0,1) !="A")
//				{
//					rodend =db.SelectAddersPrice("MetricRodEnd","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
//					if(rodend ==0)
//					{
//						lblstatus.Text ="true";
//					}
//				}				
//				rodend=Decimal.Round(rodend,2);
//				seal =seal + seal * (index /100);
//				rodend =rodend + rodend * (index /100);				
//				decimal total=0.00m;
//				total=sprice + mprice + cprice +seal + rodend ;
//				lbltotal.Text=total.ToString();
//			}
//			catch(Exception ex)
//			{
//				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
//				s=s.Replace("'"," ");
//				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
//			}				
//		}
//		public void LPricing(coder PN , string qtydiscount)
//		{
//			try
//			{
//				DBClass db=new DBClass();
//				lblmgrp.Text="LMount_Grp";
//				lbltable.Text="WEB_LPrice_Master_TableV1";
//				if(PN.DoubleRod.ToString().Trim()=="Yes")
//				{
//					return;				
//				}
//				else
//				{
//					lblmgrp.Text="LMount_Grp";
//					lbltable.Text="WEB_LPrice_Master_TableV1";
//				}
//				string allind="";
//				allind=db.SelectPriceIndex("ADDER").ToString();
//				decimal addinx=0.00m;
//				addinx=Convert.ToDecimal(allind);
//				//stroke price
//				decimal sp =0.00m;
//				decimal st =0.00m;
//				decimal sprice =0.00m;
//				decimal mprice =0.00m;
//				decimal seal=0.00m;
//				decimal rodend=0.00m;
//
//				st =Convert.ToDecimal(PN.Stroke);
//				sp =db.SelectStrokeLprice(lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
//				sp =Decimal.Round(sp,2);
//				
//				decimal temp =sp * st;
//				decimal index =0.00m;
//				index =Convert.ToDecimal(db.SelectPriceIndex("L").ToString());
//				temp =temp + temp * (index /100);
//				decimal discount =0.00m;
//				discount=Convert.ToDecimal(qtydiscount);
//				sprice =temp*(1 - (discount/100)); 
//				sprice =Decimal.Round(sprice,2);
//				if(sprice ==0)
//				{
//					lblstatus.Text ="true";
//				}
//			
//				//mount price
//				string mgrp ="";
//				decimal mp =0.00m;
//				try
//				{
//					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
//					if(mgrp.ToString() !="" || mgrp.ToString() !=null)
//					{
//						mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
//					}
//				}
//				catch(Exception ex) 
//				{
//					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
//				}
//				mp =mp + mp * (index /100);
//				mp= mp*(1 - (discount/100)); 				
//				mprice=Decimal.Round(mp,2);
//				if(mprice ==0)
//				{
//					lblstatus.Text ="true";
//				}
//				if(PN.Seal_Comp.ToString().Trim() =="L")
//				{
//					seal =db.SelectAddersPrice("SL","WEB_SeriesLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
//					if(seal ==0)
//					{
//						lblstatus.Text ="true";
//					}
//				
//				}
//				else if(PN.Seal_Comp.ToString().Trim() =="F")
//				{
//					seal =db.SelectAddersPrice("SF","WEB_SeriesLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
//					if(seal ==0)
//					{
//						lblstatus.Text ="true";
//					}
//				}
//				else if(PN.Seal_Comp.ToString().Trim()=="W")
//				{
//					seal =db.SelectAddersPrice("SW","WEB_SeriesLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
//					if(seal ==0)
//					{
//						lblstatus.Text ="true";
//					}
//				}
//				else if(PN.Seal_Comp.ToString().Trim() =="E")
//				{
//					seal =db.SelectAddersPrice("SE","WEB_SeriesLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
//					if(seal ==0)
//					{
//						lblstatus.Text ="true";
//					}
//				}
//				seal = Decimal.Round(seal,2);
//				if(PN.Rod_End.Trim().Substring(0,1) !="N" && PN.Rod_End.Trim().Substring(0,1) !="A")
//				{
//					rodend =db.SelectAddersPrice("MetricRodEnd","WEB_SeriesLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
//					if(rodend ==0)
//					{
//						lblstatus.Text ="true";
//					}
//				}				
//				rodend=Decimal.Round(rodend,2);
//				seal =seal + seal * (index /100);
//				rodend =rodend + rodend * (index /100);				
//				decimal total=0.00m;
//				total=sprice + mprice +seal + rodend  ;
//				lbltotal.Text=total.ToString();
//			}
//			catch(Exception ex)
//			{
//				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
//				s=s.Replace("'"," ");
//				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
//			}
//		}

		public static bool IsNumeric(string strInteger) 
		{
			try 
			{
				if(strInteger.Trim() =="")
				{
					return false;
				}
				else
				{
					int intTemp =0;
					if(strInteger.ToString().StartsWith(".") == true)
					{
						for(int i=1; i< strInteger.Length;i++)
						{
							intTemp = Int32.Parse( strInteger.Substring(i,1) );
						}
					}
					else
					{
						for(int i=0; i< strInteger.Length;i++)
						{
							if (strInteger.ToString().Substring(i,1) !=".")
							{
								intTemp = Int32.Parse( strInteger.Substring(i,1) );
							}
						}
					}
					return true;
				}				
			} 
			catch (FormatException) 
			{
				return false;
			}    
		}
		private string UploadFileSpec(object Sender,EventArgs E)
		{
			string file="";
			if (FileUploadSpec.PostedFile !=null) //Checking for valid file
			{	
				string name=DateTime.Today.ToShortDateString().Replace("/","").Replace(" ","");
				name += DateTime.Now.Ticks.ToString();//.Replace("/","").Replace(" ","").Replace(":","").Replace("","");
				string StrFileName = name+".pdf" ;
				int IntFileSize =FileUploadSpec.PostedFile.ContentLength;
				if (IntFileSize <=0)
				{
					lblpage.Text="Uploading of file " + StrFileName + " failed ";
				}
				else
				{
					FileUploadSpec.PostedFile.SaveAs(Server.MapPath("./specsheet/" + StrFileName.Trim()));
					file=StrFileName.ToString();
				}
			}
			return file;
		}

		private string UploadFileTemp_Customer(object Sender,EventArgs E)
		{
			string file="";
			if (FileUpload.PostedFile !=null) //Checking for valid file
			{	
				string name=DateTime.Today.ToShortDateString().Replace("/","").Replace(" ","");
				name += DateTime.Now.Ticks.ToString();//.Replace("/","").Replace(" ","").Replace(":","").Replace("","");
				string StrFileName = name+".xls" ;
				int IntFileSize =FileUpload.PostedFile.ContentLength;
				if (IntFileSize <=0)
				{
					lblpage.Text="Uploading of file " + StrFileName + " failed ";
				}
				else
				{
					FileUpload.PostedFile.SaveAs(Server.MapPath("./dbfile/" + StrFileName.Trim()));
					file=StrFileName.ToString();
				}
			}
			return file;
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			LblView.Text="";
			if(! IsPostBack)
			{
				LblUpload.Text="";
				StringBuilder disableButton = new StringBuilder();
				disableButton.Append("if (typeof(Page_ClientValidate) == 'function') { ");
				disableButton.Append("if (Page_ClientValidate() == false) { return false; }} ");
				disableButton.Append("this.value = 'Please Wait...';");
				disableButton.Append("this.disabled = true;");
				disableButton.Append(this.Page.GetPostBackEventReference(BtnQuoteGenerate));
				disableButton.Append(";"); 
				BtnQuoteGenerate.Attributes.Add("onclick", disableButton.ToString());
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.LBUpload.Click += new System.EventHandler(this.LBUpload_Click);
			this.BtnQuoteGenerate.Click += new System.EventHandler(this.BtnQuoteGenerate_Click);
			this.LBSend.Click += new System.EventHandler(this.LBSend_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void LBUpload_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(FileUpload.Value.Trim() !="")
				{
					lblup.Text="";
					lblerr.Text="";
					if(FileUpload.PostedFile.ContentType.ToString() =="application/vnd.ms-excel" || FileUpload.PostedFile.ContentType.ToString() =="application/octet-stream")
					{
						lblpage.Text="";
						string upload="";
						if(FileUpload.Value.Trim()!="")
						{
							upload=UploadFileTemp_Customer(sender,e);
							LblUpload.Text=FileUpload.PostedFile.FileName.ToString();
						}
						if(upload.Trim() !="")
						{
							lblup.Text=upload.Trim();
							lblpage.Text="File uploaded successsfully <a href='ViewUploadedFile_Way.aspx?file="+upload.Trim()+"' target='_blank'>Click here to View</a>";
							LblView.Text="<script language='javascript'>window.open('ViewUploadedFile_Way.aspx?file="+upload.Trim()+"','_blank','toolbar=no,width=1000,height=600,resizable=yes,top=0,left=0')</script>";						
							DBClass db=new DBClass();
							ArrayList qlist=new ArrayList();
							qlist=db.Select_DataFrom_ImportFile_Way(Request.PhysicalApplicationPath+"dbfile/"+lblup.Text.Trim());
							if(qlist.Count >0)
							{
								bool good=true;
								for(int i=0;i<qlist.Count;i++)
								{
									VelanClass cd1=new VelanClass();
									cd1=(VelanClass)qlist[i];
									if(cd1 !=null)
									{
										if(cd1.Qty.Trim() == "")
										{
											good =false;
										}
										if(cd1.Style.ToString().Trim() =="")
										{
											good =false;
										}
										if(cd1.PneumaticHydraulic.ToString().Trim() =="")
										{
											good =false;
										}									
										if(IsNumeric(cd1.Stroke.Trim()) != true)
										{
											good =false;
										}
										//issue #263 start
										//backup
										//if(IsNumeric(cd1.Bore.Trim()) != true)
										//update
										if(IsNumeric(cd1.Bore.Trim()) != true && cd1.PneumaticHydraulic!="Pneumatic With Spring")
										//issue #263 end
										{									
											good =false;
										}
										else if(cd1.PneumaticHydraulic.ToUpper() =="PNEUMATIC")
										{
											if(Convert.ToDecimal(cd1.Bore.ToString()) <4.00m  && cd1.PneumaticHydraulic.ToUpper() =="PNEUMATIC")
											{
												good=false;
											}
										}
										if(db.SelectValue("Seal_Code","WEB_Seal_TableV1","Seal_Type",cd1.Seals.ToString().Trim()).Trim() =="")
										{
											good =false;
										}
										if(cd1.Paint.ToString().Trim() =="")
										{
											good =false;
										}
										//issue #242 start
										//issue #267 start
										//back
										//if((cd1.ETC.ToString().Trim() =="" || cd1.PackingFriction.ToString().Trim()=="" ||  cd1.ETO.ToString().Trim()=="" || cd1.AirPressure.ToString().Trim()=="") && (cd1.PneumaticHydraulic=="Pneumatic With Spring"))
										//update
										if((cd1.ETC.ToString().Trim() =="" || cd1.PackingFriction.ToString().Trim()=="" ||  cd1.ETO.ToString().Trim()=="" || cd1.AirPressure.ToString().Trim()=="" || cd1.SaftyFactor.ToString().Trim()=="" ) && (cd1.PneumaticHydraulic=="Pneumatic With Spring"))
										//issue #267 end
										{
											good =false;
										}
										if((cd1.PneumaticHydraulic=="Pneumatic With Spring") && (cd1.Style=="Double Acting"))
										{
											good =false;
										}
										//issue #242 end
									}
								}
								if(good ==false)
								{
									lblerr.Text="Error in excel file Please <a href='ViewUploadedFile_Way.aspx?file="+upload.Trim()+"' target='_blank'>Click here to View</a> the error in uploaded file.";
									lblup.Text="";
									lblpage.Text="";
									BtnQuoteGenerate.Visible=false;
									LblUpload.Text="";
								}
								else
								{
									lblerr.Text="";
									lblpage.Text="";
									BtnQuoteGenerate.Visible=true;
								}
							}
						}
					}
					else
					{
						lblpage.Text="Please select a excel file!!!";
					}
				}
				else
				{
					lblpage.Text="Please select a excel file!!!";
				}
			}
			catch(Exception ex)
			{
				string ee=ex.Message;
				lblup.Text="";
				LblUpload.Text="";
				lblpage.Text="";
				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Not a valid import file')</script>";
			}
		}
        
		//issue #263 start
		private decimal ASSeries_SP(coder PN, Quotation quot)
		{
			DBClass db=new DBClass(); 
			decimal sptotal=0.00m;
			if(PN.Specials.ToString().ToUpper().Trim() =="SPECIAL")
			{
				decimal total =0.00m;
				decimal t1=0.00m;
				decimal t2=0.00m;
				bool result1=false;
				string sp ="";
				string Portcode ="";
				int tt=Convert.ToInt32(db.SelectLastSpNo());
				sp=(tt+1).ToString();
				PN.PNO ="Z"+PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim()+
					PN.Mount.ToString().Trim()+ PN.TandemDuplex+PN.FailMode.Replace("FO","O").Replace("FC","C")+PN.CanisterNo+"-"+"/Z"+sp.ToString();
				LblPartNo.Text ="Z"+PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim()+
					PN.Mount.ToString().Trim()+ PN.TandemDuplex+PN.FailMode.Replace("FO","O").Replace("FC","C")+PN.CanisterNo+"-"+"/Z"+sp.ToString();
				Portcode=db.SelectOneValueFunction( PN.Bore_Size.Trim() ,"sp_Select_PortCode_WEB");
				//issue #421 start
				db.InsertSpecialCount(sp.Trim());	
				//issue #421 end
				//closingtime
				//closingtime
				string strClosingTime = "";
				if(PN.ClosingTime.ToString().Trim() !="")
				{
					decimal cl1 ,cl2=0.00m;
					cl1=Convert.ToDecimal(PN.Stroke.ToString());
					cl2=Convert.ToDecimal(PN.ClosingTime.ToString());
					if(cl1 > cl2)
					{
						lblstatus.Text ="true";
					}
					else
					{
						strClosingTime=" - Estimated closing time = " + Math.Ceiling(Convert.ToDouble(cl1)).ToString()+" s";
					}
				}	
				//end
				//specialsolenoid
				
				if(PN.Solenoid !=null)
				{
					if(PN.Solenoid.Trim() !="")
					{
						
						
						t1 =db.SelectAddersPrice_Specials_Velan(PN.Solenoid.Trim(),"WEB_Velan_SpecialAdder_Pricing_TableV1",Portcode.ToString().Trim());
						total =t1; 
						sptotal +=t1;
						total=Decimal.Round(t1,2);
						if(total ==0)
						{
							result1 =true;
						}
						string des="";
						des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Solenoid.Trim());
						string prt="";
						//prt=db.SelectValue("POrtSize","WEB_Velan_BorePort_TableV1","PortCode",Portcode.Trim());
						prt=db.SelectValue(PN.Port_Type.Trim(),"SeriesASPortSize_TableV1","Bore",PN.Bore_Size.Trim());
						des= des.Replace("#",prt.Trim());
						des=des+strClosingTime;
						string sp1 ="";
						int tt1=Convert.ToInt32(db.SelectLastSpNo());
						sp1=(tt1+1).ToString();
						db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Solenoid.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"2");
						db.InsertSpecialCount(sp1.Trim());						
					}
				}
				//specialfilterregulator
				if(PN.FilterRegulator !=null)
				{
					if(PN.FilterRegulator.Trim() !="")
					{
						t1 =db.SelectAddersPrice_Specials_Velan(PN.FilterRegulator.Trim(),"WEB_Velan_SpecialAdder_Pricing_TableV1",Portcode.ToString().Trim());
						total =t1; 
						sptotal +=t1;
						total=Decimal.Round(t1,2);
						if(total ==0)
						{
							result1 =true;
						}
						string des="";
						des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.FilterRegulator.Trim());
						string sp1 ="";
						int tt1=Convert.ToInt32(db.SelectLastSpNo());
						sp1=(tt1+1).ToString();
						db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.FilterRegulator.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"3");
						db.InsertSpecialCount(sp1.Trim());
					}
				}
				//specialtubing
				if(PN.Tubing !=null)
				{
					if(PN.Tubing.Trim() !="")
					{
						decimal t3=0.00m;
						t1 =db.SelectAddersPrice_Specials_Velan(PN.Tubing.Trim()+"Base","WEB_Velan_SpecialAdder_Pricing_TableV1",Portcode.ToString().Trim());
						t2 =db.SelectAddersPrice_Specials_Velan(PN.Tubing.Trim()+"PerInch","WEB_Velan_SpecialAdder_Pricing_TableV1",Portcode.ToString().Trim());
						t3=Convert.ToDecimal(PN.Stroke.ToString().Trim());
						total =t1 + (t2 * t3); 
						sptotal +=total;
						total=Decimal.Round(total,2);
						if(total ==0)
						{
							result1 =true;
						}	
						string des="";
						des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Tubing.Trim());
						string sp1 ="";
						int tt1=Convert.ToInt32(db.SelectLastSpNo());
						sp1=(tt1+1).ToString();
						db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Tubing.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"4");
						db.InsertSpecialCount(sp1.Trim());
					}
				}
				//specialmanualoverride
				if(PN.ManualOverride !=null)
				{
					if(PN.ManualOverride.Trim() !="")
					{
						total =0.00m;
						if(total ==0)
						{
							result1 =true;
						}
						string des="";
						des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.ManualOverride.Trim());
						string sp1 ="";
						int tt1=Convert.ToInt32(db.SelectLastSpNo());
						sp1=(tt1+1).ToString();
						db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.ManualOverride.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"5");
						db.InsertSpecialCount(sp1.Trim());
					}
				}
				//specialgt3scraper
				if(PN.MetalScrapper !=null)
				{
					if(PN.MetalScrapper.Trim() =="GT3")
					{
						total =0.00m;
						total=db.SelectAddersPrice("GT3","WEB_SeriesASCommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
						if(total ==0)
						{
							result1 =true;
						}
						string des="";
						des=db.SelectAppopts(PN.MetalScrapper.ToString().Trim());
						string sp1 ="";
						int tt1=Convert.ToInt32(db.SelectLastSpNo());
						sp1=(tt1+1).ToString();
						db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.ManualOverride.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"5");
						db.InsertSpecialCount(sp1.Trim());
					}
				}
				//specialcoating
				if(PN.Coating.ToString().Trim() =="C1")
				{
					t1=0;
					t1 =db.SelectAddersPrice_Specials_ByBore("C1","WEB_Specials_Velan_Pricing_TableV1",PN.Bore_Size.ToString().Trim());
					total =t1; 
					sptotal +=total;
					if(total ==0)
					{
						result1 =true;
					}
					string desc="";
					//desc=db.SelectPopopts(PN.Coating.Trim());
					//db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.Coating.ToString(),desc.ToString(),"0.00");
					desc=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Coating.Trim());;
					string sp1 ="";
					int tt1=Convert.ToInt32(db.SelectLastSpNo());
					sp1=(tt1+1).ToString();
					db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Coating.ToString(),desc.Trim(),"0","1","0","0","7");
					db.InsertSpecialCount(sp1.Trim());
				}
				if(PN.Coating.ToString().Trim() =="CTBA")
				{
					t1=0;
					//t1 =db.SelectAddersPrice_Specials_ByBore("C1","WEB_Specials_Velan_Pricing_TableV1",PN.Bore_Size.ToString().Trim());
					total =t1; 
					sptotal +=total;
					if(total ==0)
					{
						result1 =true;
					}
					string desc="";
					desc=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Coating.Trim());;
					//db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.Coating.ToString(),desc.ToString(),"0.00");
					string sp1 ="";
					int tt1=Convert.ToInt32(db.SelectLastSpNo());
					sp1=(tt1+1).ToString();
					db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Coating.ToString(),desc.Trim(),"0","1","0","0","7");
					db.InsertSpecialCount(sp1.Trim());
				}
				//issue #234 start
				if(PN.Coating.ToString().Trim() =="C9" || PN.Coating.ToString().Trim() =="C7")
				{
					if(PN.Coating.ToString().Trim() =="C9")
					{
						t1=0;
						t1 =db.SelectAddersPrice("C8","WEB_SeriesASCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.Trim());
						total =t1; 
						sptotal +=total;
						if(total ==0)
						{
							result1 =true;
						}
					
					}
					else if(PN.Coating.ToString().Trim() =="C7")
					{
						t1=0;
					}
					string desc="";
					desc=db.SelectPopopts(PN.Coating.Trim());
					string sp1 ="";
					int tt1=Convert.ToInt32(db.SelectLastSpNo());
					sp1=(tt1+1).ToString();
					db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Coating.ToString(),desc.Trim(),"0","1","0","0","7");
					db.InsertSpecialCount(sp1.Trim());
				}	
				//issue #234 end
				if(result1 ==true)
				{
					lblstatus.Text ="true";
				}
				//speciallimitswitch
				if(PN.LimitSwitch !=null)
				{
					if(PN.LimitSwitch.Trim() !="")
					{
						t1 =db.SelectAddersPrice_Specials_Velan(PN.LimitSwitch.Trim() ,"WEB_Velan_SpecialAdder_Pricing_TableV1",Portcode.ToString().Trim());
						t2 =db.SelectAddersPrice_Specials_Velan(PN.MountKit.Trim() ,"WEB_Velan_SpecialAdder_Pricing_TableV1",Portcode.ToString().Trim());
						total =t1+t2; 
						sptotal +=total;
						total=Decimal.Round(total,2);
						if(t2 ==0)
						{
							result1 =true;
						}	
						if(total ==0)
						{
							result1 =true;
						}	
						string des="";
						des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.LimitSwitch.Trim());
						des +=", "+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.MountKit.Trim());
						string sp1 ="";
						int tt1=Convert.ToInt32(db.SelectLastSpNo());
						sp1=(tt1+1).ToString();
						db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.LimitSwitch.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"6");
						db.InsertSpecialCount(sp1.Trim());
					}
				}
				
				if(result1 ==true)
				{
					lblstatus.Text= "true";
				}
			}
			lbltotal.Text=Convert.ToString(Convert.ToDecimal(lbltotal.Text)+ sptotal);
			return sptotal;
			
		}
		//issue #263 end
		
		//issue #137 start
		private coder ASSeries_PN( VelanClass cod )
		{
			
			DBClass db=new DBClass();
			coder PN = new coder();
			//FailMaod
			PN.PackingFriction=cod.PackingFriction;
			blCanisterPN blcanister = new blCanisterPN();
			try
			{
				switch (cod.Style)
				{
					case "Fail Close":
						cod.FailMode="FC";
						//issue #263 start
						cod.AirPressure=cod.MinAirSupply;
						PN.BTOReq=cod.BTO;
						//issue #263 end
						blcanister = db.ASSeries_Select_FC(Convert.ToDecimal(cod.ETC),Convert.ToDecimal(cod.Stroke), Convert.ToDecimal(cod.AirPressure),Convert.ToDecimal(cod.PackingFriction));
						//issue #267 start
						blcanister = db.ASSeries_Select_FC(Convert.ToDecimal(cod.ETC)*(1+Convert.ToDecimal(cod.SaftyFactor)/100),Convert.ToDecimal(cod.Stroke), Convert.ToDecimal(cod.AirPressure),Convert.ToDecimal(cod.PackingFriction));
						//issue #267 end
						cod.CanisterNo=blcanister.CanisterNo;
						cod.ETO=String.Format("{0:#.##}",Convert.ToDecimal(blcanister.ETO));
						cod.BTO=String.Format("{0:#.##}",Convert.ToDecimal(blcanister.BTO));
						cod.BTC=String.Format("{0:#.##}",Convert.ToDecimal(blcanister.BTC));
						//					cod.Stroke=blcanister.Stroke;
						cod.Tandem=blcanister.Tandem;
						string strTest = cod.Tandem;
						cod.SpringRate=String.Format("{0:#.##}",Convert.ToDecimal(blcanister.SpringRate));;
						cod.Preload = String.Format("{0:#.##}",Convert.ToDecimal(blcanister.Preload));
						string strBoreSession =db.Select_BoreCode_ByBoreValue(blcanister.CylinderBoreValue.TrimEnd('0').TrimEnd('.'));
						string[] strsBoreSession = strBoreSession.Split('|');
						blcanister.CylinderBoreCode=strsBoreSession[0];
						blcanister.CylinderBoreSize=strsBoreSession[1];
						string strRodSession = db.Select_RodDependency_ByBore(blcanister.CylinderBoreCode);
						string[] strsRodSession = strRodSession.Split('|');
						blcanister.CylinderRodCode=strsRodSession[0];
						blcanister.CylinderRodSize=strsRodSession[1];
						cod.Bore=blcanister.CylinderBoreCode;
						cod.Rod_Diamtr=blcanister.CylinderRodCode;
						cod.EtcValveTrust=blcanister.EtcValveTrust;
						PN.ActualSaftyFactor=String.Format("{0:###}",Convert.ToDecimal(Convert.ToString((Convert.ToDecimal(cod.Preload)/Convert.ToDecimal(cod.ETC)-1m)*100m)));
						break;
					case "Fail Open":
						cod.FailMode="FO";
						//issue #263 start
						cod.AirPressure=String.Format("{0:#.##}",Convert.ToDecimal(cod.MinAirSupply));;
						PN.BTOReq=cod.BTO;
						cod.BTOReq=cod.BTO;
						//issue #263 end
						//issue $267 start
						//back
						//blcanister = db.ASSeries_Select_FO(Convert.ToDecimal(cod.ETC),Convert.ToDecimal(cod.ETO),Convert.ToDecimal(cod.Stroke), Convert.ToDecimal(cod.AirPressure), Convert.ToDecimal(cod.BTO));
					    //update
						blcanister = db.ASSeries_Select_FO(Convert.ToDecimal(cod.EtcValveTrust)*(1+Convert.ToDecimal(cod.SaftyFactor)/100),Convert.ToDecimal(cod.ETO),Convert.ToDecimal(cod.Stroke), Convert.ToDecimal(cod.AirPressure), Convert.ToDecimal(cod.BTO)*(1+Convert.ToDecimal(cod.SaftyFactor)/100));
						//issue #267 end
						cod.CanisterNo=blcanister.CanisterNo;
						cod.ETO=String.Format("{0:#.##}",Convert.ToDecimal(blcanister.ETO));
						cod.BTO=String.Format("{0:#.##}",Convert.ToDecimal(blcanister.BTO));
						cod.BTC=String.Format("{0:#.##}",Convert.ToDecimal(blcanister.BTC));
						//cod.Stroke=blcanister.Stroke;
						cod.Tandem=blcanister.Tandem.Replace("S","");
						cod.SpringRate=String.Format("{0:#.##}",Convert.ToDecimal(blcanister.SpringRate));;
						cod.Preload = String.Format("{0:#.##}",Convert.ToDecimal(blcanister.Preload));
						cod.EtcValveTrust=cod.ETC;
						strBoreSession =db.Select_BoreCode_ByBoreValue(blcanister.CylinderBoreValue.TrimEnd('0').TrimEnd('.'));
						strsBoreSession = strBoreSession.Split('|');
						blcanister.CylinderBoreCode=strsBoreSession[0];
						blcanister.CylinderBoreSize=strsBoreSession[1];
						strRodSession = db.Select_RodDependency_ByBore(blcanister.CylinderBoreCode);
						strsRodSession = strRodSession.Split('|');
						blcanister.CylinderRodCode=strsRodSession[0];
						blcanister.CylinderRodSize=strsRodSession[1];
						cod.Bore=blcanister.CylinderBoreCode;
						cod.Rod_Diamtr=blcanister.CylinderRodCode;
						PN.ETC=blcanister.ETC;
						cod.ETC=blcanister.ETC;
						PN.ActualSaftyFactor=String.Format("{0:###}",Convert.ToDecimal(Convert.ToString((Convert.ToDecimal(cod.ETC)/Convert.ToDecimal(cod.EtcValveTrust)-1m)*100m)));
						//issue 267 start
						//back
						//cod.EtcValveTrust=blcanister.EtcValveTrust;
						//issue #267 end
						break;
				}
			}
			catch (Exception ex)
			{		
				if(ex.Message.ToString()!="")
				{
					switch(cod.FailMode)
					{
						case "FC":
							cod.CanisterNo="12Z";
							cod.ETO="15000";
							cod.BTO="15000";
							cod.BTC="15000";
							//cod.Stroke=blcanister.Stroke;
							cod.Tandem="";
							cod.SpringRate="2000";
							cod.Preload = "15000";
							//cod.EtcValveTrust=cod.ETC;
							cod.Bore="Z";
							cod.Rod_Diamtr="Z";
							//cod.EtcValveTrust=blcanister.EtcValveTrust;
							PN.ETC=cod.ETC;
							PN.ActualSaftyFactor="Z";
							PN.Specials="SPECIAL";
							break;
						case "FO":
							cod.CanisterNo="12Z";
							cod.ETO="15000";
							cod.BTO="15000";
							cod.BTC="15000";
							//cod.Stroke=blcanister.Stroke;
							cod.Tandem="";
							cod.SpringRate="2000";
							cod.Preload = "15000";
							//cod.EtcValveTrust=cod.ETC;
							cod.Bore="Z";
							cod.Rod_Diamtr="Z";
							//cod.EtcValveTrust=blcanister.EtcValveTrust;
							PN.ETC="15000";
							cod.ETC="15000";
							PN.ActualSaftyFactor="Z";
							PN.Specials="SPECIAL";
							break;
								
					}
						
				}
					
			}
			PN.Series="AS";
			PN.MinAirSupply=cod.AirPressure;
			PN.CanisterNo=cod.CanisterNo;
			PN.FailMode=cod.FailMode;
			PN.SpringRate=cod.SpringRate;
			PN.Preload=cod.Preload;
			PN.TandemDuplex=cod.Tandem;
			PN.Stroke =String.Format("{0:##0.00}",Convert.ToDecimal(cod.Stroke.Trim()));
			PN.Rod_End ="A4";
			PN.Cushions="8";
			PN.CushionPosition="";
			PN.Port_Type="N";
			PN.Port_Pos="1";
			PN.Seal_Comp=db.SelectValue("Seal_Code","WEB_Seal_TableV1","Seal_Type",cod.Seals.ToString().Trim()).Trim();
			PN.PackingFriction=cod.PackingFriction;
			//issue #267 start
			PN.SaftyFactor = cod.SaftyFactor;
			//issue #267 end
			//bore
			decimal bore=0.00m;
			string seatingthrust=cod.ETC.ToString().Trim();
			PN.Bore_Size=cod.Bore;
			PN.Rod_Diamtr=cod.Rod_Diamtr;
			
			//rodenddim
			string sr=PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim();
			string kk=db.SelectValue("RodEnd_Dimension","RodEndDiamension_TableV1","RR_Code",sr.Trim());
			if(kk.ToString().Trim() !="")
			{
				PN.RodendDim =" KK="+kk.Trim();
			}
			else PN.RodendDim="";
			//portsize
			string tmp=db.SelectValue(PN.Port_Type.Trim(),"SeriesASPortSize_TableV1","Bore",PN.Bore_Size.Trim());
			if( tmp.Trim() !="")
			{
				PN.PortSize ="#"+ tmp.Trim();
			}
			else PN.PortSize ="";
			PN.Specials="";
			if(cod.Paint.ToString().Trim() =="Polyurethane Enamel")
			{
				PN.Coating="C9";
				PN.Specials="SPECIAL";
			}
			else
			{
				PN.Coating="";
			}
			if(cod.BTO.ToString().Trim() !="")
			{
				PN.BTO=cod.BTO.ToString().Trim();
			}
			if(cod.ETO.ToString().Trim() !="")
			{
				PN.ETO=cod.ETO.ToString().Trim();
			}
			if(cod.BTC.ToString().Trim() !="")
			{
				PN.BTC=cod.BTC.ToString().Trim();
			}
			if(cod.ETC.ToString().Trim() !="")
			{
				PN.ETC=cod.ETC.ToString().Trim();
				seatingthrust=cod.ETC.ToString().Trim();
			}
			PN.EtcValveTrust=cod.EtcValveTrust;
			string Portcode="";
			
				PN.Mount="X3";
			//mount
			PN.SecRodDiameter="";
			PN.SecRodEnd="";
			PN.DoubleRod="No";
			PN.LimitSwitch="";
			PN.ManualOverride="";
			PN.SaftyFactor="";
			//issue #267 start
			if(cod.SaftyFactor.ToString().Trim() !="" )
			{
				PN.SaftyFactor=cod.SaftyFactor.ToString().Trim();
			}	
			else
			{
				PN.SaftyFactor="0";
			}
			//issue #267 end
			PN.ClosingTime="";
			//numenclature
			PN.PNO =PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+
				PN.Rod_End.ToString().Trim()+PN.Seal_Comp.ToString().Trim()+PN.Port_Type.ToString().Trim()+PN.Port_Pos.ToString().Trim()+
				PN.Mount.ToString().Trim()+PN.Stroke.ToString().Trim()+PN.TandemDuplex.Replace("S","")+PN.FailMode.Replace("FO","O").Replace("FC","C")+PN.CanisterNo+"-"+PN.Preload;
			LblPartNo.Text=PN.PNO.ToString();
			return PN;
		}
		public void ASPricing(coder PN)
		{
			try
			{
				DBClass db=new DBClass();
				string commn="";
				string ind="0";
				ArrayList lst =new ArrayList();
				lst =(ArrayList)Session["User"];
				//issue #315 start
				//back
				//ind=db.SelectPriceIndex("AS").ToString();
				//update
				//issue #315 end
				//commn="WEB_SeriesA_Velan_CommAdders_TableV1";
				//commn="SeriesASCommAdders_TableV1";
				commn="SeriesAS_OEM_CommAdders_TableV1";
				lblmgrp.Text="ASP_Base";
				lbltable.Text="ASPrice_Master_TableV1";
				if(PN.TandemDuplex.Trim() =="TC")
				{
					lblmgrp.Text="ASP_Base";
					lbltable.Text="ASPrice_Tandem_TableV1";
				}				
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal seal=0.00m;

				st =Convert.ToDecimal(PN.Stroke);
				sp =db.SelectStrokeA(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				sp =db.SelectOneValueByAllinfo("ASP_StrokePerInch",lbltable.Text.Trim(),"Bore_Size",PN.Bore_Size.ToString().Trim(),"Rod_Size",PN.Rod_Diamtr.ToString().Trim());
				sp =Decimal.Round(sp,2);
				
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(ind.Trim());
				temp =temp + temp * (index /100);
				//issue #315 start
				decimal discount =0.00m;

				//				lblD.Text=db.SelectDiscount(lst[1].ToString().Trim(),"A");
				//				discount=Convert.ToDecimal(lblD.Text);
				//				sprice =temp*(1 - (discount/100));
				sprice =Decimal.Round(temp,2);
				//				lblstroke.Text=sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="true";
				}
				//mount price
				decimal mp =0.00m;
				string mgrp = lblmgrp.Text.ToString();
				mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				mp =mp + mp * (index /100);
				//				mp= mp*(1 - (discount/100));
				mprice=Decimal.Round(mp,2); 
				if(PN.Mount.Trim() =="NX1" ||PN.Mount.Trim() =="NX3")
				{
					mprice=mprice + 5.00m;
				}
				if(PN.Mount.Trim().Substring(0,1) =="I" || PN.Mount.Trim().Substring(0,1) =="M" )
				{
					decimal a1=0.00m;
					a1 =Convert.ToDecimal(db.SelectValue("Price","ASprice_IsoMssAdder_TableV1","Mount",PN.Mount.Trim()));
					if(a1 >0)
					{
						a1 =a1 + a1 * (index /100);
						a1= a1*(1 - (discount/100)); 
						a1 =Decimal.Round(a1,2);
						mprice +=a1;
					}

					
				}
				else if(PN.Mount.Trim().Substring(0,2) =="GR" )
				{
					decimal a1=0.00m;
					a1 =db.SelectGRPrice(PN.Mount.Trim());
					if(a1 >0)
					{
						a1 =a1 + a1 * (index /100);
						a1= a1*(1 - (discount/100)); 
						a1 =Decimal.Round(a1,2);
						mprice +=a1;
					}
				}
				if(mprice ==0)
				{
					lblstatus.Text ="true";
				}

				if(PN.Seal_Comp.ToString().Trim() =="L")
				{
					seal =db.SelectAddersPrice("SL",commn.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					
					if(lbltable.Text.Trim() =="ASPrice_Tandem_TableV1")
					{
						seal =db.SelectOneValueByAllinfo("TSL",commn.Trim(),"BoreSize",PN.Bore_Size.ToString().Trim(),"RodSize",PN.Rod_Diamtr.ToString().Trim());
					}
					else if(lbltable.Text.Trim() =="ASDPrice_Master_TableV1")
					{
						seal =db.SelectOneValueByAllinfo("TDSL",commn.Trim(),"BoreSize",PN.Bore_Size.ToString().Trim(),"RodSize",PN.Rod_Diamtr.ToString().Trim());
					}
					else
					{
						seal =db.SelectOneValueByAllinfo("SL",commn.Trim(),"BoreSize",PN.Bore_Size.ToString().Trim(),"RodSize",PN.Rod_Diamtr.ToString().Trim());
					}
					if(seal ==0)
					{
						lblstatus.Text ="true";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="F")
				{
					seal =db.SelectAddersPrice("SF",commn.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					//seal =db.SelectOneValueByAllinfo("SF",commn.Trim(),"BoreSize",PN.Bore_Size.ToString().Trim(),"RodSize",PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="true";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="E")
				{
					seal =db.SelectAddersPrice("SE",commn.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="true";
					}
				}
				seal=Decimal.Round(seal,2);

				decimal rodend=0.00m;
				if(PN.Rod_End.ToString().Substring(0,1) !="N" &&PN.Rod_End.ToString().Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd",commn.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="true";
					}
				}
				rodend=Decimal.Round(rodend,2);
				//issue #315 start
				seal =seal + seal * (index /100);
				rodend =rodend + rodend * (index /100);
				//canisterprice
				//				decimal dcCanisterPrice = Convert.ToDecimal(db.SelectOneValueByAllinfo("CanPrice","AS_PRELOAD_SPACER",
				//                                                                     "CanisterNo",PN.CanisterNo,
				//					                                                 "Preload",PN.Preload).ToString());
				decimal total=0.00m;
				total=sprice + mprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}

		private QItems ASSeries_QItem(coder PN, Quotation quot, VelanClass cod , string userID)
		{
			DBClass db=new DBClass();
			QItems Qitem =new QItems();
			Qitem.PartNo=PN.PNO;
			Qitem.ItemNo =quot.QuoteNo.ToString();
			Qitem.S_Code=PN.Series.Trim();
			Qitem.Series=db.SelectValue("Series_Name","WEB_Series_TableV1","Series_Code",PN.Series.ToString().Trim()).ToString();
			Qitem.S_Price="0.00";
			Qitem.B_Code=PN.Bore_Size.ToString().Trim();
			Qitem.Bore=db.SelectValue("Bore_Size","WEB_Bore_TableV1","Bore_Code",PN.Bore_Size.ToString().Trim()).ToString()+" Bore Size";
			Qitem.B_Price="0.00";
			Qitem.R_Code=PN.Rod_Diamtr.ToString().Trim();
			Qitem.Rod=db.SelectValue("Rod_Size","WEB_RodSerZ_TableV1","Rod_Code",PN.Rod_Diamtr.ToString().Trim())+" Rod Size";
			Qitem.R_Price="0.00";
			Qitem.Stroke_Code=PN.Stroke.ToString().Trim();
			string st="";
			if(Convert.ToDecimal(PN.Stroke.ToString()) > 120.00m)
			{
				st="For pricing please consult factory";
				lblstatus.Text="true";
			}
			Qitem.Stroke="Stroke = "+PN.Stroke.ToString()+"\""+st.ToString();
			Qitem.Stroke_Price="0.00";
			Qitem.M_code=PN.Mount.ToString().Trim();
			Qitem.Mount=db.SelectValue("Mount_Type","Mount"+PN.Series.Trim()+"_TableV1","Mount_Code",PN.Mount.ToString().Trim()).ToString();
			Qitem.M_Price="0.00";
			Qitem.RE_Code=PN.Rod_End.ToString().Trim();
			Qitem.RodEnd=db.SelectValue("RodEnd_Shape","WEB_RodEndKK_TableV1","RodEnd_Code",PN.Rod_End.ToString().Trim()).ToString()+PN.RodendDim;
			Qitem.RE_Price="0.00";
			Qitem.Cu_Code=PN.Cushions.ToString().Trim();
			Qitem.Cushion=db.SelectValue("Cushion_type","WEB_Cushion_TableV1","Cushion_Code",PN.Cushions.ToString()).ToString();
			Qitem.Cu_Price="0.00";
			Qitem.CushionPos_Code=PN.CushionPosition.ToString().Trim();
			Qitem.CushionPos=db.SelectValue("CushionPos_Pos","WEB_CushionPos_TableV1","CushionPos_Code",PN.CushionPosition.ToString().Trim()).ToString();
			Qitem.CushionPos_Price="0.00";
			Qitem.Sel_Code=PN.Seal_Comp.ToString().Trim();
			Qitem.Seal=db.SelectValue("Seal_Type","WEB_Seal_TableV1","Seal_Code",PN.Seal_Comp.ToString().Trim()).ToString();
			Qitem.Sel_Price="0.00";
			Qitem.Port_Code=PN.Port_Type.ToString().Trim();
			//Qitem.Port=PN.PortSize.Trim()+" "+ db.SelectValue("PortType_Type","WEB_portType_TableV1","PortType_Code",PN.Port_Type.ToString().Trim()).ToString();
			Qitem.Port="#"+ db.SelectValue(PN.Port_Type.Trim(),"SeriesASPortSize_TableV1","Bore",PN.Bore_Size.Trim()).ToString()+" "+ db.SelectValue("PortType_Type","WEB_portType_TableV1","PortType_Code",PN.Port_Type.ToString().Trim()).ToString();
			Qitem.Port_Price="0.00";
			Qitem.PP_Code=PN.Port_Pos.ToString().Trim();
			Qitem.PortPos=db.SelectValue("PortPos_Position","WEB_PortPosition_TableV1","PortPos_Code",PN.Port_Pos.ToString().Trim()).ToString();
			Qitem.PP_Price="0.00";
			Qitem.Quantity=cod.Qty.ToString();
			Qitem.Discount="0";
			Qitem.ApplicationOpt_Id="";
			Qitem.Special_ID="";
			Qitem.Cusomer_ID=quot.Customer.Trim();
			Qitem.Quotation_No=quot.QuoteNo.Trim();
			Qitem.Q_Date=quot.Quotedate.Trim();
			Qitem.User_ID=userID;
			Qitem.PriceList="0";
			Qitem.SpecialReq =cod.Notes.ToString();
			Qitem.Note=cod.Notes.ToString();
			if(lblstatus.Text.ToLower().Trim() =="true")
			{
				Qitem.UnitPrice ="0.00";
				Qitem.TotalPrice="0.00";
			}
			else
			{
				Qitem.UnitPrice=lbltotal.Text.ToString();
				Qitem.TotalPrice=Convert.ToString((Convert.ToDecimal(Qitem.UnitPrice)*Convert.ToDecimal(cod.Qty)));
			}
			string strTandem ="";
			if(PN.TandemDuplex=="TC") strTandem="YES";
			else strTandem="NO";
			//weight
			Qitem.Weight = "Approximate cylinder weight (does not include accessories)= "+CylWeight(PN.Series,PN.Bore_Size, PN.Rod_Diamtr, PN.CanisterNo, PN.Stroke,PN.FailMode,strTandem).ToString() +" LBS";
			//issue #233 start
			Qitem.Weight +=";The Cylinder Displacement = " + Cyl_Displacement(PN) + " in" + Convert.ToChar(0179).ToString();
			//issue #233 end
			return Qitem;
		}
		
		private decimal CylWeight(string series, string bore,string rod,string mount,string stk,string strFailMode,string tandem)
		{
			decimal c1 = 0;
			decimal c2=0;
			decimal c3=0;
			decimal c4=0;
			decimal cs = 0;
			DBClass db=new DBClass();
			if( series.Trim()== "AS")
			{
				if(tandem.ToUpper().Trim()=="NO")
				{
					c1=db.SelectOneValueByAllinfo("SWeight1","SeriesAs_Weight_TableV1","BoreSize",bore.Trim(),"1","1");
					c2=db.SelectOneValueByAllinfo("SStroke","SeriesAs_Weight_TableV1","BoreSize",bore.Trim(),"1","1");
					cs=db.SelectOneValueByAllinfo("Weight","SeriesAS_Weight_Canister_TableV1","BoreSize",mount.Trim(),"FailMode",strFailMode);
					c4=c1+c2*Convert.ToDecimal(stk.Trim()) +cs;
					c4=Decimal.Round(c4,0);
				}
				if(tandem.ToUpper().Trim()=="YES")
				{
					c1=db.SelectOneValueByAllinfo("DWeight1","SeriesAs_Weight_TableV1","BoreSize",bore.Trim(),"1","1");
					c2=db.SelectOneValueByAllinfo("DStroke","SeriesAs_Weight_TableV1","BoreSize",bore.Trim(),"1","1");
					cs=db.SelectOneValueByAllinfo("Weight","SeriesAS_Weight_Canister_TableV1","BoreSize",mount.Trim(),"FailMode",strFailMode);
					c4=c1+c2*Convert.ToDecimal(stk.Trim()) +cs;
					c4=Decimal.Round(c4,0);
				}
			}
			return c4;
		}
		//issue #263 start
		private string Create_CanisterDesc(coder PN)
		{
			string strFailMode="";
			try
			{
				//issue #230 start
				DBClass db=new DBClass();
				decimal dcCylArea = db.AS_Select_CylArea(PN.Bore_Size,PN.TandemDuplex);
				//issue #230 end
				string sfcb="";
				switch(PN.FailMode)
				{
					case "FC":		
						//if(IsNumeric(bto)) sfcb=String.Format("{0:###}",Convert.ToDecimal(Convert.ToString((Convert.ToDecimal(bto)/Convert.ToDecimal(preload)-1m)*100m)));
						//if(IsNumeric(PN.BTO)) sfcb=String.Format("{0:###}",Convert.ToDecimal(Convert.ToString((Convert.ToDecimal(PN.BTO)/Convert.ToDecimal(PN.Preload)-1m)*100m)));
						//back
						if(IsNumeric(PN.BTO)) sfcb=String.Format("{0:###}",Convert.ToDecimal(Convert.ToString((Convert.ToDecimal(PN.BTO)/Convert.ToDecimal(PN.BTOReq)-1m)*100m)));
						//if(IsNumeric(bto)) sfcb=String.Format("{0:###}",Convert.ToDecimal(Convert.ToString((Convert.ToDecimal(bto)/Convert.ToDecimal(preload))*100m)));
						//strFailMode="CAN" + canisterno + failmode+ preload +"; " + "Fail Close. ETC= "+preload+" lbs, BTC= "+btc+" lbs ; BTO= " +bto +" lbs, ETO= "+eto+" lbs";
						//strFailMode="CAN" + canisterno + failmode.Replace("FO","O").Replace("FC","C")+ preload +"; " + "Fail Close. ETC= "+preload+" lbs, BTC= "+btc+" lbs ; BTO= " +bto +" lbs, ETO= "+eto+" lbs";
						//strFailMode="CAN" + canisterno + failmode.Replace("FO","O").Replace("FC","C")+ preload +"; " + "Fail Close. ETC= "+preload+" lbs (S.F.= " + sfce + " %); ETO= " + eto+ " lbs ; BTO= " +bto +" lbs (S.F.= "+sfcb+" %); BTC= "+btc+" lbs";
						//issue #230 start
						//if(IsNumeric(PN.BTOReq)) sfcb=String.Format("{0:###}",Convert.ToDecimal(Convert.ToString((Convert.ToDecimal(PN.MinAirSupply)*dcCylArea/Convert.ToDecimal(PN.BTO)-1m)*100m)));
						//issue #230 end
						//issue #242 start
						if(PN.Units=="metric")
							strFailMode="CAN" + PN.CanisterNo + PN.FailMode.Replace("FO","O").Replace("FC","C")+ PN.Preload +"; " 
								+ "Fail Close. ETC= "+PN.Preload+" lbs ("+String.Format("{0:###}",Convert.ToDecimal(PN.Preload)*4.448m)+" N) (S.F.= " + PN.ActualSaftyFactor + " %); ETO= " 
								+ PN.ETO+ " lbs ("+String.Format("{0:###}",Convert.ToDecimal(PN.ETO)*4.448m)+" N) ; BTO= " 
								+ PN.BTO +" lbs ("+String.Format("{0:###}",Convert.ToDecimal(PN.BTO)*4.448m)+" N) (S.F.= "+sfcb+" %); BTC= "
								+PN.BTC+" lbs ("+String.Format("{0:###}",Convert.ToDecimal(PN.BTC)*4.448m)+" N)";
						else 
							//back
							strFailMode="CAN" + PN.CanisterNo + PN.FailMode.Replace("FO","O").Replace("FC","C")+ PN.Preload +"; " + "Fail Close. ETC= "+PN.Preload+" lbs (S.F.= " + PN.ActualSaftyFactor + " %); ETO= " + PN.ETO+ " lbs ; BTO= " +PN.BTO +" lbs (S.F.= "+sfcb+" %); BTC= "+PN.BTC+" lbs";
						//issue #242
						break;
					case "FO":
						//if(IsNumeric(btoreq)) sfcb=String.Format("{0:###}",Convert.ToDecimal(Convert.ToString((Convert.ToDecimal(bto)/Convert.ToDecimal(btoreq)-1m)*100m)));
						//back
						if(IsNumeric(PN.BTOReq)) sfcb=String.Format("{0:###}",Convert.ToDecimal(Convert.ToString((Convert.ToDecimal(PN.BTO)/Convert.ToDecimal(PN.BTOReq)-1m)*100m)));
						//if(IsNumeric(btoreq)) sfcb=String.Format("{0:###}",Convert.ToDecimal(Convert.ToString((Convert.ToDecimal(bto)/Convert.ToDecimal(btoreq))*100m)));
						//strFailMode="CAN" + canisterno + failmode+ preload +"; " + "Fail Open. ETO= "+eto +" lbs, BTO= " +bto +" lbs; ETC= " +etc+" lbs, BTC= "+btc+" lbs";
						//strFailMode="CAN" + canisterno + failmode.Replace("FO","O").Replace("FC","C")+ preload +"; " + "Fail Open. ETO= "+preload +" lbs, BTO= " +bto +" lbs; ETC= " +etc+" lbs, BTC= "+btc+" lbs";
						//strFailMode="CAN" + canisterno + failmode.Replace("FO","O").Replace("FC","C")+ preload +"; " + "Fail Open. ETO= "+preload +" lbs; BTO= " +bto+" lbs (S.F.= " + sfcb+" %); ETC= " +etc+" lbs (S.F.= "+ sfce + " %); BTC= "+btc+" lbs";
						//issue #230 start
						//if(IsNumeric(PN.BTOReq)) sfcb=String.Format("{0:###}",Convert.ToDecimal(Convert.ToString((Convert.ToDecimal(PN.MinAirSupply)*dcCylArea/Convert.ToDecimal(PN.BTO)-1m)*100m)));
						//issue #230 end
						//issue #242 start
						//update
						if(PN.Units=="metric")
							strFailMode="CAN" + PN.CanisterNo+ PN.FailMode.Replace("FO","O").Replace("FC","C")+ PN.Preload+"; " 
								+ "Fail Open. ETO= "+PN.Preload +" lbs ("+String.Format("{0:###}",Convert.ToDecimal(PN.Preload)*4.448m)+" N); BTO= " 
								+PN.BTO+" lbs ("+String.Format("{0:###}",Convert.ToDecimal(PN.BTO)*4.448m)+" N) (S.F.= " + sfcb+" %); ETC= " 
								+PN.ETC+" lbs ("+String.Format("{0:###}",Convert.ToDecimal(PN.ETC)*4.448m)+" N) (S.F.= "+ PN.ActualSaftyFactor + " %); BTC= "
								+PN.BTC+" lbs ("+String.Format("{0:###}",Convert.ToDecimal(PN.BTC)*4.448m)+" N)";
						else
							//back
							strFailMode="CAN" + PN.CanisterNo+ PN.FailMode.Replace("FO","O").Replace("FC","C")+ PN.Preload+"; " + "Fail Open. ETO= "+PN.Preload +" lbs; BTO= " +PN.BTO+" lbs (S.F.= " + sfcb+" %); ETC= " +PN.ETC+" lbs (S.F.= "+ PN.ActualSaftyFactor + " %); BTC= "+PN.BTC+" lbs";
						//issue #242 end
						break;
				}
				//issue #229 start
				if(PN.Bore_Size=="Z") 
				{
					strFailMode = "CAN" + PN.CanisterNo;
					switch(PN.FailMode)
					{
						case "FC":
							strFailMode = "CAN" + PN.CanisterNo + PN.FailMode.Replace("FO","O").Replace("FC","C")+ "?" +"; " + "Fail Close. ETC= "+ "?" +" lbs (S.F.= " +  "?"  + " %); ETO= " +  "?" + " lbs ; BTO= " + "?"  +" lbs (S.F.= "+ "?" +" %); BTC= "+ "?" +" lbs";
							break;
						case "FO":
							strFailMode="CAN" + PN.CanisterNo+ PN.FailMode.Replace("FO","O").Replace("FC","C")+ "?"+"; " + "Fail Open. ETO= "+"?" +" lbs; BTO= " +"?"+" lbs (S.F.= " + "?"+" %); ETC= " +"?"+" lbs (S.F.= "+ "?" + " %); BTC= "+"?"+" lbs";
							break;
					}
				}
				//issue #229 end
			}
			catch{}
			return strFailMode;
		}
		//issue #263 end
		
//		private string Create_CanisterDesc(string failmode, string canisterno, string preload, string bto, string eto, string etc , string btc)
//		{
//			string strFailMode="";
//			try
//			{
//				switch(failmode)
//				{
//					case "FC":		
//						//strFailMode="CAN" + canisterno + failmode+ preload +"; " + "Fail Close. ETC= "+preload+" lbs, BTC= "+btc+" lbs ; BTO= " +bto +" lbs, ETO= "+eto+" lbs";
//						strFailMode="CAN" + canisterno + failmode.Replace("FO","O").Replace("FC","C")+ preload +"; " + "Fail Close. ETC= "+preload+" lbs, BTC= "+btc+" lbs ; BTO= " +bto +" lbs, ETO= "+eto+" lbs";
//						break;
//					case "FO":
//						//strFailMode="CAN" + canisterno + failmode+ preload +"; " + "Fail Open. ETO= "+eto +" lbs, BTO= " +bto +" lbs; ETC= " +etc+" lbs, BTC= "+btc+" lbs";
//						strFailMode="CAN" + canisterno + failmode.Replace("FO","O").Replace("FC","C")+ preload +"; " + "Fail Open. ETO= "+eto +" lbs, BTO= " +bto +" lbs; ETC= " +etc+" lbs, BTC= "+btc+" lbs";
//						break;
//				}
//				
//			}
//			catch{}
//			return strFailMode;
//		}
		private void ASSeries_Pop(coder PN, Quotation quot, QItems Qitem)
		{
			DBClass db=new DBClass();
			decimal total =0.00m;
			decimal t1 =0.00m;
			bool result1=false;
			decimal pop=0.00m;
			//canisterprice
			decimal dcCanisterPrice = Convert.ToDecimal(db.SelectOneValueByAllinfo("CanPrice","AS_PRELOAD_SPACER",
				"CanisterNo",PN.CanisterNo,
				"Preload",PN.Preload).ToString());
			total=dcCanisterPrice;
			if(total ==0)
			{
				result1 =true;
			}
			//issue #263 start
			//back
			//string strCanisterDesc = Create_CanisterDesc(PN.FailMode,PN.CanisterNo,PN.Preload,PN.BTO,PN.ETO,PN.ETC,PN.BTC);
			//update
			string strCanisterDesc = Create_CanisterDesc(PN);
			//issue #263 end
			db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.FailMode.ToString(),strCanisterDesc,dcCanisterPrice.ToString());
			if(result1 ==true)
			{
				lblstatus.Text ="true";
			}
			else
			{
				Qitem.PopularOpt_Id="";
			}
			pop =total;
			total=0;
			//insertcanister
			//issue #267 start
			//back
			//string strCanisterIns = db.InsertCanisters(quot.QuoteNo,PN.PNO,PN.FailMode,PN.CanisterNo,PN.Preload,PN.ETC, PN.ETO,PN.BTO,PN.MinAirSupply,PN.PackingFriction,PN.SpringRate,PN.EtcValveTrust,dcCanisterPrice.ToString(),"0",PN.BTC, PN.BTOReq);
			//update
			string strCanisterIns = db.InsertCanisters(quot.QuoteNo,PN.PNO,PN.FailMode,PN.CanisterNo,PN.Preload,PN.ETC, PN.ETO,PN.BTO,PN.MinAirSupply,PN.PackingFriction,PN.SpringRate,PN.EtcValveTrust,dcCanisterPrice.ToString(),PN.SaftyFactor,PN.BTC,PN.BTOReq);
			//issue #267 end
			Qitem.PopularOpt_Id=quot.QuoteNo.Trim()+LblPartNo.Text.Trim();
			if(PN.Coating.ToString().Trim() !="")
			{
				//popoptcoating
				if(PN.Coating.ToString().Trim() =="C1")
				{
					t1=0;
					t1 =db.SelectAddersPrice_Specials_ByBore("C1","WEB_Specials_Velan_Pricing_TableV1",PN.Bore_Size.ToString().Trim());
					total =t1; 
					if(total ==0)
					{
						result1 =true;
					}
					string desc="";
					desc=db.SelectPopopts(PN.Coating.Trim());
					db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.Coating.ToString(),desc.ToString(),"0.00");
				}
				if(result1 ==true)
				{
					lblstatus.Text ="true";
				}
				pop +=total;
				Qitem.PopularOpt_Id=quot.QuoteNo.Trim()+LblPartNo.Text.Trim();
			}
			else
			{
				Qitem.PopularOpt_Id="";
			}
			if(PN.TandemDuplex.Trim() == "TC")
			{
				//tcprice
				//tcdesc
				string desc="";
				desc=db.SelectPopopts(PN.TandemDuplex.Trim());
				//tcinsert
				db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.TandemDuplex.ToString(),desc.ToString(),"0.00");
				//tctotal
			}
			else{}
			//issue #315 start
			lbltotal.Text=Convert.ToString(Convert.ToDecimal(lbltotal.Text)+ pop);
			//			decimal dcc2=0.00m;
			//			dcc2 =Convert.ToDecimal(lbltotal.Text);
			//			lbltotal.Text=dcc2.ToString();
			//update QItem
			if(lblstatus.Text.ToLower().Trim() =="true")
			{
				Qitem.UnitPrice ="0.00";
				Qitem.TotalPrice="0.00";
			}
			else
			{
				Qitem.UnitPrice=lbltotal.Text.ToString();
				Qitem.TotalPrice=Convert.ToString((Convert.ToDecimal(Qitem.UnitPrice)*Convert.ToDecimal(Qitem.Quantity)));
			}
									
		}
		//issue #137 end
		//issue #233 start
		private string Cyl_Displacement(coder PN)
		{
			string strCylDis = "TBA";
			try
			{
				DBClass db=new DBClass();
				decimal dcBore = 0m;
				decimal dcRod = 0m;
				decimal dcStroke = Convert.ToDecimal(PN.Stroke);
				dcRod = Convert.ToDecimal(db.SelectRodValue(PN.Rod_Diamtr));
				dcBore = Convert.ToDecimal(db.SelectBoreValue(PN.Bore_Size));
				decimal dcCylDis = 0m;
				decimal dcCylDisRet=0m;
				if(PN.DoubleRod == "No" && PN.FailMode != "FC" && PN.FailMode != "FO" )
				{
					dcCylDisRet=(dcBore*dcBore)*3.14m*dcStroke/4m;
				}
				dcCylDis=(dcBore*dcBore-dcRod*dcRod)*3.14m*dcStroke/4m + dcCylDisRet;
				if(PN.TandemDuplex == "TC")
				{
					dcCylDis *=2m;
				}
				dcCylDis = Math.Round(dcCylDis,0);
				strCylDis=dcCylDis.ToString();
			}
			catch (Exception ex)
			{		
				
				
			}
			return strCylDis;
		}
		//issue #233 end
		//issue #268 start
		private void Get_Bore(VelanClass cod, ref string borevalue, ref string borecode, ref string rodcode  )
		{
			DBClass db = new DBClass();
			decimal bore=0.00m;
			if(cod.MinAirSupply.ToString().Trim() !="" &&  cod.Thrust !="" &&  cod.Thrust !="" )
			{
				decimal dc1,dc2,dc3,dc4,dc5,b=0.00m;
				dc1=Convert.ToDecimal(cod.MinAirSupply.ToString().Trim());
				dc2=Convert.ToDecimal(cod.Thrust.ToString().Trim());
				dc3=Convert.ToDecimal(cod.PackingFriction.ToString().Trim());
				dc4=Convert.ToDecimal(cod.SaftyFactor.ToString().Trim());
				dc4= 1 + (dc4/100);
				dc5= (dc2) / dc1;
				b= dc5 / Convert.ToDecimal(Math.PI);
				bore=Convert.ToDecimal(( Math.Sqrt(Convert.ToDouble(b))) * 2);
				bore=Decimal.Round(bore,2);
//				string borevalue="";
				if(bore > 0.00m && bore < 4.00m )
				{
					borecode="H";
					borevalue="4";
				}
				else if(bore >= 4.00m && bore <= 5.00m )
				{
					borecode="K";
					borevalue="5";
				}
				else if(bore >= 5.00m && bore <= 6.00m )
				{
					borecode="L";
					borevalue="6";
				}
				else if(bore >= 6.00m && bore <= 7.00m )
				{
					borecode="M";
					borevalue="7";
				}
				else if(bore >= 7.00m && bore <= 8.00m )
				{
					borecode="N";
					borevalue="8";
				}
				else if(bore >= 8.00m && bore <= 10.00m )
				{
					borecode="P";
					borevalue="10";
				}
				else if(bore >= 10.00m && bore <= 12.00m )
				{
					borecode="R";
					borevalue="12";
				}
				else if(bore >= 12.00m && bore <= 14.00m )
				{
					borecode="S";
					borevalue="14";
				}
				else if(bore >= 14.00m && bore <= 16.00m )
				{
					borecode="T";
					borevalue="16";
				}
				else if(bore >= 16.00m && bore <= 18.00m )
				{
					borecode="W";
					borevalue="18";
				}
				else if(bore >= 18.00m && bore <= 20.00m )
				{
					borecode="X";
					borevalue="20";
				}
				else if(bore >= 20.00m && bore <= 22.00m )
				{
					borecode="A";
					borevalue="22";
				}
				else if(bore >= 22.00m && bore <= 24.00m )
				{
					borecode="Y";
					borevalue="24";
				}
				else if(bore >= 24.00m && bore <= 26.00m )
				{
					borecode="B";
					borevalue="26";
				}
				else if(bore >= 26.00m && bore <= 28.00m )
				{
					borecode="F";
					borevalue="28";
				}
				else if(bore >= 28.00m && bore <= 30.00m )
				{
					borecode="I";
					borevalue="30";
				}
				else
				{
					borecode="Z";
				}
				decimal cc1,cc2,cc3,cc4=0.00m;
				if(borevalue.Trim() !="")
				{
					cc1= Convert.ToDecimal(borevalue.Trim());
					cc2=((cc1 * cc1) / 4)  * Convert.ToDecimal(Math.PI) ;
					cc3=cc2 * dc1;
					cc4=((cc3 /( dc2 + dc3 )) -1) * 100;
					//PN.ActualSaftyFactor =(Decimal.Round(cc4,2)).ToString();
				}
				rodcode=db.SelectValue("Rod_Code","WEB_RodSerA_TableV1",borecode,"1").Trim();
			}
		}
		//issue #268 end
		private void BtnQuoteGenerate_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(lblup.Text.Trim() !="")
				{
					lblspecsheet.Text="";
					ArrayList lst =new ArrayList();
					lst =(ArrayList)Session["User"];
					DBClass db=new DBClass();
					ArrayList qlist=new ArrayList();
					qlist=db.Select_DataFrom_ImportFile_Way(Request.PhysicalApplicationPath+"dbfile/"+lblup.Text.Trim());
					if(qlist.Count >0)
					{
						ArrayList list1 = new ArrayList();
						ArrayList list2=new ArrayList();
						list1=db.SelectContactdetails(lst[6].ToString().Trim(),lst[5].ToString().Trim());
						list2=db.SelectCompanyterms(lst[5].ToString().Trim(),lst[6].ToString().Trim());
						Quotation quot =new Quotation();
						string company="";
						if(list1[16].ToString().Trim() =="0")
						{
							company ="Montreal";
						}
						else
						{
							company ="Mississauga";
						}
						quot.Office= company.Trim();
						quot.Customer=list1[0].ToString();
						quot.Contact=lst[0].ToString().Trim();
						quot.AttnTo1=lst[0].ToString().Trim();
						quot.AttnTo2="P:"+list1[12].ToString()+" F:"+list1[13].ToString();
						quot.AttnTo3=list1[14].ToString();
						quot.BillTo1=list1[0].ToString();
						quot.BillTo2=list1[1].ToString()+", "+list1[2].ToString();
						quot.BillTo3=list1[3].ToString()+", "+list1[4].ToString()+", "+list1[5].ToString();
						quot.ShipTo1=list1[6].ToString();
						quot.ShipTo2=list1[7].ToString()+", "+list1[8].ToString();
						quot.ShipTo3=list1[9].ToString()+", "+list1[10].ToString()+", "+list1[11].ToString();
						quot.QuoteNo=Qno(lst[3].ToString().Trim());
						quot.Quotedate=DateTime.Now.ToShortDateString();
						quot.ExpiryDate=DateTime.Now.AddDays(30).ToShortDateString();;
						quot.PrepairedBy=lst[0].ToString().Trim();
						quot.Code=list2[0].ToString();
						if(list2[4].ToString().Trim()=="0")
						{
							quot.Langu="English";
						}
						else
						{
							quot.Langu="French";
						}					
						quot.Terms=list2[2].ToString();
						//issue #263 start
						string strDelivery="TBA";
						//issue #263 end
						quot.Delivery="TBA";
						if(list2[1].ToString().Trim()=="0")
						{
							quot.Currency="CN $";
						}
						else
						{
							quot.Currency="US $";
						}
						quot.Note=TxtProjectNo.Text.ToString().Trim();
						quot.FinishedDate ="  ";
						quot.CowanQno= "  "; 
						quot.Items=quot.QuoteNo.ToString();
						quot.CompanyID=list2[5].ToString();
						string upload="";
						lblspecsheet.Text="";
						if(FileUploadSpec.Value.Trim() !="")
						{
							if(FileUploadSpec.PostedFile.ContentType.ToString() =="application/pdf")
							{										
								upload=UploadFileSpec(sender,e);
								lblstatus.Text="true";
								lblspecsheet.Text= Server.MapPath(".") + "/specsheet/"+upload.ToString();
							}
						}
						bool cyltype=true;
						quot.SpecSheet=upload.Trim();
						ArrayList Implist =new ArrayList();
						for(int i=0;i<qlist.Count;i++)
						{						
							lblmgrp.Text="";
							lbltable.Text="";
							lblstatus.Text="";
							VelanClass cod=new VelanClass();
							cod=(VelanClass)qlist[i];
							coder PN=new coder();
							if(cod !=null)
							{
								//issue #137 start
								if(cod.PneumaticHydraulic.ToString().ToUpper().Trim() !="PNEUMATIC WITH SPRING")
								{
									if(cod.PneumaticHydraulic.ToString().ToUpper().Trim() =="PNEUMATIC")
									{
										//series
										PN.Series="A";
										PN.Port_Type="N";	
										PN.Rod_End ="A4";
										cyltype=true;
										if(Convert.ToDecimal(cod.Bore.Trim()) >= 12)
										{
											PN.LiftingLugs="LE";
										}							
										else
										{
											PN.LiftingLugs="";
										}
									}							
									else if(cod.PneumaticHydraulic.ToString().ToUpper().Trim() =="HYDRAULIC")
									{
										PN.Series="ML";
										PN.Port_Type="S";
										PN.Rod_End ="N4";
										cyltype=false;
									}
									else if(cod.PneumaticHydraulic.ToString().ToUpper().Trim() =="HYDRAULIC WITH TRANSDUCER")
									{
										PN.Series="L";
										PN.Port_Type="S";
										PN.Rod_End ="N1";
										cyltype=false;
									}	
									PN.Cushions="8";
									PN.CushionPosition="";							
									PN.Port_Pos="11";
									PN.Stroke =String.Format("{0:##0.00}",Convert.ToDecimal(cod.Stroke.Trim()));
									PN.Seal_Comp=db.SelectValue("Seal_Code","WEB_Seal_TableV1","Seal_Type",cod.Seals.ToString().Trim()).Trim();
									if(cod.Paint.ToString().Trim() =="Standard Paint")
									{
										PN.Coating="C10";
									}
									else if(cod.Paint.ToString().Trim() =="Polyurethane Enamel")
									{
										PN.Coating="C11";
									}
									else
									{
										PN.Coating="";
									}		
									PN.Specials="";
									if(cod.Style.ToString().ToUpper().Trim() =="DOUBLE ACTING")
									{
										PN.Style="N";
									}							
									else if(cod.Style.ToString().ToUpper().Trim() =="FAIL CLOSE")
									{
										PN.Style="FC";
										PN.Specials="SPECIAL";
									}
									else if(cod.Style.ToString().ToUpper().Trim() =="FAIL OPEN")
									{
										PN.Style="FO";
										PN.Specials="SPECIAL";
									}
									else
									{
										PN.Style="N";
									}									
									PN.SecRodDiameter="";
									PN.SecRodEnd="";
									PN.DoubleRod="No";	
									PN.Qty=cod.Qty.ToString().Trim();
									decimal bore=0.00m;
									string boresize="";
									if(cod.Bore.ToString().Trim() !="")
									{
										bore=Convert.ToDecimal(cod.Bore.ToString());
										boresize=cod.Bore.ToString();
										if(cyltype ==true)
										{
											PN.Mount="X0";							
										}
										else
										{
											//									if(bore >=0.0m && bore <=8m)
											//									{
											PN.Mount="E5";	
											//									}
											//									else
											//									{
											//										PN.Mount="F5";	
											//									}
										}	
										string boreval="";
										boreval=db.SelectValue("Bore_Code","WEB_Bore_TableV1","BoreValue",bore.ToString());
										if(boreval.Trim() !="")
										{
											PN.Bore_Size=boreval.ToString().Trim();
										}
										if(PN.Series.Trim() =="A" || PN.Series.Trim() =="AC" || PN.Series.Trim() =="AT"  )
										{
											PN.Rod_Diamtr=db.SelectValue("Rod_Code","WEB_RodSerA_TableV1",PN.Bore_Size,"1").Trim();
										}
										else 
										{
											PN.Rod_Diamtr=db.SelectValue_Rotork("Rod_Code","WEB_RodSerM_TableV1",PN.Bore_Size,"1","RodValue").Trim();									
										}								
										if(PN.Bore_Size.Trim()!="" || PN.Rod_Diamtr.Trim()!="")
										{
											if(PN.Series.Trim() =="A")
											{
												APricing(PN);										
											}																
											else if(PN.Series.Trim() =="ML")
											{
												MLPricing(PN);
											}
											else if(PN.Series.Trim() =="L")
											{
												LPricing(PN);
											}
											else
											{
												lbltotal.Text ="0.00";
												lblstatus.Text ="true";
											}
										}
										else
										{
											lbltotal.Text ="0.00";
											lblstatus.Text ="true";
											PN.Bore_Size="Z";
											PN.Rod_Diamtr="Z";
										}
									}							
									if(PN.Style.ToString().Trim() !="N")
									{
										PN.Bore_Size="Z";
										PN.Rod_Diamtr="Z";
										if(cyltype ==true)
										{
											PN.Mount="X0";							
										}
										else
										{
											if(bore >=0.0m && bore <=8m)
											{
												PN.Mount="E5";	
											}
											else
											{
												PN.Mount="F5";	
											}
										}
									}												
							
									//numenclature
									PN.PNO =PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+
										PN.Rod_End.ToString().Trim()+PN.Cushions.ToString().Trim()+PN.CushionPosition.ToString().Trim()+
										PN.Seal_Comp.ToString().Trim()+PN.Port_Type.ToString().Trim()+PN.Port_Pos.ToString().Trim()+
										PN.Mount.ToString().Trim()+PN.Stroke.ToString().Trim()+PN.Coating.ToString().Trim()+PN.LiftingLugs.ToString().Trim();
									LblPartNo.Text=PN.PNO.ToString();							
								
									try
									{
										string st="";
										if(Convert.ToDecimal(PN.Stroke.ToString()) > 120.00m)
										{
											st="For pricing please consult factory";
											lblstatus.Text="true";
										}
										//roddim
										string rd=PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim();
										string rodenddim ="";
										if(db.SelectRRD(rd).Trim() !="")
										{
											rodenddim =" KK = "+db.SelectRRD(rd);
										}
										//portsize
										string portsiz="";					
										string table="";
										if(PN.Series.Trim().Substring(0,1) =="A" ||PN.Series.Trim().Substring(0,1) =="S" )
										{
											table ="WEB_SeriesAPortSize_TableV1";
										}
										else if(PN.Series.Trim() =="ML" || PN.Series.Trim() =="L")
										{
											table ="WEB_SeriesMMLLRPortSize_TableV1";
										}
										if(db.SelectPortSizeNo(PN.Bore_Size.Trim(),PN.Port_Type.Trim(),table.Trim()) !="")
										{
											portsiz ="#"+db.SelectPortSizeNo(PN.Bore_Size.Trim(),PN.Port_Type.Trim(),table.Trim());
										}
										//qitem
										QItems Qitem =new QItems();
										Qitem.ItemNo =quot.QuoteNo.ToString();
										Qitem.S_Code=PN.Series.Trim();
										Qitem.Series=db.SelectValue("Series_Name","WEB_Series_TableV1","Series_Code",PN.Series.ToString().Trim()).ToString();
										Qitem.S_Price="0.00";
										Qitem.B_Code=PN.Bore_Size.ToString().Trim();
										Qitem.Bore=db.SelectValue("Bore_Size","WEB_Bore_TableV1","Bore_Code",PN.Bore_Size.ToString().Trim()).ToString()+" Bore Size";
										Qitem.B_Price="0.00";
										Qitem.R_Code=PN.Rod_Diamtr.ToString().Trim();
										Qitem.Rod=db.SelectValue("Rod_Size","WEB_RodSerZ_TableV1","Rod_Code",PN.Rod_Diamtr.ToString().Trim())+" Rod Size";
										Qitem.R_Price="0.00";
										Qitem.Stroke_Code=PN.Stroke.ToString().Trim();
										Qitem.Stroke="Stroke = "+PN.Stroke.ToString()+"\""+st.ToString();
										Qitem.Stroke_Price="0.00";
										Qitem.M_code=PN.Mount.ToString().Trim();
										Qitem.Mount=db.SelectValue("Mount_Type","WEB_Mount"+PN.Series.Trim()+"_TableV1","Mount_Code",PN.Mount.ToString().Trim()).ToString();
										Qitem.M_Price="0.00";
										Qitem.RE_Code=PN.Rod_End.ToString().Trim();
										Qitem.RodEnd=db.SelectValue("RodEnd_Shape","WEB_RodEndKK_TableV1","RodEnd_Code",PN.Rod_End.ToString().Trim()).ToString()+rodenddim.ToString();
										Qitem.RE_Price="0.00";
										Qitem.Cu_Code=PN.Cushions.ToString().Trim();
										Qitem.Cushion=db.SelectValue("Cushion_type","WEB_Cushion_TableV1","Cushion_Code",PN.Cushions.ToString()).ToString();
										Qitem.Cu_Price="0.00";
										Qitem.CushionPos_Code=PN.CushionPosition.ToString().Trim();
										Qitem.CushionPos=db.SelectValue("CushionPos_Pos","WEB_CushionPos_TableV1","CushionPos_Code",PN.CushionPosition.ToString().Trim()).ToString();
										Qitem.CushionPos_Price="0.00";
										Qitem.Sel_Code=PN.Seal_Comp.ToString().Trim();
										Qitem.Seal=db.SelectValue("Seal_Type","WEB_Seal_TableV1","Seal_Code",PN.Seal_Comp.ToString().Trim()).ToString();
										Qitem.Sel_Price="0.00";
										Qitem.Port_Code=PN.Port_Type.ToString().Trim();
										Qitem.Port=portsiz.Trim()+" "+ db.SelectValue("PortType_Type","WEB_portType_TableV1","PortType_Code",PN.Port_Type.ToString().Trim()).ToString();
										Qitem.Port_Price="0.00";
										Qitem.PP_Code=PN.Port_Pos.ToString().Trim();
										Qitem.PortPos=db.SelectValue("PortPos_Position","WEB_PortPosition_TableV1","PortPos_Code",PN.Port_Pos.ToString().Trim()).ToString();
										Qitem.PP_Price="0.00";
										Qitem.Quantity=cod.Qty.ToString();
										//discount
										if(Qitem.S_Code.Trim()=="A" || Qitem.S_Code.Trim()=="AC" || Qitem.S_Code.Trim()=="AT" )
										{
											Qitem.Discount="0";
										}
										else
										{
											if(PN.Series =="ML" || PN.Series =="L" )
											{
												decimal qty=0;
												qty=Convert.ToDecimal(PN.Qty.ToString());
												if(qty >0 && qty <=2)
												{
													Qitem.Discount="35";
												}
												else if(qty >=3 && qty <=5)
												{
													Qitem.Discount="37";
												}
												else if(qty >=6 && qty <=10)
												{
													Qitem.Discount="40";
												}
												else if(qty >10)
												{
													Qitem.Discount="45";
												}
											}
											else
											{
												Qitem.Discount="35";
											}
										}
										Qitem.Cusomer_ID=quot.Customer.Trim();
										Qitem.Quotation_No=quot.QuoteNo.Trim();
										Qitem.Q_Date=quot.Quotedate.Trim();
										Qitem.User_ID=lst[0].ToString();
										Qitem.PriceList="0";
										Qitem.DWG ="";
										string dwgnotes="";								
										Qitem.SpecialReq=dwgnotes.ToString();
										Qitem.Note=cod.Notes.ToString();
										//								string commadd="";
										//								if(Qitem.S_Code.Trim() =="ML" || Qitem.S_Code.Trim() =="L" )
										//								{
										//									commadd="WEB_Series"+Qitem.S_Code.Trim()+"CommAdders_TableV1";
										//								}
										//								else if(Qitem.S_Code.Trim() =="A")
										//								{
										//									commadd="WEB_SeriesA_OEM_CommAdders_TableV1";
										//								}
										//								else if(Qitem.S_Code.Trim() =="AC")
										//								{
										//									commadd="WEB_SeriesAC_CommAdders_TableV1";
										//								}
										//special
										if(PN.Specials !=null)
										{
											if(PN.Specials.ToString().ToUpper().Trim() =="SPECIAL")
											{
												decimal total =0.00m;
												bool result1=false;
												string sp ="";
												int tt=Convert.ToInt32(db.SelectLastSpNo());
												sp=(tt+1).ToString();
												PN.PNO ="Z"+PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim()+
													PN.Cushions.ToString().Trim()+PN.Mount.ToString().Trim()+"/Z"+sp.ToString();
												LblPartNo.Text ="Z"+PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim()+
													PN.Cushions.ToString().Trim()+PN.Mount.ToString().Trim()+"/Z"+sp.ToString();
												if(PN.Style !=null)
												{
													if(PN.Style.Trim() !="")
													{
														if(PN.Style.Trim() =="FC" || PN.Style.Trim() =="FO")
														{
															total =0.00m;
															if(total ==0)
															{
																result1 =true;
															}
															string des="";
															des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Style.Trim());
															string sp1 ="";
															int tt1=Convert.ToInt32(db.SelectLastSpNo());
															sp1=(tt1+1).ToString();
															db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Style.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"1");
															db.InsertSpecialCount(sp1.Trim());
														}
													}
												}								
												if(result1 ==true)
												{
													lblstatus.Text= "true";
												}
											}
											else
											{
												Qitem.Special_ID="";
											}
										}
										else
										{
											Qitem.Special_ID="";
										}
										//commadd
										string commadd="";
										decimal index =0.00m;
										if(Qitem.S_Code.Trim() !="")
										{
											commadd="WEB_Series"+Qitem.S_Code.Trim()+"CommAdders_TableV1";
											string ind="";
											if(PN.Bore_Size.ToString().Trim()=="C" || PN.Bore_Size.ToString().Trim()=="D" || PN.Bore_Size.ToString().Trim()=="E")
											{			
												ind=db.SelectPriceIndex("MLS").ToString();	
											}
											else
											{
												ind=db.SelectPriceIndex(Qitem.S_Code.Trim()).ToString();	
											}
									
											index =Convert.ToDecimal(ind.Trim());
										}
										//appopt
										Qitem.ApplicationOpt_Id="";							
										//popopt
										decimal pop=0.00m;
										if(PN.Coating.ToString().Trim() !="")
										{
											decimal coating =0.00m;	
											decimal liftinglegs=0.00m;
											//									decimal qtydisc =0.00m;	
											if(PN.Coating.ToString().Trim() =="C10" || PN.Coating.ToString().Trim() =="C11")
											{
												if(PN.Coating.ToString().Trim() =="C11")
												{
													//											qtydisc=Convert.ToDecimal(Qitem.Discount);
													coating=0;
													coating =db.SelectAddersPrice("C11",commadd.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.Trim());
													coating =coating + coating * (index /100);
													//											coating =coating * (1 - (qtydisc/100)); 
													if(coating ==0)
													{
														lblstatus.Text ="true";
													}
												}
												string desc="";
												desc=db.SelectPopopts(PN.Coating.Trim());
												db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.Coating.ToString(),desc.ToString(),"0.00");
											}	
											if(PN.LiftingLugs.ToString().Trim() !="")
											{
												//										qtydisc=Convert.ToDecimal(Qitem.Discount);
												liftinglegs =db.SelectAddersPrice(PN.LiftingLugs.ToString().Trim(),commadd.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
												liftinglegs =liftinglegs + liftinglegs * (index /100);
												//										liftinglegs =liftinglegs * (1 - (qtydisc/100));
												if(liftinglegs ==0)
												{
													lblstatus.Text ="true";
												}
												string desc="";
												desc=db.SelectPopopts(PN.LiftingLugs.Trim());
												db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.LiftingLugs.ToString(),desc.ToString(),"0.00");
											}									
											pop = coating +liftinglegs;
											Qitem.PopularOpt_Id=quot.QuoteNo.Trim()+LblPartNo.Text.Trim();
										}								
										else
										{
											Qitem.PopularOpt_Id="";
										}
										//issue #315 start
										if(PN.Series.ToString().Trim()=="A" || PN.Series.ToString().Trim()=="AC" || PN.Series.ToString().Trim()=="AT" || PN.Series.ToString().Trim()=="AS")
										{
											decimal discount=Convert.ToDecimal(db.SelectDiscount(lst[1].ToString().Trim(),"A"));
											//app = app*(1 - (discount/100));
											pop = pop*(1 - (discount/100));
											//sptotal= sptotal*(1 - (discount/100));
										}
										//issue #315 end
										decimal d1,d2=0.00m;			
										d1 =Convert.ToDecimal(lbltotal.Text);
										d2 =d1 +  pop;							
										decimal dc1,dc3,dctotal=0.00m;
										decimal curency=1.00m;	
										//currency
										if(quot.Currency.ToString().Trim() =="CN $")	
										{	
											dctotal=d2 *curency;
											lbltotal.Text=dctotal.ToString();
											dc1=Convert.ToDecimal(cod.Qty.ToString());
											dc3= dc1 * dctotal ;
										}
										else
										{
											curency=Convert.ToDecimal(db.SelectValue("USD_Q","Dollar_ExchangeRate_TableV1","Slno","1"));
											dctotal=d2 *curency;
											lbltotal.Text=dctotal.ToString();
											dc1=Convert.ToDecimal(cod.Qty.ToString());
											dc3= dc1 * dctotal ;
										}
										if(lblspecsheet.Text.Trim() !="")
										{
											lblstatus.Text="true";
										}
										if(lblstatus.Text.ToLower().Trim() =="true" || dc3 > 100000)
										{
											Qitem.UnitPrice ="0.00";
											Qitem.TotalPrice="0.00";
										}
										else
										{
											Qitem.UnitPrice=lbltotal.Text.ToString();
											Qitem.TotalPrice=dc3.ToString();
										}
										Qitem.Special_ID="";
										Qitem.PartNo=LblPartNo.Text.ToString();
										//weight
										if(PN.Series.Trim() !="")
										{
											string weight="";
											decimal c1,c2,c22,c3=0.00m;
											decimal c4=0.00m;
											decimal c5=0.00m;
											if(PN.Series.Trim()== "A" )
											{
												//issue weight start backup
												//										if(PN.DoubleRod.ToUpper().Trim() =="YES")
												//										{
												//											c1=db.SelectAddersPrice("DWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												//											c2=db.SelectAddersPrice("DStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												//											c3=Convert.ToDecimal(PN.Stroke.Trim());
												//										}
												//										else
												//										{
												//											c1=db.SelectAddersPrice("SWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												//											c2=db.SelectAddersPrice("SStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												//											c3=Convert.ToDecimal(PN.Stroke.Trim());
												//										}
												//										c4=c1 + (c2 * c3);
												//										c4=Decimal.Round(c4,0);
												//issue weight end backup
												//issue weight start update
												if(PN.TandemDuplex ==null && PN.DoubleRod.ToUpper().Trim() =="NO")
												{
													c1=db.SelectAddersPrice("SWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													c2=db.SelectAddersPrice("SStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													c4=c1+c2*Convert.ToDecimal(PN.Stroke.Trim());
													c4=Decimal.Round(c4,0);
												}
												if(PN.TandemDuplex ==null && PN.DoubleRod.ToUpper().Trim() =="YES")
												{
													c1=db.SelectAddersPrice("DWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													c2=db.SelectAddersPrice("DStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													c4=c1+c2*Convert.ToDecimal(PN.Stroke.Trim());
													c4=Decimal.Round(c4,0);
												}
												if(PN.TandemDuplex !=null && PN.DoubleRod.ToUpper().Trim() =="NO")
												{
													c1=db.SelectAddersPrice("SWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													c2=db.SelectAddersPrice("SStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													c22=db.SelectAddersPrice("DStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													c4=c1*1.75m+(c2+c22)*Convert.ToDecimal(PN.Stroke.Trim());
													c4=Decimal.Round(c4,0);
												}
												if(PN.TandemDuplex !=null && PN.DoubleRod.ToUpper().Trim() =="YES")
												{
													c1=db.SelectAddersPrice("DWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													c22=db.SelectAddersPrice("DStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													c4=c1*1.75m+2m*c22*Convert.ToDecimal(PN.Stroke.Trim());
													c4=Decimal.Round(c4,0);
												}
												//issue weight end update
											}
											else if(PN.Series.Trim()== "M" || PN.Series.Trim()== "ML" || PN.Series.Trim()== "L")
											{
												if(PN.Bore_Size.Trim() =="P" || PN.Bore_Size.Trim() =="R" || PN.Bore_Size.Trim() =="S" || PN.Bore_Size.Trim() =="T" || PN.Bore_Size.Trim() =="W" || PN.Bore_Size.Trim() =="X")
												{
													if(PN.DoubleRod.ToUpper().Trim() =="YES")
													{
														c1=db.SelectAddersPrice("DWeight","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
														c2=db.SelectAddersPrice("DStroke","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										
														if(PN.Mount.Substring(0,2) =="T1" || PN.Mount.Substring(0,2) =="T2")
														{
															c5=db.SelectAddersPrice("SWeight1","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
														}
														else if(PN.Mount.Substring(0,2) =="E5" || PN.Mount.Substring(0,2) =="E6" || PN.Mount.Substring(0,2) =="T4")
														{
															c5=db.SelectAddersPrice("SWeight2","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
														}
														else if(PN.Mount.Substring(0,2) =="F5" || PN.Mount.Substring(0,2) =="F6" )
														{
															c5=db.SelectAddersPrice("SWeight3","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
														}
														else if(PN.Mount.Substring(0,2) =="P1" || PN.Mount.Substring(0,2) =="S2" || PN.Mount.Substring(0,2) =="S3")
														{
															c5=db.SelectAddersPrice("SWeight4","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
														}
														else
														{
															c5=db.SelectAddersPrice("SWeight1","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
														}
														c3=Convert.ToDecimal(PN.Stroke.Trim());
														c4= c1 + c5 + (c2 * c3);
														c4=Decimal.Round(c4,0);	
													}
													else
													{
														c2=db.SelectAddersPrice("SStroke","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										
														if(PN.Mount.Substring(0,2) =="T1" || PN.Mount.Substring(0,2) =="T2")
														{
															c5=db.SelectAddersPrice("SWeight1","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
														}
														else if(PN.Mount.Substring(0,2) =="E5" || PN.Mount.Substring(0,2) =="E6" || PN.Mount.Substring(0,2) =="T4")
														{
															c5=db.SelectAddersPrice("SWeight2","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
														}
														else if(PN.Mount.Substring(0,2) =="F5" || PN.Mount.Substring(0,2) =="F6" )
														{
															c5=db.SelectAddersPrice("SWeight3","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
														}
														else if(PN.Mount.Substring(0,2) =="P1" || PN.Mount.Substring(0,2) =="S2" || PN.Mount.Substring(0,2) =="S3")
														{
															c5=db.SelectAddersPrice("SWeight4","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
														}
														else
														{
															c5=db.SelectAddersPrice("SWeight1","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
														}	
														c3=Convert.ToDecimal(PN.Stroke.Trim());
														c4=c5 + (c2 * c3);
														c4=Decimal.Round(c4,0);								
													}
												}
												else if(PN.Bore_Size.Trim() !="P" && PN.Bore_Size.Trim() !="R" && PN.Bore_Size.Trim() !="S" && PN.Bore_Size.Trim() !="T" && PN.Bore_Size.Trim() !="W" && PN.Bore_Size.Trim() !="X")
												{
													if( PN.DoubleRod.ToUpper().Trim() =="YES" )
													{
														if(PN.Mount.Substring(0,1) =="P" || PN.Mount.Substring(0,1) =="T" || PN.Mount.Substring(0,1) =="E" || PN.Mount.Substring(0,1) =="S")
														{
															c1=db.SelectAddersPrice("DWeight2","WEB_SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
														}
														else
														{
															c1=db.SelectAddersPrice("DWeight1","SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
														}
														c2=db.SelectAddersPrice("DStroke","WEB_SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													else
													{
														if(PN.Mount.Substring(0,1) =="P" || PN.Mount.Substring(0,1) =="T" || PN.Mount.Substring(0,1) =="E" || PN.Mount.Substring(0,1) =="S")
														{
															c1=db.SelectAddersPrice("SWeight2","WEB_SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
														}
														else
														{
															c1=db.SelectAddersPrice("SWeight1","SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
														}
														c2=db.SelectAddersPrice("SStroke","WEB_SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													c3=Convert.ToDecimal(PN.Stroke.Trim());
													c4=c1 + (c2 * c3);
													c4=Decimal.Round(c4,0);
												}
											}
											if(c4 !=0)
											{
												weight="Approximate cylinder weight (does not include specials)= "+c4.ToString()+" LBS";
												//issue #233 start
												weight +=";Cylinder Displacement = " + Cyl_Displacement(PN) + " in" + Convert.ToChar(0179).ToString();
												//issue #233 end
											}
											else
											{
												weight="";
											}
											Qitem.Weight=weight.ToString();
										}								
										string strd="";
										//insertitem
										strd= db.InsertItems(Qitem);
										if(strd.ToString().Trim() =="1")
										{
											if(PN.Style !=null)
											{
												if(PN.Style.ToString().Trim() !="")
												{
													Opts opts=new Opts();
													string ff="";
													if(PN.Style.Trim() =="FC")
													{
														ff="Style: Fail Close\r\n";
													}
													else if(PN.Style.Trim() =="FO")
													{
														ff="Style: Fail Open\r\n";
													}
													else
													{
														ff="Style: Double Acting \r\n";
													}								
													opts.Opt =ff.Trim();
													opts.Code=quot.QuoteNo.Trim();
													string temp=db.Insertcomments(opts);
												}
											}
											Implist.Add(lblstatus.Text.ToString().ToLower().Trim());								
										}
									//issue #263 start
										if(strDelivery!="Consult factory")
										{
											strDelivery=db.SelectDelivery(PN.Series, PN.Bore_Size, Convert.ToDecimal(PN.Qty));
										}
									//issue #263 end
									}
									catch( Exception ex)
									{
										string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
										s=s.Replace("'"," ");
										LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
									}
								}
							
						//series end
									//series
								else if(cod.PneumaticHydraulic.ToString().ToUpper().Trim() =="PNEUMATIC WITH SPRING")
								{
									string strTest = "";
									if(cod.Style.ToString().ToUpper().Trim() !="FAIL CLOSE" && cod.Style.ToString().ToUpper().Trim() !="FAIL OPEN")
									{
										throw new System.ArgumentException("There is no canister size match for item: " + (i+1).ToString()+" !");
									}
									PN=ASSeries_PN(cod);
									string ind=db.SelectPriceIndex("AS").ToString();
									ASPricing(PN);
									//issue #263 start
									if(PN.Specials=="SPECIAL") ASSeries_SP(PN, quot);
									//issue #263 end
									QItems Qitem = ASSeries_QItem(PN,quot, cod, lst[0].ToString());
									ASSeries_Pop(PN,quot,Qitem);
									//issue #315 start
									//issue #263 start
									if(strDelivery!="Consult factory")
									{
										strDelivery=db.SelectDelivery(PN.Series, PN.Bore_Size, Convert.ToDecimal(PN.Qty));
									}
									Implist.Add(lblstatus.Text.ToString().ToLower().Trim());
									if(PN.FailMode !=null)
									{
										if(PN.FailMode.ToString().Trim() !="")
										{
											Opts opts=new Opts();
											string ff="";
											if(PN.FailMode.Trim() =="FC")
											{
												ff="Style: Fail Close\r\n";
											}
											else if(PN.FailMode.Trim() =="FO")
											{
												ff="Style: Fail Open\r\n";
											}
											else
											{
												ff="Style: Double Acting \r\n";
											}								
											opts.Opt =ff.Trim();
											opts.Code=quot.QuoteNo.Trim();
											string temp=db.Insertcomments(opts);
										}
									}
									//issue #263 end
									//currency
									if(lblstatus.Text.ToLower().Trim() =="true")
									{
										Qitem.UnitPrice ="0.00";
										Qitem.TotalPrice="0.00";
									}
									
										//currency
									else
									{
										lbltotal.Text=Convert.ToString( Convert.ToDecimal(lbltotal.Text)*(1+Convert.ToDecimal(Convert.ToDecimal(ind)/100m)));
										//issue #315 start
										decimal discount=Convert.ToDecimal(db.SelectDiscount(lst[1].ToString().Trim(),"[AS]"));
										lbltotal.Text=Convert.ToString( Convert.ToDecimal(lbltotal.Text)*(1-discount/100.00m));
										//issue #315 end
										Qitem.UnitPrice=lbltotal.Text;
										Qitem.TotalPrice=Convert.ToString(Convert.ToDecimal(Qitem.Quantity)*Convert.ToDecimal(Qitem.UnitPrice));
										//currency
										//issue #251 start
										decimal curency=1.00m;
										if(quot.Currency.ToString().Trim() =="US $")	
										{	
											curency=Convert.ToDecimal(db.SelectValue("USD_Q","Dollar_ExchangeRate_TableV1","Slno","1"));
											Qitem.UnitPrice=Convert.ToString(Convert.ToDecimal(Qitem.UnitPrice)*curency);
											Qitem.TotalPrice=Convert.ToString(Convert.ToDecimal(Qitem.TotalPrice)*curency);
											
										}
										//issue #251 end
										
									
									}
									
									string strd= db.InsertItems(Qitem);
								}
							}
							
							}
						bool pricestatus=true;
						for(int p=0;p<Implist.Count;p++)
						{
							if(Implist[p].ToString().Trim() =="true")
							{
								pricestatus=false;
							}
						}
						//issue #263 start
						quot.Delivery=strDelivery;
						//issue #263 end
						if(pricestatus ==true)
						{
							quot.Finish="1";
							string str =db.SelectCustomerQno(quot.QuoteNo.ToString().Trim());
							if(str.ToString() !="")
							{
								string s =db.UpdateCustomerQuote(quot,str.Trim());
							}
							else
							{
								string s =db.InsertCustomerQuote(quot);
							}
							db.UpdateCustomerQuote_UpLoadFile(lblup.Text.Trim(),str.ToString());
							lblstatus.Text="";
							Response.Redirect("ManageQ.aspx?id="+quot.QuoteNo.Trim()+"&pn=9999");		
						}
						else
						{
							PSend.Visible=true;
							Session["Quote"] = quot;
							quot.Finish="0";
							if(TxtProjectNo.Text.Trim() =="")
							{
								quot.Note ="This Quote require assitance from the factory.Please Contact Cowan Dynamics";
							}
							string str =db.SelectCustomerQno(quot.QuoteNo.ToString().Trim());
							if(str.ToString() !="")
							{
								string s =db.UpdateCustomerQuote(quot,str.Trim());
							}
							else
							{
								string s =db.InsertCustomerQuote(quot);
							}
							db.UpdateCustomerQuote_UpLoadFile(lblup.Text.Trim(),str.ToString());
							lblstatus.Text="";
						}	
						if(cyltype ==false)
						{
							Opts opts=new Opts();
							string ff="Pricing is quantity sensitive. Quantities shown to be ordered & shipped at one time.";
							opts.Opt =ff.Trim();
							opts.Code=quot.QuoteNo.Trim();
							string temp=db.Insertcomments(opts);
						}
					}
					else
					{
						LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Please fix the error in the uploaded file !!!')</script>";
					}
				}
				else
				{
					LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Please Upload a file/Please fix the error in the uploaded file !!!')</script>";
				}
			}
			catch( Exception ex)
			{
				lblup.Text="";
				LblUpload.Text="";
				lblpage.Text="";
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ");
				s=s.Replace("'"," ");
				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}

		private void LBSend_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(Page.IsValid)
				{
					DBClass db=new DBClass();
					ArrayList lst =new ArrayList();
					lst =(ArrayList)Session["User"];
					Quotation quote=new Quotation();
					quote =(Quotation)Session["Quote"];
					ArrayList lst1 =new ArrayList();
					lst1 =(ArrayList)Session["User"];
					MailMessage mail=new MailMessage();
					SmtpMail.SmtpServer ="k2smtpout.secureserver.net"; 
					mail.From= "admin@cowandynamics.com";
					mail.To = "dtaranu@cowandynamics.com";
					mail.Bcc="admin@cowandynamics.com";
					mail.Cc="jbehara@cowandynamics.com";
					mail.BodyFormat =MailFormat.Html;
					mail.Subject=   "["+DDLPriority.SelectedItem.Text.Trim() +"] New Quote from "+quote.Customer.Trim();
					if(DDLPriority.SelectedIndex ==0)
					{
						mail.Priority =MailPriority.Normal;
					}
					else if(DDLPriority.SelectedIndex ==1)
					{
						mail.Priority =MailPriority.High;
					}
					mail.Body =						
						"<hr color='#FF0000'>Bonjour, <br> "+lst1[0].ToString()+" a une nouvelle demande de prix:<br>"
						+"<p><TABLE id='Table1' borderColor='#0000ff' cellSpacing='1' cellPadding='1' align='left' border='1'>"
						+"<TR><TD noWrap>Date de demande :</TD><TD noWrap>"+quote.Quotedate.Trim()+"</TD></TR>"
						+"<TR><TD noWrap>No de demande :</TD><TD noWrap>"+quote.QuoteNo.ToString().Trim()+"</TD></TR>"
						+"<TR><TD noWrap>Special Request Note :</TD><TD noWrap>"+TxtSpecialReq.Text.Trim()+"</TD></TR></TABLE></p>"
						+"<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>Clic sur le lien ci-dessous � r�pondre: <a href='http://172.16.0.252/'>Quote System</a>"
						+"<br><br><hr> Merci <br>I-Cylinder<br><hr color='#FF0000'><br>"								
						+"<hr color='#FF0000'>Hi, <br> "+lst1[0].ToString()+" has entered a new request for a quote:<br>"
						+"<p><TABLE id='Table1' borderColor='#0000ff' cellSpacing='1' cellPadding='1' align='left' border='1'>"
						+"<TR><TD noWrap>Quote Date :</TD><TD noWrap>"+quote.Quotedate.Trim()+"</TD></TR>"
						+"<TR><TD noWrap>Quote No :</TD><TD noWrap>"+quote.QuoteNo.ToString().Trim()+"</TD></TR>"
						+"<TR><TD noWrap>Special Request Note :</TD><TD style='width:200' noWrap>"+TxtSpecialReq.Text.Trim()+"</TD></TR></TABLE></p>"
						+"<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>Please log into <a href='http://172.16.0.252/'>Quote System</a> to complete the Quote."
						+"<br><br><hr> Thanks <br>I-Cylinder<br><hr color='#FF0000'>";
					if(lblspecsheet.Text.Trim() !="")
					{
						MailAttachment attach=new MailAttachment(lblspecsheet.Text.ToString().Trim());
						mail.Attachments.Add(attach);
					}
					//issue #582 start
					mail.Body += csSignature.Get_Admin();
					//issue #582 end
					SmtpMail.Send(mail);
					db.Insert_NewRFQ_ICYL(lst[0].ToString().Trim(),quote.QuoteNo.ToString(),quote.Customer.ToString()
						,lst[0].ToString().Trim(),TxtSpecialReq.Text.ToString(),"",DateTime.Today.ToShortDateString(),DateTime.Today.AddDays(0).ToShortDateString()
						,"Select","0","1.00");
					Response.Redirect("Result.aspx?id="+quote.QuoteNo.Trim());
				}
			}
			catch (Exception ex)
			{
				lblerr.Text=ex.Message;
			}
		}
	}
}
