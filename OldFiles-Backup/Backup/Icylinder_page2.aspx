<%@ Page language="c#" Codebehind="Icylinder_page2.aspx.cs" AutoEventWireup="false" Inherits="iCylinderV1.Icylinder_page2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Icylinder_page2</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 1000px; HEIGHT: 487px" cellSpacing="0" cellPadding="0"
				width="1000" bgColor="lightgrey" border="0">
				<TR>
					<TD width="50" height="20"></TD>
					<TD style="WIDT: 484px" align="center" height="20">
						<asp:label id="Label2" runat="server" Font-Size="Small" Font-Underline="True" BorderColor="Transparent"
							BackColor="Transparent">Bore & Stroke</asp:label></TD>
					<TD align="center" height="20">
						<asp:label id="Label1" runat="server" Font-Size="Small" Font-Underline="True" BorderColor="Transparent"
							BackColor="Transparent">Rod & Rod End</asp:label></TD>
					<TD align="center" height="20">
						<asp:label id="lbl2nd" runat="server" Font-Size="Small" Font-Underline="True" BorderColor="Transparent"
							BackColor="Transparent" Visible="False">2nd Rod & Rod End</asp:label></TD>
					<TD width="50" height="20"></TD>
				</TR>
				<TR>
					<TD width="50"></TD>
					<TD language="300" align="center" bgColor="#dcdcdc">
						<asp:Panel id="Panel1" runat="server" BorderColor="White" BorderStyle="Solid" BorderWidth="1px">
							<TABLE id="Table3" border="0" cellSpacing="0" borderColor="silver" cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label4" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
											Font-Size="Smaller" ForeColor="Maroon" Font-Bold="True">Bore Size</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:radiobuttonlist id="RBLBore" runat="server" Font-Size="Smaller" BorderStyle="None" AutoPostBack="True"
											RepeatDirection="Horizontal" RepeatColumns="2"></asp:radiobuttonlist></TD>
								</TR>
							</TABLE>
						</asp:Panel>
						<asp:Panel id="Panel2" runat="server" BorderColor="White" BorderStyle="Solid" BorderWidth="1px">
							<TABLE style="HEIGHT: 8px" id="Table5" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label5" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
											Font-Size="Smaller" ForeColor="Maroon" Font-Bold="True">Stroke</asp:label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:textbox id="TxtStroke" runat="server" Font-Size="XX-Small" AutoPostBack="True" Width="64px"></asp:textbox></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:label id="Label3" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Size="XX-Small"
											ForeColor="Red">Min= 0.00, Max= 120.00(Consult Factory, if stroke >120)</asp:label></TD>
								</TR>
							</TABLE>
							<asp:LinkButton id="BtnAdvanced" runat="server" Font-Size="8pt">Click Here for Stop Tube</asp:LinkButton>
						</asp:Panel>
						<asp:Panel id="PStoptube" runat="server" Visible="False" BorderColor="White" BorderStyle="Solid"
							BorderWidth="1px">
							<TABLE id="Table6" border="0" cellSpacing="0" borderColor="silver" cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:RadioButtonList id="RBStrokeType" runat="server" Font-Size="Smaller" RepeatDirection="Horizontal"
											Width="216px" RepeatLayout="Flow">
											<asp:ListItem Value="Standard" Selected="True">Standard</asp:ListItem>
											<asp:ListItem Value="Double Piston Design">Double Piston Design</asp:ListItem>
										</asp:RadioButtonList></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:label id="Label47" runat="server" BackColor="Transparent" Font-Size="Smaller" Width="128px"
											Height="19">Stop Tube Length :</asp:label>
										<asp:TextBox id="TxtStopTube" runat="server" Font-Size="XX-Small" AutoPostBack="True" Width="53"></asp:TextBox></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:Label id="Label7" runat="server" Font-Size="Smaller" Width="128px">Enter Effective Stroke :</asp:Label>
										<asp:TextBox id="TxtEffectiveStrok" runat="server" Font-Size="XX-Small" AutoPostBack="True" Width="53px"></asp:TextBox></TD>
								</TR>
							</TABLE>
						</asp:Panel></TD>
					<TD align="center" bgColor="#dcdcdc">
						<asp:Panel id="Panel4" runat="server" BorderColor="White" BorderStyle="Solid" BorderWidth="1px">
							<TABLE style="HEIGHT: 35px" id="Table2" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label6" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
											Font-Size="Smaller" ForeColor="Maroon" Font-Bold="True">Rod</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:radiobuttonlist id="RBL1stRodSize" runat="server" Font-Size="Smaller" BorderStyle="None" AutoPostBack="True"
											RepeatDirection="Horizontal" RepeatColumns="4"></asp:radiobuttonlist></TD>
								</TR>
							</TABLE>
						</asp:Panel>
						<asp:Panel id="Panel5" runat="server" BorderColor="White" BorderStyle="Solid" BorderWidth="1px">
							<TABLE id="Table8" border="0" cellSpacing="0" borderColor="silver" cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label9" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
											Font-Size="Smaller" ForeColor="Maroon" Font-Bold="True">Rod End</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:image id="Img1stRodEnd" runat="server" Width="100px" Height="70px"></asp:image></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:radiobuttonlist id="RBLRodend1" runat="server" Font-Size="Smaller" BorderStyle="None" AutoPostBack="True"
											RepeatDirection="Horizontal" Width="144px" RepeatLayout="Flow" Height="1px">
											<asp:ListItem Value="Standard" Selected="True">Standard</asp:ListItem>
											<asp:ListItem Value="Metric">Metric</asp:ListItem>
										</asp:radiobuttonlist></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:radiobuttonlist id="RBLRodend2" runat="server" Font-Size="Smaller" BorderStyle="None" AutoPostBack="True"
											RepeatDirection="Horizontal" RepeatColumns="4" Width="248px" RepeatLayout="Flow">
											<asp:ListItem Value="Male" Selected="True">Male</asp:ListItem>
											<asp:ListItem Value="Female">Female</asp:ListItem>
											<asp:ListItem Value="Plain">Plain</asp:ListItem>
											<asp:ListItem Value="Flange">Flange</asp:ListItem>
										</asp:radiobuttonlist></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:label id="Label8" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Size="Smaller">KK</asp:label>
										<asp:dropdownlist id="DDLRodend" runat="server" Font-Size="XX-Small" AutoPostBack="True" Width="106px"
											Height="80px"></asp:dropdownlist></TD>
								</TR>
							</TABLE>
							<asp:LinkButton id="LB1stRodAdvanced" runat="server" Font-Size="8pt">Advanced Options</asp:LinkButton>
						</asp:Panel>
						<asp:Panel id="P1stAdvanced" runat="server" Visible="False" BorderColor="White" BorderStyle="Solid"
							BorderWidth="1px">
							<TABLE id="Table4" border="0" cellSpacing="0" borderColor="silver" cellPadding="0" width="299">
								<TR>
									<TD>
										<asp:label id="Label13" runat="server" BackColor="Transparent" Font-Size="Smaller" Width="88px"
											Height="19"> A Dimension :</asp:label>
										<asp:textbox id="TXTThread" runat="server" Font-Size="XX-Small" AutoPostBack="True" Width="52px"
											Height="20px"></asp:textbox>
										<asp:Label id="Label68" runat="server" Font-Size="XX-Small" ForeColor="SlateGray">(Min = 0.00 & Max = 12.00)</asp:Label></TD>
								</TR>
								<TR>
									<TD>
										<asp:label id="Label14" runat="server" BackColor="Transparent" Font-Size="Smaller" Width="88px"
											Height="19"> W  Dimension :</asp:label>
										<asp:textbox id="TXTRodEx" runat="server" Font-Size="XX-Small" AutoPostBack="True" Width="52px"
											Height="20px"></asp:textbox>
										<asp:Label id="lblw" runat="server" Font-Size="XX-Small" ForeColor="SlateGray">(Min = 0.00 & Max = 24.00)</asp:Label></TD>
								</TR>
							</TABLE>
						</asp:Panel></TD>
					<TD align="center" bgColor="gainsboro">
						<asp:Panel id="P2ndRod" runat="server" Visible="False" BorderColor="White" BorderStyle="Solid"
							BorderWidth="1px">
							<TABLE style="HEIGHT: 35px" id="Table7" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label10" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
											Font-Size="Smaller" ForeColor="Maroon" Font-Bold="True">Rod</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:radiobuttonlist id="RBL2ndRodSize" runat="server" Font-Size="Smaller" BorderStyle="None" AutoPostBack="True"
											RepeatDirection="Horizontal" RepeatColumns="4"></asp:radiobuttonlist></TD>
								</TR>
							</TABLE>
						</asp:Panel>
						<asp:Panel id="P2ndRodnd" runat="server" Visible="False" BorderColor="White" BorderStyle="Solid"
							BorderWidth="1px">
							<TABLE id="Table9" border="0" cellSpacing="0" borderColor="silver" cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label12" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
											Font-Size="Smaller" ForeColor="Maroon" Font-Bold="True">Rod End</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:image id="Img2ndRodend" runat="server" Width="100px" Height="70px"></asp:image></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:radiobuttonlist id="RBL2Rodend1" runat="server" Font-Size="Smaller" BorderStyle="None" AutoPostBack="True"
											RepeatDirection="Horizontal" RepeatColumns="2" Width="200px" RepeatLayout="Flow">
											<asp:ListItem Value="Standard" Selected="True">Standard</asp:ListItem>
											<asp:ListItem Value="Metric">Metric</asp:ListItem>
										</asp:radiobuttonlist></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:radiobuttonlist id="RBL2Rodend2" runat="server" Font-Size="Smaller" BorderStyle="None" AutoPostBack="True"
											RepeatDirection="Horizontal" RepeatColumns="4" Width="229px" RepeatLayout="Flow">
											<asp:ListItem Value="Male" Selected="True">Male</asp:ListItem>
											<asp:ListItem Value="Female">Female</asp:ListItem>
											<asp:ListItem Value="Plain">Plain</asp:ListItem>
											<asp:ListItem Value="Flange">Flange</asp:ListItem>
										</asp:radiobuttonlist></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:label id="Label11" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Size="Smaller">KK</asp:label>
										<asp:dropdownlist id="DDL2Rodend" runat="server" Font-Size="XX-Small" AutoPostBack="True" Width="106px"
											Height="80px"></asp:dropdownlist></TD>
								</TR>
							</TABLE>
							<asp:LinkButton id="LB2ndRodAdvanced" runat="server" Font-Size="8pt">Advanced Options</asp:LinkButton>
						</asp:Panel>
						<asp:Panel id="P2ndAdvanced" runat="server" Visible="False" BorderColor="White" BorderStyle="Solid"
							BorderWidth="1px">
							<TABLE id="Table10" border="0" cellSpacing="0" borderColor="silver" cellPadding="0" width="299">
								<TR>
									<TD>
										<asp:label id="Label18" runat="server" BackColor="Transparent" Font-Size="Smaller" Width="88px"
											Height="19"> A Dimension :</asp:label>
										<asp:textbox id="TxtSecThreadEx" runat="server" Font-Size="XX-Small" AutoPostBack="True" Width="53"
											Height="20px"></asp:textbox>
										<asp:Label id="Label17" runat="server" Font-Size="XX-Small" ForeColor="SlateGray">(Min = 0.00 & Max = 12.00)</asp:Label></TD>
								</TR>
								<TR>
									<TD>
										<asp:label id="Label16" runat="server" BackColor="Transparent" Font-Size="Smaller" Width="88px"
											Height="19"> W  Dimension :</asp:label>
										<asp:textbox id="TxtSecRodEx" runat="server" Font-Size="XX-Small" AutoPostBack="True" Width="53px"
											Height="20px"></asp:textbox>
										<asp:Label id="lblsecminmax1" runat="server" Font-Size="XX-Small" ForeColor="SlateGray">(Min = 0.00 & Max = 24.00)</asp:Label></TD>
								</TR>
							</TABLE>
						</asp:Panel></TD>
					<TD width="50"></TD>
				</TR>
				<TR>
					<TD align="right" width="50" height="30">
						<asp:LinkButton id="LBBack" runat="server" Font-Bold="True" Font-Size="Smaller">Back</asp:LinkButton></TD>
					<TD language="300" align="center" height="30">
						<asp:label id="lblstroke" runat="server" BackColor="Transparent" BorderColor="Transparent"
							Font-Size="X-Small" Visible="False" ForeColor="Red"></asp:label></TD>
					<TD align="center" height="30"></TD>
					<TD align="center" height="30"></TD>
					<TD width="50" height="30">
						<asp:LinkButton id="BtnNext" runat="server" Font-Bold="True" Font-Size="Smaller">Next</asp:LinkButton></TD>
				</TR>
				<TR>
					<TD width="50" height="20"></TD>
					<TD language="300" align="center" height="20"></TD>
					<TD align="center" height="20">
						<asp:label id="lblhidden" runat="server" Font-Size="XX-Small" Visible="False"></asp:label></TD>
					<TD align="center" height="20"></TD>
					<TD width="50" height="20"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
