<%@ Page language="c#" Codebehind="Icylinder_Page1.aspx.cs" AutoEventWireup="false" Inherits="iCylinderV1.Icylinder_Page1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Icylinder_Page1</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE style="WIDTH: 1000px; HEIGHT: 487px" id="Table1" border="0" cellSpacing="0" cellPadding="0"
				width="1000" bgColor="lightgrey">
				<TR>
					<TD height="20" width="50"></TD>
					<TD height="20" align="center"><asp:label id="Label2" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
							Font-Size="Small">Series</asp:label></TD>
					<TD height="20" width="50"></TD>
				</TR>
				<TR>
					<TD height="20" width="50"></TD>
					<TD height="20" align="center"><asp:linkbutton style="Z-INDEX: 0" id="LBBatchQuote" runat="server" Font-Size="Smaller" Font-Bold="True">Click Here for ML Series Batch Quote</asp:linkbutton>
						&nbsp;&nbsp;&nbsp;<asp:LinkButton id="LBBatchQuote_AS" runat="server" Font-Size="X-Small" Font-Bold="True">Click Here for A Series Batch Quote</asp:LinkButton></TD>
					<TD height="20" width="50"></TD>
				</TR>
				<TR>
					<TD width="50"></TD>
					<TD bgColor="gainsboro" align="center"><asp:radiobuttonlist id="RBLSeries" runat="server" Font-Size="Smaller" AutoPostBack="True" RepeatColumns="3"></asp:radiobuttonlist></TD>
					<TD width="50"></TD>
				</TR>
				<TR>
					<TD height="30" width="50" align="right"><asp:linkbutton id="LBHome" runat="server" Font-Size="Smaller" Font-Bold="True">Home</asp:linkbutton></TD>
					<TD bgColor="#dcdcdc" height="30" align="center"><asp:radiobuttonlist id="RBLDoubleOrSingle" runat="server" BorderColor="#404040" Font-Size="Smaller"
							RepeatDirection="Horizontal" Height="8px" Width="200px">
							<asp:ListItem Value="Single Ended" Selected="True">Single Ended</asp:ListItem>
							<asp:ListItem Value="Double Ended">Double Ended</asp:ListItem>
						</asp:radiobuttonlist></TD>
					<TD height="30" width="50"><asp:linkbutton id="BtnNext" runat="server" Font-Size="Smaller" Font-Bold="True">Next</asp:linkbutton></TD>
				</TR>
				<TR>
					<TD height="20" width="50"></TD>
					<TD height="20" align="center"><asp:label id="lblstroke" runat="server" BackColor="Transparent" BorderColor="Transparent"
							Font-Size="XX-Small" Visible="False" ForeColor="Red">Min= 0.00, Max= 120.00(Consult Factory, if stroke >120)</asp:label><asp:label id="lblhidden" runat="server" Font-Size="XX-Small" Visible="False"></asp:label></TD>
					<TD height="20" width="50"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
