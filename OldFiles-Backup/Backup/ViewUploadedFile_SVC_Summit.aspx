<%@ Page language="c#" Codebehind="ViewUploadedFile_SVC_Summit.aspx.cs" AutoEventWireup="false" Inherits="iCylinderV1.ViewUploadedFile_SVC_Summit" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ViewUploadedFile_SVC_Summit</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body leftMargin="0" rightMargin="0" topMargin="0">
		<form id="Form1" method="post" runat="server">
			<asp:datagrid style="Z-INDEX: 0" id="DGView" runat="server" OnItemDataBound="DGView_Item_Command"
				Font-Size="9pt">
				<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="Gray"></HeaderStyle>
			</asp:datagrid>
			<asp:label style="Z-INDEX: 0" id="lbl" runat="server"></asp:label>
			<asp:label style="Z-INDEX: 0" id="lblspec" runat="server"></asp:label>
			<asp:Label style="Z-INDEX: 0" id="Lblerror" runat="server" Font-Size="9pt" ForeColor="Red"></asp:Label>
			<asp:label style="Z-INDEX: 0" id="Lblerr" runat="server" Font-Size="9pt" ForeColor="Red" Font-Underline="True"
				Font-Bold="True">Excel file Error</asp:label>
		</form>
	</body>
</HTML>
