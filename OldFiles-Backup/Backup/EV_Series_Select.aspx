<%@ Page language="c#" Codebehind="EV_Series_Select.aspx.cs" AutoEventWireup="false" Inherits="iCylinderV1.EV_Series_Select" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>EV_Series_Select</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
  </HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE style="Z-INDEX: 0; WIDTH: 1000px; HEIGHT: 487px" id="Table1" border="0" cellSpacing="0"
				cellPadding="0" width="1000" bgColor="lightgrey">
				<TR>
					<TD height="20" width="50"></TD>
					<TD height="20" align="center">
						<asp:label id="Label2" runat="server" Font-Size="Small" Font-Underline="True" BorderColor="Transparent"
							BackColor="Transparent">Series</asp:label></TD>
					<TD height="20" width="50"></TD>
				</TR>
				<TR>
					<TD width="50"></TD>
					<TD bgColor="gainsboro" align="center">
      <TABLE id=Table2 border=1 cellSpacing=0 borderColor=silver cellPadding=0 
      width=300>
        <TR>
          <TD align=center>
<asp:label style="Z-INDEX: 0" id=Label18 runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller" Font-Bold="True" ForeColor="Maroon">Series</asp:label>
						<asp:radiobuttonlist id="RBLSeries" runat="server" Font-Size="Smaller" RepeatColumns="2" RepeatDirection="Horizontal" style="Z-INDEX: 0"></asp:radiobuttonlist></TD></TR>
        <TR>
          <TD align=center>
<asp:label style="Z-INDEX: 0" id=Label1 runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True" Font-Size="Smaller" Font-Bold="True" ForeColor="Maroon">Style</asp:label>
<asp:radiobuttonlist style="Z-INDEX: 0" id=RBLStyle runat="server" Font-Size="Smaller" RepeatColumns="1">
<asp:ListItem Value="OF" Selected="True">Open Frame</asp:ListItem>
<asp:ListItem Value="CF">Close Frame</asp:ListItem>
</asp:radiobuttonlist></TD></TR></TABLE></TD>
					<TD width="50"></TD>
				</TR>
				<TR>
					<TD height="30" width="50" align="right"></TD>
					<TD bgColor="#dcdcdc" height="30" align="center"></TD>
					<TD height="30" width="50">
						<asp:linkbutton id="BtnNext" runat="server" Font-Size="Smaller" Font-Bold="True">Next</asp:linkbutton></TD>
				</TR>
				<TR>
					<TD height="20" width="50"></TD>
					<TD height="20" align="center"></TD>
					<TD height="20" width="50"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
