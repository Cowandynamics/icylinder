using System;
using System.Collections;
namespace iCylinderV1
{
	/// <summary>
	/// Summary description for PartNumber.
	/// </summary>
	public class coder
	{
		public coder()
		{
         pack="0";
		 units="imperial";
		 btoreq="0";
         liftinglug="";
		}

		private string series;
		private string bore;
		private string rod;
		private string rodend;
//		private string rodend2;
//		private string rodend3;
		private string cushions;
		private string cushionpos;
		private ArrayList applicationopt =new ArrayList();
		private string secroddiam;
		private string gdrain;
		private string metalscrap;
		private string magnet;
		private string secrodend;
		private string seal;
		private string pistonsealcof;
		private string rodsealcof;
		//issue #90 start
		private string tierodecentsupp;
		//issue #90 end
		private string tandemduplex;
		private string bushingcof;
		private string porttype;
		private string portpos;
		private string mount;
		private string stroke;
		private string doublestroke;
		private ArrayList popularopt =new ArrayList();
		private string threadex;
		private string secrodthreadex;
		private string coating;
		private string cfbarrel;
		private string sstierod;
		private string sspistionrod;
		private string airbleeds;
		private string portsize;
		private string stoptube;
		private string rodext;
		private string secrodex;
		private string trunnions;
		private string bbc;
		private string bbh;
		private string pno;
		private string doublerod; 
		private string trans; 
		private string style; 
		private string pr; 
		private string prpno; 
		private string sv; 
		private string svclass; 
		private string fc; 
		private string fr; 
		private string tub; 
		private string mo;
		private string rs; 
		private string sp; 
		private string valvesize; 
		private string minair; 
		private string seatingtrust;
		private string linearpress; 
		private string mkit;
		private string limitswitch; 
		private string lcsp; 
		private string cid;
		private string pack; 
		private string sf; 
		private string asf;
		private string ct;
		private string fstyle;
		private string slno;
		private string qty;
		private string metaltag;

		private string bto;
		private string rto; 
		private string eto; 
		private string btc;
		private string rtc; 
		private string etc; 
		private string pcp;
		private string ratings;
		private string ctph;
		private string ctpl;
		private string sfph;
		private string sfpl;
		private string allss; 
		private string allssc; 
		private string thirdPartypn; 
		private string rodboot;
		private string liftinglug;
		private bool mlprice;
		//issue #137 start
		private string canisterno;
		private string failmode;
		private string springrate;
        private string preload;
		private string rodenddim;
		private string etcvalvetrust;
		private string btoreq;
        private string cyldis;
		//issue #242 start
		private string units;
		public string Units
		{
			get
			{
				return units;
			}
			set
			{
				units = value;
			}
		}
		//issue #242 end
		public string CylDis
		{
			get
			{
				return cyldis;
			}
			set
			{
				cyldis = value;
			}
		}
		public string BTOReq
		{
			get
			{
				return btoreq;
			}
			set
			{
				btoreq = value;
			}
		}
		public string EtcValveTrust
		{
			get
			{
				return etcvalvetrust;
			}
			set
			{
				etcvalvetrust = value;
			}
		}
		public string RodendDim
		{
			get
			{
				return rodenddim;
			}
			set
			{
				rodenddim = value;
			}
		}
		public string Preload
		{
			get
			{
				return preload;
			}
			set
			{
				preload = value;
			}
		}
		public string CanisterNo
		{
			get
			{
				return canisterno;
			}
			set
			{
				canisterno = value;
			}
		}
		public string FailMode
		{
			get
			{
				return failmode;
			}
			set
			{
				failmode = value;
			}
		}
		public string SpringRate
		{
			get
			{
				return springrate;
			}
			set
			{
				springrate = value;
			}
		}
		//issue #137 end
		
		public bool MLPrice
		{
			get
			{
				return mlprice;
			}
			set
			{
				mlprice = value;
			}
		}

		public string Series
		{
			get
			{
				return series;
			}
			set
			{
				series = value;
			}
		}

		public string Bore_Size
		{
			get
			{
				return bore;
			}
			set
			{
				bore = value;
			}
		}

		public string Rod_Diamtr
		{
			get
			{
				return rod;
			}
			set
			{
				rod = value;
			}
		}
		public string Rod_End
		{
			get
			{
				return rodend;
			}
			set
			{
				rodend = value;
			}
		}
		public string Cushions
		{
			get
			{
				return cushions;
			}
			set
			{
				cushions = value;
			}
		}

		public ArrayList Application_Opt
		{
			get
			{
				return applicationopt;
			}
		}

		public string CushionPosition
		{
			get
			{
				return cushionpos;
			}
			set
			{
				cushionpos = value;
			}
		}
		public string SecRodDiameter
		{
			get
			{
				return secroddiam;
			}
			set
			{
				secroddiam = value;
			}
		}

		public string GlandDrain
		{
			get
			{
				return gdrain;
			}
			set
			{
				gdrain = value;
			}
		}
		public string MetalScrapper
		{
			get
			{
				return metalscrap;
			}
			set
			{
				metalscrap = value;
			}
		}
		public string MagnetRing
		{
			get
			{
				return magnet;
			}
			set
			{
				magnet = value;
			}
		}
		public string SecRodEnd
		{
			get
			{
				return secrodend;
			}
			set
			{
				secrodend = value;
			}
		}
		public string Seal_Comp
		{
			get
			{
				return seal;
			}
			set
			{
				seal = value;
			}
		}
		public string PistonSeal_Conf
		{
			get
			{
				return pistonsealcof;
			}
			set
			{
				pistonsealcof = value;
			}
		}
		public string RodSeal_Conf
		{
			get
			{
				return rodsealcof;
			}
			set
			{
				rodsealcof = value;
			}
		}
		//issue #90 start
		public string TieRodeCentSupp
		{
			get
			{
				return tierodecentsupp;
			}
			set
			{
				tierodecentsupp = value;
			}
		}
		//issue #90 end
		public string Bushing_Conf
		{
			get
			{
				return bushingcof;
			}
			set
			{
				bushingcof = value;
			}
		}
		
		public string Port_Type
		{
			get
			{
				return porttype;
			}
			set
			{
				porttype = value;
			}
		}
		public string Port_Pos
		{
			get
			{
				return portpos;
			}
			set
			{
				portpos = value;
			}
		}
		public string Mount
		{
			get
			{
				return mount;
			}
			set
			{
				mount = value;
			}
		}
		public string Stroke
		{
			get
			{
				return stroke;
			}
			set
			{
				stroke = value;
			}
		}
		public string DoubleStroke
		{
			get
			{
				return doublestroke;
			}
			set
			{
				doublestroke = value;
			}
		}
		public ArrayList Popular_Opt
		{
			get
			{
				return popularopt;
			}
		}	
		public string ThreadEx
		{
			get
			{
				return threadex;
			}
			set
			{
				threadex = value;
			}
		}
		public string SecRodThreadx
		{
			get
			{
				return secrodthreadex;
			}
			set
			{
				secrodthreadex = value;
			}
		}
		public string Coating
		{
			get
			{
				return coating;
			}
			set
			{
				coating = value;
			}
		}
		public string CarbonFibBarrel
		{
			get
			{
				return cfbarrel;
			}
			set
			{
				cfbarrel = value;
			}
		}
		public string SSTieRod
		{
			get
			{
				return sstierod;
			}
			set
			{
				sstierod = value;
			}
		}
		public string SSPistionRod
		{
			get
			{
				return sspistionrod;
			}
			set
			{
				sspistionrod = value;
			}
		}
		public string AirBleed
		{
			get
			{
				return airbleeds;
			}
			set
			{
				airbleeds = value;
			}
		}
		public string PortSize
		{
			get
			{
				return portsize;
			}
			set
			{
				portsize = value;
			}
		}
		public string StopTube
		{
			get
			{
				return stoptube;
			}
			set
			{
				stoptube = value;
			}
		}

		public string RodEx
		{
			get
			{
				return rodext;
			}
			set
			{
				rodext = value;
			}
		}
		public string SecRodEx
		{
			get
			{
				return secrodex;
			}
			set
			{
				secrodex = value;
			}
		}
		public string Trunnions
		{
			get
			{
				return trunnions;
			}
			set
			{
				trunnions = value;
			}
		}
		public string BBC
		{
			get
			{
				return bbc;
			}
			set
			{
				bbc = value;
			}
		}
		public string BBH
		{
			get
			{
				return bbh;
			}
			set
			{
				bbh = value;
			}
		}
		public string PNO
		{
			get
			{
				return pno;
			}
			set
			{
				pno = value;
			}
		}
		public string DoubleRod
		{
			get
			{
				return doublerod;
			}
			set
			{
				doublerod = value;
			}
		}
		public string TandemDuplex
		{
			get
			{
				return tandemduplex;
			}
			set
			{
				tandemduplex = value;
			}
		}
		public string Transducer
		{
			get
			{
				return trans;
			}
			set
			{
				trans = value;
			}
		}
		public string Style
		{
			get
			{
				return style;
			}
			set
			{
				style = value;
			}
		}
		public string Positioner
		{
			get
			{
				return pr;
			}
			set
			{
				pr = value;
			}
		}
		public string PositionerPno
		{
			get
			{
				return prpno;
			}
			set
			{
				prpno = value;
			}
		}
		public string Solenoid
		{
			get
			{
				return sv;
			}
			set
			{
				sv = value;
			}
		}
		public string SolenoidClass
		{
			get
			{
				return svclass;
			}
			set
			{
				svclass = value;
			}
		}
		public string FlowControl
		{
			get
			{
				return fc;
			}
			set
			{
				fc = value;
			}
		}
		public string FilterRegulator
		{
			get
			{
				return fr;
			}
			set
			{
				fr  = value;
			}
		}
		public string Tubing
		{
			get
			{
				return tub;
			}
			set
			{
				tub = value;
			}
		}
		public string ManualOverride
		{
			get
			{
				return mo;
			}
			set
			{
				mo = value;
			}
		}
		public string ReedSwitch
		{
			get
			{
				return rs;
			}
			set
			{
				rs = value;
			}
		}
		public string ValveSize
		{
			get
			{
				return valvesize;
			}
			set
			{
				valvesize  = value;
			}
		}
		public string LinearPressure
		{
			get
			{
				return linearpress;
			}
			set
			{
				linearpress = value;
			}
		}
		public string MinAirSupply
		{
			get
			{
				return minair;
			}
			set
			{
				minair = value;
			}
		}
		public string SeatingTrust
		{
			get
			{
				return seatingtrust;
			}
			set
			{
				seatingtrust = value;
			}
		}
		public string Specials
		{
			get
			{
				return sp;
			}
			set
			{
				sp = value;
			}
		}
		public string MountKit
		{
			get
			{
				return mkit;
			}
			set
			{
				mkit = value;
			}
		}
		public string LimitSwitch
		{
			get
			{
				return limitswitch;
			}
			set
			{
				limitswitch = value;
			}
		}
		public string SpecialLimitSwitch
		{
			get
			{
				return lcsp;
			}
			set
			{
				lcsp = value;
			}
		}
		public string CompanyID
		{
			get
			{
				return cid;
			}
			set
			{
				cid = value;
			}
		}
		public string PackingFriction
		{
			get
			{
				return pack;
			}
			set
			{
				pack = value;
			}
		}
		public string SaftyFactor
		{
			get
			{
				return sf;
			}
			set
			{
				sf = value;
			}
		}
		public string ActualSaftyFactor
		{
			get
			{
				return asf;
			}
			set
			{
				asf = value;
			}
		}
		public string ClosingTime
		{
			get
			{
				return ct;
			}
			set
			{
				ct = value;
			}
		}
		public string FrameStyle
		{
			get
			{
				return fstyle;
			}
			set
			{
				fstyle = value;
			}
		}
		public string Slno
		{
			get
			{
				return slno;
			}
			set
			{
				slno = value;
			}
		}
		public string Qty
		{
			get
			{
				return qty;
			}
			set
			{
				qty = value;
			}
		}
		public string BTO
		{
			get
			{
				return bto;
			}
			set
			{
				bto = value;
			}
		}
		public string RTO
		{
			get
			{
				return rto;
			}
			set
			{
				rto = value;
			}
		}
		public string ETO
		{
			get
			{
				return eto;
			}
			set
			{
				eto = value;
			}
		}
		public string BTC
		{
			get
			{
				return btc;
			}
			set
			{
				btc = value;
			}
		}
		public string RTC
		{
			get
			{
				return rtc;
			}
			set
			{
				rtc = value;
			}
		}
		public string ETC
		{
			get
			{
				return etc;
			}
			set
			{
				etc = value;
			}
		}
		public string PneumaticP
		{
			get
			{
				return pcp;
			}
			set
			{
				pcp = value;
			}
		}
		public string Ratings
		{
			get
			{
				return ratings;
			}
			set
			{
				ratings = value;
			}
		}
		public string CylinderThrustPush
		{
			get
			{
				return ctph;
			}
			set
			{
				ctph = value;
			}
		}
		public string CylinderThrustPull
		{
			get
			{
				return ctpl;
			}
			set
			{
				ctpl = value;
			}
		}
		public string SafetyFactorPush
		{
			get
			{
				return sfph;
			}
			set
			{
				sfph = value;
			}
		}
		public string SafetyFactorPull
		{
			get
			{
				return sfpl;
			}
			set
			{
				sfpl = value;
			}
		}
		public string Metaltag
		{
			get
			{
				return metaltag;
			}
			set
			{
				metaltag = value;
			}
		}
		public string AllSS
		{
			get
			{
				return allss;
			}
			set
			{
				allss = value;
			}
		}
		public string AllSS_Carbon
		{
			get
			{
				return allssc;
			}
			set
			{
				allssc = value;
			}
		}
		public string ThirdPartyPN
		{
			get
			{
				return thirdPartypn;
			}
			set
			{
				thirdPartypn = value;
			}
		}
		public string RodBoot
		{
			get
			{
				return rodboot;
			}
			set
			{
				rodboot = value;
			}
		}
		public string LiftingLugs
		{
			get
			{
				return liftinglug;
			}
			set
			{
				liftinglug = value;
			}
		}
	}
}
