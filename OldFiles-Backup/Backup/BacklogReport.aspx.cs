using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for BacklogReport.
	/// </summary>
	public class BacklogReport : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid DGBacklog;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Image ImgLogo;
		protected System.Web.UI.WebControls.Label Lbllinks;
		protected System.Web.UI.WebControls.LinkButton LbSearch;
		protected System.Web.UI.WebControls.TextBox TxtSearch;
		protected System.Web.UI.WebControls.DropDownList DDLSearch;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label CID1;
		protected System.Web.UI.WebControls.Label CID2;
		protected System.Web.UI.WebControls.Label err;
		public ICollection CreateDataSource(string customerid1,string customerid2,string searchid,string searchvalue) 
		{
			try
			{
				DBClass db=new DBClass();
				DataSet ds=new DataSet();
				if(searchid.Trim()=="")
				{
					ds =db.BackLogReport(customerid1.Trim(),customerid2.Trim());
				}
				else if(searchid.Trim()=="PO")
				{
					ds =db.BackLogReport_PoNo(customerid1.Trim(),customerid2.Trim(),searchvalue.ToString().Trim());
				}
				else if(searchid.Trim()=="PN")
				{
					ds =db.BackLogReport_Project(customerid1.Trim(),customerid2.Trim(),searchvalue.ToString().Trim());
				}
				//issue #105 start
				else if(searchid.Trim()=="PA")
				{
					ds =db.BackLogReport_PartNo(customerid1.Trim(),customerid2.Trim(),searchvalue.ToString().Trim());
				}
				//issue #105 end
				else if(searchid.Trim()=="CR")
				{
					ds =db.BackLogReport_CustomerRef(customerid1.Trim(),customerid2.Trim(),searchvalue.ToString().Trim());
				}
				DataTable dt = new DataTable();
				DataRow dr;
				dt.Columns.Add(new DataColumn("Code"));
				dt.Columns.Add(new DataColumn("PoNo"));
				dt.Columns.Add(new DataColumn("PartNo"));
				dt.Columns.Add(new DataColumn("Qty"));
				dt.Columns.Add(new DataColumn("UnitPrice"));
				dt.Columns.Add(new DataColumn("SoNo"));
				dt.Columns.Add(new DataColumn("EntryDate",System.Type.GetType("System.DateTime")));
				dt.Columns.Add(new DataColumn("DeliveryDate",System.Type.GetType("System.DateTime")));
				dt.Columns.Add(new DataColumn("CustomerRef"));
				dt.Columns.Add(new DataColumn("ProjectRef"));
				dt.Columns.Add(new DataColumn("OrderStatus"));	
				if(ds.Tables[0].Rows.Count >0)
				{
					for (int i = 0; i < ds.Tables[0].Rows.Count; i++) 
					{
						dr = dt.NewRow();
						dr[0] = (string) ds.Tables[0].Rows[i]["CustomerCode"].ToString().Substring(0,1);
						dr[1] = (string) ds.Tables[0].Rows[i]["PoNo"].ToString();
						dr[2] = (string) ds.Tables[0].Rows[i]["PartNo"].ToString();
						dr[3] = (string) ds.Tables[0].Rows[i]["Qty"].ToString();
						dr[4] = String.Format("{0:C}", Convert.ToDecimal(ds.Tables[0].Rows[i]["UnitPrice"].ToString()));
						dr[5] = (string) ds.Tables[0].Rows[i]["SoNo"].ToString();
						if(ds.Tables[0].Rows[i]["EntryDate"].ToString() !="")
						{
							dr[6] = Convert.ToDateTime(ds.Tables[0].Rows[i]["EntryDate"]);
						}
						if(ds.Tables[0].Rows[i]["DeliveryDate"].ToString() !="")
						{
							dr[7] = Convert.ToDateTime( ds.Tables[0].Rows[i]["DeliveryDate"]);
						}
						dr[8] = (string) ds.Tables[0].Rows[i]["CustomerRef"].ToString();
						dr[9] = (string) ds.Tables[0].Rows[i]["ProjectRef"].ToString();
						dr[10] = (string) ds.Tables[0].Rows[i]["OrderStatus"].ToString();					
						dt.Rows.Add(dr);
					}	
				}
				else
				{
					dr = dt.NewRow();
					dr[0] = "";					
					dr[8] = "No Back log Items..";										
					dt.Rows.Add(dr);
				}
				DataView dv = new DataView(dt);			
				dv.Sort = "DeliveryDate ASC";
				return dv;
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s=s.Replace("'"," ");
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				return null;
			}	
		}

		private void Page_Load(object sender, System.EventArgs e)
		{		
			if(Session["user"] !=null)
			{
				if(! IsPostBack)
				{
					ArrayList lst =new ArrayList();
					lst =(ArrayList)Session["User"];					
					ImgLogo.ImageUrl=lst[6].ToString().Trim()+".gif";					
					//#588
					//if(lst[6].ToString().Trim() =="1012" || lst[6].ToString().Trim() =="1027" || lst[6].ToString().Trim() =="1028" || lst[6].ToString().Trim() =="1029")
					if(lst[6].ToString().Trim() =="1012" )
					{
						Lbllinks.Text="<p class='title_txt'><strong>Links:</strong><ol>"
							+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_P_Series_Catalogue.pdf' target='_blank'>P Series NFPA Pneumatic.</a>"
							//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_A_Series_Catalogue.pdf' target='_blank'>A Series Pneumatic Actuators.</a>"
							+"<li><a href='http://www.cowandynamics.com/include/get.php?nodeid=1890' target='_blank'>Spring Return Pneumatic Actuator</a>"
							+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=75' target='_blank'>A Series Pneumatic Actuators</a>"
							+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=1907' target='_blank'>AT Series Pneumatic Actuators with Transducer </a>"
							+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=593' target='_blank'>Series ML</a>"
							//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_L_Series_Catalogue.pdf' target='_blank'>L Series Servo Cylinder.</a>"
							+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_R5_Series_Catalogue.pdf' target='_blank'>R5 Series Mill Type.</a>"
							+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_N_Series_Catalogue.pdf' target='_blank'>N Series Medium Duty Hydraulic.</a>"
							+"</li></ol>";
						DGBacklog.DataSource=CreateDataSource("EASTERN","CHADWIC","","");
						DGBacklog.DataBind();
						DGBacklog.Columns[0].Visible=true;
						DGBacklog.Columns[9].Visible=false;
						CID1.Text="EASTERN";
						CID2.Text="CHADWIC";
					}
					else if(lst[6].ToString().Trim() =="1013")
					{
						Lbllinks.Text="<p class='title_txt'><strong>Links:</strong><ol>"
							+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_P_Series_Catalogue.pdf' target='_blank'>P Series NFPA Pneumatic.</a>"
							//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_A_Series_Catalogue.pdf' target='_blank'>A Series Pneumatic Actuators.</a>"
							+"<li><a href='http://www.cowandynamics.com/include/get.php?nodeid=1890' target='_blank'>Spring Return Pneumatic Actuator</a>"
							+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=75' target='_blank'>A Series Pneumatic Actuators</a>"
							+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=1907' target='_blank'>AT Series Pneumatic Actuators with Transducer </a>"
							+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=593' target='_blank'>Series ML</a>"
							//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_L_Series_Catalogue.pdf' target='_blank'>L Series Servo Cylinder.</a>"
							+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_R5_Series_Catalogue.pdf' target='_blank'>R5 Series Mill Type.</a>"
							+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_N_Series_Catalogue.pdf' target='_blank'>N Series Medium Duty Hydraulic.</a>"
							+"</li></ol>";
						DGBacklog.DataSource=CreateDataSource("CHADWIC","CHADWIC","","");
						DGBacklog.DataBind();
						DGBacklog.Columns[0].Visible=false;
						DGBacklog.Columns[9].Visible=false;
						CID1.Text="CHADWIC";
						CID2.Text="CHADWIC";
					}
					//issue #118
					//backup
                    //else if(lst[6].ToString().Trim() =="1002")
					//updaye
					else if(lst[6].ToString().Trim() =="1002" || lst[6].ToString().Trim() =="1021")
					//issue #118
					{
						Lbllinks.Text="<p class='title_txt'><strong>Links:</strong><ol>"
							//issue #246 start
							//back
							//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_A_Series_Catalogue.pdf' target='_blank'>A Series Pneumatic Actuators.</a>"
							//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_ML_Series_Catalogue.pdf' target='_blank'>ML Series Heavy Duty Hydraulic.</a>"
							//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_L_Series_Catalogue.pdf' target='_blank'>L Series Servo Cylinder.</a>"
							//update
							+"<li><a href='http://www.cowandynamics.com/include/get.php?nodeid=1890' target='_blank'>Spring Return Pneumatic Actuator</a>"
							+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=75' target='_blank'>A Series Pneumatic Actuators</a>"
							+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=1907' target='_blank'>AT Series Pneumatic Actuators with Transducer </a>"
							+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=593' target='_blank'>Series ML</a>"
							//issue #246 end
							+"</li></ol>";
						DGBacklog.DataSource=CreateDataSource("SLURRYF","SLURRYF","","");
						DGBacklog.DataBind();
						DGBacklog.Columns[0].Visible=false;
						DGBacklog.Columns[9].Visible=true;
						CID1.Text="SLURRYF";
						CID2.Text="SLURRYF";
					}
					else if(lst[6].ToString().Trim() =="1015")
					{
						Lbllinks.Text="<p class='title_txt'><strong>Links:</strong><ol>"
							//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_A_Series_Catalogue.pdf' target='_blank'>A Series Pneumatic Actuators.</a>"
							+"<li><a href='http://www.cowandynamics.com/include/get.php?nodeid=1890' target='_blank'>Spring Return Pneumatic Actuator</a>"
							+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=75' target='_blank'>A Series Pneumatic Actuators</a>"
							+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=1907' target='_blank'>AT Series Pneumatic Actuators with Transducer </a>"
							+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=593' target='_blank'>Series ML</a>"
							//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_L_Series_Catalogue.pdf' target='_blank'>L Series Servo Cylinder.</a>"
							+"</li></ol>";
						DGBacklog.DataSource=CreateDataSource("SUMMITV","SUMMITV","","");
						DGBacklog.DataBind();
						DGBacklog.Columns[0].Visible=false;
						DGBacklog.Columns[9].Visible=true;
						CID1.Text="SUMMITV";
						CID2.Text="SUMMITV";
					}
					else if(lst[6].ToString().Trim() =="1030")
					{
						Lbllinks.Text="<p class='title_txt'><strong>Links:</strong><ol>"
							+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_P_Series_Catalogue.pdf' target='_blank'>P Series NFPA Pneumatic.</a>"
							//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_A_Series_Catalogue.pdf' target='_blank'>A Series Pneumatic Actuators.</a>"
							+"<li><a href='http://www.cowandynamics.com/include/get.php?nodeid=1890' target='_blank'>Spring Return Pneumatic Actuator</a>"
							+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=75' target='_blank'>A Series Pneumatic Actuators</a>"
							+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=1907' target='_blank'>AT Series Pneumatic Actuators with Transducer </a>"
							+"<li><a href='http://cowandynamics.com/include/get.php?nodeid=593' target='_blank'>Series ML</a>"
							//+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_L_Series_Catalogue.pdf' target='_blank'>L Series Servo Cylinder.</a>"
							+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_R5_Series_Catalogue.pdf' target='_blank'>R5 Series Mill Type.</a>"
							+"<li><a href='http://www.cowandynamics.com/pdf/Cowan_Dynamics_N_Series_Catalogue.pdf' target='_blank'>N Series Medium Duty Hydraulic.</a>"
							+"</li></ol>";
						DGBacklog.DataSource=CreateDataSource("ROULEM","ROULEM","","");
						DGBacklog.DataBind();
						DGBacklog.Columns[0].Visible=true;
						DGBacklog.Columns[9].Visible=false;
						CID1.Text="ROULEM";
						CID2.Text="ROULEM";
					}
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.LbSearch.Click += new System.EventHandler(this.LbSearch_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		//issue #105 start
		protected void DGArchive_Command(Object sender,DataGridCommandEventArgs e) 
		{
//			DBClass db=new DBClass();
//			try
//			{
//				if(e.CommandName =="Dwg_Command")
//				{
//					string dwg="";
//					string quoteno="";
//					string debug = e.Item.Cells[11].Text.ToString();
//					quoteno=db.SelectQno_BySoNo_ByPaNo(e.Item.Cells[5].Text.ToString(),e.Item.Cells[11].Text.ToString());
//					dwg=db.SelectSTDDrawing(quoteno,e.Item.Cells[11].Text.ToString());
//					if(dwg.ToString().Trim() !="2")
//					{
//						err.Text="<script language='javascript'>window.open('Drawings.aspx?qno="+quoteno+"&pno="+e.Item.Cells[11].Text.ToString()+"','_blank','toolbar=yes,width=1000,height=800,resizable=yes,top=0,left=0')</script>";
//					}
//					else
//					{
//						err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
//					}
//				}
//				
//			}
//			catch(Exception ex)
//			{
//				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
//				s.Replace("'"," ");
//				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
//			}
			 
		}
		//issue #105 end
		private void LbSearch_Click(object sender, System.EventArgs e)
		{
			//issue 226 start
			DGBacklog.CurrentPageIndex = 0 ;
			//issue #226 end
			//back
			//if(TxtSearch.Text.ToString().Trim() !="")
			if(TxtSearch.Text.ToString().Trim() !="" || TxtSearch.Text.ToString().Trim() =="")
			{
				
				DGBacklog.DataSource=CreateDataSource(CID1.Text,CID2.Text,DDLSearch.SelectedItem.Value.ToString().Trim(),TxtSearch.Text.ToString().Trim());
				DGBacklog.DataBind();
				DGBacklog.Columns[0].Visible=false;
			}
		}

		//issue 226 start
		protected void  DGBacklog_PageChanger(Object Source ,DataGridPageChangedEventArgs E)
		{
			string strTest = "";
			try
			{
				DGBacklog.CurrentPageIndex =E.NewPageIndex ;
				if(TxtSearch.Text.ToString().Trim() !="" || TxtSearch.Text.ToString().Trim() =="")
				{
					DGBacklog.DataSource=CreateDataSource(CID1.Text,CID2.Text,DDLSearch.SelectedItem.Value.ToString().Trim(),TxtSearch.Text.ToString().Trim());
					if(DGBacklog.PageCount<E.NewPageIndex) DGBacklog.CurrentPageIndex=0;
				    DGBacklog.DataBind();
					DGBacklog.Columns[0].Visible=false;
				}
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s=s.Replace("'"," ");
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				err.Text="";
				
			}	
//			switch (DDLSearch.SelectedItem.Value)
//			{
//				case "PO":
//					ds =db.BackLogReport_PoNo(customerid1.Trim(),customerid2.Trim(),searchvalue.ToString().Trim());
//					break;
//				case "PN":
//					break;
//				case "CR":
//					break;
//				case "PA":
//					break;
//			}
		
		}
		//issue 226 end
	}
}
