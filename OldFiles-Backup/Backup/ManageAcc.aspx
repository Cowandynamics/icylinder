<%@ Page language="c#" Codebehind="ManageAcc.aspx.cs" AutoEventWireup="false" Inherits="iCylinderV1.ManageAcc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ManageAcc1</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 1000px; HEIGHT: 487px" cellSpacing="0" cellPadding="0"
				width="1000" bgColor="lightgrey" border="0">
				<TR>
					<TD width="50" height="20"></TD>
					<TD align="center" height="20">
						<asp:label id="Label2" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
							Font-Size="Small">Accessories</asp:label></TD>
					<TD width="50" height="20"></TD>
				</TR>
				<TR>
					<TD width="50"></TD>
					<TD align="center" bgColor="gainsboro">
						<TABLE id="Table2" borderColor="gray" cellSpacing="0" cellPadding="0" width="900" border="0"
							height="400">
							<TR>
								<TD align="center">
									<TABLE id="Table3" style="WIDTH: 234px; HEIGHT: 48px" borderColor="silver" cellSpacing="0"
										cellPadding="0" width="234" border="0">
										<TR>
											<TD style="HEIGHT: 13px" align="center">
												<asp:label id="Label3" runat="server" Font-Size="Smaller" Font-Underline="True" BackColor="Transparent"
													Width="232px" Height="19">Cap End Accessories</asp:label></TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 13px" align="center">
												<asp:ImageButton id="IBCap" runat="server" ImageUrl="accessories\capendaccessory.jpg"></asp:ImageButton></TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 66px" align="center">
												<asp:LinkButton id="LBcap" runat="server" Font-Size="Smaller">Configure</asp:LinkButton></TD>
										</TR>
									</TABLE>
								</TD>
								<TD align="center">
									<TABLE id="Table4" style="WIDTH: 234px; HEIGHT: 48px" borderColor="silver" cellSpacing="0"
										cellPadding="0" width="234" border="0">
										<TR>
											<TD style="HEIGHT: 13px" align="center">
												<asp:label id="Label1" runat="server" Font-Size="Smaller" Font-Underline="True" BackColor="Transparent"
													Width="232px" Height="19">Rod End Accessories</asp:label></TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 13px" align="center">
												<asp:ImageButton id="IBRod" runat="server" ImageUrl="accessories\rodendaccessory.jpg"></asp:ImageButton></TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 66px" align="center">
												<asp:LinkButton id="LBRod" runat="server" Font-Size="Smaller">Configure</asp:LinkButton></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</TD>
					<TD width="50"></TD>
				</TR>
				<TR>
					<TD align="right" width="50" height="30">
						<asp:LinkButton id="LBHome" runat="server" Font-Size="Smaller" Font-Bold="True">Home</asp:LinkButton></TD>
					<TD align="center" bgColor="#dcdcdc" height="30">
						<asp:label id="lblhidden" runat="server" Font-Size="XX-Small" Visible="False"></asp:label></TD>
					<TD width="50" height="30">
						<asp:LinkButton id="BtnNext" runat="server" Font-Size="Smaller" Font-Bold="True">Next</asp:LinkButton></TD>
				</TR>
				<TR>
					<TD width="50" height="20"></TD>
					<TD align="center" height="20"></TD>
					<TD width="50" height="20"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
