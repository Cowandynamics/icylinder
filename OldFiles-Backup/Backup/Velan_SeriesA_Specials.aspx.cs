using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for EV_SeriesA_Specials.
	/// </summary>
	public class Velan_SeriesA_Specials : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.LinkButton BtnNext;
		protected System.Web.UI.WebControls.Label lblpage;
		protected System.Web.UI.WebControls.Label lblhidden;
		protected System.Web.UI.WebControls.LinkButton BtnBack;
		protected System.Web.UI.WebControls.Label lblfc;
		protected System.Web.UI.WebControls.RadioButtonList RBLFC;
		protected System.Web.UI.WebControls.Image Image4;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Panel PanelFC;
		protected System.Web.UI.WebControls.Label lblfr;
		protected System.Web.UI.WebControls.RadioButtonList RBLFR;
		protected System.Web.UI.WebControls.Image Image5;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Panel PanelFR;
		protected System.Web.UI.WebControls.Label lbltubing;
		protected System.Web.UI.WebControls.RadioButtonList RBLTubing;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Panel PanelTB;
		protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator2;
		protected System.Web.UI.WebControls.TextBox TxtClassification;
		protected System.Web.UI.WebControls.Label lblclassi;
		protected System.Web.UI.WebControls.Label LblSVDesc;
		protected System.Web.UI.WebControls.Image Image3;
		protected System.Web.UI.WebControls.DropDownList DDLSVType;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Panel PanelSV;
		protected System.Web.UI.WebControls.RadioButtonList RBLMO;
		protected System.Web.UI.WebControls.Image ImageMO;
		protected System.Web.UI.WebControls.Label Label18;
		protected System.Web.UI.WebControls.Panel Panel9;
		protected System.Web.UI.WebControls.RadioButtonList RBLLS;
		protected System.Web.UI.WebControls.RequiredFieldValidator RFVLS;
		protected System.Web.UI.WebControls.TextBox TxtLSDesc;
		protected System.Web.UI.WebControls.Label LblLSDesc;
		protected System.Web.UI.WebControls.Image Image1;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Panel Panel6;
		protected System.Web.UI.WebControls.RadioButtonList RBLMKit;
		protected System.Web.UI.WebControls.DropDownList DDLValveSize;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label LblMKDesc;
		protected System.Web.UI.WebControls.Image ImageMK;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Panel Panel2;
		protected System.Web.UI.WebControls.Label Label1;
		coder cd1=new coder();
		protected System.Web.UI.WebControls.Label lblstroke;
		DBClass db=new DBClass();
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["User"] !=null)
			{
				if(! IsPostBack)
				{
					lblstroke.Text="";
					PanelTB.Enabled =true;
					lblpage.Visible=false;
					lblhidden.Text="";
					lblclassi.Visible=false;
					TxtClassification.Visible=false;
					RBLMKit.SelectedIndex=2;
					RBLMKit_SelectedIndexChanged(sender,e);
					RBLLS.SelectedIndex=2;
					RBLLS_SelectedIndexChanged(sender,e);
					ListItem li=new ListItem();
					if(Session["Coder"] !=null)
					{
						cd1=new coder();
						cd1 =(coder)Session["Coder"];
						if(cd1.Bore_Size.Trim() !="")
						{
							if(cd1.Bore_Size.Trim() =="H" || cd1.Bore_Size.Trim() =="K" || cd1.Bore_Size.Trim() =="L" ||cd1.Bore_Size.Trim() =="M" ||
								cd1.Bore_Size.Trim() =="N" || cd1.Bore_Size.Trim() =="P")
							{
								DDLSVType.Items.Clear();
								li=new ListItem("No","No");
								DDLSVType.Items.Add(li);
								li=new ListItem("SV1","General Purpose 5 Way 2 pos solenoid valve with 12 VDC coil, 1/4 NPT ports, Cv=0.80. Energize coil to close valve");
								DDLSVType.Items.Add(li);
								li=new ListItem("SV2","General Purpose 5 Way 2 pos solenoid valve with 120 VDC coil, 1/4 NPT ports, Cv=0.80. Energize coil to close valve");
								DDLSVType.Items.Add(li);
								li=new ListItem("SV3","Asco General Purpose 4 Way 2 pos solenoid valve with 120 VDC coil, 1/4 NPT ports, Cv=0.70. Energize coil to close valve");
								DDLSVType.Items.Add(li);
								li=new ListItem("SV4","Stainless Steel Solenoid Valve");
								DDLSVType.Items.Add(li);
								li=new ListItem("SV5","Hazardous Area Solenoid Valve");
								DDLSVType.Items.Add(li);
							}
							else if(cd1.Bore_Size.Trim() =="R" || cd1.Bore_Size.Trim() =="S" || cd1.Bore_Size.Trim() =="T" ||cd1.Bore_Size.Trim() =="W" ||
								cd1.Bore_Size.Trim() =="X" || cd1.Bore_Size.Trim() =="A" || cd1.Bore_Size.Trim() =="Y" || cd1.Bore_Size.Trim() =="B" ||cd1.Bore_Size.Trim() =="F" ||
								cd1.Bore_Size.Trim() =="I" || cd1.Bore_Size.Trim() =="J")
							{
								DDLSVType.Items.Clear();
								li=new ListItem("No","No");
								DDLSVType.Items.Add(li);
								li=new ListItem("FSV1","General Purpose 5 Way 2 pos solenoid valve with 12 VDC coil, 3/8 NPT ports, Cv=2.00. Energize coil to close valve");
								DDLSVType.Items.Add(li);
								li=new ListItem("FSV2","General Purpose 5 Way 2 pos solenoid valve with 120 VDC coil, 3/8 NPT ports, Cv=2.00. Energize coil to close valve");
								DDLSVType.Items.Add(li);
								li=new ListItem("FSV3","Asco General Purpose 4 Way 2 pos solenoid valve with 120 VDC coil, 3/8 NPT ports, Cv=1.40. Energize coil to close valve");
								DDLSVType.Items.Add(li);
								li=new ListItem("FSV4","Stainless Steel Solenoid Valve");
								DDLSVType.Items.Add(li);
								li=new ListItem("FSV5","Hazardous Area Solenoid Valve");
								DDLSVType.Items.Add(li);
							}
							else
							{
								DDLSVType.Items.Clear();
								li=new ListItem("No","No");
								DDLSVType.Items.Add(li);
								li=new ListItem("FS1","General Purpose 3 Way 2 pos solenoid valve with 12 VDC coil. Energize coil to close valve");
								DDLSVType.Items.Add(li);
								li=new ListItem("FS2","General Purpose 3 Way 2 pos solenoid valve with 120 VAC coil. Energize coil to close valve");
								DDLSVType.Items.Add(li);
								li=new ListItem("FS3","Asco General Purpose 3 Way 2 pos solenoid valve with 120 VAC coil. Energize coil to close valve");
								DDLSVType.Items.Add(li);
								li=new ListItem("FS4","Stainless Steel Solenoid Valve");
								DDLSVType.Items.Add(li);
								li=new ListItem("FS5","Hazardous Area Solenoid Valve");
								DDLSVType.Items.Add(li);
							}
							DDLSVType.SelectedIndex=0;
							DDLSVType_SelectedIndexChanged(sender,e);
							ArrayList vlist=new ArrayList();
							vlist=db.SelectValues_List("ValveSize","WEB_Specials_Velan_Pricing_TableV1","BoreSize",cd1.Bore_Size.Trim());
							DDLValveSize.DataSource=vlist;
							DDLValveSize.DataBind();
							if(DDLValveSize.Items.Count >0)
							{
								DDLValveSize.SelectedIndex=0;
							}
						}
						if(cd1.ManualOverride !=null)
						{
							if(cd1.ManualOverride.Trim() !="")
							{
								RBLMO.SelectedValue=cd1.ManualOverride.Trim();
							}
						}
						if(cd1.LimitSwitch !=null)
						{
							if(cd1.LimitSwitch.Trim() !="")
							{
								RBLLS.SelectedValue=cd1.LimitSwitch.Trim();
								RBLLS_SelectedIndexChanged(sender,e);
								if(cd1.SpecialLimitSwitch.Trim() !="")
								{
									TxtLSDesc.Text=cd1.SpecialLimitSwitch.Trim();
								}
							}
						}
						if(cd1.MountKit !=null)
						{
							if(cd1.MountKit.Trim() !="")
							{
								RBLMKit.SelectedValue=cd1.MountKit.Trim();
								RBLMKit_SelectedIndexChanged(sender,e);
								DDLValveSize.SelectedValue=cd1.ValveSize.Trim();
							}
						}
						if(cd1.FilterRegulator !=null)
						{
							if(cd1.FilterRegulator.Trim() !="")
							{
								RBLFR.SelectedValue=cd1.FilterRegulator.Trim();
								RBLFR_SelectedIndexChanged(sender,e);
							}
						}
						if(cd1.Solenoid !=null)
						{
							if(cd1.Solenoid.Trim() !="")
							{
								DBClass db=new DBClass();
								li=new ListItem();
								li.Value=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.Solenoid.Trim());
								li.Text=cd1.Solenoid.Trim();
								DDLSVType.SelectedValue =li.Value;
								DDLSVType_SelectedIndexChanged(sender,e);
								TxtClassification.Text=cd1.SolenoidClass.ToString().Trim();			
							}
						}
						 
						if(cd1.FlowControl !=null)
						{
							if(cd1.FlowControl.Trim() !="")
							{
								RBLFC.SelectedValue=cd1.FlowControl.Trim();
								RBLFC_SelectedIndexChanged(sender,e);
							}
						}
						if(cd1.Tubing !=null)
						{
							if(cd1.Tubing.Trim() !="")
							{
								RBLTubing.SelectedValue=cd1.Tubing.Trim();
							}
						}
					}
				}
			}
			else
			{
				Response.Redirect("Login.aspx");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.DDLValveSize.SelectedIndexChanged += new System.EventHandler(this.DDLValveSize_SelectedIndexChanged);
			this.RBLMKit.SelectedIndexChanged += new System.EventHandler(this.RBLMKit_SelectedIndexChanged);
			this.RBLLS.SelectedIndexChanged += new System.EventHandler(this.RBLLS_SelectedIndexChanged);
			this.DDLSVType.SelectedIndexChanged += new System.EventHandler(this.DDLSVType_SelectedIndexChanged);
			this.RBLTubing.SelectedIndexChanged += new System.EventHandler(this.RBLTubing_SelectedIndexChanged);
			this.RBLFR.SelectedIndexChanged += new System.EventHandler(this.RBLFR_SelectedIndexChanged);
			this.RBLFC.SelectedIndexChanged += new System.EventHandler(this.RBLFC_SelectedIndexChanged);
			this.BtnBack.Click += new System.EventHandler(this.BtnBack_Click);
			this.BtnNext.Click += new System.EventHandler(this.BtnNext_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void RBLMKit_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(RBLMKit.SelectedIndex !=-1)
			{
				lblstroke.Text ="";
				if(RBLMKit.SelectedIndex==0)
				{
					cd1=new coder();
					cd1 =(coder)Session["Coder"];
					if(DDLValveSize.Items.Count >0)
					{
						lblstroke.Text ="Stroke is modified to "+(Convert.ToDecimal(DDLValveSize.SelectedItem.Text.Trim())+1)+"\"";
					}
					LblMKDesc.Text="Mounting Kit for standard knife-gate includes mounting post 316 stainless steel coupling manufactured from valve stem.";
					ImageMK.ImageUrl="specials/kgt.gif";	
					DDLValveSize.Enabled=true;
				}
				if(RBLMKit.SelectedIndex==1)
				{
					cd1=new coder();
					cd1 =(coder)Session["Coder"];
					if(DDLValveSize.Items.Count >0)
					{
						lblstroke.Text ="Stroke is modified to "+(Convert.ToDecimal(DDLValveSize.SelectedItem.Text.Trim())+1)+"\"";
					}
					LblMKDesc.Text="Mounting Kit for standard bonnet type includes mounting post 316 stainless steel coupling manufactured from valve stem.";
					ImageMK.ImageUrl="specials/bt.gif";	
					DDLValveSize.Enabled=true;
				}
				if(RBLMKit.SelectedIndex==2)
				{
					LblMKDesc.Text="";
					ImageMK.ImageUrl="specials/n.gif";	
					DDLValveSize.Enabled=false;
				}
			}
		}

		private void DDLSVType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(DDLSVType.SelectedIndex >0)
			{
				LblSVDesc.Text=DDLSVType.SelectedItem.Value.Trim();
				PanelTB.Enabled=true;
				PanelFC.Enabled=true;
				PanelFR.Enabled=true;
				if(DDLSVType.SelectedIndex ==5)
				{
					lblclassi.Visible=true;
					TxtClassification.Visible=true;
					TxtClassification.Text="";
					RequiredFieldValidator2.Visible=true;
				}
				else
				{
					RequiredFieldValidator2.Visible=false;
					lblclassi.Visible=false;
					TxtClassification.Visible=false;
					TxtClassification.Text="";
					if(DDLSVType.SelectedIndex == 0 && RBLFR.SelectedIndex == 2 &&  RBLFC.SelectedIndex ==2 )
					{
						PanelTB.Enabled=false;
					}
					else
					{
						PanelTB.Enabled=true;
					}
				}
			}
			else
			{
				LblSVDesc.Text="";
				RequiredFieldValidator2.Visible=false;
				lblclassi.Visible=false;
				TxtClassification.Visible=false;
				TxtClassification.Text="";
				PanelFC.Enabled=false;
				RBLFC.SelectedIndex=2;
				PanelFR.Enabled=false;
				RBLFR.SelectedIndex=2;
				if(DDLSVType.SelectedIndex == 0 && RBLFR.SelectedIndex == 2 &&  RBLFC.SelectedIndex ==2 )
				{
					PanelTB.Enabled=false;
				}
				else
				{
					PanelTB.Enabled=true;
				}
			}
		}

		private void RBLFC_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lblfr.Visible=false;
			if(RBLFC.SelectedIndex  ==0 )
			{
				PanelTB.Enabled=true;
				if(RBLTubing.SelectedIndex ==1 || RBLTubing.SelectedIndex ==2 || RBLFR.SelectedIndex ==1 )
				{
					RBLFC.SelectedIndex=1;
					lblfc.Visible=true;
				}
				else
				{
					lblfc.Visible=false;
				}
			}
			else if(RBLFC.SelectedIndex  ==1)
			{
				PanelTB.Enabled=true;
				lblfc.Visible=false;
				if(RBLTubing.SelectedIndex ==0)
				{
					RBLTubing.SelectedIndex=1;
				} 
				if(RBLFR.SelectedIndex ==0)
				{
					RBLFR.SelectedIndex=1;
				}  
			}
			else if(DDLSVType.SelectedIndex == 0 && RBLFR.SelectedIndex == 2 &&  RBLFC.SelectedIndex ==2)
			{
				PanelTB.Enabled=false;
				lblfc.Visible=false;
			}
			else
			{
				PanelTB.Enabled=true;
				lblfc.Visible=false;
			}
		}

		private void RBLFR_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lblfc.Visible=false;
			if(RBLFR.SelectedIndex  ==0 )
			{
				PanelTB.Enabled=true;
				if(RBLTubing.SelectedIndex ==1 || RBLTubing.SelectedIndex ==2 || RBLFC.SelectedIndex ==1 )
				{
					RBLFR.SelectedIndex=1;
					lblfr.Visible=true;
				}
				else
				{
					lblfr.Visible=false;
				}
			}
			else if(RBLFR.SelectedIndex  ==1)
			{
				PanelTB.Enabled=true;
				lblfr.Visible=false;
				if(RBLTubing.SelectedIndex ==0)
				{
					RBLTubing.SelectedIndex=1;
				} 
				if(RBLFC.SelectedIndex ==0)
				{
					RBLFC.SelectedIndex=1;
				}  
			}
			else if(DDLSVType.SelectedIndex == 0 && RBLFR.SelectedIndex == 2 &&  RBLFC.SelectedIndex ==2 )
			{
				PanelTB.Enabled=false;
				lblfr.Visible=false;
			}
			else
			{
				PanelTB.Enabled=true;
				lblfr.Visible=false;
			}
		}
		private void RBLTubing_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(RBLTubing.SelectedIndex  ==0)
			{
				if(RBLFR.SelectedIndex ==1 || RBLFR.SelectedIndex ==1 )
				{
					RBLTubing.SelectedIndex=1;
					lbltubing.Visible=true;
				}
				else
				{
					lbltubing.Visible=false;
				}
			}
			else if(RBLTubing.SelectedIndex  ==1 || RBLTubing.SelectedIndex  ==2)
			{
				if(RBLFR.SelectedIndex ==0)
				{
					RBLFR.SelectedIndex=1;
				} 
				if(RBLFC.SelectedIndex ==0)
				{
					RBLFC.SelectedIndex=1;
				}  
			}
			else
			{
				lbltubing.Visible=false;
			}
		}

		private void RBLLS_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(RBLLS.SelectedIndex ==0)
			{
				LblLSDesc.Text="Limit Switch, Mechanical type SPDT, Nema 4 for Open & Close Indication";
				TxtLSDesc.Text="";
				TxtLSDesc.Visible=false;
				RFVLS.Enabled=false;
			}
			else if(RBLLS.SelectedIndex ==1)
			{
				LblLSDesc.Text="Please Specify Special Limit Switch Specification";
				TxtLSDesc.Text="";
				TxtLSDesc.Visible=true;
				RFVLS.Enabled=true;
			}
			else if(RBLLS.SelectedIndex ==2)
			{
				LblLSDesc.Text=" No Limit Switch";
				TxtLSDesc.Text="";
				TxtLSDesc.Visible=false;
				RFVLS.Enabled=false;
			}
		}


		private void BtnNext_Click(object sender, System.EventArgs e)
		{
			if(Page.IsValid)
			{
				if(Session["Coder"] !=null)
				{
					cd1=new coder();
					cd1 =(coder)Session["Coder"];
					Session["Coder"] =null;
					if(RBLMKit.SelectedIndex >-1 && RBLMKit.SelectedIndex <2)
					{
						cd1.MountKit=RBLMKit.SelectedItem.Value.Trim();
						if(DDLValveSize.Items.Count >0)
						{
							cd1.ValveSize=DDLValveSize.SelectedItem.Text.Trim();
							if(RBLMKit.SelectedIndex ==0)
							{
								cd1.Stroke=Convert.ToString((Convert.ToDecimal(cd1.ValveSize.Trim()) + 1 ));
							}
							else if(RBLMKit.SelectedIndex ==1)
							{
								cd1.Stroke=Convert.ToString((Convert.ToDecimal(cd1.ValveSize.Trim()) + 1 ));
							}
						}
						else
						{
							cd1.ValveSize="TBA";
						}
						cd1.Specials="SPECIAL";
					}
					else
					{
						cd1.MountKit="";
						cd1.ValveSize="TBA";
					}
					if(RBLLS.SelectedIndex ==0)
					{
						cd1.LimitSwitch=RBLLS.SelectedItem.Value.Trim();
						cd1.Specials="SPECIAL";
						cd1.SpecialLimitSwitch="";
					}
					else if(RBLLS.SelectedIndex ==1)
					{
						cd1.LimitSwitch=RBLLS.SelectedItem.Value.Trim();
						cd1.Specials="SPECIAL";
						cd1.SpecialLimitSwitch=TxtLSDesc.Text.Trim();
					}
					else
					{
						cd1.LimitSwitch="";
						cd1.SpecialLimitSwitch="";
					}
					if(RBLMO.SelectedIndex ==0)
					{
						cd1.ManualOverride=RBLMO.SelectedItem.Value.ToString().Trim();
						cd1.Specials="SPECIAL";
						cd1.SecRodDiameter="D"+cd1.Rod_Diamtr.Trim()+"2";
						cd1.SecRodEnd="RA4";
						cd1.DoubleRod="Yes";
					}
					else
					{
						cd1.ManualOverride="";
						cd1.SecRodDiameter="";
						cd1.SecRodEnd="";
						cd1.DoubleRod="No";
					}
					if(DDLSVType.SelectedIndex >0)
					{
						cd1.Solenoid=DDLSVType.SelectedItem.Text.Trim();
						cd1.SolenoidClass=TxtClassification.Text.Trim();
						cd1.Specials="SPECIAL";
					}
					else 
					{
						cd1.SolenoidClass="";
						cd1.Solenoid="";
					}
					if(RBLFC.SelectedIndex == 0 || RBLFC.SelectedIndex == 1)
					{
						cd1.FlowControl=RBLFC.SelectedItem.Value.ToString().Trim();
						cd1.Specials="SPECIAL";
					}
					else
					{
						cd1.FlowControl="";
					}
					if(RBLFR.SelectedIndex == 0 || RBLFR.SelectedIndex == 1)
					{
						cd1.FilterRegulator=RBLFR.SelectedItem.Value.ToString().Trim();
						cd1.Specials="SPECIAL";
					}
					else
					{
						cd1.FilterRegulator="";
					}
					if(PanelTB.Enabled ==true)
					{
						if(RBLTubing.SelectedIndex >-1)
						{
							cd1.Tubing=RBLTubing.SelectedItem.Value.ToString().Trim();
							cd1.Specials="SPECIAL";
						}
						else
						{
							cd1.Tubing="";
						}
					}
					else
					{
						cd1.Tubing="";
					}
					cd1.CompanyID="1004";
					Session["Coder"] =cd1;
					Response.Redirect("Icylinder_Page6.aspx");
				}
				else
				{
					lblpage.Visible=true;
				}
			}
		}
		private void BtnBack_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Velan_SeriesA.aspx");
		}

		private void DDLValveSize_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(DDLValveSize.SelectedIndex >-1)
			{
				cd1=new coder();
				cd1 =(coder)Session["Coder"];
				if(DDLValveSize.Items.Count >0)
				{
					lblstroke.Text ="Stroke is modified to "+(Convert.ToDecimal(DDLValveSize.SelectedItem.Text.Trim())+1)+"\"";
				}
			}
		}


	}
}
