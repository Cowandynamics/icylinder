using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
using System.Text;
namespace iCylinderV1
{
	/// <summary>
	/// Summary description for Velan_BachQuote.
	/// </summary>
	public class Velan_BachQuote : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblpage;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.HtmlControls.HtmlInputFile FileUpload;
		protected System.Web.UI.WebControls.Label LblView;
		protected System.Web.UI.WebControls.LinkButton BtnQuoteGenerate;
		protected System.Web.UI.WebControls.LinkButton LBUpload;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label LblUpload;
		protected System.Web.UI.WebControls.DropDownList DDLPriority;
		protected System.Web.UI.WebControls.Label Label58;
		protected System.Web.UI.WebControls.Panel PSend;
		protected System.Web.UI.WebControls.Label lblup;
		protected System.Web.UI.WebControls.Label lbltable;
		protected System.Web.UI.WebControls.Label lbltotal;
		protected System.Web.UI.WebControls.Label lblmgrp;
		protected System.Web.UI.WebControls.Label lblstatus;
		protected System.Web.UI.WebControls.Label LblPartNo;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label57;
		protected System.Web.UI.WebControls.TextBox TxtSpecialReq;
		protected System.Web.UI.WebControls.LinkButton LBSend;
		protected System.Web.UI.WebControls.Label lblerr;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.HyperLink HyperLink1;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.TextBox TxtProjectNo;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.HtmlControls.HtmlInputFile FileUploadSpec;
		protected System.Web.UI.WebControls.Label lblspecsheet;
		protected System.Web.UI.WebControls.Panel Panel1;
		protected System.Web.UI.WebControls.Panel Panel2;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label1;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			LblView.Text="";
			if(! IsPostBack)
			{
				LblUpload.Text="";
				//url
				//issue #234 start
				ArrayList lst =new ArrayList();
				lst =(ArrayList)Session["User"];
				string strCompanyID = "";
				if(lst[6]!=null) strCompanyID=lst[6].ToString();
				if(strCompanyID=="1009" || strCompanyID=="1011" || strCompanyID=="996") HyperLink1.NavigateUrl = @"crtfiles/template_Rotork.xls";
				//issue #224 start
				else if(strCompanyID=="1017" || strCompanyID=="995") HyperLink1.NavigateUrl = @"crtfiles/template_Sistag.xls";
				//issue #224 end
				else HyperLink1.NavigateUrl = @"crtfiles/template.xls";
				//issue #234 end
				StringBuilder disableButton = new StringBuilder();
				disableButton.Append("if (typeof(Page_ClientValidate) == 'function') { ");
				disableButton.Append("if (Page_ClientValidate() == false) { return false; }} ");
				disableButton.Append("this.value = 'Please Wait...';");
				disableButton.Append("this.disabled = true;");
				disableButton.Append(this.Page.GetPostBackEventReference(BtnQuoteGenerate));
				disableButton.Append(";"); 
				BtnQuoteGenerate.Attributes.Add("onclick", disableButton.ToString());
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.LBUpload.Click += new System.EventHandler(this.LBUpload_Click);
			this.BtnQuoteGenerate.Click += new System.EventHandler(this.BtnQuoteGenerate_Click);
			this.LBSend.Click += new System.EventHandler(this.LBSend_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		private string UploadFileTemp_Customer(object Sender,EventArgs E)
		{
			string file="";
			if (FileUpload.PostedFile !=null) //Checking for valid file
			{	
				string name=DateTime.Today.ToShortDateString().Replace("/","").Replace(" ","");
						name += DateTime.Now.Ticks.ToString();//.Replace("/","").Replace(" ","").Replace(":","").Replace("","");
				string StrFileName = name+".xls" ;
				int IntFileSize =FileUpload.PostedFile.ContentLength;
				if (IntFileSize <=0)
				{
					lblpage.Text="Uploading of file " + StrFileName + " failed ";
				}
				else
				{
					FileUpload.PostedFile.SaveAs(Server.MapPath("./dbfile/" + StrFileName.Trim()));
					file=StrFileName.ToString();
				}
			}
			return file;
		}
		private void LBUpload_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(FileUpload.Value.Trim() !="")
				{
					lblup.Text="";
					lblerr.Text="";
					if(FileUpload.PostedFile.ContentType.ToString() =="application/vnd.ms-excel")
					{
						lblpage.Text="";
						string upload="";
						if(FileUpload.Value.Trim()!="")
						{
							upload=UploadFileTemp_Customer(sender,e);
							LblUpload.Text=FileUpload.PostedFile.FileName.ToString();
						}
						if(upload.Trim() !="")
						{
							lblup.Text=upload.Trim();
							//validation
							//issue #242 start
							ArrayList lst =new ArrayList();
							lst =(ArrayList)Session["User"];
							string strCompanyID = "";
							if(lst[6]!=null) strCompanyID=lst[6].ToString();
							if(strCompanyID=="1017" || strCompanyID=="995")
							{
								lblpage.Text="File uploaded successsfully <a href='Validate_UploadFiles_All.aspx?file="+upload.Trim()+"' target='_blank'>Click here to View</a>";
								LblView.Text="<script language='javascript'>window.open('Validate_UploadFiles_All.aspx?file="+upload.Trim()+"','_blank','toolbar=no,width=1000,height=600,resizable=yes,top=0,left=0')</script>";	
							}
							else
							{
								//back
								lblpage.Text="File uploaded successsfully <a href='ViewUploadedFile.aspx?file="+upload.Trim()+"' target='_blank'>Click here to View</a>";
								LblView.Text="<script language='javascript'>window.open('ViewUploadedFile.aspx?file="+upload.Trim()+"','_blank','toolbar=no,width=1000,height=600,resizable=yes,top=0,left=0')</script>";						
							}
							//end #242 end
							DBClass db=new DBClass();
							ArrayList qlist=new ArrayList();
							//fetching
							//issue #242 start
							if(strCompanyID=="1017" || strCompanyID=="995") qlist=db.Select_DataFrom_ImportFile_All(Request.PhysicalApplicationPath+"dbfile/"+lblup.Text.Trim());
							else 
							//back
							//qlist=db.Select_DataFrom_ImportFile(Request.PhysicalApplicationPath+"dbfile/"+lblup.Text.Trim());
							//update
							qlist=db.Select_DataFrom_ImportFile(Request.PhysicalApplicationPath+"dbfile/"+lblup.Text.Trim());
							//issue #242 end
							if(qlist.Count >0)
							{
								bool good=true;
								for(int i=0;i<qlist.Count;i++)
								{
									VelanClass cd1=new VelanClass();
									cd1=(VelanClass)qlist[i];
									if(cd1 !=null)
									{
//										if(cd1.VelanTag.Trim() == "")
//										{
//											good =false;
//										}
										if(IsNumeric(cd1.Stroke.Trim()) != true)
										{
											good =false;
										}
										if(IsNumeric(cd1.MinAirSupply.Trim()) != true)
										{
											good =false;
										}
										if(IsNumeric(cd1.ETC.Trim()) != true) //seating thrust
										{
											good =false;
										}
										if(IsNumeric(cd1.RTC.Trim()) != true) //packing friction
										{
											good =false;
										}
										if(cd1.Mount.ToString().Trim() =="")
										{
											good =false;
										}
										if(db.SelectValue("Seal_Code","WEB_Seal_TableV1","Seal_Type",cd1.Seals.ToString().Trim()).Trim() =="")
										{
											good =false;
										}
										if(cd1.Paint.ToString().Trim() =="")
										{
											good =false;
										}
										if(cd1.Style.ToString().Trim() =="")
										{
											good =false;
										}
										if(cd1.PneumaticP.ToString().Trim() =="")
										{
											good =false;
										}
										if(cd1.LimitSwitch.ToString().Trim() =="")
										{
											good =false;
										}
										if(cd1.Ratings.ToString().Trim() =="")
										{
											good =false;
										}
										if(cd1.ManualOverride.ToString().Trim() =="")
										{
											good =false;
										}
										if(cd1.Ratings.ToString().Trim() =="" )
										{
											good =false;
										}
									}
								}
								if(good ==false)
								{
									//validation
									if(strCompanyID=="1017" || strCompanyID=="995")
									{
										lblerr.Text="Error in excel file Please <a href='Validate_UploadFiles_All.aspx?file="+upload.Trim()+"' target='_blank'>Click here to View</a> the error in uploaded file.";
									}
									else
									{
									//back
									lblerr.Text="Error in excel file Please <a href='ViewUploadedFile.aspx?file="+upload.Trim()+"' target='_blank'>Click here to View</a> the error in uploaded file.";
									}
									//end #242 end
									lblup.Text="";
									lblpage.Text="";
								}
							}
//							else 
//							{
//								lblerr.Text="Error in excel file Please <a href='ViewUploadedFile.aspx?file="+upload.Trim()+"' target='_blank'>Click here to View</a> the error in uploaded file.";
//								lblup.Text="";
//								lblpage.Text="";
//							}
						}
					}
					else
					{
						lblpage.Text="Please select a excel file!!!";
					}
				}
				else
				{
					lblpage.Text="Please select a excel file!!!";
				}
			}
			catch(Exception ex)
			{
				string ee=ex.Message;
				lblup.Text="";
				LblUpload.Text="";
				lblpage.Text="";
				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Not a valid import file')</script>";
			}
		}
		public string Qno(string use)
		{
			try
			{
				DBClass db=new DBClass();
				string qno ="";
				string count = "";
				string usr=use.ToUpper();
				string st= usr.Substring(0,1);
				string s1=DateTime.Today.Month.ToString();
				count=db.SelectValue("QuoteNo","WEB_QuoteCount_TableV1","SLNo","1");
				if(count.Length !=0)
				{
					int lst=Convert.ToInt32(count.Substring(5));
				
				
					string s=DateTime.Today.Month.ToString();
					if(s.Length ==1)
					{
						s="0"+DateTime.Today.Month.ToString();
					}
					else
					{
						s=DateTime.Today.Month.ToString();
					}
					if(count !="")
					{
						if(count.Substring(1,2).Equals(DateTime.Today.Year.ToString().Substring(2)))
						{
							if(count.Substring(3,2).Equals(s))
							{
								lst++;
								string num="";
								if(lst.ToString().Length ==4)
								{
									num=lst.ToString();
								}
								else if(lst.ToString().Length ==3)
								{
									num="0"+lst.ToString();
								}
								else if(lst.ToString().Length ==2)
								{
									num="00"+lst.ToString();
								}
								else 
								{
									num="000"+lst.ToString();
								}
							
								if(s1.Length ==1)
								{
									s1="0"+DateTime.Today.Month.ToString();
								}
								else
								{
									s1=DateTime.Today.Month.ToString();
								}
								qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+num.ToString();
							}
							else
							{
						
								if(s1.Length ==1)
								{
									s1="0"+DateTime.Today.Month.ToString();
								}
								else
								{
									s1=DateTime.Today.Month.ToString();
								}
								qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+"0001";
							}
	
						}
						else
						{
						
							if(s1.Length ==1)
							{
								s1="0"+DateTime.Today.Month.ToString();
							}
							else
							{
								s1=DateTime.Today.Month.ToString();
							}
							qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+"0001";
						}
					}
					else
					{
					
						if(s1.Length ==1)
						{
							s1="0"+DateTime.Today.Month.ToString();
						}
						else
						{
							s1=DateTime.Today.Month.ToString();
						}
						qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+"0001";
					}
				}
				else
				{
					qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+"0001";
				}
				string sav=db.InsertQuoteNo(qno);
				db.InsertQuoteCount(qno);
				return qno;
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				return null;
			}		
		}
		
		
		public void APricing(coder PN)
		{
			try
			{
				DBClass db=new DBClass();
				string commn="";
				string ind="0";
				ArrayList lst =new ArrayList();
				lst =(ArrayList)Session["User"];
				ind=db.SelectPriceIndex("VA").ToString();
				commn="WEB_SeriesA_Velan_CommAdders_TableV1";
				lblmgrp.Text="AP_Base";
				lbltable.Text="WEB_APrice_Velan_TableV1";
				if(PN.DoubleRod.ToString().Trim() =="Yes")
				{
					lblmgrp.Text="AP_Base";
					lbltable.Text="WEB_ADPrice_Velan_TableV1";
				}				
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal seal=0.00m;

				st =Convert.ToDecimal(PN.Stroke);
				sp =db.SelectStrokeA(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				sp =Decimal.Round(sp,2);
				
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(ind.Trim());
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
                //issue #315 start
				//lblD.Text=db.SelectDiscount(lst[1].ToString().Trim(),"A");
				discount=Convert.ToDecimal(db.SelectDiscount(lst[1].ToString().Trim(),"A"));
				sprice =temp*(1 - (discount/100));
				//issue #315 end
				sprice =Decimal.Round(sprice,2);
//				lblstroke.Text=sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="true";
				}
				//mount price
				decimal mp =0.00m;
				string mgrp = lblmgrp.Text.ToString();
				mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100));
				mprice=Decimal.Round(mp,2); 
				if(PN.Mount.Trim() =="NX1" ||PN.Mount.Trim() =="NX3")
				{
					mprice=mprice + 5.00m;
				}
				if(PN.Mount.Trim().Substring(0,1) =="I" || PN.Mount.Trim().Substring(0,1) =="M" )
				{
					decimal a1=0.00m;
					a1 =db.SelectISSMSSPrice(PN.Mount.Trim());
					if(a1 >0)
					{
						a1 =a1 + a1 * (index /100);
						a1= a1*(1 - (discount/100)); 
						a1 =Decimal.Round(a1,2);
						mprice +=a1;
					}
				}
				else if(PN.Mount.Trim().Substring(0,2) =="GR" )
				{
					decimal a1=0.00m;
					a1 =db.SelectGRPrice(PN.Mount.Trim());
					if(a1 >0)
					{
						a1 =a1 + a1 * (index /100);
						a1= a1*(1 - (discount/100)); 
						a1 =Decimal.Round(a1,2);
						mprice +=a1;
					}
				}
				if(mprice ==0)
				{
					lblstatus.Text ="true";
				}

				if(PN.Seal_Comp.ToString().Trim() =="L")
				{
					seal =db.SelectAddersPrice("SL",commn.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="true";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="F")
				{
					seal =db.SelectAddersPrice("SF",commn.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="true";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="E")
				{
					seal =db.SelectAddersPrice("SE",commn.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="true";
					}
				}
				seal=Decimal.Round(seal,2);

				decimal rodend=0.00m;
				if(PN.Rod_End.ToString().Substring(0,1) !="N" &&PN.Rod_End.ToString().Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd",commn.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="true";
					}
				}
				rodend=Decimal.Round(rodend,2);
				seal =seal + seal * (index /100);
				rodend =rodend + rodend * (index /100);
				//issue #315 start
				seal = seal*(1 - (discount/100));
				seal = Decimal.Round(seal,2);
				rodend = rodend*(1 - (discount/100));
				rodend = Decimal.Round(rodend,2);
				//issue #315 end
				decimal total=0.00m;
				total=sprice + mprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}

		public static bool IsNumeric(string strInteger) 
		{
			try 
			{
				if(strInteger.Trim() =="")
				{
					return false;
				}
				else
				{
					int intTemp =0;
					if(strInteger.ToString().StartsWith(".") == true)
					{
						for(int i=1; i< strInteger.Length;i++)
						{
							intTemp = Int32.Parse( strInteger.Substring(i,1) );
						}
					}
					else
					{
						for(int i=0; i< strInteger.Length;i++)
						{
							if (strInteger.ToString().Substring(i,1) !=".")
							{
								intTemp = Int32.Parse( strInteger.Substring(i,1) );
							}
						}
					}
					return true;
				}				
			} 
			catch (FormatException) 
			{
				return false;
			}    
		}
		private string UploadFileSpec(object Sender,EventArgs E)
		{
			string file="";
			if (FileUploadSpec.PostedFile !=null) //Checking for valid file
			{	
				string name=DateTime.Today.ToShortDateString().Replace("/","").Replace(" ","");
				name += DateTime.Now.Ticks.ToString();//.Replace("/","").Replace(" ","").Replace(":","").Replace("","");
				string StrFileName = name+".pdf" ;
				int IntFileSize =FileUploadSpec.PostedFile.ContentLength;
				if (IntFileSize <=0)
				{
					lblpage.Text="Uploading of file " + StrFileName + " failed ";
				}
				else
				{
					FileUploadSpec.PostedFile.SaveAs(Server.MapPath("./specsheet/" + StrFileName.Trim()));
					file=StrFileName.ToString();
				}
			}
			return file;
		}
		//asseriespn start
		//issue #137 start
		public decimal MLPricing(coder PN)
		{
			decimal total=0.00m;
			try
			{				
				if(PN.Style.ToString().Trim()=="N")
				{
					DBClass db=new DBClass();	
					string pindex="0";
					//pindex=db.SelectPriceIndex("ML").ToString();
					string comtable="WEB_SeriesML_CommAdders_TableV1";					
					lblmgrp.Text="MLSMount_Grp";
					lbltable.Text="WEB_MLPrice_Master_TableV1";
				
					decimal strokeprice =0.00m;
					decimal stroke =0.00m;
					decimal mountprice =0.00m;
					decimal cushionprice =0.00m;
					decimal sealprice=0.00m;
					decimal temp=0;
					decimal index =0.00m;
					
//					if(PN.Bore_Size.ToString().Trim()=="C" || PN.Bore_Size.ToString().Trim()=="D" || PN.Bore_Size.ToString().Trim()=="E")
//					{			
//						index =Convert.ToDecimal(db.SelectPriceIndex("MLS").ToString());
//					}
//					else
//					{
//						index =Convert.ToDecimal(db.SelectPriceIndex("ML").ToString());
//					}

					decimal discount =0.00m;
					decimal qty=0;
					qty=Convert.ToDecimal(PN.Qty.ToString());
//					if(qty >0 && qty <=2)
//					{
//						discount=35;
//					}
//					else if(qty >=3 && qty <=5)
//					{
//						discount=37;
//					}
//					else if(qty >=6 && qty <=10)
//					{
//						discount=40;
//					}
//					else if(qty >10)
//					{
//						discount=45;
//					}

					//stroke price 
					stroke =Convert.ToDecimal(PN.Stroke.ToString().Trim());
					strokeprice =db.SelectStrokePriceML(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					strokeprice =Decimal.Round(strokeprice,2);				
					temp =stroke * strokeprice;			
					temp =temp + temp * (index /100);
					temp =temp *(1 - (discount/100)); 
					strokeprice =Decimal.Round(temp,2);
					if(strokeprice ==0)
					{
						lblstatus.Text ="true";
					}

					//mount price		
					temp=0;
					string mgrp ="";
					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
					temp =db.SelectMountPriceML(mgrp.ToString(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					temp =temp + temp * (index /100);
					temp =temp *(1 - (discount/100)); 
					mountprice=Decimal.Round(temp,2); 				
					if(mountprice ==0)
					{
						lblstatus.Text ="true";
					}

					//cushion price
					temp=0;					
					temp =db.SelectCushionPriceML(lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());						
					temp =temp + temp * (index /100);	
					if(PN.Cushions.ToString() =="5")
					{
						temp =temp * 2;
						temp = temp * (1 - (discount/100));		
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}
					else if(PN.Cushions.ToString() =="8")
					{
						temp=0;
					}
					else
					{
						temp = temp*(1 - (discount/100)); 	
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}			
					cushionprice =Decimal.Round(temp,2);					

					//seal price
					temp=0;
					if(PN.Seal_Comp.ToString().Trim() =="L")
					{
						temp =db.SelectAddersPrice("SL",comtable.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());		
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}
					else if(PN.Seal_Comp.ToString().Trim() =="F")
					{
						temp =db.SelectAddersPrice("SF",comtable.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
						if(temp ==0)
						{
							lblstatus.Text ="true";
						}
					}
					temp =temp + temp * (index /100);	
					sealprice=Decimal.Round(temp,2);	
					
					total=strokeprice + mountprice + cushionprice + sealprice;
					lbltotal.Text=total.ToString();
				}
				else
				{
					lbltotal.Text="0.00";
				}
				return total;
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				return total;
			}
		}
		public void ASPricing(coder PN)
		{
			try
			{
				DBClass db=new DBClass();
				string commn="";
				string ind="0";
				ArrayList lst =new ArrayList();
				lst =(ArrayList)Session["User"];
				//ind=db.SelectPriceIndex("AS").ToString();
				//commn="WEB_SeriesA_Velan_CommAdders_TableV1";
				commn="SeriesAS_OEM_CommAdders_TableV1";
				lblmgrp.Text="ASP_Base";
				lbltable.Text="ASPrice_Master_TableV1";
				if(PN.TandemDuplex.Trim() =="TC")
				{
					lblmgrp.Text="ASP_Base";
					lbltable.Text="ASPrice_Tandem_TableV1";
				}				
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal seal=0.00m;

				st =Convert.ToDecimal(PN.Stroke);
				sp =db.SelectStrokeA(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				sp =db.SelectOneValueByAllinfo("ASP_StrokePerInch",lbltable.Text.Trim(),"Bore_Size",PN.Bore_Size.ToString().Trim(),"Rod_Size",PN.Rod_Diamtr.ToString().Trim());
				sp =Decimal.Round(sp,2);
				
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(ind.Trim());
				temp =temp + temp * (index /100);
				decimal discount =0.00m;

				//				lblD.Text=db.SelectDiscount(lst[1].ToString().Trim(),"A");
				//				discount=Convert.ToDecimal(lblD.Text);
				//				sprice =temp*(1 - (discount/100));
				sprice =Decimal.Round(temp,2);
				//				lblstroke.Text=sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="true";
				}
				//mount price
				decimal mp =0.00m;
				string mgrp = lblmgrp.Text.ToString();
				mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				mp =mp + mp * (index /100);
				//				mp= mp*(1 - (discount/100));
				mprice=Decimal.Round(mp,2); 
				if(PN.Mount.Trim() =="NX1" ||PN.Mount.Trim() =="NX3")
				{
					mprice=mprice + 5.00m;
				}
				if(PN.Mount.Trim().Substring(0,1) =="I" || PN.Mount.Trim().Substring(0,1) =="M" )
				{
					decimal a1=0.00m;
					a1 =Convert.ToDecimal(db.SelectValue("Price","ASprice_IsoMssAdder_TableV1","Mount",PN.Mount.Trim()));
					if(a1 >0)
					{
						a1 =a1 + a1 * (index /100);
						a1= a1*(1 - (discount/100)); 
						a1 =Decimal.Round(a1,2);
						mprice +=a1;
					}

					
				}
				else if(PN.Mount.Trim().Substring(0,2) =="GR" )
				{
					decimal a1=0.00m;
					a1 =db.SelectGRPrice(PN.Mount.Trim());
					if(a1 >0)
					{
						a1 =a1 + a1 * (index /100);
						a1= a1*(1 - (discount/100)); 
						a1 =Decimal.Round(a1,2);
						mprice +=a1;
					}
				}
				if(mprice ==0)
				{
					lblstatus.Text ="true";
				}

				if(PN.Seal_Comp.ToString().Trim() =="L")
				{
					seal =db.SelectAddersPrice("SL",commn.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					
					if(lbltable.Text.Trim() =="ASPrice_Tandem_TableV1")
					{
						seal =db.SelectOneValueByAllinfo("TSL",commn.Trim(),"BoreSize",PN.Bore_Size.ToString().Trim(),"RodSize",PN.Rod_Diamtr.ToString().Trim());
					}
					else if(lbltable.Text.Trim() =="ASDPrice_Master_TableV1")
					{
						seal =db.SelectOneValueByAllinfo("TDSL",commn.Trim(),"BoreSize",PN.Bore_Size.ToString().Trim(),"RodSize",PN.Rod_Diamtr.ToString().Trim());
					}
					else
					{
						seal =db.SelectOneValueByAllinfo("SL",commn.Trim(),"BoreSize",PN.Bore_Size.ToString().Trim(),"RodSize",PN.Rod_Diamtr.ToString().Trim());
					}
					if(seal ==0)
					{
						lblstatus.Text ="true";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="F")
				{
					seal =db.SelectAddersPrice("SF",commn.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					//split based on tandem
					if(lbltable.Text.Trim() =="ASPrice_Tandem_TableV1")
					{
						seal =db.SelectOneValueByAllinfo("TSF",commn.Trim(),"BoreSize",PN.Bore_Size.ToString().Trim(),"RodSize",PN.Rod_Diamtr.ToString().Trim());
					}
					else
					{
                        seal =db.SelectOneValueByAllinfo("SF",commn.Trim(),"BoreSize",PN.Bore_Size.ToString().Trim(),"RodSize",PN.Rod_Diamtr.ToString().Trim());
					}
					if(seal ==0)
					{
						lblstatus.Text ="true";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="E")
				{
					seal =db.SelectAddersPrice("SE",commn.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="true";
					}
				}
				seal=Decimal.Round(seal,2);

				decimal rodend=0.00m;
				if(PN.Rod_End.ToString().Substring(0,1) !="N" &&PN.Rod_End.ToString().Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd",commn.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="true";
					}
				}
				rodend=Decimal.Round(rodend,2);
				seal =seal + seal * (index /100);
				rodend =rodend + rodend * (index /100);
				//canisterprice
				//				decimal dcCanisterPrice = Convert.ToDecimal(db.SelectOneValueByAllinfo("CanPrice","AS_PRELOAD_SPACER",
				//                                                                     "CanisterNo",PN.CanisterNo,
				//					                                                 "Preload",PN.Preload).ToString());
				decimal total=0.00m;
				total=sprice + mprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}

		private coder MLSeries_PN(VelanClass cod )
		{
            coder PN = new coder();
			DBClass db=new DBClass();
			PN.Series="ML";
			PN.Port_Type="S";
			PN.Rod_End ="N4";
			bool cyltype=false;
			PN.Cushions="8";
			PN.CushionPosition="";							
			PN.Port_Pos="11";
			PN.Stroke =String.Format("{0:##0.00}",Convert.ToDecimal(cod.Stroke.Trim()));
			string boreval="";
			decimal bore=0m;
			//bore=Convert.ToDecimal(cod.Bore.ToString());
			bore=Convert.ToDecimal("6.00");
			boreval=db.SelectValue("Bore_Code","WEB_Bore_TableV1","BoreValue",bore.ToString());
			if(boreval.Trim() !="")
			{
				PN.Bore_Size=boreval.ToString().Trim();
			}
			PN.Rod_Diamtr=db.SelectValue_Rotork("Rod_Code","WEB_RodSerM_TableV1",PN.Bore_Size,"1","RodValue").Trim();	
			PN.Seal_Comp=db.SelectValue("Seal_Code","WEB_Seal_TableV1","Seal_Type",cod.Seals.ToString().Trim()).Trim();
			if(cod.Paint.ToString().Trim() =="Standard Paint")
			{
				PN.Coating="C10";
			}
			else if(cod.Paint.ToString().Trim() =="Polyurethane Enamel")
			{
				PN.Coating="C11";
			}
			else
			{
				PN.Coating="";
			}		
			//pnpopopt
			PN.CarbonFibBarrel="";
			PN.SSPistionRod="";
			PN.TandemDuplex="";
			PN.Transducer="";
			//pnappopt
			cod.RodWiper="YES";
			if(cod.RodWiper.ToString().Trim().ToUpper() =="YES")
			{
				if(cyltype ==true)
				{
					PN.MetalScrapper="GT2";
				}
				else
				{
					PN.MetalScrapper="GR2";
				}
			}
			else
			{
				PN.MetalScrapper="";
			}
			//mount
			if(bore >=0.0m && bore <=8m)
			{
				PN.Mount="E5";	
			}
			else
			{
				PN.Mount="F5";	
			}
			PN.SSPistionRod="";
			PN.CarbonFibBarrel="";
			PN.Transducer="";
			PN.Specials="";
			if(cod.Style.ToString().ToUpper().Trim() =="DOUBLE ACTING")
			{
				PN.Style="N";
			}							
			else if(cod.Style.ToString().ToUpper().Trim() =="FAIL CLOSE")
			{
				PN.Style="FC";
				PN.Specials="SPECIAL";
			}
			else if(cod.Style.ToString().ToUpper().Trim() =="FAIL OPEN")
			{
				PN.Style="FO";
				PN.Specials="SPECIAL";
			}
			else
			{
				PN.Style="N";
			}									
			PN.SecRodDiameter="";
			PN.SecRodEnd="";
			PN.DoubleRod="No";	
			PN.Qty=cod.Qty.ToString().Trim();
			//numenclature		
			PN.PNO =PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+
				PN.Rod_End.ToString().Trim()+PN.Cushions.ToString().Trim()+PN.CushionPosition.ToString().Trim()+PN.MetalScrapper.ToString().Trim()+
				PN.Seal_Comp.ToString().Trim()+PN.Port_Type.ToString().Trim()+PN.Port_Pos.ToString().Trim()+
				PN.Mount.ToString().Trim()+PN.Stroke.ToString().Trim()+PN.SSPistionRod.ToString().Trim()+PN.CarbonFibBarrel.ToString().Trim()+PN.Transducer.ToString().Trim();
			LblPartNo.Text=PN.PNO.ToString();	
									
		    return PN;
		}
		
		private coder ASSeries_PN( VelanClass cod )
		{
			
			coder PN = new coder();
			PN.Specials="";
			try
			{
				string packingfriction = "";
				decimal s1=0 , s2 = 0;
				if(cod.RTO.ToString().Trim() !="")
				{
					s1=Convert.ToDecimal(cod.RTO.ToString().Trim());
				}
				if(cod.RTC.ToString().Trim() !="")
				{
					s2=Convert.ToDecimal(cod.RTC.ToString().Trim());
				}
				//issue #421 start
				//back
//				if(s1 >s2)
//				{
//					cod.PackingFriction=String.Format("{0:#.##}",Convert.ToDecimal(cod.RTO.ToString().Trim()));
//				}
//				else
//				{
//					cod.PackingFriction=String.Format("{0:#.##}",Convert.ToDecimal(cod.RTC.ToString().Trim()));;
//				}
				//update
				decimal s3=0m;
				if(cod.ETO.ToString().Trim() !="")
				{
					s3=Convert.ToDecimal(cod.ETO.ToString().Trim());
				}
				switch (cod.Style)
				{
					case "SeriesAS Fail Close":
						decimal dcPackingF=Math.Max(Math.Max(s1,s2),s3);
						cod.PackingFriction=Convert.ToString(dcPackingF);
						break;
					case "SeriesAS Fail Open":
						decimal dcETO=Math.Max(Math.Max(s1,s2),s3);
						cod.ETO = Convert.ToString(dcETO);
						break;
				}
				//issue #421 end
				DBClass db=new DBClass();
				//FailMaod
				blCanisterPN blcanister = new blCanisterPN();
				//issue #229 start
				try
				{
					switch (cod.Style)
					{
						case "SeriesAS Fail Close":
							cod.FailMode="FC";
							cod.AirPressure=cod.MinAirSupply;
							PN.BTOReq=cod.BTO;
							blcanister = db.ASSeries_Select_FC(Convert.ToDecimal(cod.ETC),Convert.ToDecimal(cod.Stroke), Convert.ToDecimal(cod.AirPressure),Convert.ToDecimal(cod.PackingFriction));
							blcanister = db.ASSeries_Select_FC(Convert.ToDecimal(cod.ETC)*(1+Convert.ToDecimal(cod.SaftyFactor)/100),Convert.ToDecimal(cod.Stroke), Convert.ToDecimal(cod.AirPressure),Convert.ToDecimal(cod.PackingFriction));
							if(blcanister==null)
							{
								//throw new System.ArgumentException("There is no match for canister size!");
								throw new System.ArgumentException("NoCanister");
							}
							cod.CanisterNo=blcanister.CanisterNo;
							cod.ETO=String.Format("{0:#.##}",Convert.ToDecimal(blcanister.ETO));
							cod.BTO=String.Format("{0:#.##}",Convert.ToDecimal(blcanister.BTO));
							cod.BTC=String.Format("{0:#.##}",Convert.ToDecimal(blcanister.BTC));
							//					cod.Stroke=blcanister.Stroke;
							cod.Tandem=blcanister.Tandem;
							string strTest = cod.Tandem;
							cod.SpringRate=String.Format("{0:#.##}",Convert.ToDecimal(blcanister.SpringRate));;
							cod.Preload = String.Format("{0:#.##}",Convert.ToDecimal(blcanister.Preload));
							string strBoreSession =db.Select_BoreCode_ByBoreValue(blcanister.CylinderBoreValue.TrimEnd('0').TrimEnd('.'));
							string[] strsBoreSession = strBoreSession.Split('|');
							blcanister.CylinderBoreCode=strsBoreSession[0];
							blcanister.CylinderBoreSize=strsBoreSession[1];
							string strRodSession = db.Select_RodDependency_ByBore(blcanister.CylinderBoreCode);
							string[] strsRodSession = strRodSession.Split('|');
							blcanister.CylinderRodCode=strsRodSession[0];
							blcanister.CylinderRodSize=strsRodSession[1];
							cod.Bore=blcanister.CylinderBoreCode;
							cod.Rod_Diamtr=blcanister.CylinderRodCode;
							cod.EtcValveTrust=blcanister.EtcValveTrust;
							PN.ActualSaftyFactor=String.Format("{0:###}",Convert.ToDecimal(Convert.ToString((Convert.ToDecimal(cod.Preload)/Convert.ToDecimal(cod.ETC)-1m)*100m)));
							//PN.ActualSaftyFactor=String.Format("{0:###}",Convert.ToDecimal(Convert.ToString((Convert.ToDecimal(cod.Preload)/Convert.ToDecimal(cod.ETC))*100m)));
							break;
						case "SeriesAS Fail Open":
							cod.FailMode="FO";
							cod.AirPressure=String.Format("{0:#.##}",Convert.ToDecimal(cod.MinAirSupply));;
							//blcanister = db.ASSeries_Select_FO(Convert.ToDecimal(cod.ETC),Convert.ToDecimal(cod.ETO),Convert.ToDecimal(cod.Stroke), Convert.ToDecimal(cod.AirPressure), Convert.ToDecimal(cod.BTO));
							//blcanister = db.ASSeries_Select_FO(Convert.ToDecimal(cod.ETC)*(1+Convert.ToDecimal(cod.SaftyFactor)/100),Convert.ToDecimal(cod.ETO),Convert.ToDecimal(cod.Stroke), Convert.ToDecimal(cod.AirPressure), Convert.ToDecimal(cod.BTO)*(1+Convert.ToDecimal(cod.SaftyFactor)/100));
							PN.BTOReq=cod.BTO;
							cod.BTOReq=cod.BTO;
							blcanister = db.ASSeries_Select_FO(Convert.ToDecimal(cod.EtcValveTrust)*(1+Convert.ToDecimal(cod.SaftyFactor)/100),Convert.ToDecimal(cod.ETO),Convert.ToDecimal(cod.Stroke), Convert.ToDecimal(cod.AirPressure), Convert.ToDecimal(cod.BTO)*(1+Convert.ToDecimal(cod.SaftyFactor)/100));
							if(blcanister==null)
							{
								//throw new System.ArgumentException("There is no match for canister size!");
								throw new System.ArgumentException("NoCanister");
							}
							cod.CanisterNo=blcanister.CanisterNo;
							cod.ETO=String.Format("{0:#.##}",Convert.ToDecimal(blcanister.ETO));
							cod.BTO=String.Format("{0:#.##}",Convert.ToDecimal(blcanister.BTO));
							cod.BTC=String.Format("{0:#.##}",Convert.ToDecimal(blcanister.BTC));
							//cod.Stroke=blcanister.Stroke;
							cod.Tandem=blcanister.Tandem.Replace("S","");
							cod.SpringRate=String.Format("{0:#.##}",Convert.ToDecimal(blcanister.SpringRate));;
							cod.Preload = String.Format("{0:#.##}",Convert.ToDecimal(blcanister.Preload));
							//cod.EtcValveTrust=cod.ETC;
							strBoreSession =db.Select_BoreCode_ByBoreValue(blcanister.CylinderBoreValue.TrimEnd('0').TrimEnd('.'));
							strsBoreSession = strBoreSession.Split('|');
							blcanister.CylinderBoreCode=strsBoreSession[0];
							blcanister.CylinderBoreSize=strsBoreSession[1];
							strRodSession = db.Select_RodDependency_ByBore(blcanister.CylinderBoreCode);
							strsRodSession = strRodSession.Split('|');
							blcanister.CylinderRodCode=strsRodSession[0];
							blcanister.CylinderRodSize=strsRodSession[1];
							cod.Bore=blcanister.CylinderBoreCode;
							cod.Rod_Diamtr=blcanister.CylinderRodCode;
							//cod.EtcValveTrust=blcanister.EtcValveTrust;
							PN.ETC=blcanister.ETC;
							cod.ETC=blcanister.ETC;
							PN.ActualSaftyFactor=String.Format("{0:###}",Convert.ToDecimal(Convert.ToString((Convert.ToDecimal(cod.ETC)/Convert.ToDecimal(cod.EtcValveTrust)-1m)*100m)));
							//PN.ActualSaftyFactor=String.Format("{0:###}",Convert.ToDecimal(Convert.ToString((Convert.ToDecimal(cod.ETC)/Convert.ToDecimal(cod.EtcValveTrust))*100m)));
							//cyldisplacement
						
				
							break;
					}
				}
				//issue #234 start
				catch (Exception ex)
				{		
					if(ex.Message.ToString()=="NoCanister")
					{
						switch(cod.FailMode)
						{
							case "FC":
								cod.CanisterNo="12Z";
								cod.ETO="15000";
								cod.BTO="15000";
								cod.BTC="15000";
								//cod.Stroke=blcanister.Stroke;
								cod.Tandem="";
								cod.SpringRate="2000";
								cod.Preload = "15000";
								//cod.EtcValveTrust=cod.ETC;
								cod.Bore="Z";
								cod.Rod_Diamtr="Z";
								//cod.EtcValveTrust=blcanister.EtcValveTrust;
								PN.ETC=cod.ETC;
								PN.ActualSaftyFactor="Z";
								PN.Specials="SPECIAL";
								break;
							case "FO":
								cod.CanisterNo="12Z";
								cod.ETO="15000";
								cod.BTO="15000";
								cod.BTC="15000";
								//cod.Stroke=blcanister.Stroke;
								cod.Tandem="";
								cod.SpringRate="2000";
								cod.Preload = "15000";
								//cod.EtcValveTrust=cod.ETC;
								cod.Bore="Z";
								cod.Rod_Diamtr="Z";
								//cod.EtcValveTrust=blcanister.EtcValveTrust;
								PN.ETC="15000";
								cod.ETC="15000";
								PN.ActualSaftyFactor="Z";
								PN.Specials="SPECIAL";
								break;
								
						}
						
					}
					
				}
				//issue #234 end
//				if(IsNumeric(blcanister.CylinderBoreValue)&&IsNumeric(cod.Stroke.Trim()))
//				{
//					PN.CylDis=String.Format("{0:###}",Convert.ToDecimal(blcanister.CylinderBoreValue)*Convert.ToDecimal(blcanister.CylinderBoreValue)*3.14m*Convert.ToDecimal(cod.Stroke.Trim())/4m);
//				}
				PN.Series="AS";
				//issue #242 start
				PN.Units=cod.Units;
				//issue #242 end
				PN.CanisterNo=cod.CanisterNo;
				PN.FailMode=cod.FailMode;
				PN.SpringRate=cod.SpringRate;
				PN.Preload=cod.Preload;
				PN.TandemDuplex=cod.Tandem;
				PN.Stroke =String.Format("{0:##0.00}",Convert.ToDecimal(cod.Stroke.Trim()));
				PN.Rod_End ="A4";
				PN.Cushions="8";
				PN.CushionPosition="";
				PN.Port_Type="N";
				PN.Port_Pos="1";
				PN.Seal_Comp=db.SelectValue("Seal_Code","WEB_Seal_TableV1","Seal_Type",cod.Seals.ToString().Trim()).Trim();
				PN.PackingFriction=cod.PackingFriction;
				PN.SaftyFactor = cod.SaftyFactor;
				//issue #421 start
				//PN.Specials="SPECIAL";
				//issue #421 end
				//bore
				decimal bore=0.00m;
				string seatingthrust=cod.ETC.ToString().Trim();
				if(cod.MinAirSupply.ToString().Trim() !="" &&  seatingthrust.ToString().Trim() !="" &&
					PN.PackingFriction.Trim() !="" && cod.SaftyFactor.ToString().Trim() !="" )
				{
					decimal dc1,dc2,dc3,dc4,dc5,b=0.00m;
					dc1=Convert.ToDecimal(cod.MinAirSupply.ToString().Trim());
					dc2=Convert.ToDecimal(seatingthrust.ToString().Trim());
					dc3=Convert.ToDecimal(PN.PackingFriction.ToString().Trim());
					dc4=Convert.ToDecimal(cod.SaftyFactor.ToString().Trim());
					dc4= 1 + (dc4/100);
					dc5= (( dc2 + dc3 ) *  dc4 ) / dc1;
					b= dc5 / Convert.ToDecimal(Math.PI);
					bore=Convert.ToDecimal(( Math.Sqrt(Convert.ToDouble(b))) * 2);
					bore=Decimal.Round(bore,2);
				}
				PN.Bore_Size=cod.Bore;
				PN.Rod_Diamtr=cod.Rod_Diamtr;
			
				//rodenddim
				string sr=PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim();
				string kk=db.SelectValue("RodEnd_Dimension","RodEndDiamension_TableV1","RR_Code",sr.Trim());
				if(kk.ToString().Trim() !="")
				{
					PN.RodendDim =" KK="+kk.Trim();
				}
				else PN.RodendDim="";
				//portsize
				string tmp=db.SelectValue(PN.Port_Type.Trim(),"SeriesASPortSize_TableV1","Bore",PN.Bore_Size.Trim());
				if( tmp.Trim() !="")
				{
					PN.PortSize ="#"+ tmp.Trim();
				}
				else PN.PortSize ="";
				//gt3scraper
				if(cod.RodWiper.ToString().Trim() =="YES")
				{
					PN.MetalScrapper="GT3";
					PN.Specials="SPECIAL";
				}
				//coating
				//if(cod.Paint.ToString().Trim() =="Epoxy Paint")
				if(cod.Paint.ToString().Trim() =="Epoxy Paint (black)")
			    {
					PN.Coating="C1";
					PN.Specials="SPECIAL";
				}
				else if(cod.Paint.ToString().Trim() =="Other")
				{
					PN.Coating="CTBA";
					PN.Specials="SPECIAL";
				}
				//issue #234 start
				else if(cod.Paint.ToString().Trim() =="Standard Paint Rotork Red")
				{
					PN.Coating="C7";
					PN.Specials="SPECIAL";
				}
				else if(cod.Paint.ToString().Trim() =="Polyurethane Enamel Rotork Red")
				{
					PN.Coating="C8";
					PN.Specials="SPECIAL";
				}
				//issue #234 end 
				else
				{
					PN.Coating="";
				}
				if(cod.MinAirSupply.ToString().Trim() !="")
				{
					PN.MinAirSupply=cod.MinAirSupply.ToString().Trim();
				}
				//			string seatingthrust="";
				//			string packingfriction="";
				if(cod.BTO.ToString().Trim() !="")
				{
					PN.BTO=cod.BTO.ToString().Trim();
				}
				if(cod.RTO.ToString().Trim() !="")
				{
					PN.RTO=cod.RTO.ToString().Trim();
					s1=Convert.ToDecimal(cod.RTO.ToString().Trim());
				}
				if(cod.ETO.ToString().Trim() !="")
				{
					PN.ETO=cod.ETO.ToString().Trim();
				}
				if(cod.BTC.ToString().Trim() !="")
				{
					PN.BTC=cod.BTC.ToString().Trim();
				}
				if(cod.RTC.ToString().Trim() !="")
				{
					PN.RTC=cod.RTC.ToString().Trim();
					s2=Convert.ToDecimal(cod.RTC.ToString().Trim());
				}
				if(cod.ETC.ToString().Trim() !="")
				{
					PN.ETC=cod.ETC.ToString().Trim();
					seatingthrust=cod.ETC.ToString().Trim();
				}
				PN.EtcValveTrust=cod.EtcValveTrust;
				string Portcode="";
				if(PN.Bore_Size.Trim() !="Z")
				{							
					Portcode=db.SelectOneValueFunction( PN.Bore_Size.Trim() ,"sp_Select_PortCode_WEB");
				}

				//mount
	
				//			if(cod.Mount.ToString().Trim() !="" && bore >0 && PN.Bore_Size.ToString().Trim() !="Z")
				//			{
				//				if(cod.Mount.ToString().Trim()=="MX3 Mount")
				//				{
				//					PN.Mount="X3";
				//				}
				//				else if(cod.Mount.ToString().Trim()=="MSS Mount")
				//				{
				//					string mount="";
				//					decimal cylthrust=0.00m;
				//					decimal minair=0.00m;
				//					minair=Convert.ToDecimal(cod.MinAirSupply.ToString().Trim());
				//					cylthrust=(bore * bore) * 0.785m * minair;
				//					ArrayList blist=new ArrayList();
				//					ArrayList blist1=new ArrayList();
				//					ArrayList blist2=new ArrayList();
				//					blist=db.Select_Mount_Thrust("SELECT Mounts,MaxThrust FROM WEB_Velan_Mount_Bore_TableV1 Where Mounts like 'M%' and "+PN.Bore_Size.ToString().Trim()+"='"+PN.Bore_Size.ToString().Trim()+"' Order by MaxThrust ASC ");
				//					blist1=(ArrayList)blist[0];
				//					blist2=(ArrayList)blist[1];
				//					if(blist1.Count >0)
				//					{
				//						for(int j=0;j<blist1.Count;j++)
				//						{
				//							decimal tr=0.00m;
				//							tr=Convert.ToDecimal(blist2[j].ToString());
				//							if(cylthrust < tr)
				//							{
				//								if(mount.Trim() =="")
				//								{
				//									mount=	blist1[j].ToString().Trim();
				//									PN.Mount =	blist1[j].ToString().Trim();
				//								}
				//							}
				//						}
				//					}
				//					if(mount.Trim() =="")
				//					{
				//						PN.Mount="X3";
				//					}
				//				}
				//				else if(cod.Mount.ToString().Trim()=="ISO Mount")
				//				{
				//					string mount="";
				//					decimal cylthrust=0.00m;
				//					decimal minair=0.00m;
				//					minair=Convert.ToDecimal(cod.MinAirSupply.ToString().Trim());
				//					cylthrust=(bore * bore) * 0.785m * minair;
				//					ArrayList blist=new ArrayList();
				//					ArrayList blist1=new ArrayList();
				//					ArrayList blist2=new ArrayList();
				//					blist=db.Select_Mount_Thrust("SELECT Mounts,MaxThrust FROM WEB_Velan_Mount_Bore_TableV1 Where Mounts like 'I%' and "+PN.Bore_Size.ToString().Trim()+"='"+PN.Bore_Size.ToString().Trim()+"' Order by MaxThrust ASC ");
				//					blist1=(ArrayList)blist[0];
				//					blist2=(ArrayList)blist[1];
				//					if(blist1.Count >0)
				//					{
				//						for(int j=0;j<blist1.Count;j++)
				//						{
				//							decimal tr=0.00m;
				//							tr=Convert.ToDecimal(blist2[j].ToString());
				//							if(cylthrust < tr)
				//							{
				//								if(mount.Trim() =="")
				//								{
				//									mount=	blist1[j].ToString().Trim();
				//									PN.Mount =	blist1[j].ToString().Trim();
				//								}
				//							}
				//						}
				//					}
				//					if(mount.Trim() =="")
				//					{
				//						PN.Mount="X3";
				//					}
				//				}
				//				else if(cod.Mount.ToString().Trim()=="MX Mount")
				//				{
				//					PN.Mount="X3";
				//				}
				//				else  
				//				{
				//					PN.Mount="X3";
				//				}
				//			}
				//			else
				//			{
				//				PN.Mount="X3";
				//			}
				//mount
				string mount="";
				decimal cylthrust=0.00m;
				decimal minair=0.00m;
				//issue #137		
				//back
				//if(cod.Mount.ToString().Trim() !="" && bore >0 && PN.Bore_Size.ToString().Trim() !="Z")
				//update
				if(cod.Mount.ToString().Trim() !="" && PN.Bore_Size.ToString().Trim() !="Z")
					//issue #137 end
				{
					switch(cod.Mount)
					{
						case "MSS Mount":
							minair=Convert.ToDecimal(cod.MinAirSupply.ToString().Trim());
							cylthrust=(bore * bore) * 0.785m * minair;
							ArrayList blist=new ArrayList();
							ArrayList blist1=new ArrayList();
							ArrayList blist2=new ArrayList();
							blist=db.Select_Mount_Thrust("SELECT Mounts,MaxThrust FROM WEB_Velan_Mount_Bore_TableV1 Where Mounts like 'M%' and "+PN.Bore_Size.ToString().Trim()+"='"+PN.Bore_Size.ToString().Trim()+"' Order by MaxThrust ASC ");
							blist1=(ArrayList)blist[0];
							blist2=(ArrayList)blist[1];
							if(blist1.Count >0)
							{
								for(int j=0;j<blist1.Count;j++)
								{
									decimal tr=0.00m;
									tr=Convert.ToDecimal(blist2[j].ToString());
									if(cylthrust < tr)
									{
										if(mount.Trim() =="")
										{
											mount=	blist1[j].ToString().Trim();
											PN.Mount =	blist1[j].ToString().Trim();
										}
									}
								}
							}
							break;
						case "ISO Mount":
							minair=Convert.ToDecimal(cod.MinAirSupply.ToString().Trim());
							cylthrust=(bore * bore) * 0.785m * minair;
							blist=db.Select_Mount_Thrust("SELECT Mounts,MaxThrust FROM WEB_Velan_Mount_Bore_TableV1 Where Mounts like 'I%' and "+PN.Bore_Size.ToString().Trim()+"='"+PN.Bore_Size.ToString().Trim()+"' Order by MaxThrust ASC ");
							blist1=(ArrayList)blist[0];
							blist2=(ArrayList)blist[1];
							if(blist1.Count >0)
							{
								for(int j=0;j<blist1.Count;j++)
								{
									decimal tr=0.00m;
									tr=Convert.ToDecimal(blist2[j].ToString());
									if(cylthrust < tr)
									{
										if(mount.Trim() =="")
										{
											mount=	blist1[j].ToString().Trim();
											PN.Mount =	blist1[j].ToString().Trim();
										}
									}
								}
							}
							break;
						case "MX3 Mount":
							PN.Mount="X3";
							break;
						default:
							PN.Mount="X3";
							break;

					}
				}
					//nocanister
				else if(cod.Mount.ToString().Trim() !="" && PN.Bore_Size.ToString().Trim() =="Z") 
				{
					switch(cod.Mount)
					{
						case "MSS Mount":
							PN.Mount="M07";
							break;
						case "ISO Mount":
							PN.Mount="I07";
							break;
						case "MX3 Mount":
							PN.Mount="X3";
							break;
					}
				}
				if(cod.Ratings.ToString().Trim() !="" )
				{
					if(cod.Ratings.ToString().Trim() =="Standard")
					{
						PN.Ratings="R1";
					}
					else if(cod.Ratings.ToString().Trim() =="Class 1 Div 1")
					{
						PN.Ratings="R2";
					}
					else if(cod.Ratings.ToString().Trim() =="Atex EExd Group II C")
					{
						PN.Ratings="R3";
					}
					else
					{
						PN.Ratings="R1";
					}
				}
				PN.SecRodDiameter="";
				PN.SecRodEnd="";
				PN.DoubleRod="No";
				if(cod.LimitSwitch.ToString().Trim().ToUpper() !="NO" && cod.LimitSwitch.ToString().Trim() !="")
				{
					if(cod.LimitSwitch.ToString().Trim() =="Yes, Honeywell Mechanical with Standard Mounting Kit")
					{
						PN.MountKit="MNT1";
					}
					else if(cod.LimitSwitch.ToString().Trim() =="Yes, Honeywell Mechanical with Stainless Steel Mounting Kit")
					{
						PN.MountKit="MNT2";
					}
					else if(cod.LimitSwitch.ToString().Trim() =="Yes, GO Proxy with Standard Mounting Kit")
					{
						PN.MountKit="MNT3";
					}
					else if(cod.LimitSwitch.ToString().Trim() =="Yes, GO Proxy with Stainless Steel Mounting Kit")
					{
						PN.MountKit="MNT4";
					}
					if(PN.Ratings.Trim() == "R1")
					{
						PN.LimitSwitch="LSW1";
					}
					else if(PN.Ratings.Trim() == "R2")
					{
						PN.LimitSwitch="LSW2";
					}
					else if(PN.Ratings.Trim() == "R3")
					{
						PN.LimitSwitch="LSW3";
					}
					PN.SecRodDiameter="D"+PN.Rod_Diamtr.Trim()+"2";
					PN.SecRodEnd="RA4";
					PN.DoubleRod="Yes";
					PN.Specials="SPECIAL";
				}
				else
				{
					PN.LimitSwitch="";
				}
				//manualoverride
				//issue #137
				//back
				//update
				//if(cod.ManualOverride.ToString().Trim() =="Yes")
				//update
				if(cod.ManualOverride.ToString().Trim() =="Yes, mechanical")
				{
					PN.ManualOverride="MOM";
				}
				else if(cod.ManualOverride.ToString().Trim() =="Yes, hydraulic")
				{
					PN.ManualOverride="MOH";
				}
				//issue #137 end
//				{
//					PN.ManualOverride="MO";
//					PN.SecRodDiameter="D"+PN.Rod_Diamtr.Trim()+"2";
//					PN.SecRodEnd="RA4";
//					PN.DoubleRod="Yes";
//					PN.Specials="SPECIAL";
//				}
				else
				{
					PN.ManualOverride="";
					//								PN.SecRodDiameter="";
					//								PN.SecRodEnd="";
					//								PN.DoubleRod="No";
				}
				//if(cod.PneumaticP.ToString().Trim() !="" || cod.PneumaticP.ToString().Trim() !="None")
				if(cod.PneumaticP.ToString().Trim() !="" && cod.PneumaticP.ToString().Trim() !="None")
				{
					if(cod.PneumaticP.ToString().Trim() =="Standard")
					{
						PN.Tubing="TF1";
						PN.FilterRegulator="FR1";
						PN.Specials="SPECIAL";
					}
					else if(cod.PneumaticP.ToString().Trim() =="Stainless Steel")
					{
						PN.Tubing="TF2";
						PN.FilterRegulator="FR2";
						PN.Specials="SPECIAL";
					}
					if(PN.Ratings.Trim() == "R1")
					{
//						PN.Solenoid="VA1";
						PN.Solenoid="VAS1";
					}
					else if(PN.Ratings.Trim() == "R2")
					{
						if(Portcode.Trim() =="A")
						{
//							PN.Solenoid="VA2";
							PN.Solenoid="VAS2";
						}
						else
						{
							PN.Solenoid="VA3";
						}
					}
					else if(PN.Ratings.Trim() == "R3")
					{
						if(Portcode.Trim() =="A")
						{
							PN.Solenoid="VA4";
						}
						else
						{
							PN.Solenoid="VA5";
						}
					}
				}
				if(cod.SaftyFactor.ToString().Trim() !="" )
				{
					PN.SaftyFactor=cod.SaftyFactor.ToString().Trim();
				}	
				else
				{
					PN.SaftyFactor="";
				}
				if(cod.ClosingTime.ToString().Trim() !="" )
				{
					PN.ClosingTime=cod.ClosingTime.ToString().Trim();
				}
				else
				{
					PN.ClosingTime="";
				}
				
				//numenclature
				PN.PNO =PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+
					PN.Rod_End.ToString().Trim()+PN.Seal_Comp.ToString().Trim()+PN.Port_Type.ToString().Trim()+PN.Port_Pos.ToString().Trim()+
					PN.Mount.ToString().Trim()+PN.Stroke.ToString().Trim()+PN.TandemDuplex.Replace("S","")+PN.FailMode.Replace("FO","O").Replace("FC","C")+PN.CanisterNo+"-"+PN.Preload;
				LblPartNo.Text=PN.PNO.ToString();
				
			}
			catch(Exception ex)
			{
				string ee=ex.Message;
				lblup.Text="";
				LblUpload.Text="";
				lblpage.Text="";
				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+ee+"')</script>";
				throw;
			}
			return PN;
		}

		
		private decimal ASSeries_SP(coder PN, Quotation quot)
		{
			DBClass db=new DBClass(); 
			decimal sptotal=0.00m;
			if(PN.Specials.ToString().ToUpper().Trim() =="SPECIAL")
			{
				decimal total =0.00m;
				decimal t1=0.00m;
				decimal t2=0.00m;
				bool result1=false;
				string sp ="";
				string Portcode ="";
				int tt=Convert.ToInt32(db.SelectLastSpNo());
				sp=(tt+1).ToString();
				PN.PNO ="Z"+PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim()+
					        PN.Mount.ToString().Trim()+ PN.TandemDuplex+PN.FailMode.Replace("FO","O").Replace("FC","C")+PN.CanisterNo+"-"+"/Z"+sp.ToString();
				LblPartNo.Text ="Z"+PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim()+
					PN.Mount.ToString().Trim()+ PN.TandemDuplex+PN.FailMode.Replace("FO","O").Replace("FC","C")+PN.CanisterNo+"-"+"/Z"+sp.ToString();
				Portcode=db.SelectOneValueFunction( PN.Bore_Size.Trim() ,"sp_Select_PortCode_WEB");
				db.InsertSpecialCount(sp.Trim());	
				//closingtime
				//closingtime
				string strClosingTime = "";
				if(PN.ClosingTime.ToString().Trim() !="")
				{
					decimal cl1 ,cl2=0.00m;
					cl1=Convert.ToDecimal(PN.Stroke.ToString());
					cl2=Convert.ToDecimal(PN.ClosingTime.ToString());
					if(cl1 > cl2)
					{
						lblstatus.Text ="true";
					}
					else
					{
						strClosingTime=" - Estimated closing time = " + Math.Ceiling(Convert.ToDouble(cl1)).ToString()+" s";
					}
				}	
				//end
				//specialbore
				if(PN.Bore_Size=="Z" )
				{
					    string sp1 ="";
						int tt1=Convert.ToInt32(db.SelectLastSpNo());
						sp1=(tt1+1).ToString();
						db.InsertSpecialCount(sp1.Trim());						
					
				}
				//specialsolenoid
				
				if(PN.Solenoid !=null)
				{
					if(PN.Solenoid.Trim() !="")
					{
						
						
						t1 =db.SelectAddersPrice_Specials_Velan(PN.Solenoid.Trim(),"WEB_Velan_SpecialAdder_Pricing_TableV1",Portcode.ToString().Trim());
						total =t1; 
						sptotal +=t1;
						total=Decimal.Round(t1,2);
						if(total ==0)
						{
							result1 =true;
						}
						string des="";
						des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Solenoid.Trim());
						string prt="";
						//prt=db.SelectValue("POrtSize","WEB_Velan_BorePort_TableV1","PortCode",Portcode.Trim());
						prt=db.SelectValue(PN.Port_Type.Trim(),"SeriesASPortSize_TableV1","Bore",PN.Bore_Size.Trim());
						des= des.Replace("#",prt.Trim());
						des=des+strClosingTime;
						string sp1 ="";
						int tt1=Convert.ToInt32(db.SelectLastSpNo());
						sp1=(tt1+1).ToString();
						db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Solenoid.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"2");
						db.InsertSpecialCount(sp1.Trim());						
					}
				}
				//specialfilterregulator
				if(PN.FilterRegulator !=null)
				{
					if(PN.FilterRegulator.Trim() !="")
					{
						t1 =db.SelectAddersPrice_Specials_Velan(PN.FilterRegulator.Trim(),"WEB_Velan_SpecialAdder_Pricing_TableV1",Portcode.ToString().Trim());
						total =t1; 
						sptotal +=t1;
						total=Decimal.Round(t1,2);
						if(total ==0)
						{
							result1 =true;
						}
						string des="";
						des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.FilterRegulator.Trim());
						string sp1 ="";
						int tt1=Convert.ToInt32(db.SelectLastSpNo());
						sp1=(tt1+1).ToString();
						db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.FilterRegulator.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"3");
						db.InsertSpecialCount(sp1.Trim());
					}
				}
				//specialtubing
				if(PN.Tubing !=null)
				{
					if(PN.Tubing.Trim() !="")
					{
						decimal t3=0.00m;
						t1 =db.SelectAddersPrice_Specials_Velan(PN.Tubing.Trim()+"Base","WEB_Velan_SpecialAdder_Pricing_TableV1",Portcode.ToString().Trim());
						t2 =db.SelectAddersPrice_Specials_Velan(PN.Tubing.Trim()+"PerInch","WEB_Velan_SpecialAdder_Pricing_TableV1",Portcode.ToString().Trim());
						t3=Convert.ToDecimal(PN.Stroke.ToString().Trim());
						total =t1 + (t2 * t3); 
						sptotal +=total;
						total=Decimal.Round(total,2);
						if(total ==0)
						{
							result1 =true;
						}	
						string des="";
						des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Tubing.Trim());
						string sp1 ="";
						int tt1=Convert.ToInt32(db.SelectLastSpNo());
						sp1=(tt1+1).ToString();
						db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Tubing.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"4");
						db.InsertSpecialCount(sp1.Trim());
					}
				}
				//specialmanualoverride
				if(PN.ManualOverride !=null)
				{
					if(PN.ManualOverride.Trim() !="")
					{
						total =0.00m;
						if(total ==0)
						{
							result1 =true;
						}
						string des="";
						des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.ManualOverride.Trim());
						string sp1 ="";
						int tt1=Convert.ToInt32(db.SelectLastSpNo());
						sp1=(tt1+1).ToString();
						db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.ManualOverride.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"5");
						db.InsertSpecialCount(sp1.Trim());
					}
				}
				//specialgt3scraper
				if(PN.MetalScrapper !=null)
				{
					if(PN.MetalScrapper.Trim() =="GT3")
					{
						total =0.00m;
						total=db.SelectAddersPrice("GT3","WEB_SeriesASCommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
						if(total ==0)
						{
							result1 =true;
						}
						string des="";
						des=db.SelectAppopts(PN.MetalScrapper.ToString().Trim());
						string sp1 ="";
						int tt1=Convert.ToInt32(db.SelectLastSpNo());
						sp1=(tt1+1).ToString();
						db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.ManualOverride.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"5");
						db.InsertSpecialCount(sp1.Trim());
					}
				}
				//specialcoating
				if(PN.Coating.ToString().Trim() =="C1")
				{
					t1=0;
					t1 =db.SelectAddersPrice_Specials_ByBore("C1","WEB_Specials_Velan_Pricing_TableV1",PN.Bore_Size.ToString().Trim());
					total =t1; 
					sptotal +=total;
					if(total ==0)
					{
						result1 =true;
					}
					string desc="";
					//desc=db.SelectPopopts(PN.Coating.Trim());
					//db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.Coating.ToString(),desc.ToString(),"0.00");
					desc=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Coating.Trim());;
					string sp1 ="";
					int tt1=Convert.ToInt32(db.SelectLastSpNo());
					sp1=(tt1+1).ToString();
					db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Coating.ToString(),desc.Trim(),"0","1","0","0","7");
					db.InsertSpecialCount(sp1.Trim());
				}
				if(PN.Coating.ToString().Trim() =="CTBA")
				{
					t1=0;
					//t1 =db.SelectAddersPrice_Specials_ByBore("C1","WEB_Specials_Velan_Pricing_TableV1",PN.Bore_Size.ToString().Trim());
					total =t1; 
					sptotal +=total;
					if(total ==0)
					{
						result1 =true;
					}
					string desc="";
					desc=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Coating.Trim());;
					//db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.Coating.ToString(),desc.ToString(),"0.00");
					string sp1 ="";
					int tt1=Convert.ToInt32(db.SelectLastSpNo());
					sp1=(tt1+1).ToString();
					db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Coating.ToString(),desc.Trim(),"0","1","0","0","7");
					db.InsertSpecialCount(sp1.Trim());
				}
				//issue #234 start
				if(PN.Coating.ToString().Trim() =="C8" || PN.Coating.ToString().Trim() =="C7")
				{
					if(PN.Coating.ToString().Trim() =="C8")
					{
						t1=0;
						t1 =db.SelectAddersPrice("C8","WEB_SeriesASCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.Trim());
						total =t1; 
						sptotal +=total;
						if(total ==0)
						{
							result1 =true;
						}
					
					}
					else if(PN.Coating.ToString().Trim() =="C7")
					{
						t1=0;
					}
					string desc="";
					desc=db.SelectPopopts(PN.Coating.Trim());
					string sp1 ="";
					int tt1=Convert.ToInt32(db.SelectLastSpNo());
					sp1=(tt1+1).ToString();
					db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Coating.ToString(),desc.Trim(),"0","1","0","0","7");
					db.InsertSpecialCount(sp1.Trim());
				}	
				//issue #234 end
				if(result1 ==true)
				{
					lblstatus.Text ="true";
				}
				//speciallimitswitch
				if(PN.LimitSwitch !=null)
				{
					if(PN.LimitSwitch.Trim() !="")
					{
						t1 =db.SelectAddersPrice_Specials_Velan(PN.LimitSwitch.Trim() ,"WEB_Velan_SpecialAdder_Pricing_TableV1",Portcode.ToString().Trim());
						t2 =db.SelectAddersPrice_Specials_Velan(PN.MountKit.Trim() ,"WEB_Velan_SpecialAdder_Pricing_TableV1",Portcode.ToString().Trim());
						total =t1+t2; 
						sptotal +=total;
						total=Decimal.Round(total,2);
						if(t2 ==0)
						{
							result1 =true;
						}	
						if(total ==0)
						{
							result1 =true;
						}	
						string des="";
						des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.LimitSwitch.Trim());
						des +=", "+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.MountKit.Trim());
						string sp1 ="";
						int tt1=Convert.ToInt32(db.SelectLastSpNo());
						sp1=(tt1+1).ToString();
						db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.LimitSwitch.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"6");
						db.InsertSpecialCount(sp1.Trim());
					}
				}
				
				if(result1 ==true)
				{
					lblstatus.Text= "true";
				}
			}
			lbltotal.Text=Convert.ToString(Convert.ToDecimal(lbltotal.Text)+ sptotal);
			return sptotal;
			
		}
		//issue #233 start
		private string Cyl_Displacement(coder PN)
		{
			string strCylDis = "TBA";
			try
			{
				DBClass db=new DBClass();
				decimal dcBore = 0m;
				decimal dcRod = 0m;
				decimal dcStroke = Convert.ToDecimal(PN.Stroke);
				dcRod = Convert.ToDecimal(db.SelectRodValue(PN.Rod_Diamtr));
				dcBore = Convert.ToDecimal(db.SelectBoreValue(PN.Bore_Size));
				decimal dcCylDis = 0m;
				decimal dcCylDisRet=0m;
				if(PN.DoubleRod == "No" && PN.FailMode != "FC" && PN.FailMode != "FO" )
				{
					dcCylDisRet=(dcBore*dcBore)*3.14m*dcStroke/4m;
				}
				dcCylDis=(dcBore*dcBore-dcRod*dcRod)*3.14m*dcStroke/4m + dcCylDisRet;
				if(PN.TandemDuplex == "TC")
				{
					dcCylDis *=2m;
				}
				dcCylDis = Math.Round(dcCylDis,0);
				strCylDis=dcCylDis.ToString();
			}
			catch (Exception ex)
			{		
				
				
			}
			return strCylDis;
		}
		//issue #233 end
		//canisternum 
		//private string Create_CanisterDesc(string failmode, string canisterno, string preload, string bto, string eto, string etc , string btc, string sfce, string btoreq)
		private string Create_CanisterDesc(coder PN)
		{
			string strFailMode="";
			try
			{
                //issue #230 start
				DBClass db=new DBClass();
                decimal dcCylArea = db.AS_Select_CylArea(PN.Bore_Size,PN.TandemDuplex);
				//issue #230 end
				string sfcb="";
				switch(PN.FailMode)
				{
					case "FC":		
						//if(IsNumeric(bto)) sfcb=String.Format("{0:###}",Convert.ToDecimal(Convert.ToString((Convert.ToDecimal(bto)/Convert.ToDecimal(preload)-1m)*100m)));
						//if(IsNumeric(PN.BTO)) sfcb=String.Format("{0:###}",Convert.ToDecimal(Convert.ToString((Convert.ToDecimal(PN.BTO)/Convert.ToDecimal(PN.Preload)-1m)*100m)));
				        //back
						if(IsNumeric(PN.BTO)) sfcb=String.Format("{0:###}",Convert.ToDecimal(Convert.ToString((Convert.ToDecimal(PN.BTO)/Convert.ToDecimal(PN.BTOReq)-1m)*100m)));
						//if(IsNumeric(bto)) sfcb=String.Format("{0:###}",Convert.ToDecimal(Convert.ToString((Convert.ToDecimal(bto)/Convert.ToDecimal(preload))*100m)));
						//strFailMode="CAN" + canisterno + failmode+ preload +"; " + "Fail Close. ETC= "+preload+" lbs, BTC= "+btc+" lbs ; BTO= " +bto +" lbs, ETO= "+eto+" lbs";
						//strFailMode="CAN" + canisterno + failmode.Replace("FO","O").Replace("FC","C")+ preload +"; " + "Fail Close. ETC= "+preload+" lbs, BTC= "+btc+" lbs ; BTO= " +bto +" lbs, ETO= "+eto+" lbs";
						//strFailMode="CAN" + canisterno + failmode.Replace("FO","O").Replace("FC","C")+ preload +"; " + "Fail Close. ETC= "+preload+" lbs (S.F.= " + sfce + " %); ETO= " + eto+ " lbs ; BTO= " +bto +" lbs (S.F.= "+sfcb+" %); BTC= "+btc+" lbs";
					    //issue #230 start
						//if(IsNumeric(PN.BTOReq)) sfcb=String.Format("{0:###}",Convert.ToDecimal(Convert.ToString((Convert.ToDecimal(PN.MinAirSupply)*dcCylArea/Convert.ToDecimal(PN.BTO)-1m)*100m)));
						//issue #230 end
						//issue #242 start
						if(PN.Units=="metric")
						strFailMode="CAN" + PN.CanisterNo + PN.FailMode.Replace("FO","O").Replace("FC","C")+ PN.Preload +"; " 
							        + "Fail Close. ETC= "+PN.Preload+" lbs ("+String.Format("{0:###}",Convert.ToDecimal(PN.Preload)*4.448m)+" N) (S.F.= " + PN.ActualSaftyFactor + " %); ETO= " 
							        + PN.ETO+ " lbs ("+String.Format("{0:###}",Convert.ToDecimal(PN.ETO)*4.448m)+" N) ; BTO= " 
							        + PN.BTO +" lbs ("+String.Format("{0:###}",Convert.ToDecimal(PN.BTO)*4.448m)+" N) (S.F.= "+sfcb+" %); BTC= "
							        +PN.BTC+" lbs ("+String.Format("{0:###}",Convert.ToDecimal(PN.BTC)*4.448m)+" N)";
						else 
						//back
						strFailMode="CAN" + PN.CanisterNo + PN.FailMode.Replace("FO","O").Replace("FC","C")+ PN.Preload +"; " + "Fail Close. ETC= "+PN.Preload+" lbs (S.F.= " + PN.ActualSaftyFactor + " %); ETO= " + PN.ETO+ " lbs ; BTO= " +PN.BTO +" lbs (S.F.= "+sfcb+" %); BTC= "+PN.BTC+" lbs";
						//issue #242
						break;
					case "FO":
						//if(IsNumeric(btoreq)) sfcb=String.Format("{0:###}",Convert.ToDecimal(Convert.ToString((Convert.ToDecimal(bto)/Convert.ToDecimal(btoreq)-1m)*100m)));
			            //back
						if(IsNumeric(PN.BTOReq)) sfcb=String.Format("{0:###}",Convert.ToDecimal(Convert.ToString((Convert.ToDecimal(PN.BTO)/Convert.ToDecimal(PN.BTOReq)-1m)*100m)));
						//if(IsNumeric(btoreq)) sfcb=String.Format("{0:###}",Convert.ToDecimal(Convert.ToString((Convert.ToDecimal(bto)/Convert.ToDecimal(btoreq))*100m)));
						//strFailMode="CAN" + canisterno + failmode+ preload +"; " + "Fail Open. ETO= "+eto +" lbs, BTO= " +bto +" lbs; ETC= " +etc+" lbs, BTC= "+btc+" lbs";
						//strFailMode="CAN" + canisterno + failmode.Replace("FO","O").Replace("FC","C")+ preload +"; " + "Fail Open. ETO= "+preload +" lbs, BTO= " +bto +" lbs; ETC= " +etc+" lbs, BTC= "+btc+" lbs";
						//strFailMode="CAN" + canisterno + failmode.Replace("FO","O").Replace("FC","C")+ preload +"; " + "Fail Open. ETO= "+preload +" lbs; BTO= " +bto+" lbs (S.F.= " + sfcb+" %); ETC= " +etc+" lbs (S.F.= "+ sfce + " %); BTC= "+btc+" lbs";
						//issue #230 start
						//if(IsNumeric(PN.BTOReq)) sfcb=String.Format("{0:###}",Convert.ToDecimal(Convert.ToString((Convert.ToDecimal(PN.MinAirSupply)*dcCylArea/Convert.ToDecimal(PN.BTO)-1m)*100m)));
						//issue #230 end
						//issue #242 start
						//update
						if(PN.Units=="metric")
						strFailMode="CAN" + PN.CanisterNo+ PN.FailMode.Replace("FO","O").Replace("FC","C")+ PN.Preload+"; " 
							       + "Fail Open. ETO= "+PN.Preload +" lbs ("+String.Format("{0:###}",Convert.ToDecimal(PN.Preload)*4.448m)+" N); BTO= " 
							       +PN.BTO+" lbs ("+String.Format("{0:###}",Convert.ToDecimal(PN.BTO)*4.448m)+" N) (S.F.= " + sfcb+" %); ETC= " 
							       +PN.ETC+" lbs ("+String.Format("{0:###}",Convert.ToDecimal(PN.ETC)*4.448m)+" N) (S.F.= "+ PN.ActualSaftyFactor + " %); BTC= "
							       +PN.BTC+" lbs ("+String.Format("{0:###}",Convert.ToDecimal(PN.BTC)*4.448m)+" N)";
						else
						//back
						strFailMode="CAN" + PN.CanisterNo+ PN.FailMode.Replace("FO","O").Replace("FC","C")+ PN.Preload+"; " + "Fail Open. ETO= "+PN.Preload +" lbs; BTO= " +PN.BTO+" lbs (S.F.= " + sfcb+" %); ETC= " +PN.ETC+" lbs (S.F.= "+ PN.ActualSaftyFactor + " %); BTC= "+PN.BTC+" lbs";
						//issue #242 end
						break;
				}
				//issue #229 start
				if(PN.Bore_Size=="Z") 
				{
					strFailMode = "CAN" + PN.CanisterNo;
					switch(PN.FailMode)
					{
						case "FC":
							strFailMode = "CAN" + PN.CanisterNo + PN.FailMode.Replace("FO","O").Replace("FC","C")+ "?" +"; " + "Fail Close. ETC= "+ "?" +" lbs (S.F.= " +  "?"  + " %); ETO= " +  "?" + " lbs ; BTO= " + "?"  +" lbs (S.F.= "+ "?" +" %); BTC= "+ "?" +" lbs";
							break;
						case "FO":
							strFailMode="CAN" + PN.CanisterNo+ PN.FailMode.Replace("FO","O").Replace("FC","C")+ "?"+"; " + "Fail Open. ETO= "+"?" +" lbs; BTO= " +"?"+" lbs (S.F.= " + "?"+" %); ETC= " +"?"+" lbs (S.F.= "+ "?" + " %); BTC= "+"?"+" lbs";
							break;
					}
				}
				//issue #229 end
			}
			catch{}
			return strFailMode;
		}
		
		private void MLSeries_App(coder PN, Quotation quot, QItems Qitem)
		{
			DBClass db=new DBClass();
			decimal total =0.00m;
			decimal t1 =0.00m;
			bool result1=false;
			decimal app=0.00m;
			string commadd = "";
			if(Qitem.S_Code.Trim() !="")
			{
				commadd="WEB_Series"+Qitem.S_Code.Trim()+"CommAdders_TableV1";
				string ind="0";
//				if(PN.Bore_Size.ToString().Trim()=="C" || PN.Bore_Size.ToString().Trim()=="D" || PN.Bore_Size.ToString().Trim()=="E")
//				{			
//					ind=db.SelectPriceIndex("MLS").ToString();	
//				}
//				else
//				{
//					ind=db.SelectPriceIndex(Qitem.S_Code.Trim()).ToString();	
//				}									
				//index =Convert.ToDecimal(ind.Trim());
			}
			if(PN.MetalScrapper.ToString().Trim() !="" && PN.Series!="AS")
			{
				if(PN.MetalScrapper.ToString().Trim() =="GR2")
				{
					if(PN.Seal_Comp.Trim() =="F")
					{
						app =db.SelectAddersPrice("FGR2",commadd.Trim(),PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());											
					}
						// issue #79
					else
					{	
						app =db.SelectAddersPrice("GR2",commadd.Trim(),PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());											
					}
				}
				//issue #137 start
				if(PN.MetalScrapper.ToString().Trim() =="GT2")
				{
					app =db.SelectAddersPrice("GT2",commadd.Trim(),PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());											
											
				}
				//issue #137 end
				if(app ==0)
				{
					lblstatus.Text ="true";
				}
				string desc="";
				desc=db.SelectAppopts(PN.MetalScrapper.ToString().Trim());
				db.InsertItemApplicationOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.MetalScrapper.ToString(),desc.ToString(),"0.00");
				Qitem.ApplicationOpt_Id=quot.QuoteNo.Trim()+LblPartNo.Text.Trim();
			}
			else
			{
				Qitem.ApplicationOpt_Id="";
			}	
			lbltotal.Text=Convert.ToString(Convert.ToDecimal(lbltotal.Text)+ app);
			
		}

		private void MLSeries_Pop(coder PN, Quotation quot, QItems Qitem)
		{
			DBClass db=new DBClass();
			decimal total =0.00m;
			decimal t1 =0.00m;
			bool result1=false;
			decimal pop=0.00m;
			if(PN.CarbonFibBarrel.ToString().Trim() !="" || PN.SSPistionRod.ToString().Trim() !="" || PN.TandemDuplex.ToString().Trim() !="" || PN.Transducer.ToString().Trim() !="" )
			{
				if(PN.CarbonFibBarrel.ToString().Trim() =="M8")
				{
					string desc="";
					desc=db.SelectPopopts(PN.CarbonFibBarrel.Trim());
					db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.CarbonFibBarrel.ToString(),desc.ToString(),"0.00");
				}
				if(PN.SSPistionRod.ToString().Trim() !="")
				{
					string desc="";
					desc=db.SelectPopopts(PN.SSPistionRod.Trim());
					db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.SSPistionRod.ToString(),desc.ToString(),"0.00");
				}
				if(PN.TandemDuplex.ToString().Trim() !="")
				{										
					string desc="";
					desc=db.SelectPopopts(PN.TandemDuplex.Trim());
					db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.TandemDuplex.ToString(),desc.ToString(),"0.00");
				}	
				if(PN.Transducer.ToString().Trim() !="")
				{
					decimal transducer=0;
					if(PN.Series.Trim()=="L" || PN.Series.Trim()=="ML")
					{										
						decimal tbbase=0;
						decimal tbstroke=0;
						tbbase =db.SelectAddersPrice("TB_Base","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
						tbstroke=db.SelectAddersPrice_Transducer("TB_Series"+PN.Series.Trim(),PN.Stroke.ToString().Trim());
						transducer=tbbase +tbstroke;
						//transducer =transducer + transducer * (index /100);
						pop=transducer;
					}
					if(transducer ==0)
					{
						lblstatus.Text ="true";
					}
										
					string desc="";
					desc=db.SelectPopopts(PN.Transducer.Trim());
					db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.Transducer.ToString(),desc.ToString(),"0.00");
				}
				Qitem.PopularOpt_Id=quot.QuoteNo.Trim()+LblPartNo.Text.Trim();
			}								
			else
			{
				Qitem.PopularOpt_Id="";
			}
			lbltotal.Text=Convert.ToString(Convert.ToDecimal(lbltotal.Text)+ pop);
			
		}

		private void ASSeries_Pop(coder PN, Quotation quot, QItems Qitem)
		{
			DBClass db=new DBClass();
			decimal total =0.00m;
			decimal t1 =0.00m;
			bool result1=false;
			decimal pop=0.00m;
			//canisterprice
			decimal dcCanisterPrice = Convert.ToDecimal(db.SelectOneValueByAllinfo("CanPrice","AS_PRELOAD_SPACER",
				"CanisterNo",PN.CanisterNo,
				"Preload",PN.Preload).ToString());
			total=dcCanisterPrice;
			if(total ==0)
			{
				result1 =true;
			}
			//string strCanisterDesc = Create_CanisterDesc(PN.FailMode,PN.CanisterNo,PN.Preload,PN.BTO,PN.ETO,PN.ETC,PN.BTC, PN.ActualSaftyFactor, PN.BTOReq);
			string strCanisterDesc = Create_CanisterDesc(PN);
			db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.FailMode.ToString(),strCanisterDesc,dcCanisterPrice.ToString());
			if(result1 ==true)
			{
				lblstatus.Text ="true";
			}
			else
			{
				Qitem.PopularOpt_Id="";
			}
			pop =total;
			total=0;
			//insertcanister
			 string strBTOReq = PN.BTOReq;
			string strCanisterIns = db.InsertCanisters(quot.QuoteNo,PN.PNO,PN.FailMode,PN.CanisterNo,PN.Preload,PN.ETC, PN.ETO,PN.BTO,PN.MinAirSupply,PN.PackingFriction,PN.SpringRate,PN.EtcValveTrust,dcCanisterPrice.ToString(),PN.SaftyFactor,PN.BTC,PN.BTOReq);
//			switch(PN.FailMode)
//			{
//				case "FC":
//					string strCanisterIns = db.InsertCanisters(quot.QuoteNo,PN.PNO,PN.FailMode,PN.CanisterNo,PN.Preload,PN.ETC, PN.ETO,PN.BTO,PN.MinAirSupply,PN.PackingFriction,PN.SpringRate,PN.EtcValveTrust,dcCanisterPrice.ToString(),PN.SaftyFactor,PN.BTC);
//					break;
//				case "FO":
//					string strCanisterIns = db.InsertCanisters(quot.QuoteNo,PN.PNO,PN.FailMode,PN.CanisterNo,PN.Preload,PN.ETC, PN.ETO,PN.BTO,PN.MinAirSupply,PN.PackingFriction,PN.SpringRate,PN.EtcValveTrust,dcCanisterPrice.ToString(),PN.SaftyFactor,PN.BTC);
//					break;
//			}
			Qitem.PopularOpt_Id=quot.QuoteNo.Trim()+LblPartNo.Text.Trim();
//			if(PN.Coating.ToString().Trim() !="")
//			{
//				//popoptcoating
//				if(PN.Coating.ToString().Trim() =="C1")
//				{
//					t1=0;
//					t1 =db.SelectAddersPrice_Specials_ByBore("C1","WEB_Specials_Velan_Pricing_TableV1",PN.Bore_Size.ToString().Trim());
//					total =t1; 
//					if(total ==0)
//					{
//						result1 =true;
//					}
//					string desc="";
//					desc=db.SelectPopopts(PN.Coating.Trim());
//					db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.Coating.ToString(),desc.ToString(),"0.00");
//				}
//				if(result1 ==true)
//				{
//					lblstatus.Text ="true";
//				}
//				pop +=total;
//				Qitem.PopularOpt_Id=quot.QuoteNo.Trim()+LblPartNo.Text.Trim();
//			}
//			else
//			{
//				Qitem.PopularOpt_Id="";
//			}
			if(PN.TandemDuplex.Trim() == "TC")
			{
			//tcprice
			//tcdesc
				string desc="";
				desc=db.SelectPopopts(PN.TandemDuplex.Trim());
			//tcinsert
				db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.TandemDuplex.ToString(),desc.ToString(),"0.00");
			//tctotal
			}
			else{}
			lbltotal.Text=Convert.ToString(Convert.ToDecimal(lbltotal.Text)+ pop);
//			decimal dcc2=0.00m;
//			dcc2 =Convert.ToDecimal(lbltotal.Text);
//			lbltotal.Text=dcc2.ToString();
		}
		
		private QItems MLSeries_QItem(coder PN, Quotation quot, VelanClass cod , string userID)
		{
			DBClass db=new DBClass();
			QItems Qitem =new QItems();
			//roddim
			string rd=PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim();
			string rodenddim ="";
			if(db.SelectRRD(rd).Trim() !="")
			{
				rodenddim =" KK = "+db.SelectRRD(rd);
			}
			//portsize
			string portsiz="";
			if(db.SelectPortSizeNo(PN.Bore_Size.Trim(),PN.Port_Type.Trim(),"WEB_SeriesMMLLRPortSize_TableV1") !="")
			{
				portsiz ="#"+db.SelectPortSizeNo(PN.Bore_Size.Trim(),PN.Port_Type.Trim(),"WEB_SeriesMMLLRPortSize_TableV1");
			}
			string st="";
			if(Convert.ToDecimal(PN.Stroke.ToString()) > 120.00m)
			{
				st="For pricing please consult factory";
				lblstatus.Text="true";
			}
			//partno
			Qitem.PartNo=PN.PNO;
			Qitem.ItemNo =PN.PNO;
			Qitem.S_Code=PN.Series.Trim();
			Qitem.Series=db.SelectValue("Series_Name","WEB_Series_TableV1","Series_Code",PN.Series.ToString().Trim()).ToString();
			Qitem.S_Price="0.00";
			Qitem.B_Code=PN.Bore_Size.ToString().Trim();
			Qitem.Bore=db.SelectValue("Bore_Size","WEB_Bore_TableV1","Bore_Code",PN.Bore_Size.ToString().Trim()).ToString()+" Bore Size";
			Qitem.B_Price="0.00";
			Qitem.R_Code=PN.Rod_Diamtr.ToString().Trim();
			Qitem.Rod=db.SelectValue("Rod_Size","WEB_RodSerZ_TableV1","Rod_Code",PN.Rod_Diamtr.ToString().Trim())+" Rod Size";
			Qitem.R_Price="0.00";
			Qitem.Stroke_Code=PN.Stroke.ToString().Trim();
			Qitem.Stroke="Stroke = "+PN.Stroke.ToString()+"\""+st.ToString();
			Qitem.Stroke_Price="0.00";
			Qitem.M_code=PN.Mount.ToString().Trim();
			Qitem.Mount=db.SelectValue("Mount_Type","WEB_Mount"+PN.Series.Trim()+"_TableV1","Mount_Code",PN.Mount.ToString().Trim()).ToString();
			Qitem.M_Price="0.00";
			Qitem.RE_Code=PN.Rod_End.ToString().Trim();
			Qitem.RodEnd=db.SelectValue("RodEnd_Shape","WEB_RodEndKK_TableV1","RodEnd_Code",PN.Rod_End.ToString().Trim()).ToString()+rodenddim.ToString();
			Qitem.RE_Price="0.00";
			Qitem.Cu_Code=PN.Cushions.ToString().Trim();
			Qitem.Cushion=db.SelectValue("Cushion_type","WEB_Cushion_TableV1","Cushion_Code",PN.Cushions.ToString()).ToString();
			Qitem.Cu_Price="0.00";
			Qitem.CushionPos_Code=PN.CushionPosition.ToString().Trim();
			Qitem.CushionPos=db.SelectValue("CushionPos_Pos","WEB_CushionPos_TableV1","CushionPos_Code",PN.CushionPosition.ToString().Trim()).ToString();
			Qitem.CushionPos_Price="0.00";
			Qitem.Sel_Code=PN.Seal_Comp.ToString().Trim();
			Qitem.Seal=db.SelectValue("Seal_Type","WEB_Seal_TableV1","Seal_Code",PN.Seal_Comp.ToString().Trim()).ToString();
			Qitem.Sel_Price="0.00";
			Qitem.Port_Code=PN.Port_Type.ToString().Trim();
			Qitem.Port=portsiz.Trim()+" "+ db.SelectValue("PortType_Type","WEB_portType_TableV1","PortType_Code",PN.Port_Type.ToString().Trim()).ToString();
			Qitem.Port_Price="0.00";
			Qitem.PP_Code=PN.Port_Pos.ToString().Trim();
			Qitem.PortPos=db.SelectValue("PortPos_Position","WEB_PortPosition_TableV1","PortPos_Code",PN.Port_Pos.ToString().Trim()).ToString();
			Qitem.PP_Price="0.00";
			Qitem.Quantity=cod.Qty.ToString();
			//discount
			if(Qitem.S_Code.Trim()=="A" || Qitem.S_Code.Trim()=="AC" || Qitem.S_Code.Trim()=="AT" )
			{
				Qitem.Discount="0";
			}
			else
			{
				if(PN.Series =="ML" || PN.Series =="L" )
				{
					decimal qty=0;
					qty=Convert.ToDecimal(PN.Qty.ToString());
					if(qty >0 && qty <=2)
					{
						Qitem.Discount="35";
					}
					else if(qty >=3 && qty <=5)
					{
						Qitem.Discount="37";
					}
					else if(qty >=6 && qty <=10)
					{
						Qitem.Discount="40";
					}
					else if(qty >10)
					{
						Qitem.Discount="45";
					}
				}
				else
				{
					Qitem.Discount="35";
				}
			}
			Qitem.ApplicationOpt_Id="";
			Qitem.Special_ID="";
			Qitem.Cusomer_ID=quot.Customer.Trim();
			Qitem.Quotation_No=quot.QuoteNo.Trim();
			Qitem.Q_Date=quot.Quotedate.Trim();
			Qitem.User_ID=userID;
			Qitem.PriceList="0";
			Qitem.DWG ="";
			string dwgnotes="";								
			Qitem.SpecialReq=dwgnotes.ToString();
			Qitem.Note=cod.Notes.ToString();
			Qitem.Weight = "Approximate cylinder weight (does not include accessories)= "+CylWeight_ML(PN).ToString() +" LBS";
			//issue #233 start
			Qitem.Weight +=";The Cylinder Displacement = " + Cyl_Displacement(PN) + " in" + Convert.ToChar(0179).ToString();
			//issue #233 end
			return Qitem;
		}
		private QItems ASSeries_QItem(coder PN, Quotation quot, VelanClass cod , string userID)
		{
			DBClass db=new DBClass();
			QItems Qitem =new QItems();
			Qitem.PartNo=PN.PNO;
			//issue #242 start
			Qitem.Units = PN.Units;
			//issue #242 end
			Qitem.ItemNo =quot.QuoteNo.ToString();
			Qitem.S_Code=PN.Series.Trim();
			Qitem.Series=db.SelectValue("Series_Name","WEB_Series_TableV1","Series_Code",PN.Series.ToString().Trim()).ToString();
			Qitem.S_Price="0.00";
			Qitem.B_Code=PN.Bore_Size.ToString().Trim();
			Qitem.Bore=db.SelectValue("Bore_Size","WEB_Bore_TableV1","Bore_Code",PN.Bore_Size.ToString().Trim()).ToString()+" Bore Size";
			Qitem.B_Price="0.00";
			Qitem.R_Code=PN.Rod_Diamtr.ToString().Trim();
			Qitem.Rod=db.SelectValue("Rod_Size","WEB_RodSerZ_TableV1","Rod_Code",PN.Rod_Diamtr.ToString().Trim())+" Rod Size";
			Qitem.R_Price="0.00";
			Qitem.Stroke_Code=PN.Stroke.ToString().Trim();
			string st="";
			if(Convert.ToDecimal(PN.Stroke.ToString()) > 120.00m)
			{
				st="For pricing please consult factory";
				lblstatus.Text="true";
			}
			Qitem.Stroke="Stroke = "+PN.Stroke.ToString()+"\""+st.ToString();
			Qitem.Stroke_Price="0.00";
			Qitem.M_code=PN.Mount.ToString().Trim();
			Qitem.Mount=db.SelectValue("Mount_Type","Mount"+PN.Series.Trim()+"_TableV1","Mount_Code",PN.Mount.ToString().Trim()).ToString();
			Qitem.M_Price="0.00";
			Qitem.RE_Code=PN.Rod_End.ToString().Trim();
			Qitem.RodEnd=db.SelectValue("RodEnd_Shape","WEB_RodEndKK_TableV1","RodEnd_Code",PN.Rod_End.ToString().Trim()).ToString()+PN.RodendDim;
			Qitem.RE_Price="0.00";
			Qitem.Cu_Code=PN.Cushions.ToString().Trim();
			Qitem.Cushion=db.SelectValue("Cushion_type","WEB_Cushion_TableV1","Cushion_Code",PN.Cushions.ToString()).ToString();
			Qitem.Cu_Price="0.00";
			Qitem.CushionPos_Code=PN.CushionPosition.ToString().Trim();
			Qitem.CushionPos=db.SelectValue("CushionPos_Pos","WEB_CushionPos_TableV1","CushionPos_Code",PN.CushionPosition.ToString().Trim()).ToString();
			Qitem.CushionPos_Price="0.00";
			Qitem.Sel_Code=PN.Seal_Comp.ToString().Trim();
			Qitem.Seal=db.SelectValue("Seal_Type","WEB_Seal_TableV1","Seal_Code",PN.Seal_Comp.ToString().Trim()).ToString();
			Qitem.Sel_Price="0.00";
			Qitem.Port_Code=PN.Port_Type.ToString().Trim();
			//Qitem.Port=PN.PortSize.Trim()+" "+ db.SelectValue("PortType_Type","WEB_portType_TableV1","PortType_Code",PN.Port_Type.ToString().Trim()).ToString();
			Qitem.Port="#"+ db.SelectValue(PN.Port_Type.Trim(),"SeriesASPortSize_TableV1","Bore",PN.Bore_Size.Trim()).ToString()+" "+ db.SelectValue("PortType_Type","WEB_portType_TableV1","PortType_Code",PN.Port_Type.ToString().Trim()).ToString();
			Qitem.Port_Price="0.00";
			Qitem.PP_Code=PN.Port_Pos.ToString().Trim();
			Qitem.PortPos=db.SelectValue("PortPos_Position","WEB_PortPosition_TableV1","PortPos_Code",PN.Port_Pos.ToString().Trim()).ToString();
			Qitem.PP_Price="0.00";
			Qitem.Quantity=cod.Qty.ToString();
			Qitem.Discount="0";
			Qitem.ApplicationOpt_Id="";
	        Qitem.Special_ID="";
			Qitem.Cusomer_ID=quot.Customer.Trim();
			Qitem.Quotation_No=quot.QuoteNo.Trim();
			Qitem.Q_Date=quot.Quotedate.Trim();
			Qitem.User_ID=userID;
			Qitem.PriceList="0";
			Qitem.SpecialReq =cod.Notes.ToString();
			Qitem.Note=cod.VelanTag.ToString() +";Notes: "+cod.Notes.ToString();
			if(lblstatus.Text.ToLower().Trim() =="true")
			{
				Qitem.UnitPrice ="0.00";
				Qitem.TotalPrice="0.00";
			}
			else
			{
				Qitem.UnitPrice=lbltotal.Text.ToString();
				Qitem.TotalPrice=Convert.ToString((Convert.ToDecimal(Qitem.UnitPrice)*Convert.ToDecimal(cod.Qty)));
			}
			string strTandem ="";
			if(PN.TandemDuplex=="TC") strTandem="YES";
			else strTandem="NO";
			//weight
			Qitem.Weight = "Approximate cylinder weight (does not include accessories)= "+CylWeight(PN.Series,PN.Bore_Size, PN.Rod_Diamtr, PN.CanisterNo, PN.Stroke,PN.FailMode,strTandem).ToString() +" LBS";
			//issue #233 start
			Qitem.Weight +=";The Cylinder Displacement = " + Cyl_Displacement(PN) + " in" + Convert.ToChar(0179).ToString();
			//issue #233 end
			return Qitem;
		}
		
		private decimal CylWeight_ML(coder PN)
		{
			DBClass db=new DBClass();
			decimal c4 = 0m;
		    decimal c1,c2,c3,c5,dc1 ,dc3,dctotal=0.00m;
				if(PN.Series.Trim()== "M" || PN.Series.Trim()== "ML" || PN.Series.Trim()== "L")
										{
											if(PN.Bore_Size.Trim() =="P" || PN.Bore_Size.Trim() =="R" || PN.Bore_Size.Trim() =="S" || PN.Bore_Size.Trim() =="T" || PN.Bore_Size.Trim() =="W" || PN.Bore_Size.Trim() =="X")
											{
												if(PN.DoubleRod.ToUpper().Trim() =="YES")
												{
													c1=db.SelectAddersPrice("DWeight","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													c2=db.SelectAddersPrice("DStroke","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										
													if(PN.Mount.Substring(0,2) =="T1" || PN.Mount.Substring(0,2) =="T2")
													{
														c5=db.SelectAddersPrice("SWeight1","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													else if(PN.Mount.Substring(0,2) =="E5" || PN.Mount.Substring(0,2) =="E6" || PN.Mount.Substring(0,2) =="T4")
													{
														c5=db.SelectAddersPrice("SWeight2","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													else if(PN.Mount.Substring(0,2) =="F5" || PN.Mount.Substring(0,2) =="F6" )
													{
														c5=db.SelectAddersPrice("SWeight3","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													else if(PN.Mount.Substring(0,2) =="P1" || PN.Mount.Substring(0,2) =="S2" || PN.Mount.Substring(0,2) =="S3")
													{
														c5=db.SelectAddersPrice("SWeight4","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													else
													{
														c5=db.SelectAddersPrice("SWeight1","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													c3=Convert.ToDecimal(PN.Stroke.Trim());
													c4= c1 + c5 + (c2 * c3);
													c4=Decimal.Round(c4,0);	
												}
												else
												{
													c2=db.SelectAddersPrice("SStroke","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										
													if(PN.Mount.Substring(0,2) =="T1" || PN.Mount.Substring(0,2) =="T2")
													{
														c5=db.SelectAddersPrice("SWeight1","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													else if(PN.Mount.Substring(0,2) =="E5" || PN.Mount.Substring(0,2) =="E6" || PN.Mount.Substring(0,2) =="T4")
													{
														c5=db.SelectAddersPrice("SWeight2","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													else if(PN.Mount.Substring(0,2) =="F5" || PN.Mount.Substring(0,2) =="F6" )
													{
														c5=db.SelectAddersPrice("SWeight3","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													else if(PN.Mount.Substring(0,2) =="P1" || PN.Mount.Substring(0,2) =="S2" || PN.Mount.Substring(0,2) =="S3")
													{
														c5=db.SelectAddersPrice("SWeight4","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													else
													{
														c5=db.SelectAddersPrice("SWeight1","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}	
													c3=Convert.ToDecimal(PN.Stroke.Trim());
													c4=c5 + (c2 * c3);
													c4=Decimal.Round(c4,0);								
												}
											}
											else if(PN.Bore_Size.Trim() !="P" && PN.Bore_Size.Trim() !="R" && PN.Bore_Size.Trim() !="S" && PN.Bore_Size.Trim() !="T" && PN.Bore_Size.Trim() !="W" && PN.Bore_Size.Trim() !="X")
											{
												if( PN.DoubleRod.ToUpper().Trim() =="YES" )
												{
													if(PN.Mount.Substring(0,1) =="P" || PN.Mount.Substring(0,1) =="T" || PN.Mount.Substring(0,1) =="E" || PN.Mount.Substring(0,1) =="S")
													{
														c1=db.SelectAddersPrice("DWeight2","WEB_SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													else
													{
														c1=db.SelectAddersPrice("DWeight1","SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													c2=db.SelectAddersPrice("DStroke","WEB_SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												}
												else
												{
													if(PN.Mount.Substring(0,1) =="P" || PN.Mount.Substring(0,1) =="T" || PN.Mount.Substring(0,1) =="E" || PN.Mount.Substring(0,1) =="S")
													{
														c1=db.SelectAddersPrice("SWeight2","WEB_SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													else
													{
														c1=db.SelectAddersPrice("SWeight1","SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													}
													c2=db.SelectAddersPrice("SStroke","WEB_SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												}
												c3=Convert.ToDecimal(PN.Stroke.Trim());
												c4=c1 + (c2 * c3);
												c4=Decimal.Round(c4,0);
											}
										}
		return c4;
		}
		
		private decimal CylWeight(string series, string bore,string rod,string mount,string stk,string strFailMode,string tandem)
		{
			decimal c1 = 0;
			decimal c2=0;
			decimal c3=0;
			decimal c4=0;
			decimal cs = 0;
			DBClass db=new DBClass();
			if( series.Trim()== "AS")
				{
					if(tandem.ToUpper().Trim()=="NO")
					{
						c1=db.SelectOneValueByAllinfo("SWeight1","SeriesAs_Weight_TableV1","BoreSize",bore.Trim(),"1","1");
						c2=db.SelectOneValueByAllinfo("SStroke","SeriesAs_Weight_TableV1","BoreSize",bore.Trim(),"1","1");
                        cs=db.SelectOneValueByAllinfo("Weight","SeriesAS_Weight_Canister_TableV1","BoreSize",mount.Trim(),"FailMode",strFailMode);
						c4=c1+c2*Convert.ToDecimal(stk.Trim()) +cs;
						c4=Decimal.Round(c4,0);
					}
					if(tandem.ToUpper().Trim()=="YES")
					{
						c1=db.SelectOneValueByAllinfo("DWeight1","SeriesAs_Weight_TableV1","BoreSize",bore.Trim(),"1","1");
						c2=db.SelectOneValueByAllinfo("DStroke","SeriesAs_Weight_TableV1","BoreSize",bore.Trim(),"1","1");
						cs=db.SelectOneValueByAllinfo("Weight","SeriesAS_Weight_Canister_TableV1","BoreSize",mount.Trim(),"FailMode",strFailMode);
						c4=c1+c2*Convert.ToDecimal(stk.Trim()) +cs;
						c4=Decimal.Round(c4,0);
					}
				}
			return c4;
		}
		//issue #137 end
		//asseriespn end
		private void BtnQuoteGenerate_Click(object sender, System.EventArgs e)
		{
			if(LblUpload.Text.Trim() !="" || lblup.Text.Trim() !="")
			{
				lblspecsheet.Text="";
				ArrayList lst =new ArrayList();
				lst =(ArrayList)Session["User"];
				DBClass db=new DBClass();
				ArrayList qlist=new ArrayList();
				//fetching
				//validation
				//issue #242 start
				string strCompanyID = "";
				if(lst[6]!=null) strCompanyID=lst[6].ToString();
				if(strCompanyID=="1017" || strCompanyID=="995") qlist=db.Select_DataFrom_ImportFile_All(Request.PhysicalApplicationPath+"dbfile/"+lblup.Text.Trim());
				else 
				//back
				qlist=db.Select_DataFrom_ImportFile(Request.PhysicalApplicationPath+"dbfile/"+lblup.Text.Trim());
				//issue #242 end
				if(qlist.Count >0)
				{
					ArrayList list1 = new ArrayList();
					ArrayList list2=new ArrayList();
					list1=db.SelectContactdetails(lst[6].ToString().Trim(),lst[5].ToString().Trim());
					list2=db.SelectCompanyterms(lst[5].ToString().Trim(),lst[6].ToString().Trim());
					Quotation quot =new Quotation();
					string company="";
					if(list1[16].ToString().Trim() =="0")
					{
						company ="Montreal";
					}
					else
					{
						company ="Mississauga";
					}
					quot.Office= company.Trim();
					quot.Customer=list1[0].ToString();
					quot.Contact=lst[0].ToString().Trim();
					quot.AttnTo1=lst[0].ToString().Trim();
					quot.AttnTo2="P:"+list1[12].ToString()+" F:"+list1[13].ToString();
					quot.AttnTo3=list1[14].ToString();
					quot.BillTo1=list1[0].ToString();
					quot.BillTo2=list1[1].ToString()+", "+list1[2].ToString();
					quot.BillTo3=list1[3].ToString()+", "+list1[4].ToString()+", "+list1[5].ToString();
					quot.ShipTo1=list1[6].ToString();
					quot.ShipTo2=list1[7].ToString()+", "+list1[8].ToString();
					quot.ShipTo3=list1[9].ToString()+", "+list1[10].ToString()+", "+list1[11].ToString();
					quot.QuoteNo=Qno(lst[3].ToString().Trim());
					quot.Quotedate=DateTime.Now.ToShortDateString();
					quot.ExpiryDate=DateTime.Now.AddDays(30).ToShortDateString();;
					quot.PrepairedBy=lst[0].ToString().Trim();
					quot.Code=list2[0].ToString();
					if(list2[4].ToString().Trim()=="0")
					{
						quot.Langu="English";
					}
					else
					{
						quot.Langu="French";
					}
					
					quot.Terms=list2[2].ToString();
					quot.Delivery="TBA";
					if(list2[1].ToString().Trim()=="0")
					{
						quot.Currency="CN $";
					}
					else
					{
						quot.Currency="US $";
					}
					quot.Note=TxtProjectNo.Text.ToString().Trim();
					quot.FinishedDate ="  ";
					quot.CowanQno= "  "; 
					quot.Items=quot.QuoteNo.ToString();
					quot.CompanyID=list2[5].ToString();
					string upload="";
					lblspecsheet.Text="";
					if(FileUploadSpec.Value.Trim() !="")
					{
						if(FileUploadSpec.PostedFile.ContentType.ToString() =="application/pdf")
						{										
							upload=UploadFileSpec(sender,e);
							lblstatus.Text="true";
							lblspecsheet.Text= Server.MapPath(".") + "/specsheet/"+upload.ToString();
						}
					}
					quot.SpecSheet=upload.Trim();
					ArrayList Implist =new ArrayList();
					//issue #229 start
					string strErrorAS="";
					//issue #229 end
					for(int i=0;i<qlist.Count;i++)
					{
						lblmgrp.Text="";
						lbltable.Text="";
						lblstatus.Text="";
						VelanClass cod=new VelanClass();
						cod=(VelanClass)qlist[i];
						//issue #220 start
						//cod.RodWiper
						//string strCompanyID = "";
						if(lst[6]!=null) strCompanyID=lst[6].ToString();
						if(strCompanyID=="998" || strCompanyID=="1015") cod.RodWiper="YES";
						else cod.RodWiper="";
						//issue #220 end
					
						coder PN=new coder();
						//issue #242 start
						PN.Units=cod.Units;
						//issue #242 end
						//asseriespn start
						if(cod !=null)
						{
							if(cod.Style=="SeriesA Double Acting")
							{
								PN.Series="A";
								PN.Stroke =String.Format("{0:##0.00}",Convert.ToDecimal(cod.Stroke.Trim()));
								PN.Rod_End ="A4";
								PN.Cushions="8";
								PN.CushionPosition="";
								PN.Port_Type="N";
								PN.Port_Pos="11";
								PN.Seal_Comp=db.SelectValue("Seal_Code","WEB_Seal_TableV1","Seal_Type",cod.Seals.ToString().Trim()).Trim();
								//issue #220 start
								if (cod.RodWiper!=null)
								{
									if(cod.RodWiper=="YES") PN.MetalScrapper="GT3";
									else PN.MetalScrapper="";
								}
								else if(cod.RodWiper==null) PN.MetalScrapper="";
								//issue #220 end
								if(cod.Paint.ToString().Trim() =="Epoxy Paint")
								{
									PN.Coating="C1";
								}
									//issue #234 start
								else if(cod.Paint.ToString().Trim() =="Standard Paint Rotork Red")
								{
									PN.Coating="C7";
									
								}
								else if(cod.Paint.ToString().Trim() =="Polyurethane Enamel Rotork Red")
								{
									PN.Coating="C8";
									
								}
									//issue #234 end 
								else
								{
									PN.Coating="";
								}
								PN.Specials="";
								if(cod.Style.ToString().Trim() =="Double Acting")
								{
									PN.Style="N";
								}							
								else if(cod.Style.ToString().Trim() =="Fail Close")
								{
									PN.Style="FC";
									PN.Specials="SPECIAL";
								}
								else if(cod.Style.ToString().Trim() =="Fail Open")
								{
									PN.Style="FO";
									PN.Specials="SPECIAL";
								}
								else
								{
									PN.Style="N";
								}
								if(cod.MinAirSupply.ToString().Trim() !="")
								{
									PN.MinAirSupply=cod.MinAirSupply.ToString().Trim();
								}
								string seatingthrust="";
								string packingfriction="";
								decimal s1=0.00m;
								decimal s2=0.00m;
								if(cod.BTO.ToString().Trim() !="")
								{
									PN.BTO=cod.BTO.ToString().Trim();
								}
								if(cod.RTO.ToString().Trim() !="")
								{
									PN.RTO=cod.RTO.ToString().Trim();
									s1=Convert.ToDecimal(cod.RTO.ToString().Trim());
								}
								if(cod.ETO.ToString().Trim() !="")
								{
									PN.ETO=cod.ETO.ToString().Trim();
								}
								if(cod.BTC.ToString().Trim() !="")
								{
									PN.BTC=cod.BTC.ToString().Trim();
								}
								if(cod.RTC.ToString().Trim() !="")
								{
									PN.RTC=cod.RTC.ToString().Trim();
									s2=Convert.ToDecimal(cod.RTC.ToString().Trim());
								}
								if(cod.ETC.ToString().Trim() !="")
								{
									PN.ETC=cod.ETC.ToString().Trim();
									seatingthrust=cod.ETC.ToString().Trim();
								}
								if(s1 >s2)
								{
									packingfriction=cod.RTO.ToString().Trim();
								}
								else
								{
									packingfriction=cod.RTC.ToString().Trim();
								}
								decimal bore=0.00m;
								if(cod.MinAirSupply.ToString().Trim() !="" &&  seatingthrust.ToString().Trim() !="" &&
									packingfriction.ToString().Trim() !="" && cod.SaftyFactor.ToString().Trim() !="" )
								{
									decimal dc1,dc2,dc3,dc4,dc5,b=0.00m;
									dc1=Convert.ToDecimal(cod.MinAirSupply.ToString().Trim());
									dc2=Convert.ToDecimal(seatingthrust.ToString().Trim());
									dc3=Convert.ToDecimal(packingfriction.ToString().Trim());
									dc4=Convert.ToDecimal(cod.SaftyFactor.ToString().Trim());
									dc4= 1 + (dc4/100);
									dc5= (( dc2 + dc3 ) *  dc4 ) / dc1;
									b= dc5 / Convert.ToDecimal(Math.PI);
									bore=Convert.ToDecimal(( Math.Sqrt(Convert.ToDouble(b))) * 2);
									bore=Decimal.Round(bore,2);
									string boresize="";
									if(bore > 0.00m && bore < 4.00m )
									{
										PN.Bore_Size="H";
										boresize="4";
									}
									else if(bore >= 4.00m && bore <= 5.00m )
									{
										PN.Bore_Size="K";
										boresize="5";
									}
									else if(bore >= 5.00m && bore <= 6.00m )
									{
										PN.Bore_Size="L";
										boresize="6";
									}
									else if(bore >= 6.00m && bore <= 7.00m )
									{
										PN.Bore_Size="M";
										boresize="7";
									}
									else if(bore >= 7.00m && bore <= 8.00m )
									{
										PN.Bore_Size="N";
										boresize="8";
									}
									else if(bore >= 8.00m && bore <= 10.00m )
									{
										PN.Bore_Size="P";
										boresize="10";
									}
									else if(bore >= 10.00m && bore <= 12.00m )
									{
										PN.Bore_Size="R";
										boresize="12";
									}
									else if(bore >= 12.00m && bore <= 14.00m )
									{
										PN.Bore_Size="S";
										boresize="14";
									}
									else if(bore >= 14.00m && bore <= 16.00m )
									{
										PN.Bore_Size="T";
										boresize="16";
									}
									else if(bore >= 16.00m && bore <= 18.00m )
									{
										PN.Bore_Size="W";
										boresize="18";
									}
									else if(bore >= 18.00m && bore <= 20.00m )
									{
										PN.Bore_Size="X";
										boresize="20";
									}
									else if(bore >= 20.00m && bore <= 22.00m )
									{
										PN.Bore_Size="A";
										boresize="22";
									}
									else if(bore >= 22.00m && bore <= 24.00m )
									{
										PN.Bore_Size="Y";
										boresize="24";
									}
									else if(bore >= 24.00m && bore <= 26.00m )
									{
										PN.Bore_Size="B";
										boresize="26";
									}
									else if(bore >= 26.00m && bore <= 28.00m )
									{
										PN.Bore_Size="F";
										boresize="28";
									}
									else if(bore >= 28.00m && bore <= 30.00m )
									{
										PN.Bore_Size="I";
										boresize="30";
									}
									else
									{
										PN.Bore_Size="Z";
									}
									decimal cc1,cc2,cc3,cc4=0.00m;
									if(boresize.Trim() !="")
									{
										cc1= Convert.ToDecimal(boresize.Trim());
										cc2=((cc1 * cc1) / 4)  * Convert.ToDecimal(Math.PI) ;
										cc3=cc2 * dc1;
										cc4=((cc3 /( dc2 + dc3 )) -1) * 100;
										PN.ActualSaftyFactor =(Decimal.Round(cc4,2)).ToString();
									}
									PN.Rod_Diamtr=db.SelectValue("Rod_Code","WEB_RodSerA_TableV1",PN.Bore_Size,"1").Trim();
								}
								else
								{
									PN.Bore_Size="Z";
									PN.Rod_Diamtr="Z";
								}
								string Portcode="";
								if(PN.Bore_Size.Trim() !="Z")
								{							
									Portcode=db.SelectOneValueFunction( PN.Bore_Size.Trim() ,"sp_Select_PortCode_WEB");
								}
								if(PN.Style.ToString().Trim() !="N")
								{
									PN.Bore_Size="Z";
									PN.Rod_Diamtr="Z";
								}
							
								if(cod.Mount.ToString().Trim() !="" && bore >0 && PN.Bore_Size.ToString().Trim() !="Z")
								{
									if(cod.Mount.ToString().Trim()=="No Mount")
									{
										PN.Mount="X0";
									}
									else if(cod.Mount.ToString().Trim()=="MSS Mount")
									{
										string mount="";
										decimal cylthrust=0.00m;
										decimal minair=0.00m;
										minair=Convert.ToDecimal(cod.MinAirSupply.ToString().Trim());
										cylthrust=(bore * bore) * 0.785m * minair;
										ArrayList blist=new ArrayList();
										ArrayList blist1=new ArrayList();
										ArrayList blist2=new ArrayList();
										blist=db.Select_Mount_Thrust("SELECT Mounts,MaxThrust FROM WEB_Velan_Mount_Bore_TableV1 Where Mounts like 'M%' and "+PN.Bore_Size.ToString().Trim()+"='"+PN.Bore_Size.ToString().Trim()+"' Order by MaxThrust ASC ");
										blist1=(ArrayList)blist[0];
										blist2=(ArrayList)blist[1];
										if(blist1.Count >0)
										{
											for(int j=0;j<blist1.Count;j++)
											{
												decimal tr=0.00m;
												tr=Convert.ToDecimal(blist2[j].ToString());
												if(cylthrust < tr)
												{
													if(mount.Trim() =="")
													{
														mount=	blist1[j].ToString().Trim();
														PN.Mount =	blist1[j].ToString().Trim();
													}
												}
											}
										}
										if(mount.Trim() =="")
										{
											PN.Mount="X0";
										}
									}
									else if(cod.Mount.ToString().Trim()=="ISO Mount")
									{
										string mount="";
										decimal cylthrust=0.00m;
										decimal minair=0.00m;
										minair=Convert.ToDecimal(cod.MinAirSupply.ToString().Trim());
										cylthrust=(bore * bore) * 0.785m * minair;
										ArrayList blist=new ArrayList();
										ArrayList blist1=new ArrayList();
										ArrayList blist2=new ArrayList();
										blist=db.Select_Mount_Thrust("SELECT Mounts,MaxThrust FROM WEB_Velan_Mount_Bore_TableV1 Where Mounts like 'I%' and "+PN.Bore_Size.ToString().Trim()+"='"+PN.Bore_Size.ToString().Trim()+"' Order by MaxThrust ASC ");
										blist1=(ArrayList)blist[0];
										blist2=(ArrayList)blist[1];
										if(blist1.Count >0)
										{
											for(int j=0;j<blist1.Count;j++)
											{
												decimal tr=0.00m;
												tr=Convert.ToDecimal(blist2[j].ToString());
												if(cylthrust < tr)
												{
													if(mount.Trim() =="")
													{
														mount=	blist1[j].ToString().Trim();
														PN.Mount =	blist1[j].ToString().Trim();
													}
												}
											}
										}
										if(mount.Trim() =="")
										{
											PN.Mount="X0";
										}
									}
									else if(cod.Mount.ToString().Trim()=="MX1 Mount")
									{
										PN.Mount="X1";
									}
									else  
									{
										PN.Mount="X0";
									}
								}
								else
								{
									PN.Mount="X0";
								}
						
								if(cod.Ratings.ToString().Trim() !="" )
								{
									if(cod.Ratings.ToString().Trim() =="Standard")
									{
										PN.Ratings="R1";
									}
									else if(cod.Ratings.ToString().Trim() =="Class 1 Div 1")
									{
										PN.Ratings="R2";
									}
									else if(cod.Ratings.ToString().Trim() =="Atex EExd Group II C")
									{
										PN.Ratings="R3";
									}
									else
									{
										PN.Ratings="R1";
									}
								}
								PN.SecRodDiameter="";
								PN.SecRodEnd="";
								PN.DoubleRod="No";
								if(cod.LimitSwitch.ToString().Trim() !="No" && cod.LimitSwitch.ToString().Trim() !="")
								{
									if(cod.LimitSwitch.ToString().Trim() =="Yes, Honeywell Mechanical with Standard Mounting Kit")
									{
										PN.MountKit="MNT1";
									}
									else if(cod.LimitSwitch.ToString().Trim() =="Yes, Honeywell Mechanical with Stainless Steel Mounting Kit")
									{
										PN.MountKit="MNT2";
									}
									else if(cod.LimitSwitch.ToString().Trim() =="Yes, GO Proxy with Standard Mounting Kit")
									{
										PN.MountKit="MNT3";
									}
									else if(cod.LimitSwitch.ToString().Trim() =="Yes, GO Proxy with Stainless Steel Mounting Kit")
									{
										PN.MountKit="MNT4";
									}
									if(PN.Ratings.Trim() == "R1")
									{
										PN.LimitSwitch="LSW1";
									}
									else if(PN.Ratings.Trim() == "R2")
									{
										PN.LimitSwitch="LSW2";
									}
									else if(PN.Ratings.Trim() == "R3")
									{
										PN.LimitSwitch="LSW3";
									}
									PN.SecRodDiameter="D"+PN.Rod_Diamtr.Trim()+"2";
									PN.SecRodEnd="RA4";
									PN.DoubleRod="Yes";
									PN.Specials="SPECIAL";
								}
								else
								{
									PN.LimitSwitch="";
								}
								if(cod.ManualOverride.ToString().Trim() =="Yes")
								{
									PN.ManualOverride="MO";
									PN.SecRodDiameter="D"+PN.Rod_Diamtr.Trim()+"2";
									PN.SecRodEnd="RA4";
									PN.DoubleRod="Yes";
									PN.Specials="SPECIAL";
								}
								else
								{
									PN.ManualOverride="";
									//								PN.SecRodDiameter="";
									//								PN.SecRodEnd="";
									//								PN.DoubleRod="No";
								}
								if(cod.PneumaticP.ToString().Trim() !="" || cod.PneumaticP.ToString().Trim() !="None")
								{
									if(cod.PneumaticP.ToString().Trim() =="Standard")
									{
										PN.Tubing="TF1";
										PN.FilterRegulator="FR1";
										PN.Specials="SPECIAL";
									}
									else if(cod.PneumaticP.ToString().Trim() =="Stainless Steel")
									{
										PN.Tubing="TF2";
										PN.FilterRegulator="FR2";
										PN.Specials="SPECIAL";
									}
									if(PN.Ratings.Trim() == "R1")
									{
										PN.Solenoid="VA1";
									}
									else if(PN.Ratings.Trim() == "R2")
									{
										if(Portcode.Trim() =="A")
										{
											PN.Solenoid="VA2";
										}
										else
										{
											PN.Solenoid="VA3";
										}
									}
									else if(PN.Ratings.Trim() == "R3")
									{
										if(Portcode.Trim() =="A")
										{
											PN.Solenoid="VA4";
										}
										else
										{
											PN.Solenoid="VA5";
										}
									}
								}
								if(cod.SaftyFactor.ToString().Trim() !="" )
								{
									PN.SaftyFactor=cod.SaftyFactor.ToString().Trim();
								}	
								else
								{
									PN.SaftyFactor="";
								}
								if(cod.ClosingTime.ToString().Trim() !="" )
								{
									PN.ClosingTime=cod.ClosingTime.ToString().Trim();
								}
								else
								{
									PN.ClosingTime="";
								}
                                //issue #220 start
								//backup
//								PN.PNO =PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+
//									PN.Rod_End.ToString().Trim()+PN.Cushions.ToString().Trim()+PN.CushionPosition.ToString().Trim()+PN.SecRodDiameter.ToString().Trim() + PN.SecRodEnd.ToString().Trim()+
//									PN.Seal_Comp.ToString().Trim()+PN.Port_Type.ToString().Trim()+PN.Port_Pos.ToString().Trim()+
//									PN.Mount.ToString().Trim()+PN.Stroke.ToString().Trim()+PN.Coating.ToString().Trim();
								//update
								PN.PNO =PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+
									PN.Rod_End.ToString().Trim()+PN.Cushions.ToString().Trim()+PN.CushionPosition.ToString().Trim()+PN.MetalScrapper.ToString().Trim()+PN.SecRodDiameter.ToString().Trim() + PN.SecRodEnd.ToString().Trim()+
									PN.Seal_Comp.ToString().Trim()+PN.Port_Type.ToString().Trim()+PN.Port_Pos.ToString().Trim()+
									PN.Mount.ToString().Trim()+PN.Stroke.ToString().Trim()+PN.Coating.ToString().Trim();
								//issue #220 end
								LblPartNo.Text=PN.PNO.ToString();
								if(PN.Series.Trim() =="A")
								{
									APricing(PN);
								}
								else
								{
									lbltotal.Text ="0.00";
									lblstatus.Text ="true";
								}
								try
								{
									string st="";
									if(Convert.ToDecimal(PN.Stroke.ToString()) > 120.00m)
									{
										st="For pricing please consult factory";
										lblstatus.Text="true";
									}
									string rd=PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim();
									string rodenddim ="";
									if(db.SelectRRD(rd).Trim() !="")
									{
										rodenddim =" KK = "+db.SelectRRD(rd);
									}
									string portsiz="";					
									string table="";
									if(PN.Series.Trim().Substring(0,1) =="A" ||PN.Series.Trim().Substring(0,1) =="S" )
									{
										table ="WEB_SeriesAPortSize_TableV1";
									}
									if(db.SelectPortSizeNo(PN.Bore_Size.Trim(),PN.Port_Type.Trim(),table.Trim()) !="")
									{
										portsiz ="#"+db.SelectPortSizeNo(PN.Bore_Size.Trim(),PN.Port_Type.Trim(),table.Trim());
									}
									QItems Qitem =new QItems();
									Qitem.ItemNo =quot.QuoteNo.ToString();
									Qitem.S_Code=PN.Series.Trim();
									Qitem.Series=db.SelectValue("Series_Name","WEB_Series_TableV1","Series_Code",PN.Series.ToString().Trim()).ToString();
									Qitem.S_Price="0.00";
									Qitem.B_Code=PN.Bore_Size.ToString().Trim();
									Qitem.Bore=db.SelectValue("Bore_Size","WEB_Bore_TableV1","Bore_Code",PN.Bore_Size.ToString().Trim()).ToString()+" Bore Size";
									Qitem.B_Price="0.00";
									Qitem.R_Code=PN.Rod_Diamtr.ToString().Trim();
									Qitem.Rod=db.SelectValue("Rod_Size","WEB_RodSerZ_TableV1","Rod_Code",PN.Rod_Diamtr.ToString().Trim())+" Rod Size";
									Qitem.R_Price="0.00";
									Qitem.Stroke_Code=PN.Stroke.ToString().Trim();
									Qitem.Stroke="Stroke = "+PN.Stroke.ToString()+"\""+st.ToString();
									Qitem.Stroke_Price="0.00";
									Qitem.M_code=PN.Mount.ToString().Trim();
									Qitem.Mount=db.SelectValue("Mount_Type","WEB_Mount"+PN.Series.Trim()+"_TableV1","Mount_Code",PN.Mount.ToString().Trim()).ToString();
									Qitem.M_Price="0.00";
									Qitem.RE_Code=PN.Rod_End.ToString().Trim();
									Qitem.RodEnd=db.SelectValue("RodEnd_Shape","WEB_RodEndKK_TableV1","RodEnd_Code",PN.Rod_End.ToString().Trim()).ToString()+rodenddim.ToString();
									Qitem.RE_Price="0.00";
									Qitem.Cu_Code=PN.Cushions.ToString().Trim();
									Qitem.Cushion=db.SelectValue("Cushion_type","WEB_Cushion_TableV1","Cushion_Code",PN.Cushions.ToString()).ToString();
									Qitem.Cu_Price="0.00";
									Qitem.CushionPos_Code=PN.CushionPosition.ToString().Trim();
									Qitem.CushionPos=db.SelectValue("CushionPos_Pos","WEB_CushionPos_TableV1","CushionPos_Code",PN.CushionPosition.ToString().Trim()).ToString();
									Qitem.CushionPos_Price="0.00";
									Qitem.Sel_Code=PN.Seal_Comp.ToString().Trim();
									Qitem.Seal=db.SelectValue("Seal_Type","WEB_Seal_TableV1","Seal_Code",PN.Seal_Comp.ToString().Trim()).ToString();
									Qitem.Sel_Price="0.00";
									Qitem.Port_Code=PN.Port_Type.ToString().Trim();
									Qitem.Port=portsiz.Trim()+" "+ db.SelectValue("PortType_Type","WEB_portType_TableV1","PortType_Code",PN.Port_Type.ToString().Trim()).ToString();
									Qitem.Port_Price="0.00";
									Qitem.PP_Code=PN.Port_Pos.ToString().Trim();
									Qitem.PortPos=db.SelectValue("PortPos_Position","WEB_PortPosition_TableV1","PortPos_Code",PN.Port_Pos.ToString().Trim()).ToString();
									Qitem.PP_Price="0.00";
									Qitem.Quantity=cod.Qty.ToString();
									Qitem.Discount="0";
					                //issue #242 start
									Qitem.Units=PN.Units;
									//issue #242 end
									Qitem.Cusomer_ID=quot.Customer.Trim();
									Qitem.Quotation_No=quot.QuoteNo.Trim();
									Qitem.Q_Date=quot.Quotedate.Trim();
									Qitem.User_ID=lst[0].ToString();
									Qitem.PriceList="0";
									Qitem.SpecialReq =cod.Notes.ToString();
									Qitem.Note=cod.VelanTag.ToString();
									string commadd="";

									if(Qitem.S_Code.Trim() !="")
									{
										commadd="WEB_Series"+Qitem.S_Code.Trim()+"_Velan_";
									}
									decimal sptotal=0.00m;
									if(PN.Specials !=null)
									{
										if(PN.Specials.ToString().ToUpper().Trim() =="SPECIAL")
										{
											decimal total =0.00m;
											decimal t1=0.00m;
											decimal t2=0.00m;
											bool result1=false;
											string sp ="";
											int tt=Convert.ToInt32(db.SelectLastSpNo());
											sp=(tt+1).ToString();
											PN.PNO ="Z"+PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim()+
												PN.Cushions.ToString().Trim()+PN.Mount.ToString().Trim()+"/Z"+sp.ToString();
											LblPartNo.Text ="Z"+PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim()+
												PN.Cushions.ToString().Trim()+PN.Mount.ToString().Trim()+"/Z"+sp.ToString();
											//specialstyle
											if(PN.Style !=null)
											{
												if(PN.Style.Trim() !="")
												{
													if(PN.Style.Trim() =="FC" || PN.Style.Trim() =="FO")
													{
														total =0.00m;
														if(total ==0)
														{
															result1 =true;
														}
														string des="";
														des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Style.Trim()).Replace("#",PN.MinAirSupply.Trim());
														string sp1 ="";
														int tt1=Convert.ToInt32(db.SelectLastSpNo());
														sp1=(tt1+1).ToString();
														db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Style.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"1");
														db.InsertSpecialCount(sp1.Trim());
													}
												}
											}
											//specialsolenoid
											if(PN.Solenoid !=null)
											{
												if(PN.Solenoid.Trim() !="")
												{
													t1 =db.SelectAddersPrice_Specials_Velan(PN.Solenoid.Trim(),"WEB_Velan_SpecialAdder_Pricing_TableV1",Portcode.ToString().Trim());
													total =t1; 
													sptotal +=t1;
													total=Decimal.Round(t1,2);
													if(total ==0)
													{
														result1 =true;
													}
													string des="";
													des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Solenoid.Trim());
													string prt="";
													prt=db.SelectValue("POrtSize","WEB_Velan_BorePort_TableV1","PortCode",Portcode.Trim());
													des= des.Replace("#",prt.Trim());
													string sp1 ="";
													int tt1=Convert.ToInt32(db.SelectLastSpNo());
													sp1=(tt1+1).ToString();
													db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Solenoid.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"2");
													db.InsertSpecialCount(sp1.Trim());						
												}
											}
											//specialfilterregulator
											if(PN.FilterRegulator !=null)
											{
												if(PN.FilterRegulator.Trim() !="")
												{
													t1 =db.SelectAddersPrice_Specials_Velan(PN.FilterRegulator.Trim(),"WEB_Velan_SpecialAdder_Pricing_TableV1",Portcode.ToString().Trim());
													total =t1; 
													sptotal +=t1;
													total=Decimal.Round(t1,2);
													if(total ==0)
													{
														result1 =true;
													}
													string des="";
													des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.FilterRegulator.Trim());
													string sp1 ="";
													int tt1=Convert.ToInt32(db.SelectLastSpNo());
													sp1=(tt1+1).ToString();
													db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.FilterRegulator.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"3");
													db.InsertSpecialCount(sp1.Trim());
												}
											}
											//specialtubing
											if(PN.Tubing !=null)
											{
												if(PN.Tubing.Trim() !="")
												{
													decimal t3=0.00m;
													t1 =db.SelectAddersPrice_Specials_Velan(PN.Tubing.Trim()+"Base","WEB_Velan_SpecialAdder_Pricing_TableV1",Portcode.ToString().Trim());
													t2 =db.SelectAddersPrice_Specials_Velan(PN.Tubing.Trim()+"PerInch","WEB_Velan_SpecialAdder_Pricing_TableV1",Portcode.ToString().Trim());
													t3=Convert.ToDecimal(PN.Stroke.ToString().Trim());
													total =t1 + (t2 * t3); 
													sptotal +=total;
													total=Decimal.Round(total,2);
													if(total ==0)
													{
														result1 =true;
													}	
													string des="";
													des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Tubing.Trim());
													string sp1 ="";
													int tt1=Convert.ToInt32(db.SelectLastSpNo());
													sp1=(tt1+1).ToString();
													db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Tubing.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"4");
													db.InsertSpecialCount(sp1.Trim());
												}
											}
											//specialmanualoverride
											if(PN.ManualOverride !=null)
											{
												if(PN.ManualOverride.Trim() !="")
												{
													total =0.00m;
													if(total ==0)
													{
														result1 =true;
													}
													string des="";
													des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.ManualOverride.Trim());
													string sp1 ="";
													int tt1=Convert.ToInt32(db.SelectLastSpNo());
													sp1=(tt1+1).ToString();
													db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.ManualOverride.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"5");
													db.InsertSpecialCount(sp1.Trim());
												}
											}
											//speciallimitswitch
											if(PN.LimitSwitch !=null)
											{
												if(PN.LimitSwitch.Trim() !="")
												{
													t1 =db.SelectAddersPrice_Specials_Velan(PN.LimitSwitch.Trim() ,"WEB_Velan_SpecialAdder_Pricing_TableV1",Portcode.ToString().Trim());
													t2 =db.SelectAddersPrice_Specials_Velan(PN.MountKit.Trim() ,"WEB_Velan_SpecialAdder_Pricing_TableV1",Portcode.ToString().Trim());
													total =t1+t2; 
													sptotal +=total;
													total=Decimal.Round(total,2);
													if(t2 ==0)
													{
														result1 =true;
													}	
													if(total ==0)
													{
														result1 =true;
													}	
													string des="";
													des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.LimitSwitch.Trim());
													des +=", "+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.MountKit.Trim());
													string sp1 ="";
													int tt1=Convert.ToInt32(db.SelectLastSpNo());
													sp1=(tt1+1).ToString();
													db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.LimitSwitch.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"6");
													db.InsertSpecialCount(sp1.Trim());
												}
											}
											if(result1 ==true)
											{
												lblstatus.Text= "true";
											}
										}
										else
										{
											Qitem.Special_ID="";
										}
									}
									else
									{
										Qitem.Special_ID="";
									}
									//appoptdoublerod
									decimal app=0;
									if(PN.DoubleRod.ToString().Trim() =="Yes")
									{
										if(PN.SecRodDiameter.ToString().Trim() !="")
										{
											string desc="";
											desc=db.SelectAppopts(PN.SecRodDiameter.ToString().Trim());
											db.InsertItemApplicationOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.SecRodDiameter.ToString(),desc.ToString(),"0.00");
										}
										if(PN.SecRodEnd.ToString().Trim() !="")
										{
											string desc="";
											desc=db.SelectAppopts(PN.SecRodEnd.ToString().Trim());
											db.InsertItemApplicationOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.SecRodEnd.ToString(),desc.ToString(),"0.00");
										}
										Qitem.ApplicationOpt_Id=quot.QuoteNo.Trim()+LblPartNo.Text.Trim();;
									}
									else
									{
										Qitem.ApplicationOpt_Id="";
									}
									//issue #220 start
									if(PN.MetalScrapper !=null)
									{
										if(PN.MetalScrapper.ToString().Trim() =="GT3")
										{
											string desc="";
											desc=db.SelectAppopts(PN.MetalScrapper.ToString().Trim());
											app =db.SelectAddersPrice("GT3","WEB_SeriesASCommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
											db.InsertItemApplicationOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.MetalScrapper.ToString(),desc.ToString(),app.ToString());
										}
										Qitem.ApplicationOpt_Id=quot.QuoteNo.Trim()+LblPartNo.Text.Trim();;
									}
									else
									{
										Qitem.ApplicationOpt_Id="";
									}
									//issue #220 end
									decimal pop=0.00m;
									if(PN.Coating.ToString().Trim() !="")
									{
										decimal total =0.00m;
										decimal t1 =0.00m;
										bool result1=false;
										//popoptcoating
										if(PN.Coating.ToString().Trim() =="C1")
										{
											t1=0;
											t1 =db.SelectAddersPrice_Specials_ByBore("C1","WEB_Specials_Velan_Pricing_TableV1",PN.Bore_Size.ToString().Trim());
											total =t1; 
											if(total ==0)
											{
												result1 =true;
											}
											string desc="";
											desc=db.SelectPopopts(PN.Coating.Trim());
											db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.Coating.ToString(),desc.ToString(),"0.00");
										}
										if(result1 ==true)
										{
											lblstatus.Text ="true";
										}
										//issue #234 start
										if(PN.Coating.ToString().Trim() =="C7")
										{
											t1=0;
											string desc="";
											desc=db.SelectPopopts(PN.Coating.Trim());
											db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.Coating.ToString(),desc.ToString(),"0.00");
										}
										if(result1 ==true)
										{
											lblstatus.Text ="true";
										}
										if(PN.Coating.ToString().Trim() =="C8")
										{
											t1=0;
											t1 =db.SelectAddersPrice_Specials_ByBore("C8","WEB_SeriesACommAdders_TableV1",PN.Bore_Size.ToString().Trim());
											total =t1; 
											if(total ==0)
											{
												result1 =true;
											}
											string desc="";
											desc=db.SelectPopopts(PN.Coating.Trim());
											db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.Coating.ToString(),desc.ToString(),"0.00");
										}
										if(result1 ==true)
										{
											lblstatus.Text ="true";
										}
										//issue #234 end
										pop =total;
										Qitem.PopularOpt_Id=quot.QuoteNo.Trim()+LblPartNo.Text.Trim();
									}
									else
									{
										Qitem.PopularOpt_Id="";
									}
									decimal dcc2=0.00m;
									dcc2 =Convert.ToDecimal(lbltotal.Text);
									lbltotal.Text=dcc2.ToString();
								
									decimal d1,d2=0.00m;
									d1 =Convert.ToDecimal(lbltotal.Text);
									string pindex="";
									pindex=db.SelectPriceIndex(PN.Series.Trim());
									decimal pindex1=0.00m;
									pindex1 =Convert.ToDecimal(pindex);
									pop =pop + pop * (pindex1 /100);
									//issue #220 start
									app =app + app * (pindex1 /100);
									//issue #220 end
									string spindex="";
									spindex=db.SelectPriceIndex("VSP");
									decimal spindex1=0.00m;
									spindex1 =Convert.ToDecimal(spindex);
									//issue #315 start
									if(PN.Series.ToString().Trim()=="A" || PN.Series.ToString().Trim()=="AC" || PN.Series.ToString().Trim()=="AT" || PN.Series.ToString().Trim()=="AS")
									{
										decimal discount=Convert.ToDecimal(db.SelectDiscount(lst[1].ToString().Trim(),"A"));
				                        app = app*(1 - (discount/100));
										pop = pop*(1 - (discount/100));
										sptotal= sptotal*(1 - (discount/100));
									}
									//issue #315 end
									sptotal =sptotal + sptotal * (spindex1 /100);
									//issue #220 start
									//backup
									//d2 =d1 +  pop + sptotal;
									//update
									d2 =d1 +  pop + app + sptotal;
									//issue #220 end
									lbltotal.Text=d2.ToString();
															
									decimal dc1 ,dc2,dc3=0.00m;
									dc1=Convert.ToDecimal(cod.Qty.ToString());
									dc2=Convert.ToDecimal(lbltotal.Text.ToString());
									dc3= dc1 * dc2 ;
									if(PN.ClosingTime.ToString().Trim() !="")
									{
										decimal cl1 ,cl2=0.00m;
										cl1=Convert.ToDecimal(PN.Stroke.ToString());
										cl2=Convert.ToDecimal(PN.ClosingTime.ToString());
										if(cl1 > cl2)
										{
											lblstatus.Text ="true";
										}
									}	
									if(lblspecsheet.Text.Trim() !="")
									{
										lblstatus.Text="true";
									}
									if(lblstatus.Text.ToLower().Trim() =="true")
									{
										Qitem.UnitPrice ="0.00";
										Qitem.TotalPrice="0.00";
									}
									//currency
									else
									{
										Qitem.UnitPrice=lbltotal.Text.ToString();
										Qitem.TotalPrice=dc3.ToString();
										//currency
										//issue #242 start
										decimal curencya=1.00m;
										if(quot.Currency.ToString().Trim() =="US $")	
										{	
											curencya=Convert.ToDecimal(db.SelectValue("USD_Q","Dollar_ExchangeRate_TableV1","Slno","1"));
											Qitem.UnitPrice=Convert.ToString(Convert.ToDecimal(Qitem.UnitPrice)*curencya);
											Qitem.TotalPrice=Convert.ToString(Convert.ToDecimal(Qitem.TotalPrice)*curencya);
											
										}
										//issue #242 end
									}
									Qitem.Special_ID="";
									Qitem.PartNo=LblPartNo.Text.ToString();
									if(PN.Series.Trim() !="")
									{
										string weight="";
										decimal c1,c2,c22,c3=0.00m;
										decimal c4=0.00m;
										if(PN.Series.Trim()== "A" )
										{
											//issue weight start backup
											//										if(PN.DoubleRod.ToUpper().Trim() =="YES")
											//										{
											//											c1=db.SelectAddersPrice("DWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
											//											c2=db.SelectAddersPrice("DStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
											//											c3=Convert.ToDecimal(PN.Stroke.Trim());
											//										}
											//										else
											//										{
											//											c1=db.SelectAddersPrice("SWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
											//											c2=db.SelectAddersPrice("SStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
											//											c3=Convert.ToDecimal(PN.Stroke.Trim());
											//										}
											//										c4=c1 + (c2 * c3);
											//										c4=Decimal.Round(c4,0);
											//issue weight end backup
											//issue weight start update
											if(PN.TandemDuplex ==null && PN.DoubleRod.ToUpper().Trim() =="NO")
											{
												c1=db.SelectAddersPrice("SWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												c2=db.SelectAddersPrice("SStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												c4=c1+c2*Convert.ToDecimal(PN.Stroke.Trim());
												c4=Decimal.Round(c4,0);
											}
											if(PN.TandemDuplex ==null && PN.DoubleRod.ToUpper().Trim() =="YES")
											{
												c1=db.SelectAddersPrice("DWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												c2=db.SelectAddersPrice("DStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												c4=c1+c2*Convert.ToDecimal(PN.Stroke.Trim());
												c4=Decimal.Round(c4,0);
											}
											if(PN.TandemDuplex !=null && PN.DoubleRod.ToUpper().Trim() =="NO")
											{
												c1=db.SelectAddersPrice("SWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												c2=db.SelectAddersPrice("SStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												c22=db.SelectAddersPrice("DStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												c4=c1*1.75m+(c2+c22)*Convert.ToDecimal(PN.Stroke.Trim());
												c4=Decimal.Round(c4,0);
											}
											if(PN.TandemDuplex !=null && PN.DoubleRod.ToUpper().Trim() =="YES")
											{
												c1=db.SelectAddersPrice("DWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												c22=db.SelectAddersPrice("DStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												c4=c1*1.75m+2m*c22*Convert.ToDecimal(PN.Stroke.Trim());
												c4=Decimal.Round(c4,0);
											}
											//issue weight end update
										}
										if(c4 !=0)
										{
											//weight
											weight="Approximate cylinder weight (does not include accessories)= "+c4.ToString()+" LBS";
											//issue #233 start
											weight +=";The Cylinder Displacement = " + Cyl_Displacement(PN) + " in" + Convert.ToChar(0179).ToString();
											//issue #233 end
			
										}
										else
										{
											weight="";
										}
										Qitem.Weight=weight.ToString();
									}
									string strd="";
									//qiteminsert
									//metrica
									strd= db.InsertItems(Qitem);
									if(strd.ToString().Trim() =="1")
									{
										if(PN.Style !=null)
										{
											if(PN.Style.ToString().Trim() !="")
											{
												Opts opts=new Opts();
												string ff="";
												if(PN.Style.Trim() =="FC")
												{
													ff="Style: Fail Close\r\n";
												}
												else if(PN.Style.Trim() =="FO")
												{
													ff="Style: Fail Open\r\n";
												}
												else
												{
													ff="Style: Double Acting \r\n";
												}
												//										if(PN.ValveSize !=null)
												//										{
												//											ff +="  Valve Size: "+PN.ValveSize.ToString().Trim()+"\r\n";
												//										}
												//											if(cod.LinearPressure !=null)
												//											{
												//												ff +="  Line Pressure : "+cod.LinearPressure.ToString().Trim()+"\r\n";
												//											}
												if(cod.MinAirSupply !=null)
												{
													ff +="  Min Air Supply : "+PN.MinAirSupply.ToString().Trim()+"\r\n";
												}
												if(cod.BTO !=null)
												{
													ff +="  Break to open : "+PN.BTO.ToString().Trim()+"\r\n";
												}
												if(cod.RTO !=null)
												{
													ff +="  Run to open : "+PN.RTO.ToString().Trim()+"\r\n";
												}
												if(cod.ETO !=null)
												{
													ff +="  End to open : "+PN.ETO.ToString().Trim()+"\r\n";
												}
												if(cod.BTC !=null)
												{
													ff +="  Break to close : "+PN.BTC.ToString().Trim()+"\r\n";
												}
												if(cod.RTC !=null)
												{
													ff +="  Run to close : "+PN.RTC.ToString().Trim()+"\r\n";
												}
												if(cod.ETC !=null)
												{
													ff +="  End to close : "+PN.ETC.ToString().Trim()+"\r\n";
												}
												if(cod.SaftyFactor !=null)
												{
													ff +="  Requested Safety Factor : "+cod.SaftyFactor.ToString().Trim()+"\r\n";
												}
												if(PN.ActualSaftyFactor !=null)
												{
													ff +="  Actual Safety Factor : "+PN.ActualSaftyFactor.ToString().Trim()+"\r\n";
												}
												if(cod.Ratings !=null)
												{
													ff +="  Ratings : "+cod.Ratings.ToString().Trim()+"\r\n";
												}
												if(cod.ClosingTime !=null)
												{
													if(cod.ClosingTime.ToString().Trim() !="")
													{
														ff +="  Closing Time : "+cod.ClosingTime.ToString().Trim()+"\r\n";
													}
												}
												if(cod.Notes !=null)
												{
													if(cod.Notes.ToString().Trim() !="")
													{
														ff +="  Notes : "+cod.Notes.ToString().Trim()+"\r\n";
													}
												}
												opts.Opt =ff.Trim();
												opts.Code=quot.QuoteNo.Trim();
												string temp=db.Insertcomments(opts);
											}
										}
										Implist.Add(lblstatus.Text.ToString().Trim());
								
									}
								}
								catch( Exception ex)
								{
									string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
									s=s.Replace("'"," ");
									LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
								}
							}
								//current
							else if(cod.Style=="SeriesAS Fail Close" || cod.Style=="SeriesAS Fail Open")
							{
								//issue #229 start
								
								try
								{
								//issue #229 end	
									PN=new coder();
									PN = ASSeries_PN( cod );
									string ind=db.SelectPriceIndex("AS").ToString();
									ASPricing(PN);
									decimal sptotal=0.00m;
									if(PN.Specials=="SPECIAL") sptotal=ASSeries_SP(PN, quot);
									QItems Qitem = ASSeries_QItem(PN,quot, cod, lst[0].ToString());
									ASSeries_Pop(PN,quot,Qitem);
									string strd="";
									//qiteminsert
									//								if(cod.Notes!="")
									//								AS_Insertcomments(quot.QuoteNo, cod.Notes);
									if(lblstatus.Text.ToLower().Trim() =="true")
									{
										Qitem.UnitPrice ="0.00";
										Qitem.TotalPrice="0.00";
									}
									//currency
									else
									{
										lbltotal.Text=Convert.ToString( Convert.ToDecimal(lbltotal.Text)*(1+Convert.ToDecimal(Convert.ToDecimal(ind)/100m)));
										//issue #315 start
										decimal discount=Convert.ToDecimal(db.SelectDiscount(lst[1].ToString().Trim(),"[AS]"));
										lbltotal.Text=Convert.ToString( Convert.ToDecimal(lbltotal.Text)*(1-discount/100.00m));
										//issue #315 end
										Qitem.UnitPrice=lbltotal.Text;
										Qitem.TotalPrice=Convert.ToString(Convert.ToDecimal(Qitem.Quantity)*Convert.ToDecimal(Qitem.UnitPrice));
										//currency
										//issue #242 start
										decimal curency=1.00m;
										if(quot.Currency.ToString().Trim() =="US $")	
										{	
										    curency=Convert.ToDecimal(db.SelectValue("USD_Q","Dollar_ExchangeRate_TableV1","Slno","1"));
											Qitem.UnitPrice=Convert.ToString(Convert.ToDecimal(Qitem.UnitPrice)*curency);
											Qitem.TotalPrice=Convert.ToString(Convert.ToDecimal(Qitem.TotalPrice)*curency);
											
										 }
										//issue #242 end
									}
									strd= db.InsertItems(Qitem);
									//issue #229
									Implist.Add(lblstatus.Text.ToString().Trim());
									//issue #229
								//issue #229 start
								}
								catch(Exception ex)
								{
									strErrorAS+=ex.Message + " for item : " +(i+1).ToString()+ ";";
									lblup.Text="";
									LblUpload.Text="";
									lblpage.Text="";
									LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+strErrorAS+ "')</script>";
									//throw;
								}
								//issue #229 end
			

							}
							else if(cod.Style=="SeriesML")
							{
								PN=new coder();
								PN = MLSeries_PN( cod );
								string ind=db.SelectPriceIndex("ML").ToString();
								MLPricing(PN);
								decimal sptotal=0.00m;
								if(PN.Specials=="SPECIAL") sptotal=ASSeries_SP(PN, quot);
								QItems Qitem = MLSeries_QItem(PN,quot, cod, lst[0].ToString());
								MLSeries_Pop(PN,quot,Qitem);
								MLSeries_App(PN,quot,Qitem);
								decimal discount =0.00m;
								decimal qty=0;
								qty=Convert.ToDecimal(PN.Qty.ToString());
								if(qty >0 && qty <=2)
								{
									discount=35;
								}
								else if(qty >=3 && qty <=5)
								{
									discount=37;
								}
								else if(qty >=6 && qty <=10)
								{
									discount=40;
								}
								else if(qty >10)
								{
									discount=45;
								}
								discount=0;
								string strd="";
								//qiteminsert
								//								if(cod.Notes!="")
								//								AS_Insertcomments(quot.QuoteNo, cod.Notes);
								if(lblstatus.Text.ToLower().Trim() =="true")
								{
									Qitem.UnitPrice ="0.00";
									Qitem.TotalPrice="0.00";
								}
								else
								{
									//indexissue
									lbltotal.Text=Convert.ToString( Convert.ToDecimal(lbltotal.Text)*(1+Convert.ToDecimal(Convert.ToDecimal(ind)/100m))*(1-Convert.ToDecimal(Convert.ToDecimal(discount)/100m)));
									Qitem.UnitPrice=lbltotal.Text;
									Qitem.TotalPrice=Convert.ToString(Convert.ToDecimal(Qitem.Quantity)*Convert.ToDecimal(Qitem.UnitPrice));
								}
								strd= db.InsertItems(Qitem);
			

							}
							//asseriespn
						}
						
					}
					bool pricestatus=true;
					for(int p=0;p<Implist.Count;p++)
					{
						if(Implist[p].ToString().Trim() =="true")
						{
							pricestatus=false;
						}
					}
					if(pricestatus ==true)
					{
						quot.Finish="1";
						string str =db.SelectCustomerQno(quot.QuoteNo.ToString().Trim());
						if(str.ToString() !="")
						{
							string s =db.UpdateCustomerQuote(quot,str.Trim());
						}
						else
						{
							string s =db.InsertCustomerQuote(quot);
						}
						db.UpdateCustomerQuote_UpLoadFile(lblup.Text.Trim(),str.ToString());
						lblstatus.Text="";
						Response.Redirect("ManageQ.aspx?id="+quot.QuoteNo.Trim()+"&pn=1234");		
					}
					else
					{
						PSend.Visible=true;
						Session["Quote"] = quot;
						quot.Finish="0";
						if(TxtProjectNo.Text.Trim() =="")
						{
							quot.Note ="This Quote require assitance from the factory.Please Contact Cowan Dynamics";
						}
						string str =db.SelectCustomerQno(quot.QuoteNo.ToString().Trim());
						if(str.ToString() !="")
						{
							string s =db.UpdateCustomerQuote(quot,str.Trim());
						}
						else
						{
							string s =db.InsertCustomerQuote(quot);
						}
						db.UpdateCustomerQuote_UpLoadFile(lblup.Text.Trim(),str.ToString());
						lblstatus.Text="";
					}
				}
				else
				{
					lblup.Text="";
					LblUpload.Text="";
					lblpage.Text="";
					LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Please review your file!')</script>";
				}
			}
			else
			{
				lblup.Text="";
				LblUpload.Text="";
				lblpage.Text="";
				LblView.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Please Upload a file')</script>";
			}
		}

		private void LBSend_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(Page.IsValid)
				{
					Quotation quote=new Quotation();
					quote =(Quotation)Session["Quote"];
					ArrayList lst1 =new ArrayList();
					lst1 =(ArrayList)Session["User"];
					MailMessage mail=new MailMessage();
					SmtpMail.SmtpServer ="k2smtpout.secureserver.net"; 
					mail.From= "admin@cowandynamics.com";
					mail.To = "dtaranu@cowandynamics.com";
					mail.Bcc="admin@cowandynamics.com";
					mail.Cc="jbehara@cowandynamics.com";
					mail.BodyFormat =MailFormat.Html;
					mail.Subject=   "["+DDLPriority.SelectedItem.Text.Trim() +"] New Quote from "+quote.Customer.Trim();
					if(DDLPriority.SelectedIndex ==0)
					{
						mail.Priority =MailPriority.Normal;
					}
					else if(DDLPriority.SelectedIndex ==1)
					{
						mail.Priority =MailPriority.High;
					}
					mail.Body =						
						"<hr color='#FF0000'>Bonjour, <br> "+lst1[0].ToString()+" a une nouvelle demande de prix:<br>"
						+"<p><TABLE id='Table1' borderColor='#0000ff' cellSpacing='1' cellPadding='1' align='left' border='1'>"
						+"<TR><TD noWrap>Date de demande :</TD><TD noWrap>"+quote.Quotedate.Trim()+"</TD></TR>"
						+"<TR><TD noWrap>No de demande :</TD><TD noWrap>"+quote.QuoteNo.ToString().Trim()+"</TD></TR>"
						+"<TR><TD noWrap>Special Request Note :</TD><TD noWrap>"+TxtSpecialReq.Text.Trim()+"</TD></TR></TABLE></p>"
						+"<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>Clic sur le lien ci-dessous � r�pondre: <a href='http://172.16.0.252/'>Quote System</a>"
						+"<br><br><hr> Merci <br>I-Cylinder<br><hr color='#FF0000'><br>"								
						+"<hr color='#FF0000'>Hi, <br> "+lst1[0].ToString()+" has entered a new request for a quote:<br>"
						+"<p><TABLE id='Table1' borderColor='#0000ff' cellSpacing='1' cellPadding='1' align='left' border='1'>"
						+"<TR><TD noWrap>Quote Date :</TD><TD noWrap>"+quote.Quotedate.Trim()+"</TD></TR>"
						+"<TR><TD noWrap>Quote No :</TD><TD noWrap>"+quote.QuoteNo.ToString().Trim()+"</TD></TR>"
						+"<TR><TD noWrap>Special Request Note :</TD><TD style='width:200' noWrap>"+TxtSpecialReq.Text.Trim()+"</TD></TR></TABLE></p>"
						+"<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>Please log into <a href='http://172.16.0.252/'>Quote System</a> to complete the Quote."
						+"<br><br><hr> Thanks <br>I-Cylinder<br><hr color='#FF0000'>";
					if(lblspecsheet.Text.Trim() !="")
					{
						MailAttachment attach=new MailAttachment(lblspecsheet.Text.ToString().Trim());
						mail.Attachments.Add(attach);
					}
					//issue #582 start
					mail.Body += csSignature.Get_Admin();
					//issue #582 end
					SmtpMail.Send(mail);
					Response.Redirect("Result.aspx?id="+quote.QuoteNo.Trim());
				}
			}
			catch (Exception ex)
			{
				lblerr.Text=ex.Message;
			}
		}
	}
}
