using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for EV_Series_Select.
	/// </summary>
	public class EV_Series_Select : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.LinkButton BtnNext;
		protected System.Web.UI.WebControls.RadioButtonList RBLSeries;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label18;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.RadioButtonList RBLStyle;
		coder cd1=new coder();
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["User"] !=null)
			{
				if(! IsPostBack)
				{
					if(Session["Coder"] !=null)
					{
						Session["Coder"] =null;
					}
					ListItem litem=new ListItem();
					litem =new ListItem((String.Format("Series A<br /><img  align='middle' alt='A - Pneumatic 150 PSI Linear Valve Actuator' src='{0}'>", "images\\A.jpg")), "A");
					RBLSeries.Items.Add(litem);
					litem=new ListItem();
					litem =new ListItem((String.Format("Series AT<br /><img  align='middle' alt='AT - Pneumatic 150 PSI  Actuator With Transducer' src='{0}'>", "images\\AT.jpg")), "AT");
					RBLSeries.Items.Add(litem);
					RBLSeries.SelectedIndex =0;
					if(Session["Coder"] !=null)
					{
						cd1=(coder)Session["Coder"];
						if(cd1.Series !=null && cd1.Series !="")
						{
							RBLSeries.SelectedValue = cd1.Series.Trim(); 
						}
						if(cd1.FrameStyle !=null)
						{
							RBLStyle.SelectedValue=cd1.FrameStyle.ToString().Trim();
						}
					}
				}
			}
			else
			{
				Response.Redirect("Login.aspx");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.BtnNext.Click += new System.EventHandler(this.BtnNext_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void BtnNext_Click(object sender, System.EventArgs e)
		{
			if(RBLSeries.SelectedIndex >- 1 )
			{
				Session["Coder"] =null;
				cd1.Series =RBLSeries.SelectedItem.Value.ToString().Trim();
				cd1.FrameStyle=RBLStyle.SelectedItem.Value.ToString();
				Session["Coder"] =cd1;
				Response.Redirect("EV_SeriesA.aspx");
			}
		}
	}
}
