<%@ Page language="c#" Codebehind="EV_SeriesAT_Trans.aspx.cs" AutoEventWireup="false" Inherits="iCylinderV1.EV_SeriesAT_Page1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>EV_SeriesAT_Page1</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE style="Z-INDEX: 0; WIDTH: 1000px; HEIGHT: 487px" id="Table1" border="0" cellSpacing="0"
				cellPadding="0" width="1000" bgColor="#d3d3d3">
				<TR>
					<TD height="20" width="50"></TD>
					<TD height="20" align="center">
						<asp:label id="Label2" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
							Font-Size="Small" ForeColor="Black">Transducer Options</asp:label></TD>
					<TD height="20" width="50"></TD>
				</TR>
				<TR>
					<TD width="50"></TD>
					<TD bgColor="#dcdcdc">
						<TABLE id="Table2" border="1" cellSpacing="0" borderColor="silver" cellPadding="0" width="902">
							<TR>
								<TD align="center">
									<asp:RadioButtonList id="RBLTrans" runat="server" Font-Size="Smaller" RepeatColumns="3" RepeatDirection="Horizontal"></asp:RadioButtonList></TD>
							</TR>
							<TR>
								<TD height="20" align="center">
									<asp:label style="Z-INDEX: 0" id="Label1" runat="server" Font-Size="Smaller">Select Positioner Type</asp:label></TD>
							</TR>
							<TR>
								<TD height="20" align="center">
									<asp:radiobuttonlist style="Z-INDEX: 0" id="RBLRemote" runat="server" Font-Size="9pt" AutoPostBack="True"
										Height="114px">
										<asp:ListItem Value="R">Remote Positioner</asp:ListItem>
										<asp:ListItem Value="P">PMV D3 Positioner (Supplied by EVR installed by Cowan)</asp:ListItem>
										<asp:ListItem Value="C">Custom Positioner</asp:ListItem>
										<asp:ListItem Value="N" Selected="True">None</asp:ListItem>
									</asp:radiobuttonlist>
									<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0">
									</TABLE>
									<asp:textbox style="Z-INDEX: 0" id="Txtcustom" runat="server" Font-Size="XX-Small" Width="294px"
										Height="22px" Visible="False"></asp:textbox></TD>
							</TR>
							<TR>
								<TD height="20" align="center">
									<asp:RadioButtonList style="Z-INDEX: 0" id="RBLTubing" runat="server" Font-Size="Smaller">
										<asp:ListItem Value="T1" Selected="True">Copper Tubing &amp; Brass Fittings</asp:ListItem>
										<asp:ListItem Value="T2">Stainless Steel Tubing &amp; Fittings</asp:ListItem>
									</asp:RadioButtonList></TD>
							</TR>
						</TABLE>
					</TD>
					<TD width="50"></TD>
				</TR>
				<TR>
					<TD height="30" width="50" align="right">
						<asp:LinkButton id="LBBack" runat="server" Font-Size="Smaller" Font-Bold="True">Back</asp:LinkButton></TD>
					<TD bgColor="#dcdcdc" height="30" align="center">
						<asp:label id="lblxi" runat="server" Font-Size="Smaller">Resistance</asp:label>
						<asp:textbox id="Txtxi" runat="server" Font-Size="XX-Small" AutoPostBack="True" Width="30px"
							MaxLength="2">10</asp:textbox>
						<asp:label id="omega" runat="server" BackColor="Transparent" Width="8px" Height="19">&Omega;</asp:label>
						<asp:label id="lbrange" runat="server" Font-Size="Smaller" ForeColor="Red">(5 < range> 99)</asp:label></TD>
					<TD height="30" width="50">
						<asp:LinkButton id="BtnNext" runat="server" Font-Size="Smaller" Font-Bold="True">Next</asp:LinkButton></TD>
				</TR>
				<TR>
					<TD height="20" width="50"></TD>
					<TD height="20" align="center">
						<asp:label id="lblseries" runat="server" BackColor="Transparent" BorderColor="Transparent"
							Font-Size="XX-Small" ForeColor="Red"></asp:label></TD>
					<TD height="20" width="50"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
