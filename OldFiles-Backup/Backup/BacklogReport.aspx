<%@ Page language="c#" Codebehind="BacklogReport.aspx.cs" AutoEventWireup="false" Inherits="iCylinderV1.BacklogReport" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>BacklogReport</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE style="Z-INDEX: 0; WIDTH: 1000px; HEIGHT: 487px" id="Table1" border="0" cellSpacing="0"
				cellPadding="0" width="1000" bgColor="lightgrey">
				<TR>
					<TD height="20" width="1"></TD>
					<TD height="20" align="center"></TD>
					<TD height="20" width="1"></TD>
				</TR>
				<TR>
					<TD width="1"></TD>
					<TD bgColor="gainsboro" align="center">
						<TABLE id="Table2" border="0" cellSpacing="1" cellPadding="1" align="center" width="970">
							<TBODY>
								<TR>
									<TD>
										<div align="center"><asp:label id="Label1" runat="server" Font-Size="9pt">Customized for:</asp:label><br>
											<asp:image id="ImgLogo" runat="server"></asp:image></div>
									</TD>
								</TR>
								<TR>
									<TD align="center">
										<TABLE style="Z-INDEX: 0" id="Table3" border="0" cellSpacing="1" cellPadding="1">
											<TR>
												<TD>
													<asp:label style="Z-INDEX: 0" id="Label2" runat="server" Font-Size="9pt">Search By:</asp:label></TD>
												<TD>
													<asp:dropdownlist style="Z-INDEX: 0" id="DDLSearch" runat="server" Font-Size="8pt" Width="125px" AutoPostBack="True">
														<asp:ListItem Value="PO">Purchase Order</asp:ListItem>
														<asp:ListItem Value="PN">Project Name</asp:ListItem>
														<asp:ListItem Value="CR">Customer Ref</asp:ListItem>
														<asp:ListItem Value="PA">Cowan Part No</asp:ListItem>
													</asp:dropdownlist></TD>
												<TD>
													<asp:textbox style="Z-INDEX: 0" id="TxtSearch" runat="server" Font-Size="9pt" Width="144px"></asp:textbox></TD>
												<TD align="center">
													<asp:linkbutton style="Z-INDEX: 0" id="LbSearch" runat="server" Font-Size="9pt" Width="48px" Height="20px"
														BorderStyle="Outset" BorderWidth="2px" BackColor="Silver" BorderColor="WhiteSmoke" ForeColor="Black">Search</asp:linkbutton></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
								<TR>
									<TD align="center"><asp:datagrid style="Z-INDEX: 0" id="DGBacklog" runat="server" Font-Size="8pt" BorderColor="White"
											BackColor="White" AutoGenerateColumns="False" GridLines="None" CellPadding="0" CellSpacing="1" BorderWidth="2px"
											OnItemCommand="DGArchive_Command" BorderStyle="Ridge" Height="144px" Width="100%" Caption="Back Log Report"
											CaptionAlign="Left" AllowPaging="True" OnPageIndexChanged="DGBacklog_PageChanger">
											<FooterStyle ForeColor="Black" BackColor="#C6C3C6"></FooterStyle>
											<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#9471DE"></SelectedItemStyle>
											<ItemStyle ForeColor="Black" BackColor="#DEDFDE"></ItemStyle>
											<HeaderStyle Font-Bold="True" ForeColor="#E7E7FF" BackColor="#404040"></HeaderStyle>
											<Columns>
												<asp:BoundColumn DataField="Code" HeaderText="Code">
													<HeaderStyle HorizontalAlign="Left" Width="40px" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="PoNo" HeaderText="P/O No">
													<HeaderStyle HorizontalAlign="Center" Width="80px" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
												<asp:ButtonColumn DataTextField="PartNo" HeaderText="Part No" CommandName="Dwg_Command">
													<HeaderStyle HorizontalAlign="Center" Width="200px" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:ButtonColumn>
												<asp:BoundColumn DataField="Qty" HeaderText="Qty">
													<HeaderStyle HorizontalAlign="Center" Width="30px" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="UnitPrice" HeaderText="Unit Price">
													<HeaderStyle HorizontalAlign="Center" Width="70px" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="SoNo" HeaderText="S/O No">
													<HeaderStyle HorizontalAlign="Center" Width="60px" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="EntryDate" HeaderText="Entry Date" DataFormatString="{0:MM/dd/yyyy}">
													<HeaderStyle HorizontalAlign="Center" Width="80px" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="DeliveryDate" HeaderText="Delivery Date" DataFormatString="{0:MM/dd/yyyy}">
													<HeaderStyle HorizontalAlign="Center" Width="80px" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="CustomerRef" HeaderText="Customer Ref">
													<HeaderStyle HorizontalAlign="Center" Width="150px" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="ProjectRef" HeaderText="Project No">
													<HeaderStyle HorizontalAlign="Center" Width="150px" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="OrderStatus" HeaderText="Order Status">
													<HeaderStyle HorizontalAlign="Center" Width="120px" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="PartNo" HeaderText="P/O No">
													<HeaderStyle HorizontalAlign="Center" Width="80px" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
											</Columns>
											<PagerStyle HorizontalAlign="Left" ForeColor="Black" BackColor="White" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<TR>
									<TD><div><asp:Label id="Lbllinks" runat="server" Font-Size="9pt"></asp:Label></div>
									</TD>
					</TD>
				</TR>
			</TABLE>
			</TD>
			<TD width="1"></TD>
			</TR>
			<TR>
				<TD height="30" width="1" align="right"></TD>
				<TD bgColor="#dcdcdc" height="30" align="center">
					<asp:Label style="Z-INDEX: 0" id="CID1" runat="server" Visible="False"></asp:Label>
					<asp:Label style="Z-INDEX: 0" id="CID2" runat="server" Visible="False"></asp:Label></TD>
				<TD height="30" width="1"></TD>
			</TR>
			<TR>
				<TD height="20" width="1"></TD>
				<TD height="20" align="center">
					<asp:Label id="err" runat="server"></asp:Label></TD>
				<TD height="20" width="1"></TD>
			</TR>
			</TBODY></TABLE></form>
	</body>
</HTML>
