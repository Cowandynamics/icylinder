using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
namespace iCylinderV1
{
	/// <summary>
	/// Summary description for Manage_Rod_Acc.
	/// </summary>
	public class Manage_Rod_Acc : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.LinkButton BtnNext;
		protected System.Web.UI.WebControls.LinkButton LBHome;
		protected System.Web.UI.WebControls.Image ImgLinearcoupler;
		protected System.Web.UI.WebControls.TextBox TxtLinearcoupler;
		protected System.Web.UI.WebControls.Label Label24;
		protected System.Web.UI.WebControls.DropDownList DDLLinearcoupler;
		protected System.Web.UI.WebControls.CheckBox CBLinearcoupler;
		protected System.Web.UI.WebControls.Panel PanelLinearCoupler;
		protected System.Web.UI.WebControls.Image ImgSphericalrodeye;
		protected System.Web.UI.WebControls.TextBox TxtSphericalrodeye;
		protected System.Web.UI.WebControls.Label Label20;
		protected System.Web.UI.WebControls.DropDownList DDLSphericalrodeye;
		protected System.Web.UI.WebControls.CheckBox CBSphericalrodeye;
		protected System.Web.UI.WebControls.Panel PanelSphericalRodEye;
		protected System.Web.UI.WebControls.Image ImgSphericalpp;
		protected System.Web.UI.WebControls.TextBox TxtSphericalpp;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.DropDownList DDLSphericalpp;
		protected System.Web.UI.WebControls.CheckBox CBSpericalpp;
		protected System.Web.UI.WebControls.Panel PanelSphericalPivotpin;
		protected System.Web.UI.WebControls.Image ImgClevisbracket;
		protected System.Web.UI.WebControls.TextBox TxtClevisbracket;
		protected System.Web.UI.WebControls.Label Label18;
		protected System.Web.UI.WebControls.DropDownList DDLClevisbracket;
		protected System.Web.UI.WebControls.CheckBox CBClevisbracket;
		protected System.Web.UI.WebControls.Panel PanelClevisBracket;
		protected System.Web.UI.WebControls.TextBox TxtEyeb;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.DropDownList DDLEyeb;
		protected System.Web.UI.WebControls.CheckBox CBEyeb;
		protected System.Web.UI.WebControls.Panel PanelEyeBracket;
		protected System.Web.UI.WebControls.Image ImgRodeye;
		protected System.Web.UI.WebControls.TextBox TxtRodeye;
		protected System.Web.UI.WebControls.Label Label22;
		protected System.Web.UI.WebControls.DropDownList DDLRodeye;
		protected System.Web.UI.WebControls.CheckBox CBRodeye;
		protected System.Web.UI.WebControls.Panel PanelRodeye;
		protected System.Web.UI.WebControls.Image ImgSphericalclevisbraket;
		protected System.Web.UI.WebControls.TextBox TxtSphericalclevisbraket;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.WebControls.DropDownList DDLSphericalclevisbracket;
		protected System.Web.UI.WebControls.CheckBox CBSphericalclevisbracket;
		protected System.Web.UI.WebControls.Panel PanelSphericalClevisBracket;
		protected System.Web.UI.WebControls.Image ImgRodclevis;
		protected System.Web.UI.WebControls.TextBox TxtRodclevis;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.DropDownList DDLRodclevis;
		protected System.Web.UI.WebControls.CheckBox CBRodclevis;
		protected System.Web.UI.WebControls.Panel PanelRodClevis;
		protected System.Web.UI.WebControls.Image ImgPivotpin;
		protected System.Web.UI.WebControls.TextBox txtPivotpin;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.DropDownList DDLPivotpin;
		protected System.Web.UI.WebControls.CheckBox CBPivotpin;
		protected System.Web.UI.WebControls.Panel PanelPivotPin;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.DropDownList DDLKK;
		protected System.Web.UI.WebControls.RadioButtonList RBLMaleFemale;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.HyperLink HLPrint;
		protected System.Web.UI.WebControls.Label lblinfo;
		protected System.Web.UI.WebControls.Image ImgEyeb;
		DBClass db=new DBClass();
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["User"] !=null)
			{
				if(! IsPostBack)
				{
						lblinfo.Text="";
						RBLMaleFemale.SelectedIndex=0;
						ArrayList klist=new ArrayList();
						klist =db.SelectValues_RodendKK("WEB_KK_TableV1");
						DDLKK.Items.Clear();
						for(int i=0;i<klist.Count;i++)
						{
							DDLKK.Items.Add(klist[i].ToString());
								
						}
						DDLKK.SelectedIndex =0;
						string cd="";
						cd =db.SelectValue("CD","WEB_Accessories_TableV11","KK",DDLKK.SelectedItem.Text.Trim());

						if(RBLMaleFemale.SelectedIndex ==0)
						{
							PanelRodeye.Enabled =true;
							PanelRodClevis.Enabled =true;
							PanelPivotPin.Enabled =true;
							PanelEyeBracket.Enabled =true;
							PanelClevisBracket.Enabled =true;
							PanelLinearCoupler.Enabled =true;
							PanelSphericalClevisBracket.Enabled=false;
							PanelSphericalPivotpin.Enabled=false;
							PanelSphericalRodEye.Enabled=false;
								
						}
						else
						{
							PanelRodeye.Enabled =false;
							PanelRodClevis.Enabled =false;
							PanelPivotPin.Enabled =false;
							PanelEyeBracket.Enabled =false;
							PanelClevisBracket.Enabled =false;
							PanelLinearCoupler.Enabled =false;
							PanelSphericalClevisBracket.Enabled=true;
							PanelSphericalPivotpin.Enabled=true;
							PanelSphericalRodEye.Enabled=true;
						}
						if(PanelRodeye.Enabled ==true)
						{
							ArrayList plist=new ArrayList();
							plist =db.SelectAccessories_Discription2("WEB_Accessories_TableV11","Rod Eye",DDLKK.SelectedItem.Text.Trim());
							DDLRodeye.Items.Clear();
							ArrayList list1 = new ArrayList();
							ArrayList list2 = new ArrayList();
							list1=(ArrayList)plist[0];
							list2=(ArrayList)plist[1];
							ListItem li=new ListItem();
							if(list1.Count >0)
							{
								for(int i=0;i<list1.Count;i++)
								{
									li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
									DDLRodeye.Items.Add(li);
								}
								ImgRodeye.ImageUrl="accessories/rodeye.jpg";
							}
							else
							{
								PanelRodeye.Enabled =false;
								DDLRodeye.Items.Clear();
								TxtRodeye.Text="";
								CBRodeye.Checked=false;
								ImgRodeye.ImageUrl="accessories/na.jpg";
							}
						}
						else
						{
							PanelRodeye.Enabled =false;
							DDLRodeye.Items.Clear();
							TxtRodeye.Text="";
							CBRodeye.Checked=false;
							ImgRodeye.ImageUrl="accessories/na.jpg";
						}
						if(PanelRodClevis.Enabled ==true)
						{
							ArrayList plist=new ArrayList();
							plist =db.SelectAccessories_Discription2("WEB_Accessories_TableV11","Rod Clevis",DDLKK.SelectedItem.Text.Trim());
							DDLRodclevis.Items.Clear();
							ArrayList list1 = new ArrayList();
							ArrayList list2 = new ArrayList();
							list1=(ArrayList)plist[0];
							list2=(ArrayList)plist[1];
							ListItem li=new ListItem();
							if(list1.Count >0)
							{
								for(int i=0;i<list1.Count;i++)
								{
									li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
									DDLRodclevis.Items.Add(li);
								}
								ImgRodclevis.ImageUrl="accessories/rodclevis.jpg";
							}
							else
							{
								PanelRodClevis.Enabled =false;
								DDLRodclevis.Items.Clear();
								TxtRodclevis.Text="";
								CBRodclevis.Checked=false;
								ImgRodclevis.ImageUrl="accessories/na.jpg";
							}
						}
						else
						{
							PanelRodClevis.Enabled =false;
							DDLRodclevis.Items.Clear();
							TxtRodclevis.Text="";
							CBRodclevis.Checked=false;
							ImgRodclevis.ImageUrl="accessories/na.jpg";
						}
						if(PanelPivotPin.Enabled ==true)
						{
							ArrayList plist=new ArrayList();
							plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Pivot Pin",cd.Trim());
							DDLPivotpin.Items.Clear();
							ArrayList list1 = new ArrayList();
							ArrayList list2 = new ArrayList();
							list1=(ArrayList)plist[0];
							list2=(ArrayList)plist[1];
							ListItem li=new ListItem();
							if(list1.Count >0)
							{
								for(int i=0;i<list1.Count;i++)
								{
									li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
									DDLPivotpin.Items.Add(li);
								}
								ImgPivotpin.ImageUrl="accessories/pivotpin.jpg";
							}
							else
							{
								PanelPivotPin.Enabled =false;
								DDLPivotpin.Items.Clear();
								txtPivotpin.Text="";
								CBPivotpin.Checked=false;
								ImgPivotpin.ImageUrl="accessories/na.jpg";
							}
						}
						else
						{
							PanelPivotPin.Enabled =false;
							DDLPivotpin.Items.Clear();
							txtPivotpin.Text="";
							CBPivotpin.Checked=false;
							ImgPivotpin.ImageUrl="accessories/na.jpg";
						}
						if(PanelEyeBracket.Enabled ==true)
						{
							ArrayList plist=new ArrayList();
							plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Eye Bracket",cd.Trim());
							DDLEyeb.Items.Clear();
							ArrayList list1 = new ArrayList();
							ArrayList list2 = new ArrayList();
							list1=(ArrayList)plist[0];
							list2=(ArrayList)plist[1];
							ListItem li=new ListItem();
							if(list1.Count >0)
							{
								for(int i=0;i<list1.Count;i++)
								{
									li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
									DDLEyeb.Items.Add(li);
								}
								ImgEyeb.ImageUrl="accessories/eyebracket.jpg";
							}
							else
							{
								PanelEyeBracket.Enabled =false;
								DDLEyeb.Items.Clear();
								TxtEyeb.Text="";
								CBEyeb.Checked=false;
								ImgEyeb.ImageUrl="accessories/na.jpg";
							}
						}
						else
						{
							PanelEyeBracket.Enabled =false;
							DDLEyeb.Items.Clear();
							TxtEyeb.Text="";
							CBEyeb.Checked=false;
							ImgEyeb.ImageUrl="accessories/na.jpg";
						}
						if(PanelClevisBracket.Enabled ==true)
						{
							ArrayList plist=new ArrayList();
							plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Clevis Bracket",cd.Trim());
							DDLClevisbracket.Items.Clear();
							ArrayList list1 = new ArrayList();
							ArrayList list2 = new ArrayList();
							list1=(ArrayList)plist[0];
							list2=(ArrayList)plist[1];
							ListItem li=new ListItem();
							if(list1.Count >0)
							{
								for(int i=0;i<list1.Count;i++)
								{
									li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
									DDLClevisbracket.Items.Add(li);
								}
								ImgClevisbracket.ImageUrl="accessories/clevisbracket.jpg";
							}
							else
							{
								PanelClevisBracket.Enabled =false;
								DDLClevisbracket.Items.Clear();
								TxtClevisbracket.Text="";
								CBClevisbracket.Checked=false;
								ImgClevisbracket.ImageUrl="accessories/na.jpg";
							}
						}
						else
						{
							PanelClevisBracket.Enabled =false;
							DDLClevisbracket.Items.Clear();
							TxtClevisbracket.Text="";
							CBClevisbracket.Checked=false;
							ImgClevisbracket.ImageUrl="accessories/na.jpg";
						}
						if(PanelLinearCoupler.Enabled ==true)
						{
							ArrayList plist=new ArrayList();
							plist =db.SelectAccessories_Discription2("WEB_Accessories_TableV11","Linear Alignment Coupler",DDLKK.SelectedItem.Text.Trim());
							DDLLinearcoupler.Items.Clear();
							ArrayList list1 = new ArrayList();
							ArrayList list2 = new ArrayList();
							list1=(ArrayList)plist[0];
							list2=(ArrayList)plist[1];
							ListItem li=new ListItem();
							if(list1.Count >0)
							{
								for(int i=0;i<list1.Count;i++)
								{
									li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
									DDLLinearcoupler.Items.Add(li);
								}
								ImgLinearcoupler.ImageUrl="accessories/linearcoupler.jpg";
							}
							else
							{
								PanelLinearCoupler.Enabled =false;
								DDLLinearcoupler.Items.Clear();
								TxtLinearcoupler.Text="";
								CBLinearcoupler.Checked=false;
								ImgLinearcoupler.ImageUrl="accessories/na.jpg";
							}
						}
						else
						{
							PanelLinearCoupler.Enabled =false;
							DDLLinearcoupler.Items.Clear();
							TxtLinearcoupler.Text="";
							CBLinearcoupler.Checked=false;
							ImgLinearcoupler.ImageUrl="accessories/na.jpg";
						}
						if(PanelSphericalRodEye.Enabled ==true)
						{
							ArrayList plist=new ArrayList();
							plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Spherical Rod Eye",cd.Trim());
							DDLSphericalrodeye.Items.Clear();
							ArrayList list1 = new ArrayList();
							ArrayList list2 = new ArrayList();
							list1=(ArrayList)plist[0];
							list2=(ArrayList)plist[1];
							ListItem li=new ListItem();
							if(list1.Count >0)
							{
								for(int i=0;i<list1.Count;i++)
								{
									li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
									DDLSphericalrodeye.Items.Add(li);
								}
								ImgSphericalrodeye.ImageUrl="accessories/sphericalrodeye.jpg";
							}
							else
							{
								PanelSphericalRodEye.Enabled =false;
								DDLSphericalrodeye.Items.Clear();
								TxtSphericalrodeye.Text="";
								CBSphericalrodeye.Checked=false;
								ImgSphericalrodeye.ImageUrl="accessories/na.jpg";
							}
						}
						else
						{
							PanelSphericalRodEye.Enabled =false;
							DDLSphericalrodeye.Items.Clear();
							TxtSphericalrodeye.Text="";
							CBSphericalrodeye.Checked=false;
							ImgSphericalrodeye.ImageUrl="accessories/na.jpg";
						}
						if(PanelSphericalClevisBracket.Enabled ==true)
						{
							ArrayList plist=new ArrayList();
							plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Spherical Clevis Bracket",cd.Trim());
							DDLSphericalclevisbracket.Items.Clear();
							ArrayList list1 = new ArrayList();
							ArrayList list2 = new ArrayList();
							list1=(ArrayList)plist[0];
							list2=(ArrayList)plist[1];
							ListItem li=new ListItem();
							if(list1.Count >0)
							{
								for(int i=0;i<list1.Count;i++)
								{
									li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
									DDLSphericalclevisbracket.Items.Add(li);
								}
								ImgSphericalclevisbraket.ImageUrl="accessories/sphericalclevisbracket.jpg";
							}
							else
							{
								PanelSphericalClevisBracket.Enabled =false;
								DDLSphericalclevisbracket.Items.Clear();
								TxtSphericalclevisbraket.Text="";
								CBSphericalclevisbracket.Checked=false;
								ImgSphericalclevisbraket.ImageUrl="accessories/na.jpg";
							}
						}
						else
						{
							PanelSphericalClevisBracket.Enabled =false;
							DDLSphericalclevisbracket.Items.Clear();
							TxtSphericalclevisbraket.Text="";
							CBSphericalclevisbracket.Checked=false;
							ImgSphericalclevisbraket.ImageUrl="accessories/na.jpg";
						}
						if(PanelSphericalPivotpin.Enabled ==true)
						{
							ArrayList plist=new ArrayList();
							plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Spherical Pivot Pin",cd.Trim());
							DDLSphericalpp.Items.Clear();
							ArrayList list1 = new ArrayList();
							ArrayList list2 = new ArrayList();
							list1=(ArrayList)plist[0];
							list2=(ArrayList)plist[1];
							ListItem li=new ListItem();
							if(list1.Count >0)
							{
								for(int i=0;i<list1.Count;i++)
								{
									li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
									DDLSphericalpp.Items.Add(li);
								}
								ImgSphericalpp.ImageUrl="accessories/sphericalpivotpin.jpg";
							}
							else
							{
								PanelSphericalPivotpin.Enabled =false;
								DDLSphericalpp.Items.Clear();
								TxtSphericalpp.Text="";
								CBSpericalpp.Checked=false;
								ImgSphericalpp.ImageUrl="accessories/na.jpg";
							}
						}
						else
						{
							PanelSphericalPivotpin.Enabled =false;
							DDLSphericalpp.Items.Clear();
							TxtSphericalpp.Text="";
							CBSpericalpp.Checked=false;
							ImgSphericalpp.ImageUrl="accessories/na.jpg";
						}
								
					}
				}
			else
			{
				Response.Redirect("Login.aspx");
			}

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.RBLMaleFemale.SelectedIndexChanged += new System.EventHandler(this.RBLMaleFemale_SelectedIndexChanged);
			this.DDLKK.SelectedIndexChanged += new System.EventHandler(this.DDLKK_SelectedIndexChanged);
			this.TxtRodeye.TextChanged += new System.EventHandler(this.TxtRodeye_TextChanged);
			this.TxtRodclevis.TextChanged += new System.EventHandler(this.TxtRodclevis_TextChanged);
			this.txtPivotpin.TextChanged += new System.EventHandler(this.txtPivotpin_TextChanged);
			this.TxtEyeb.TextChanged += new System.EventHandler(this.TxtEyeb_TextChanged);
			this.TxtClevisbracket.TextChanged += new System.EventHandler(this.TxtClevisbracket_TextChanged);
			this.TxtLinearcoupler.TextChanged += new System.EventHandler(this.TxtLinearcoupler_TextChanged);
			this.TxtSphericalrodeye.TextChanged += new System.EventHandler(this.TxtSphericalrodeye_TextChanged);
			this.TxtSphericalclevisbraket.TextChanged += new System.EventHandler(this.TxtSphericalclevisbraket_TextChanged);
			this.TxtSphericalpp.TextChanged += new System.EventHandler(this.TxtSphericalpp_TextChanged);
			this.LBHome.Click += new System.EventHandler(this.LBHome_Click);
			this.BtnNext.Click += new System.EventHandler(this.BtnNext_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void RBLMaleFemale_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			
			BtnNext.Visible=true;
			HLPrint.Visible=false;
			if(RBLMaleFemale.SelectedIndex ==0)
			{
				
				lblinfo.Text="";
				ArrayList klist=new ArrayList();
				klist =db.SelectValues_RodendKK("WEB_KK_TableV1");
				DDLKK.Items.Clear();
				for(int i=0;i<klist.Count;i++)
				{
					DDLKK.Items.Add(klist[i].ToString());
								
				}
				DDLKK.SelectedIndex =0;
				string cd="";
				cd =db.SelectValue("CD","WEB_Accessories_TableV11","KK",DDLKK.SelectedItem.Text.Trim());

				if(RBLMaleFemale.SelectedIndex ==0)
				{
					PanelRodeye.Enabled =true;
					PanelRodClevis.Enabled =true;
					PanelPivotPin.Enabled =true;
					PanelEyeBracket.Enabled =true;
					PanelClevisBracket.Enabled =true;
					PanelLinearCoupler.Enabled =true;
					PanelSphericalClevisBracket.Enabled=false;
					PanelSphericalPivotpin.Enabled=false;
					PanelSphericalRodEye.Enabled=false;
								
				}
				else
				{
					PanelRodeye.Enabled =false;
					PanelRodClevis.Enabled =false;
					PanelPivotPin.Enabled =false;
					PanelEyeBracket.Enabled =false;
					PanelClevisBracket.Enabled =false;
					PanelLinearCoupler.Enabled =false;
					PanelSphericalClevisBracket.Enabled=true;
					PanelSphericalPivotpin.Enabled=true;
					PanelSphericalRodEye.Enabled=true;
				}
				if(PanelRodeye.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription2("WEB_Accessories_TableV11","Rod Eye",DDLKK.SelectedItem.Text.Trim());
					DDLRodeye.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLRodeye.Items.Add(li);
						}
						ImgRodeye.ImageUrl="accessories/rodeye.jpg";
					}
					else
					{
						PanelRodeye.Enabled =false;
						DDLRodeye.Items.Clear();
						TxtRodeye.Text="";
						CBRodeye.Checked=false;
						ImgRodeye.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelRodeye.Enabled =false;
					DDLRodeye.Items.Clear();
					TxtRodeye.Text="";
					CBRodeye.Checked=false;
					ImgRodeye.ImageUrl="accessories/na.jpg";
				}
				if(PanelRodClevis.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription2("WEB_Accessories_TableV11","Rod Clevis",DDLKK.SelectedItem.Text.Trim());
					DDLRodclevis.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLRodclevis.Items.Add(li);
						}
						ImgRodclevis.ImageUrl="accessories/rodclevis.jpg";
					}
					else
					{
						PanelRodClevis.Enabled =false;
						DDLRodclevis.Items.Clear();
						TxtRodclevis.Text="";
						CBRodclevis.Checked=false;
						ImgRodclevis.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelRodClevis.Enabled =false;
					DDLRodclevis.Items.Clear();
					TxtRodclevis.Text="";
					CBRodclevis.Checked=false;
					ImgRodclevis.ImageUrl="accessories/na.jpg";
				}
				if(PanelPivotPin.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Pivot Pin",cd.Trim());
					DDLPivotpin.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLPivotpin.Items.Add(li);
						}
						ImgPivotpin.ImageUrl="accessories/pivotpin.jpg";
					}
					else
					{
						PanelPivotPin.Enabled =false;
						DDLPivotpin.Items.Clear();
						txtPivotpin.Text="";
						CBPivotpin.Checked=false;
						ImgPivotpin.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelPivotPin.Enabled =false;
					DDLPivotpin.Items.Clear();
					txtPivotpin.Text="";
					CBPivotpin.Checked=false;
					ImgPivotpin.ImageUrl="accessories/na.jpg";
				}
				if(PanelEyeBracket.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Eye Bracket",cd.Trim());
					DDLEyeb.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLEyeb.Items.Add(li);
						}
						ImgEyeb.ImageUrl="accessories/eyebracket.jpg";
					}
					else
					{
						PanelEyeBracket.Enabled =false;
						DDLEyeb.Items.Clear();
						TxtEyeb.Text="";
						CBEyeb.Checked=false;
						ImgEyeb.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelEyeBracket.Enabled =false;
					DDLEyeb.Items.Clear();
					TxtEyeb.Text="";
					CBEyeb.Checked=false;
					ImgEyeb.ImageUrl="accessories/na.jpg";
				}
				if(PanelClevisBracket.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Clevis Bracket",cd.Trim());
					DDLClevisbracket.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLClevisbracket.Items.Add(li);
						}
						ImgClevisbracket.ImageUrl="accessories/clevisbracket.jpg";
					}
					else
					{
						PanelClevisBracket.Enabled =false;
						DDLClevisbracket.Items.Clear();
						TxtClevisbracket.Text="";
						CBClevisbracket.Checked=false;
						ImgClevisbracket.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelClevisBracket.Enabled =false;
					DDLClevisbracket.Items.Clear();
					TxtClevisbracket.Text="";
					CBClevisbracket.Checked=false;
					ImgClevisbracket.ImageUrl="accessories/na.jpg";
				}
				if(PanelLinearCoupler.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription2("WEB_Accessories_TableV11","Linear Alignment Coupler",DDLKK.SelectedItem.Text.Trim());
					DDLLinearcoupler.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLLinearcoupler.Items.Add(li);
						}
						ImgLinearcoupler.ImageUrl="accessories/linearcoupler.jpg";
					}
					else
					{
						PanelLinearCoupler.Enabled =false;
						DDLLinearcoupler.Items.Clear();
						TxtLinearcoupler.Text="";
						CBLinearcoupler.Checked=false;
						ImgLinearcoupler.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelLinearCoupler.Enabled =false;
					DDLLinearcoupler.Items.Clear();
					TxtLinearcoupler.Text="";
					CBLinearcoupler.Checked=false;
					ImgLinearcoupler.ImageUrl="accessories/na.jpg";
				}
				if(PanelSphericalRodEye.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Spherical Rod Eye",cd.Trim());
					DDLSphericalrodeye.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLSphericalrodeye.Items.Add(li);
						}
						ImgSphericalrodeye.ImageUrl="accessories/sphericalrodeye.jpg";
					}
					else
					{
						PanelSphericalRodEye.Enabled =false;
						DDLSphericalrodeye.Items.Clear();
						TxtSphericalrodeye.Text="";
						CBSphericalrodeye.Checked=false;
						ImgSphericalrodeye.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelSphericalRodEye.Enabled =false;
					DDLSphericalrodeye.Items.Clear();
					TxtSphericalrodeye.Text="";
					CBSphericalrodeye.Checked=false;
					ImgSphericalrodeye.ImageUrl="accessories/na.jpg";
				}
				if(PanelSphericalClevisBracket.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Spherical Clevis Bracket",cd.Trim());
					DDLSphericalclevisbracket.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLSphericalclevisbracket.Items.Add(li);
						}
						ImgSphericalclevisbraket.ImageUrl="accessories/sphericalclevisbracket.jpg";
					}
					else
					{
						PanelSphericalClevisBracket.Enabled =false;
						DDLSphericalclevisbracket.Items.Clear();
						TxtSphericalclevisbraket.Text="";
						CBSphericalclevisbracket.Checked=false;
						ImgSphericalclevisbraket.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelSphericalClevisBracket.Enabled =false;
					DDLSphericalclevisbracket.Items.Clear();
					TxtSphericalclevisbraket.Text="";
					CBSphericalclevisbracket.Checked=false;
					ImgSphericalclevisbraket.ImageUrl="accessories/na.jpg";
				}
				if(PanelSphericalPivotpin.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Spherical Pivot Pin",cd.Trim());
					DDLSphericalpp.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLSphericalpp.Items.Add(li);
						}
						ImgSphericalpp.ImageUrl="accessories/sphericalpivotpin.jpg";
					}
					else
					{
						PanelSphericalPivotpin.Enabled =false;
						DDLSphericalpp.Items.Clear();
						TxtSphericalpp.Text="";
						CBSpericalpp.Checked=false;
						ImgSphericalpp.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelSphericalPivotpin.Enabled =false;
					DDLSphericalpp.Items.Clear();
					TxtSphericalpp.Text="";
					CBSpericalpp.Checked=false;
					ImgSphericalpp.ImageUrl="accessories/na.jpg";
				}
								
			}
			else if(RBLMaleFemale.SelectedIndex ==1)
			{
				lblinfo.Text="";
				ArrayList klist=new ArrayList();
				klist =db.SelectValues_RodendKK("WEB_KK_TableV1");
				DDLKK.Items.Clear();
				for(int i=0;i<klist.Count;i++)
				{
					DDLKK.Items.Add(klist[i].ToString());
								
				}
				DDLKK.SelectedIndex =0;
				string cd="";
				cd =db.SelectValue("CD","WEB_Accessories_TableV11","KK",DDLKK.SelectedItem.Text.Trim());

				if(RBLMaleFemale.SelectedIndex ==0)
				{
					PanelRodeye.Enabled =true;
					PanelRodClevis.Enabled =true;
					PanelPivotPin.Enabled =true;
					PanelEyeBracket.Enabled =true;
					PanelClevisBracket.Enabled =true;
					PanelLinearCoupler.Enabled =true;
					PanelSphericalClevisBracket.Enabled=false;
					PanelSphericalPivotpin.Enabled=false;
					PanelSphericalRodEye.Enabled=false;
								
				}
				else
				{
					PanelRodeye.Enabled =false;
					PanelRodClevis.Enabled =false;
					PanelPivotPin.Enabled =false;
					PanelEyeBracket.Enabled =false;
					PanelClevisBracket.Enabled =false;
					PanelLinearCoupler.Enabled =false;
					PanelSphericalClevisBracket.Enabled=true;
					PanelSphericalPivotpin.Enabled=true;
					PanelSphericalRodEye.Enabled=true;
				}
				if(PanelRodeye.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription2("WEB_Accessories_TableV11","Rod Eye",DDLKK.SelectedItem.Text.Trim());
					DDLRodeye.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLRodeye.Items.Add(li);
						}
						ImgRodeye.ImageUrl="accessories/rodeye.jpg";
					}
					else
					{
						PanelRodeye.Enabled =false;
						DDLRodeye.Items.Clear();
						TxtRodeye.Text="";
						CBRodeye.Checked=false;
						ImgRodeye.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelRodeye.Enabled =false;
					DDLRodeye.Items.Clear();
					TxtRodeye.Text="";
					CBRodeye.Checked=false;
					ImgRodeye.ImageUrl="accessories/na.jpg";
				}
				if(PanelRodClevis.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription2("WEB_Accessories_TableV11","Rod Clevis",DDLKK.SelectedItem.Text.Trim());
					DDLRodclevis.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLRodclevis.Items.Add(li);
						}
						ImgRodclevis.ImageUrl="accessories/rodclevis.jpg";
					}
					else
					{
						PanelRodClevis.Enabled =false;
						DDLRodclevis.Items.Clear();
						TxtRodclevis.Text="";
						CBRodclevis.Checked=false;
						ImgRodclevis.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelRodClevis.Enabled =false;
					DDLRodclevis.Items.Clear();
					TxtRodclevis.Text="";
					CBRodclevis.Checked=false;
					ImgRodclevis.ImageUrl="accessories/na.jpg";
				}
				if(PanelPivotPin.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Pivot Pin",cd.Trim());
					DDLPivotpin.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLPivotpin.Items.Add(li);
						}
						ImgPivotpin.ImageUrl="accessories/pivotpin.jpg";
					}
					else
					{
						PanelPivotPin.Enabled =false;
						DDLPivotpin.Items.Clear();
						txtPivotpin.Text="";
						CBPivotpin.Checked=false;
						ImgPivotpin.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelPivotPin.Enabled =false;
					DDLPivotpin.Items.Clear();
					txtPivotpin.Text="";
					CBPivotpin.Checked=false;
					ImgPivotpin.ImageUrl="accessories/na.jpg";
				}
				if(PanelEyeBracket.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Eye Bracket",cd.Trim());
					DDLEyeb.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLEyeb.Items.Add(li);
						}
						ImgEyeb.ImageUrl="accessories/eyebracket.jpg";
					}
					else
					{
						PanelEyeBracket.Enabled =false;
						DDLEyeb.Items.Clear();
						TxtEyeb.Text="";
						CBEyeb.Checked=false;
						ImgEyeb.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelEyeBracket.Enabled =false;
					DDLEyeb.Items.Clear();
					TxtEyeb.Text="";
					CBEyeb.Checked=false;
					ImgEyeb.ImageUrl="accessories/na.jpg";
				}
				if(PanelClevisBracket.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Clevis Bracket",cd.Trim());
					DDLClevisbracket.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLClevisbracket.Items.Add(li);
						}
						ImgClevisbracket.ImageUrl="accessories/clevisbracket.jpg";
					}
					else
					{
						PanelClevisBracket.Enabled =false;
						DDLClevisbracket.Items.Clear();
						TxtClevisbracket.Text="";
						CBClevisbracket.Checked=false;
						ImgClevisbracket.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelClevisBracket.Enabled =false;
					DDLClevisbracket.Items.Clear();
					TxtClevisbracket.Text="";
					CBClevisbracket.Checked=false;
					ImgClevisbracket.ImageUrl="accessories/na.jpg";
				}
				if(PanelLinearCoupler.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription2("WEB_Accessories_TableV11","Linear Alignment Coupler",DDLKK.SelectedItem.Text.Trim());
					DDLLinearcoupler.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLLinearcoupler.Items.Add(li);
						}
						ImgLinearcoupler.ImageUrl="accessories/linearcoupler.jpg";
					}
					else
					{
						PanelLinearCoupler.Enabled =false;
						DDLLinearcoupler.Items.Clear();
						TxtLinearcoupler.Text="";
						CBLinearcoupler.Checked=false;
						ImgLinearcoupler.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelLinearCoupler.Enabled =false;
					DDLLinearcoupler.Items.Clear();
					TxtLinearcoupler.Text="";
					CBLinearcoupler.Checked=false;
					ImgLinearcoupler.ImageUrl="accessories/na.jpg";
				}
				if(PanelSphericalRodEye.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Spherical Rod Eye",cd.Trim());
					DDLSphericalrodeye.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLSphericalrodeye.Items.Add(li);
						}
						ImgSphericalrodeye.ImageUrl="accessories/sphericalrodeye.jpg";
					}
					else
					{
						PanelSphericalRodEye.Enabled =false;
						DDLSphericalrodeye.Items.Clear();
						TxtSphericalrodeye.Text="";
						CBSphericalrodeye.Checked=false;
						ImgSphericalrodeye.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelSphericalRodEye.Enabled =false;
					DDLSphericalrodeye.Items.Clear();
					TxtSphericalrodeye.Text="";
					CBSphericalrodeye.Checked=false;
					ImgSphericalrodeye.ImageUrl="accessories/na.jpg";
				}
				if(PanelSphericalClevisBracket.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Spherical Clevis Bracket",cd.Trim());
					DDLSphericalclevisbracket.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLSphericalclevisbracket.Items.Add(li);
						}
						ImgSphericalclevisbraket.ImageUrl="accessories/sphericalclevisbracket.jpg";
					}
					else
					{
						PanelSphericalClevisBracket.Enabled =false;
						DDLSphericalclevisbracket.Items.Clear();
						TxtSphericalclevisbraket.Text="";
						CBSphericalclevisbracket.Checked=false;
						ImgSphericalclevisbraket.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelSphericalClevisBracket.Enabled =false;
					DDLSphericalclevisbracket.Items.Clear();
					TxtSphericalclevisbraket.Text="";
					CBSphericalclevisbracket.Checked=false;
					ImgSphericalclevisbraket.ImageUrl="accessories/na.jpg";
				}
				if(PanelSphericalPivotpin.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Spherical Pivot Pin",cd.Trim());
					DDLSphericalpp.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLSphericalpp.Items.Add(li);
						}
						ImgSphericalpp.ImageUrl="accessories/sphericalpivotpin.jpg";
					}
					else
					{
						PanelSphericalPivotpin.Enabled =false;
						DDLSphericalpp.Items.Clear();
						TxtSphericalpp.Text="";
						CBSpericalpp.Checked=false;
						ImgSphericalpp.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelSphericalPivotpin.Enabled =false;
					DDLSphericalpp.Items.Clear();
					TxtSphericalpp.Text="";
					CBSpericalpp.Checked=false;
					ImgSphericalpp.ImageUrl="accessories/na.jpg";
				}
			}
		}

		private void DDLKK_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			BtnNext.Visible=true;
			HLPrint.Visible=false;
			if(DDLKK.SelectedIndex !=-1)
			{
				lblinfo.Text="";
				string cd="";
				cd =db.SelectValue("CD","WEB_Accessories_TableV11","KK",DDLKK.SelectedItem.Text.Trim());

				if(RBLMaleFemale.SelectedIndex ==0)
				{
					PanelRodeye.Enabled =true;
					PanelRodClevis.Enabled =true;
					PanelPivotPin.Enabled =true;
					PanelEyeBracket.Enabled =true;
					PanelClevisBracket.Enabled =true;
					PanelLinearCoupler.Enabled =true;
					PanelSphericalClevisBracket.Enabled=false;
					PanelSphericalPivotpin.Enabled=false;
					PanelSphericalRodEye.Enabled=false;
								
				}
				else
				{
					PanelRodeye.Enabled =false;
					PanelRodClevis.Enabled =false;
					PanelPivotPin.Enabled =false;
					PanelEyeBracket.Enabled =false;
					PanelClevisBracket.Enabled =false;
					PanelLinearCoupler.Enabled =false;
					PanelSphericalClevisBracket.Enabled=true;
					PanelSphericalPivotpin.Enabled=true;
					PanelSphericalRodEye.Enabled=true;
				}
				if(PanelRodeye.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription2("WEB_Accessories_TableV11","Rod Eye",DDLKK.SelectedItem.Text.Trim());
					DDLRodeye.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLRodeye.Items.Add(li);
						}
						ImgRodeye.ImageUrl="accessories/rodeye.jpg";
					}
					else
					{
						PanelRodeye.Enabled =false;
						DDLRodeye.Items.Clear();
						TxtRodeye.Text="";
						CBRodeye.Checked=false;
						ImgRodeye.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelRodeye.Enabled =false;
					DDLRodeye.Items.Clear();
					TxtRodeye.Text="";
					CBRodeye.Checked=false;
					ImgRodeye.ImageUrl="accessories/na.jpg";
				}
				if(PanelRodClevis.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription2("WEB_Accessories_TableV11","Rod Clevis",DDLKK.SelectedItem.Text.Trim());
					DDLRodclevis.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLRodclevis.Items.Add(li);
						}
						ImgRodclevis.ImageUrl="accessories/rodclevis.jpg";
					}
					else
					{
						PanelRodClevis.Enabled =false;
						DDLRodclevis.Items.Clear();
						TxtRodclevis.Text="";
						CBRodclevis.Checked=false;
						ImgRodclevis.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelRodClevis.Enabled =false;
					DDLRodclevis.Items.Clear();
					TxtRodclevis.Text="";
					CBRodclevis.Checked=false;
					ImgRodclevis.ImageUrl="accessories/na.jpg";
				}
				if(PanelPivotPin.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Pivot Pin",cd.Trim());
					DDLPivotpin.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLPivotpin.Items.Add(li);
						}
						ImgPivotpin.ImageUrl="accessories/pivotpin.jpg";
					}
					else
					{
						PanelPivotPin.Enabled =false;
						DDLPivotpin.Items.Clear();
						txtPivotpin.Text="";
						CBPivotpin.Checked=false;
						ImgPivotpin.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelPivotPin.Enabled =false;
					DDLPivotpin.Items.Clear();
					txtPivotpin.Text="";
					CBPivotpin.Checked=false;
					ImgPivotpin.ImageUrl="accessories/na.jpg";
				}
				if(PanelEyeBracket.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Eye Bracket",cd.Trim());
					DDLEyeb.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLEyeb.Items.Add(li);
						}
						ImgEyeb.ImageUrl="accessories/eyebracket.jpg";
					}
					else
					{
						PanelEyeBracket.Enabled =false;
						DDLEyeb.Items.Clear();
						TxtEyeb.Text="";
						CBEyeb.Checked=false;
						ImgEyeb.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelEyeBracket.Enabled =false;
					DDLEyeb.Items.Clear();
					TxtEyeb.Text="";
					CBEyeb.Checked=false;
					ImgEyeb.ImageUrl="accessories/na.jpg";
				}
				if(PanelClevisBracket.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Clevis Bracket",cd.Trim());
					DDLClevisbracket.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLClevisbracket.Items.Add(li);
						}
						ImgClevisbracket.ImageUrl="accessories/clevisbracket.jpg";
					}
					else
					{
						PanelClevisBracket.Enabled =false;
						DDLClevisbracket.Items.Clear();
						TxtClevisbracket.Text="";
						CBClevisbracket.Checked=false;
						ImgClevisbracket.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelClevisBracket.Enabled =false;
					DDLClevisbracket.Items.Clear();
					TxtClevisbracket.Text="";
					CBClevisbracket.Checked=false;
					ImgClevisbracket.ImageUrl="accessories/na.jpg";
				}
				if(PanelLinearCoupler.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription2("WEB_Accessories_TableV11","Linear Alignment Coupler",DDLKK.SelectedItem.Text.Trim());
					DDLLinearcoupler.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLLinearcoupler.Items.Add(li);
						}
						ImgLinearcoupler.ImageUrl="accessories/linearcoupler.jpg";
					}
					else
					{
						PanelLinearCoupler.Enabled =false;
						DDLLinearcoupler.Items.Clear();
						TxtLinearcoupler.Text="";
						CBLinearcoupler.Checked=false;
						ImgLinearcoupler.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelLinearCoupler.Enabled =false;
					DDLLinearcoupler.Items.Clear();
					TxtLinearcoupler.Text="";
					CBLinearcoupler.Checked=false;
					ImgLinearcoupler.ImageUrl="accessories/na.jpg";
				}
				if(PanelSphericalRodEye.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Spherical Rod Eye",cd.Trim());
					DDLSphericalrodeye.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLSphericalrodeye.Items.Add(li);
						}
						ImgSphericalrodeye.ImageUrl="accessories/sphericalrodeye.jpg";
					}
					else
					{
						PanelSphericalRodEye.Enabled =false;
						DDLSphericalrodeye.Items.Clear();
						TxtSphericalrodeye.Text="";
						CBSphericalrodeye.Checked=false;
						ImgSphericalrodeye.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelSphericalRodEye.Enabled =false;
					DDLSphericalrodeye.Items.Clear();
					TxtSphericalrodeye.Text="";
					CBSphericalrodeye.Checked=false;
					ImgSphericalrodeye.ImageUrl="accessories/na.jpg";
				}
				if(PanelSphericalClevisBracket.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Spherical Clevis Bracket",cd.Trim());
					DDLSphericalclevisbracket.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLSphericalclevisbracket.Items.Add(li);
						}
						ImgSphericalclevisbraket.ImageUrl="accessories/sphericalclevisbracket.jpg";
					}
					else
					{
						PanelSphericalClevisBracket.Enabled =false;
						DDLSphericalclevisbracket.Items.Clear();
						TxtSphericalclevisbraket.Text="";
						CBSphericalclevisbracket.Checked=false;
						ImgSphericalclevisbraket.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelSphericalClevisBracket.Enabled =false;
					DDLSphericalclevisbracket.Items.Clear();
					TxtSphericalclevisbraket.Text="";
					CBSphericalclevisbracket.Checked=false;
					ImgSphericalclevisbraket.ImageUrl="accessories/na.jpg";
				}
				if(PanelSphericalPivotpin.Enabled ==true)
				{
					ArrayList plist=new ArrayList();
					plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Spherical Pivot Pin",cd.Trim());
					DDLSphericalpp.Items.Clear();
					ArrayList list1 = new ArrayList();
					ArrayList list2 = new ArrayList();
					list1=(ArrayList)plist[0];
					list2=(ArrayList)plist[1];
					ListItem li=new ListItem();
					if(list1.Count >0)
					{
						for(int i=0;i<list1.Count;i++)
						{
							li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
							DDLSphericalpp.Items.Add(li);
						}
						ImgSphericalpp.ImageUrl="accessories/sphericalpivotpin.jpg";
					}
					else
					{
						PanelSphericalPivotpin.Enabled =false;
						DDLSphericalpp.Items.Clear();
						TxtSphericalpp.Text="";
						CBSpericalpp.Checked=false;
						ImgSphericalpp.ImageUrl="accessories/na.jpg";
					}
				}
				else
				{
					PanelSphericalPivotpin.Enabled =false;
					DDLSphericalpp.Items.Clear();
					TxtSphericalpp.Text="";
					CBSpericalpp.Checked=false;
					ImgSphericalpp.ImageUrl="accessories/na.jpg";
				}
			}
			
		}

	private void BtnNext_Click(object sender, System.EventArgs e)
		{
			int ss=0; 
			if(PanelRodeye.Enabled ==true || PanelRodClevis.Enabled ==true || PanelPivotPin.Enabled ==true || 
				PanelEyeBracket.Enabled ==true || PanelClevisBracket.Enabled ==true || PanelLinearCoupler.Enabled ==true ||
				PanelSphericalRodEye.Enabled ==true || PanelSphericalClevisBracket.Enabled ==true || PanelSphericalPivotpin.Enabled ==true)
			{
				
				if(CBRodeye.Checked ==true || CBRodclevis.Checked ==true || CBPivotpin.Checked ==true || CBEyeb.Checked ==true ||
					CBClevisbracket.Checked ==true || CBLinearcoupler.Checked ==true || CBSphericalrodeye.Checked ==true || 
					CBSphericalclevisbracket.Checked ==true ||	CBSpericalpp.Checked ==true)
				{
					
					ArrayList lst =new ArrayList();
					lst =(ArrayList)Session["User"];
					string discount="0";
					string quoteno="";
					quoteno =Qno("Z");
					discount= db.SelectDiscount(lst[1].ToString(),"Accessories");
					ArrayList list1 = new ArrayList();
					ArrayList list2=new ArrayList();
					list1=db.SelectContactdetails(lst[6].ToString().Trim(),lst[5].ToString().Trim());
					list2=db.SelectCompanyterms(lst[5].ToString().Trim(),lst[6].ToString().Trim());
					Quotation quot =new Quotation();
					string company="";
					if(list1[16].ToString().Trim() =="0")
					{
						company ="Montreal";
					}
					else
					{
						company ="Mississauga";
					}
					quot.Office= company.Trim();
					quot.Customer=list1[0].ToString();
					quot.Contact=lst[0].ToString().Trim();
					quot.AttnTo1=lst[0].ToString().Trim();
					quot.AttnTo2="P:"+list1[12].ToString()+" F:"+list1[13].ToString();
					quot.AttnTo3=list1[14].ToString();
					quot.BillTo1=list1[0].ToString();
					quot.BillTo2=list1[1].ToString()+", "+list1[2].ToString();
					quot.BillTo3=list1[3].ToString()+", "+list1[4].ToString()+", "+list1[5].ToString();
					quot.ShipTo1=list1[6].ToString();
					quot.ShipTo2=list1[7].ToString()+", "+list1[8].ToString();
					quot.ShipTo3=list1[9].ToString()+", "+list1[10].ToString()+", "+list1[11].ToString();
					quot.QuoteNo=quoteno.Trim();
					quot.Quotedate=DateTime.Now.ToShortDateString();
					quot.ExpiryDate=DateTime.Now.AddDays(30).ToShortDateString();;
					quot.PrepairedBy=lst[0].ToString().Trim();
					quot.Code=list2[0].ToString();
					if(list2[4].ToString().Trim()=="0")
					{
						quot.Langu="English";
					}
					else
					{
						quot.Langu="French";
					}
					
					quot.Terms=list2[2].ToString();
					quot.Delivery="TBA";
					if(list2[1].ToString().Trim()=="0")
					{
						quot.Currency="CN $";
					}
					else
					{
						quot.Currency="US $";
					}
					quot.Note="";
					quot.FinishedDate ="  ";
					quot.CowanQno= "  "; 
					quot.Items=quot.QuoteNo.ToString();
					quot.CompanyID=list2[5].ToString();

					decimal dc1=0.00m;
					decimal dc2=0.00m;
					decimal dc3=0.00m;
					decimal dc4=0.00m;
					decimal dc5=0.00m;
					int temp=0;
					lblinfo.Text="";
					if(CBRodeye.Checked ==true)
					{
						if(TxtRodeye.Text.Trim() =="" )
						{
							lblinfo.Text= "Please enter Rod Eye quantity";
							temp =1;
						}
					}
					if(CBRodclevis.Checked ==true)
					{
						if(TxtRodclevis.Text.Trim() =="")
						{
							lblinfo.Text= "Please enter Rod Clevis quantity";
							temp =1;
						}
					}
					if(CBPivotpin.Checked ==true)
					{
						if(txtPivotpin.Text.Trim() =="")
						{
							lblinfo.Text= "Please enter Pivot Pin quantity";
							temp =1;
						}

					}
					if(CBEyeb.Checked ==true)
					{
						if(TxtEyeb.Text.Trim() =="")
						{
							lblinfo.Text= "Please enter Eye Bracket quantity";
							temp =1;
						}
					}
					if(CBClevisbracket.Checked ==true)
					{
						if(TxtClevisbracket.Text.Trim() =="")
						{
							lblinfo.Text= "Please enter Clevis Bracket quantity";
							temp =1;
						}
					}
					if(CBLinearcoupler.Checked ==true)
					{
						if(TxtLinearcoupler.Text.Trim() =="")
						{
							lblinfo.Text= "Please enter Linear Alignment Coupler quantity";
							temp =1;
						}
					}
					if(CBSphericalrodeye.Checked ==true)
					{
						if(TxtSphericalrodeye.Text.Trim() =="")
						{
							lblinfo.Text= "Please enter Spherical Rod Eye quantity";
							temp =1;
						}
					}
					if(CBSphericalclevisbracket.Checked ==true)
					{
						if(TxtSphericalclevisbraket.Text.Trim() =="")
						{
							lblinfo.Text= "Please enter Spherical Clevis Bracket quantity";
							temp =1;
						}
					}
					if(CBSpericalpp.Checked ==true)
					{
						if(TxtSphericalpp.Text.Trim() =="")
						{
							lblinfo.Text= "Please enter Spherical Pivot Pin quantity";
							temp =1;
						}
					}
					if(temp !=1)
					{
						string pindex=db.SelectPriceIndex("Accessories");
						decimal accindex=0.00m;
						accindex=Convert.ToDecimal(pindex);
						if(CBRodeye.Checked ==true)
						{
							if(TxtRodeye.Text !="" )
							{
								string price="";
								price=db.SelectValue("ListPrice","WEB_Accessories_TableV11","PartNo",DDLRodeye.SelectedItem.Value.ToString().Trim());
								dc2 = Convert.ToDecimal(TxtRodeye.Text); 
								dc3 = Convert.ToDecimal(discount); 
								if(price.Trim() !="")
								{
									dc1 = Convert.ToDecimal(price);
									dc1=dc1 + dc1 * (accindex /100);
									dc4 = dc1 * (1 - (dc3 / 100));
									dc5= dc4 * dc2;
									string str=db.InsertAccessories(quoteno.Trim(),"Rod Eye", DDLRodeye.SelectedItem.Value.ToString().Trim(),"Rod Eye - "+DDLRodeye.SelectedItem.Text.Trim(),dc4.ToString(),dc2.ToString(),dc3.ToString(),dc5.ToString());
					
								}
								else
								{
									string str=db.InsertAccessories(quoteno.Trim(),"Rod Eye", DDLRodeye.SelectedItem.Value.ToString().Trim(),"Rod Eye - "+DDLRodeye.SelectedItem.Text.Trim(),"0.00",dc2.ToString(),dc3.ToString(),"0.00");
									ss =1;
								}
							}
							else
							{
								lblinfo.Text= "Please enter quantity";
							}
						}
						if(CBRodclevis.Checked ==true)
						{
							if(TxtRodclevis.Text !="")
							{
								string price="";
								price=db.SelectValue("ListPrice","WEB_Accessories_TableV11","PartNo",DDLRodclevis.SelectedItem.Value.ToString().Trim());
								dc2 = Convert.ToDecimal(TxtRodclevis.Text); 
								dc3 = Convert.ToDecimal(discount); 
								if(price.Trim() !="")
								{
									dc1 = Convert.ToDecimal(price);
									dc1=dc1 + dc1 * (accindex /100);
									dc4 = dc1 * (1 - (dc3 / 100));
									dc5= dc4 * dc2;
									string str=db.InsertAccessories(quoteno.Trim(),"Rod Clevis", DDLRodclevis.SelectedItem.Value.ToString().Trim(),"Rod Clevis - "+DDLRodclevis.SelectedItem.Text.Trim(),dc4.ToString(),dc2.ToString(),dc3.ToString(),dc5.ToString());
					
								}
								else
								{
									string str=db.InsertAccessories(quoteno.Trim(),"Rod Clevis", DDLRodclevis.SelectedItem.Value.ToString().Trim(),"Rod Clevis - "+DDLRodclevis.SelectedItem.Text.Trim(),"0.00",dc2.ToString(),dc3.ToString(),"0.00");
									ss =1;
								}
							}
							else
							{
								lblinfo.Text= "Please enter quantity";
							}
						}
						if(CBPivotpin.Checked ==true)
						{
							if(txtPivotpin.Text !="")
							{
								string price="";
								price=db.SelectValue("ListPrice","WEB_Accessories_TableV11","PartNo",DDLPivotpin.SelectedItem.Value.ToString().Trim());
								dc2 = Convert.ToDecimal(txtPivotpin.Text); 
								dc3 = Convert.ToDecimal(discount); 
								if(price.Trim() !="")
								{
									dc1 = Convert.ToDecimal(price);
									dc1=dc1 + dc1 * (accindex /100);
									dc4 = dc1 * (1 - (dc3 / 100));
									dc5= dc4 * dc2;
									string str=db.InsertAccessories(quoteno.Trim(),"Pivot Pin", DDLPivotpin.SelectedItem.Value.ToString().Trim(),"Pivot Pin - "+DDLPivotpin.SelectedItem.Text.Trim(),dc4.ToString(),dc2.ToString(),dc3.ToString(),dc5.ToString());
					
								}
								else
								{
									string str=db.InsertAccessories(quoteno.Trim(),"Pivot Pin", DDLPivotpin.SelectedItem.Value.ToString().Trim(),"Pivot Pin - "+DDLPivotpin.SelectedItem.Text.Trim(),"0.00",dc2.ToString(),dc3.ToString(),"0.00");
									ss =1;
								}
							}
							else
							{
								lblinfo.Text= "Please enter quantity";
							}

						}
						if(CBEyeb.Checked ==true)
						{
							if(TxtEyeb.Text !="")
							{
								string price="";
								price=db.SelectValue("ListPrice","WEB_Accessories_TableV11","PartNo",DDLEyeb.SelectedItem.Value.ToString().Trim());
								dc2 = Convert.ToDecimal(TxtEyeb.Text); 
								dc3 = Convert.ToDecimal(discount); 
								if(price.Trim() !="")
								{
									dc1 = Convert.ToDecimal(price);
									dc1=dc1 + dc1 * (accindex /100);
									dc4 = dc1 * (1 - (dc3 / 100));
									dc5= dc4 * dc2;
									string str=db.InsertAccessories(quoteno.Trim(),"Eye Bracket", DDLEyeb.SelectedItem.Value.ToString().Trim(),"Eye Bracket - "+DDLEyeb.SelectedItem.Text.Trim(),dc4.ToString(),dc2.ToString(),dc3.ToString(),dc5.ToString());
					
								}
								else
								{
									string str=db.InsertAccessories(quoteno.Trim(),"Eye Bracket", DDLEyeb.SelectedItem.Value.ToString().Trim(),"Eye Bracket - "+DDLEyeb.SelectedItem.Text.Trim(),dc4.ToString(),"0.00",dc3.ToString(),"0.00");
									ss =1;
								}
							}
							else
							{
								lblinfo.Text= "Please enter quantity";
							}
						}
						if(CBClevisbracket.Checked ==true)
						{
							if(TxtClevisbracket.Text !="")
							{
								string price="";
								price=db.SelectValue("ListPrice","WEB_Accessories_TableV11","PartNo",DDLClevisbracket.SelectedItem.Value.ToString().Trim());
								dc2 = Convert.ToDecimal(TxtClevisbracket.Text); 
								dc3 = Convert.ToDecimal(discount); 
								if(price.Trim() !="")
								{
									dc1 = Convert.ToDecimal(price);
									dc1=dc1 + dc1 * (accindex /100);
									dc4 = dc1 * (1 - (dc3 / 100));
									dc5= dc4 * dc2;
									string str=db.InsertAccessories(quoteno.Trim(),"Clevis Bracket", DDLClevisbracket.SelectedItem.Value.ToString().Trim(),"Clevis Bracket - "+DDLClevisbracket.SelectedItem.Text.Trim(),dc4.ToString(),dc2.ToString(),dc3.ToString(),dc5.ToString());
					
								}
								else
								{
									string str=db.InsertAccessories(quoteno.Trim(),"Clevis Bracket", DDLClevisbracket.SelectedItem.Value.ToString().Trim(),"Clevis Bracket - "+DDLClevisbracket.SelectedItem.Text.Trim(),"0.00",dc2.ToString(),dc3.ToString(),"0.00");
									ss =1;
								}
							}
							else
							{
								lblinfo.Text= "Please enter quantity";
							}
						}
						if(CBLinearcoupler.Checked ==true)
						{
							if(TxtLinearcoupler.Text !="")
							{
								string price="";
								price=db.SelectValue("ListPrice","WEB_Accessories_TableV11","PartNo",DDLLinearcoupler.SelectedItem.Value.ToString().Trim());
								dc2 = Convert.ToDecimal(TxtLinearcoupler.Text); 
								dc3 = Convert.ToDecimal(discount); 
								if(price.Trim() !="")
								{
									dc1 = Convert.ToDecimal(price);
									dc1=dc1 + dc1 * (accindex /100);
									dc4 = dc1 * (1 - (dc3 / 100));
									dc5= dc4 * dc2;
									string str=db.InsertAccessories(quoteno.Trim(),"Linear Alignment Coupler", DDLLinearcoupler.SelectedItem.Value.ToString().Trim(),"Linear Alignment Coupler - "+DDLLinearcoupler.SelectedItem.Text.Trim(),dc4.ToString(),dc2.ToString(),dc3.ToString(),dc5.ToString());
					
								}
								else
								{
									string str=db.InsertAccessories(quoteno.Trim(),"Linear Alignment Coupler", DDLLinearcoupler.SelectedItem.Value.ToString().Trim(),"Linear Alignment Coupler - "+DDLLinearcoupler.SelectedItem.Text.Trim(),"0.00",dc2.ToString(),dc3.ToString(),"0.00");
									ss =1;
								}
							}
							else
							{
								lblinfo.Text= "Please enter quantity";
							}
						}
						if(CBSphericalrodeye.Checked ==true)
						{
							if(TxtSphericalrodeye.Text!="")
							{
								string price="";
								price=db.SelectValue("ListPrice","WEB_Accessories_TableV11","PartNo",DDLSphericalrodeye.SelectedItem.Value.ToString().Trim());
								dc2 = Convert.ToDecimal(TxtSphericalrodeye.Text); 
								dc3 = Convert.ToDecimal(discount); 
								if(price.Trim() !="")
								{
									dc1 = Convert.ToDecimal(price);
									dc1=dc1 + dc1 * (accindex /100);
									dc4 = dc1 * (1 - (dc3 / 100));
									dc5= dc4 * dc2;
									string str=db.InsertAccessories(quoteno.Trim(),"Spherical Rod Eye", DDLSphericalrodeye.SelectedItem.Value.ToString().Trim(),"Spherical Rod Eye - "+DDLSphericalrodeye.SelectedItem.Text.Trim(),dc4.ToString(),dc2.ToString(),dc3.ToString(),dc5.ToString());
					
								}
								else
								{
									string str=db.InsertAccessories(quoteno.Trim(),"Spherical Rod Eye", DDLSphericalrodeye.SelectedItem.Value.ToString().Trim(),"Spherical Rod Eye - "+DDLSphericalrodeye.SelectedItem.Text.Trim(),"0.00",dc2.ToString(),dc3.ToString(),"0.00");
									ss =1;
								}
							}
							else
							{
								lblinfo.Text= "Please enter quantity";
							}
						}
						if(CBSphericalclevisbracket.Checked ==true)
						{
							if(TxtSphericalclevisbraket.Text !="")
							{
								string price="";
								price=db.SelectValue("ListPrice","WEB_Accessories_TableV11","PartNo",DDLSphericalclevisbracket.SelectedItem.Value.ToString().Trim());
								dc2 = Convert.ToDecimal(TxtSphericalclevisbraket.Text); 
								dc3 = Convert.ToDecimal(discount); 
								if(price.Trim() !="")
								{
									dc1 = Convert.ToDecimal(price);
									dc1=dc1 + dc1 * (accindex /100);
									dc4 = dc1 * (1 - (dc3 / 100));
									dc5= dc4 * dc2;
									string str=db.InsertAccessories(quoteno.Trim(),"Spherical Clevis Bracket", DDLSphericalclevisbracket.SelectedItem.Value.ToString().Trim(),"Spherical Clevis Bracket - "+DDLSphericalclevisbracket.SelectedItem.Text.Trim(),dc4.ToString(),dc2.ToString(),dc3.ToString(),dc5.ToString());
					
								}
								else
								{
									string str=db.InsertAccessories(quoteno.Trim(),"Spherical Clevis Bracket", DDLSphericalclevisbracket.SelectedItem.Value.ToString().Trim(),"Spherical Clevis Bracket - "+DDLSphericalclevisbracket.SelectedItem.Text.Trim(),"0.00",dc2.ToString(),dc3.ToString(),"0.00");
									ss =1;
								}
							}
							else
							{
								lblinfo.Text= "Please enter quantity";
							}
						}
						if(CBSpericalpp.Checked ==true)
						{
							if(TxtSphericalpp.Text !="")
							{
								string price="";
								price=db.SelectValue("ListPrice","WEB_Accessories_TableV11","PartNo",DDLSphericalpp.SelectedItem.Value.ToString().Trim());
								dc2 = Convert.ToDecimal(TxtSphericalpp.Text); 
								dc3 = Convert.ToDecimal(discount); 
								if(price.Trim() !="")
								{
									dc1 = Convert.ToDecimal(price);
									dc1=dc1 + dc1 * (accindex /100);
									dc4 = dc1 * (1 - (dc3 / 100));
									dc5= dc4 * dc2;
									string str=db.InsertAccessories(quoteno.Trim(),"Spherical Pivot Pin", DDLSphericalpp.SelectedItem.Value.ToString().Trim(),"Spherical Pivot Pin - "+DDLSphericalpp.SelectedItem.Text.Trim(),dc4.ToString(),dc2.ToString(),dc3.ToString(),dc5.ToString());
					
								}
								else
								{
									string str=db.InsertAccessories(quoteno.Trim(),"Spherical Pivot Pin", DDLSphericalpp.SelectedItem.Value.ToString().Trim(),"Spherical Pivot Pin - "+DDLSphericalpp.SelectedItem.Text.Trim(),"0.00",dc2.ToString(),dc3.ToString(),"0.00");
									ss =1;
								}
							}
							else
							{
								lblinfo.Text= "Please enter quantity";
							}					
						
						}
						if(ss ==1)
						{
							quot.Finish="0";
							string str =db.SelectCustomerQno(quot.QuoteNo.ToString().Trim());
							if(str.ToString() !="")
							{
								string s =db.UpdateCustomerQuote(quot,str.Trim());
							}
							else
							{
								string s =db.InsertCustomerQuote(quot);
							}
							if(Page.IsValid)
							{
								MailMessage mail=new MailMessage();
								SmtpMail.SmtpServer ="k2smtpout.secureserver.net"; 
								mail.From= "admin@cowandynamics.com";
								mail.To = "dtaranu@cowandynamics.com";
								mail.Bcc="admin@cowandynamics.com";
								mail.Cc="jbehara@cowandynamics.com";
								mail.BodyFormat =MailFormat.Html;
								mail.Subject=  " New Quote";
								mail.Priority =MailPriority.Normal;
								mail.Body =						
									"<hr color='#FF0000'>Bonjour, <br> "+lst[0].ToString()+" a une nouvelle demande de prix:"
									+"<TABLE id='Table1' borderColor='#0000ff' cellSpacing='1' cellPadding='1' align='left' border='1'>"
									+"<TR><TD noWrap>Date de demande :</TD><TD noWrap>"+DateTime.Today.ToShortDateString().Trim()+"</TD></TR>"
									+"<TR><TD noWrap>No de demande :</TD><TD noWrap>"+quoteno.Trim()+"</TD></TR>"
									+"<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>Clic sur le lien ci-dessous � r�pondre: <a href='http://172.16.0.253:9000/'>Quote System</a>"
									+"<br><br><hr> Merci <br>I-Cylinder<br><hr color='#FF0000'><br>"								
									+"<hr color='#FF0000'>Hi, <br> "+lst[0].ToString()+" has entered a new request for a quote:"
									+"<TABLE id='Table1' borderColor='#0000ff' cellSpacing='1' cellPadding='1' align='left' border='1'>"
									+"<TR><TD noWrap>Quote Date :</TD><TD noWrap>"+DateTime.Today.ToShortDateString().Trim()+"</TD></TR>"
									+"<TR><TD noWrap>Quote No :</TD><TD noWrap>"+quoteno.Trim()+"</TD></TR>"
									+"<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>Please log into <a href='http://172.16.0.253:9000/'>Quote System</a> to complete the Quote."
									+"<br><br><hr> Thanks <br>I-Cylinder<br><hr color='#FF0000'>";
								//issue #582 start
								mail.Body += csSignature.Get_Admin();
								//issue #582 end
								SmtpMail.Send(mail);
//								Message message = new Message();
//								message.From.Email = lst[1].ToString();
////								message.To.Add( "admin@cowandynamics.com" );
//								message.To.Add( "dtaranu@cowandynamics.com" );
//								message.Cc.Add( "rwenker@cowandynamics.com" );
//								message.Bcc.Add( "admin@cowandynamics.com" );
//								message.Subject ="New Quote ";
//								message.Charset = System.Text.Encoding.GetEncoding("iso-8859-7");
//								message.Priority =Priority.Medium;
//								message.BodyHtml ="<hr color='#FF0000'>Bonjour, <br> "+lst[0].ToString()+" a une nouvelle demande de prix:"
//									+"<TABLE id='Table1' borderColor='#0000ff' cellSpacing='1' cellPadding='1' align='left' border='1'>"
//									+"<TR><TD noWrap>Date de demande :</TD><TD noWrap>"+DateTime.Today.ToShortDateString().Trim()+"</TD></TR>"
//									+"<TR><TD noWrap>No de demande :</TD><TD noWrap>"+quoteno.Trim()+"</TD></TR>"
//									+"<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>Clic sur le lien ci-dessous � r�pondre: <a href='http://172.16.0.253:9000/'>Quote System</a>"
//									+"<br><br><hr> Merci <br>I-Cylinder<br><hr color='#FF0000'><br>"								
//									+"<hr color='#FF0000'>Hi, <br> "+lst[0].ToString()+" has entered a new request for a quote:"
//									+"<TABLE id='Table1' borderColor='#0000ff' cellSpacing='1' cellPadding='1' align='left' border='1'>"
//									+"<TR><TD noWrap>Quote Date :</TD><TD noWrap>"+DateTime.Today.ToShortDateString().Trim()+"</TD></TR>"
//									+"<TR><TD noWrap>Quote No :</TD><TD noWrap>"+quoteno.Trim()+"</TD></TR>"
//									+"<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>Please log into <a href='http://172.16.0.253:9000/'>Quote System</a> to complete the Quote."
//									+"<br><br><hr> Thanks <br>I-Cylinder<br><hr color='#FF0000'>";
//								Smtp.Send( message, "smtpout.secureserver.net", 80, GetDomain( message.From.Email ),SmtpAuthentication.Login,"admin@cowandynamics.com","hockey13" );					
//								//								Smtp.Send( message, "smtp10.bellnet.ca", 25, GetDomain( message.From.Email ) );
								Response.Redirect("Result.aspx?id="+quoteno.Trim());		
							}
						}
						else
						{
							quot.Finish="1";
							string str =db.SelectCustomerQno(quot.QuoteNo.ToString().Trim());
							if(str.ToString() !="")
							{
								string s =db.UpdateCustomerQuote(quot,str.Trim());
							}
							else
							{
								string s =db.InsertCustomerQuote(quot);
							}
							//issue #669 start
							Response.Redirect("ManageQ.aspx?id="+quot.QuoteNo.Trim()+"&pn=1234");
							//issue #669 end
							//HLPrint.NavigateUrl="HTMLQuote.aspx?id="+quoteno.Trim();
							//HLPrint.Visible=true;
							//BtnNext.Visible=false;
						}
						ss=0;
					
					}
					else
					{
						lblinfo.Text="Please enter quantities of all selections";
					}
				}
			}
			else
			{
				lblinfo.Text="There is no accessory available for the selected Rodend,KK or CD";
			}
		}
		private string GetDomain( string email )
		{
			int index = email.IndexOf( '@' );
			return email.Substring( index + 1 );
		}
		public string Qno(string use)
		{
			try
			{
				string qno ="";
				string count = "";
				string usr=use.ToUpper();
				string st= usr.Substring(0,1);
				string s1=DateTime.Today.Month.ToString();
				count=db.SelectLastQno();
				if(count.Length !=0)
				{
					int lst=Convert.ToInt32(count.Substring(5));
				
				
					string s=DateTime.Today.Month.ToString();
					if(s.Length ==1)
					{
						s="0"+DateTime.Today.Month.ToString();
					}
					else
					{
						s=DateTime.Today.Month.ToString();
					}
					if(count !="")
					{
						if(count.Substring(1,2).Equals(DateTime.Today.Year.ToString().Substring(2)))
						{
							if(count.Substring(3,2).Equals(s))
							{
								lst++;
								string num="";
								if(lst.ToString().Length ==4)
								{
									num=lst.ToString();
								}
								else if(lst.ToString().Length ==3)
								{
									num="0"+lst.ToString();
								}
								else if(lst.ToString().Length ==2)
								{
									num="00"+lst.ToString();
								}
								else 
								{
									num="000"+lst.ToString();
								}
							
								if(s1.Length ==1)
								{
									s1="0"+DateTime.Today.Month.ToString();
								}
								else
								{
									s1=DateTime.Today.Month.ToString();
								}
								qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+num.ToString();
							}
							else
							{
						
								if(s1.Length ==1)
								{
									s1="0"+DateTime.Today.Month.ToString();
								}
								else
								{
									s1=DateTime.Today.Month.ToString();
								}
								qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+"0001";
							}
	
						}
						else
						{
						
							if(s1.Length ==1)
							{
								s1="0"+DateTime.Today.Month.ToString();
							}
							else
							{
								s1=DateTime.Today.Month.ToString();
							}
							qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+"0001";
						}
					}
					else
					{
					
						if(s1.Length ==1)
						{
							s1="0"+DateTime.Today.Month.ToString();
						}
						else
						{
							s1=DateTime.Today.Month.ToString();
						}
						qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+"0001";
					}
				}
				else
				{
					qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+"0001";
				}
				string sav=db.InsertQuoteNo(qno);
				db.InsertQuoteCount(qno);
				return qno;
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				lblinfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				return null;
			}
		
		}


		public static bool IsNumeric(string strInteger) 
		{
			try 
			{
					int intTemp =0;
				if(strInteger.ToString().StartsWith(".") == true)
				{
					for(int i=1; i< strInteger.Length;i++)
					{
						intTemp = Int32.Parse( strInteger.Substring(i,1) );
					}
				}
				else
				{
					for(int i=0; i< strInteger.Length;i++)
					{
						if (strInteger.ToString().Substring(i,1) !=".")
						{
							intTemp = Int32.Parse( strInteger.Substring(i,1) );
						}
					}
				}
				return true;
			} 
			catch (FormatException) 
			{
				return false;
			}    
		}
		private void TxtRodeye_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtRodeye.Text.ToString().Trim() !="" && IsNumeric(TxtRodeye.Text.ToString().Trim()) ==true  )
			{
			
				TxtRodeye.Text =String.Format("{0:###}",Convert.ToDecimal(TxtRodeye.Text));
				
			}
			else
			{
				TxtRodeye.Text="";
			}
		}

		private void TxtRodclevis_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtRodclevis.Text.ToString().Trim() !="" && IsNumeric(TxtRodclevis.Text.ToString().Trim()) ==true  )
			{
			
				TxtRodclevis.Text =String.Format("{0:###}",Convert.ToDecimal(TxtRodclevis.Text));
				
			}
			else
			{
				TxtRodclevis.Text="";
			}
		}

		private void txtPivotpin_TextChanged(object sender, System.EventArgs e)
		{
			if(txtPivotpin.Text.ToString().Trim() !="" && IsNumeric(txtPivotpin.Text.ToString().Trim()) ==true  )
			{
			
				txtPivotpin.Text =String.Format("{0:###}",Convert.ToDecimal(txtPivotpin.Text));
				
			}
			else
			{
				txtPivotpin.Text="";
			}
		}

		private void TxtEyeb_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtEyeb.Text.ToString().Trim() !="" && IsNumeric(TxtEyeb.Text.ToString().Trim()) ==true  )
			{
			
				TxtEyeb.Text =String.Format("{0:###}",Convert.ToDecimal(TxtEyeb.Text));
				
			}
			else
			{
				TxtEyeb.Text="";
			}
		}

		private void TxtClevisbracket_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtClevisbracket.Text.ToString().Trim() !="" && IsNumeric(TxtClevisbracket.Text.ToString().Trim()) ==true  )
			{
			
				TxtClevisbracket.Text =String.Format("{0:###}",Convert.ToDecimal(TxtClevisbracket.Text));
				
			}
			else
			{
				TxtClevisbracket.Text="";
			}
		}

		private void TxtLinearcoupler_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtLinearcoupler.Text.ToString().Trim() !="" && IsNumeric(TxtLinearcoupler.Text.ToString().Trim()) ==true  )
			{
			
				TxtLinearcoupler.Text =String.Format("{0:###}",Convert.ToDecimal(TxtLinearcoupler.Text));
				
			}
			else
			{
				TxtLinearcoupler.Text="";
			}
		}

		private void TxtSphericalrodeye_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtSphericalrodeye.Text.ToString().Trim() !="" && IsNumeric(TxtSphericalrodeye.Text.ToString().Trim()) ==true  )
			{
			
				TxtSphericalrodeye.Text =String.Format("{0:###}",Convert.ToDecimal(TxtSphericalrodeye.Text));
				
			}
			else
			{
				TxtSphericalrodeye.Text="";
			}
		}

		private void TxtSphericalclevisbraket_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtSphericalclevisbraket.Text.ToString().Trim() !="" && IsNumeric(TxtSphericalclevisbraket.Text.ToString().Trim()) ==true  )
			{
			
				TxtSphericalclevisbraket.Text =String.Format("{0:###}",Convert.ToDecimal(TxtSphericalclevisbraket.Text));
				
			}
			else
			{
				TxtSphericalclevisbraket.Text="";
			}
		}

		private void TxtSphericalpp_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtSphericalpp.Text.ToString().Trim() !="" && IsNumeric(TxtSphericalpp.Text.ToString().Trim()) ==true  )
			{
			
				TxtSphericalpp.Text =String.Format("{0:###}",Convert.ToDecimal(TxtSphericalpp.Text));
				
			}
			else
			{
				TxtSphericalpp.Text="";
			}
		}

		private void LBHome_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("ManageAcc.aspx");
		}

		
	}
}
