<%@ Page language="c#" Codebehind="ViewUploadedFile_Way.aspx.cs" AutoEventWireup="false" Inherits="iCylinderV1.ViewUploadedFile_Way" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ViewUploadedFile_Way</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:datagrid style="Z-INDEX: 0" id="DGView" runat="server" OnItemDataBound="DGView_Item_Command"
				Font-Size="9pt">
				<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="Gray"></HeaderStyle>
			</asp:datagrid>
			<asp:label style="Z-INDEX: 0" id="lbl" runat="server"></asp:label>
			<asp:label style="Z-INDEX: 0" id="lblspec" runat="server"></asp:label>
			<asp:label style="Z-INDEX: 0" id="Lblerr" runat="server" Font-Size="9pt" Font-Underline="True"
				Font-Bold="True" ForeColor="Red">Excel file Error</asp:label>
			<asp:Label style="Z-INDEX: 0" id="Lblerror" runat="server" Font-Size="9pt" ForeColor="Red"></asp:Label>
		</form>
	</body>
</HTML>
