using System;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for Specials.
	/// </summary>
	public class Specials
	{
		public Specials()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		private string ccode;
		private string spname;
		private string spdesc;
		private string spunitcost;
		private string qty;
		private string markup;
		private string total;

		public string CCode
		{
			get
			{
				return ccode;
			}
			set
			{
				ccode = value;
			}
		}
		public string SPName
		{
			get
			{
				return spname;
			}
			set
			{
				spname = value;
			}
		}
		public string SPDesc
		{
			get
			{
				return spdesc;
			}
			set
			{
				spdesc = value;
			}
		}

		public string UnitCost
		{
			get
			{
				return spunitcost;
			}
			set
			{
				spunitcost = value;
			}
		}
		public string Qty
		{
			get
			{
				return qty;
			}
			set
			{
				qty = value;
			}
		}
		public string MarkUp
		{
			get
			{
				return markup ;
			}
			set
			{
				markup = value;
			}
		}
		public string Total
		{
			get
			{
				return total;
			}
			set
			{
				total = value;
			}
		}
	}
}
