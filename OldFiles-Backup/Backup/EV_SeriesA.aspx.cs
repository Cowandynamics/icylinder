using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for EV_SeriesA.
	/// </summary>
	public class EV_SeriesA : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblhidden;
		protected System.Web.UI.WebControls.LinkButton BtnNext;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Panel Panel3;
		protected System.Web.UI.WebControls.Label Label17;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Panel P2ndRodnd;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.Panel Panel7;
		protected System.Web.UI.WebControls.RadioButtonList RBLRodend1;
		protected System.Web.UI.WebControls.Image Img1stRodEnd;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Panel Panel5;
		protected System.Web.UI.WebControls.RadioButtonList RBLMount;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Label Label18;
		protected System.Web.UI.WebControls.DropDownList DDLValveSize;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Lblas;
		protected System.Web.UI.WebControls.Label Label20;
		protected System.Web.UI.WebControls.Panel Panel4;
		protected System.Web.UI.WebControls.TextBox TxtLinearP;
		protected System.Web.UI.WebControls.TextBox TxtMinAS;
		protected System.Web.UI.WebControls.TextBox TxtStroke;
		protected System.Web.UI.WebControls.Label Lbllp;
		protected System.Web.UI.WebControls.Panel Panel9;
		protected System.Web.UI.WebControls.Label Lblp1;
		protected System.Web.UI.WebControls.Panel P1stAdvanced;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.RadioButtonList RBLStyle;
		protected System.Web.UI.WebControls.Label Lblval;
		DBClass db=new DBClass();
		protected System.Web.UI.WebControls.Label lblpage;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.RadioButtonList RBLFunnel;
		protected System.Web.UI.WebControls.Label lblstroke;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Panel Panel1;
		protected System.Web.UI.WebControls.RadioButtonList RBLPaint;
		protected System.Web.UI.WebControls.DropDownList DDLMount;
		protected System.Web.UI.WebControls.Label Label22;
		protected System.Web.UI.WebControls.Panel Panel10;
		protected System.Web.UI.WebControls.RadioButtonList RBLCylType;
		protected System.Web.UI.WebControls.RadioButtonList RBLPRod;
		protected System.Web.UI.WebControls.Label Lblvalve;
		protected System.Web.UI.WebControls.Panel PanelLP;
		protected System.Web.UI.WebControls.Panel PanelMAS;
		protected System.Web.UI.WebControls.Panel PanelM;
		protected System.Web.UI.WebControls.Label lblthrust;
		protected System.Web.UI.WebControls.TextBox TxtThrust;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.WebControls.Panel PanelT;
		protected System.Web.UI.WebControls.LinkButton LBBack;
		protected System.Web.UI.WebControls.Label lbls;
		protected System.Web.UI.WebControls.Label lblfstyle;
		protected System.Web.UI.WebControls.Label lblmount;
		coder cd1=new coder();
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["User"] !=null)
			{
				if(! IsPostBack)
				{
					if(Request.QueryString["p"] !=null)
					{
						if(Request.QueryString["p"].ToString() =="new")
						{
							Session["Coder"] =null;
						}
					}
					PanelT.Visible=false;
					lblpage.Visible=false;
					Lblp1.Text="";
					lblhidden.Text="";
					ListItem lite=new ListItem();
//					lite=new ListItem((String.Format("NX3<br /><img alt='NX3 mount: Extended tie rod head end with 4 extra tie rod nuts' src='{0}'>", "mounts\\MX3.jpg")), "NX3");
//					RBLMount.Items.Add(lite);	
//					lite=new ListItem((String.Format("NX1<br /><img alt='NX1 mount: Extended tie rod both ends with 4 extra tie rod nuts' src='{0}'>", "mounts\\MX1.jpg")), "NX1");
//					RBLMount.Items.Add(lite);
//					lite=new ListItem("Other-Mounts", "OTH");
//					RBLMount.Items.Add(lite);
//					RBLMount.SelectedIndex=0;
					RBLCylType.Items.Clear();
					lite=new ListItem((String.Format("Single Ended<br /><img alt='Single Ended Cylinder' src='{0}'>", "images\\single.jpg")), "Sigle");
					RBLCylType.Items.Add(lite);	
					lite=new ListItem((String.Format("Double Ended<br /><img alt='Double Ended Cylinder' src='{0}'>", "images\\double.jpg")), "Double");
					RBLCylType.Items.Add(lite);
					RBLCylType.SelectedIndex=0;

					Img1stRodEnd.ImageUrl="rodends\\smallfemale.jpg";
					RBLRodend1.SelectedValue ="A4";
					if(Session["Coder"] !=null)
					{
						cd1=new coder();
						cd1 =(coder)Session["Coder"];
						if(cd1.Series !=null)
						{
							if(cd1.Series.ToString().Trim() =="AT")
							{
								RBLCylType.Items.Clear();
								lite=new ListItem((String.Format("Single Ended<br /><img alt='Single Ended Cylinder' src='{0}'>", "images\\single.jpg")), "Sigle");
								RBLCylType.Items.Add(lite);	
								RBLCylType.SelectedIndex=0;
								lbls.Text="Series  AT - EVR Cylinder with Transducer";
							}
							else
							{
								RBLCylType.Items.Clear();
								lite=new ListItem((String.Format("Single Ended<br /><img alt='Single Ended Cylinder' src='{0}'>", "images\\single.jpg")), "Sigle");
								RBLCylType.Items.Add(lite);	
								lite=new ListItem((String.Format("Double Ended<br /><img alt='Double Ended Cylinder' src='{0}'>", "images\\double.jpg")), "Double");
								RBLCylType.Items.Add(lite);
								RBLCylType.SelectedIndex=0;
								lbls.Text="Series  A - EVR Cylinder";
							}
						}
						if(cd1.FrameStyle !=null)
						{
							lblfstyle.Text=cd1.FrameStyle.ToString();
						}
						if(cd1.ValveSize !=null)
						{
							DDLValveSize.SelectedValue=cd1.ValveSize.Trim();
							DDLValveSize_SelectedIndexChanged(sender,e);
							if(DDLValveSize.SelectedItem.Text.ToString().Trim()== "1.5" || DDLValveSize.SelectedItem.Text.ToString().Trim()== "2.5")
							{
								if(cd1.SeatingTrust !=null)
								{
									TxtThrust.Text=cd1.SeatingTrust.Trim();
								}
								TxtLinearP.Text="";
							}
							else
							{
								
								TxtThrust.Text="";
							}
						}
						if(cd1.LinearPressure !=null)
						{
							if(cd1.LinearPressure.ToString().Trim() !="")
							{
								TxtLinearP.Text=cd1.LinearPressure.Trim();
								TxtLinearP_TextChanged(sender,e);
							}
						}
						if(cd1.MinAirSupply !=null)
						{
							TxtMinAS.Text=cd1.MinAirSupply.Trim();
							TxtMinAS_TextChanged(sender,e);
						}
						if(cd1.Stroke !=null)
						{
							if(Convert.ToDecimal(TxtStroke.Text.Trim()) != Convert.ToDecimal(cd1.Stroke.Trim()))
							{
								RBLFunnel.SelectedIndex=0;
								RBLFunnel_SelectedIndexChanged(sender,e);
								TxtStroke.Text=cd1.Stroke.Trim();
								TxtStroke_TextChanged(sender,e);
							}
						}
						if(cd1.Mount !=null)
						{
							if(cd1.Mount.Trim().Substring(0,1) =="N")
							{
								RBLMount.SelectedValue="OTH";
								RBLMount_SelectedIndexChanged(sender,e);
								DDLMount.SelectedValue=cd1.Mount.Trim();
							}
							else
							{
								RBLMount.SelectedValue=cd1.Mount.Trim();
								RBLStyle.SelectedValue=cd1.Style.Trim();
							}
						}
						if(cd1.SSPistionRod !=null)
						{
							RBLPRod.SelectedValue=cd1.SSPistionRod.Trim();
						}
						
						if(cd1.Coating !=null)
						{
							RBLPaint.SelectedValue=cd1.Coating.Trim();
						}
						if(cd1.DoubleRod !=null)
						{
							if(cd1.DoubleRod.ToString().ToUpper() =="YES")
							{
								RBLCylType.SelectedIndex =1;
							}
						}
					}
				}
			}
			else
			{
				Response.Redirect("Login.aspx");
			}
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.DDLValveSize.SelectedIndexChanged += new System.EventHandler(this.DDLValveSize_SelectedIndexChanged);
			this.TxtLinearP.TextChanged += new System.EventHandler(this.TxtLinearP_TextChanged);
			this.TxtMinAS.TextChanged += new System.EventHandler(this.TxtMinAS_TextChanged);
			this.TxtThrust.TextChanged += new System.EventHandler(this.TxtThrust_TextChanged);
			this.RBLFunnel.SelectedIndexChanged += new System.EventHandler(this.RBLFunnel_SelectedIndexChanged);
			this.TxtStroke.TextChanged += new System.EventHandler(this.TxtStroke_TextChanged);
			this.RBLMount.SelectedIndexChanged += new System.EventHandler(this.RBLMount_SelectedIndexChanged);
			this.LBBack.Click += new System.EventHandler(this.LBBack_Click);
			this.BtnNext.Click += new System.EventHandler(this.BtnNext_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		private void DDLValveSize_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(DDLValveSize.SelectedIndex >0)
			{
				if(DDLValveSize.SelectedItem.Text.ToString().Trim()== "1.5" || DDLValveSize.SelectedItem.Text.ToString().Trim()== "2.5")
				{
					if(lblfstyle.Text.ToString().Trim() =="CF")
					{
						PanelLP.Visible=false;
						PanelT.Visible=true;
						Lblvalve.Text="GR 2200 Valve";
						string p1="";
						p1=db.SelectValue("P1","WEB_EVValve_TableV1","ValveSize",DDLValveSize.SelectedItem.Text.Trim());
						Lblp1.Text=p1.ToString();
						decimal dd=0.00m;
						if(DDLValveSize.SelectedItem.Text.ToString().Trim()== "1.5") 
						{
							dd=0.25m;
							RBLMount.Items.Clear();
							ListItem lite=new ListItem();
							lite=new ListItem((String.Format("GR15<br /><img alt='GR15 mount: Valve free issue by Customer and mounted by Cowan' src='{0}'>", "mounts\\MX0.jpg")), "GR15");
							RBLMount.Items.Add(lite);
							lite=new ListItem("Other-Mounts", "OTH");
							RBLMount.Items.Add(lite);
							RBLMount.SelectedIndex=0;
						}
						else if(DDLValveSize.SelectedItem.Text.ToString().Trim()== "2.5")
						{
							dd=0.00m;
							RBLMount.Items.Clear();
							ListItem lite=new ListItem();
							lite=new ListItem((String.Format("GR25<br /><img alt='GR25 mount: Valve free issue by Customer and mounted by Cowan' src='{0}'>", "mounts\\MX0.jpg")), "GR25");
							RBLMount.Items.Add(lite);
							lite=new ListItem("Other-Mounts", "OTH");
							RBLMount.Items.Add(lite);
							RBLMount.SelectedIndex=0;
						}
						decimal d1=0.00m;
						d1=Convert.ToDecimal(DDLValveSize.SelectedItem.Text.ToString());
						TxtStroke.Text=String.Format("{0:##0.00}",d1 + dd);
						RBLFunnel.SelectedIndex=1;
						RBLFunnel_SelectedIndexChanged(sender,e);
					}
					else if(lblfstyle.Text.ToString().Trim() =="OF")
					{
						PanelLP.Visible=true;
						PanelT.Visible=false;
						Lblvalve.Text="EVR 1200/2400 Valve";
						string p1="";
						p1=db.SelectValue("P1","WEB_EVValve_TableV1","ValveSize",DDLValveSize.SelectedItem.Text.Trim());
						Lblp1.Text=p1.ToString();
						decimal dd=0.00m;
						dd=0.25m;
						decimal d1=0.00m;
						d1=Convert.ToDecimal(DDLValveSize.SelectedItem.Text.ToString());
						TxtStroke.Text=String.Format("{0:##0.00}",d1 + dd);
						RBLFunnel.SelectedIndex=1;
						RBLFunnel_SelectedIndexChanged(sender,e);
						RBLMount.Items.Clear();
						if(DDLValveSize.SelectedIndex >0 && TxtLinearP.Text.Trim() !="" && TxtMinAS.Text.Trim() !="")
						{
							Find_Mount();
						}
					}
				}
				else
				{
					PanelLP.Visible=true;
					PanelT.Visible=false;
					Lblvalve.Text="EVR 1200/2400 Valve";
					string p1="";
					p1=db.SelectValue("P1","WEB_EVValve_TableV1","ValveSize",DDLValveSize.SelectedItem.Text.Trim());
					Lblp1.Text=p1.ToString();
					decimal dd=0.00m;
					dd=0.25m;
					decimal d1=0.00m;
					d1=Convert.ToDecimal(DDLValveSize.SelectedItem.Text.ToString());
					TxtStroke.Text=String.Format("{0:##0.00}",d1 + dd);
					RBLFunnel.SelectedIndex=1;
					RBLFunnel_SelectedIndexChanged(sender,e);
					RBLMount.Items.Clear();
//					ListItem lite=new ListItem();
//					lite=new ListItem((String.Format("NX3<br /><img alt='NX3 mount: Extended tie rod head end with 4 extra tie rod nuts' src='{0}'>", "mounts\\MX3.jpg")), "NX3");
//					RBLMount.Items.Add(lite);	
//					lite=new ListItem((String.Format("NX1<br /><img alt='NX1 mount: Extended tie rod both ends with 4 extra tie rod nuts' src='{0}'>", "mounts\\MX1.jpg")), "NX1");
//					RBLMount.Items.Add(lite);
//					lite=new ListItem("Other-Mounts", "OTH");
//					RBLMount.Items.Add(lite);
//					RBLMount.SelectedIndex=0;
					if(DDLValveSize.SelectedIndex >0 && TxtLinearP.Text.Trim() !="" && TxtMinAS.Text.Trim() !="")
					{
						Find_Mount();
					}
				}
				
			}
		}
		public static bool IsNumeric(string strInteger) 
		{
			try 
			{
				int intTemp =0;
				if(strInteger.ToString().StartsWith(".") == true)
				{
					for(int i=1; i< strInteger.Length;i++)
					{
						intTemp = Int32.Parse( strInteger.Substring(i,1) );
					}
				}
				else
				{
					for(int i=0; i< strInteger.Length;i++)
					{
						if (strInteger.ToString().Substring(i,1) !=".")
						{
							intTemp = Int32.Parse( strInteger.Substring(i,1) );
						}
					}
				}
				return true;
			} 
			catch (FormatException) 
			{
				return false;
			}    
		}
		private void TxtLinearP_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtLinearP.Text.ToString().Trim() !="" && IsNumeric(TxtLinearP.Text.ToString().Trim()) ==true  )
			{
			
				TxtLinearP.Text =String.Format("{0:##0}",Convert.ToDecimal(TxtLinearP.Text));
				if(DDLValveSize.SelectedIndex >0 && TxtLinearP.Text.Trim() !="" && TxtMinAS.Text.Trim() !="")
				{
					Find_Mount();
				}
				if(Lblp1.Text.Trim() =="")
				{
					Lblval.Visible=true;	
				}
				else
				{
					if(Convert.ToInt32(TxtLinearP.Text) > Convert.ToInt32(Lblp1.Text))
					{
						Lbllp.Visible=true;
					}
					else if(Convert.ToInt32(TxtLinearP.Text) == 0)
					{
						TxtLinearP.Text="1";
					}
					else
					{
						Lbllp.Visible=false;
					}
				}
			}
			else
			{
				TxtLinearP.Text="";
			}
		}

		private void TxtMinAS_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtMinAS.Text.ToString().Trim() !="" && IsNumeric(TxtMinAS.Text.ToString().Trim()) ==true  )
			{
			
				TxtMinAS.Text =String.Format("{0:##0}",Convert.ToDecimal(TxtMinAS.Text));
				if(Convert.ToInt32(TxtMinAS.Text) > 150)
				{
					Lblas.Visible=true;
					TxtMinAS.Text="150";
					Lblas.Text="Min Air Supply is greater than Series A capacity. The Sizing program will use 150 PSI";
					
				}
				else if(Convert.ToInt32(TxtMinAS.Text) < 25)
				{
					Lblas.Visible=true;
					TxtMinAS.Text="25";
					Lblas.Text="Min Air Supply is not high enough to ensure smooth operation. The Sizing program will use 25 PSI";
				}
				else
				{
					
					Lblas.Visible=false;
				}
				if(DDLValveSize.SelectedIndex >0 && TxtLinearP.Text.Trim() !="" && TxtMinAS.Text.Trim() !="")
				{
					Find_Mount();
				}
			}
			else
			{
				TxtMinAS.Text="";
			}
		}
		private string RealBore(decimal area)
		{
			string OriginalB="";
			double b=0.00;
			decimal bore=0.00m;
			b=Convert.ToDouble(area) / Math.PI;
			bore=Convert.ToDecimal(( Math.Sqrt(b)) * 2);
			if(bore > 0.00m && bore < 4.00m )
			{
				OriginalB="H";
			}
			else if(bore > 4.00m && bore < 5.00m )
			{
				OriginalB="K";
			}
			else if(bore > 5.00m && bore < 6.00m )
			{
				OriginalB="L";
			}
			else if(bore > 6.00m && bore < 7.00m )
			{
				OriginalB="M";
			}
			else if(bore > 7.00m && bore < 8.00m )
			{
				OriginalB="N";
			}
			else if(bore > 8.00m && bore < 10.00m )
			{
				OriginalB="P";
			}
			else if(bore > 10.00m && bore < 12.00m )
			{
				OriginalB="R";
			}
			else if(bore > 12.00m && bore < 14.00m )
			{
				OriginalB="S";
			}
			else if(bore > 14.00m && bore < 16.00m )
			{
				OriginalB="T";
			}
			else if(bore > 16.00m && bore < 18.00m )
			{
				OriginalB="W";
			}
			else if(bore > 18.00m && bore < 20.00m )
			{
				OriginalB="X";
			}
			else if(bore > 20.00m && bore < 22.00m )
			{
				OriginalB="A";
			}
			else if(bore > 22.00m && bore < 24.00m )
			{
				OriginalB="Y";
			}
			else
			{
				OriginalB="Z";
			}
			return OriginalB;
		}
		private decimal RealBoreValue(decimal area)
		{
			decimal bb=0.00m;
			double b=0.00;
			decimal bore=0.00m;
			b=Convert.ToDouble(area) / Math.PI;
			bore=Convert.ToDecimal(( Math.Sqrt(b)) * 2);
			if(bore > 0.00m && bore < 4.00m )
			{
				bb=4.00m;
			}
			else if(bore > 4.00m && bore < 5.00m )
			{
				bb=5.00m;
			}
			else if(bore > 5.00m && bore < 6.00m )
			{
				bb=6.00m;
			}
			else if(bore > 6.00m && bore < 7.00m )
			{
				bb=7.00m;
			}
			else if(bore > 7.00m && bore < 8.00m )
			{
				bb=8.00m;
			}
			else if(bore > 8.00m && bore < 10.00m )
			{
				bb=10.00m;
			}
			else if(bore > 10.00m && bore < 12.00m )
			{
				bb=12.00m;
			}
			else if(bore > 12.00m && bore < 14.00m )
			{
				bb=14.00m;
			}
			else if(bore > 14.00m && bore < 16.00m )
			{
				bb=16.00m;
			}
			else if(bore > 16.00m && bore < 18.00m )
			{
				bb=18.00m;
			}
			else if(bore > 18.00m && bore < 20.00m )
			{
				bb=20.00m;
			}
			else if(bore > 20.00m && bore < 22.00m )
			{
				bb=22.00m;
			}
			else if(bore > 22.00m && bore < 24.00m )
			{
				bb=24.00m;
			}
			return bb;
		}
		public void Find_Mount()
		{
			RBLMount.RepeatColumns=1;
			RBLMount.RepeatDirection=RepeatDirection.Vertical;
			lblmount.Visible=false;
			int seatingT=0;
			decimal cylinderA=0.00m;
			string st1="";
			string st2="";
			st1=db.SelectValue("St1","WEB_EVValve_TableV1","ValveSize",DDLValveSize.SelectedItem.Text.Trim());
			st2=db.SelectValue("St2","WEB_EVValve_TableV1","ValveSize",DDLValveSize.SelectedItem.Text.Trim());
			decimal ST=0.00m;
			decimal linep=0.00m;
			decimal stt1=0.00m;
			decimal stt2=0.00m;
			stt1=Convert.ToDecimal(st1);
			stt2=Convert.ToDecimal(st2);
			linep=Convert.ToInt32(TxtLinearP.Text);
			ST=linep * stt1 + stt2;
			seatingT =Convert.ToInt32(Decimal.Round(ST,0));
			decimal map=0.00m;
			map=Convert.ToInt32(TxtMinAS.Text);
			cylinderA= seatingT / map;
			string OriginalB="";
			OriginalB=RealBore(cylinderA);
			ArrayList mlist=new ArrayList();
			ArrayList mlist1=new ArrayList();
			ArrayList mlist2=new ArrayList();
			mlist=db.Select_Mount_Thrust("SELECT Mounts,MaxThrust FROM WEB_EVR_Mount_Valve_TableV1 Where F"+DDLValveSize.SelectedItem.Text.Trim().Replace(".","_")+"="+DDLValveSize.SelectedItem.Text.Trim());
			mlist1=(ArrayList)mlist[0];
			mlist2=(ArrayList)mlist[1];
			string mount="";
			RBLMount.Items.Clear();
			if(mlist1.Count > 0)
			{
				decimal bore=0.00m;
				decimal cylthrust=0.00m;
				bore=RealBoreValue(cylinderA);
				if(bore.ToString().Trim() =="0")
				{
					RBLMount.RepeatColumns=2;
					RBLMount.RepeatDirection=RepeatDirection.Horizontal;
					lblmount.Visible=true;
					ListItem lite=new ListItem();
					lite=new ListItem((String.Format("NX3<br /><img alt='NX3 mount: Extended tie rod head end with 4 extra tie rod nuts' src='{0}'>", "mounts\\MX3.jpg")), "NX3");
					RBLMount.Items.Add(lite);	
					lite=new ListItem((String.Format("NX1<br /><img alt='NX1 mount: Extended tie rod both ends with 4 extra tie rod nuts' src='{0}'>", "mounts\\MX1.jpg")), "NX1");
					RBLMount.Items.Add(lite);
					RBLMount.SelectedIndex=0;
				}
				else
				{
					cylthrust=(bore * bore) * 0.785m * map;
					ArrayList blist=new ArrayList();
					ArrayList blist1=new ArrayList();
					ArrayList blist2=new ArrayList();
					blist=db.Select_Mount_Thrust("SELECT Mounts,MaxThrust FROM WEB_EVR_Mount_Bore_TableV1 Where "+OriginalB.ToString().Trim()+"='"+OriginalB.ToString().Trim()+"'");
					blist1=(ArrayList)blist[0];
					blist2=(ArrayList)blist[1];
					if(blist1.Count >0)
					{
						for(int i=0;i<mlist1.Count;i++)
						{
							for(int j=0;j<blist1.Count;j++)
							{
								if(mlist1[i].ToString().Trim() == blist1[j].ToString().Trim() )
								{
									decimal tr=0.00m;
									tr=Convert.ToDecimal(mlist2[i].ToString());
									if(cylthrust < tr)
									{
										if(mount.Trim() =="")
										{
											mount=	mlist1[i].ToString().Trim();
										}
									}
								}
							}
						}
					}
					if(mount.Trim() !="")
					{
						string mdesc="";
						mdesc=db.SelectValue("Mount_Type","WEB_MountA_TableV1","Mount_Code",mount.ToString().Trim()).ToString();
						RBLMount.Items.Clear();
						ListItem lite=new ListItem();
						lite=new ListItem((String.Format(mount.ToString().Replace("M","FA")+"<br /><img alt='"+mdesc.ToString().Trim()+"' src='{0}'>", "mounts\\MM07.jpg")), mount.ToString());
						RBLMount.Items.Add(lite);
						lite=new ListItem("Other-Mounts", "OTH");
						RBLMount.Items.Add(lite);
						RBLMount.SelectedIndex=0;
					}
					else if(mount.Trim() =="")
					{
						RBLMount.RepeatColumns=2;
						RBLMount.RepeatDirection=RepeatDirection.Horizontal;
						lblmount.Visible=true;
						ListItem lite=new ListItem();
						lite=new ListItem((String.Format("NX3<br /><img alt='NX3 mount: Extended tie rod head end with 4 extra tie rod nuts' src='{0}'>", "mounts\\MX3.jpg")), "NX3");
						RBLMount.Items.Add(lite);	
						lite=new ListItem((String.Format("NX1<br /><img alt='NX1 mount: Extended tie rod both ends with 4 extra tie rod nuts' src='{0}'>", "mounts\\MX1.jpg")), "NX1");
						RBLMount.Items.Add(lite);
						RBLMount.SelectedIndex=0;
					}
				}
			}
			
		}
		private void BtnNext_Click(object sender, System.EventArgs e)
		{
			if(DDLValveSize.SelectedIndex >0 && TxtLinearP.Text.Trim() !="" && TxtMinAS.Text.Trim() !="")
			{
				lblpage.Visible=false;
				int seatingT=0;
				decimal cylinderA=0.00m;
				string rod="";
				string st1="";
				string st2="";
				st1=db.SelectValue("St1","WEB_EVValve_TableV1","ValveSize",DDLValveSize.SelectedItem.Text.Trim());
				st2=db.SelectValue("St2","WEB_EVValve_TableV1","ValveSize",DDLValveSize.SelectedItem.Text.Trim());
				decimal ST=0.00m;
				decimal linep=0.00m;
				decimal stt1=0.00m;
				decimal stt2=0.00m;
				stt1=Convert.ToDecimal(st1);
				stt2=Convert.ToDecimal(st2);
				linep=Convert.ToInt32(TxtLinearP.Text);
				ST=linep * stt1 + stt2;
				seatingT =Convert.ToInt32(Decimal.Round(ST,0));
				decimal map=0.00m;
				map=Convert.ToInt32(TxtMinAS.Text);
				cylinderA= seatingT / map;
				string OriginalB="";
				OriginalB=RealBore(cylinderA);
				if(OriginalB.Trim() !="Z")
				{
					rod=db.SelectValue("Rod_Code","WEB_RodSerA_TableV1",OriginalB.Trim(),"1");
				}
				else
				{
					rod="Z";
				}
				cd1=new coder();
				cd1=(coder)Session["Coder"];
				cd1.Bore_Size =OriginalB.Trim();
				cd1.Stroke =TxtStroke.Text.Trim();
				cd1.Rod_Diamtr=rod.Trim();
				cd1.Rod_End ="A4";
				string SecBore="";
				string secrod="";
				if(RBLCylType.SelectedIndex ==0)
				{
					cd1.DoubleRod ="No";
					cd1.SecRodDiameter="";
					cd1.SecRodEnd="";
				}	
				else if(RBLCylType.SelectedIndex ==1)
				{
					cd1.DoubleRod ="Yes";
					cd1.SecRodDiameter="D"+rod.Trim()+"2";
					cd1.SecRodEnd="RA4";
					while(rod.Trim() !=secrod.Trim())
					{
						decimal dd1=0.00m;
						decimal cylarea=0.00m;
						decimal consta=0.785m;
						string rvalue="";
						rvalue=db.SelectValue("Rod","WEB_RodSerA_TableV1","Rod_Code",cd1.Rod_Diamtr.Trim());
						dd1=Convert.ToDecimal(rvalue.Trim());
						cylarea=cylinderA -(consta *(dd1 * dd1));
				
						SecBore=RealBore(cylarea);
						if(OriginalB.Trim() !=SecBore.Trim())
						{
							cd1.Bore_Size=SecBore.Trim();
							if(SecBore.Trim() !="Z")
							{
								secrod=db.SelectValue("Rod_Code","WEB_RodSerA_TableV1",SecBore.Trim(),"1");
							}
							else
							{
								secrod="Z";
							}
							if(rod.Trim() !=secrod.Trim())
							{
								cd1.Rod_Diamtr=secrod.Trim();
								cd1.SecRodDiameter="D"+secrod.Trim()+"2";
								rod=secrod.Trim();
							}
						}
						else
						{
							secrod=rod.Trim();
						}
					}
				}
				cd1.Cushions="8";
				cd1.CushionPosition="";
				if(RBLMount.SelectedItem.Value.ToString().Trim() =="OTH")
				{
					cd1.Mount=DDLMount.SelectedItem.Value.ToString().Trim();
				}
				else
				{
					cd1.Mount=RBLMount.SelectedItem.Value.ToString().Trim();
				}
				cd1.Port_Type="N";
				cd1.Port_Pos="11";
				cd1.Seal_Comp="N";
				if(RBLPRod.SelectedIndex ==1)
				{
					cd1.SSPistionRod=RBLPRod.SelectedItem.Value.Trim();
				}
				if(RBLPaint.SelectedIndex ==1)
				{
					cd1.Coating="C1";
				}
				cd1.Style=RBLStyle.SelectedItem.Value.Trim();
				cd1.ValveSize=DDLValveSize.SelectedItem.Text.Trim();
				cd1.LinearPressure=TxtLinearP.Text.Trim();
				cd1.MinAirSupply=TxtMinAS.Text.Trim();
				cd1.SeatingTrust=seatingT.ToString().Trim();
				cd1.Specials="NO";
				if(RBLStyle.SelectedIndex > 0)
				{		
					cd1.Bore_Size="Z";
					cd1.Rod_Diamtr="Z";
					cd1.Specials="YES";
				}
				Session["Coder"] =cd1;
				if(cd1.Series.ToString().Trim() =="A")
				{
					Response.Redirect("EV_SeriesA_Specials.aspx");
				}
				else if(cd1.Series.ToString().Trim() =="AT")
				{
					Response.Redirect("EV_SeriesAT_Trans.aspx");
				}
			}
			else if(DDLValveSize.SelectedIndex >0 && TxtThrust.Text.Trim() !="" && TxtMinAS.Text.Trim() !="")
			{
				lblpage.Visible=false;
				int seatingT=0;
				decimal cylinderA=0.00m;
				string rod="";
				seatingT =Convert.ToInt32(TxtThrust.Text.ToString());
				decimal map=0.00m;
				map=Convert.ToInt32(TxtMinAS.Text);
				cylinderA= seatingT / map;
				string OriginalB="";
				OriginalB=RealBore(cylinderA);
				if(OriginalB.Trim() !="Z")
				{
					rod=db.SelectValue("Rod_Code","WEB_RodSerA_TableV1",OriginalB.Trim(),"1");
				}
				else
				{
					rod="Z";
				}
				cd1=new coder();
				cd1=(coder)Session["Coder"];
				cd1.Bore_Size =OriginalB.Trim();
				cd1.Stroke =TxtStroke.Text.Trim();
				cd1.Rod_Diamtr=rod.Trim();
				cd1.Rod_End ="A4";
				string SecBore="";
				string secrod="";
				if(RBLCylType.SelectedIndex ==0)
				{
					cd1.DoubleRod ="No";
					cd1.SecRodDiameter="";
					cd1.SecRodEnd="";
				}	
				else if(RBLCylType.SelectedIndex ==1)
				{
					cd1.DoubleRod ="Yes";
					cd1.SecRodDiameter="D"+rod.Trim()+"2";
					cd1.SecRodEnd="RA4";
					while(rod.Trim() !=secrod.Trim())
					{
						decimal dd1=0.00m;
						decimal cylarea=0.00m;
						decimal consta=0.785m;
						string rvalue="";
						rvalue=db.SelectValue("Rod","WEB_RodSerA_TableV1","Rod_Code",cd1.Rod_Diamtr.Trim());
						dd1=Convert.ToDecimal(rvalue.Trim());
						cylarea=cylinderA -(consta *(dd1 * dd1));
				
						SecBore=RealBore(cylarea);
						if(OriginalB.Trim() !=SecBore.Trim())
						{
							cd1.Bore_Size=SecBore.Trim();
							if(SecBore.Trim() !="Z")
							{
								secrod=db.SelectValue("Rod_Code","WEB_RodSerA_TableV1",SecBore.Trim(),"1");
							}
							else
							{
								secrod="Z";
							}
							if(rod.Trim() !=secrod.Trim())
							{
								cd1.Rod_Diamtr=secrod.Trim();
								cd1.SecRodDiameter="D"+secrod.Trim()+"2";
								rod=secrod.Trim();
							}
						}
						else
						{
							secrod=rod.Trim();
						}
					}
				}
				cd1.Cushions="8";
				cd1.CushionPosition="";
				if(RBLMount.SelectedItem.Value.ToString().Trim() =="OTH")
				{
					cd1.Mount=DDLMount.SelectedItem.Value.ToString().Trim();
				}
				else
				{
					cd1.Mount=RBLMount.SelectedItem.Value.ToString().Trim();
				}
				cd1.Port_Type="N";
				cd1.Port_Pos="11";
				cd1.Seal_Comp="N";
				if(RBLPRod.SelectedIndex ==1)
				{
					cd1.SSPistionRod=RBLPRod.SelectedItem.Value.Trim();
				}
				if(RBLPaint.SelectedIndex ==1)
				{
					cd1.Coating="C1";
				}
				cd1.Style=RBLStyle.SelectedItem.Value.Trim();
				cd1.ValveSize=DDLValveSize.SelectedItem.Text.Trim();
				cd1.LinearPressure="Not specified";
				cd1.MinAirSupply=TxtMinAS.Text.Trim();
				cd1.SeatingTrust=seatingT.ToString().Trim();
				cd1.Specials="NO";
				if(RBLStyle.SelectedIndex > 0)
				{		
					cd1.Bore_Size="Z";
					cd1.Rod_Diamtr="Z";
					cd1.Specials="YES";
				}
				Session["Coder"] =cd1;
				if(cd1.Series.ToString().Trim() =="A")
				{
					Response.Redirect("EV_SeriesA_Specials.aspx");
				}
				else if(cd1.Series.ToString().Trim() =="AT")
				{
					Response.Redirect("EV_SeriesAT_Trans.aspx");
				}
			}				 
			else
			{
				lblpage.Visible=true;
			}
		}

		private void RBLFunnel_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(DDLValveSize.SelectedIndex >0)
			{
				if(RBLFunnel.SelectedIndex ==0)
				{
					TxtStroke.ReadOnly=false;
					decimal dd,d1=0.00m;
					dd=0.25m;
					d1=Convert.ToDecimal(DDLValveSize.SelectedItem.Text.ToString());
					TxtStroke.Text=String.Format("{0:##0.00}",d1 + dd);
					lblstroke.Text="Stroke must be between 0.00 and "+String.Format("{0:##0.00}",d1 + dd);
					lblstroke.Visible=true;
				}
				else
				{
					TxtStroke.ReadOnly=true;
					decimal dd,d1=0.00m;
					dd=0.25m;
					if(DDLValveSize.SelectedItem.Text.ToString().Trim()== "1.5") 
					{
						dd=0.25m;
						RBLMount.Items.Clear();
						ListItem lite=new ListItem();
						lite=new ListItem((String.Format("GR15<br /><img alt='GR15 mount: Valve free issue by Customer and mounted by Cowan' src='{0}'>", "mounts\\MX0.jpg")), "GR15");
						RBLMount.Items.Add(lite);
						lite=new ListItem("Other-Mounts", "OTH");
						RBLMount.Items.Add(lite);
						RBLMount.SelectedIndex=0;
					}
					else if(DDLValveSize.SelectedItem.Text.ToString().Trim()== "2.5")
					{
						dd=0.00m;
						RBLMount.Items.Clear();
						ListItem lite=new ListItem();
						lite=new ListItem((String.Format("GR25<br /><img alt='GR25 mount: Valve free issue by Customer and mounted by Cowan' src='{0}'>", "mounts\\MX0.jpg")), "GR25");
						RBLMount.Items.Add(lite);
						lite=new ListItem("Other-Mounts", "OTH");
						RBLMount.Items.Add(lite);
						RBLMount.SelectedIndex=0;
					}
					d1=Convert.ToDecimal(DDLValveSize.SelectedItem.Text.ToString());
					TxtStroke.Text=String.Format("{0:##0.00}",d1 + dd);
					lblstroke.Text="";
					lblstroke.Visible=false;
				}
			}
			else
			{
		
			}
		}
		private void TxtStroke_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtStroke.Text.ToString().Trim() !="" && IsNumeric(TxtStroke.Text.ToString().Trim()) ==true  )
			{
				TxtStroke.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TxtStroke.Text));
			}
			else
			{
				TxtStroke.Text="";
			}
		}

		private void RBLMount_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(RBLMount.SelectedItem.Value.ToString().Trim() =="OTH")
			{
				DDLMount.Visible=true;
				DDLMount.Items.Clear();
				ListItem lite=new ListItem();
				lite=new ListItem("Extended tie rod head end with 4 extra tie rod nuts", "NX3");
				DDLMount.Items.Add(lite);	
				lite=new ListItem("Extended tie rod both ends with 4 extra tie rod nuts","NX1");
				DDLMount.Items.Add(lite);
			}
			else
			{
				DDLMount.Visible=false;
				DDLMount.Items.Clear();
			}
		}

		private void TxtThrust_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtThrust.Text.ToString().Trim() !="" && IsNumeric(TxtThrust.Text.ToString().Trim()) ==true  )
			{
			
				TxtThrust.Text =String.Format("{0:##0}",Convert.ToDecimal(TxtThrust.Text));
				if(Lblp1.Text.Trim() =="")
				{
					Lblval.Visible=true;	
				}
			}
			else
			{
				TxtThrust.Text="";
			}
		}

		private void LBBack_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("EV_Series_Select.aspx");
		}
	}
}
