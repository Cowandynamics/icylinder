using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
namespace iCylinderV1
{
	/// <summary>
	/// Summary description for Authorizing_Quote.
	/// </summary>
	public class Authorizing_Quote : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label LblInfo;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				if(Request.QueryString["status"] !=null &&  Request.QueryString["qno"] !=null &&  Request.QueryString["type"] !=null )
				{
					string status=Request.QueryString["status"].ToString().Trim();
					string qno=Request.QueryString["qno"].ToString().Trim();
					string type=Request.QueryString["type"].ToString().Trim();
					if(status.ToUpper().Trim() =="Y")
					{
						DBClass db=new DBClass();
						string result="";
						if(type.Trim().ToUpper() =="SMC")
						{
							result=db.Update_AuthotrizeQuote_SMC(qno.ToString());
						}
						else if(type.Trim().ToUpper() =="ICYL")
						{
							result=db.Update_AuthotrizeQuote_SMC(qno.ToString());
						}
						if(result.Trim() !="")
						{
							db.FillDataToCrystalReport_ICylinder(qno.Trim());	
							CrystalDecisions.CrystalReports.Engine.ReportDocument objReport = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
							string path;
							path = Request.PhysicalApplicationPath;
							objReport.Load(path +"Quotation_Template_ICylinder.rpt");
							string strCurrentDir = Server.MapPath(".") + "//quote//";
							db.RemoveFiles(strCurrentDir);
							objReport.Refresh();       
							objReport.SetDatabaseLogon("cowandynamicsV1","37YggDDj","208.109.126.100\\SQLEXPRESS","ICylinderV1");
							objReport.SetParameterValue("@Qno",qno.Trim());
							CrystalDecisions.Shared.DiskFileDestinationOptions diskOpts = new CrystalDecisions.Shared.DiskFileDestinationOptions();
							objReport.ExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
							objReport.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
							diskOpts.DiskFileName = path+"//quote//"+ qno.Trim()+".pdf";
							objReport.ExportOptions.DestinationOptions = diskOpts;
							objReport.Export();
							Quotation quote=new Quotation();
							quote=db.SelectCustomerDetails(qno.ToString());
							MailMessage mail=new MailMessage();
							SmtpMail.SmtpServer ="k2smtpout.secureserver.net"; 
							mail.From= "admin@cowandynamics.com";
							mail.To = quote.AttnTo3.ToString();
							mail.Bcc="admin@cowandynamics.com";							
							mail.BodyFormat =MailFormat.Html;
							mail.Subject=  "Quote Ref #"+quote.QuoteNo.ToString()+" Received From Cowan";
							mail.Priority =MailPriority.Normal;		
							if(quote.CompanyID.ToString().StartsWith("SMC"))
							{
								mail.Body =						
									"<hr color='#C00000'>Hi,<br> Your Quote is updated: <br>You can access this quote from  <a href='http://www.cowan-icylinder.com'>I-Cylinder.</a>"
									+"<TABLE id='Table1' borderColor='#00C000' cellSpacing='1' cellPadding='1' align='left' border='1'>"
									+"<TR><TD noWrap>RFQ Ref # :</TD><TD noWrap>"+quote.Items.ToString()+"</TD></TR>"
									+"<TR><TD noWrap>Quote Date :</TD><TD noWrap>"+quote.Quotedate.ToString()+"</TD></TR>"
									+"<TR><TD noWrap>Cowan Quote No :</TD><TD noWrap>"+quote.QuoteNo.ToString()+"</TD></TR>"
									+"</TABLE>"
									+"<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>Please log into <a href='http://www.cowan-icylinder.com'>I-Cylinder</a> for additional details. "
									+"<br><br><hr> Thanks <br> Admin<br><hr color='#C00000'>";
							}
							else
							{
								mail.Body=
									"<hr color='#C00000'>Hi,<br> Your Quote is updated: <br>You can access this quote from  <a href='http://www.cowan-cylinder.com/'>I-Cylinder.</a>"
									+"<TABLE id='Table1' borderColor='#00C000' cellSpacing='1' cellPadding='1' align='left' border='1'>"
									+"<TR><TD noWrap>RFQ Ref # :</TD><TD noWrap>"+quote.Items.ToString()+"</TD></TR>"
									+"<TR><TD noWrap>Quote Date :</TD><TD noWrap>"+quote.Quotedate.ToString()+"</TD></TR>"
									+"<TR><TD noWrap>Cowan Quote No :</TD><TD noWrap>"+quote.QuoteNo.ToString()+"</TD></TR>"
									+"</TABLE>"
									+"<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>Please log into <a href='http://www.cowan-cylinder.com/'>I-Cylinder</a> for additional details."
									+"<br><br><hr> Thanks <br> Admin<br><hr color='#C00000'>";
							}
							MailAttachment attach;
							attach=new MailAttachment(path+"//quote//"+ qno.Trim()+".pdf");
							mail.Attachments.Add(attach);											
							//issue #582 start
							mail.Body += csSignature.Get_Admin();
							//issue #582 end
							SmtpMail.Send(mail);
							LblInfo.Text="This quote is authorized & forwarded to the customer";
						}
					}
					else
					{
						LblInfo.Text="This quote is not authozied !!";
					}
				}
			}
			catch(Exception ex)
			{
				MailMessage mail=new MailMessage();
				SmtpMail.SmtpServer ="k2smtpout.secureserver.net"; 
				mail.From= "admin@cowandynamics.com";
				mail.To="admin@cowandynamics.com";			
				mail.BodyFormat =MailFormat.Html;
				mail.Subject=   "email error- authoriation";
				mail.Priority =MailPriority.Normal;						
				mail.Body =	ex.Message;								
				//issue #582 start
				mail.Body += csSignature.Get_Admin();
				//issue #582 end
				SmtpMail.Send(mail);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
