using System;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for Rkits.
	/// </summary>
	public class Rkits
	{
		public Rkits()
		{
		 
		}
		private string qno;
		private string pno;
		private string scode;
		private string series;
		private string sprice;
		private string bcode;
		private string bore;
		private string bprice;
		private string rcode;
		private string rod;
		private string rprice;
		private string ccode;
		private string cushion;
		private string cprice;
		private string selcode;
		private string seal;
		private string selprice;
		private string secrcode;
		private string secrod;
		private string secrprice;
		private string pscode;
		private string pseal;
		private string psealprice;
		private string rscode;
		private string rseal;
		private string rsealprice;
		private string mcode;
		private string metalscrapper;
		private string mprice;
		private string gcode;
		private string glanddrain;
		private string gprice;
		private string multicode;
		private string multicylinder;
		private string multiprice;
		private string quantity;
		private string dicount;
		private string unitp;
		private string tprice;
		private string note;
		private string slno;
		public string Slno
		{
			get
			{
				return slno ;
			}
			set
			{
				slno  = value;
			}
		}
		public string Qno
		{
			get
			{
				return qno ;
			}
			set
			{
				qno  = value;
			}
		}

		public string PartNo
		{
			get
			{
				return pno;
			}
			set
			{
				pno = value;
			}
		}

		public string S_Code
		{
			get
			{
				return scode;
			}
			set
			{
				scode = value;
			}
		}
		public string Series
		{
			get
			{
				return series;
			}
			set
			{
				series = value;
			}
		}
		public string S_Price
		{
			get
			{
				return sprice;
			}
			set
			{
				sprice = value;
			}
		}
		public string B_Code
		{
			get
			{
				return bcode;
			}
			set
			{
				bcode = value;
			}
		}
		public string Bore
		{
			get
			{
				return bore;
			}
			set
			{
				bore = value;
			}
		}
		public string B_Price
		{
			get
			{
				return bprice;
			}
			set
			{
				bprice = value;
			}
		}
		public string R_Code
		{
			get
			{
				return rcode;
			}
			set
			{
				rcode = value;
			}
		}
		public string Rod
		{
			get
			{
				return rod;
			}
			set
			{
				rod = value;
			}
		}
		public string R_Price
		{
			get
			{
				return rprice;
			}
			set
			{
				rprice = value;
			}
		}
		public string C_Code
		{
			get
			{
				return ccode;
			}
			set
			{
				ccode = value;
			}
		}
		public string Cushion
		{
			get
			{
				return cushion;
			}
			set
			{
				cushion = value;
			}
		}
		public string C_Price
		{
			get
			{
				return cprice;
			}
			set
			{
				cprice = value;
			}
		}
		public string Sel_Code
		{
			get
			{
				return selcode;
			}
			set
			{
				selcode = value;
			}
		}
		public string Seal
		{
			get
			{
				return seal;
			}
			set
			{
				seal = value;
			}
		}
		public string Sel_price
		{
			get
			{
				return selprice;
			}
			set
			{
				selprice = value;
			}
		}
		public string SecR_Code
		{
			get
			{
				return secrcode;
			}
			set
			{
				secrcode = value;
			}
		}
		public string SecRod
		{
			get
			{
				return secrod;
			}
			set
			{
				secrod = value;
			}
		}
		public string SecR_Price
		{
			get
			{
				return secrprice;
			}
			set
			{
				secrprice = value;
			}
		}
		public string PistonS_Code
		{
			get
			{
				return pscode;
			}
			set
			{
				pscode = value;
			}
		}
		public string PistonSeal
		{
			get
			{
				return pseal;
			}
			set
			{
				pseal = value;
			}
		}
		public string PistonS_Price
		{
			get
			{
				return psealprice;
			}
			set
			{
				psealprice = value;
			}
		}
		public string RodS_Code
		{
			get
			{
				return rscode;
			}
			set
			{
				rscode = value;
			}
		}
		public string RodSeal
		{
			get
			{
				return rseal;
			}
			set
			{
				rseal = value;
			}
		}
		public string RodS_Price
		{
			get
			{
				return rsealprice;
			}
			set
			{
				rsealprice = value;
			}
		}
		public string MS_Code
		{
			get
			{
				return mcode;
			}
			set
			{
				mcode = value;
			}
		}
		public string MetalScrapper
		{
			get
			{
				return metalscrapper;
			}
			set
			{
				metalscrapper = value;
			}
		}
		public string MetalS_Price
		{
			get
			{
				return mprice;
			}
			set
			{
				mprice = value;
			}
		}
		public string GD_Code
		{
			get
			{
				return gcode;
			}
			set
			{
				gcode = value;
			}
		}
		public string GlandDrain
		{
			get
			{
				return glanddrain;
			}
			set
			{
				glanddrain = value;
			}
		}
		public string Gland_Price
		{
			get
			{
				return gprice;
			}
			set
			{
				gprice = value;
			}
		}
		public string Multi_code
		{
			get
			{
				return multicode;
			}
			set
			{
				multicode = value;
			}
		}
		public string MultiCylinder
		{
			get
			{
				return multicylinder;
			}
			set
			{
				multicylinder = value;
			}
		}
		public string Multi_Price
		{
			get
			{
				return multiprice;
			}
			set
			{
				multiprice = value;
			}
		}
		public string Qty
		{
			get
			{
				return quantity;
			}
			set
			{
				quantity = value;
			}
		}
		public string NetPrice
		{
			get
			{
				return unitp;
			}
			set
			{
				unitp = value;
			}
		}
		public string Discount
		{
			get
			{
				return dicount;
			}
			set
			{
				dicount = value;
			}
		}
		public string TPrice
		{
			get
			{
				return tprice;
			}
			set
			{
				tprice = value;
			}
		}
		public string Note
		{
			get
			{
				return note;
			}
			set
			{
				note = value;
			}
		}
	}
}
