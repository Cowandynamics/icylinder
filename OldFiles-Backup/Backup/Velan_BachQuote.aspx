<%@ Page language="c#" Codebehind="Velan_BachQuote.aspx.cs" AutoEventWireup="false" Inherits="iCylinderV1.Velan_BachQuote" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Velan_BachQuote</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript">
		 function showPleaseWait()  
		{     
			document.getElementById('PleaseWait').style.display = 'block';  
		}
		</script>
	</HEAD>
	<body leftMargin="0" rightMargin="0" topMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE style="Z-INDEX: 0; WIDTH: 1000px; HEIGHT: 487px" id="Table1" border="0" cellSpacing="0"
				cellPadding="0" width="1000" bgColor="lightgrey">
				<TR>
					<TD height="20" width="50"></TD>
					<TD height="20" align="center"><asp:label id="Label1" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
							Font-Size="Small">A Series Batch Quote</asp:label></TD>
					<TD height="20" width="50"></TD>
				</TR>
				<TR>
					<TD width="50"></TD>
					<TD style="Z-INDEX: 0" bgColor="#c0c0c0" vAlign="top" align="center">
						<TABLE id="Table2" border="0" cellSpacing="1" cellPadding="1" width="100%">
							<TR>
								<TD colSpan="2" align="center"><asp:panel id="Panel1" runat="server" BackColor="LightGray" BorderColor="DarkGray" BorderWidth="3px"
										BorderStyle="Double" Width="700px">
										<TABLE id="Table3" border="0" cellSpacing="1" cellPadding="1" width="100%">
											<TR>
												<TD height="15" align="right"></TD>
												<TD height="15" width="50%" align="left">
													<asp:label style="Z-INDEX: 0" id="Label9" runat="server" Font-Size="9pt" BorderColor="Transparent"
														BackColor="Transparent" Width="342px" Height="15px" ForeColor="Red"></asp:label></TD>
											</TR>
											<TR>
												<TD align="right">
													<asp:label style="Z-INDEX: 0" id="Label5" runat="server" Font-Size="9pt">Import File Template:</asp:label></TD>
												<TD width="50%" align="left">
													<asp:hyperlink style="Z-INDEX: 0" id="HyperLink1" runat="server" Font-Size="9pt" NavigateUrl="crtfiles/template.xls"
														Target="_blank">Download Now..</asp:hyperlink></TD>
											</TR>
											<TR>
												<TD align="right">
													<asp:label style="Z-INDEX: 0" id="Label6" runat="server" Font-Size="9pt" Width="119px">Project Number:</asp:label></TD>
												<TD width="50%" align="left">
													<asp:textbox style="Z-INDEX: 0" id="TxtProjectNo" runat="server" Font-Size="8pt" Width="248px"
														Height="20px"></asp:textbox></TD>
											</TR>
											<TR>
												<TD align="right">
													<asp:label style="Z-INDEX: 0" id="Label2" runat="server" Font-Size="9pt" Width="119px">Select Import File:</asp:label></TD>
												<TD width="50%" align="left"><INPUT style="Z-INDEX: 0; WIDTH: 336px; HEIGHT: 23px; FONT-SIZE: 7pt" id="FileUpload" size="36"
														type="file" name="File1" runat="server"></TD>
											</TR>
											<TR>
												<TD align="right">
													<asp:label style="Z-INDEX: 0" id="Label3" runat="server" Font-Size="9pt" Width="119px">Uploaded File:</asp:label></TD>
												<TD width="50%" align="left">
													<asp:label style="Z-INDEX: 0" id="LblUpload" runat="server" Font-Size="9pt" ForeColor="Gray"></asp:label></TD>
											</TR>
											<TR>
												<TD colSpan="2" align="center">
													<asp:linkbutton style="Z-INDEX: 0" id="LBUpload" runat="server" Font-Size="9pt" Width="36px" ToolTip="Please make sure the selected file is not opened while uploading.."
														Font-Bold="True">Upload</asp:linkbutton></TD>
											</TR>
										</TABLE>
										<asp:label style="Z-INDEX: 0" id="lblpage" runat="server" Font-Size="9pt" BorderColor="Transparent"
											BackColor="Transparent" Width="342px" ForeColor="Red"></asp:label>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD style="Z-INDEX: 0; HEIGHT: 17px" colSpan="2" align="center"><asp:panel id="Panel2" runat="server" BackColor="LightGray" BorderColor="DarkGray" BorderWidth="3px"
										BorderStyle="Double" Width="700px">
										<TABLE id="Table4" border="0" cellSpacing="1" cellPadding="1" width="100%">
											<TR>
												<TD align="right">
													<asp:label style="Z-INDEX: 0" id="Label7" runat="server" Font-Size="9pt" Width="191px">Select Spec Sheet(Optional) :</asp:label></TD>
												<TD width="50%" align="left"><INPUT style="Z-INDEX: 0; WIDTH: 336px; HEIGHT: 23px; FONT-SIZE: 7pt" id="FileUploadSpec"
														size="36" type="file" name="File1" runat="server"></TD>
											</TR>
											<TR>
												<TD align="right"></TD>
												<TD width="50%" align="left">
													<asp:label style="Z-INDEX: 0" id="Label8" runat="server" Font-Size="8pt" ForeColor="DimGray">Uploading Spec sheet (pdf) will send  the RFQ directly to Cowan</asp:label></TD>
											</TR>
											<TR>
												<TD colSpan="2" align="center">
													<asp:linkbutton style="Z-INDEX: 0" id="BtnQuoteGenerate" onmouseup="showPleaseWait()" runat="server"
														Font-Size="Smaller" Font-Bold="True">Generate Quote</asp:linkbutton></TD>
											</TR>
										</TABLE>
										<asp:label style="Z-INDEX: 0" id="lblerr" runat="server" Font-Size="9pt" BorderColor="Transparent"
											BackColor="Transparent" Width="342px" ForeColor="Red"></asp:label>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD height="30" colSpan="2" align="center">
									<DIV style="Z-INDEX: 0; TEXT-ALIGN: center; DISPLAY: none; HEIGHT: 18px; COLOR: white; VERTICAL-ALIGN: top"
										id="PleaseWait" class="helptext">
										<TABLE style="POSITION: relative; FONT-SIZE: 10pt; TOP: 0px; LEFT: 0px" id="MyTable" cellSpacing="0"
											cellPadding="0" width="200" height="15">
											<TR>
												<TD height="15"><FONT color="red">
														<MARQUEE behavior="alternate">Please Wait...</MARQUEE>
													</FONT>
												</TD>
											</TR>
										</TABLE>
									</DIV>
								</TD>
							</TR>
							<TR>
								<TD colSpan="2" align="center" style="Z-INDEX: 0"><asp:panel style="Z-INDEX: 0" id="PSend" runat="server" BackColor="LightGray" BorderColor="DarkGray"
										BorderWidth="3px" BorderStyle="Double" Width="700px" Height="78px" Visible="False">
										<TABLE id="Table12" border="0" cellSpacing="0" cellPadding="0" width="100%">
											<TR>
												<TD style="HEIGHT: 7px" colSpan="2" align="center">
													<asp:Label id="Label57" runat="server" Font-Size="Smaller" BackColor="Transparent" Width="249px"
														Height="18px" ForeColor="Red">This Quote require assitance from the factory:</asp:Label></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 309px; HEIGHT: 21px" align="right">
													<asp:Label id="Label58" runat="server" Font-Size="X-Small" Width="216px">Please Select the priority of this RFQ :</asp:Label></TD>
												<TD style="HEIGHT: 21px" width="50%">
													<asp:DropDownList style="Z-INDEX: 0" id="DDLPriority" runat="server" Font-Size="XX-Small" Width="80px"
														Height="18px">
														<asp:ListItem Value="Normal" Selected="True">Normal</asp:ListItem>
														<asp:ListItem Value="Urgent">Urgent</asp:ListItem>
													</asp:DropDownList></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 309px" align="right">
													<asp:Label style="Z-INDEX: 0" id="Label4" runat="server" Font-Size="X-Small" Width="209px">Special Notes:</asp:Label></TD>
												<TD>
													<asp:textbox style="Z-INDEX: 0" id="TxtSpecialReq" runat="server" Font-Size="8pt" Width="336px"
														Height="64px" TextMode="MultiLine" MaxLength="500"></asp:textbox></TD>
											</TR>
											<TR>
												<TD colSpan="2" align="center">
													<asp:LinkButton style="Z-INDEX: 0" id="LBSend" runat="server" Font-Size="Smaller" Width="93px" Font-Bold="True">Send Request</asp:LinkButton></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
						</TABLE>
						<asp:label style="Z-INDEX: 0" id="lblup" runat="server" Font-Size="9pt" Width="119px" Visible="False"></asp:label></TD>
					<TD width="50"></TD>
				</TR>
				<TR>
					<TD height="30" width="50" align="right"></TD>
					<TD height="30" align="center"><asp:label style="Z-INDEX: 0" id="LblView" runat="server" Font-Size="9pt" Width="119px"></asp:label><asp:label style="Z-INDEX: 0" id="lblmgrp" runat="server" Visible="False"></asp:label><asp:label style="Z-INDEX: 0" id="lbltotal" runat="server" Visible="False"></asp:label><asp:label style="Z-INDEX: 0" id="lbltable" runat="server" Visible="False"></asp:label><asp:label style="Z-INDEX: 0" id="lblstatus" runat="server" Visible="False"></asp:label><asp:label style="Z-INDEX: 0" id="LblPartNo" runat="server" Font-Underline="True" Font-Size="Smaller"
							Font-Bold="True" Visible="False"></asp:label><asp:label style="Z-INDEX: 0" id="lblspecsheet" runat="server" Font-Size="9pt" ForeColor="Gray"
							Visible="False"></asp:label></TD>
					<TD height="30" width="50"></TD>
				</TR>
			</TABLE>
			&nbsp;
		</form>
	</body>
</HTML>
