<%@ Page language="c#" Codebehind="Icylinder_Page6.aspx.cs" AutoEventWireup="false" Inherits="iCylinderV1.Icylinder_Page6" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Icylinder_Page6</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE style="WIDTH: 1000px; HEIGHT: 487px" id="Table1" border="0" cellSpacing="0" cellPadding="0"
				width="1000" bgColor="lightgrey">
				<TR>
					<TD height="20" width="50"></TD>
					<TD height="20" align="center"><asp:label id="Label2" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
							Font-Size="Small">Cylinder Description  </asp:label></TD>
					<TD height="20" width="50"></TD>
				</TR>
				<TR>
					<TD width="50"></TD>
					<TD bgColor="gainsboro" align="center">
						<TABLE id="Table4" border="0" cellSpacing="0" cellPadding="0" width="899">
							<TR>
								<TD width="449" align="center">
									<TABLE id="Table3" border="1" cellSpacing="0" borderColor="silver" cellPadding="0" width="449"
										height="360">
										<TR>
											<TD style="HEIGHT: 11px"><asp:label id="Label9" runat="server" Font-Underline="True" Font-Size="Smaller" Width="216px">Detailed Description of your Cylinder :</asp:label></TD>
										</TR>
										<TR>
											<TD><asp:label id="LblPartNo" runat="server" Font-Underline="True" Font-Size="Smaller" Font-Bold="True"></asp:label><asp:label id="LBLSeries" runat="server" Font-Size="Smaller" Width="432px" Height="6px"></asp:label></TD>
										</TR>
									</TABLE>
								</TD>
								<TD align="center">
									<TABLE id="Table2" border="1" cellSpacing="0" borderColor="silver" cellPadding="0" width="449"
										bgColor="#cccccc" height="360">
										<TR>
											<TD style="HEIGHT: 14px" align="left"><asp:label id="Label13" runat="server" Font-Underline="True" Font-Size="Smaller" Width="196px">Do you have any Special Request :</asp:label></TD>
										</TR>
										<TR>
											<TD height="20" align="left"><asp:radiobuttonlist id="RBLQty" runat="server" Font-Size="Smaller" Width="440px" RepeatLayout="Flow"
													RepeatColumns="2" RepeatDirection="Horizontal" AutoPostBack="True">
													<asp:ListItem Value="Yes">Yes</asp:ListItem>
													<asp:ListItem Value="No" Selected="True">No</asp:ListItem>
												</asp:radiobuttonlist><asp:textbox id="TxtSpecialReq" runat="server" Font-Size="8pt" Width="451px" Height="80px" TextMode="MultiLine"
													Visible="False" MaxLength="500"></asp:textbox></TD>
										</TR>
										<TR>
											<TD height="20" align="left"><asp:label id="Label12" runat="server" Font-Underline="True" Font-Size="Smaller">Enter The Quantity :</asp:label></TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 18px" align="left"><asp:textbox id="TxtQty" runat="server" Font-Size="XX-Small" Width="56px" Height="18px"></asp:textbox><asp:label id="lblQty" runat="server" Font-Size="Smaller" Visible="False" ForeColor="Red">Please Enter Quantity !</asp:label></TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 18px" align="left"><asp:label style="Z-INDEX: 0" id="Label1" runat="server" Font-Underline="True" Font-Size="Smaller">Upload Drawing (pdf format):</asp:label></TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 18px" align="left"><INPUT style="Z-INDEX: 0; WIDTH: 320px; HEIGHT: 22px; FONT-SIZE: 8pt" id="UploadDwg" size="34"
													type="file" name="File1" runat="server">
												<asp:linkbutton style="Z-INDEX: 0" id="LbPreview" runat="server" BackColor="Silver" BorderColor="WhiteSmoke"
													Font-Size="9pt" Width="48px" Height="20px" ForeColor="Black" BorderWidth="2px" BorderStyle="Outset">Preview</asp:linkbutton></TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 18px" align="left"><asp:checkbox style="Z-INDEX: 0" id="cbupload" runat="server" Font-Size="8pt" Visible="False"
													TextAlign="Left" Text="Upload this File:" Checked="True"></asp:checkbox><asp:label style="Z-INDEX: 0" id="lblfilename" runat="server" BackColor="Transparent" Font-Size="8pt"
													Height="6px" ForeColor="DimGray"></asp:label></TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 27px" colSpan="2" align="center"><asp:linkbutton id="LBGenerate" runat="server" Font-Size="Smaller" Width="108px" Font-Bold="True">Generate Quote</asp:linkbutton></TD>
										</TR>
										<TR>
											<TD colSpan="2" align="center"><asp:label id="lblgenerateprice" runat="server" BackColor="Transparent" Font-Size="Smaller"
													Width="338px" Height="18px" ForeColor="Blue"></asp:label><asp:panel id="PSend" runat="server" Height="78px" Visible="False" BorderWidth="1px" BorderStyle="Solid">
													<TABLE id="Table12" border="1" cellSpacing="0" borderColor="silver" cellPadding="0" width="450">
														<TR>
															<TD style="HEIGHT: 7px" align="center">
																<asp:Label id="Label57" runat="server" Font-Size="Smaller" BackColor="Transparent" Width="249px"
																	Height="18px" ForeColor="Red">This Quote require assitance from the factory:</asp:Label></TD>
														</TR>
														<TR>
															<TD>
																<asp:Label id="Label58" runat="server" Font-Size="X-Small" Width="240px" ForeColor="#FF8000">Please Select the priority of this RFQ :</asp:Label>
																<asp:DropDownList id="DDLPriority" runat="server" Font-Size="XX-Small" Width="80px" Height="18px">
																	<asp:ListItem Value="Normal" Selected="True">Normal</asp:ListItem>
																	<asp:ListItem Value="Urgent">Urgent</asp:ListItem>
																</asp:DropDownList></TD>
														</TR>
														<TR>
															<TD align="center">
																<asp:LinkButton id="LBSend" runat="server" Font-Size="Smaller">Send Request</asp:LinkButton></TD>
														</TR>
													</TABLE>
												</asp:panel></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
						<asp:label id="LblInfo" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
							Font-Size="Smaller" ForeColor="Red"></asp:label><asp:label id="lbltable" runat="server" Visible="False"></asp:label><asp:label id="lbl" runat="server" Visible="False"></asp:label><asp:label id="lblbor" runat="server" Visible="False"></asp:label><asp:label id="lblmgrp" runat="server" Visible="False"></asp:label><asp:label id="lbltotal" runat="server" Visible="False" Text="0"></asp:label><asp:label id="lblqno" runat="server" Visible="False"></asp:label><asp:label id="lblstroke" runat="server" Visible="False"></asp:label><asp:label id="lblstatus" runat="server" Visible="False"></asp:label><asp:label id="lblD" runat="server" Visible="False"></asp:label><asp:label style="Z-INDEX: 0" id="lblfile" runat="server" Visible="False"></asp:label></TD>
					<TD width="50"></TD>
				</TR>
				<TR>
					<TD height="30" width="50" align="right"></TD>
					<TD bgColor="#dcdcdc" height="30" align="center"></TD>
					<TD height="30" width="50"></TD>
				</TR>
				<TR>
					<TD height="20" width="50"><asp:linkbutton id="LBBack" runat="server" Font-Size="Smaller" Font-Bold="True">Back</asp:linkbutton></TD>
					<TD height="20" align="center"></TD>
					<TD height="20" width="50"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
