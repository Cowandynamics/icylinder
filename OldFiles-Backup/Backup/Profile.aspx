<%@ Page language="c#" Codebehind="Profile.aspx.cs" AutoEventWireup="false" Inherits="iCylinderV1.Profile" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Profile</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body bgColor="gainsboro">
		<form id="Form1" method="post" runat="server">
			<asp:panel id="Panel2" style="Z-INDEX: 102; LEFT: 0px; POSITION: absolute; TOP: 128px" runat="server"
				BorderColor="Transparent" BackColor="Transparent" Height="280px" Width="1000px" HorizontalAlign="Center"
				BorderWidth="1px">
				<TABLE id="Table3" style="WIDTH: 520px; HEIGHT: 240px" cellSpacing="0" cellPadding="0"
					width="520" border="0">
					<TR>
						<TD style="WIDTH: 61px" height="30"></TD>
						<TD style="WIDTH: 163px" height="30"></TD>
						<TD style="WIDTH: 232px" height="30">
							<asp:label id="Label1" runat="server" Font-Underline="True" Font-Size="Small" ForeColor="Red">User Profile</asp:label></TD>
						<TD height="30"></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 61px" height="30"></TD>
						<TD style="WIDTH: 163px" height="30"></TD>
						<TD style="WIDTH: 232px" height="30">
							<asp:Label id="Label7" runat="server" Width="144px" Font-Size="Smaller">Your Password has expired. Please update with new password!!</asp:Label></TD>
						<TD height="30"></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 61px"></TD>
						<TD style="WIDTH: 163px">
							<asp:Label id="Label2" runat="server" Font-Size="Smaller">User Name :</asp:Label></TD>
						<TD style="WIDTH: 232px">
							<asp:TextBox id="TxtUserName" runat="server" Width="112px" Height="20px" Font-Size="XX-Small"
								Enabled="False" BorderStyle="Ridge"></asp:TextBox></TD>
						<TD></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 61px" height="24"></TD>
						<TD style="WIDTH: 163px" height="24"></TD>
						<TD style="WIDTH: 232px" height="24"></TD>
						<TD height="24"></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 61px"></TD>
						<TD style="WIDTH: 163px">
							<asp:Label id="Label5" runat="server" Font-Size="Smaller">Email :</asp:Label></TD>
						<TD style="WIDTH: 232px">
							<asp:TextBox id="TxtEmail" runat="server" Width="232px" Height="20px" Font-Size="XX-Small" Enabled="False"
								BorderStyle="Ridge"></asp:TextBox></TD>
						<TD></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 61px; HEIGHT: 22px"></TD>
						<TD style="WIDTH: 163px; HEIGHT: 22px">
							<asp:Label id="Label3" runat="server" Font-Size="Smaller">Enter Old Password :</asp:Label></TD>
						<TD style="WIDTH: 232px; HEIGHT: 22px">
							<asp:TextBox id="TxtoldPass" runat="server" Width="112px" Height="20px" Font-Size="XX-Small"
								BorderStyle="Ridge" TextMode="Password"></asp:TextBox></TD>
						<TD style="HEIGHT: 22px"></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 61px" height="24"></TD>
						<TD style="WIDTH: 163px" height="24"></TD>
						<TD style="WIDTH: 232px" height="24"></TD>
						<TD height="24"></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 61px; HEIGHT: 19px"></TD>
						<TD style="WIDTH: 163px; HEIGHT: 19px">
							<asp:Label id="Label6" runat="server" Font-Size="Smaller">Enter New password :</asp:Label></TD>
						<TD style="WIDTH: 232px; HEIGHT: 19px">
							<asp:TextBox id="TxtNewPass" runat="server" Width="112px" Height="20px" Font-Size="XX-Small"
								BorderStyle="Ridge" TextMode="Password"></asp:TextBox>
							<asp:RegularExpressionValidator id="RegularExpressionValidator1" runat="server" Font-Size="XX-Small" ValidationExpression="^[a-zA-Z0-9]{6,10}$"
								ControlToValidate="TxtNewPass" ErrorMessage="Minimum 6 - 10 charecters"></asp:RegularExpressionValidator></TD>
						<TD style="HEIGHT: 19px"></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 61px"></TD>
						<TD style="WIDTH: 163px">
							<asp:Label id="Label4" runat="server" Font-Size="Smaller">Confirm New password :</asp:Label></TD>
						<TD style="WIDTH: 232px">
							<asp:TextBox id="Txtconfirm" runat="server" Width="112px" Height="20px" Font-Size="XX-Small"
								BorderStyle="Ridge" TextMode="Password"></asp:TextBox></TD>
						<TD></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 61px"></TD>
						<TD align="center" colSpan="3">
							<asp:Button id="BtnSave" runat="server" Width="40px" Text="Save "></asp:Button></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 61px; HEIGHT: 29px"></TD>
						<TD style="WIDTH: 163px; HEIGHT: 29px" align="center" colSpan="2">
							<asp:CompareValidator id="CompareValidator2" runat="server" Width="382px" Font-Size="Smaller" ControlToValidate="TxtoldPass"
								ErrorMessage="Both Old &amp; New passwords must be diffrent !!" Operator="NotEqual" ControlToCompare="TxtNewPass"></asp:CompareValidator>
							<asp:CompareValidator id="CompareValidator1" runat="server" Width="383px" Font-Size="X-Small" ControlToValidate="Txtconfirm"
								ErrorMessage="Both  new &amp; Confirm password must be same !!" ControlToCompare="TxtNewPass"></asp:CompareValidator></TD>
						<TD style="HEIGHT: 29px"></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 61px"></TD>
						<TD style="WIDTH: 395px" align="center" colSpan="2">
							<asp:Label id="LblInfo" runat="server" Font-Size="Smaller" ForeColor="Red"></asp:Label></TD>
						<TD></TD>
					</TR>
				</TABLE>
			</asp:panel></form>
	</body>
</HTML>
