using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net;
using System.Security.Principal;
namespace iCylinderV1
{
	/// <summary>
	/// Summary description for ManageAcc1.
	/// </summary>
	public class ManageAcc1 : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblhidden;
		protected System.Web.UI.WebControls.LinkButton BtnNext;
		protected System.Web.UI.WebControls.LinkButton LBHome;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.LinkButton LBcap;
		protected System.Web.UI.WebControls.LinkButton LBRod;
		protected System.Web.UI.WebControls.ImageButton IBCap;
		protected System.Web.UI.WebControls.ImageButton IBRod;
		protected System.Web.UI.WebControls.Label Label2;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["User"] ==null)
			{
				Response.Redirect("Login.aspx");
			}
//			string ipAddress = IpAddress(); 
//			System.Security.Principal.WindowsPrincipal wp = new System.Security.Principal.WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent()); 
//			string user = wp.Identity.Name; 
//			string hostName = Dns.GetHostByAddress(ipAddress).HostName.ToString(); 
//			StreamWriter wrtr = new StreamWriter (Server.MapPath( "visitors.log" ), true ); 
//			wrtr.WriteLine( DateTime .Now.ToString() + " | " + ipAddress + " | " + user + " | " + hostName + " | " + Request.Url.ToString()); 
//			wrtr.Close(); 

		}
		private string IpAddress() 
		{ 
			string strIpAddress; 
			strIpAddress = Request.ServerVariables[ "HTTP_X_FORWARDED_FOR" ]; 
			if (strIpAddress == null ) 
				strIpAddress = Request.ServerVariables[ "REMOTE_ADDR" ]; 
			return strIpAddress; 

		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.IBCap.Click += new System.Web.UI.ImageClickEventHandler(this.IBCap_Click);
			this.LBcap.Click += new System.EventHandler(this.LBcap_Click);
			this.IBRod.Click += new System.Web.UI.ImageClickEventHandler(this.IBRod_Click);
			this.LBRod.Click += new System.EventHandler(this.LBRod_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void IBCap_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			Response.Redirect("Manage_Cap_Acc.aspx");
		}

		private void IBRod_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			Response.Redirect("Manage_Rod_Acc.aspx");
		}

		private void LBcap_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Manage_Cap_Acc.aspx");
		}

		private void LBRod_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Manage_Rod_Acc.aspx");
		}
	}
}
