using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Security.Principal;
using System.Net;
using System.Web.Mail;
namespace iCylinderV1
{
	/// <summary>
	/// Summary description for ManageQ.
	/// </summary>
	public class ManageQ : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Panel PArchive;
		protected System.Web.UI.WebControls.DataGrid DGArchive;
		protected System.Web.UI.WebControls.DropDownList DDLAPartNo;
		protected System.Web.UI.WebControls.DropDownList DDLAQNo;
		protected System.Web.UI.WebControls.DropDownList DDLACreatedate;
		protected System.Web.UI.WebControls.DropDownList DDLAContacts;
		protected System.Web.UI.WebControls.Panel PUnfinshed;
		protected System.Web.UI.WebControls.DropDownList DDLUContacts;
		protected System.Web.UI.WebControls.DropDownList DDLUCreateDate;
		protected System.Web.UI.WebControls.DropDownList DDLUQuoteNO;
		protected System.Web.UI.WebControls.DropDownList DDLUPartNo;
		protected System.Web.UI.WebControls.DataGrid DGUnfinished;
		protected System.Web.UI.WebControls.RadioButtonList RBLOptions;
		DBClass db=new DBClass();
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label lbl;
		protected System.Web.UI.WebControls.Label err;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.RadioButtonList RBLSearch;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Button BtnSearh;
		protected System.Web.UI.WebControls.Panel ppno;
		protected System.Web.UI.WebControls.Panel pqno;
		protected System.Web.UI.WebControls.TextBox TxtQno;
		protected System.Web.UI.WebControls.TextBox TxtPno;
		protected System.Web.UI.WebControls.Label lblinfo;
		protected System.Web.UI.WebControls.Panel PLeedtime;
		protected System.Web.UI.WebControls.Label Label17;
		protected System.Web.UI.WebControls.Label Label18;
		protected System.Web.UI.WebControls.TextBox TxtRLTTo;
		protected System.Web.UI.WebControls.Label Label20;
		protected System.Web.UI.WebControls.TextBox TxtRLTSubject;
		protected System.Web.UI.WebControls.Label Label21;
		protected System.Web.UI.WebControls.Label Label22;
		protected System.Web.UI.WebControls.TextBox TxtRLTMessage;
		protected System.Web.UI.WebControls.Button BtnSend;
		protected System.Web.UI.WebControls.Label lblrfldate;
		protected System.Web.UI.WebControls.Label lblrflqno;
		protected System.Web.UI.WebControls.Label lblrflpno;
		protected System.Web.UI.WebControls.Label Lblmail;
		protected System.Web.UI.WebControls.Label Label19;
		protected System.Web.UI.WebControls.Label lblpage;
		protected System.Web.UI.WebControls.Label dwg;
		protected System.Web.UI.WebControls.Panel pdate;
		protected System.Web.UI.WebControls.TextBox TxtDate2;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.TextBox TxtDate1;
		protected System.Web.UI.WebControls.Label Label7;
		protected String SortExpression="";
		private void Datefill()
		{
			TimeSpan t1 =new TimeSpan(30,0,0,0);
			DateTime dt2=new DateTime(DateTime.Today.Year,DateTime.Today.Month,DateTime.Today.Day);	
			DateTime dt1=dt2.Subtract(t1);
			TxtDate1.Text=dt1.ToShortDateString();
			TxtDate2.Text=dt2.ToShortDateString();
		}
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if(Session["user"] !=null)
			{
				Session["Quote"] =null;
				err.Text="";
				Lblmail.Text="";
				lblpage.Text="";
				dwg.Text="";
				if(! IsPostBack)
				{
					try
					{
						if(Request.QueryString["id"] !=null)
						{
							lblpage.Text="<script language='javascript'>window.open('print.aspx?id="+Request.QueryString["id"].ToString().Trim()+"&type=icylinder','blank','toolbar=yes,width=800,height=800,resizable=yes,top=0,left=0')</script>";
							if(Request.QueryString["pn"].Substring(0,1) =="Z" ||Request.QueryString["pn"].Substring(0,1) =="A" || Request.QueryString["pn"].Substring(0,2) =="ML" || Request.QueryString["pn"].Substring(0,1) =="P" || Request.QueryString["pn"].Substring(0,1) =="N")
							{
								dwg.Text="<script language='javascript'>window.open('Drawings.aspx?qno="+Request.QueryString["id"].ToString().Trim()+"&pno="+Request.QueryString["pn"].ToString().Trim()+"','_blank','toolbar=yes,width=1000,height=800,resizable=yes,top=0,left=0')</script>";
							}
							if(Request.QueryString["pn"].ToString().Trim() =="9999")
							{
								ArrayList qlist=new ArrayList();
								qlist=db.SelectPartNosByQuoteNo(Request.QueryString["id"].ToString().Trim());
								if(qlist.Count <=15)
								{
									for(int i=0;i<qlist.Count;i++)
									{
										dwg.Text +="<script language='javascript'>window.open('Drawings.aspx?qno="+Request.QueryString["id"].ToString().Trim()+"&pno="+qlist[i].ToString().Trim()+"','_blank','toolbar=yes,width=1000,height=800,resizable=yes,top=0,left=0')</script>";
									}
								}
								else
								{
									err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('There are more than 15 drawing.. please go to manage quotes and click on the part number to view the drawing!!!')</script>";
								}
							}
						}
						if (SortExpression == "") 
						{
							SortExpression = "QuoteDate DESC";
						} 
						Datefill();
						if(RBLSearch.SelectedIndex == 0)
						{
							pdate.Visible =true;
							pqno.Visible =false;
							ppno.Visible =false;
						}
						else if(RBLSearch.SelectedIndex == 1)
						{
							pdate.Visible =false;
							pqno.Visible =true;
							ppno.Visible =false;
						}
						else if(RBLSearch.SelectedIndex == 2)
						{
							pdate.Visible =false;
							pqno.Visible =false;
							ppno.Visible =true;

						}
						lbl.Text="";
					
						Session["Command"]=null;
						DGUnfinished.AllowSorting=true;
						DGArchive.AllowSorting=true;
						ArrayList lst =new ArrayList();
						lst =(ArrayList)Session["User"];
						lbl.Text="";
						//issue #325
						//visible projectno view
						//issue #118 start
						//backup
						//if(lst[6].ToString().Trim() =="1004" || lst[6].ToString().Trim() =="1009" || lst[6].ToString().Trim() =="1011" || lst[6].ToString().Trim() =="1015" || lst[6].ToString().Trim() =="1016"|| lst[6].ToString().Trim() =="1002" || lst[6].ToString().Trim() =="1018")
						//update
						if(lst[6].ToString().Trim() =="1004" || lst[6].ToString().Trim() =="1009" || lst[6].ToString().Trim() =="1011" || lst[6].ToString().Trim() =="1015" || lst[6].ToString().Trim() =="1016"|| lst[6].ToString().Trim() =="1002" || lst[6].ToString().Trim() =="1018" || lst[6].ToString().Trim() =="1021" || lst[6].ToString().Trim() =="999"  || lst[6].ToString().Trim() =="1017" || lst[6].ToString().Trim() =="998" || lst[6].ToString().Trim() =="1026")
						//issue #118 end
						{
							DGArchive.Columns[8].HeaderText="Project #";
							DGArchive.Columns[11].Visible=true;
							//issue #209 start
							//back
							DGUnfinished.Columns[9].Visible=true;
							//update
							DGArchive.Columns[9].Visible=false;
							//issue #209 end
						}
						else
						{
							DGArchive.Columns[11].Visible=false;
							DGUnfinished.Columns[9].Visible=false;
						}
						
						string customerid=lst[6].ToString();
						//projectsearch
						//issue #118 start
						//backup
						//if(customerid.ToString().Trim() =="1004"|| lst[6].ToString().Trim() =="1009" || lst[6].ToString().Trim() =="1011" || lst[6].ToString().Trim() =="1015" || lst[6].ToString().Trim() =="1016" || lst[6].ToString().Trim() =="1002"|| lst[6].ToString().Trim() =="1002"|| lst[6].ToString().Trim() =="1018")
						//update
						//if(customerid.ToString().Trim() =="1004"|| lst[6].ToString().Trim() =="1009" || lst[6].ToString().Trim() =="1011" || lst[6].ToString().Trim() =="1015" || lst[6].ToString().Trim() =="1016" || lst[6].ToString().Trim() =="1002"|| lst[6].ToString().Trim() =="1002"|| lst[6].ToString().Trim() =="1018" || lst[6].ToString().Trim() =="1021"  || lst[6].ToString().Trim() =="999")
						//issue #118 end
						//{
							RBLSearch.Items.Add("Project No");
						//}
						if(RBLOptions.SelectedIndex ==0)
						{
							DGArchive.CurrentPageIndex = 0 ;
							PArchive.Visible= true;
							PUnfinshed.Visible= false;
							DGArchive.DataSource=CreateDataSource3(customerid.Trim(), "","1","",TxtDate1.Text.Trim(),TxtDate2.Text.Trim(),"","");
							DGArchive.DataBind();
							ArrayList colist=new ArrayList();
							colist=db.GetContacts1(customerid.Trim(), "","1",TxtDate1.Text.Trim(),TxtDate2.Text.Trim());
							DDLAContacts.Items.Clear();
							DDLAContacts.Items.Add("Contacts");
							for(int i=0; i<colist.Count;i++)
							{
								DDLAContacts.Items.Add(colist[i].ToString());
							}
							ArrayList qdlist=new ArrayList();
							qdlist=db.Getquotedate1(customerid.Trim(), "","1","",TxtDate1.Text.Trim(),TxtDate2.Text.Trim());
							DDLACreatedate.Items.Clear();
							DDLACreatedate.Items.Add("Quote Date");
							for(int i=0; i<qdlist.Count;i++)
							{
								DDLACreatedate.Items.Add(qdlist[i].ToString());
							}
				
							ArrayList qnlist=new ArrayList();
							qnlist=db.GetQno1(customerid.Trim(), "","1","",TxtDate1.Text.Trim(),TxtDate2.Text.Trim());
							DDLAQNo.Items.Clear();
							DDLAQNo.Items.Add("Quote No");
							for(int i=0; i<qnlist.Count;i++)
							{
								DDLAQNo.Items.Add(qnlist[i].ToString());
							}

							ArrayList plist=new ArrayList();
							plist=db.GetPno1(customerid.Trim(), "","1","",TxtDate1.Text.Trim(),TxtDate2.Text.Trim(),"");
							ArrayList zlist=new ArrayList();
							zlist=db.GetZno_1(customerid.Trim(), "","1","",TxtDate1.Text.Trim(),TxtDate2.Text.Trim(),"");
							ArrayList alist=new ArrayList();
							alist=db.GetAno1(customerid.Trim(), "","1","",TxtDate1.Text.Trim(),TxtDate2.Text.Trim(),"");
							ArrayList rlist=new ArrayList();
							rlist=db.GetRno1(customerid.Trim(), "","1","",TxtDate1.Text.Trim(),TxtDate2.Text.Trim(),"");
					
							DDLAPartNo.Items.Clear();
							DDLAPartNo.Items.Add("Part No");
							for(int i=0; i<plist.Count;i++)
							{
								DDLAPartNo.Items.Add(plist[i].ToString());
							}
							for(int i=0; i<zlist.Count;i++)
							{
								DDLAPartNo.Items.Add(zlist[i].ToString());
							}
							for(int i=0; i<alist.Count;i++)
							{
								DDLAPartNo.Items.Add(alist[i].ToString());
							}
							for(int i=0; i<rlist.Count;i++)
							{
								DDLAPartNo.Items.Add(rlist[i].ToString());
							}
							
						}
						if(RBLOptions.SelectedIndex ==1)
						{
							DGUnfinished.CurrentPageIndex = 0 ;
							PUnfinshed.Visible= true;
							PArchive.Visible= false;
							DGUnfinished.DataSource=CreateDataSource4(customerid.Trim(),lbl.Text.Trim(),"0","",TxtDate1.Text.Trim(),TxtDate2.Text.Trim(),"","");
							DGUnfinished.DataBind();
						
							ArrayList colist=new ArrayList();
							colist=db.GetContacts1(customerid.Trim(), lbl.Text.Trim(),"0",TxtDate1.Text.Trim(),TxtDate2.Text.Trim());
							DDLUContacts.Items.Clear();
							DDLUContacts.Items.Add("Contacts");
							for(int i=0; i<colist.Count;i++)
							{
								DDLUContacts.Items.Add(colist[i].ToString());
							}
							ArrayList qdlist=new ArrayList();
							qdlist=db.Getquotedate1(customerid.Trim(), lbl.Text.Trim(),"0","",TxtDate1.Text.Trim(),TxtDate2.Text.Trim());
							DDLUCreateDate.Items.Clear();
							DDLUCreateDate.Items.Add("Quote Date");
							for(int i=0; i<qdlist.Count;i++)
							{
								DDLUCreateDate.Items.Add(qdlist[i].ToString());
							}
							ArrayList qnlist=new ArrayList();
							qnlist=db.GetQno1(customerid.Trim(), lbl.Text.Trim(),"0","",TxtDate1.Text.Trim(),TxtDate2.Text.Trim());
							DDLUQuoteNO.Items.Clear();
							DDLUQuoteNO.Items.Add("Quote No");
							for(int i=0; i<qnlist.Count;i++)
							{
								DDLUQuoteNO.Items.Add(qnlist[i].ToString());
							}
							ArrayList plist=new ArrayList();
							plist=db.GetPno1(customerid.Trim(), lbl.Text.Trim(),"0","",TxtDate1.Text.Trim(),TxtDate2.Text.Trim(),"");
							ArrayList zlist=new ArrayList();
							zlist=db.GetZno_1(customerid.Trim(), lbl.Text.Trim(),"0","",TxtDate1.Text.Trim(),TxtDate2.Text.Trim(),"");
							ArrayList alist=new ArrayList();
							alist=db.GetAno1(customerid.Trim(), lbl.Text.Trim(),"0","",TxtDate1.Text.Trim(),TxtDate2.Text.Trim(),"");
							ArrayList rlist=new ArrayList();
							rlist=db.GetRno1(customerid.Trim(), lbl.Text.Trim(),"0","",TxtDate1.Text.Trim(),TxtDate2.Text.Trim(),"");
					
							DDLUPartNo.Items.Clear();
							DDLUPartNo.Items.Add("Part No");
							for(int i=0; i<plist.Count;i++)
							{
								DDLUPartNo.Items.Add(plist[i].ToString());
							}
							for(int i=0; i<zlist.Count;i++)
							{
								DDLUPartNo.Items.Add(zlist[i].ToString());
							}
							for(int i=0; i<alist.Count;i++)
							{
								DDLUPartNo.Items.Add(alist[i].ToString());
							}
							for(int i=0; i<rlist.Count;i++)
							{
								DDLUPartNo.Items.Add(rlist[i].ToString());
							}							
						}
					//issue #209 start
					//DGUnfinished.Columns[0].Visible=false;
					//issue #209 end
					}
					catch(Exception ex)
					{
						string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
						s.Replace("'"," ");
						err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
					}
				}
			}
			else
			{
				Response.Redirect("Login.aspx");
			}
		}
		private string IpAddress() 
		{ 
			string strIpAddress; 
			strIpAddress = Request.ServerVariables[ "HTTP_X_FORWARDED_FOR" ]; 
			if (strIpAddress == null ) 
				strIpAddress = Request.ServerVariables[ "REMOTE_ADDR" ]; 
			return strIpAddress; 

		}
		public static void FocusControlOnPageLoad(string ClientID, System.Web.UI.Page page)
		{
			page.RegisterClientScriptBlock("CtrlFocus",@"<script>function ScrollView(){var el = document.getElementById('"+ClientID+@"');if (el != null){el.scrollIntoView();el.focus();}}window.onload = ScrollView;</script>");
		}
		public void DGArchive_SortCommand(Object o, DataGridSortCommandEventArgs e) 
		{
			try
			{
				err.Text="";
				string cont="";
				string cdate="";
				string pn="";
				string qn="";
				if(DDLAContacts.SelectedIndex > 0)
				{
					cont= DDLAContacts.SelectedItem.Text.Trim();
				}
				if(DDLACreatedate.SelectedIndex > 0)
				{
					cdate= DDLACreatedate.SelectedItem.Text.Trim();
				}
				if(DDLAQNo.SelectedIndex > 0)
				{
					if(TxtQno.Text.Trim() =="")
					{
						qn= DDLAQNo.SelectedItem.Text.Trim();
					}
					else
					{
						qn=TxtQno.Text.Trim();
					}
				}
				if(DDLAPartNo.SelectedIndex > 0)
				{
					if(TxtPno.Text.Trim() =="")
					{
						pn= DDLAPartNo.SelectedItem.Text.Trim();
					}
					else
					{
						pn=TxtPno.Text.Trim();
					}
						
				}
				ArrayList lst =new ArrayList();
				lst =(ArrayList)Session["User"];	
				SortExpression = e.SortExpression.ToString()+ "  DESC";
				if(pdate.Visible ==true)
				{
					DateTime dt1=Convert.ToDateTime(TxtDate1.Text.ToString());
					DateTime dt2=Convert.ToDateTime(TxtDate2.Text.ToString());
					DGArchive.DataSource = CreateDataSource3(lst[6].ToString().Trim(),"","1",cont.Trim(),dt1.ToShortDateString(),dt2.ToShortDateString(),qn.Trim(),pn.Trim());
				}
				else if(pqno.Visible ==true)
				{
					DGArchive.DataSource = CreateDataSource(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
				}
				else if(ppno.Visible ==true)
				{
					DGArchive.DataSource = CreateDataSource(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
				}

				DGArchive.DataBind();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
//				try
//				{
//					ArrayList lst =new ArrayList();
//					lst =(ArrayList)Session["User"];
//					SortExpression = e.SortExpression.ToString()+ "  DESC";
//					DGArchive.DataSource = CreateDataSource(lst[6].ToString().Trim(),"","1","","","","");
//					DGArchive.DataBind();
//				}
//				catch(Exception ex)
//				{
//					string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
//					s.Replace("'"," ");
//					err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
//				}
			
		}
		public void DGUnfinished_SortCommand(Object o, DataGridSortCommandEventArgs e) 
		{     
			try
			{
				err.Text="";
				string cont="";
				string cdate="";
				string pn="";
				string qn="";
				if(DDLUContacts.SelectedIndex > 0)
				{
					cont= DDLUContacts.SelectedItem.Text.Trim();
				}
				if(DDLUCreateDate.SelectedIndex > 0)
				{
					cdate= DDLUCreateDate.SelectedItem.Text.Trim();
				}
				if(DDLUQuoteNO.SelectedIndex > 0)
				{
					if(TxtQno.Text.Trim() =="")
					{
						qn= DDLUQuoteNO.SelectedItem.Text.Trim();
					}
					else
					{
						qn=TxtQno.Text.Trim();
					}
				}
				if(DDLUPartNo.SelectedIndex > 0)
				{
					if(TxtPno.Text.Trim() =="")
					{
						pn= DDLUPartNo.SelectedItem.Text.Trim();
					}
					else
					{
						pn=TxtPno.Text.Trim();
					}
						
				}
				ArrayList lst =new ArrayList();
				lst =(ArrayList)Session["User"];	
				SortExpression = e.SortExpression.ToString()+ "  DESC";
				if(pdate.Visible ==true)
				{
					DateTime dt1=Convert.ToDateTime(TxtDate1.Text.ToString());
					DateTime dt2=Convert.ToDateTime(TxtDate2.Text.ToString());
					DGUnfinished.DataSource = CreateDataSource4(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),dt1.ToShortDateString(),dt2.ToShortDateString(),qn.Trim(),pn.Trim());
				}
				else if(pqno.Visible ==true)
				{
					DGUnfinished.DataSource = CreateDataSource1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
				}
				else if(ppno.Visible ==true)
				{
					DGUnfinished.DataSource = CreateDataSource1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
				}
				DGUnfinished.DataBind();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
//			try
//			{
//				ArrayList lst =new ArrayList();
//				lst =(ArrayList)Session["User"];
//				SortExpression = e.SortExpression.ToString()+ "  DESC";
//				DGUnfinished.DataSource = CreateDataSource1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","","","","");
//				DGUnfinished.DataBind();
//			}
//			catch(Exception ex)
//			{
//				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
//				s.Replace("'"," ");
//				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
//			}
		}
		
		protected void DGArchive_Command(Object sender,DataGridCommandEventArgs e) 
		{
			try
			{
				if(e.CommandName =="CopyCommand")
				{
					ArrayList lst =new ArrayList();
					lst =(ArrayList)Session["User"];
					//issue #118 start
					//backup
					//if(lst[6].ToString().Trim() =="1002" || lst[6].ToString().Trim() =="1003" || lst[6].ToString().Trim() =="1004")
					//update
					if(lst[6].ToString().Trim() =="1002" || lst[6].ToString().Trim() =="1003" || lst[6].ToString().Trim() =="1004"  || lst[6].ToString().Trim() =="1021")
					//issue #118 end
					{
					
					}
					else
					{
						ArrayList list =new ArrayList();
						list.Add("Edit");
						list.Add(e.Item.Cells[4].Text.ToString());
						list.Add(e.Item.Cells[5].Text.ToString());
						Session["Command"]=list;
						Response.Redirect("PNOAdvanced.aspx");
					}
				}
				
				if(e.CommandName =="Dwg_Command")
				{
					string dwg="";
					dwg=db.SelectSTDDrawing(e.Item.Cells[4].Text.ToString(),e.Item.Cells[5].Text.ToString());
					if(dwg.ToString().Trim() !="2")
					{
						err.Text="<script language='javascript'>window.open('Drawings.aspx?qno="+e.Item.Cells[4].Text.ToString()+"&pno="+e.Item.Cells[5].Text.ToString()+"','_blank','toolbar=yes,width=1000,height=800,resizable=yes,top=0,left=0')</script>";
					}
					else
					{
						err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
					}
				}
				if(e.CommandName =="LeadTimeCommand")
				{
					string del="";
					del=db.SelectCustomerDelivery(e.Item.Cells[4].Text.ToString().Trim());
					//issue #264 start
					//back
					//if(del.Trim().ToUpper() =="TBA")
					//update
					if(del.Trim().ToUpper() =="TBA" || del.Trim().ToUpper() =="Consult factory")
					//issue #264 end
					{
						PLeedtime.Visible=true;
						TxtRLTTo.Text ="danny@cowandynamics.com";
						TxtRLTSubject.Text="Lead Time Request RFQ # "+e.Item.Cells[4].Text.ToString().Trim();
						lblrfldate.Text=e.Item.Cells[1].Text.ToString().Trim();
						lblrflqno.Text=e.Item.Cells[4].Text.ToString().Trim();
						lblrflpno.Text=e.Item.Cells[5].Text.ToString().Trim();
						err.Text="<script>function ScrollView(){var el = document.getElementById('"+TxtRLTMessage.ClientID+@"');if (el != null){el.scrollIntoView();el.focus();}}window.onload = ScrollView;</script>";

					}
					else if(del.Trim().ToUpper() =="UNDER REVIEW")
					{
						err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('The Lead Time for this quote is already requested. It is under review.For more information please contact Cowan.')</script>";
					}
					else
					{
						err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('The Lead Time for this quote was already requested. It is "+del.Trim()+".For more information please contact Cowan.')</script>";
					}
				}
				if(e.CommandName =="ViewFile_Command")
				{
					if(e.Item.Cells[12].Text.ToString().Trim() !="")
					{
						ArrayList lst =new ArrayList();
						lst =(ArrayList)Session["User"];
						//issue #209 start
						//backup
//						if(lst[6].ToString().Trim() =="1009")
//						{
//							err.Text="<script language='javascript'>window.open('ViewUploadedFile_Rotork.aspx?file="+e.Item.Cells[12].Text.ToString().Trim()+"&qno="+e.Item.Cells[4].Text.ToString().Trim()+"','_blank','toolbar=no,width=1000,height=600,resizable=yes,top=0,left=0')</script>";	
//						}
//						else if(lst[6].ToString().Trim() =="1015")
						//update
						//issie #220 start
						//backup
						//if(lst[6].ToString().Trim() =="1015")
						//issue #220 update
						if(lst[6].ToString().Trim() =="10015")
						{
							err.Text="<script language='javascript'>window.open('ViewUploadedFile_SVC_Summit.aspx?file="+e.Item.Cells[12].Text.ToString().Trim()+"&qno="+e.Item.Cells[4].Text.ToString().Trim()+"','_blank','toolbar=no,width=1000,height=600,resizable=yes,top=0,left=0')</script>";	
						}
//						else if(lst[6].ToString().Trim() =="1016")
//						{
//							err.Text="<script language='javascript'>window.open('ViewUploadedFile_Way.aspx?file="+e.Item.Cells[12].Text.ToString().Trim()+"&qno="+e.Item.Cells[4].Text.ToString().Trim()+"','_blank','toolbar=no,width=1000,height=600,resizable=yes,top=0,left=0')</script>";	
//						}
						//issue #209 end
						else
						{
							//issue #228 start
							//update
							//err.Text="<script language='javascript'>window.open('ViewUploadedFile.aspx?file="+e.Item.Cells[12].Text.ToString().Trim()+"&qno="+e.Item.Cells[4].Text.ToString().Trim()+"','_blank','toolbar=no,width=1000,height=600,resizable=yes,top=0,left=0')</script>";	
						    //backup
							err.Text="<script language='javascript'>window.open('ViewUploadedFiles.aspx?file="+e.Item.Cells[12].Text.ToString().Trim()+"&qno="+e.Item.Cells[4].Text.ToString().Trim()+"','_blank','toolbar=no,width=1000,height=600,resizable=yes,top=0,left=0')</script>";	
							//issue #226 end
						}
					}
				}
//				else if(e.CommandName =="ViewDWG_Command")
//				{
//					if(e.Item.Cells[14].Text.ToString().Trim() !="" && e.Item.Cells[14].Text.ToString().Trim() !="&nbsp;")
//					{
//						err.Text="<script language='javascript'>window.open('icyl_drawing/"+e.Item.Cells[14].Text.ToString().Trim()+"','_blank','toolbar=yes,width=1000,height=750,resizable=yes,top=0,left=0')</script>";						
//					}
//					else
//					{
//						err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('No Drawing is attached with this Quote')</script>";
//					}
//				}
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ");
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
			 
		}
		protected void DGUnfinished_Command(Object sender,DataGridCommandEventArgs e) 
		{
			try
			{
				if(e.CommandName =="ViewFile_Command")
				{
					if(e.Item.Cells[10].Text.ToString().Trim() !="")
					{
						ArrayList lst =new ArrayList();
						lst =(ArrayList)Session["User"];
						if(lst[6].ToString().Trim() =="10009")
						{
							err.Text="<script language='javascript'>window.open('ViewUploadedFile_Rotork.aspx?file="+e.Item.Cells[10].Text.ToString().Trim()+"&qno="+e.Item.Cells[4].Text.ToString().Trim()+"','_blank','toolbar=no,width=1000,height=600,resizable=yes,top=0,left=0')</script>";	
						}
						//issue #228

//						if(lst[6].ToString().Trim() =="1009")
//						{
//							err.Text="<script language='javascript'>window.open('ViewUploadedFile_Rotork.aspx?file="+e.Item.Cells[10].Text.ToString().Trim()+"&qno="+e.Item.Cells[4].Text.ToString().Trim()+"','_blank','toolbar=no,width=1000,height=600,resizable=yes,top=0,left=0')</script>";	
//						}
//						else if(lst[6].ToString().Trim() =="1015")
//						{
//							err.Text="<script language='javascript'>window.open('ViewUploadedFile_SVC_Summit.aspx?file="+e.Item.Cells[10].Text.ToString().Trim()+"&qno="+e.Item.Cells[4].Text.ToString().Trim()+"','_blank','toolbar=no,width=1000,height=600,resizable=yes,top=0,left=0')</script>";	
//						}
//						else if(lst[6].ToString().Trim() =="1016")
//						{
//							err.Text="<script language='javascript'>window.open('ViewUploadedFile_Way.aspx?file="+e.Item.Cells[10].Text.ToString().Trim()+"&qno="+e.Item.Cells[4].Text.ToString().Trim()+"','_blank','toolbar=no,width=1000,height=600,resizable=yes,top=0,left=0')</script>";	
//						}
						else
						{
							//issue #228
							//back
							//err.Text="<script language='javascript'>window.open('ViewUploadedFile.aspx?file="+e.Item.Cells[10].Text.ToString().Trim()+"&qno="+e.Item.Cells[4].Text.ToString().Trim()+"','_blank','toolbar=no,width=1000,height=600,resizable=yes,top=0,left=0')</script>";	
							//backup
							err.Text="<script language='javascript'>window.open('ViewUploadedFiles.aspx?file="+e.Item.Cells[10].Text.ToString().Trim()+"&qno="+e.Item.Cells[4].Text.ToString().Trim()+"','_blank','toolbar=no,width=1000,height=600,resizable=yes,top=0,left=0')</script>";	
							//issue #228 end
						}
					}
				}
				else if(e.CommandName =="ViewDWG_Command")
				{
					if(e.Item.Cells[12].Text.ToString().Trim() !="" && e.Item.Cells[12].Text.ToString().Trim() !="&nbsp;")
					{
						err.Text="<script language='javascript'>window.open('icyl_drawing/"+e.Item.Cells[12].Text.ToString().Trim()+"','_blank','toolbar=yes,width=1000,height=750,resizable=yes,top=0,left=0')</script>";						
					}
					else
					{
						err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('No Drawing is attached with this Quote')</script>";
					}
				}
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
			 
		}

		protected void  DGArchive_PageChanger(Object Source ,DataGridPageChangedEventArgs E)
		{
			if (SortExpression == "") 
			{
				SortExpression = "QuoteDate DESC";
			} 
			if(pdate.Visible ==true)
			{
				DateTime dt1=Convert.ToDateTime(TxtDate1.Text.ToString());
				DateTime dt2=Convert.ToDateTime(TxtDate2.Text.ToString());
				if(dt1 > dt2)
				{
					lblinfo.Text ="Please select date appropriately";
				}
				else
				{
					try
					{
						ArrayList lst =new ArrayList();
						lst =(ArrayList)Session["User"];
						if(RBLOptions.SelectedIndex ==0)
						{
							DGArchive.CurrentPageIndex =E.NewPageIndex ;
							PArchive.Visible= true;
							PUnfinshed.Visible= false;
							DGArchive.DataSource=CreateDataSource3(lst[6].ToString().Trim(),"","1","",dt1.ToShortDateString(),dt2.ToShortDateString(),"","");
							DGArchive.DataBind();
											
						}
						if(RBLOptions.SelectedIndex ==1)
						{
							DGUnfinished.CurrentPageIndex =E.NewPageIndex ;
							PUnfinshed.Visible= true;
							PArchive.Visible= false;
							DGUnfinished.DataSource=CreateDataSource4(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","",dt1.ToShortDateString(),dt2.ToShortDateString(),"","");
							DGUnfinished.DataBind();
						
						}
					}
					catch(Exception ex)
					{
						string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
						s.Replace("'"," ");
						err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
					}
					
				}
			}
			else if(pqno.Visible ==true)
			{
				try
				{
					ArrayList lst =new ArrayList();
					lst =(ArrayList)Session["User"];
					if(TxtQno.Text !="")
					{
						if(RBLOptions.SelectedIndex ==0)
						{
							DGArchive.CurrentPageIndex =E.NewPageIndex ;
							PArchive.Visible= true;
							PUnfinshed.Visible= false;
							DGArchive.DataSource=CreateDataSource(lst[6].ToString().Trim(),"","1","","",TxtQno.Text.Trim(),"");
							DGArchive.DataBind();
							
						}
						if(RBLOptions.SelectedIndex ==1)
						{
							DGUnfinished.CurrentPageIndex =E.NewPageIndex ;
							PUnfinshed.Visible= true;
							PArchive.Visible= false;
							DGUnfinished.DataSource=CreateDataSource1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","","",TxtQno.Text.Trim(),"");
							DGUnfinished.DataBind();
						}
					}
				}
				catch(Exception ex)
				{
					string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
					s.Replace("'"," ");
					err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				}
			}
			else if(ppno.Visible ==true)
			{
				try
				{
					ArrayList lst =new ArrayList();
					lst =(ArrayList)Session["User"];
					if(TxtPno.Text !="")
					{
						if(RBLOptions.SelectedIndex ==0)
						{
							string strPgidx=DGArchive.PageCount.ToString();
							DGArchive.CurrentPageIndex =E.NewPageIndex ;
							PArchive.Visible= true;
							PUnfinshed.Visible= false;
							//issue #209 start
							//back
							//DGArchive.DataSource=CreateDataSource(lst[6].ToString().Trim(),"","1","","","",TxtPno.Text.Trim());
							//update
							switch(RBLSearch.SelectedIndex)
							{
								case 2 :
									DGArchive.DataSource=CreateDataSource(lst[6].ToString().Trim(),"","1","","","",TxtPno.Text.Trim());
									break;
								case 3 :
									DGArchive.DataSource=CreateDataSource_Project(lst[6].ToString().Trim(),"","1","","","","",TxtPno.Text.Trim());
									break;

							}
							
							//issue #209 end
							strPgidx=DGArchive.PageCount.ToString();
							strPgidx=DGArchive.CurrentPageIndex.ToString();
							DGArchive.DataBind();
							
						}
						if(RBLOptions.SelectedIndex ==1)
						{
							DGUnfinished.CurrentPageIndex =E.NewPageIndex ;
							PUnfinshed.Visible= true;
							PArchive.Visible= false;
							//issue #209 start
							//back
							//DGUnfinished.DataSource=CreateDataSource1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","","","",TxtPno.Text.Trim());
							//update
							DGArchive.DataSource=CreateDataSource_Project(lst[6].ToString().Trim(),"","0","","","","",TxtPno.Text.Trim());
							//issue #209 end
							DGUnfinished.DataBind();
						}
					}
				}	
				catch(Exception ex)
				{
					string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
					s.Replace("'"," ");
					err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				}
				
			}
		 }
		protected void  DGUnfinished_PageChanger(Object Source ,DataGridPageChangedEventArgs E)
		{
			if (SortExpression == "") 
			{
				SortExpression = "QuoteDate DESC";
			} 
			if(pdate.Visible ==true)
			{
				DateTime dt1=Convert.ToDateTime(TxtDate1.Text.ToString());
				DateTime dt2=Convert.ToDateTime(TxtDate2.Text.ToString());
				if(dt1 > dt2)
				{
					lblinfo.Text ="Please select date appropriately";
				}
				else
				{
					try
					{
						ArrayList lst =new ArrayList();
						lst =(ArrayList)Session["User"];
						if(RBLOptions.SelectedIndex ==0)
						{
							DGArchive.CurrentPageIndex =E.NewPageIndex ;
							PArchive.Visible= true;
							PUnfinshed.Visible= false;
							DGArchive.DataSource=CreateDataSource3(lst[6].ToString().Trim(),"","1","",dt1.ToShortDateString(),dt2.ToShortDateString(),"","");
							DGArchive.DataBind();
											
						}
						if(RBLOptions.SelectedIndex ==1)
						{
							DGUnfinished.CurrentPageIndex =E.NewPageIndex ;
							PUnfinshed.Visible= true;
							PArchive.Visible= false;
							DGUnfinished.DataSource=CreateDataSource4(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","",dt1.ToShortDateString(),dt2.ToShortDateString(),"","");
							DGUnfinished.DataBind();
						
						}
					}
					catch(Exception ex)
					{
						string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
						s.Replace("'"," ");
						err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
					}
					
				}
			}
			else if(pqno.Visible ==true)
			{
				try
				{
					ArrayList lst =new ArrayList();
					lst =(ArrayList)Session["User"];
					if(TxtQno.Text !="")
					{
						if(RBLOptions.SelectedIndex ==0)
						{
							DGArchive.CurrentPageIndex =E.NewPageIndex ;
							PArchive.Visible= true;
							PUnfinshed.Visible= false;
							DGArchive.DataSource=CreateDataSource(lst[6].ToString().Trim(),"","1","","",TxtQno.Text.Trim(),"");
							DGArchive.DataBind();
							
						}
						if(RBLOptions.SelectedIndex ==1)
						{
							DGUnfinished.CurrentPageIndex =E.NewPageIndex ;
							PUnfinshed.Visible= true;
							PArchive.Visible= false;
							DGUnfinished.DataSource=CreateDataSource1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","","",TxtQno.Text.Trim(),"");
							DGUnfinished.DataBind();
						}
					}
				}
				catch(Exception ex)
				{
					string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
					s.Replace("'"," ");
					err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				}
			}
			else if(ppno.Visible ==true)
			{
				try
				{
					ArrayList lst =new ArrayList();
					lst =(ArrayList)Session["User"];
					if(TxtPno.Text !="")
					{
						if(RBLOptions.SelectedIndex ==0)
						{
							DGArchive.CurrentPageIndex =E.NewPageIndex ;
							PArchive.Visible= true;
							PUnfinshed.Visible= false;
							DGArchive.DataSource=CreateDataSource(lst[6].ToString().Trim(),"","1","","","",TxtPno.Text.Trim());
							DGArchive.DataBind();
							
						}
						if(RBLOptions.SelectedIndex ==1)
						{
							DGUnfinished.CurrentPageIndex =E.NewPageIndex ;
							PUnfinshed.Visible= true;
							PArchive.Visible= false;
							DGUnfinished.DataSource=CreateDataSource1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","","","",TxtPno.Text.Trim());
							DGUnfinished.DataBind();
						}
					}
				}	
				catch(Exception ex)
				{
					string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
					s.Replace("'"," ");
					err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				}
				
			}
		}
		public ICollection CreateDataSource(string customerid,string usr,string finish,string contact,string qdate,string qno,string pno) 
		{
			try
			{
			 
				err.Text="";
				DataSet ds=new DataSet();
				DataSet ds1=new DataSet();
				DataSet ds2=new DataSet();
				DataSet ds3=new DataSet();
				ds =db.GetRifinedQuotes(customerid.Trim(), usr.Trim(),finish.Trim(),contact.Trim(),qdate.Trim(),qno.Trim(),pno.Trim());
				ds1=db.GetRifinedAccesories(customerid.Trim(), usr.Trim(),finish.Trim(),contact.Trim(),qdate.Trim(),qno.Trim(),pno.Trim());
				ds2=db.GetRifinedRepairKit(customerid.Trim(), usr.Trim(),finish.Trim(),contact.Trim(),qdate.Trim(),qno.Trim(),pno.Trim());
				ds3=db.GetRifinedZ101(customerid.Trim(),usr.Trim(),finish.Trim(),contact.Trim(),qdate.Trim(),qno.Trim(),pno.Trim());
				
				DataTable dt = new DataTable();
				DataRow dr;
				dt.Columns.Add(new DataColumn("Contact"));
				dt.Columns.Add(new DataColumn("QuoteDate",System.Type.GetType("System.DateTime")));
				dt.Columns.Add(new DataColumn("QuoteNo"));
				dt.Columns.Add(new DataColumn("QuoteNo1"));
				dt.Columns.Add(new DataColumn("QuoteNo2"));
				dt.Columns.Add(new DataColumn("PartNo1"));
				dt.Columns.Add(new DataColumn("PartNo"));
				dt.Columns.Add(new DataColumn("TotalPrice"));
				dt.Columns.Add(new DataColumn("Note"));
				dt.Columns.Add(new DataColumn("File"));
				dt.Columns.Add(new DataColumn("FileName"));
				dt.Columns.Add(new DataColumn("Dwg"));
				dt.Columns.Add(new DataColumn("Dwg1"));
				for (int i = 0; i < ds.Tables[0].Rows.Count; i++) 
				{
					dr = dt.NewRow();
					dr[0] = (string) ds.Tables[0].Rows[i]["Contact"];
					if(ds.Tables[0].Rows[i]["QuoteDate"].ToString() !="")
					{
						dr[1] = (string) ds.Tables[0].Rows[i]["QuoteDate"];
					}
					dr[2] = (string) ds.Tables[0].Rows[i]["QuoteNo"];
					dr[3] = (string) ds.Tables[0].Rows[i]["QuoteNo"].ToString().Substring(1);
					dr[4] = (string) ds.Tables[0].Rows[i]["QuoteNo"];
					dr[5] = (string) ds.Tables[0].Rows[i]["PartNo"];
					dr[6] = (string) ds.Tables[0].Rows[i]["PartNo"];
					dr[7] =  String.Format("{0:$###,##0.00}",Convert.ToDecimal(ds.Tables[0].Rows[i]["TotalPrice"]));
					dr[8] = (string) ds.Tables[0].Rows[i]["Note"];
					if( ds.Tables[0].Rows[i]["UploadFile"].ToString().Trim() !="")
					{
						dr[9] = "View";
						dr[10] = ds.Tables[0].Rows[i]["UploadFile"].ToString().Trim();
					}
					else
					{
						dr[9] = "";
						dr[10] = "";
					}
					if( ds.Tables[0].Rows[i]["Dwg"].ToString().Trim() !="")
					{
						dr[11] = "View";
						dr[12] = ds.Tables[0].Rows[i]["Dwg"].ToString().Trim();
					}
					else
					{
						dr[11] = "";
						dr[12] = "";
					}
					dt.Rows.Add(dr);
				}
				for (int i = 0; i < ds3.Tables[0].Rows.Count; i++) 
				{
					dr = dt.NewRow();
					dr[0] = (string) ds3.Tables[0].Rows[i]["Contact"];
					if(ds3.Tables[0].Rows[i]["QuoteDate"].ToString() !="")
					{
						dr[1] = (string) ds3.Tables[0].Rows[i]["QuoteDate"];
					}
					dr[2] = (string) ds3.Tables[0].Rows[i]["QuoteNo"];
					dr[3] = (string) ds3.Tables[0].Rows[i]["QuoteNo"].ToString().Substring(1);
					dr[4] = (string) ds3.Tables[0].Rows[i]["QuoteNo"];
					dr[5] = (string) ds3.Tables[0].Rows[i]["Zno"];
					dr[6] = (string) ds3.Tables[0].Rows[i]["Zno"];
					dr[7] =  String.Format("{0:$###,###.00}",Convert.ToDecimal(ds3.Tables[0].Rows[i]["TotalPrice"]));
					dr[8] = (string) ds3.Tables[0].Rows[i]["Note"];
					dr[9] = "";
					dr[10] = "";
					dr[11] = "";
					dr[12] = "";
					dt.Rows.Add(dr);
				}
				for (int i = 0; i < ds1.Tables[0].Rows.Count; i++) 
				{
					dr = dt.NewRow();
					dr[0] = (string) ds1.Tables[0].Rows[i]["Contact"];
					if(ds1.Tables[0].Rows[i]["QuoteDate"].ToString() !="")
					{
						dr[1] = (string) ds1.Tables[0].Rows[i]["QuoteDate"];
					}
					dr[2] = (string) ds1.Tables[0].Rows[i]["QuoteNo"];
					dr[3] = (string) ds1.Tables[0].Rows[i]["QuoteNo"].ToString().Substring(1);
					dr[4] = (string) ds1.Tables[0].Rows[i]["QuoteNo"];
					dr[5] = (string) ds1.Tables[0].Rows[i]["PartNo"];
					dr[6] = (string) ds1.Tables[0].Rows[i]["PartNo"];
					dr[7] =  String.Format("{0:$###,###.00}",Convert.ToDecimal(ds1.Tables[0].Rows[i]["TotalPrice"]));
					dr[8] = (string) ds1.Tables[0].Rows[i]["Note"];
					dr[9] = "";
					dr[10] = "";
					dr[11] = "";
					dr[12] = "";
					dt.Rows.Add(dr);
				}
				for (int i = 0; i < ds2.Tables[0].Rows.Count; i++) 
				{
					dr = dt.NewRow();
					dr[0] = (string) ds2.Tables[0].Rows[i]["Contact"];
					if(ds2.Tables[0].Rows[i]["QuoteDate"].ToString() !="")
					{
						dr[1] = (string) ds2.Tables[0].Rows[i]["QuoteDate"];
					}
					dr[2] = (string) ds2.Tables[0].Rows[i]["QuoteNo"];
					dr[3] = (string) ds2.Tables[0].Rows[i]["QuoteNo"].ToString().Substring(1);
					dr[4] = (string) ds2.Tables[0].Rows[i]["QuoteNo"];
					dr[5] = (string) ds2.Tables[0].Rows[i]["PartNo"];
					dr[6] = (string) ds2.Tables[0].Rows[i]["PartNo"];
					dr[7] =  String.Format("{0:$###,###.00}",Convert.ToDecimal(ds2.Tables[0].Rows[i]["TotalPrice"]));
					dr[8] = (string) ds2.Tables[0].Rows[i]["Note"];
					dr[9] = "";
					dr[10] = "";
					dr[11] = "";
					dr[12] = "";
					dt.Rows.Add(dr);
				}
				DataView dv = new DataView(dt);
			
				dv.Sort = SortExpression;
				return dv;
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s=s.Replace("'"," ");
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				return null;
			}	
		}
		public ICollection CreateDataSource_Project(string customerid,string usr,string finish,string contact,string qdate,string qno,string pno,string project) 
		{
			try
			{
			 
				err.Text="";
				DataSet ds=new DataSet();
				DataSet ds1=new DataSet();
				DataSet ds2=new DataSet();
				DataSet ds3=new DataSet();
				ds =db.GetRifinedQuotes_Project(customerid.Trim(), usr.Trim(),finish.Trim(),contact.Trim(),qdate.Trim(),qno.Trim(),pno.Trim(),project.Trim());
				DataTable dt = new DataTable();
				DataRow dr;
				dt.Columns.Add(new DataColumn("Contact"));
				dt.Columns.Add(new DataColumn("QuoteDate",System.Type.GetType("System.DateTime")));
				dt.Columns.Add(new DataColumn("QuoteNo"));
				dt.Columns.Add(new DataColumn("QuoteNo1"));
				dt.Columns.Add(new DataColumn("QuoteNo2"));
				dt.Columns.Add(new DataColumn("PartNo1"));
				dt.Columns.Add(new DataColumn("PartNo"));
				dt.Columns.Add(new DataColumn("TotalPrice"));
				dt.Columns.Add(new DataColumn("Note"));
				dt.Columns.Add(new DataColumn("File"));
				dt.Columns.Add(new DataColumn("FileName"));
				dt.Columns.Add(new DataColumn("Dwg"));
				dt.Columns.Add(new DataColumn("Dwg1"));
				for (int i = 0; i < ds.Tables[0].Rows.Count; i++) 
				{
					dr = dt.NewRow();
					dr[0] = (string) ds.Tables[0].Rows[i]["Contact"];
					if(ds.Tables[0].Rows[i]["QuoteDate"].ToString() !="")
					{
						dr[1] = (string) ds.Tables[0].Rows[i]["QuoteDate"];
					}
					dr[2] = (string) ds.Tables[0].Rows[i]["QuoteNo"];
					dr[3] = (string) ds.Tables[0].Rows[i]["QuoteNo"].ToString().Substring(1);
					dr[4] = (string) ds.Tables[0].Rows[i]["QuoteNo"];
					dr[5] = (string) ds.Tables[0].Rows[i]["PartNo"];
					dr[6] = (string) ds.Tables[0].Rows[i]["PartNo"];
					dr[7] =  String.Format("{0:$###,##0.00}",Convert.ToDecimal(ds.Tables[0].Rows[i]["TotalPrice"]));
					dr[8] = (string) ds.Tables[0].Rows[i]["Note"];
					if( ds.Tables[0].Rows[i]["UploadFile"].ToString().Trim() !="")
					{
						dr[9] = "View";
						dr[10] = ds.Tables[0].Rows[i]["UploadFile"].ToString().Trim();
					}
					else
					{
						dr[9] = "";
						dr[10] = "";
					}
					if( ds.Tables[0].Rows[i]["Dwg"].ToString().Trim() !="")
					{
						dr[11] = "View";
						dr[12] = ds.Tables[0].Rows[i]["Dwg"].ToString().Trim();
					}
					else
					{
						dr[11] = "";
						dr[12] = "";
					}
					dt.Rows.Add(dr);
				}				
				DataView dv = new DataView(dt);			
				dv.Sort = SortExpression;
				return dv;
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s=s.Replace("'"," ");
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				return null;
			}	
		}
		public ICollection CreateDataSource1(string customerid,string usr,string finish,string contact,string qdate,string qno,string pno) 
		{
			try
			{
			 
				err.Text="";
				DataSet ds=new DataSet();
				DataSet ds1=new DataSet();
				DataSet ds2=new DataSet();
				DataSet ds3=new DataSet();
				ds =db.GetRifinedQuotes1(customerid.Trim(), usr.Trim(),finish.Trim(),contact.Trim(),qdate.Trim(),qno.Trim(),pno.Trim());
				ds1=db.GetRifinedAccesories1(customerid.Trim(), usr.Trim(),finish.Trim(),contact.Trim(),qdate.Trim(),qno.Trim(),pno.Trim());
				ds2=db.GetRifinedRepairKit1(customerid.Trim(), usr.Trim(),finish.Trim(),contact.Trim(),qdate.Trim(),qno.Trim(),pno.Trim());
				ds3=db.GetRifinedZ1011(customerid.Trim(),usr.Trim(),finish.Trim(),contact.Trim(),qdate.Trim(),qno.Trim(),pno.Trim());

				DataTable dt = new DataTable();
				DataRow dr;
				dt.Columns.Add(new DataColumn("Contact"));
				dt.Columns.Add(new DataColumn("QuoteDate",System.Type.GetType("System.DateTime")));
				dt.Columns.Add(new DataColumn("QuoteNo"));
				dt.Columns.Add(new DataColumn("QuoteNo1"));
				dt.Columns.Add(new DataColumn("QuoteNo2"));
				dt.Columns.Add(new DataColumn("PartNo"));
				dt.Columns.Add(new DataColumn("Note"));
				dt.Columns.Add(new DataColumn("Specialrequest"));
				dt.Columns.Add(new DataColumn("FinishedDate"));
				dt.Columns.Add(new DataColumn("CowanQno"));
				dt.Columns.Add(new DataColumn("File"));
				dt.Columns.Add(new DataColumn("FileName"));
				dt.Columns.Add(new DataColumn("Dwg"));
				dt.Columns.Add(new DataColumn("Dwg1"));
				for (int i = 0; i < ds.Tables[0].Rows.Count; i++) 
				{
					dr = dt.NewRow();
					dr[0] = (string) ds.Tables[0].Rows[i]["Contact"];
					if(ds.Tables[0].Rows[i]["QuoteDate"].ToString() !="")
					{
						dr[1] = (string) ds.Tables[0].Rows[i]["QuoteDate"];
					}
					dr[2] = (string) ds.Tables[0].Rows[i]["QuoteNo"];
					dr[3] = (string) ds.Tables[0].Rows[i]["QuoteNo"].ToString().Substring(1);
					dr[4] = (string) ds.Tables[0].Rows[i]["QuoteNo"];
					dr[5] = (string) ds.Tables[0].Rows[i]["PartNo"];
					dr[6] = (string) ds.Tables[0].Rows[i]["Note"];
					dr[7] = (string) ds.Tables[0].Rows[i]["Finish"];
					dr[8] = (string) ds.Tables[0].Rows[i]["FinishedDate"];
					dr[9] = (string) ds.Tables[0].Rows[i]["CowanQno"];
					if( ds.Tables[0].Rows[i]["UploadFile"].ToString().Trim() !="")
					{
						dr[10] = "View";
						dr[11] = ds.Tables[0].Rows[i]["UploadFile"].ToString().Trim();
					}
					else
					{
						dr[10] = "";
						dr[11] = "";
					}
					if( ds.Tables[0].Rows[i]["Dwg"].ToString().Trim() !="")
					{
						dr[12] = "View";
						dr[13] = ds.Tables[0].Rows[i]["Dwg"].ToString().Trim();
					}
					else
					{
						dr[12] = "";
						dr[13] = "";
					}
					dt.Rows.Add(dr);
				}
				for (int i = 0; i < ds3.Tables[0].Rows.Count; i++) 
				{
					dr = dt.NewRow();
					dr[0] = (string) ds3.Tables[0].Rows[i]["Contact"];
					if(ds3.Tables[0].Rows[i]["QuoteDate"].ToString() !="")
					{
						dr[1] = (string) ds3.Tables[0].Rows[i]["QuoteDate"];
					}
					dr[2] = (string) ds3.Tables[0].Rows[i]["QuoteNo"];
					dr[3] = (string) ds3.Tables[0].Rows[i]["QuoteNo"].ToString().Substring(1);
					dr[4] = (string) ds3.Tables[0].Rows[i]["QuoteNo"];
					dr[5] = (string) ds3.Tables[0].Rows[i]["Zno"];
					dr[6] = (string) ds3.Tables[0].Rows[i]["Note"];
					dr[7] =  " ";
					dr[8] = (string) ds3.Tables[0].Rows[i]["FinishedDate"];
					dr[9] = (string) ds3.Tables[0].Rows[i]["CowanQno"];
					dr[10] = "";
					dr[11] = "";
					dr[12] = "";
					dr[13] = "";
					dt.Rows.Add(dr);
				}
				for (int i = 0; i < ds1.Tables[0].Rows.Count; i++) 
				{
					dr = dt.NewRow();
					dr[0] = (string) ds1.Tables[0].Rows[i]["Contact"];
					if(ds1.Tables[0].Rows[i]["QuoteDate"].ToString() !="")
					{
						dr[1] = (string) ds1.Tables[0].Rows[i]["QuoteDate"];
					}
					dr[2] = (string) ds1.Tables[0].Rows[i]["QuoteNo"];
					dr[3] = (string) ds1.Tables[0].Rows[i]["QuoteNo"].ToString().Substring(1);
					dr[4] = (string) ds1.Tables[0].Rows[i]["QuoteNo"];
					dr[5] = (string) ds1.Tables[0].Rows[i]["PartNo"];
					dr[6] = (string) ds1.Tables[0].Rows[i]["Note"];
					dr[7] =  " ";
					dr[8] = (string) ds1.Tables[0].Rows[i]["FinishedDate"];
					dr[9] = (string) ds1.Tables[0].Rows[i]["CowanQno"];
					dr[10] = "";
					dr[11] = "";
					dr[12] = "";
					dr[13] = "";
					dt.Rows.Add(dr);
				}
				for (int i = 0; i < ds2.Tables[0].Rows.Count; i++) 
				{
					dr = dt.NewRow();
					dr[0] = (string) ds2.Tables[0].Rows[i]["Contact"];
					if(ds2.Tables[0].Rows[i]["QuoteDate"].ToString() !="")
					{
						dr[1] = (string) ds2.Tables[0].Rows[i]["QuoteDate"];
					}
					dr[2] = (string) ds2.Tables[0].Rows[i]["QuoteNo"];
					dr[3] = (string) ds2.Tables[0].Rows[i]["QuoteNo"].ToString().Substring(1);
					dr[4] = (string) ds2.Tables[0].Rows[i]["QuoteNo"];
					dr[5] = (string) ds2.Tables[0].Rows[i]["PartNo"];
					dr[6] = (string) ds2.Tables[0].Rows[i]["Note"];
					dr[7] =  " ";
					dr[8] = (string) ds2.Tables[0].Rows[i]["FinishedDate"];
					dr[9] = (string) ds2.Tables[0].Rows[i]["CowanQno"];
					dr[10] = "";
					dr[11] = "";
					dr[12] = "";
					dr[13] = "";
					dt.Rows.Add(dr);
				}
				DataView dv = new DataView(dt);
			
				dv.Sort = SortExpression;
				return dv;
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s=s.Replace("'"," ");
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				return null;
			}	
		}
		public ICollection CreateDataSource_ProjectNo(string customerid,string usr,string finish,string contact,string qdate,string qno,string pno,string projectno) 
		{
			try
			{
			 
				err.Text="";
				DataSet ds=new DataSet();
				DataSet ds1=new DataSet();
				DataSet ds2=new DataSet();
				DataSet ds3=new DataSet();
				ds =db.GetRifinedQuotes_ProjectNo(customerid.Trim(), usr.Trim(),finish.Trim(),contact.Trim(),qdate.Trim(),qno.Trim(),pno.Trim(),projectno.Trim());
				DataTable dt = new DataTable();
				DataRow dr;
				dt.Columns.Add(new DataColumn("Contact"));
				dt.Columns.Add(new DataColumn("QuoteDate",System.Type.GetType("System.DateTime")));
				dt.Columns.Add(new DataColumn("QuoteNo"));
				dt.Columns.Add(new DataColumn("QuoteNo1"));
				dt.Columns.Add(new DataColumn("QuoteNo2"));
				dt.Columns.Add(new DataColumn("PartNo"));
				dt.Columns.Add(new DataColumn("Note"));
				dt.Columns.Add(new DataColumn("Specialrequest"));
				dt.Columns.Add(new DataColumn("FinishedDate"));
				dt.Columns.Add(new DataColumn("CowanQno"));
				dt.Columns.Add(new DataColumn("File"));
				dt.Columns.Add(new DataColumn("FileName"));
				dt.Columns.Add(new DataColumn("Dwg"));
				dt.Columns.Add(new DataColumn("Dwg1"));
				for (int i = 0; i < ds.Tables[0].Rows.Count; i++) 
				{
					dr = dt.NewRow();
					dr[0] = (string) ds.Tables[0].Rows[i]["Contact"];
					if(ds.Tables[0].Rows[i]["QuoteDate"].ToString() !="")
					{
						dr[1] = (string) ds.Tables[0].Rows[i]["QuoteDate"];
					}
					dr[2] = (string) ds.Tables[0].Rows[i]["QuoteNo"];
					dr[3] = (string) ds.Tables[0].Rows[i]["QuoteNo"].ToString().Substring(1);
					dr[4] = (string) ds.Tables[0].Rows[i]["QuoteNo"];
					dr[5] = (string) ds.Tables[0].Rows[i]["PartNo"];
					dr[6] = (string) ds.Tables[0].Rows[i]["Note"];
					dr[7] = (string) ds.Tables[0].Rows[i]["Finish"];
					dr[8] = (string) ds.Tables[0].Rows[i]["FinishedDate"];
					dr[9] = (string) ds.Tables[0].Rows[i]["CowanQno"];
					if( ds.Tables[0].Rows[i]["UploadFile"].ToString().Trim() !="")
					{
						dr[10] = "View";
						dr[11] = ds.Tables[0].Rows[i]["UploadFile"].ToString().Trim();
					}
					else
					{
						dr[10] = "";
						dr[11] = "";
					}
					if( ds.Tables[0].Rows[i]["Dwg"].ToString().Trim() !="")
					{
						dr[12] = "View";
						dr[13] = ds.Tables[0].Rows[i]["Dwg"].ToString().Trim();
					}
					else
					{
						dr[12] = "";
						dr[13] = "";
					}
					dt.Rows.Add(dr);
				}
				DataView dv = new DataView(dt);
				dv.Sort = SortExpression;
				return dv;
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s=s.Replace("'"," ");
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				return null;
			}	
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.RBLOptions.SelectedIndexChanged += new System.EventHandler(this.RBLOptions_SelectedIndexChanged);
			this.RBLSearch.SelectedIndexChanged += new System.EventHandler(this.RBLSearch_SelectedIndexChanged);
			this.BtnSearh.Click += new System.EventHandler(this.BtnSearh_Click);
			this.DDLAContacts.SelectedIndexChanged += new System.EventHandler(this.DDLAContacts_SelectedIndexChanged);
			this.DDLACreatedate.SelectedIndexChanged += new System.EventHandler(this.DDLACreatedate_SelectedIndexChanged);
			this.DDLAQNo.SelectedIndexChanged += new System.EventHandler(this.DDLAQNo_SelectedIndexChanged);
			this.DDLAPartNo.SelectedIndexChanged += new System.EventHandler(this.DDLAPartNo_SelectedIndexChanged);
			this.DDLUContacts.SelectedIndexChanged += new System.EventHandler(this.DDLUContacts_SelectedIndexChanged);
			this.DDLUCreateDate.SelectedIndexChanged += new System.EventHandler(this.DDLUCreateDate_SelectedIndexChanged);
			this.DDLUQuoteNO.SelectedIndexChanged += new System.EventHandler(this.DDLUQuoteNO_SelectedIndexChanged);
			this.DDLUPartNo.SelectedIndexChanged += new System.EventHandler(this.DDLUPartNo_SelectedIndexChanged);
			this.BtnSend.Click += new System.EventHandler(this.BtnSend_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		private void RBLOptions_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				if (SortExpression == "") 
				{
					SortExpression = "QuoteDate DESC";
				} 
				err.Text="";
				DateTime dt1=Convert.ToDateTime(TxtDate1.Text.ToString());
				DateTime dt2=Convert.ToDateTime(TxtDate2.Text.ToString());
				ArrayList lst =new ArrayList();
				lst =(ArrayList)Session["User"];

				if(RBLOptions.SelectedIndex ==0)
				{
					DGArchive.CurrentPageIndex = 0 ;
					PArchive.Visible= true;
					PUnfinshed.Visible= false;
					DGArchive.DataSource=CreateDataSource3(lst[6].ToString().Trim(),"","1","",dt1.ToShortDateString(),dt2.ToShortDateString(),"","");
					DGArchive.DataBind();
					ArrayList colist=new ArrayList();
					colist=db.GetContacts1(lst[6].ToString().Trim(),"","1",dt1.ToShortDateString(),dt2.ToShortDateString());
					DDLAContacts.Items.Clear();
					DDLAContacts.Items.Add("Contacts");
					for(int i=0; i<colist.Count;i++)
					{
						DDLAContacts.Items.Add(colist[i].ToString());
					}
					ArrayList qdlist=new ArrayList();
					qdlist=db.Getquotedate1(lst[6].ToString().Trim(),"","1","",dt1.ToShortDateString(),dt2.ToShortDateString());
					DDLACreatedate.Items.Clear();
					DDLACreatedate.Items.Add("Quote Date");
					for(int i=0; i<qdlist.Count;i++)
					{
						DDLACreatedate.Items.Add(qdlist[i].ToString());
					}
				
					ArrayList qnlist=new ArrayList();
					qnlist=db.GetQno1(lst[6].ToString().Trim(),"","1","",dt1.ToShortDateString(),dt2.ToShortDateString());
					DDLAQNo.Items.Clear();
					DDLAQNo.Items.Add("Quote No");
					for(int i=0; i<qnlist.Count;i++)
					{
						DDLAQNo.Items.Add(qnlist[i].ToString());
					}

					ArrayList plist=new ArrayList();
					plist=db.GetPno1(lst[6].ToString().Trim(),"","1","",dt1.ToShortDateString(),dt2.ToShortDateString(),"");
					ArrayList zlist=new ArrayList();
					zlist=db.GetZno_1(lst[6].ToString().Trim(),"","1","",dt1.ToShortDateString(),dt2.ToShortDateString(),"");
					ArrayList alist=new ArrayList();
					alist=db.GetAno1(lst[6].ToString().Trim(),"","1","",dt1.ToShortDateString(),dt2.ToShortDateString(),"");
					ArrayList rlist=new ArrayList();
					rlist=db.GetRno1(lst[6].ToString().Trim(),"","1","",dt1.ToShortDateString(),dt2.ToShortDateString(),"");
					
					DDLAPartNo.Items.Clear();
					DDLAPartNo.Items.Add("Part No");
					for(int i=0; i<plist.Count;i++)
					{
						DDLAPartNo.Items.Add(plist[i].ToString());
					}
					for(int i=0; i<zlist.Count;i++)
					{
						DDLAPartNo.Items.Add(zlist[i].ToString());
					}
					for(int i=0; i<alist.Count;i++)
					{
						DDLAPartNo.Items.Add(alist[i].ToString());
					}
					for(int i=0; i<rlist.Count;i++)
					{
						DDLAPartNo.Items.Add(rlist[i].ToString());
					}
							
				}
				if(RBLOptions.SelectedIndex ==1)
				{
					DGUnfinished.CurrentPageIndex = 0 ;
					PUnfinshed.Visible= true;
					PArchive.Visible= false;
					DGUnfinished.DataSource=CreateDataSource4(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","",dt1.ToShortDateString(),dt2.ToShortDateString(),"","");
					DGUnfinished.DataBind();
						
					ArrayList colist=new ArrayList();
					colist=db.GetContacts1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",dt1.ToShortDateString(),dt2.ToShortDateString());
					DDLUContacts.Items.Clear();
					DDLUContacts.Items.Add("Contacts");
					for(int i=0; i<colist.Count;i++)
					{
						DDLUContacts.Items.Add(colist[i].ToString());
					}
					ArrayList qdlist=new ArrayList();
					qdlist=db.Getquotedate1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","",dt1.ToShortDateString(),dt2.ToShortDateString());
					DDLUCreateDate.Items.Clear();
					DDLUCreateDate.Items.Add("Quote Date");
					for(int i=0; i<qdlist.Count;i++)
					{
						DDLUCreateDate.Items.Add(qdlist[i].ToString());
					}
					ArrayList qnlist=new ArrayList();
					qnlist=db.GetQno1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","",dt1.ToShortDateString(),dt2.ToShortDateString());
					DDLUQuoteNO.Items.Clear();
					DDLUQuoteNO.Items.Add("Quote No");
					for(int i=0; i<qnlist.Count;i++)
					{
						DDLUQuoteNO.Items.Add(qnlist[i].ToString());
					}
					ArrayList plist=new ArrayList();
					plist=db.GetPno1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","",dt1.ToShortDateString(),dt2.ToShortDateString(),"");
					ArrayList zlist=new ArrayList();
					zlist=db.GetZno_1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","",dt1.ToShortDateString(),dt2.ToShortDateString(),"");
					ArrayList alist=new ArrayList();
					alist=db.GetAno1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","",dt1.ToShortDateString(),dt2.ToShortDateString(),"");
					ArrayList rlist=new ArrayList();
					rlist=db.GetRno1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","",dt1.ToShortDateString(),dt2.ToShortDateString(),"");
					
					DDLUPartNo.Items.Clear();
					DDLUPartNo.Items.Add("Part No");
					for(int i=0; i<plist.Count;i++)
					{
						DDLUPartNo.Items.Add(plist[i].ToString());
					}
					for(int i=0; i<zlist.Count;i++)
					{
						DDLUPartNo.Items.Add(zlist[i].ToString());
					}
					for(int i=0; i<alist.Count;i++)
					{
						DDLUPartNo.Items.Add(alist[i].ToString());
					}
					for(int i=0; i<rlist.Count;i++)
					{
						DDLUPartNo.Items.Add(rlist[i].ToString());
					}
							
				}
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}

		private void DDLAContacts_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				err.Text="";
				if(DDLAContacts.SelectedIndex >0)
				{
					DDLACreatedate.SelectedIndex=0;
					DDLAQNo.SelectedIndex =0;
					DDLAPartNo.SelectedIndex=0;
					DGArchive.CurrentPageIndex = 0 ;
					err.Text="";
					string cont="";
					string cdate="";
					string pn="";
					string qn="";
					if(DDLAContacts.SelectedIndex > 0)
					{
						cont= DDLAContacts.SelectedItem.Text.Trim();
					}
					if(DDLACreatedate.SelectedIndex > 0)
					{
						cdate= DDLACreatedate.SelectedItem.Text.Trim();
					}
					if(DDLAQNo.SelectedIndex > 0)
					{
						qn= DDLAQNo.SelectedItem.Text.Trim();
					}
					if(pqno.Visible ==true)
					{
						if(TxtQno.Text.Trim() !="")
						{
							qn=TxtQno.Text.Trim();
						}
					}
					if(DDLAPartNo.SelectedIndex > 0)
					{
						pn= DDLAPartNo.SelectedItem.Text.Trim();
					}
					if(ppno.Visible ==true)
					{
						if(TxtPno.Text.Trim() !="")
						{
							pn=TxtPno.Text.Trim();
						}
					}
					ArrayList lst =new ArrayList();
					lst =(ArrayList)Session["User"];
					if(pdate.Visible ==true)
					{
						DateTime dt1=Convert.ToDateTime(TxtDate1.Text.ToString());
						DateTime dt2=Convert.ToDateTime(TxtDate2.Text.ToString());
						DGArchive.DataSource = CreateDataSource3(lst[6].ToString().Trim(),"","1",cont.Trim(),dt1.ToShortDateString(),dt2.ToShortDateString(),qn.Trim(),pn.Trim());
						
						ArrayList qdlist=new ArrayList();
						qdlist=db.Getquotedate1(lst[6].ToString().Trim(),"","1",cont.Trim(),dt1.ToShortDateString(),dt2.ToShortDateString());
						DDLACreatedate.Items.Clear();
						DDLACreatedate.Items.Add("Quote Date");
						for(int i=0; i<qdlist.Count;i++)
						{
							DDLACreatedate.Items.Add(qdlist[i].ToString());
						}
				
						ArrayList qnlist=new ArrayList();
						qnlist=db.GetQno1(lst[6].ToString().Trim(),"","1",cont.Trim(),dt1.ToShortDateString(),dt2.ToShortDateString());
						DDLAQNo.Items.Clear();
						DDLAQNo.Items.Add("Quote No");
						for(int i=0; i<qnlist.Count;i++)
						{
							DDLAQNo.Items.Add(qnlist[i].ToString());
						}

						ArrayList plist=new ArrayList();
						plist=db.GetPno1(lst[6].ToString().Trim(),"","1",cont.Trim(),dt1.ToShortDateString(),dt2.ToShortDateString(),qn.Trim());
						ArrayList zlist=new ArrayList();
						zlist=db.GetZno_1(lst[6].ToString().Trim(),"","1",cont.Trim(),dt1.ToShortDateString(),dt2.ToShortDateString(),qn.Trim());
						ArrayList alist=new ArrayList();
						alist=db.GetAno1(lst[6].ToString().Trim(),"","1",cont.Trim(),dt1.ToShortDateString(),dt2.ToShortDateString(),qn.Trim());
						ArrayList rlist=new ArrayList();
						rlist=db.GetRno1(lst[6].ToString().Trim(),"","1",cont.Trim(),dt1.ToShortDateString(),dt2.ToShortDateString(),qn.Trim());
					
						DDLAPartNo.Items.Clear();
						DDLAPartNo.Items.Add("Part No");
						for(int i=0; i<plist.Count;i++)
						{
							DDLAPartNo.Items.Add(plist[i].ToString());
						}
						for(int i=0; i<zlist.Count;i++)
						{
							DDLAPartNo.Items.Add(zlist[i].ToString());
						}
						for(int i=0; i<alist.Count;i++)
						{
							DDLAPartNo.Items.Add(alist[i].ToString());
						}
						for(int i=0; i<rlist.Count;i++)
						{
							DDLAPartNo.Items.Add(rlist[i].ToString());
						}
					}
					else if(pqno.Visible ==true)
					{
						DGArchive.DataSource = CreateDataSource(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						
						ArrayList qdlist=new ArrayList();
						qdlist=db.Getquotedate2(lst[6].ToString().Trim(),"","1",cont.Trim(),qn.Trim());
						DDLACreatedate.Items.Clear();
						DDLACreatedate.Items.Add("Quote Date");
						for(int i=0; i<qdlist.Count;i++)
						{
							DDLACreatedate.Items.Add(qdlist[i].ToString());
						}
				
						ArrayList qnlist=new ArrayList();
						qnlist=db.GetQno2(lst[6].ToString().Trim(),"","1",cont.Trim(),qn.Trim(),pn.Trim());
						DDLAQNo.Items.Clear();
						DDLAQNo.Items.Add("Quote No");
						for(int i=0; i<qnlist.Count;i++)
						{
							DDLAQNo.Items.Add(qnlist[i].ToString());
						}

						ArrayList plist=new ArrayList();
						plist=db.GetPno2(lst[6].ToString().Trim(),"","1",cont.Trim(),qn.Trim(),pn.Trim());
						ArrayList zlist=new ArrayList();
						zlist=db.GetZno_2(lst[6].ToString().Trim(),"","1",cont.Trim(),qn.Trim(),pn.Trim());
						ArrayList alist=new ArrayList();
						alist=db.GetAno2(lst[6].ToString().Trim(),"","1",cont.Trim(),qn.Trim(),pn.Trim());
						ArrayList rlist=new ArrayList();
						rlist=db.GetRno2(lst[6].ToString().Trim(),"","1",cont.Trim(),qn.Trim(),pn.Trim());
					
						DDLAPartNo.Items.Clear();
						DDLAPartNo.Items.Add("Part No");
						for(int i=0; i<plist.Count;i++)
						{
							DDLAPartNo.Items.Add(plist[i].ToString());
						}
						for(int i=0; i<zlist.Count;i++)
						{
							DDLAPartNo.Items.Add(zlist[i].ToString());
						}
						for(int i=0; i<alist.Count;i++)
						{
							DDLAPartNo.Items.Add(alist[i].ToString());
						}
						for(int i=0; i<rlist.Count;i++)
						{
							DDLAPartNo.Items.Add(rlist[i].ToString());
						}
						
					}
					else if(ppno.Visible ==true)
					{
						DGArchive.DataSource = CreateDataSource(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						
						ArrayList qdlist=new ArrayList();
						qdlist=db.Getquotedate3(lst[6].ToString().Trim(),"","1",cont.Trim(),pn.Trim());
						DDLACreatedate.Items.Clear();
						DDLACreatedate.Items.Add("Quote Date");
						for(int i=0; i<qdlist.Count;i++)
						{
							DDLACreatedate.Items.Add(qdlist[i].ToString());
						}
				
						ArrayList qnlist=new ArrayList();
						qnlist=db.GetQno3(lst[6].ToString().Trim(),"","1",cont.Trim(),qn.Trim(),pn.Trim());
						DDLAQNo.Items.Clear();
						DDLAQNo.Items.Add("Quote No");
						for(int i=0; i<qnlist.Count;i++)
						{
							DDLAQNo.Items.Add(qnlist[i].ToString());
						}

						ArrayList plist=new ArrayList();
						plist=db.GetPno3(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(), qn.Trim(),pn.Trim());
						ArrayList zlist=new ArrayList();
						zlist=db.GetZno_3(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(), qn.Trim(),pn.Trim());
						ArrayList alist=new ArrayList();
						alist=db.GetAno3(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(), qn.Trim(),pn.Trim());
						ArrayList rlist=new ArrayList();
						rlist=db.GetRno3(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(), qn.Trim(),pn.Trim());
					
						DDLAPartNo.Items.Clear();
						DDLAPartNo.Items.Add("Part No");
						for(int i=0; i<plist.Count;i++)
						{
							DDLAPartNo.Items.Add(plist[i].ToString());
						}
						for(int i=0; i<zlist.Count;i++)
						{
							DDLAPartNo.Items.Add(zlist[i].ToString());
						}
						for(int i=0; i<alist.Count;i++)
						{
							DDLAPartNo.Items.Add(alist[i].ToString());
						}
						for(int i=0; i<rlist.Count;i++)
						{
							DDLAPartNo.Items.Add(rlist[i].ToString());
						}
					}
				DGArchive.DataBind();
				}
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}

		private void DDLACreatedate_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				err.Text="";
				if(DDLACreatedate.SelectedIndex >0)
				{
				
					DDLAQNo.SelectedIndex =0;
					DDLAPartNo.SelectedIndex=0;
					DGArchive.CurrentPageIndex = 0 ;
					err.Text="";
					string cont="";
					string cdate="";
					string pn="";
					string qn="";
					if(DDLAContacts.SelectedIndex > 0)
					{
						cont= DDLAContacts.SelectedItem.Text.Trim();
					}
					if(DDLACreatedate.SelectedIndex > 0)
					{
						cdate= DDLACreatedate.SelectedItem.Text.Trim();
					}
					if(DDLAQNo.SelectedIndex > 0)
					{
						qn= DDLAQNo.SelectedItem.Text.Trim();
					}
					if(pqno.Visible ==true)
					{
						if(TxtQno.Text.Trim() !="")
						{
							qn=TxtQno.Text.Trim();
						}
					}
					if(DDLAPartNo.SelectedIndex > 0)
					{
						pn= DDLAPartNo.SelectedItem.Text.Trim();
					}
					if(ppno.Visible ==true)
					{
						if(TxtPno.Text.Trim() !="")
						{
							pn=TxtPno.Text.Trim();
						}
					}
					ArrayList lst =new ArrayList();
					lst =(ArrayList)Session["User"];
					if(pdate.Visible ==true)
					{
						DateTime dt1=Convert.ToDateTime(TxtDate1.Text.ToString());
						DateTime dt2=Convert.ToDateTime(TxtDate2.Text.ToString());
						DGArchive.DataSource = CreateDataSource3(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						
						ArrayList qnlist=new ArrayList();
						qnlist=db.GetQno1(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(),cdate.Trim());
						DDLAQNo.Items.Clear();
						DDLAQNo.Items.Add("Quote No");
						for(int i=0; i<qnlist.Count;i++)
						{
							DDLAQNo.Items.Add(qnlist[i].ToString());
						}

						ArrayList plist=new ArrayList();
						plist=db.GetPno1(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(),cdate.Trim(),qn.Trim());
						ArrayList zlist=new ArrayList();
						zlist=db.GetZno_1(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(),cdate.Trim(),qn.Trim());
						ArrayList alist=new ArrayList();
						alist=db.GetAno1(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(),cdate.Trim(),qn.Trim());
						ArrayList rlist=new ArrayList();
						rlist=db.GetRno1(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(),cdate.Trim(),qn.Trim());
					
						DDLAPartNo.Items.Clear();
						DDLAPartNo.Items.Add("Part No");
						for(int i=0; i<plist.Count;i++)
						{
							DDLAPartNo.Items.Add(plist[i].ToString());
						}
						for(int i=0; i<zlist.Count;i++)
						{
							DDLAPartNo.Items.Add(zlist[i].ToString());
						}
						for(int i=0; i<alist.Count;i++)
						{
							DDLAPartNo.Items.Add(alist[i].ToString());
						}
						for(int i=0; i<rlist.Count;i++)
						{
							DDLAPartNo.Items.Add(rlist[i].ToString());
						}
					
					}
					else if(pqno.Visible ==true)
					{
						DGArchive.DataSource = CreateDataSource(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
					
						ArrayList qnlist=new ArrayList();
						qnlist=db.GetQno2(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(),qn.Trim());
						DDLAQNo.Items.Clear();
						DDLAQNo.Items.Add("Quote No");
						for(int i=0; i<qnlist.Count;i++)
						{
							DDLAQNo.Items.Add(qnlist[i].ToString());
						}

						ArrayList plist=new ArrayList();
						plist=db.GetPno2(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(),qn.Trim());
						ArrayList zlist=new ArrayList();
						zlist=db.GetZno_2(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(),qn.Trim());
						ArrayList alist=new ArrayList();
						alist=db.GetAno2(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(),qn.Trim());
						ArrayList rlist=new ArrayList();
						rlist=db.GetRno2(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(),qn.Trim());
					
						DDLAPartNo.Items.Clear();
						DDLAPartNo.Items.Add("Part No");
						for(int i=0; i<plist.Count;i++)
						{
							DDLAPartNo.Items.Add(plist[i].ToString());
						}
						for(int i=0; i<zlist.Count;i++)
						{
							DDLAPartNo.Items.Add(zlist[i].ToString());
						}
						for(int i=0; i<alist.Count;i++)
						{
							DDLAPartNo.Items.Add(alist[i].ToString());
						}
						for(int i=0; i<rlist.Count;i++)
						{
							DDLAPartNo.Items.Add(rlist[i].ToString());
						}
					
					}
					else if(ppno.Visible ==true)
					{
						DGArchive.DataSource = CreateDataSource(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
										
						ArrayList qnlist=new ArrayList();
						qnlist=db.GetQno3(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(),qn.Trim());
						DDLAQNo.Items.Clear();
						DDLAQNo.Items.Add("Quote No");
						for(int i=0; i<qnlist.Count;i++)
						{
							DDLAQNo.Items.Add(qnlist[i].ToString());
						}

						ArrayList plist=new ArrayList();
						plist=db.GetPno3(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						ArrayList zlist=new ArrayList();
						zlist=db.GetZno_3(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(), qn.Trim(),pn.Trim());
						ArrayList alist=new ArrayList();
						alist=db.GetAno3(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(), qn.Trim(),pn.Trim());
						ArrayList rlist=new ArrayList();
						rlist=db.GetRno3(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(), qn.Trim(),pn.Trim());
					
						DDLAPartNo.Items.Clear();
						DDLAPartNo.Items.Add("Part No");
						for(int i=0; i<plist.Count;i++)
						{
							DDLAPartNo.Items.Add(plist[i].ToString());
						}
						for(int i=0; i<zlist.Count;i++)
						{
							DDLAPartNo.Items.Add(zlist[i].ToString());
						}
						for(int i=0; i<alist.Count;i++)
						{
							DDLAPartNo.Items.Add(alist[i].ToString());
						}
						for(int i=0; i<rlist.Count;i++)
						{
							DDLAPartNo.Items.Add(rlist[i].ToString());
						}
					
					}
				DGArchive.DataBind();
				}
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}

		private void DDLAQNo_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				if(DDLAQNo.SelectedIndex >0)
				{
					DDLAPartNo.SelectedIndex=0;
					DGArchive.CurrentPageIndex = 0 ;
					err.Text="";
					string cont="";
					string cdate="";
					string pn="";
					string qn="";
					if(DDLAContacts.SelectedIndex > 0)
					{
						cont= DDLAContacts.SelectedItem.Text.Trim();
					}
					if(DDLACreatedate.SelectedIndex > 0)
					{
						cdate= DDLACreatedate.SelectedItem.Text.Trim();
					}
					if(DDLAQNo.SelectedIndex > 0)
					{
						qn= DDLAQNo.SelectedItem.Text.Trim();
					}
					if(pqno.Visible ==true)
					{
						if(TxtQno.Text.Trim() !="")
						{
							qn=TxtQno.Text.Trim();
						}
					}
					if(DDLAPartNo.SelectedIndex > 0)
					{
						pn= DDLAPartNo.SelectedItem.Text.Trim();
					}
					if(ppno.Visible ==true)
					{
						if(TxtPno.Text.Trim() !="")
						{
							pn=TxtPno.Text.Trim();
						}
					}
					ArrayList lst =new ArrayList();
					lst =(ArrayList)Session["User"];

					if(pdate.Visible ==true)
					{
						DateTime dt1=Convert.ToDateTime(TxtDate1.Text.ToString());
						DateTime dt2=Convert.ToDateTime(TxtDate2.Text.ToString());
						DGArchive.DataSource = CreateDataSource3(lst[6].ToString().Trim(),"","1",cont.Trim(),dt1.ToShortDateString(),dt2.ToShortDateString(),qn.Trim(),pn.Trim());
						
						ArrayList plist=new ArrayList();
						plist=db.GetPno1(lst[6].ToString().Trim(),"","1",cont.Trim(),dt1.ToShortDateString(),dt2.ToShortDateString(),qn.Trim());
						ArrayList zlist=new ArrayList();
						zlist=db.GetZno_1(lst[6].ToString().Trim(),"","1",cont.Trim(),dt1.ToShortDateString(),dt2.ToShortDateString(),qn.Trim());
						ArrayList alist=new ArrayList();
						alist=db.GetAno1(lst[6].ToString().Trim(),"","1",cont.Trim(),dt1.ToShortDateString(),dt2.ToShortDateString(),qn.Trim());
						ArrayList rlist=new ArrayList();
						rlist=db.GetRno1(lst[6].ToString().Trim(),"","1",cont.Trim(),dt1.ToShortDateString(),dt2.ToShortDateString(),qn.Trim());
					
						DDLAPartNo.Items.Clear();
						DDLAPartNo.Items.Add("Part No");
						for(int i=0; i<plist.Count;i++)
						{
							DDLAPartNo.Items.Add(plist[i].ToString());
						}
						for(int i=0; i<zlist.Count;i++)
						{
							DDLAPartNo.Items.Add(zlist[i].ToString());
						}
						for(int i=0; i<alist.Count;i++)
						{
							DDLAPartNo.Items.Add(alist[i].ToString());
						}
						for(int i=0; i<rlist.Count;i++)
						{
							DDLAPartNo.Items.Add(rlist[i].ToString());
						}
							
					}
					else if(pqno.Visible ==true)
					{
						DGArchive.DataSource = CreateDataSource(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						
						
						ArrayList plist=new ArrayList();
						plist=db.GetPno3(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						ArrayList zlist=new ArrayList();
						zlist=db.GetZno_3(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(), qn.Trim(),pn.Trim());
						ArrayList alist=new ArrayList();
						alist=db.GetAno3(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(), qn.Trim(),pn.Trim());
						ArrayList rlist=new ArrayList();
						rlist=db.GetRno3(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(), qn.Trim(),pn.Trim());
					
						DDLAPartNo.Items.Clear();
						DDLAPartNo.Items.Add("Part No");
						for(int i=0; i<plist.Count;i++)
						{
							DDLAPartNo.Items.Add(plist[i].ToString());
						}
						for(int i=0; i<zlist.Count;i++)
						{
							DDLAPartNo.Items.Add(zlist[i].ToString());
						}
						for(int i=0; i<alist.Count;i++)
						{
							DDLAPartNo.Items.Add(alist[i].ToString());
						}
						for(int i=0; i<rlist.Count;i++)
						{
							DDLAPartNo.Items.Add(rlist[i].ToString());
						}
							
					}
					else if(ppno.Visible ==true)
					{
						DGArchive.DataSource = CreateDataSource(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						
						
						ArrayList plist=new ArrayList();
						plist=db.GetPno3(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						ArrayList zlist=new ArrayList();
						zlist=db.GetZno_3(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(), qn.Trim(),pn.Trim());
						ArrayList alist=new ArrayList();
						alist=db.GetAno3(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(), qn.Trim(),pn.Trim());
						ArrayList rlist=new ArrayList();
						rlist=db.GetRno3(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(), qn.Trim(),pn.Trim());
					
						DDLAPartNo.Items.Clear();
						DDLAPartNo.Items.Add("Part No");
						for(int i=0; i<plist.Count;i++)
						{
							DDLAPartNo.Items.Add(plist[i].ToString());
						}
						for(int i=0; i<zlist.Count;i++)
						{
							DDLAPartNo.Items.Add(zlist[i].ToString());
						}
						for(int i=0; i<alist.Count;i++)
						{
							DDLAPartNo.Items.Add(alist[i].ToString());
						}
						for(int i=0; i<rlist.Count;i++)
						{
							DDLAPartNo.Items.Add(rlist[i].ToString());
						}
							
					}
				 DGArchive.DataBind();
				}
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}
		private void DDLAPartNo_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				err.Text="";
				if(DDLAPartNo.SelectedIndex >0)
				{
					DGArchive.CurrentPageIndex = 0 ;
					err.Text="";
					string cont="";
					string cdate="";
					string pn="";
					string qn="";
					if(DDLAContacts.SelectedIndex > 0)
					{
						cont= DDLAContacts.SelectedItem.Text.Trim();
					}
					if(DDLACreatedate.SelectedIndex > 0)
					{
						cdate= DDLACreatedate.SelectedItem.Text.Trim();
					}
					if(DDLAQNo.SelectedIndex > 0)
					{
						qn= DDLAQNo.SelectedItem.Text.Trim();
					}
					if(pqno.Visible ==true)
					{
						if(TxtQno.Text.Trim() !="")
						{
							qn=TxtQno.Text.Trim();
						}
					}
					if(DDLAPartNo.SelectedIndex > 0)
					{
						pn= DDLAPartNo.SelectedItem.Text.Trim();
					}
					if(ppno.Visible ==true)
					{
						if(TxtPno.Text.Trim() !="")
						{
							pn=TxtPno.Text.Trim();
						}
					}
					ArrayList lst =new ArrayList();
					lst =(ArrayList)Session["User"];
					if(pdate.Visible ==true)
					{
						DateTime dt1=Convert.ToDateTime(TxtDate1.Text.ToString());
						DateTime dt2=Convert.ToDateTime(TxtDate2.Text.ToString());
						DGArchive.DataSource = CreateDataSource3(lst[6].ToString().Trim(),"","1",cont.Trim(),dt1.ToShortDateString(),dt2.ToShortDateString(),qn.Trim(),pn.Trim());
						
					}
					else if(pqno.Visible ==true)
					{
						DGArchive.DataSource = CreateDataSource(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						
					}
					else if(ppno.Visible ==true)
					{
						DGArchive.DataSource = CreateDataSource(lst[6].ToString().Trim(),"","1",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
					}
			   	DGArchive.DataBind();
				}
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}

		private void DDLUContacts_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				err.Text="";
				if(DDLUContacts.SelectedIndex >0)
				{
					DDLUCreateDate.SelectedIndex=0;
					DDLUQuoteNO.SelectedIndex =0;
					DDLUPartNo.SelectedIndex=0;
					DGUnfinished.CurrentPageIndex = 0 ;
					err.Text="";
					string cont="";
					string cdate="";
					string pn="";
					string qn="";
					if(DDLUContacts.SelectedIndex > 0)
					{
						cont= DDLUContacts.SelectedItem.Text.Trim();
					}
					if(DDLUCreateDate.SelectedIndex > 0)
					{
						cdate= DDLUCreateDate.SelectedItem.Text.Trim();
					}
					if(DDLUQuoteNO.SelectedIndex > 0)
					{
						qn= DDLUQuoteNO.SelectedItem.Text.Trim();
					}
					if(pqno.Visible ==true)
					{
						if(TxtQno.Text.Trim() !="")
						{
							qn=TxtQno.Text.Trim();
						}
					}
					if(DDLUPartNo.SelectedIndex > 0)
					{
						pn= DDLUPartNo.SelectedItem.Text.Trim();
					}
					if(ppno.Visible ==true)
					{
						if(TxtPno.Text.Trim() !="")
						{
							pn=TxtPno.Text.Trim();
						}
					}
					ArrayList lst =new ArrayList();
					lst =(ArrayList)Session["User"];
					if(pdate.Visible ==true)
					{
						DateTime dt1=Convert.ToDateTime(TxtDate1.Text.ToString());
						DateTime dt2=Convert.ToDateTime(TxtDate2.Text.ToString());
						DGUnfinished.DataSource = CreateDataSource4(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),dt1.ToShortDateString(),dt2.ToShortDateString(),qn.Trim(),pn.Trim());
					
						ArrayList qdlist=new ArrayList();
						qdlist=db.Getquotedate1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),dt1.ToShortDateString(),dt2.ToShortDateString());
						DDLUCreateDate.Items.Clear();
						DDLUCreateDate.Items.Add("Quote Date");
						for(int i=0; i<qdlist.Count;i++)
						{
							DDLUCreateDate.Items.Add(qdlist[i].ToString());
						}
				
						ArrayList qnlist=new ArrayList();
						qnlist=db.GetQno1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),dt1.ToShortDateString(),dt2.ToShortDateString());
						DDLUQuoteNO.Items.Clear();
						DDLUQuoteNO.Items.Add("Quote No");
						for(int i=0; i<qnlist.Count;i++)
						{
							DDLUQuoteNO.Items.Add(qnlist[i].ToString());
						}

						ArrayList plist=new ArrayList();
						plist=db.GetPno1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),dt1.ToShortDateString(),dt2.ToShortDateString(),qn.Trim());
						ArrayList zlist=new ArrayList();
						zlist=db.GetZno_1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),dt1.ToShortDateString(),dt2.ToShortDateString(),qn.Trim());
						ArrayList alist=new ArrayList();
						alist=db.GetAno1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),dt1.ToShortDateString(),dt2.ToShortDateString(),qn.Trim());
						ArrayList rlist=new ArrayList();
						rlist=db.GetRno1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),dt1.ToShortDateString(),dt2.ToShortDateString(),qn.Trim());
					
						DDLUPartNo.Items.Clear();
						DDLUPartNo.Items.Add("Part No");
						for(int i=0; i<plist.Count;i++)
						{
							DDLUPartNo.Items.Add(plist[i].ToString());
						}
						for(int i=0; i<zlist.Count;i++)
						{
							DDLUPartNo.Items.Add(zlist[i].ToString());
						}
						for(int i=0; i<alist.Count;i++)
						{
							DDLUPartNo.Items.Add(alist[i].ToString());
						}
						for(int i=0; i<rlist.Count;i++)
						{
							DDLUPartNo.Items.Add(rlist[i].ToString());
						}
						
					}
					else if(pqno.Visible ==true)
					{
						DGUnfinished.DataSource = CreateDataSource1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						
						ArrayList qdlist=new ArrayList();
						qdlist=db.Getquotedate2(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),qn.Trim());
						DDLUCreateDate.Items.Clear();
						DDLUCreateDate.Items.Add("Quote Date");
						for(int i=0; i<qdlist.Count;i++)
						{
							DDLUCreateDate.Items.Add(qdlist[i].ToString());
						}
				
						ArrayList qnlist=new ArrayList();
						qnlist=db.GetQno2(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim());
						DDLUQuoteNO.Items.Clear();
						DDLUQuoteNO.Items.Add("Quote No");
						for(int i=0; i<qnlist.Count;i++)
						{
							DDLUQuoteNO.Items.Add(qnlist[i].ToString());
						}

						ArrayList plist=new ArrayList();
						plist=db.GetPno3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						ArrayList zlist=new ArrayList();
						zlist=db.GetZno_3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						ArrayList alist=new ArrayList();
						alist=db.GetAno3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						ArrayList rlist=new ArrayList();
						rlist=db.GetRno3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
					
						DDLUPartNo.Items.Clear();
						DDLUPartNo.Items.Add("Part No");
						for(int i=0; i<plist.Count;i++)
						{
							DDLUPartNo.Items.Add(plist[i].ToString());
						}
						for(int i=0; i<zlist.Count;i++)
						{
							DDLUPartNo.Items.Add(zlist[i].ToString());
						}
						for(int i=0; i<alist.Count;i++)
						{
							DDLUPartNo.Items.Add(alist[i].ToString());
						}
						for(int i=0; i<rlist.Count;i++)
						{
							DDLUPartNo.Items.Add(rlist[i].ToString());
						}
					
					}
					else if(ppno.Visible ==true)
					{
						DGUnfinished.DataSource = CreateDataSource1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						
						ArrayList qdlist=new ArrayList();
						qdlist=db.Getquotedate3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),pn.Trim());
						DDLUCreateDate.Items.Clear();
						DDLUCreateDate.Items.Add("Quote Date");
						for(int i=0; i<qdlist.Count;i++)
						{
							DDLUCreateDate.Items.Add(qdlist[i].ToString());
						}
				
						ArrayList qnlist=new ArrayList();
						qnlist=db.GetQno3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),pn.Trim());
						DDLUQuoteNO.Items.Clear();
						DDLUQuoteNO.Items.Add("Quote No");
						for(int i=0; i<qnlist.Count;i++)
						{
							DDLUQuoteNO.Items.Add(qnlist[i].ToString());
						}

						ArrayList plist=new ArrayList();
						plist=db.GetPno3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						ArrayList zlist=new ArrayList();
						zlist=db.GetZno_3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						ArrayList alist=new ArrayList();
						alist=db.GetAno3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						ArrayList rlist=new ArrayList();
						rlist=db.GetRno3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
					
						DDLUPartNo.Items.Clear();
						DDLUPartNo.Items.Add("Part No");
						for(int i=0; i<plist.Count;i++)
						{
							DDLUPartNo.Items.Add(plist[i].ToString());
						}
						for(int i=0; i<zlist.Count;i++)
						{
							DDLUPartNo.Items.Add(zlist[i].ToString());
						}
						for(int i=0; i<alist.Count;i++)
						{
							DDLUPartNo.Items.Add(alist[i].ToString());
						}
						for(int i=0; i<rlist.Count;i++)
						{
							DDLUPartNo.Items.Add(rlist[i].ToString());
						}
						
					}
				
					DGUnfinished.DataBind();
				}
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}

		private void DDLUCreateDate_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				err.Text="";
				if(DDLUCreateDate.SelectedIndex >0)
				{
					DDLUQuoteNO.SelectedIndex =0;
					DDLUPartNo.SelectedIndex=0;
					DGUnfinished.CurrentPageIndex = 0 ;
					err.Text="";
					string cont="";
					string cdate="";
					string pn="";
					string qn="";
					if(DDLUContacts.SelectedIndex > 0)
					{
						cont= DDLUContacts.SelectedItem.Text.Trim();
					}
					if(DDLUCreateDate.SelectedIndex > 0)
					{
						cdate= DDLUCreateDate.SelectedItem.Text.Trim();
					}
					if(DDLUQuoteNO.SelectedIndex > 0)
					{
						qn= DDLUQuoteNO.SelectedItem.Text.Trim();
					}
					if(pqno.Visible ==true)
					{
						if(TxtQno.Text.Trim() !="")
						{
							qn=TxtQno.Text.Trim();
						}
					}
					if(DDLUPartNo.SelectedIndex > 0)
					{
						pn= DDLUPartNo.SelectedItem.Text.Trim();
					}
					if(ppno.Visible ==true)
					{
						if(TxtPno.Text.Trim() !="")
						{
							pn=TxtPno.Text.Trim();
						}
					}
					ArrayList lst =new ArrayList();
					lst =(ArrayList)Session["User"];
					if(pdate.Visible ==true)
					{
						DateTime dt1=Convert.ToDateTime(TxtDate1.Text.ToString());
						DateTime dt2=Convert.ToDateTime(TxtDate2.Text.ToString());
						DGUnfinished.DataSource = CreateDataSource4(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
					
						
						ArrayList qnlist=new ArrayList();
						qnlist=db.GetQno1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),cdate.Trim());
						DDLUQuoteNO.Items.Clear();
						DDLUQuoteNO.Items.Add("Quote No");
						for(int i=0; i<qnlist.Count;i++)
						{
							DDLUQuoteNO.Items.Add(qnlist[i].ToString());
						}

						ArrayList plist=new ArrayList();
						plist=db.GetPno1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),cdate.Trim(),qn.Trim());
						ArrayList zlist=new ArrayList();
						zlist=db.GetZno_1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),cdate.Trim(),qn.Trim());
						ArrayList alist=new ArrayList();
						alist=db.GetAno1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),cdate.Trim(),qn.Trim());
						ArrayList rlist=new ArrayList();
						rlist=db.GetRno1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),cdate.Trim(),qn.Trim());
					
						DDLUPartNo.Items.Clear();
						DDLUPartNo.Items.Add("Part No");
						for(int i=0; i<plist.Count;i++)
						{
							DDLUPartNo.Items.Add(plist[i].ToString());
						}
						for(int i=0; i<zlist.Count;i++)
						{
							DDLUPartNo.Items.Add(zlist[i].ToString());
						}
						for(int i=0; i<alist.Count;i++)
						{
							DDLUPartNo.Items.Add(alist[i].ToString());
						}
						for(int i=0; i<rlist.Count;i++)
						{
							DDLUPartNo.Items.Add(rlist[i].ToString());
						}
						
					}
					else if(pqno.Visible ==true)
					{
						DGUnfinished.DataSource = CreateDataSource1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						
										
						ArrayList qnlist=new ArrayList();
						qnlist=db.GetQno2(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim() ,cdate.Trim(),qn.Trim());
						DDLUQuoteNO.Items.Clear();
						DDLUQuoteNO.Items.Add("Quote No");
						for(int i=0; i<qnlist.Count;i++)
						{
							DDLUQuoteNO.Items.Add(qnlist[i].ToString());
						}

						ArrayList plist=new ArrayList();
						plist=db.GetPno2(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim());
						ArrayList zlist=new ArrayList();
						zlist=db.GetZno_2(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim());
						ArrayList alist=new ArrayList();
						alist=db.GetAno2(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim());
						ArrayList rlist=new ArrayList();
						rlist=db.GetRno2(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim());
					
						DDLUPartNo.Items.Clear();
						DDLUPartNo.Items.Add("Part No");
						for(int i=0; i<plist.Count;i++)
						{
							DDLUPartNo.Items.Add(plist[i].ToString());
						}
						for(int i=0; i<zlist.Count;i++)
						{
							DDLUPartNo.Items.Add(zlist[i].ToString());
						}
						for(int i=0; i<alist.Count;i++)
						{
							DDLUPartNo.Items.Add(alist[i].ToString());
						}
						for(int i=0; i<rlist.Count;i++)
						{
							DDLUPartNo.Items.Add(rlist[i].ToString());
						}
						
					}
					else if(ppno.Visible ==true)
					{
						DGUnfinished.DataSource = CreateDataSource1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						
						
						ArrayList qnlist=new ArrayList();
						qnlist=db.GetQno3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),pn.Trim());
						DDLUQuoteNO.Items.Clear();
						DDLUQuoteNO.Items.Add("Quote No");
						for(int i=0; i<qnlist.Count;i++)
						{
							DDLUQuoteNO.Items.Add(qnlist[i].ToString());
						}

						ArrayList plist=new ArrayList();
						plist=db.GetPno3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						ArrayList zlist=new ArrayList();
						zlist=db.GetZno_3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						ArrayList alist=new ArrayList();
						alist=db.GetAno3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						ArrayList rlist=new ArrayList();
						rlist=db.GetRno3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
					
						DDLUPartNo.Items.Clear();
						DDLUPartNo.Items.Add("Part No");
						for(int i=0; i<plist.Count;i++)
						{
							DDLUPartNo.Items.Add(plist[i].ToString());
						}
						for(int i=0; i<zlist.Count;i++)
						{
							DDLUPartNo.Items.Add(zlist[i].ToString());
						}
						for(int i=0; i<alist.Count;i++)
						{
							DDLUPartNo.Items.Add(alist[i].ToString());
						}
						for(int i=0; i<rlist.Count;i++)
						{
							DDLUPartNo.Items.Add(rlist[i].ToString());
						}
					}
				
					DGUnfinished.DataBind();
				}
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}

		private void DDLUQuoteNO_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				err.Text="";
				if(DDLUQuoteNO.SelectedIndex >0)
				{
					DDLUPartNo.SelectedIndex=0;
					DGUnfinished.CurrentPageIndex = 0 ;
					err.Text="";
					string cont="";
					string cdate="";
					string pn="";
					string qn="";
					if(DDLUContacts.SelectedIndex > 0)
					{
						cont= DDLUContacts.SelectedItem.Text.Trim();
					}
					if(DDLUCreateDate.SelectedIndex > 0)
					{
						cdate= DDLUCreateDate.SelectedItem.Text.Trim();
					}
					if(DDLUQuoteNO.SelectedIndex > 0)
					{
						qn= DDLUQuoteNO.SelectedItem.Text.Trim();
					}
					if(pqno.Visible ==true)
					{
						if(TxtQno.Text.Trim() !="")
						{
							qn=TxtQno.Text.Trim();
						}
					}
					if(DDLUPartNo.SelectedIndex > 0)
					{
						pn= DDLUPartNo.SelectedItem.Text.Trim();
					}
					if(ppno.Visible ==true)
					{
						if(TxtPno.Text.Trim() !="")
						{
							pn=TxtPno.Text.Trim();
						}
					}
					ArrayList lst =new ArrayList();
					lst =(ArrayList)Session["User"];
					if(pdate.Visible ==true)
					{
						DateTime dt1=Convert.ToDateTime(TxtDate1.Text.ToString());
						DateTime dt2=Convert.ToDateTime(TxtDate2.Text.ToString());
						DGUnfinished.DataSource = CreateDataSource4(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),dt1.ToShortDateString(),dt2.ToShortDateString(),qn.Trim(),pn.Trim());
					
						ArrayList plist=new ArrayList();
						plist=db.GetPno1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),cdate.Trim(),qn.Trim());
						ArrayList zlist=new ArrayList();
						zlist=db.GetZno_1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),cdate.Trim(),qn.Trim());
						ArrayList alist=new ArrayList();
						alist=db.GetAno1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),cdate.Trim(),qn.Trim());
						ArrayList rlist=new ArrayList();
						rlist=db.GetRno1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),cdate.Trim(),qn.Trim());
					
						DDLUPartNo.Items.Clear();
						DDLUPartNo.Items.Add("Part No");
						for(int i=0; i<plist.Count;i++)
						{
							DDLUPartNo.Items.Add(plist[i].ToString());
						}
						for(int i=0; i<zlist.Count;i++)
						{
							DDLUPartNo.Items.Add(zlist[i].ToString());
						}
						for(int i=0; i<alist.Count;i++)
						{
							DDLUPartNo.Items.Add(alist[i].ToString());
						}
						for(int i=0; i<rlist.Count;i++)
						{
							DDLUPartNo.Items.Add(rlist[i].ToString());
						}
					}
					else if(pqno.Visible ==true)
					{
						DGUnfinished.DataSource = CreateDataSource1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						
						ArrayList plist=new ArrayList();
						plist=db.GetPno3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						ArrayList zlist=new ArrayList();
						zlist=db.GetZno_3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						ArrayList alist=new ArrayList();
						alist=db.GetAno3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						ArrayList rlist=new ArrayList();
						rlist=db.GetRno3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
					
						DDLUPartNo.Items.Clear();
						DDLUPartNo.Items.Add("Part No");
						for(int i=0; i<plist.Count;i++)
						{
							DDLUPartNo.Items.Add(plist[i].ToString());
						}
						for(int i=0; i<zlist.Count;i++)
						{
							DDLUPartNo.Items.Add(zlist[i].ToString());
						}
						for(int i=0; i<alist.Count;i++)
						{
							DDLUPartNo.Items.Add(alist[i].ToString());
						}
						for(int i=0; i<rlist.Count;i++)
						{
							DDLUPartNo.Items.Add(rlist[i].ToString());
						}
						
					}
					else if(ppno.Visible ==true)
					{
						DGUnfinished.DataSource = CreateDataSource1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						ArrayList plist=new ArrayList();
						plist=db.GetPno3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						ArrayList zlist=new ArrayList();
						zlist=db.GetZno_3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						ArrayList alist=new ArrayList();
						alist=db.GetAno3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						ArrayList rlist=new ArrayList();
						rlist=db.GetRno3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
					
						DDLUPartNo.Items.Clear();
						DDLUPartNo.Items.Add("Part No");
						for(int i=0; i<plist.Count;i++)
						{
							DDLUPartNo.Items.Add(plist[i].ToString());
						}
						for(int i=0; i<zlist.Count;i++)
						{
							DDLUPartNo.Items.Add(zlist[i].ToString());
						}
						for(int i=0; i<alist.Count;i++)
						{
							DDLUPartNo.Items.Add(alist[i].ToString());
						}
						for(int i=0; i<rlist.Count;i++)
						{
							DDLUPartNo.Items.Add(rlist[i].ToString());
						}
					
					}
				
					DGUnfinished.DataBind();
				}
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}

		private void DDLUPartNo_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				err.Text="";
				if(DDLUPartNo.SelectedIndex >0)
				{
					DGUnfinished.CurrentPageIndex = 0 ;
					err.Text="";
					string cont="";
					string cdate="";
					string pn="";
					string qn="";
					if(DDLUContacts.SelectedIndex > 0)
					{
						cont= DDLUContacts.SelectedItem.Text.Trim();
					}
					if(DDLUCreateDate.SelectedIndex > 0)
					{
						cdate= DDLUCreateDate.SelectedItem.Text.Trim();
					}
					if(DDLUQuoteNO.SelectedIndex > 0)
					{
						qn= DDLUQuoteNO.SelectedItem.Text.Trim();
					}
					if(pqno.Visible ==true)
					{
						if(TxtQno.Text.Trim() !="")
						{
							qn=TxtQno.Text.Trim();
						}
					}
					if(DDLUPartNo.SelectedIndex > 0)
					{
						pn= DDLUPartNo.SelectedItem.Text.Trim();
					}
					if(ppno.Visible ==true)
					{
						if(TxtPno.Text.Trim() !="")
						{
							pn=TxtPno.Text.Trim();
						}
					}
					ArrayList lst =new ArrayList();
					lst =(ArrayList)Session["User"];
					if(pdate.Visible ==true)
					{
						DateTime dt1=Convert.ToDateTime(TxtDate1.Text.ToString());
						DateTime dt2=Convert.ToDateTime(TxtDate2.Text.ToString());
						DGUnfinished.DataSource = CreateDataSource4(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),dt1.ToShortDateString(),dt2.ToShortDateString(),qn.Trim(),pn.Trim());
										
					}
					else if(pqno.Visible ==true)
					{
						DGUnfinished.DataSource = CreateDataSource1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						
					}
					else if(ppno.Visible ==true)
					{
						DGUnfinished.DataSource = CreateDataSource1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",cont.Trim(),cdate.Trim(),qn.Trim(),pn.Trim());
						
					}
				
					DGUnfinished.DataBind();
				}
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}

		private void RBLSearch_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (SortExpression == "") 
			{
				SortExpression = "QuoteDate DESC";
			} 
			if(RBLSearch.SelectedIndex == 0)
			{
				pdate.Visible =true;
				pqno.Visible =false;
				ppno.Visible =false;
			}
			else if(RBLSearch.SelectedIndex == 1)
			{
				pdate.Visible =false;
				pqno.Visible =true;
				ppno.Visible =false;
			}
			else if(RBLSearch.SelectedIndex == 2)
			{
				pdate.Visible =false;
				pqno.Visible =false;
				ppno.Visible =true;
			}
			else if(RBLSearch.SelectedIndex == 3)
			{
				pdate.Visible =false;
				pqno.Visible =false;
				ppno.Visible =true;
			}
			ArrayList lst =new ArrayList();
			lst =(ArrayList)Session["User"];
			if(RBLOptions.SelectedIndex ==0)
			{
				PArchive.Visible= true;
				PUnfinshed.Visible= false;
				DGArchive.DataSource=CreateDataSource(lst[6].ToString().Trim(),"","1","","","","");
				DGArchive.DataBind();
				ArrayList colist=new ArrayList();
				colist=db.GetContacts(lst[6].ToString().Trim(),"","1");
				DDLAContacts.Items.Clear();
				DDLAContacts.Items.Add("Contacts");
				for(int i=0; i<colist.Count;i++)
				{
					DDLAContacts.Items.Add(colist[i].ToString());
				}
				ArrayList qdlist=new ArrayList();
				qdlist=db.Getquotedate(lst[6].ToString().Trim(),"","1","");
				DDLACreatedate.Items.Clear();
				DDLACreatedate.Items.Add("Quote Date");
				for(int i=0; i<qdlist.Count;i++)
				{
					DDLACreatedate.Items.Add(qdlist[i].ToString());
				}
				
				ArrayList qnlist=new ArrayList();
				qnlist=db.GetQno(lst[6].ToString().Trim(),"","1","","");
				DDLAQNo.Items.Clear();
				DDLAQNo.Items.Add("Quote No");
				for(int i=0; i<qnlist.Count;i++)
				{
					DDLAQNo.Items.Add(qnlist[i].ToString());
				}

				ArrayList plist=new ArrayList();
				plist=db.GetPno1(lst[6].ToString().Trim(),"","1","","","","");
				ArrayList zlist=new ArrayList();
				zlist=db.GetZno_1(lst[6].ToString().Trim(),"","1","","","","");
				ArrayList alist=new ArrayList();
				alist=db.GetAno1(lst[6].ToString().Trim(),"","1","","","","");
				ArrayList rlist=new ArrayList();
				rlist=db.GetRno1(lst[6].ToString().Trim(),"","1","","","","");
					
				DDLAPartNo.Items.Clear();
				DDLAPartNo.Items.Add("Part No");
				for(int i=0; i<plist.Count;i++)
				{
					DDLAPartNo.Items.Add(plist[i].ToString());
				}
				for(int i=0; i<zlist.Count;i++)
				{
					DDLAPartNo.Items.Add(zlist[i].ToString());
				}
				for(int i=0; i<alist.Count;i++)
				{
					DDLAPartNo.Items.Add(alist[i].ToString());
				}
				for(int i=0; i<rlist.Count;i++)
				{
					DDLAPartNo.Items.Add(rlist[i].ToString());
				}
							
			}
			if(RBLOptions.SelectedIndex ==1)
			{
				PUnfinshed.Visible= true;
				PArchive.Visible= false;
				DGUnfinished.DataSource=CreateDataSource1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","","","","");
				DGUnfinished.DataBind();
						
				ArrayList colist=new ArrayList();
				colist=db.GetContacts(lst[6].ToString().Trim(),lbl.Text.Trim(),"0");
				DDLUContacts.Items.Clear();
				DDLUContacts.Items.Add("Contacts");
				for(int i=0; i<colist.Count;i++)
				{
					DDLUContacts.Items.Add(colist[i].ToString());
				}
				ArrayList qdlist=new ArrayList();
				qdlist=db.Getquotedate(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","");
				DDLUCreateDate.Items.Clear();
				DDLUCreateDate.Items.Add("Quote Date");
				for(int i=0; i<qdlist.Count;i++)
				{
					DDLUCreateDate.Items.Add(qdlist[i].ToString());
				}
				ArrayList qnlist=new ArrayList();
				qnlist=db.GetQno(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","","");
				DDLUQuoteNO.Items.Clear();
				DDLUQuoteNO.Items.Add("Quote No");
				for(int i=0; i<qnlist.Count;i++)
				{
					DDLUQuoteNO.Items.Add(qnlist[i].ToString());
				}
				ArrayList plist=new ArrayList();
				plist=db.GetPno1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","","","","");
				ArrayList zlist=new ArrayList();
				zlist=db.GetZno_1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","","","","");
				ArrayList alist=new ArrayList();
				alist=db.GetAno1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","","","","");
				ArrayList rlist=new ArrayList();
				rlist=db.GetRno1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","","","","");
					
				DDLUPartNo.Items.Clear();
				DDLUPartNo.Items.Add("Part No");
				for(int i=0; i<plist.Count;i++)
				{
					DDLUPartNo.Items.Add(plist[i].ToString());
				}
				for(int i=0; i<zlist.Count;i++)
				{
					DDLUPartNo.Items.Add(zlist[i].ToString());
				}
				for(int i=0; i<alist.Count;i++)
				{
					DDLUPartNo.Items.Add(alist[i].ToString());
				}
				for(int i=0; i<rlist.Count;i++)
				{
					DDLUPartNo.Items.Add(rlist[i].ToString());
				}
			}
		}

		private void BtnSearh_Click(object sender, System.EventArgs e)
		{
			//issue #206 start
			DGArchive.CurrentPageIndex=0;
			//issue #206 end
			if (SortExpression == "") 
			{
				SortExpression = "QuoteDate DESC";
			} 
			if(pdate.Visible ==true)
			{
				DateTime dt1=Convert.ToDateTime(TxtDate1.Text.ToString());
				DateTime dt2=Convert.ToDateTime(TxtDate2.Text.ToString());
				if(dt1 > dt2)
				{
					lblinfo.Text ="Please select date appropriately";
				}
				else
				{
					try
					{
						ArrayList lst =new ArrayList();
						lst =(ArrayList)Session["User"];
						if(RBLOptions.SelectedIndex ==0)
						{
							DGArchive.CurrentPageIndex = 0 ;
							PArchive.Visible= true;
							PUnfinshed.Visible= false;
							DGArchive.DataSource=CreateDataSource3(lst[6].ToString().Trim(),"","1","",dt1.ToShortDateString(),dt2.ToShortDateString(),"","");
							DGArchive.DataBind();
							ArrayList colist=new ArrayList();
							colist=db.GetContacts1(lst[6].ToString().Trim(),"","1",dt1.ToShortDateString(),dt2.ToShortDateString());
							DDLAContacts.Items.Clear();
							DDLAContacts.Items.Add("Contacts");
							for(int i=0; i<colist.Count;i++)
							{
								DDLAContacts.Items.Add(colist[i].ToString());
							}
							ArrayList qdlist=new ArrayList();
							qdlist=db.Getquotedate1(lst[6].ToString().Trim(),"","1","",dt1.ToShortDateString(),dt2.ToShortDateString());
							DDLACreatedate.Items.Clear();
							DDLACreatedate.Items.Add("Quote Date");
							for(int i=0; i<qdlist.Count;i++)
							{
								DDLACreatedate.Items.Add(qdlist[i].ToString());
							}
				
							ArrayList qnlist=new ArrayList();
							qnlist=db.GetQno1(lst[6].ToString().Trim(),"","1","",dt1.ToShortDateString(),dt2.ToShortDateString());
							DDLAQNo.Items.Clear();
							DDLAQNo.Items.Add("Quote No");
							for(int i=0; i<qnlist.Count;i++)
							{
								DDLAQNo.Items.Add(qnlist[i].ToString());
							}

							ArrayList plist=new ArrayList();
							plist=db.GetPno1(lst[6].ToString().Trim(),"","1","",dt1.ToShortDateString(),dt2.ToShortDateString(),"");
							ArrayList zlist=new ArrayList();
							zlist=db.GetZno_1(lst[6].ToString().Trim(),"","1","",dt1.ToShortDateString(),dt2.ToShortDateString(),"");
							ArrayList alist=new ArrayList();
							alist=db.GetAno1(lst[6].ToString().Trim(),"","1","",dt1.ToShortDateString(),dt2.ToShortDateString(),"");
							ArrayList rlist=new ArrayList();
							rlist=db.GetRno1(lst[6].ToString().Trim(),"","1","",dt1.ToShortDateString(),dt2.ToShortDateString(),"");
					
							DDLAPartNo.Items.Clear();
							DDLAPartNo.Items.Add("Part No");
							for(int i=0; i<plist.Count;i++)
							{
								DDLAPartNo.Items.Add(plist[i].ToString());
							}
							for(int i=0; i<zlist.Count;i++)
							{
								DDLAPartNo.Items.Add(zlist[i].ToString());
							}
							for(int i=0; i<alist.Count;i++)
							{
								DDLAPartNo.Items.Add(alist[i].ToString());
							}
							for(int i=0; i<rlist.Count;i++)
							{
								DDLAPartNo.Items.Add(rlist[i].ToString());
							}
							
						}
						if(RBLOptions.SelectedIndex ==1)
						{
							DGUnfinished.CurrentPageIndex = 0 ;
							PUnfinshed.Visible= true;
							PArchive.Visible= false;
							DGUnfinished.DataSource=CreateDataSource4(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","",dt1.ToShortDateString(),dt2.ToShortDateString(),"","");
							DGUnfinished.DataBind();
						
							ArrayList colist=new ArrayList();
							colist=db.GetContacts1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",dt1.ToShortDateString(),dt2.ToShortDateString());
							DDLUContacts.Items.Clear();
							DDLUContacts.Items.Add("Contacts");
							for(int i=0; i<colist.Count;i++)
							{
								DDLUContacts.Items.Add(colist[i].ToString());
							}
							ArrayList qdlist=new ArrayList();
							qdlist=db.Getquotedate1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","",dt1.ToShortDateString(),dt2.ToShortDateString());
							DDLUCreateDate.Items.Clear();
							DDLUCreateDate.Items.Add("Quote Date");
							for(int i=0; i<qdlist.Count;i++)
							{
								DDLUCreateDate.Items.Add(qdlist[i].ToString());
							}
							ArrayList qnlist=new ArrayList();
							qnlist=db.GetQno1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","",dt1.ToShortDateString(),dt2.ToShortDateString());
							DDLUQuoteNO.Items.Clear();
							DDLUQuoteNO.Items.Add("Quote No");
							for(int i=0; i<qnlist.Count;i++)
							{
								DDLUQuoteNO.Items.Add(qnlist[i].ToString());
							}
							ArrayList plist=new ArrayList();
							plist=db.GetPno1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","",dt1.ToShortDateString(),dt2.ToShortDateString(),"");
							ArrayList zlist=new ArrayList();
							zlist=db.GetZno_1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","",dt1.ToShortDateString(),dt2.ToShortDateString(),"");
							ArrayList alist=new ArrayList();
							alist=db.GetAno1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","",dt1.ToShortDateString(),dt2.ToShortDateString(),"");
							ArrayList rlist=new ArrayList();
							rlist=db.GetRno1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","",dt1.ToShortDateString(),dt2.ToShortDateString(),"");
							DDLUPartNo.Items.Clear();
							DDLUPartNo.Items.Add("Part No");
							for(int i=0; i<plist.Count;i++)
							{
								DDLUPartNo.Items.Add(plist[i].ToString());
							}
							for(int i=0; i<zlist.Count;i++)
							{
								DDLUPartNo.Items.Add(zlist[i].ToString());
							}
							for(int i=0; i<alist.Count;i++)
							{
								DDLUPartNo.Items.Add(alist[i].ToString());
							}
							for(int i=0; i<rlist.Count;i++)
							{
								DDLUPartNo.Items.Add(rlist[i].ToString());
							}							
						}
					}
					catch(Exception ex)
					{
						string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
						s=s.Replace("'"," ");
						err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
					}					
				}
			}
			else if(pqno.Visible ==true)
			{
				try
				{
					ArrayList lst =new ArrayList();
					lst =(ArrayList)Session["User"];
					if(TxtQno.Text !="")
					{
						if(RBLOptions.SelectedIndex ==0)
						{
							PArchive.Visible= true;
							PUnfinshed.Visible= false;
							DGArchive.DataSource=CreateDataSource(lst[6].ToString().Trim(),"","1","","",TxtQno.Text.Trim(),"");
							DGArchive.DataBind();
							ArrayList colist=new ArrayList();
							colist=db.GetContacts2(lst[6].ToString().Trim(),"","1",TxtQno.Text.Trim());
							DDLAContacts.Items.Clear();
							DDLAContacts.Items.Add("Contacts");
							for(int i=0; i<colist.Count;i++)
							{
								DDLAContacts.Items.Add(colist[i].ToString());
							}
							ArrayList qdlist=new ArrayList();
							qdlist=db.Getquotedate2(lst[6].ToString().Trim(),"","1","",TxtQno.Text.Trim());
							DDLACreatedate.Items.Clear();
							DDLACreatedate.Items.Add("Quote Date");
							for(int i=0; i<qdlist.Count;i++)
							{
								DDLACreatedate.Items.Add(qdlist[i].ToString());
							}
				
							ArrayList qnlist=new ArrayList();
							qnlist=db.GetQno2(lst[6].ToString().Trim(),"","1","","",TxtQno.Text.Trim());
							DDLAQNo.Items.Clear();
							DDLAQNo.Items.Add("Quote No");
							for(int i=0; i<qnlist.Count;i++)
							{
								DDLAQNo.Items.Add(qnlist[i].ToString());
							}

							ArrayList plist=new ArrayList();
							plist=db.GetPno2(lst[6].ToString().Trim(),"","1","","",TxtQno.Text.Trim());
							ArrayList zlist=new ArrayList();
							zlist=db.GetZno_2(lst[6].ToString().Trim(),"","1","","",TxtQno.Text.Trim());
							ArrayList alist=new ArrayList();
							alist=db.GetAno2(lst[6].ToString().Trim(),"","1","","",TxtQno.Text.Trim());
							ArrayList rlist=new ArrayList();
							rlist=db.GetRno2(lst[6].ToString().Trim(),"","1","","",TxtQno.Text.Trim());
					
							DDLAPartNo.Items.Clear();
							DDLAPartNo.Items.Add("Part No");
							for(int i=0; i<plist.Count;i++)
							{
								DDLAPartNo.Items.Add(plist[i].ToString());
							}
							for(int i=0; i<zlist.Count;i++)
							{
								DDLAPartNo.Items.Add(zlist[i].ToString());
							}
							for(int i=0; i<alist.Count;i++)
							{
								DDLAPartNo.Items.Add(alist[i].ToString());
							}
							for(int i=0; i<rlist.Count;i++)
							{
								DDLAPartNo.Items.Add(rlist[i].ToString());
							}
							
						}
						if(RBLOptions.SelectedIndex ==1)
						{
							PUnfinshed.Visible= true;
							PArchive.Visible= false;
							DGUnfinished.DataSource=CreateDataSource1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","","",TxtQno.Text.Trim(),"");
							DGUnfinished.DataBind();
						
							ArrayList colist=new ArrayList();
							colist=db.GetContacts2(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",TxtQno.Text.Trim());
							DDLUContacts.Items.Clear();
							DDLUContacts.Items.Add("Contacts");
							for(int i=0; i<colist.Count;i++)
							{
								DDLUContacts.Items.Add(colist[i].ToString());
							}
							ArrayList qdlist=new ArrayList();
							qdlist=db.Getquotedate2(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","",TxtQno.Text.Trim());
							DDLUCreateDate.Items.Clear();
							DDLUCreateDate.Items.Add("Quote Date");
							for(int i=0; i<qdlist.Count;i++)
							{
								DDLUCreateDate.Items.Add(qdlist[i].ToString());
							}
							ArrayList qnlist=new ArrayList();
							qnlist=db.GetQno2(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","","",TxtQno.Text.Trim());
							DDLUQuoteNO.Items.Clear();
							DDLUQuoteNO.Items.Add("Quote No");
							for(int i=0; i<qnlist.Count;i++)
							{
								DDLUQuoteNO.Items.Add(qnlist[i].ToString());
							}
							ArrayList plist=new ArrayList();
							plist=db.GetPno2(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","","",TxtQno.Text.Trim());
							ArrayList zlist=new ArrayList();
							zlist=db.GetZno_2(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","","",TxtQno.Text.Trim());
							ArrayList alist=new ArrayList();
							alist=db.GetAno2(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","","",TxtQno.Text.Trim());
							ArrayList rlist=new ArrayList();
							rlist=db.GetRno2(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","","",TxtQno.Text.Trim());
					
							DDLUPartNo.Items.Clear();
							DDLUPartNo.Items.Add("Part No");
							for(int i=0; i<plist.Count;i++)
							{
								DDLUPartNo.Items.Add(plist[i].ToString());
							}
							for(int i=0; i<zlist.Count;i++)
							{
								DDLUPartNo.Items.Add(zlist[i].ToString());
							}
							for(int i=0; i<alist.Count;i++)
							{
								DDLUPartNo.Items.Add(alist[i].ToString());
							}
							for(int i=0; i<rlist.Count;i++)
							{
								DDLUPartNo.Items.Add(rlist[i].ToString());
							}						
						}
					}
				}
				catch(Exception ex)
				{
					string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
					s.Replace("'"," ");
					err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				}
			}
			else if(ppno.Visible ==true)
			{
				if(RBLSearch.SelectedIndex == 2)
				{
					try
					{
						ArrayList lst =new ArrayList();
						lst =(ArrayList)Session["User"];
						if(TxtPno.Text !="")
						{
							if(RBLOptions.SelectedIndex ==0)
							{
								PArchive.Visible= true;
								PUnfinshed.Visible= false;
								DGArchive.DataSource=CreateDataSource(lst[6].ToString().Trim(),"","1","","","",TxtPno.Text.Trim());
								DGArchive.DataBind();
								ArrayList colist=new ArrayList();
								colist=db.GetContacts3(lst[6].ToString().Trim(),"","1",TxtPno.Text.Trim());
								DDLAContacts.Items.Clear();
								DDLAContacts.Items.Add("Contacts");
								for(int i=0; i<colist.Count;i++)
								{
									DDLAContacts.Items.Add(colist[i].ToString());
								}
								ArrayList qdlist=new ArrayList();
								qdlist=db.Getquotedate3(lst[6].ToString().Trim(),"","1","",TxtPno.Text.Trim());
								DDLACreatedate.Items.Clear();
								DDLACreatedate.Items.Add("Quote Date");
								for(int i=0; i<qdlist.Count;i++)
								{
									DDLACreatedate.Items.Add(qdlist[i].ToString());
								}
				
								ArrayList qnlist=new ArrayList();
								qnlist=db.GetQno3(lst[6].ToString().Trim(),"","1","","",TxtPno.Text.Trim());
								DDLAQNo.Items.Clear();
								DDLAQNo.Items.Add("Quote No");
								for(int i=0; i<qnlist.Count;i++)
								{
									DDLAQNo.Items.Add(qnlist[i].ToString());
								}

								ArrayList plist=new ArrayList();
								plist=db.GetPno3(lst[6].ToString().Trim(),"","1","","","",TxtPno.Text.Trim());
								ArrayList zlist=new ArrayList();
								zlist=db.GetZno_3(lst[6].ToString().Trim(),"","1","","","",TxtPno.Text.Trim());
								ArrayList alist=new ArrayList();
								alist=db.GetAno3(lst[6].ToString().Trim(),"","1","","","",TxtPno.Text.Trim());
								ArrayList rlist=new ArrayList();
								rlist=db.GetRno3(lst[6].ToString().Trim(),"","1","","","",TxtPno.Text.Trim());
					
								DDLAPartNo.Items.Clear();
								DDLAPartNo.Items.Add("Part No");
								for(int i=0; i<plist.Count;i++)
								{
									DDLAPartNo.Items.Add(plist[i].ToString());
								}
								for(int i=0; i<zlist.Count;i++)
								{
									DDLAPartNo.Items.Add(zlist[i].ToString());
								}
								for(int i=0; i<alist.Count;i++)
								{
									DDLAPartNo.Items.Add(alist[i].ToString());
								}
								for(int i=0; i<rlist.Count;i++)
								{
									DDLAPartNo.Items.Add(rlist[i].ToString());
								}
							
							}
							if(RBLOptions.SelectedIndex ==1)
							{
								PUnfinshed.Visible= true;
								PArchive.Visible= false;
								DGUnfinished.DataSource=CreateDataSource1(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","","","",TxtPno.Text.Trim());
								DGUnfinished.DataBind();
						
								ArrayList colist=new ArrayList();
								colist=db.GetContacts3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0",TxtPno.Text.Trim());
								DDLUContacts.Items.Clear();
								DDLUContacts.Items.Add("Contacts");
								for(int i=0; i<colist.Count;i++)
								{
									DDLUContacts.Items.Add(colist[i].ToString());
								}
								ArrayList qdlist=new ArrayList();
								qdlist=db.Getquotedate3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","",TxtPno.Text.Trim());
								DDLUCreateDate.Items.Clear();
								DDLUCreateDate.Items.Add("Quote Date");
								for(int i=0; i<qdlist.Count;i++)
								{
									DDLUCreateDate.Items.Add(qdlist[i].ToString());
								}
								ArrayList qnlist=new ArrayList();
								qnlist=db.GetQno3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","","",TxtPno.Text.Trim());
								DDLUQuoteNO.Items.Clear();
								DDLUQuoteNO.Items.Add("Quote No");
								for(int i=0; i<qnlist.Count;i++)
								{
									DDLUQuoteNO.Items.Add(qnlist[i].ToString());
								}
								ArrayList plist=new ArrayList();
								plist=db.GetPno3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","","","",TxtPno.Text.Trim());
								ArrayList zlist=new ArrayList();
								zlist=db.GetZno_3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","","","",TxtPno.Text.Trim());
								ArrayList alist=new ArrayList();
								alist=db.GetAno3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","","","",TxtPno.Text.Trim());
								ArrayList rlist=new ArrayList();
								rlist=db.GetRno3(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","","","",TxtPno.Text.Trim());
					
								DDLUPartNo.Items.Clear();
								DDLUPartNo.Items.Add("Part No");
								for(int i=0; i<plist.Count;i++)
								{
									DDLUPartNo.Items.Add(plist[i].ToString());
								}
								for(int i=0; i<zlist.Count;i++)
								{
									DDLUPartNo.Items.Add(zlist[i].ToString());
								}
								for(int i=0; i<alist.Count;i++)
								{
									DDLUPartNo.Items.Add(alist[i].ToString());
								}
								for(int i=0; i<rlist.Count;i++)
								{
									DDLUPartNo.Items.Add(rlist[i].ToString());
								}
							
							}
						}
					}	
					catch(Exception ex)
					{
						string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
						s=s.Replace("'"," ");
						err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
					}
				}
				else if(RBLSearch.SelectedIndex == 3)
				{
					try
					{
						ArrayList lst =new ArrayList();
						lst =(ArrayList)Session["User"];
						if(TxtPno.Text !="")
						{
							if(RBLOptions.SelectedIndex ==0)
							{
								PArchive.Visible= true;
								PUnfinshed.Visible= false;
								DGArchive.DataSource=CreateDataSource_Project(lst[6].ToString().Trim(),"","1","","","","",TxtPno.Text.Trim());
								DGArchive.DataBind();						
							}
							if(RBLOptions.SelectedIndex ==1)
							{
								PUnfinshed.Visible= true;
								PArchive.Visible= false;
								DGUnfinished.DataSource=CreateDataSource_ProjectNo(lst[6].ToString().Trim(),lbl.Text.Trim(),"0","","","","",TxtPno.Text.Trim());
								//issue #137 start
								DGUnfinished.CurrentPageIndex=0;
								//issue #137 end
								DGUnfinished.DataBind();								
							}
						}
					}	
					catch(Exception ex)
					{
						string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
						s=s.Replace("'"," ");
						err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
					}
				}
				
			}

		}
		public ICollection CreateDataSource3(string customerid,string usr,string finish,string contact,string qdate,string qdate2,string qno,string pno) 
		{
			try
			{
			 
				err.Text="";
				DataSet ds=new DataSet();
				DataSet ds1=new DataSet();
				DataSet ds2=new DataSet();
				DataSet ds3=new DataSet();
				ds =db.GetRifinedQuotes3(customerid.Trim(), usr.Trim(),finish.Trim(),contact.Trim(),qdate.Trim(),qdate2.Trim(),qno.Trim(),pno.Trim());
				ds1=db.GetRifinedAccesories3(customerid.Trim(), usr.Trim(),finish.Trim(),contact.Trim(),qdate.Trim(),qdate2.Trim(),qno.Trim(),pno.Trim());
				ds2=db.GetRifinedRepairKit3(customerid.Trim(), usr.Trim(),finish.Trim(),contact.Trim(),qdate.Trim(),qdate2.Trim(),qno.Trim(),pno.Trim());
				ds3=db.GetRifinedZ1013(customerid.Trim(), usr.Trim(),finish.Trim(),contact.Trim(),qdate.Trim(),qdate2.Trim(),qno.Trim(),pno.Trim());
				DataTable dt = new DataTable();
				DataRow dr;
				dt.Columns.Add(new DataColumn("Contact"));
				dt.Columns.Add(new DataColumn("QuoteDate",System.Type.GetType("System.DateTime")));
				dt.Columns.Add(new DataColumn("QuoteNo"));
				dt.Columns.Add(new DataColumn("QuoteNo1"));
				dt.Columns.Add(new DataColumn("QuoteNo2"));
				dt.Columns.Add(new DataColumn("PartNo1"));
				dt.Columns.Add(new DataColumn("PartNo"));
				dt.Columns.Add(new DataColumn("TotalPrice"));
				dt.Columns.Add(new DataColumn("Note"));
				dt.Columns.Add(new DataColumn("File"));
				dt.Columns.Add(new DataColumn("FileName"));
				dt.Columns.Add(new DataColumn("Dwg"));
				dt.Columns.Add(new DataColumn("Dwg1"));
				for (int i = 0; i < ds.Tables[0].Rows.Count; i++) 
				{
					dr = dt.NewRow();
					dr[0] = (string) ds.Tables[0].Rows[i]["Contact"];
					if(ds.Tables[0].Rows[i]["QuoteDate"].ToString() !="")
					{
						dr[1] = (string) ds.Tables[0].Rows[i]["QuoteDate"];
					}
					dr[2] = (string) ds.Tables[0].Rows[i]["QuoteNo"];
					dr[3] = (string) ds.Tables[0].Rows[i]["QuoteNo"].ToString().Substring(1);
					dr[4] = (string) ds.Tables[0].Rows[i]["QuoteNo"];
					dr[5] = (string) ds.Tables[0].Rows[i]["PartNo"];
					dr[6] = (string) ds.Tables[0].Rows[i]["PartNo"];
					dr[7] =  String.Format("{0:$###,###.00}",Convert.ToDecimal(ds.Tables[0].Rows[i]["TotalPrice"]));
					dr[8] = (string) ds.Tables[0].Rows[i]["Note"];
					if( ds.Tables[0].Rows[i]["UploadFile"].ToString().Trim() !="")
					{
						dr[9] = "View";
						dr[10] = ds.Tables[0].Rows[i]["UploadFile"].ToString().Trim();
					}
					else
					{
						dr[9] = "";
						dr[10] = "";
					}
					if( ds.Tables[0].Rows[i]["Dwg"].ToString().Trim() !="")
					{
						dr[11] = "View";
						dr[12] = ds.Tables[0].Rows[i]["Dwg"].ToString().Trim();
					}
					else
					{
						dr[11] = "";
						dr[12] = "";
					}
					dt.Rows.Add(dr);
				}
				for (int i = 0; i < ds3.Tables[0].Rows.Count; i++) 
				{
					dr = dt.NewRow();
					dr[0] = (string) ds3.Tables[0].Rows[i]["Contact"];
					if(ds3.Tables[0].Rows[i]["QuoteDate"].ToString() !="")
					{
						dr[1] = (string) ds3.Tables[0].Rows[i]["QuoteDate"];
					}
					dr[2] = (string) ds3.Tables[0].Rows[i]["QuoteNo"];
					dr[3] = (string) ds3.Tables[0].Rows[i]["QuoteNo"].ToString().Substring(1);
					dr[4] = (string) ds3.Tables[0].Rows[i]["QuoteNo"];
					dr[5] = (string) ds3.Tables[0].Rows[i]["Zno"];
					dr[6] = (string) ds3.Tables[0].Rows[i]["Zno"];
					dr[7] =  String.Format("{0:$###,###.00}",Convert.ToDecimal(ds3.Tables[0].Rows[i]["TotalPrice"]));
					dr[8] = (string) ds3.Tables[0].Rows[i]["Note"];
					dr[9] = "";
					dr[10] = "";
					dr[11] = "";
					dr[12] = "";
					dt.Rows.Add(dr);
				}
				for (int i = 0; i < ds1.Tables[0].Rows.Count; i++) 
				{
					dr = dt.NewRow();
					dr[0] = (string) ds1.Tables[0].Rows[i]["Contact"];
					if(ds1.Tables[0].Rows[i]["QuoteDate"].ToString() !="")
					{
						dr[1] = (string) ds1.Tables[0].Rows[i]["QuoteDate"];
					}
					dr[2] = (string) ds1.Tables[0].Rows[i]["QuoteNo"];
					dr[3] = (string) ds1.Tables[0].Rows[i]["QuoteNo"].ToString().Substring(1);
					dr[4] = (string) ds1.Tables[0].Rows[i]["QuoteNo"];
					dr[5] = (string) ds1.Tables[0].Rows[i]["PartNo"];
					dr[6] = (string) ds1.Tables[0].Rows[i]["PartNo"];
					dr[7] =  String.Format("{0:$###,###.00}",Convert.ToDecimal(ds1.Tables[0].Rows[i]["TotalPrice"]));
					dr[8] = (string) ds1.Tables[0].Rows[i]["Note"];
					dr[9] = "";
					dr[10] = "";
					dr[11] = "";
					dr[12] = "";
					dt.Rows.Add(dr);
				}
				for (int i = 0; i < ds2.Tables[0].Rows.Count; i++) 
				{
					dr = dt.NewRow();
					dr[0] = (string) ds2.Tables[0].Rows[i]["Contact"];
					if(ds2.Tables[0].Rows[i]["QuoteDate"].ToString() !="")
					{
						dr[1] = (string) ds2.Tables[0].Rows[i]["QuoteDate"];
					}
					dr[2] = (string) ds2.Tables[0].Rows[i]["QuoteNo"];
					dr[3] = (string) ds2.Tables[0].Rows[i]["QuoteNo"].ToString().Substring(1);
					dr[4] = (string) ds2.Tables[0].Rows[i]["QuoteNo"];
					dr[5] = (string) ds2.Tables[0].Rows[i]["PartNo"];
					dr[6] = (string) ds2.Tables[0].Rows[i]["PartNo"];
					dr[7] =  String.Format("{0:$###,###.00}",Convert.ToDecimal(ds2.Tables[0].Rows[i]["TotalPrice"]));
					dr[8] = (string) ds2.Tables[0].Rows[i]["Note"];
					dr[9] = "";
					dr[10] = "";
					dr[11] = "";
					dr[12] = "";
					dt.Rows.Add(dr);
				}
				DataView dv = new DataView(dt);
			
				dv.Sort = SortExpression;
				return dv;
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				return null;
			}	
		}
		public ICollection CreateDataSource4(string customerid,string usr,string finish,string contact,string qdate,string qdate2,string qno,string pno) 
		{
			try
			{
				err.Text="";
				DataSet ds=new DataSet();
				DataSet ds1=new DataSet();
				DataSet ds2=new DataSet();
				DataSet ds3=new DataSet();
				ds =db.GetRifinedQuotes4(customerid.Trim(), usr.Trim(),finish.Trim(),contact.Trim(),qdate.Trim(),qdate2.Trim(),qno.Trim(),pno.Trim());
				ds1=db.GetRifinedAccesories4(customerid.Trim(), usr.Trim(),finish.Trim(),contact.Trim(),qdate.Trim(),qdate2.Trim(),qno.Trim(),pno.Trim());
				ds2=db.GetRifinedRepairKit4(customerid.Trim(), usr.Trim(),finish.Trim(),contact.Trim(),qdate.Trim(),qdate2.Trim(),qno.Trim(),pno.Trim());
				ds3=db.GetRifinedZ1014(customerid.Trim(), usr.Trim(),finish.Trim(),contact.Trim(),qdate.Trim(),qdate2.Trim(),qno.Trim(),pno.Trim());
				DataTable dt = new DataTable();
				DataRow dr;
				dt.Columns.Add(new DataColumn("Contact"));
				dt.Columns.Add(new DataColumn("QuoteDate",System.Type.GetType("System.DateTime")));
				dt.Columns.Add(new DataColumn("QuoteNo"));
				dt.Columns.Add(new DataColumn("QuoteNo1"));
				dt.Columns.Add(new DataColumn("QuoteNo2"));
				dt.Columns.Add(new DataColumn("PartNo"));
				dt.Columns.Add(new DataColumn("Note"));
				dt.Columns.Add(new DataColumn("Specialrequest"));
				dt.Columns.Add(new DataColumn("FinishedDate"));
				dt.Columns.Add(new DataColumn("CowanQno"));
				dt.Columns.Add(new DataColumn("File"));
				dt.Columns.Add(new DataColumn("FileName"));
				dt.Columns.Add(new DataColumn("Dwg"));
				dt.Columns.Add(new DataColumn("Dwg1"));
				for (int i = 0; i < ds.Tables[0].Rows.Count; i++) 
				{
					dr = dt.NewRow();
					dr[0] = (string) ds.Tables[0].Rows[i]["Contact"];
					if(ds.Tables[0].Rows[i]["QuoteDate"].ToString() !="")
					{
						dr[1] = (string) ds.Tables[0].Rows[i]["QuoteDate"];
					}
					dr[2] = (string) ds.Tables[0].Rows[i]["QuoteNo"];
					dr[3] = (string) ds.Tables[0].Rows[i]["QuoteNo"].ToString().Substring(1);
					dr[4] = (string) ds.Tables[0].Rows[i]["QuoteNo"];
					dr[5] = (string) ds.Tables[0].Rows[i]["PartNo"];
					dr[6] = (string) ds.Tables[0].Rows[i]["Note"];
					dr[7] = (string) ds.Tables[0].Rows[i]["Finish"];
					dr[8] = (string) ds.Tables[0].Rows[i]["FinishedDate"];
					dr[9] = (string) ds.Tables[0].Rows[i]["CowanQno"];
					if( ds.Tables[0].Rows[i]["UploadFile"].ToString().Trim() !="")
					{
						dr[10] = "View";
						dr[11] = ds.Tables[0].Rows[i]["UploadFile"].ToString().Trim();
					}
					else
					{
						dr[10] = "";
						dr[11] = "";
					}
					if( ds.Tables[0].Rows[i]["Dwg"].ToString().Trim() !="")
					{
						dr[12] = "View";
						dr[13] = ds.Tables[0].Rows[i]["Dwg"].ToString().Trim();
					}
					else
					{
						dr[12] = "";
						dr[13] = "";
					}
					dt.Rows.Add(dr);
				}
				for (int i = 0; i < ds3.Tables[0].Rows.Count; i++) 
				{
					dr = dt.NewRow();
					dr[0] = (string) ds3.Tables[0].Rows[i]["Contact"];
					if(ds3.Tables[0].Rows[i]["QuoteDate"].ToString() !="")
					{
						dr[1] = (string) ds3.Tables[0].Rows[i]["QuoteDate"];
					}
					dr[2] = (string) ds3.Tables[0].Rows[i]["QuoteNo"];
					dr[3] = (string) ds3.Tables[0].Rows[i]["QuoteNo"].ToString().Substring(1);
					dr[4] = (string) ds3.Tables[0].Rows[i]["QuoteNo"];
					dr[5] = (string) ds3.Tables[0].Rows[i]["Zno"];
					dr[6] = (string) ds3.Tables[0].Rows[i]["Note"];
					dr[7] = "";
					dr[8] = (string) ds3.Tables[0].Rows[i]["FinishedDate"];
					dr[9] = (string) ds3.Tables[0].Rows[i]["CowanQno"];
					dr[10] = "";
					dr[11] = "";
					dr[12] = "";
					dr[13] = "";
					dt.Rows.Add(dr);
				}
				for (int i = 0; i < ds1.Tables[0].Rows.Count; i++) 
				{
					dr = dt.NewRow();
					dr[0] = (string) ds1.Tables[0].Rows[i]["Contact"];
					if(ds1.Tables[0].Rows[i]["QuoteDate"].ToString() !="")
					{
						dr[1] = (string) ds1.Tables[0].Rows[i]["QuoteDate"];
					}
					dr[2] = (string) ds1.Tables[0].Rows[i]["QuoteNo"];
					dr[3] = (string) ds1.Tables[0].Rows[i]["QuoteNo"].ToString().Substring(1);
					dr[4] = (string) ds1.Tables[0].Rows[i]["QuoteNo"];
					dr[5] = (string) ds1.Tables[0].Rows[i]["PartNo"];
					dr[6] = (string) ds1.Tables[0].Rows[i]["Note"];
					dr[7] = " ";
					dr[8] = (string) ds1.Tables[0].Rows[i]["FinishedDate"];
					dr[9] = (string) ds1.Tables[0].Rows[i]["CowanQno"];
					dr[10] = "";
					dr[11] = "";
					dr[12] = "";
					dr[13] = "";
					dt.Rows.Add(dr);
				}
				for (int i = 0; i < ds2.Tables[0].Rows.Count; i++) 
				{
					dr = dt.NewRow();
					dr[0] = (string) ds2.Tables[0].Rows[i]["Contact"];
					if(ds2.Tables[0].Rows[i]["QuoteDate"].ToString() !="")
					{
						dr[1] = (string) ds2.Tables[0].Rows[i]["QuoteDate"];
					}
					dr[2] = (string) ds2.Tables[0].Rows[i]["QuoteNo"];
					dr[3] = (string) ds2.Tables[0].Rows[i]["QuoteNo"].ToString().Substring(1);
					dr[4] = (string) ds2.Tables[0].Rows[i]["QuoteNo"];
					dr[5] = (string) ds2.Tables[0].Rows[i]["PartNo"];
					dr[6] = (string) ds2.Tables[0].Rows[i]["Note"];
					dr[7] = " ";
					dr[8] = (string) ds2.Tables[0].Rows[i]["FinishedDate"];
					dr[9] = (string) ds2.Tables[0].Rows[i]["CowanQno"];
					dr[10] = "";
					dr[11] = "";
					dr[12] = "";
					dr[13] = "";
					dt.Rows.Add(dr);
				}
				DataView dv = new DataView(dt);
			
				dv.Sort = SortExpression;
				return dv;
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s=s.Replace("'"," ");
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				return null;
			}	
		}

		private void BtnSend_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(TxtRLTMessage.Text.Trim() !="")
				{
				
					PLeedtime.Visible=false;	
					ArrayList lst1 =new ArrayList();
					lst1 =(ArrayList)Session["User"];
					MailMessage mail=new MailMessage();
					SmtpMail.SmtpServer ="k2smtpout.secureserver.net"; 
					mail.From= "admin@cowandynamics.com";
					mail.To =TxtRLTTo.Text.Trim();
					mail.Bcc="admin@cowandynamics.com";
					mail.Cc="jbehara@cowandynamics.com";
					mail.BodyFormat =MailFormat.Html;
					mail.Subject=   TxtRLTSubject.Text.Trim();
					mail.Priority =MailPriority.High;
					mail.Body =						
						"<hr color='#C0C000'>Bonjour, <br> <br> "+lst1[0].ToString()+" a soumis une demande pour le d�lai de livraison:"
						+"<TABLE id='Table1' borderColor='#80FFFF' cellSpacing='1' cellPadding='1' align='left' border='1'>"
						+"<TR><TD noWrap>Date de demande :</TD><TD noWrap>"+lblrfldate.Text.Trim()+"</TD></TR>"
						+"<TR><TD noWrap>No de demande :</TD><TD noWrap>"+lblrflqno.Text.Trim()+"</TD></TR>"
						+"<TR><TD noWrap>Part No :</TD><TD noWrap>"+lblrflpno.Text.Trim()+"</TD></TR>"
						+"<TR><TD noWrap>Lead Time Request :</TD><TD noWrap>"+TxtRLTMessage.Text.Trim()+"</TD></TR></TABLE>"
						+"<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>Clic sur le lien ci-dessous pour r�pondre dans la section .<a href='http://172.16.0.253:9000/'>\"Manage Client Quote\"</a>"
						+"<br><br><hr> Merci <br>I-Cylinder<br><hr color='#C0C000'><br>"								
						+"<hr color='#C0C000'>Hi, <br> "+lst1[0].ToString()+" has entered a new request for a quote:"
						+"<TABLE id='Table1' borderColor='#80FFFF' cellSpacing='1' cellPadding='1' align='left' border='1'>"
						+"<TR><TD noWrap>Quote Date :</TD><TD noWrap>"+lblrfldate.Text.Trim()+"</TD></TR>"
						+"<TR><TD noWrap>Quote No :</TD><TD noWrap>"+lblrflqno.Text.Trim()+"</TD></TR>"
						+"<TR><TD noWrap>Part No :</TD><TD noWrap>"+lblrflpno.Text.Trim()+"</TD></TR>"
						+"<TR><TD noWrap>Lead Time Request :</TD><TD noWrap>"+TxtRLTMessage.Text.Trim()+"</TD></TR></TABLE>"
						+"<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>Click on the link below to reply in the <a href='http://172.16.0.253:9000/'>\"Manage Client Quote\"</a>"
						+"<br><br><hr> Thanks <br>I-Cylinder<br><hr color='#C0C000'>";
					//issue #582 start
					mail.Body += csSignature.Get_Admin();
					//issue #582 end
					SmtpMail.Send(mail);
//					Message message = new Message();
//					message.From.Email = lst1[1].ToString();
//					message.To.Add( TxtRLTTo.Text.Trim() );
//					message.Cc.Add("rwenker@cowandynamics.com");
//					message.Bcc.Add("admin@cowandynamics.com");
//					message.Subject = TxtRLTSubject.Text.Trim();
//					message.Charset = System.Text.Encoding.GetEncoding("iso-8859-7");
//					message.BodyHtml ="<hr color='#C0C000'>Bonjour, <br> <br> "+lst1[0].ToString()+" a soumis une demande pour le d�lai de livraison:"
//						+"<TABLE id='Table1' borderColor='#80FFFF' cellSpacing='1' cellPadding='1' align='left' border='1'>"
//						+"<TR><TD noWrap>Date de demande :</TD><TD noWrap>"+lblrfldate.Text.Trim()+"</TD></TR>"
//						+"<TR><TD noWrap>No de demande :</TD><TD noWrap>"+lblrflqno.Text.Trim()+"</TD></TR>"
//						+"<TR><TD noWrap>Part No :</TD><TD noWrap>"+lblrflpno.Text.Trim()+"</TD></TR>"
//						+"<TR><TD noWrap>Lead Time Request :</TD><TD noWrap>"+TxtRLTMessage.Text.Trim()+"</TD></TR></TABLE>"
//						+"<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>Clic sur le lien ci-dessous pour r�pondre dans la section .<a href='http://172.16.0.253:9000/'>\"Manage Client Quote\"</a>"
//						+"<br><br><hr> Merci <br>I-Cylinder<br><hr color='#C0C000'><br>"								
//						+"<hr color='#C0C000'>Hi, <br> "+lst1[0].ToString()+" has entered a new request for a quote:"
//						+"<TABLE id='Table1' borderColor='#80FFFF' cellSpacing='1' cellPadding='1' align='left' border='1'>"
//						+"<TR><TD noWrap>Quote Date :</TD><TD noWrap>"+lblrfldate.Text.Trim()+"</TD></TR>"
//						+"<TR><TD noWrap>Quote No :</TD><TD noWrap>"+lblrflqno.Text.Trim()+"</TD></TR>"
//						+"<TR><TD noWrap>Part No :</TD><TD noWrap>"+lblrflpno.Text.Trim()+"</TD></TR>"
//						+"<TR><TD noWrap>Lead Time Request :</TD><TD noWrap>"+TxtRLTMessage.Text.Trim()+"</TD></TR></TABLE>"
//						+"<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>Click on the link below to reply in the <a href='http://172.16.0.253:9000/'>\"Manage Client Quote\"</a>"
//						+"<br><br><hr> Thanks <br>I-Cylinder<br><hr color='#C0C000'>";
//					Smtp.Send( message, "smtpout.secureserver.net", 80, GetDomain( message.From.Email ),SmtpAuthentication.Login,"admin@cowandynamics.com","hockey13" );
//					//										Smtp.Send( message, "smtp10.bellnet.ca", 25, GetDomain( message.From.Email ) );
					if(Page.IsValid)
					{
						string slno ="";
						slno=db.SelectCustomerQno( lblrflqno.Text.Trim());
						string res=db.UpdateDeliveryLeadTime(lblrflqno.Text.Trim(),slno.Trim());
						if(res.Trim() =="1")
						{
							Lblmail.Text="Your request has been sent";
							TxtRLTMessage.Text="";
						}

					}
					
				}
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}
		private string GetDomain( string email )
		{
			int index = email.IndexOf( '@' );
			return email.Substring( index + 1 );
		}
	}
}
