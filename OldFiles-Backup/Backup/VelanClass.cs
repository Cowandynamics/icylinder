using System;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for VelanClass.
	/// </summary>
	public class VelanClass
	{
		public VelanClass()
		{
			//issue #137 start
			eto="";
			etc="";
			bto="";
			rto="";
			rtc="";
			canisterno="";
			canisterboresize="";
			springrate="";
			cylinderboreno="";
			cylinderboresize="";
			tandem="";
			cylinderrodsize="";
			stroke="";
			failmode="";
			cylinderseries="";
			cylinderborecode="";
			cylinderborevalue="";
			cylinderrodcode="";
			airpressure="";
			//the only zero
			packingfriction="0";
			price="";
			btc="0";
			units="imperial";
			sftyfactor="0";
			//issue #137 end
		}
		private string slno; 
		private string qty; 
		private string style; 
		private string stroke;
		private string minair; 
		private string bto;
		private string rto; 
		private string eto; 
		private string btc;
		private string rtc; 
		private string etc; 
		private string sftyfactor;
		private string seals;
		private string paint;
		private string mount;
		private string pcp;
		private string limitswitch;
		private string ratings;
		private string mo;
		private string ct;
		private string velant;
		private string notes;
		private string pneumatichydraulic;
		private string bore;
		private string barrel;
		private string rodwiper;
		private string thrust;
		private string prod;
		private string rodend;
		private string rodboot;
		private string liftinglug;
		//isuue 137 start
		private string canisterno;
		private string canisterboresize;
		private string springrate;
		private string cylinderboreno;
		private string cylinderboresize;
		private string cylinderborevalue;
		private string cylinderborecode;
		private string cylinderrodsize;
		private string cylinderrodcode;
		private string tandem;
		private string cylinderseries;
		private string failmode;
		private string preload;
		private string airpressure;
		private string packingfriction;
		private string etcvalvetrust;
		private string price;
		private string rod_diamtr;
		private string btoreq;
		private string units;
		//issue #242 start
		private string companyid;
		public string CompanyID
		{
			get
			{
				return companyid;
			}
			set
			{
				companyid = value;
			}
		}
		//issue #242 end
		public string Units
		{
			get
			{
				return units;
			}
			set
			{
				units = value;
			}
		}
		public string BTOReq
		{
			get
			{
				return btoreq;
			}
			set
			{
				btoreq = value;
			}
		}
		public string Rod_Diamtr
		{
			get
			{
				return rod_diamtr;
			}
			set
			{
				rod_diamtr = value;
			}
		}
		public string CanisterNo
		{
			get
			{
				return canisterno;
			}
			set
			{
				canisterno = value;
			}
		}
		public string CanisterBoreSize
		{
			get
			{
				return canisterboresize;
			}
			set
			{
				canisterboresize = value;
			}
		}
		public string SpringRate
		{
			get
			{
				return springrate;
			}
			set
			{
				springrate = value;
			}
		}
		public string CylinderBoreNo
		{
			get
			{
				return cylinderboreno;
			}
			set
			{
				cylinderboreno = value;
			}
		}
		public string CylinderBoreSize
		{
			get
			{
				return cylinderboresize;
			}
			set
			{
				cylinderboresize = value;
			}
		}
		public string CylinderBoreValue
		{
			get
			{
				return cylinderborevalue;
			}
			set
			{
				cylinderborevalue = value;
			}
		}
		public string CylinderBoreCode
		{
			get
			{
				return cylinderborecode;
			}
			set
			{
				cylinderborecode = value;
			}
		}
		public string CylinderRodSize
		{
			get
			{
				return cylinderrodsize;
			}
			set
			{
				cylinderrodsize = value;
			}
		}
		public string CylinderRodCode
		{
			get
			{
				return cylinderrodcode;
			}
			set
			{
				cylinderrodcode = value;
			}
		}
		public string Tandem
		{
			get
			{
				return tandem;
			}
			set
			{
				tandem = value;
			}
		}
		public string CylinderSeries
		{
			get
			{
				return cylinderseries;
			}
			set
			{
				cylinderseries = value;
			}
		}
		public string FailMode
		{
			get
			{
				return failmode;
			}
			set
			{
				failmode = value;
			}
		}
		public string Preload
		{
			get
			{
				return preload;
			}
			set
			{
				preload = value;
			}
		}
		public string AirPressure
		{
			get
			{
				return airpressure;
			}
			set
			{
				airpressure = value;
			}
		}
		public string PackingFriction
		{
			get
			{
				return packingfriction;
			}
			set
			{
				packingfriction = value;
			}
		}
		public string EtcValveTrust
		{
			get
			{
				return etcvalvetrust;
			}
			set
			{
				etcvalvetrust = value;
			}
		}
		public string Price
		{
			get
			{
				return price;
			}
			set
			{
				price = value;
			}
		}
		
		//issue 137 end

		public string Qty
		{
			get
			{
				return qty;
			}
			set
			{
				qty = value;
			}
		}
		public string Bore
		{
			get
			{
				return bore;
			}
			set
			{
				bore = value;
			}
		}
		public string Style
		{
			get
			{
				return style;
			}
			set
			{
				style = value;
			}
		}
		public string Stroke
		{
			get
			{
				return stroke;
			}
			set
			{
				stroke = value;
			}
		}
		public string MinAirSupply
		{
			get
			{
				return minair;
			}
			set
			{
				minair = value;
			}
		}
		public string BTO
		{
			get
			{
				return bto;
			}
			set
			{
				bto = value;
			}
		}
		public string RTO
		{
			get
			{
				return rto;
			}
			set
			{
				rto = value;
			}
		}
		public string ETO
		{
			get
			{
				return eto;
			}
			set
			{
				eto = value;
			}
		}
		public string BTC
		{
			get
			{
				return btc;
			}
			set
			{
				btc = value;
			}
		}
		public string RTC
		{
			get
			{
				return rtc;
			}
			set
			{
				rtc = value;
			}
		}
		public string ETC
		{
			get
			{
				return etc;
			}
			set
			{
				etc = value;
			}
		}
		public string SaftyFactor
		{
			get
			{
				return sftyfactor;
			}
			set
			{
				sftyfactor = value;
			}
		}
		public string Seals
		{
			get
			{
				return seals;
			}
			set
			{
				seals = value;
			}
		}
		public string Paint
		{
			get
			{
				return paint;
			}
			set
			{
				paint = value;
			}
		}

		public string Mount
		{
			get
			{
				return mount;
			}
			set
			{
				mount = value;
			}
		}
		public string PneumaticP
		{
			get
			{
				return pcp;
			}
			set
			{
				pcp = value;
			}
		}
		public string LimitSwitch
		{
			get
			{
				return limitswitch;
			}
			set
			{
				limitswitch = value;
			}
		}
		public string Ratings
		{
			get
			{
				return ratings;
			}
			set
			{
				ratings = value;
			}
		}
		public string ManualOverride
		{
			get
			{
				return mo;
			}
			set
			{
				mo = value;
			}
		}
		public string Slno
		{
			get
			{
				return slno;
			}
			set
			{
				slno = value;
			}
		}
		public string ClosingTime
		{
			get
			{
				return ct;
			}
			set
			{
				ct = value;
			}
		}
		public string VelanTag
		{
			get
			{
				return velant;
			}
			set
			{
				velant = value;
			}
		}
		public string Notes
		{
			get
			{
				return notes;
			}
			set
			{
				notes = value;
			}
		}
		public string PneumaticHydraulic
		{
			get
			{
				return pneumatichydraulic;
			}
			set
			{
				pneumatichydraulic = value;
			}
		}	
		public string Barrel
		{
			get
			{
				return barrel;
			}
			set
			{
				barrel = value;
			}
		}
		public string RodWiper
		{
			get
			{
				return rodwiper;
			}
			set
			{
				rodwiper = value;
			}
		}
		public string Thrust
		{
			get
			{
				return thrust;
			}
			set
			{
				thrust = value;
			}
		}
		public string PistonRod
		{
			get
			{
				return prod;
			}
			set
			{
				prod = value;
			}
		}
		public string RodEnd
		{
			get
			{
				return rodend;
			}
			set
			{
				rodend = value;
			}
		}
		public string RodBoot
		{
			get
			{
				return rodboot;
			}
			set
			{
				rodboot = value;
			}
		}
		public string LiftingLugs
		{
			get
			{
				return liftinglug;
			}
			set
			{
				liftinglug = value;
			}
		}
	}
}
