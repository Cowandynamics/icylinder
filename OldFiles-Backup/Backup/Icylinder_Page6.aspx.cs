using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
namespace iCylinderV1
{
	/// <summary>
	/// Summary description for Icylinder_Page6.
	/// </summary>
	public class Icylinder_Page6 : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.LinkButton LBAdvanced;
		protected System.Web.UI.WebControls.LinkButton LBBack;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.Label LBLSeries;
		protected System.Web.UI.WebControls.Label LblPartNo;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.TextBox TxtSpecialReq;
		protected System.Web.UI.WebControls.TextBox TxtQty;
		protected System.Web.UI.WebControls.Label lblQty;
		protected System.Web.UI.WebControls.DropDownList DDLPriority;
		protected System.Web.UI.WebControls.Label Label58;
		protected System.Web.UI.WebControls.Label Label57;
		protected System.Web.UI.WebControls.Panel PSend;
		protected System.Web.UI.WebControls.Label Label2;
		coder cd1=new coder();
		protected System.Web.UI.WebControls.RadioButtonList RBLQty;
		protected System.Web.UI.WebControls.LinkButton LBGenerate;
		protected System.Web.UI.WebControls.LinkButton LBSend;
		protected System.Web.UI.WebControls.Label LblInfo;
		protected System.Web.UI.WebControls.Label lblD;
		protected System.Web.UI.WebControls.Label lblstatus;
		protected System.Web.UI.WebControls.Label lblstroke;
		protected System.Web.UI.WebControls.Label lblqno;
		protected System.Web.UI.WebControls.Label lbltotal;
		protected System.Web.UI.WebControls.Label lblmgrp;
		protected System.Web.UI.WebControls.Label lblbor;
		protected System.Web.UI.WebControls.Label lbl;
		protected System.Web.UI.WebControls.Label lbltable;
		protected System.Web.UI.WebControls.Label lblgenerateprice;
		protected System.Web.UI.HtmlControls.HtmlInputFile UploadDwg;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.LinkButton LbPreview;
		protected System.Web.UI.WebControls.Label lblfilename;
		protected System.Web.UI.WebControls.CheckBox cbupload;
		protected System.Web.UI.WebControls.Label lblfile;
		DBClass db=new DBClass();

		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["User"] !=null)
			{
				LblInfo.Text="";
				lblgenerateprice.Text="";
				if(! IsPostBack)
				{
					if(Session["Coder"] != null)
					{
						cd1= (coder)Session["Coder"];
						if(cd1.Series.Trim() !="PBS")
						{
							cd1.Application_Opt.Clear();
							cd1.Popular_Opt.Clear();
							if(cd1.SecRodDiameter !=null )
							{
								if(cd1.SecRodDiameter.Trim() !="")
								{
									cd1.Application_Opt.Add(cd1.SecRodDiameter);
								}
							}
							if(cd1.GlandDrain !=null )
							{
								if(cd1.GlandDrain.Trim() !="")
								{
									cd1.Application_Opt.Add(cd1.GlandDrain);	
								}
							}
							if(cd1.Bushing_Conf !=null )
							{
								if(cd1.Bushing_Conf.Trim() !="")
								{
									cd1.Application_Opt.Add(cd1.Bushing_Conf);	
								}
							}
							if(cd1.MetalScrapper !=null)
							{
								if(cd1.MetalScrapper.Trim() !="")
								{
									cd1.Application_Opt.Add(cd1.MetalScrapper);	 
								}
							}
							if(cd1.PistonSeal_Conf !=null )
							{
								if(cd1.PistonSeal_Conf.Trim() !="")
								{
									if(cd1.PistonSeal_Conf.Trim() =="P1")
									{
										cd1.Application_Opt.Add(cd1.PistonSeal_Conf);
									}
								}
							}
							if(cd1.MagnetRing !=null )
							{
								if(cd1.MagnetRing.Trim() !="")
								{
									cd1.Application_Opt.Add(cd1.MagnetRing);	
								}
							}
							if(cd1.PistonSeal_Conf !=null )
							{
								if(cd1.PistonSeal_Conf.Trim() !="")
								{
									if(cd1.PistonSeal_Conf.Trim() !="P1")
									{
										cd1.Application_Opt.Add(cd1.PistonSeal_Conf);
									}
								}
							}
							if(cd1.SecRodEnd !=null)
							{
								if( cd1.SecRodEnd.Trim() !="")
								{
									cd1.Application_Opt.Add(cd1.SecRodEnd);	 
								}
							}
							if(cd1.RodSeal_Conf !=null )
							{
								if(cd1.RodSeal_Conf.Trim() !="")
								{
									cd1.Application_Opt.Add(cd1.RodSeal_Conf);	 
								}
							}
							//issue #90 start
							if(cd1.TieRodeCentSupp !=null )
							{
								if(cd1.TieRodeCentSupp.Trim() !="")
								{
									cd1.Application_Opt.Add(cd1.TieRodeCentSupp); 
								}
							}
							//issue #90 end
							if(cd1.Coating !=null )
							{
								if(cd1.Coating.Trim() !="")
								{
									if(cd1.Coating.Substring(0,1)=="W")
									{
										cd1.Popular_Opt.Add(cd1.Coating); 
									}
						 
								}
							}
							if(cd1.ThreadEx !=null )
							{
								if(cd1.ThreadEx.Trim() !="")
								{
									cd1.Popular_Opt.Add(cd1.ThreadEx);	 
								}
							}
							if(cd1.SecRodThreadx !=null)
							{
								if( cd1.SecRodThreadx.Trim() !="")
								{
									cd1.Popular_Opt.Add(cd1.SecRodThreadx);	 
								}
							}
							if(cd1.BBC !=null)
							{
								if(cd1.BBC.Trim() !="")
								{
									cd1.Popular_Opt.Add(cd1.BBC);	 
								}
							}
							if(cd1.BBH !=null)
							{
								if(cd1.BBH.Trim() !="")
								{
									cd1.Popular_Opt.Add(cd1.BBH);	 
								}
							}
							if(cd1.TandemDuplex !=null )
							{
								if(cd1.TandemDuplex.Trim() !=""  )
								{
									if(cd1.TandemDuplex.Substring(0,1)=="B")
									{
										cd1.Popular_Opt.Add(cd1.TandemDuplex);	 
									}
								}
							}
							if(cd1.Coating !=null)
							{
								if(cd1.Coating.Trim() !="")
								{
									if(cd1.Coating.Substring(0,1)!="W")
									{
										cd1.Popular_Opt.Add(cd1.Coating); 
									}
						 
								}
							}
							if(cd1.TandemDuplex !=null)
							{
								if(cd1.TandemDuplex.Trim() !=""  )
								{
									if(cd1.TandemDuplex.Substring(0,1)=="D")
									{
										cd1.Popular_Opt.Add(cd1.TandemDuplex);	 
									}
								}
							}
							if(cd1.StopTube !=null )
							{
								if(cd1.StopTube.Trim() !="")
								{
									if(cd1.StopTube.Substring(0,1)=="D")
									{
										cd1.Popular_Opt.Add(cd1.StopTube);	 
									}
								}
							}
							if(cd1.CarbonFibBarrel !=null )
							{
								if(cd1.CarbonFibBarrel.Trim() !="")
								{
									if(cd1.CarbonFibBarrel.Substring(0,2)=="M1")
									{
										cd1.Popular_Opt.Add(cd1.CarbonFibBarrel);	 
									}
								}
							}
							if(cd1.SSTieRod !=null )
							{
								if(cd1.SSTieRod.Trim() !="")
								{
									cd1.Popular_Opt.Add(cd1.SSTieRod);	 
								}
							}
							if(cd1.SSPistionRod !=null)
							{
								if(cd1.SSPistionRod.Trim() !="")
								{
									cd1.Popular_Opt.Add(cd1.SSPistionRod);	 
								}
							}
							if(cd1.CarbonFibBarrel !=null )
							{
								if(cd1.CarbonFibBarrel.Trim() !="")
								{
									if(cd1.CarbonFibBarrel.Substring(0,2)=="M6")
									{
										cd1.Popular_Opt.Add(cd1.CarbonFibBarrel);	 
									}
								}	
							}
							if(cd1.AirBleed !=null )
							{
								if(cd1.AirBleed.Trim() !="")
								{
									cd1.Popular_Opt.Add(cd1.AirBleed);	 
								}
							}
							if(cd1.PortSize !=null)
							{
								if(cd1.PortSize.Trim() !="")
								{
									cd1.Popular_Opt.Add(cd1.PortSize);	 
								}
							}
							if(cd1.StopTube !=null )
							{
								if(cd1.StopTube.Trim() !="")
								{
									if(cd1.StopTube.Substring(0,1)=="S")
									{
										cd1.Popular_Opt.Add(cd1.StopTube);	 
									}
								}
							}
							if(cd1.TandemDuplex !=null )
							{
								if(cd1.TandemDuplex.Trim() !=""  )
								{
									if(cd1.TandemDuplex.Substring(0,1)=="T")
									{
										cd1.Popular_Opt.Add(cd1.TandemDuplex);	 
									}
								}
							}
							if(cd1.RodEx !=null )
							{
								if(cd1.RodEx.Trim() !="")
								{
									cd1.Popular_Opt.Add(cd1.RodEx);	 
								}
							}
							if(cd1.SecRodEx !=null )
							{
								if(cd1.SecRodEx.Trim() !="")
								{
									cd1.Popular_Opt.Add(cd1.SecRodEx);	 
								}
							}
							if(cd1.Trunnions !=null)
							{
								if(cd1.Trunnions.Trim() !="")
								{
									cd1.Popular_Opt.Add(cd1.Trunnions);	 
								}
							}
							if(cd1.Transducer !=null)
							{
								if(cd1.Transducer.Trim() !="")
								{
									cd1.Popular_Opt.Add(cd1.Transducer);	 
								}
							}
							string appoptcode="";

							for(int a=0;a<cd1.Application_Opt.Count;a++)
							{
								appoptcode +=cd1.Application_Opt[a].ToString();
							}
							string popoptcode="";
							for(int p=0;p<cd1.Popular_Opt.Count;p++)
							{
								popoptcode +=cd1.Popular_Opt[p].ToString();
							}
							string series="";
							if(cd1.Series.Trim() =="SA")
							{
								series="A";
							}
							else
							{
								series=cd1.Series.Trim();
							}
							cd1.PNO =series.ToString().Trim()+cd1.Bore_Size.ToString().Trim()+cd1.Rod_Diamtr.ToString().Trim()+
								cd1.Rod_End.ToString().Trim()+cd1.Cushions.ToString().Trim()+cd1.CushionPosition.ToString().Trim()+
								appoptcode.Trim()+ cd1.Seal_Comp.ToString().Trim()+cd1.Port_Type.ToString().Trim()+cd1.Port_Pos.ToString().Trim()+
								cd1.Mount.ToString().Trim()+cd1.Stroke.ToString().Trim()+popoptcode.Trim();					
										
							decimal f =120.00m;
							string st="";
							if(Convert.ToDecimal(cd1.Stroke.ToString()) > f)
							{
								st="For pricing please consult factory";
							}
							
							string sto ="";
							if(cd1.StopTube !=null)
							{
								if(cd1.StopTube.ToString() !="")
								{
									decimal a =0.00m;
									decimal b =0.00m;
									decimal c =0.00m;
									string ss="";
									if(cd1.StopTube.Trim().Substring(0,3) =="DST")
									{
										ss=cd1.StopTube.Trim().Substring(3);
									}
									else
									{
										ss=cd1.StopTube.Trim().Substring(2);
									}
									a =Convert.ToDecimal(ss);
									b= Convert.ToDecimal(cd1.Stroke.ToString());
									c= b - a;
									sto =" (Effective Stroke ="+ c.ToString()+"\")";
								}
							}
							string s=cd1.Rod_Diamtr.ToString().Trim()+cd1.Rod_End.ToString().Trim();
							string rodenddim ="";
							if(db.SelectRRD(s).Trim() !="")
							{
								rodenddim =" KK = "+db.SelectRRD(s).Trim();
							}
							string portsiz="";
					
							string table="";
							if(cd1.Series.Trim().Substring(0,1) =="A" ||cd1.Series.Trim().Substring(0,1) =="S")
							{
								table ="WEB_SeriesAPortSize_TableV1";
							}
							else if(cd1.Series.Trim().Substring(0,1) =="P" || cd1.Series.Trim().Substring(0,1) =="N")
							{
								table ="WEB_SeriesPNPortSize_TableV1";
							}
							else if(cd1.Series.Trim().Substring(0,1) =="M" || cd1.Series.Trim().Substring(0,1) =="L" || cd1.Series.Trim().Substring(0,1) =="R")
							{
								table ="WEB_SeriesMMLLRPortSize_TableV1";
							}
							if(cd1.Series.Trim() !="Z")
							{
								if(db.SelectValue(cd1.Port_Type.Trim(),table.Trim(),"Bore",cd1.Bore_Size.Trim()) !="")
								{
									portsiz ="#"+db.SelectPortSizeNo(cd1.Bore_Size.Trim(),cd1.Port_Type.Trim(),table.Trim());
								}
							}
							string pmml="";
							string tem="";
							if(cd1.Series.ToString().Trim() =="N")
							{
								if(cd1.Mount.ToString().Trim() =="F1")
								{
									tem =db.SelectSeriesNPressure(cd1.Series.ToString().Trim()+cd1.Bore_Size.ToString().Trim()+cd1.Rod_Diamtr.ToString().Trim(),"F1");
								}
								else
								{
									tem =db.SelectSeriesNPressure(cd1.Series.ToString().Trim()+cd1.Bore_Size.ToString().Trim()+cd1.Rod_Diamtr.ToString().Trim(),"NonF1");
								}
								if(tem.Trim() !="")
								{
									pmml="( Downrated to "+tem.Trim()+" PSI )";
								}
							}
							else if(cd1.Series.ToString().Trim() =="M" || cd1.Series.ToString().Trim() =="ML" || cd1.Series.ToString().Trim() =="L")
							{
								if(cd1.Mount.ToString().Trim() =="F1")
								{								
									tem =db.SelectSeriesMMLPressure("M"+cd1.Bore_Size.ToString().Trim()+cd1.Rod_Diamtr.ToString().Trim(),"F1");
									if(tem.Trim() !="")
									{
										pmml="( Downrated to "+tem.Trim()+" PSI )";
									}
								}
							}
							string head="<TABLE id=Table19 style= 'Z-INDEX: 203; LEFT: 0px; POSITION: relative; TOP: 0px;FONT-SIZE: 8pt; FONT-FAMILY: Arial;'  cellSpacing=0 cellPadding=0 width=101% border=0><TR><TD style= 'FONT-SIZE: 8pt; FONT-FAMILY: Arial;'ForeColor='Black' align=center bgColor=#808080>Code</TD><TD style= 'FONT-SIZE: 8pt; FONT-FAMILY: Arial;' bgColor=#808080>Description</TD></TR>";
							string 	str ="";
							str ="<TR><TD>"+series.Trim()+"</TD><TD>"+ db.SelectValue("Series_Name","WEB_Series_TableV1","Series_Code",cd1.Series.ToString().Trim())+"</TD></TR>";	
							str += "<TR><TD>"+cd1.Bore_Size+"</TD><TD>"+ db.SelectValue("Bore_Size","WEB_Bore_TableV1","Bore_Code",cd1.Bore_Size.ToString().Trim())+" Bore Size</TD></TR>";	
							str += "<TR><TD>"+cd1.Rod_Diamtr+"</TD><TD>"+db.SelectValue("Rod_Size","WEB_RodSerZ_TableV1","Rod_Code",cd1.Rod_Diamtr.ToString().Trim())+" Rod Size</TD></TR>";	
							str +="<TR><TD>"+cd1.Rod_End+"</TD><TD>"+db.SelectValue("RodEnd_Shape","WEB_RodEndKK_TableV1","RodEnd_Code",cd1.Rod_End.ToString().Trim())+rodenddim.ToString()+"</TD></TR>";	
							str +="<TR><TD>"+cd1.Cushions+"</TD><TD>"+db.SelectValue("Cushion_type","WEB_Cushion_TableV1","Cushion_Code",cd1.Cushions.ToString())+"</TD></TR>";	
							if(cd1.CushionPosition !=null)
							{
								if(cd1.CushionPosition.Trim() !="")
								{
									str +="<TR><TD>"+cd1.CushionPosition+"</TD><TD>"+db.SelectValue("CushionPos_Pos","WEB_CushionPos_TableV1","CushionPos_Code",cd1.CushionPosition.ToString().Trim())+"</TD></TR>";		
								}
							}	
						
							if(cd1.SecRodDiameter !=null)
							{
								if(cd1.SecRodDiameter.Trim() !="")
								{
									str +="<TR><TD>"+cd1.SecRodDiameter+"</TD><TD>"+db.SelectValue("SecRod_Diam","WEB_SecRod_TableV1","SecRod_Code",cd1.SecRodDiameter.ToString().Trim())+"Second Rod Diameter</TD></TR>";		
								}
							}
							if(cd1.GlandDrain !=null)
							{
								if(cd1.GlandDrain.Trim() !="")
								{
									str +="<TR><TD>"+cd1.GlandDrain+"</TD><TD>Gland Drain</TD></TR>";		
								}
							}
							if(cd1.Bushing_Conf !=null)
							{
								if(cd1.Bushing_Conf.Trim() !="")
								{
									str +="<TR><TD>"+cd1.Bushing_Conf+"</TD><TD>"+db.SelectValue("Bush_Type","WEB_GlandBushing_TableV1","Bush_Code",cd1.Bushing_Conf.ToString().Trim())+"</TD></TR>";	
								}
							}
							if(cd1.MetalScrapper !=null)
							{
								if(cd1.MetalScrapper.Trim() !="")
								{
									str +="<TR><TD>"+cd1.MetalScrapper+"</TD><TD>"+db.SelectValue("MetalScrapper","WEB_MetalScrapper_TableV1","MS_Code",cd1.MetalScrapper.ToString().Trim())+" Metal Scrapper</TD></TR>";
								}
							}
							if(cd1.PistonSeal_Conf !=null)
							{
								if(cd1.PistonSeal_Conf.Trim() !="")
								{
									if(cd1.PistonSeal_Conf =="P1")
									{
										str +="<TR><TD>"+cd1.PistonSeal_Conf+"</TD><TD>"+db.SelectValue("SealConf_Conf","WEB_PistonSealConf_TableV1","SealConf_Code",cd1.PistonSeal_Conf.ToString().Trim())+"</TD></TR>";		
									}
								}
							}
							if(cd1.MagnetRing !=null)
							{
								if(cd1.MagnetRing.Trim() !="")
								{
									str +="<TR><TD>"+cd1.MagnetRing+"</TD><TD>Magnet on piston</TD></TR>";		
								}
							}
							if(cd1.PistonSeal_Conf !=null)
							{
								if(cd1.PistonSeal_Conf.Trim() !="")
								{
									if(cd1.PistonSeal_Conf !="P1")
									{
										str +="<TR><TD>"+cd1.PistonSeal_Conf+"</TD><TD>"+db.SelectValue("SealConf_Conf","WEB_PistonSealConf_TableV1","SealConf_Code",cd1.PistonSeal_Conf.ToString().Trim())+"</TD></TR>";		
									}
								}
							}
							if(cd1.SecRodEnd !=null)
							{
								if(cd1.SecRodEnd.Trim() !="")
								{
									str +="<TR><TD>"+cd1.SecRodEnd+"</TD><TD>"+db.SelectValue("SecRodEnd_Type","WEB_SecRodEnd_TableV1","SecRodEnd_Code",cd1.SecRodEnd.ToString().Trim())+"</TD></TR>";		
								}
							}	
							if(cd1.RodSeal_Conf !=null)
							{
								if(cd1.RodSeal_Conf.Trim() !="")
								{
									str +="<TR><TD>"+cd1.RodSeal_Conf+"</TD><TD>"+db.SelectValue("SealConf_Conf","WEB_RodSealConf_TableV1","SealConf_Code",cd1.RodSeal_Conf.ToString().Trim())+"</TD></TR>";		
								}
							}
							if(cd1.Coating !=null)
							{
								if(cd1.Coating.Trim() !="")
								{
									if(cd1.Coating.Substring(0,1)=="W")
									{
										str +="<TR><TD >"+cd1.Coating+"</TD><TD >"+db.SelectValue("Coating_Type","WEB_Coating_TableV1","Coating_Code",cd1.Coating.ToString().Trim())+"</TD></TR>";		
									}
								}
							}
							//issue #90 start
							if(cd1.TieRodeCentSupp !=null)
							{
								if(cd1.TieRodeCentSupp.Trim() !="")
								{
									str +="<TR><TD >"+cd1.TieRodeCentSupp+"</TD><TD >"+db.SelectValue("Description","WEB_ApplicationOpt_TableV1","Application_Code",cd1.TieRodeCentSupp.ToString().Trim())+"</TD></TR>";		
								}
							}
							//issue #90 end
							str +="<TR><TD>"+cd1.Seal_Comp+"</TD><TD>"+db.SelectValue("Seal_Type","WEB_Seal_TableV1","Seal_Code",cd1.Seal_Comp.ToString().Trim())+"</TD></TR>";	
							str +="<TR><TD>"+cd1.Port_Type+"</TD><TD>"+portsiz.Trim()+" "+db.SelectValue("PortType_Type","WEB_portType_TableV1","PortType_Code",cd1.Port_Type.ToString().Trim())+"</TD></TR>";	
							str +="<TR><TD>"+cd1.Port_Pos+"</TD><TD>"+db.SelectValue("PortPos_Position","WEB_PortPosition_TableV1","PortPos_Code",cd1.Port_Pos.ToString().Trim())+"</TD></TR>";		
							str +="<TR><TD>"+cd1.Mount+"</TD><TD>"+db.SelectValue("Mount_Type","WEB_Mount"+cd1.Series.Trim()+"_TableV1","Mount_Code",cd1.Mount.ToString().Trim())+" "+pmml.Trim()+"</TD></TR>";	
							str +="<TR><TD>"+cd1.Stroke+"</TD><TD>"+"Stroke = "+cd1.Stroke.ToString()+"\""+sto.ToString()+st.ToString()+"</TD></TR>";	
						
				
							if(cd1.ThreadEx !=null)
							{
								if(cd1.ThreadEx.Trim() !="")
								{
									str +="<TR><TD>"+cd1.ThreadEx+"</TD><TD>Dim \"A\" ="+cd1.ThreadEx.Substring(1) +"\"</TD></TR>";		
								}
							}
							if(cd1.SecRodThreadx !=null)
							{
								if(cd1.SecRodThreadx.Trim() !="")
								{
									str +="<TR><TD>"+cd1.SecRodThreadx+"</TD><TD>Dim \"AD\" ="+cd1.SecRodThreadx.Substring(2) +"\"</TD></TR>";		
								}
							}
							if(cd1.BBC !=null)
							{
								if(cd1.BBC.Trim() !="")
								{
									str +="<TR><TD>"+cd1.BBC+"</TD><TD>Special Tie Rod Extension(Cap End)\"BB\" ="+cd1.BBC.Substring(3) +"\"</TD></TR>";		
								}
							}
							if(cd1.BBH !=null)
							{
								if(cd1.BBH.Trim() !="")
								{
									str +="<TR><TD>"+cd1.BBH+"</TD><TD>Special Tie Rod Extension(Head End)\"BB\" ="+cd1.BBH.Substring(3) +"\"</TD></TR>";		
								}
							}
							if(cd1.TandemDuplex !=null)
							{
								if(cd1.TandemDuplex.Trim() !="")
								{
									if(cd1.TandemDuplex.Trim().Substring(0,2) =="BC")
									{
										string tan="";
										string ss="";
										ss=cd1.TandemDuplex.Trim().Substring(2)+"\"";
										tan =cd1.TandemDuplex.ToString().Trim().Substring(0,2);
										str +="<TR><TD>"+cd1.TandemDuplex+"</TD><TD>"+db.SelectValue("Description","WEB_popular_Opt_TableV1","Popular_Code",tan.Trim())+ss.Trim()+"</TD></TR>";	
									}
								}
							}
							if(cd1.Coating !=null)
							{
								if(cd1.Coating.Trim() !="")
								{
									if(cd1.Coating.Substring(0,1)!="W")
									{
										str +="<TR><TD>"+cd1.Coating+"</TD><TD>"+db.SelectValue("Coating_Type","WEB_Coating_TableV1","Coating_Code",cd1.Coating.ToString().Trim())+"</TD></TR>";		
									}
								}
							}
							if(cd1.TandemDuplex !=null)
							{
								if(cd1.TandemDuplex.Trim() !="")
								{
									if(cd1.TandemDuplex.Substring(0,2) =="DC")
									{
										string tan="";
										string ss="";
										ss=cd1.TandemDuplex.Trim().Substring(2)+"\"";
										tan =cd1.TandemDuplex.ToString().Trim().Substring(0,2);
										str +="<TR><TD>"+cd1.TandemDuplex+"</TD><TD>"+db.SelectValue("Description","WEB_popular_Opt_TableV1","Popular_Code",tan.Trim())+ss.Trim()+"</TD></TR>";	
									}
								}
							}
							if(cd1.StopTube !=null)
							{
								if(cd1.StopTube.Trim() !="")
								{
									if(cd1.StopTube.Substring(0,1) =="D")
									{
										string ss="";
										if(cd1.StopTube.Trim().Substring(0,3) =="DST")
										{
											ss="Stop Tube Double Piston Design DST ="+cd1.StopTube.Trim().Substring(3);
										}
										else
										{
											ss="Stop Tube ="+cd1.StopTube.Trim().Substring(2);
										}
										str +="<TR><TD>"+cd1.StopTube+"</TD><TD>"+ss.Trim() +"\"</TD></TR>";		
									}
								}
							}
							if(cd1.CarbonFibBarrel !=null)
							{
								if(cd1.CarbonFibBarrel.Trim() !="")
								{
									if(cd1.CarbonFibBarrel =="M1")
									{
										str +="<TR><TD>"+cd1.CarbonFibBarrel+"</TD><TD>"+db.SelectValue("Description","WEB_BarrelMaterial_TableV1","Popular_Code",cd1.CarbonFibBarrel.ToString().Trim())+"</TD></TR>";		
									}
								}
							}
							if(cd1.SSTieRod !=null)
							{
								if(cd1.SSTieRod.Trim() !="")
								{
									str +="<TR><TD>"+cd1.SSTieRod+"</TD><TD>Stainless Steel Tie Rod </TD></TR>";		
								}
							}
							if(cd1.SSPistionRod !=null)
							{
								if(cd1.SSPistionRod.Trim() !="")
								{
									str +="<TR><TD>"+cd1.SSPistionRod+"</TD><TD>"+db.SelectValue("SSPRod_Type","WEB_SSPRod_TableV1","SSPRod_Code",cd1.SSPistionRod.ToString().Trim())+" Piston Rod</TD></TR>";		
								}
							}
							if(cd1.CarbonFibBarrel !=null)
							{
								if(cd1.CarbonFibBarrel.Trim() !="")
								{
									if(cd1.CarbonFibBarrel =="M6")
									{
										str +="<TR><TD >"+cd1.CarbonFibBarrel+"</TD><TD >"+db.SelectValue("Description","WEB_BarrelMaterial_TableV1","Popular_Code",cd1.CarbonFibBarrel.ToString().Trim())+"</TD></TR>";		
									}
								}
							}
							if(cd1.AirBleed !=null)
							{
								if(cd1.AirBleed.Trim() !="")
								{
									str +="<TR><TD>"+cd1.AirBleed+"</TD><TD>"+db.SelectValue("AirBleed_Type","WEB_AirBleed_TableV1","AirBleed_Code",cd1.AirBleed.ToString().Trim())+"</TD></TR>";		
								}
							}
							if(cd1.PortSize !=null)
							{
								if(cd1.PortSize.Trim() !="")
								{
									str +="<TR><TD>"+cd1.PortSize+"</TD><TD>"+db.SelectValue("PortSize_Type","WEB_PortSize_TableV1","PortSize_Code",cd1.PortSize.ToString().Trim())+"</TD></TR>";		
								}
							}
							if(cd1.StopTube !=null)
							{
								if(cd1.StopTube.Trim() !="")
								{
									if(cd1.StopTube.Substring(0,1) =="S")
									{
										string ss="";
										if(cd1.StopTube.Trim().Substring(0,3) =="DST")
										{
											ss="Stop Tube Double Piston Design DST ="+cd1.StopTube.Trim().Substring(3);
										}
										else
										{
											ss="Stop Tube ="+cd1.StopTube.Trim().Substring(2);
										}
										str +="<TR><TD>"+cd1.StopTube+"</TD><TD>"+ss.Trim() +"\"</TD></TR>";		
									}
								}
							}
							if(cd1.TandemDuplex !=null)
							{
								if(cd1.TandemDuplex.Trim() !="")
								{
									if(cd1.TandemDuplex.Substring(0,2) =="TC")
									{
										str +="<TR><TD>"+cd1.TandemDuplex+"</TD><TD>"+db.SelectValue("Description","WEB_popular_Opt_TableV1","Popular_Code",cd1.TandemDuplex.Trim())+"</TD></TR>";	
									}
								}
							}
							if(cd1.RodEx !=null)
							{
								if(cd1.RodEx.Trim() !="")
								{
									str +="<TR><TD>"+cd1.RodEx+"</TD><TD>Dim \"W\" ="+cd1.RodEx.Substring(1) +"\"</TD></TR>";		
								}
							}
							if(cd1.SecRodEx !=null)
							{
								if(cd1.SecRodEx.Trim() !="")
								{
									str +="<TR><TD>"+cd1.SecRodEx+"</TD><TD>Dim \"WD\" ="+cd1.SecRodEx.Substring(2) +"\"</TD></TR>";		
								}
							}
							if(cd1.Trunnions !=null)
							{
								if(cd1.Trunnions.Trim() !="")
								{
									str +="<TR><TD>"+cd1.Trunnions+"</TD><TD>Intermediate Trunnions XI="+ cd1.Trunnions.Trim().Substring(2)+"\"</TD></TR>";		
								}
							}
							if(cd1.Transducer !=null)
							{
								string companyid="";
								if(cd1.CompanyID !=null)
								{
									if(cd1.CompanyID.ToString().Trim() !="")
									{
										companyid=cd1.CompanyID.ToString().Trim();
									}
								}
								//issue #118
								//backup
								//if(companyid.ToString().Trim() =="1002" || companyid.ToString().Trim() =="1015")
								//update
								if(companyid.ToString().Trim() =="1002" || companyid.ToString().Trim() =="1015" || companyid.ToString().Trim() =="1021")
								//issue #118 end
								{
									string desc="";
									desc=db.SelectPopopts(cd1.Transducer.Trim());
									str +="<TR><TD>"+cd1.Transducer+"</TD><TD>"+ desc.Trim()+"</TD></TR>";
								}
								else if(companyid.ToString().Trim() =="1003")
								{
									if(cd1.Transducer.Trim() !="")
									{
										string tt="";	
										if(cd1.Transducer.Trim() =="T2")
										{
											ArrayList lst=new ArrayList();
											lst=db.SelectTransducerDisc(cd1.Transducer.ToString().Trim());		
											for(int h=0;h<lst.Count;h++)
											{
												tt +=lst[h].ToString();
												if(lst[h].ToString()!=null && h < lst.Count -1)
												{
													tt +=", ";
												}
											}
										}
										else
										{
											if(cd1.Transducer.Length == 3)
											{
												ArrayList lst=new ArrayList();
												lst=db.SelectTransducerDisc(cd1.Transducer.ToString().Substring(0,3));		
												for(int h=0;h<lst.Count;h++)
												{
													tt +=lst[h].ToString();
													if(lst[h].ToString()!=null && h < lst.Count -1)
													{
														if(lst[h].ToString().Trim() !="")
														{
															tt +=", ";
														}
													}
												}
											}
											else if(cd1.Transducer.Length > 3)
											{
												if(cd1.Transducer.Substring(2,1) =="R" || cd1.Transducer.Substring(2,1) =="P" 
													|| cd1.Transducer.Substring(2,1) =="C" || cd1.Transducer.Substring(2,1) =="Q"
													|| cd1.Transducer.Substring(2,1) =="D")
												{
													ArrayList lst=new ArrayList();
													lst=db.SelectTransducerDisc(cd1.Transducer.ToString().Substring(0,3));		
													for(int h=0;h<lst.Count;h++)
													{
														tt +=lst[h].ToString();
														if(lst[h].ToString()!=null && h < lst.Count -1)
														{
															if(lst[h].ToString().Trim() !="")
															{
																tt +=", ";
															}
														}
													}
													if(cd1.Transducer.ToString().Trim().Length >3)
													{
														tt =tt.Replace("10K",cd1.Transducer.ToString().Substring(3)+"K");
													}
												}
												else
												{
													ArrayList lst=new ArrayList();
													lst=db.SelectTransducerDisc(cd1.Transducer.ToString().Substring(0,2));		
													for(int h=0;h<lst.Count;h++)
													{
														tt +=lst[h].ToString();
														if(lst[h].ToString()!=null && h < lst.Count -1)
														{
															if(lst[h].ToString().Trim() !="")
															{
																tt +=", ";
															}
														}
													}
													if(cd1.Transducer.ToString().Trim().Length >2)
													{
														tt =tt.Replace("10K",cd1.Transducer.ToString().Substring(2)+"K");
													}
												}
											}
										}
										str +="<TR><TD>"+cd1.Transducer+"</TD><TD>"+tt.Trim()+"</TD></TR>";		
									}
								}
								else
								{
									if(cd1.Transducer.Trim() !="")
									{
										string tt="";	
										if(cd1.Transducer.Length >2)
										{
											if(cd1.Transducer.ToString().Trim().Substring(2,1) =="S")
											{
									
												ArrayList lst=new ArrayList();
												lst=db.SelectTransducerDisc(cd1.Transducer.ToString().Substring(0,3));		
												for(int h=0;h<lst.Count;h++)
												{
													tt +=lst[h].ToString();
													if(lst[h].ToString()!=null && h < lst.Count -1)
													{
														tt +=", ";
													}
												}
												if(cd1.Transducer.ToString().Trim().Length >3)
												{
													tt =tt.Replace("10K",cd1.Transducer.ToString().Substring(3)+"K");
												}
											}
											else if(cd1.Transducer.ToString().Trim().Substring(2,1) =="R")
											{
									
												ArrayList lst=new ArrayList();
												lst=db.SelectTransducerDisc(cd1.Transducer.ToString().Substring(0,3));		
												for(int h=0;h<lst.Count;h++)
												{
													tt +=lst[h].ToString();
													if(lst[h].ToString()!=null && h < lst.Count -1)
													{
														if(lst[h].ToString().Trim() !="")
														{
															tt +=", ";
														}
													}
												}
												if(cd1.Transducer.ToString().Trim().Length >3)
												{
													tt =tt.Replace("10K",cd1.Transducer.ToString().Substring(3)+"K");
												}
											}
											else if(cd1.Transducer.ToString().Trim().Substring(1,1) =="1" ||cd1.Transducer.ToString().Trim().Substring(1,1) =="2"
												||cd1.Transducer.ToString().Trim().Substring(1,1) =="3" || cd1.Transducer.ToString().Trim().Substring(1,1) =="4"
												|| cd1.Transducer.ToString().Trim().Substring(1,1) =="5")
											{
										
												ArrayList lst=new ArrayList();
												lst=db.SelectTransducerDisc(cd1.Transducer.ToString().Substring(0,2));		
												for(int h=0;h<lst.Count;h++)
												{
													tt +=lst[h].ToString();
													if(lst[h].ToString()!=null && h < lst.Count -1)
													{
														tt +=", ";
													}
												}
												if(cd1.Transducer.ToString().Trim().Length >2)
												{
													tt =tt.Replace("10K",cd1.Transducer.ToString().Substring(2)+"K");
												}
											}
										}
										else
										{
											ArrayList lst=new ArrayList();
											lst=db.SelectTransducerDisc(cd1.Transducer.ToString().Substring(0,2));		
											for(int h=0;h<lst.Count;h++)
											{
												tt +=lst[h].ToString();
												if(lst[h].ToString()!=null && h < lst.Count -1)
												{
													tt +=", ";
												}
											}
										}
										str +="<TR><TD>"+cd1.Transducer+"</TD><TD>"+tt.Trim()+"</TD></TR>";		
									}
								}
							}
							if(cd1.CompanyID !=null)
							{
								if(cd1.CompanyID.ToString().Trim() =="1004")
								{
									if(cd1.Specials !=null)
									{
										if(cd1.Specials.ToUpper().Trim() =="YES")
										{
											string sp ="";
											int tt=Convert.ToInt32(db.SelectLastSpNo());
											sp=(tt+1).ToString();
											cd1.PNO ="Z"+cd1.Series.ToString().Trim()+cd1.Bore_Size.ToString().Trim()+cd1.Rod_Diamtr.ToString().Trim()+cd1.Rod_End.ToString().Trim()+
												cd1.Cushions.ToString().Trim()+cd1.Mount.ToString().Trim()+"/Z"+sp.ToString();
											if(cd1.MountKit !=null)
											{
												if(cd1.MountKit.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.MountKit.Trim())+" (Valve Size ="+cd1.ValveSize.Trim()+"\")</TD></TR>";		
												}
											}
											if(cd1.ManualOverride !=null)
											{
												if(cd1.ManualOverride.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.ManualOverride.Trim())+"</TD></TR>";		
												}
											}
											if(cd1.LimitSwitch !=null)
											{
												if(cd1.LimitSwitch.Trim() !="")
												{
													string spls="";
													if(cd1.SpecialLimitSwitch.Trim() !="")
													{
														spls=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.LimitSwitch.Trim());
														spls=spls.ToString().Replace("#", cd1.SpecialLimitSwitch.Trim());
													}
													else
													{
														spls=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.LimitSwitch.Trim());
													}
													str +="<TR><TD></TD><TD>"+spls.Trim()+"</TD></TR>";		
												}
											}
											if(cd1.FilterRegulator !=null)
											{
												if(cd1.FilterRegulator.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.FilterRegulator.Trim())+"</TD></TR>";		
												}
											}
											if(cd1.Solenoid !=null)
											{
												if(cd1.Solenoid.Trim() !="")
												{
													string ss="";
													if(cd1.SolenoidClass.ToString().Trim() !="")
													{
														ss=" (Classification: "+cd1.SolenoidClass.ToString().Trim()+")";
													}
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.Solenoid.Trim())+ss.Trim()+"</TD></TR>";		
												}
											}
											if(cd1.FlowControl !=null)
											{
												if(cd1.FlowControl.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.FlowControl.Trim())+"</TD></TR>";		
												}
											}
											if(cd1.Tubing !=null)
											{
												if(cd1.Tubing.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.Tubing.Trim())+"</TD></TR>";		
												}
											}
										}
										else if(cd1.Specials.ToUpper().Trim() =="SPECIAL")
										{
											string sp ="";
											int tt=Convert.ToInt32(db.SelectLastSpNo());
											sp=(tt+1).ToString();
											cd1.PNO ="Z"+cd1.Series.ToString().Trim()+cd1.Bore_Size.ToString().Trim()+cd1.Rod_Diamtr.ToString().Trim()+cd1.Rod_End.ToString().Trim()+
												cd1.Cushions.ToString().Trim()+cd1.Mount.ToString().Trim()+"/Z"+sp.ToString();
											if(cd1.Style !=null)
											{
												if(cd1.Style.Trim() !="")
												{
													if(cd1.Style.Trim() =="FC" || cd1.Style.Trim() =="FO")
													{
														//														RBLQty.SelectedIndex=0;
														//														RBLQty_SelectedIndexChanged(sender,e);
														string ff="";
														if(cd1.Style.Trim() =="FC")
														{
															ff="Fail Close\r\n";
														}
														else
														{
															ff="Fail Open\r\n";
														}
														if(cd1.ValveSize !=null)
														{
															ff +="  Valve Size : "+cd1.ValveSize.ToString().Trim()+"\r\n";
														}
														if(cd1.LinearPressure !=null)
														{
															ff +="  Line Pressure : "+cd1.LinearPressure.ToString().Trim()+"\r\n";
														}
														if(cd1.MinAirSupply !=null)
														{
															ff +="  Min Air Supply : "+cd1.MinAirSupply.ToString().Trim()+"\r\n";
														}
														if(cd1.SeatingTrust !=null)
														{
															ff +="  Seating Thrust : "+cd1.SeatingTrust.ToString().Trim()+"\r\n";
														}
														if(cd1.PackingFriction !=null)
														{
															ff +="  Packing Friction : "+cd1.PackingFriction.ToString().Trim()+"\r\n";
														}
														if(cd1.SaftyFactor !=null)
														{
															ff +="  Requested Safety Factor : "+cd1.SaftyFactor.ToString().Trim()+"\r\n";
														}
														if(cd1.ActualSaftyFactor !=null)
														{
															ff +="  Actual Safety Factor : "+cd1.ActualSaftyFactor.ToString().Trim()+"\r\n";
														}
														if(cd1.ClosingTime !=null)
														{
															ff +="  Closing Time(Sec) : "+cd1.ClosingTime.ToString().Trim()+"\r\n";
														}
														//														TxtSpecialReq.Text="Style: "+ff.Trim();
														string fcfo="";
														fcfo=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.Style.Trim()).Replace(";","<BR>");
														str +="<TR><TD></TD><TD>"+fcfo.ToString().Replace("#",cd1.MinAirSupply.Trim())+"</TD></TR>";		
													}
												}
											}
											if(cd1.MountKit !=null)
											{
												if(cd1.MountKit.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.MountKit.Trim())+" (Valve Size ="+cd1.ValveSize.Trim()+"\")</TD></TR>";		
												}
											}
											if(cd1.ManualOverride !=null)
											{
												if(cd1.ManualOverride.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.ManualOverride.Trim())+"</TD></TR>";		
												}
											}
											if(cd1.LimitSwitch !=null)
											{
												if(cd1.LimitSwitch.Trim() !="")
												{
													string spls="";
													if(cd1.SpecialLimitSwitch.Trim() !="")
													{
														spls=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.LimitSwitch.Trim());
														spls=spls.ToString().Replace("#", cd1.SpecialLimitSwitch.Trim());
													}
													else
													{
														spls=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.LimitSwitch.Trim());
													}
													str +="<TR><TD></TD><TD>"+spls.Trim()+"</TD></TR>";		
												}
											}
											if(cd1.FilterRegulator !=null)
											{
												if(cd1.FilterRegulator.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.FilterRegulator.Trim())+"</TD></TR>";		
												}
											}
											if(cd1.Solenoid !=null)
											{
												if(cd1.Solenoid.Trim() !="")
												{
													string ss="";
													if(cd1.SolenoidClass.ToString().Trim() !="")
													{
														ss=" (Classification: "+cd1.SolenoidClass.ToString().Trim()+")";
													}
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.Solenoid.Trim())+ss.Trim()+"</TD></TR>";		
												}
											}
											if(cd1.FlowControl !=null)
											{
												if(cd1.FlowControl.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.FlowControl.Trim())+"</TD></TR>";		
												}
											}
											if(cd1.Tubing !=null)
											{
												if(cd1.Tubing.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.Tubing.Trim())+"</TD></TR>";		
												}
											}
										}
									}
								}

								else if(cd1.CompanyID.ToString().Trim() =="1003")
								{
									if(cd1.Specials !=null)
									{
										if(cd1.Specials.ToUpper().Trim() =="YES")
										{
											string sp ="";
											int tt=Convert.ToInt32(db.SelectLastSpNo());
											sp=(tt+1).ToString();
											cd1.PNO ="Z"+cd1.Series.ToString().Trim()+cd1.Bore_Size.ToString().Trim()+cd1.Rod_Diamtr.ToString().Trim()+cd1.Rod_End.ToString().Trim()+
												cd1.Cushions.ToString().Trim()+cd1.Mount.ToString().Trim()+"/Z"+sp.ToString();
											if(cd1.Style !=null)
											{
												if(cd1.Style.Trim() !="")
												{
													if(cd1.Style.Trim() =="FC" || cd1.Style.Trim() =="FO")
													{
														//														RBLQty.SelectedIndex=0;
														//														RBLQty_SelectedIndexChanged(sender,e);
														string ff="";
														if(cd1.Style.Trim() =="FC")
														{
															ff="Fail Close\r\n";
														}
														else
														{
															ff="Fail Open\r\n";
														}
														if(cd1.ValveSize !=null)
														{
															ff +="Valve Size : "+cd1.ValveSize.ToString().Trim()+"\r\n";
														}
														if(cd1.LinearPressure !=null)
														{
															ff +="Line Pressure : "+cd1.LinearPressure.ToString().Trim()+"\r\n";
														}
														if(cd1.MinAirSupply !=null)
														{
															ff +="Min Air Supply : "+cd1.MinAirSupply.ToString().Trim()+"\r\n";
														}
														if(cd1.SeatingTrust !=null)
														{
															ff +="Seating Thrust : "+cd1.SeatingTrust.ToString().Trim()+"\r\n";
														}
														if(cd1.PackingFriction !=null)
														{
															ff +="Packing Friction(LBS) : "+cd1.PackingFriction.ToString().Trim()+"\r\n";
														}
														if(cd1.SaftyFactor !=null)
														{
															ff +="Safety Factor : "+cd1.SaftyFactor.ToString().Trim()+"\r\n";
														}
														if(cd1.ActualSaftyFactor !=null)
														{
															ff +="Actual Safety Factor : "+cd1.ActualSaftyFactor.ToString().Trim()+"\r\n";
														}
														if(cd1.ClosingTime !=null)
														{
															ff +="Closing Time(Sec) : "+cd1.ClosingTime.ToString().Trim()+"\r\n";
														}
														//														TxtSpecialReq.Text="Style: "+ff.Trim();
														string fcfo="";
														fcfo=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.Style.Trim()).Replace(";","<BR>");
														str +="<TR><TD></TD><TD>"+fcfo.ToString().Replace("#",cd1.MinAirSupply.Trim())+"</TD></TR>";		
													}
												}
											}
											if(cd1.ManualOverride !=null)
											{
												if(cd1.ManualOverride.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.ManualOverride.Trim())+"</TD></TR>";		
												}
											}
											if(cd1.ReedSwitch !=null)
											{
												if(cd1.ReedSwitch.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.ReedSwitch.Trim())+"</TD></TR>";		
												}
											}
											if(cd1.MountKit !=null)
											{
												if(cd1.MountKit.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.MountKit.Trim())+"</TD></TR>";		
												}
											}
											if(cd1.LimitSwitch !=null)
											{
												if(cd1.LimitSwitch.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.LimitSwitch.Trim())+"</TD></TR>";		
												}
											}
											if(cd1.Positioner !=null)
											{
												if(cd1.Positioner.Trim() !="")
												{
													string pp="";
													if(cd1.PositionerPno.ToString().Trim() !="")
													{
														pp=" (Part# "+cd1.PositionerPno.ToString().Trim()+")";
													}
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.Positioner.Trim())+pp.Trim()+"</TD></TR>";		
												}
											}
											if(cd1.FilterRegulator !=null)
											{
												if(cd1.FilterRegulator.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.FilterRegulator.Trim())+"</TD></TR>";		
												}
											}
											if(cd1.Solenoid !=null)
											{
												if(cd1.Solenoid.Trim() !="")
												{
													string ss="";
													if(cd1.SolenoidClass.ToString().Trim() !="")
													{
														ss=" (Classification: "+cd1.SolenoidClass.ToString().Trim()+")";
													}
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.Solenoid.Trim())+ss.Trim()+"</TD></TR>";		
												}
											}
											if(cd1.FlowControl !=null)
											{
												if(cd1.FlowControl.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.FlowControl.Trim())+"</TD></TR>";		
												}
											}
											if(cd1.Tubing !=null)
											{
												if(cd1.Tubing.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.Tubing.Trim())+"</TD></TR>";		
												}
											}
										}
									}
								}
								if(cd1.CompanyID.ToString().Trim() =="1017")
								{
									if(cd1.Specials !=null)
									{
										if(cd1.Specials.ToUpper().Trim() =="YES")
										{
											string sp ="";
											int tt=Convert.ToInt32(db.SelectLastSpNo());
											sp=(tt+1).ToString();
											cd1.PNO ="Z"+cd1.Series.ToString().Trim()+cd1.Bore_Size.ToString().Trim()+cd1.Rod_Diamtr.ToString().Trim()+cd1.Rod_End.ToString().Trim()+
												cd1.Cushions.ToString().Trim()+cd1.Mount.ToString().Trim()+"/Z"+sp.ToString();
											if(cd1.MountKit !=null)
											{
												if(cd1.MountKit.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.MountKit.Trim())+" (Valve Size ="+cd1.ValveSize.Trim()+"\")</TD></TR>";		
												}
											}
											if(cd1.ManualOverride !=null)
											{
												if(cd1.ManualOverride.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.ManualOverride.Trim())+"</TD></TR>";		
												}
											}
											if(cd1.LimitSwitch !=null)
											{
												if(cd1.LimitSwitch.Trim() !="")
												{
													string spls="";
													if(cd1.SpecialLimitSwitch.Trim() !="")
													{
														spls=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.LimitSwitch.Trim());
														spls=spls.ToString().Replace("#", cd1.SpecialLimitSwitch.Trim());
													}
													else
													{
														spls=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.LimitSwitch.Trim());
													}
													str +="<TR><TD></TD><TD>"+spls.Trim()+"</TD></TR>";		
												}
											}
											if(cd1.FilterRegulator !=null)
											{
												if(cd1.FilterRegulator.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.FilterRegulator.Trim())+"</TD></TR>";		
												}
											}
											if(cd1.Solenoid !=null)
											{
												if(cd1.Solenoid.Trim() !="")
												{
													string ss="";
													if(cd1.SolenoidClass.ToString().Trim() !="")
													{
														ss=" (Classification: "+cd1.SolenoidClass.ToString().Trim()+")";
													}
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.Solenoid.Trim())+ss.Trim()+"</TD></TR>";		
												}
											}
											if(cd1.FlowControl !=null)
											{
												if(cd1.FlowControl.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.FlowControl.Trim())+"</TD></TR>";		
												}
											}
											if(cd1.Tubing !=null)
											{
												if(cd1.Tubing.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.Tubing.Trim())+"</TD></TR>";		
												}
											}
										}
										else if(cd1.Specials.ToUpper().Trim() =="SPECIAL")
										{
											string sp ="";
											int tt=Convert.ToInt32(db.SelectLastSpNo());
											sp=(tt+1).ToString();
											cd1.PNO ="Z"+cd1.Series.ToString().Trim()+cd1.Bore_Size.ToString().Trim()+cd1.Rod_Diamtr.ToString().Trim()+cd1.Rod_End.ToString().Trim()+
												cd1.Cushions.ToString().Trim()+cd1.Mount.ToString().Trim()+"/Z"+sp.ToString();
											if(cd1.Style !=null)
											{
												if(cd1.Style.Trim() !="")
												{
													if(cd1.Style.Trim() =="FC" || cd1.Style.Trim() =="FO")
													{
														//														RBLQty.SelectedIndex=0;
														//														RBLQty_SelectedIndexChanged(sender,e);
														string ff="";
														if(cd1.Style.Trim() =="FC")
														{
															ff="Fail Close\r\n";
														}
														else
														{
															ff="Fail Open\r\n";
														}
														if(cd1.ValveSize !=null)
														{
															ff +="  Valve Size : "+cd1.ValveSize.ToString().Trim()+"\r\n";
														}
														if(cd1.LinearPressure !=null)
														{
															ff +="  Line Pressure : "+cd1.LinearPressure.ToString().Trim()+"\r\n";
														}
														if(cd1.MinAirSupply !=null)
														{
															ff +="  Min Air Supply : "+cd1.MinAirSupply.ToString().Trim()+"\r\n";
														}
														if(cd1.SeatingTrust !=null)
														{
															ff +="  Seating Thrust : "+cd1.SeatingTrust.ToString().Trim()+"\r\n";
														}
														if(cd1.PackingFriction !=null)
														{
															ff +="  Packing Friction : "+cd1.PackingFriction.ToString().Trim()+"\r\n";
														}
														if(cd1.SaftyFactor !=null)
														{
															ff +="  Requested Safety Factor : "+cd1.SaftyFactor.ToString().Trim()+"\r\n";
														}
														if(cd1.ActualSaftyFactor !=null)
														{
															ff +="  Actual Safety Factor : "+cd1.ActualSaftyFactor.ToString().Trim()+"\r\n";
														}
														if(cd1.ClosingTime !=null)
														{
															ff +="  Closing Time(Sec) : "+cd1.ClosingTime.ToString().Trim()+"\r\n";
														}
														//														TxtSpecialReq.Text="Style: "+ff.Trim();
														string fcfo="";
														fcfo=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.Style.Trim()).Replace(";","<BR>");
														str +="<TR><TD></TD><TD>"+fcfo.ToString().Replace("#",cd1.MinAirSupply.Trim())+"</TD></TR>";		
													}
												}
											}
											if(cd1.MountKit !=null)
											{
												if(cd1.MountKit.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.MountKit.Trim())+" (Valve Size ="+cd1.ValveSize.Trim()+"\")</TD></TR>";		
												}
											}
											if(cd1.ManualOverride !=null)
											{
												if(cd1.ManualOverride.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.ManualOverride.Trim())+"</TD></TR>";		
												}
											}
											if(cd1.LimitSwitch !=null)
											{
												if(cd1.LimitSwitch.Trim() !="")
												{
													string spls="";
													if(cd1.SpecialLimitSwitch.Trim() !="")
													{
														spls=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.LimitSwitch.Trim());
														spls=spls.ToString().Replace("#", cd1.SpecialLimitSwitch.Trim());
													}
													else
													{
														spls=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.LimitSwitch.Trim());
													}
													str +="<TR><TD></TD><TD>"+spls.Trim()+"</TD></TR>";		
												}
											}
											if(cd1.FilterRegulator !=null)
											{
												if(cd1.FilterRegulator.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.FilterRegulator.Trim())+"</TD></TR>";		
												}
											}
											if(cd1.Solenoid !=null)
											{
												if(cd1.Solenoid.Trim() !="")
												{
													string ss="";
													if(cd1.SolenoidClass.ToString().Trim() !="")
													{
														ss=" (Classification: "+cd1.SolenoidClass.ToString().Trim()+")";
													}
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.Solenoid.Trim())+ss.Trim()+"</TD></TR>";		
												}
											}
											if(cd1.FlowControl !=null)
											{
												if(cd1.FlowControl.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.FlowControl.Trim())+"</TD></TR>";		
												}
											}
											if(cd1.Tubing !=null)
											{
												if(cd1.Tubing.Trim() !="")
												{
													str +="<TR><TD></TD><TD>"+db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",cd1.Tubing.Trim())+"</TD></TR>";		
												}
											}
										}
									}
								}
							}
							
							if(Session["Accessories"] !=null)
							{
								ArrayList acclist=new ArrayList();
								acclist=(ArrayList)Session["Accessories"];
								Others oth=new Others();
								for(int p=0;p<acclist.Count;p++)
								{
									oth=new Others();
									oth =(Others)acclist[p];
									str +="<TR><TD><strong>"+oth.Ids.Trim()+"</strong></TD><TD>"+ oth.Desc1.Trim()+"</TD></TR>";		
								}
							}
							str+="</TABLE>";
							LblPartNo.Text=cd1.PNO.ToString();
							LBLSeries.Text=head.ToString()+str.ToString();
						}
									
					}
				}
			}
			else
			{
				Response.Redirect("Login.aspx");
			}
		}
		public string Qno(string use)
		{
			try
			{
				string qno ="";
				string count = "";
				string usr=use.ToUpper();
				string st= usr.Substring(0,1);
				string s1=DateTime.Today.Month.ToString();
				count=db.SelectValue("QuoteNo","WEB_QuoteCount_TableV1","SLNo","1");
				if(count.Length !=0)
				{
					int lst=Convert.ToInt32(count.Substring(5));
				
				
					string s=DateTime.Today.Month.ToString();
					if(s.Length ==1)
					{
						s="0"+DateTime.Today.Month.ToString();
					}
					else
					{
						s=DateTime.Today.Month.ToString();
					}
					if(count !="")
					{
						if(count.Substring(1,2).Equals(DateTime.Today.Year.ToString().Substring(2)))
						{
							if(count.Substring(3,2).Equals(s))
							{
								lst++;
								string num="";
								if(lst.ToString().Length ==4)
								{
									num=lst.ToString();
								}
								else if(lst.ToString().Length ==3)
								{
									num="0"+lst.ToString();
								}
								else if(lst.ToString().Length ==2)
								{
									num="00"+lst.ToString();
								}
								else 
								{
									num="000"+lst.ToString();
								}
							
								if(s1.Length ==1)
								{
									s1="0"+DateTime.Today.Month.ToString();
								}
								else
								{
									s1=DateTime.Today.Month.ToString();
								}
								qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+num.ToString();
							}
							else
							{
						
								if(s1.Length ==1)
								{
									s1="0"+DateTime.Today.Month.ToString();
								}
								else
								{
									s1=DateTime.Today.Month.ToString();
								}
								qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+"0001";
							}
	
						}
						else
						{
						
							if(s1.Length ==1)
							{
								s1="0"+DateTime.Today.Month.ToString();
							}
							else
							{
								s1=DateTime.Today.Month.ToString();
							}
							qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+"0001";
						}
					}
					else
					{
					
						if(s1.Length ==1)
						{
							s1="0"+DateTime.Today.Month.ToString();
						}
						else
						{
							s1=DateTime.Today.Month.ToString();
						}
						qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+"0001";
					}
				}
				else
				{
					qno =st.Trim().ToUpper()+DateTime.Today.Year.ToString().Substring(2)+s1.ToString()+"0001";
				}
				string sav=db.InsertQuoteNo(qno);
				db.InsertQuoteCount(qno);
				return qno;
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
				return null;
			}
		
		}
		//issue #233 start
		private string Cyl_Displacement(coder PN)
		{
			string strCylDis = "TBA";
			try
			{
				DBClass db=new DBClass();
				decimal dcBore = 0m;
				decimal dcRod = 0m;
				decimal dcStroke = Convert.ToDecimal(PN.Stroke);
				dcRod = Convert.ToDecimal(db.SelectRodValue(PN.Rod_Diamtr));
				dcBore = Convert.ToDecimal(db.SelectBoreValue(PN.Bore_Size));
				decimal dcCylDis = 0m;
				decimal dcCylDisRet=0m;
				if(PN.DoubleRod == "No" && PN.FailMode != "FC" && PN.FailMode != "FO" )
				{
					dcCylDisRet=(dcBore*dcBore)*3.14m*dcStroke/4m;
				}
				dcCylDis=(dcBore*dcBore-dcRod*dcRod)*3.14m*dcStroke/4m + dcCylDisRet;
				if(PN.TandemDuplex == "TC")
				{
					dcCylDis *=2m;
				}
				dcCylDis = Math.Round(dcCylDis,0);
				strCylDis=dcCylDis.ToString();
			}
			catch (Exception ex)
			{		
				
				
			}
			return strCylDis;
		}
		//issue #233 end
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.RBLQty.SelectedIndexChanged += new System.EventHandler(this.RBLQty_SelectedIndexChanged);
			this.LbPreview.Click += new System.EventHandler(this.LbPreview_Click);
			this.LBGenerate.Click += new System.EventHandler(this.LBGenerate_Click);
			this.LBSend.Click += new System.EventHandler(this.LBSend_Click);
			this.LBBack.Click += new System.EventHandler(this.LBBack_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		private void RBLQty_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(RBLQty.SelectedIndex == 0)
			{
				TxtSpecialReq.Visible =true;
			}
			else
			{
				TxtSpecialReq.Visible =false;
				TxtSpecialReq.Text="";
			}
		}

		private void LBGenerate_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(TxtQty.Text.Trim() !=""  && Session["Coder"] !=null)
				{
					lblQty.Visible =false;
					ArrayList lst =new ArrayList();
					lst =(ArrayList)Session["User"];
					coder PN= (coder)Session["Coder"];
					if(PN.DoubleRod ==null)
					{
						if(PN.SecRodDiameter !=null)
						{
							if(PN.SecRodDiameter !="")
							{
								PN.DoubleRod="Yes";
							}
							else
							{
								PN.DoubleRod="No";
							}
						}
						else
						{
							PN.DoubleRod="No";
						}
					}
					if(PN.Series.Trim() =="PA")
					{
						//issue #644 start
						if(lst[6].ToString().Trim() =="1030")
						{
							PAPricing_SMC();
						}
						else
						//issue #644 end
						PAPricing();
					}
					else if(PN.Series.Trim() =="PS")
					{
						//issue #644 start
						if(lst[6].ToString().Trim() =="1030")
						{
							PSPricing_SMC();
						}
						else
						//issue #644 end
						PSPricing();
					}
					else if(PN.Series.Trim() =="PC")
					{
						//issue #644 start
						if(lst[6].ToString().Trim() =="1030")
						{
							PCPricing_SMC();
						}
						else
						//issue #644 end
						PCPricing();
					}
					else if(PN.Series.Trim() =="A")
					{
						APricing();
					}
					else if(PN.Series.Trim() =="AT")
					{
						ATPricing();
					}
					else if(PN.Series.Trim() =="SA")
					{
						SAPricing();
					}
					else if(PN.Series.Trim() =="M")
					{
						MPricing();
					}
					else if(PN.Series.Trim() =="ML")
					{
						MLPricing();
					}
					else if(PN.Series.Trim() =="L")
					{
						LPricing();
					}
					else if(PN.Series.Trim() =="N")
					{
						NPricing();
					}
					else
					{
						lbltotal.Text ="0.00";
						lblstroke.Text ="0.00";
						lblstatus.Text ="true";
					}

					try
					{
						ArrayList list1 = new ArrayList();
						ArrayList list2=new ArrayList();
						list1=db.SelectContactdetails(lst[6].ToString().Trim(),lst[5].ToString().Trim());
						list2=db.SelectCompanyterms(lst[5].ToString().Trim(),lst[6].ToString().Trim());
						Quotation quot =new Quotation();
						string company="";
						if(list1[16].ToString().Trim() =="0")
						{
							company ="Montreal";
						}
						else
						{
							company ="Mississauga";
						}
						quot.Office= company.Trim();
						quot.Customer=list1[0].ToString();
						quot.Contact=lst[0].ToString().Trim();
						quot.AttnTo1=lst[0].ToString().Trim();
						quot.AttnTo2="P:"+list1[12].ToString()+" F:"+list1[13].ToString();
						quot.AttnTo3=list1[14].ToString();
						quot.BillTo1=list1[0].ToString();
						quot.BillTo2=list1[1].ToString()+", "+list1[2].ToString();
						quot.BillTo3=list1[3].ToString()+", "+list1[4].ToString()+", "+list1[5].ToString();
						quot.ShipTo1=list1[6].ToString();
						quot.ShipTo2=list1[7].ToString()+", "+list1[8].ToString();
						quot.ShipTo3=list1[9].ToString()+", "+list1[10].ToString()+", "+list1[11].ToString();
						quot.QuoteNo=Qno(lst[3].ToString().Trim());
						quot.Quotedate=DateTime.Now.ToShortDateString();
						quot.ExpiryDate=DateTime.Now.AddDays(30).ToShortDateString();;
						quot.PrepairedBy=lst[0].ToString().Trim();
						quot.Code=list2[0].ToString();
						if(list2[4].ToString().Trim()=="0")
						{
							quot.Langu="English";
						}
						else
						{
							quot.Langu="French";
						}
					
						quot.Terms=list2[2].ToString();
						quot.Delivery="TBA";
						if(list2[1].ToString().Trim()=="0")
						{
							quot.Currency="CN $";
						}
						else
						{
							quot.Currency="US $";
						}
						quot.Note="";
						quot.FinishedDate ="  ";
						quot.CowanQno= "  "; 
						quot.Items=quot.QuoteNo.ToString();
						quot.CompanyID=list2[5].ToString();
						quot.SpecSheet="";
						decimal f =120.00m;
						string st="";
						if(Convert.ToDecimal(PN.Stroke.ToString()) > f)
						{
							st="For pricing please consult factory";
							lblstatus.Text="true";
						}
						string strn="";
						for (int t=0;t<PN.Popular_Opt.Count; t++)
						{
							strn +=(PN.Popular_Opt[t].ToString().Trim());
						}
						LblPartNo.Text=PN.PNO.ToString();
						string sto ="";
						if(PN.StopTube !=null)
						{
							if(PN.StopTube.ToString() !="")
							{
								decimal a =0.00m;
								decimal b =0.00m;
								decimal c =0.00m;
								string ss="";
								if(PN.StopTube.Trim().Substring(0,3) =="DST")
								{
									ss=PN.StopTube.Trim().Substring(3);
								}
								else
								{
									ss=PN.StopTube.Trim().Substring(2);
								}
								a =Convert.ToDecimal(ss);
								b= Convert.ToDecimal(PN.Stroke.ToString());
								c= b - a;
								sto =" (Effective Stroke ="+ c.ToString()+"\")";
							}
						}
						string rd=PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim();
						string rodenddim ="";
						if(db.SelectRRD(rd).Trim() !="")
						{
							rodenddim =" KK = "+db.SelectRRD(rd);
						}
						string portsiz="";
					
						string table="";
						if(PN.Series.Trim().Substring(0,1) =="A" ||PN.Series.Trim().Substring(0,1) =="S" )
						{
							table ="WEB_SeriesAPortSize_TableV1";
						}
						else if(PN.Series.Trim().Substring(0,1) =="P" || PN.Series.Trim().Substring(0,1) =="N")
						{
							table ="WEB_SeriesPNPortSize_TableV1";
						}
						else if(PN.Series.Trim().Substring(0,1) =="M" || PN.Series.Trim().Substring(0,1) =="L" || PN.Series.Trim().Substring(0,1) =="R")
						{
							table ="WEB_SeriesMMLLRPortSize_TableV1";
						}
						if(PN.Series.Trim() !="Z")
						{
							if(db.SelectPortSizeNo(PN.Bore_Size.Trim(),PN.Port_Type.Trim(),table.Trim()) !="")
							{
								portsiz ="#"+db.SelectPortSizeNo(PN.Bore_Size.Trim(),PN.Port_Type.Trim(),table.Trim());
							}
						}
						string pmml="";
						string tem="";
						if(PN.Series.ToString().Trim() =="N")
						{
							if(PN.Mount.ToString().Trim() =="F1")
							{
								tem =db.SelectSeriesNPressure(PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim(),"F1");
							}
							else
							{
								tem =db.SelectSeriesNPressure(PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim(),"NonF1");
							}
							if(tem.Trim() !="")
							{
								pmml="( Downrated to "+tem.Trim()+" PSI )";
							}
						}
						else if(PN.Series.ToString().Trim() =="M" || PN.Series.ToString().Trim() =="ML" || PN.Series.ToString().Trim() =="L")
						{
							if(PN.Mount.ToString().Trim() =="F1")
							{								
								tem =db.SelectSeriesMMLPressure("M"+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim(),"F1");
								if(tem.Trim() !="")
								{
									pmml="( Downrated to "+tem.Trim()+" PSI )";
								}
							}
						}
						string series="";
						if(PN.Series.Trim() =="SA")
						{
							series="A";
						}
						else
						{
							series=PN.Series.Trim();
						}
						QItems Qitem =new QItems();
						Qitem.ItemNo =quot.QuoteNo.ToString();
						Qitem.S_Code=series.ToString().Trim();
						Qitem.Series=db.SelectValue("Series_Name","WEB_Series_TableV1","Series_Code",PN.Series.ToString().Trim()).ToString();
						Qitem.S_Price="0.00";
						Qitem.B_Code=PN.Bore_Size.ToString().Trim();
						Qitem.Bore=db.SelectValue("Bore_Size","WEB_Bore_TableV1","Bore_Code",PN.Bore_Size.ToString().Trim()).ToString()+" Bore Size";
						Qitem.B_Price="0.00";
						Qitem.R_Code=PN.Rod_Diamtr.ToString().Trim();
						Qitem.Rod=db.SelectValue("Rod_Size","WEB_RodSerZ_TableV1","Rod_Code",PN.Rod_Diamtr.ToString().Trim())+" Rod Size";
						Qitem.R_Price="0.00";
						Qitem.Stroke_Code=PN.Stroke.ToString().Trim();
						Qitem.Stroke="Stroke = "+PN.Stroke.ToString()+"\""+sto.ToString()+st.ToString();
						Qitem.Stroke_Price="0.00";
						Qitem.M_code=PN.Mount.ToString().Trim();
						Qitem.Mount=db.SelectValue("Mount_Type","WEB_Mount"+PN.Series.Trim()+"_TableV1","Mount_Code",PN.Mount.ToString().Trim()).ToString()+" "+pmml.Trim();
						Qitem.M_Price="0.00";
						Qitem.RE_Code=PN.Rod_End.ToString().Trim();
						Qitem.RodEnd=db.SelectValue("RodEnd_Shape","WEB_RodEndKK_TableV1","RodEnd_Code",PN.Rod_End.ToString().Trim()).ToString()+rodenddim.ToString();
						Qitem.RE_Price="0.00";
						Qitem.Cu_Code=PN.Cushions.ToString().Trim();
						Qitem.Cushion=db.SelectValue("Cushion_type","WEB_Cushion_TableV1","Cushion_Code",PN.Cushions.ToString()).ToString();
						Qitem.Cu_Price="0.00";
						Qitem.CushionPos_Code=PN.CushionPosition.ToString().Trim();
						Qitem.CushionPos=db.SelectValue("CushionPos_Pos","WEB_CushionPos_TableV1","CushionPos_Code",PN.CushionPosition.ToString().Trim()).ToString();
						Qitem.CushionPos_Price="0.00";
						Qitem.Sel_Code=PN.Seal_Comp.ToString().Trim();
						Qitem.Seal=db.SelectValue("Seal_Type","WEB_Seal_TableV1","Seal_Code",PN.Seal_Comp.ToString().Trim()).ToString();
						Qitem.Sel_Price="0.00";
						Qitem.Port_Code=PN.Port_Type.ToString().Trim();
						Qitem.Port=portsiz.Trim()+" "+ db.SelectValue("PortType_Type","WEB_portType_TableV1","PortType_Code",PN.Port_Type.ToString().Trim()).ToString();
						Qitem.Port_Price="0.00";
						Qitem.PP_Code=PN.Port_Pos.ToString().Trim();
						Qitem.PortPos=db.SelectValue("PortPos_Position","WEB_PortPosition_TableV1","PortPos_Code",PN.Port_Pos.ToString().Trim()).ToString();
						Qitem.PP_Price="0.00";
						Qitem.Quantity=TxtQty.Text.ToString();
						Qitem.Discount=lblD.Text;
					
						Qitem.Cusomer_ID=quot.Customer.Trim();
						Qitem.Quotation_No=quot.QuoteNo.Trim();
						Qitem.Q_Date=quot.Quotedate.Trim();
						Qitem.User_ID=lst[0].ToString();
						Qitem.PriceList="0";
						string upload="";
						if(UploadDwg.Value.Trim()!="")
						{
							if(UploadDwg.PostedFile.ContentType.ToString() =="application/pdf")
							{
								upload=UploadFile(sender,e);
								lblstatus.Text ="true";
							}
							else
							{
								LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Please select a PDF file!!!')</script>";
							}
						}
						else if(cbupload.Checked ==true && lblfile.Text.Trim() !="")
						{
							string source=Server.MapPath("./temp_drawing/" + lblfile.Text.Trim());
							string destination=Server.MapPath("./icyl_drawing/" + lblfile.Text.Trim());
							System.IO.File.Copy(source,destination,true);
							upload=lblfile.Text.Trim();
							lblstatus.Text ="true";
						}
						lblfile.Text=upload.ToString();
						Qitem.DWG=upload.ToString();
						if(TxtSpecialReq.Text.Trim() !="")
						{
							Qitem.SpecialReq =TxtSpecialReq.Text;
						}
						else
						{
							Qitem.SpecialReq ="";
						}
						string commadd="";
						if(Qitem.S_Code.Trim() !="")
						{
							if(Qitem.Cusomer_ID.Trim().ToUpper()  =="SLURRYFLO VALVE CORPORATION")
							{
								commadd="WEB_SVCSeries"+Qitem.S_Code.Trim();
							}
							else if(Qitem.Cusomer_ID.Trim().ToUpper()  =="VELAN INC.")
							{
								commadd="WEB_Series"+Qitem.S_Code.Trim()+"_Velan_";
							}
								//issue #644 start
							else if(lst[6].ToString().Trim() =="1030" && (Qitem.S_Code=="PA" || Qitem.S_Code=="PC" ||Qitem.S_Code=="PS" ))
							{
								switch(Qitem.S_Code)
								{
									case "PA":
										commadd="SMCSeries"+Qitem.S_Code.Trim();
										break;
									case "PC":
										commadd="SMCSeries"+Qitem.S_Code.Trim();
										break;
									case "PS":
										commadd="SMCSeries"+Qitem.S_Code.Trim();
										break;
								}

							}
								//issue #644 end
							else
							{
								commadd="WEB_Series"+Qitem.S_Code.Trim();
							}
						}						
						decimal sptotal=0.00m;
						if(PN.CompanyID !=null)
						{
							string sp_pindex="";
							sp_pindex=db.SelectPriceIndex(PN.Series.Trim());
							decimal spindex=0.00m;
							spindex=Convert.ToDecimal(sp_pindex.ToString());

							if(PN.CompanyID.ToString().Trim() =="1004")
							{
								if(PN.Specials !=null)
								{									
									if(PN.Specials.ToString().ToUpper().Trim() =="YES")
									{
										decimal total =0.00m;
										decimal t1=0.00m;
										bool result1=false;
										string sp ="";
										int tt=Convert.ToInt32(db.SelectLastSpNo());
										sp=(tt+1).ToString();
										PN.PNO ="Z"+PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim()+
											PN.Cushions.ToString().Trim()+PN.Mount.ToString().Trim()+"/Z"+sp.ToString();
										LblPartNo.Text ="Z"+PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim()+
											PN.Cushions.ToString().Trim()+PN.Mount.ToString().Trim()+"/Z"+sp.ToString();								
										if(PN.MountKit !=null)
										{
											if(PN.MountKit.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials_Velan(PN.MountKit.Trim(),"WEB_Specials_Velan_Pricing_TableV1",PN.ValveSize.ToString().Trim(),PN.Bore_Size.ToString().Trim());
												total =t1 + (t1 * (spindex /100)); 
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}	
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.MountKit.Trim())+" (Valve Size ="+PN.ValveSize.Trim()+"\")";
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.MountKit.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"1");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(PN.ManualOverride !=null)
										{
											if(PN.ManualOverride.Trim() !="")
											{
												total =0.00m;
												if(total ==0)
												{
													result1 =true;
												}
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.ManualOverride.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.ManualOverride.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"2");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(PN.LimitSwitch !=null)
										{
											if(PN.LimitSwitch.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials_Velan(PN.LimitSwitch.Trim() ,"WEB_Specials_Velan_Pricing_TableV1",PN.ValveSize.ToString().Trim(),PN.Bore_Size.ToString().Trim());
												total =t1 + (t1 * (spindex /100)); 
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}	
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.LimitSwitch.Trim());
												if(PN.SpecialLimitSwitch.Trim() !="")
												{
													des=des.Replace("#", PN.SpecialLimitSwitch.Trim());
												}
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.LimitSwitch.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"2");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(PN.FilterRegulator !=null)
										{
											if(PN.FilterRegulator.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials_Velan(PN.FilterRegulator.Trim(),"WEB_Specials_Velan_Pricing_TableV1",PN.ValveSize.ToString().Trim(),PN.Bore_Size.ToString().Trim());
												total =t1 + (t1 * (spindex /100)); 
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.FilterRegulator.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.FilterRegulator.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"3");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(PN.Solenoid !=null)
										{
											if(PN.Solenoid.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials_Velan(PN.Solenoid.Trim().Replace("F",""),"WEB_Specials_Velan_Pricing_TableV1",PN.ValveSize.ToString().Trim(),PN.Bore_Size.ToString().Trim());
												total =t1 + (t1 * (spindex /100)); 
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}
												string ss="";
												if(PN.SolenoidClass.ToString().Trim() !="")
												{
													ss=" (Classification: "+PN.SolenoidClass.ToString().Trim()+")";
												}
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Solenoid.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Solenoid.Trim(),des.Trim()+ss.Trim(),total.ToString(),"1","0",total.ToString(),"4");
												db.InsertSpecialCount(sp1.Trim());						
											}
										}
										if(PN.FlowControl !=null)
										{
											if(PN.FlowControl.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials_Velan(PN.FlowControl.Trim(),"WEB_Specials_Velan_Pricing_TableV1",PN.ValveSize.ToString().Trim(),PN.Bore_Size.ToString().Trim());
												total =t1 + (t1 * (spindex /100)); 
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.FlowControl.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.FlowControl.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"5");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(PN.Tubing !=null)
										{
											if(PN.Tubing.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials_Velan(PN.Tubing.Trim(),"WEB_Specials_Velan_Pricing_TableV1",PN.ValveSize.ToString().Trim(),PN.Bore_Size.ToString().Trim());
												total =t1 + (t1 * (spindex /100)); 
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}	
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Tubing.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Tubing.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"6");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(result1 ==true)
										{
											lblstatus.Text= "true";
										}
									}
									else if(PN.Specials.ToString().ToUpper().Trim() =="SPECIAL")
									{
										decimal total =0.00m;
										decimal t1=0.00m;
										bool result1=false;
										string sp ="";
										int tt=Convert.ToInt32(db.SelectLastSpNo());
										sp=(tt+1).ToString();
										PN.PNO ="Z"+PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim()+
											PN.Cushions.ToString().Trim()+PN.Mount.ToString().Trim()+"/Z"+sp.ToString();
										LblPartNo.Text ="Z"+PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim()+
											PN.Cushions.ToString().Trim()+PN.Mount.ToString().Trim()+"/Z"+sp.ToString();
										if(PN.Style !=null)
										{
											if(PN.Style.Trim() !="")
											{
												if(PN.Style.Trim() =="FC" || PN.Style.Trim() =="FO")
												{
													total =0.00m;
													if(total ==0)
													{
														result1 =true;
													}
													string des="";
													des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Style.Trim()).Replace("#",PN.MinAirSupply.Trim());
													string sp1 ="";
													int tt1=Convert.ToInt32(db.SelectLastSpNo());
													sp1=(tt1+1).ToString();
													db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Style.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"1");
													db.InsertSpecialCount(sp1.Trim());
												}
											}
										}
										if(PN.MountKit !=null)
										{
											if(PN.MountKit.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials_Velan(PN.MountKit.Trim(),"WEB_Specials_Velan_Pricing_TableV1",PN.ValveSize.ToString().Trim(),PN.Bore_Size.ToString().Trim());
												total =t1 + (t1 * (spindex /100)); 
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}	
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.MountKit.Trim())+" (Valve Size ="+PN.ValveSize.Trim()+"\")";
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.MountKit.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"2");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(PN.ManualOverride !=null)
										{
											if(PN.ManualOverride.Trim() !="")
											{
												total =0.00m;
												if(total ==0)
												{
													result1 =true;
												}
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.ManualOverride.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.ManualOverride.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"3");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(PN.LimitSwitch !=null)
										{
											if(PN.LimitSwitch.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials_Velan(PN.LimitSwitch.Trim() ,"WEB_Specials_Velan_Pricing_TableV1",PN.ValveSize.ToString().Trim(),PN.Bore_Size.ToString().Trim());
												total =t1 + (t1 * (spindex /100)); 
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}	
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.LimitSwitch.Trim());
												if(PN.SpecialLimitSwitch.Trim() !="")
												{
													des=des.Replace("#", PN.SpecialLimitSwitch.Trim());
												}
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.LimitSwitch.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"4");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(PN.FilterRegulator !=null)
										{
											if(PN.FilterRegulator.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials_Velan(PN.FilterRegulator.Trim(),"WEB_Specials_Velan_Pricing_TableV1",PN.ValveSize.ToString().Trim(),PN.Bore_Size.ToString().Trim());
												total =t1 + (t1 * (spindex /100)); 
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.FilterRegulator.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.FilterRegulator.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"5");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(PN.Solenoid !=null)
										{
											if(PN.Solenoid.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials_Velan(PN.Solenoid.Trim().Replace("F",""),"WEB_Specials_Velan_Pricing_TableV1",PN.ValveSize.ToString().Trim(),PN.Bore_Size.ToString().Trim());
												total =t1 + (t1 * (spindex /100)); 
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}
												string ss="";
												if(PN.SolenoidClass.ToString().Trim() !="")
												{
													ss=" (Classification: "+PN.SolenoidClass.ToString().Trim()+")";
												}
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Solenoid.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Solenoid.Trim(),des.Trim()+ss.Trim(),total.ToString(),"1","0",total.ToString(),"6");
												db.InsertSpecialCount(sp1.Trim());						
											}
										}
										if(PN.FlowControl !=null)
										{
											if(PN.FlowControl.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials_Velan(PN.FlowControl.Trim(),"WEB_Specials_Velan_Pricing_TableV1",PN.ValveSize.ToString().Trim(),PN.Bore_Size.ToString().Trim());
												total =t1 + (t1 * (spindex /100));  
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.FlowControl.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.FlowControl.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"7");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(PN.Tubing !=null)
										{
											if(PN.Tubing.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials_Velan(PN.Tubing.Trim(),"WEB_Specials_Velan_Pricing_TableV1",PN.ValveSize.ToString().Trim(),PN.Bore_Size.ToString().Trim());
												total =t1 + (t1 * (spindex /100));  
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}	
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Tubing.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Tubing.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"8");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(result1 ==true)
										{
											lblstatus.Text= "true";
										}
									}
									else
									{
										Qitem.Special_ID="";
									}
								}
								else
								{
									Qitem.Special_ID="";
								}
							}
							else if(PN.CompanyID.ToString().Trim() =="1003")
							{
								if(PN.Specials !=null)
								{
									if(PN.Specials.ToString().ToUpper().Trim() =="YES")
									{
										decimal total =0.00m;
										decimal t1=0.00m;
										bool result1=false;
										string sp ="";
										int tt=Convert.ToInt32(db.SelectLastSpNo());
										sp=(tt+1).ToString();
										PN.PNO ="Z"+PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim()+
											PN.Cushions.ToString().Trim()+PN.Mount.ToString().Trim()+"/Z"+sp.ToString();
										LblPartNo.Text ="Z"+PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim()+
											PN.Cushions.ToString().Trim()+PN.Mount.ToString().Trim()+"/Z"+sp.ToString();
										if(PN.Style !=null)
										{
											if(PN.Style.Trim() !="")
											{
												if(PN.Style.Trim() =="FC" || PN.Style.Trim() =="FO")
												{
													total =0.00m;
													if(total ==0)
													{
														result1 =true;
													}
													string des="";
													des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Style.Trim()).Replace("#",PN.MinAirSupply.Trim());
													string sp1 ="";
													int tt1=Convert.ToInt32(db.SelectLastSpNo());
													sp1=(tt1+1).ToString();
													db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Style.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"1");
													db.InsertSpecialCount(sp1.Trim());
												}
											}
										}
										if(PN.ManualOverride !=null)
										{
											if(PN.ManualOverride.Trim() !="")
											{
												total =0.00m;
												if(total ==0)
												{
													result1 =true;
												}
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.ManualOverride.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.ManualOverride.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"2");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(PN.ReedSwitch !=null)
										{
											if(PN.ReedSwitch.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials("RS","WEB_Specials_EV_Pricing_TableV1",PN.ValveSize.ToString().Trim());
												total =t1 + (t1 * (spindex /100)); 
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}	
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.ReedSwitch.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.ReedSwitch.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"3");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(PN.Positioner !=null)
										{
											if(PN.Positioner.Trim() !="")
											{
												total =0.00m;
												decimal dcstrk=0.00m;
												dcstrk=Convert.ToDecimal(PN.Stroke.ToString().Trim());
												if(dcstrk <= 6.25m)
												{
													if(PN.Positioner.ToString().Trim() =="PMVEP5")
													{
														t1 =db.SelectAddersPrice_Specials("PMVEP5","WEB_Specials_EV_Pricing_TableV1",PN.ValveSize.ToString().Trim());
													}
												}
												total =t1 + (t1 * (spindex /100)); 
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}	
												string pp="";
												if(PN.PositionerPno.ToString().Trim() !="")
												{
													pp=" (Part# "+PN.PositionerPno.ToString().Trim()+")";
												}
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Positioner.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.PositionerPno.ToString().Trim(),des.Trim()+pp.Trim(),total.ToString(),"1","0",total.ToString(),"4");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(PN.FilterRegulator !=null)
										{
											if(PN.FilterRegulator.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials(PN.FilterRegulator.Trim(),"WEB_Specials_EV_Pricing_TableV1",PN.ValveSize.ToString().Trim());
												total =t1 + (t1 * (spindex /100)); 
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.FilterRegulator.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.FilterRegulator.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"5");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(PN.Solenoid !=null)
										{
											if(PN.Solenoid.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials(PN.Solenoid.Trim().Replace("F",""),"WEB_Specials_EV_Pricing_TableV1",PN.ValveSize.ToString().Trim());
												total =t1 + (t1 * (spindex /100)); 
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}
												string ss="";
												if(PN.SolenoidClass.ToString().Trim() !="")
												{
													ss=" (Classification: "+PN.SolenoidClass.ToString().Trim()+")";
												}
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Solenoid.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Solenoid.Trim(),des.Trim()+ss.Trim(),total.ToString(),"1","0",total.ToString(),"6");
												db.InsertSpecialCount(sp1.Trim());						
											}
										}
										if(PN.FlowControl !=null)
										{
											if(PN.FlowControl.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials(PN.FlowControl.Trim(),"WEB_Specials_EV_Pricing_TableV1",PN.ValveSize.ToString().Trim());
												total =t1 + (t1 * (spindex /100)); 
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.FlowControl.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.FlowControl.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"7");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(PN.Tubing !=null)
										{
											if(PN.Tubing.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials(PN.Tubing.Trim(),"WEB_Specials_EV_Pricing_TableV1",PN.ValveSize.ToString().Trim());
												total =t1 + (t1 * (spindex /100));  
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}	
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Tubing.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Tubing.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"8");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(result1 ==true)
										{
											lblstatus.Text= "true";
										}
									}
									else
									{
										Qitem.Special_ID="";
									}
								}
							}
							if(PN.CompanyID.ToString().Trim() =="1017")
							{
								if(PN.Specials !=null)
								{									
									if(PN.Specials.ToString().ToUpper().Trim() =="YES")
									{
										decimal total =0.00m;
										decimal t1=0.00m;
										bool result1=false;
										string sp ="";
										int tt=Convert.ToInt32(db.SelectLastSpNo());
										sp=(tt+1).ToString();
										PN.PNO ="Z"+PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim()+
											PN.Cushions.ToString().Trim()+PN.Mount.ToString().Trim()+"/Z"+sp.ToString();
										LblPartNo.Text ="Z"+PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim()+
											PN.Cushions.ToString().Trim()+PN.Mount.ToString().Trim()+"/Z"+sp.ToString();								
										if(PN.MountKit !=null)
										{
											if(PN.MountKit.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials_Velan(PN.MountKit.Trim(),"WEB_Specials_Velan_Pricing_TableV1",PN.ValveSize.ToString().Trim(),PN.Bore_Size.ToString().Trim());
												total =t1 + (t1 * (spindex /100)); 
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}	
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.MountKit.Trim())+" (Valve Size ="+PN.ValveSize.Trim()+"\")";
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.MountKit.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"1");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(PN.ManualOverride !=null)
										{
											if(PN.ManualOverride.Trim() !="")
											{
												total =0.00m;
												if(total ==0)
												{
													result1 =true;
												}
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.ManualOverride.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.ManualOverride.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"2");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(PN.LimitSwitch !=null)
										{
											if(PN.LimitSwitch.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials_Velan(PN.LimitSwitch.Trim() ,"WEB_Specials_Velan_Pricing_TableV1",PN.ValveSize.ToString().Trim(),PN.Bore_Size.ToString().Trim());
												total =t1 + (t1 * (spindex /100)); 
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}	
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.LimitSwitch.Trim());
												if(PN.SpecialLimitSwitch.Trim() !="")
												{
													des=des.Replace("#", PN.SpecialLimitSwitch.Trim());
												}
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.LimitSwitch.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"2");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(PN.FilterRegulator !=null)
										{
											if(PN.FilterRegulator.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials_Velan(PN.FilterRegulator.Trim(),"WEB_Specials_Velan_Pricing_TableV1",PN.ValveSize.ToString().Trim(),PN.Bore_Size.ToString().Trim());
												total =t1 + (t1 * (spindex /100)); 
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.FilterRegulator.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.FilterRegulator.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"3");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(PN.Solenoid !=null)
										{
											if(PN.Solenoid.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials_Velan(PN.Solenoid.Trim().Replace("F",""),"WEB_Specials_Velan_Pricing_TableV1",PN.ValveSize.ToString().Trim(),PN.Bore_Size.ToString().Trim());
												total =t1 + (t1 * (spindex /100)); 
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}
												string ss="";
												if(PN.SolenoidClass.ToString().Trim() !="")
												{
													ss=" (Classification: "+PN.SolenoidClass.ToString().Trim()+")";
												}
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Solenoid.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Solenoid.Trim(),des.Trim()+ss.Trim(),total.ToString(),"1","0",total.ToString(),"4");
												db.InsertSpecialCount(sp1.Trim());						
											}
										}
										if(PN.FlowControl !=null)
										{
											if(PN.FlowControl.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials_Velan(PN.FlowControl.Trim(),"WEB_Specials_Velan_Pricing_TableV1",PN.ValveSize.ToString().Trim(),PN.Bore_Size.ToString().Trim());
												total =t1 + (t1 * (spindex /100)); 
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.FlowControl.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.FlowControl.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"5");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(PN.Tubing !=null)
										{
											if(PN.Tubing.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials_Velan(PN.Tubing.Trim(),"WEB_Specials_Velan_Pricing_TableV1",PN.ValveSize.ToString().Trim(),PN.Bore_Size.ToString().Trim());
												total =t1 + (t1 * (spindex /100)); 
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}	
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Tubing.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Tubing.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"6");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(result1 ==true)
										{
											lblstatus.Text= "true";
										}
									}
									else if(PN.Specials.ToString().ToUpper().Trim() =="SPECIAL")
									{
										decimal total =0.00m;
										decimal t1=0.00m;
										bool result1=false;
										string sp ="";
										int tt=Convert.ToInt32(db.SelectLastSpNo());
										sp=(tt+1).ToString();
										PN.PNO ="Z"+PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim()+
											PN.Cushions.ToString().Trim()+PN.Mount.ToString().Trim()+"/Z"+sp.ToString();
										LblPartNo.Text ="Z"+PN.Series.ToString().Trim()+PN.Bore_Size.ToString().Trim()+PN.Rod_Diamtr.ToString().Trim()+PN.Rod_End.ToString().Trim()+
											PN.Cushions.ToString().Trim()+PN.Mount.ToString().Trim()+"/Z"+sp.ToString();
										if(PN.Style !=null)
										{
											if(PN.Style.Trim() !="")
											{
												if(PN.Style.Trim() =="FC" || PN.Style.Trim() =="FO")
												{
													total =0.00m;
													if(total ==0)
													{
														result1 =true;
													}
													string des="";
													des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Style.Trim()).Replace("#",PN.MinAirSupply.Trim());
													string sp1 ="";
													int tt1=Convert.ToInt32(db.SelectLastSpNo());
													sp1=(tt1+1).ToString();
													db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Style.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"1");
													db.InsertSpecialCount(sp1.Trim());
												}
											}
										}
										if(PN.MountKit !=null)
										{
											if(PN.MountKit.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials_Velan(PN.MountKit.Trim(),"WEB_Specials_Velan_Pricing_TableV1",PN.ValveSize.ToString().Trim(),PN.Bore_Size.ToString().Trim());
												total =t1 + (t1 * (spindex /100)); 
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}	
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.MountKit.Trim())+" (Valve Size ="+PN.ValveSize.Trim()+"\")";
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.MountKit.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"2");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(PN.ManualOverride !=null)
										{
											if(PN.ManualOverride.Trim() !="")
											{
												total =0.00m;
												if(total ==0)
												{
													result1 =true;
												}
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.ManualOverride.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.ManualOverride.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"3");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(PN.LimitSwitch !=null)
										{
											if(PN.LimitSwitch.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials_Velan(PN.LimitSwitch.Trim() ,"WEB_Specials_Velan_Pricing_TableV1",PN.ValveSize.ToString().Trim(),PN.Bore_Size.ToString().Trim());
												total =t1 + (t1 * (spindex /100)); 
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}	
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.LimitSwitch.Trim());
												if(PN.SpecialLimitSwitch.Trim() !="")
												{
													des=des.Replace("#", PN.SpecialLimitSwitch.Trim());
												}
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.LimitSwitch.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"4");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(PN.FilterRegulator !=null)
										{
											if(PN.FilterRegulator.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials_Velan(PN.FilterRegulator.Trim(),"WEB_Specials_Velan_Pricing_TableV1",PN.ValveSize.ToString().Trim(),PN.Bore_Size.ToString().Trim());
												total =t1 + (t1 * (spindex /100)); 
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.FilterRegulator.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.FilterRegulator.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"5");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(PN.Solenoid !=null)
										{
											if(PN.Solenoid.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials_Velan(PN.Solenoid.Trim().Replace("F",""),"WEB_Specials_Velan_Pricing_TableV1",PN.ValveSize.ToString().Trim(),PN.Bore_Size.ToString().Trim());
												total =t1 + (t1 * (spindex /100)); 
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}
												string ss="";
												if(PN.SolenoidClass.ToString().Trim() !="")
												{
													ss=" (Classification: "+PN.SolenoidClass.ToString().Trim()+")";
												}
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Solenoid.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Solenoid.Trim(),des.Trim()+ss.Trim(),total.ToString(),"1","0",total.ToString(),"6");
												db.InsertSpecialCount(sp1.Trim());						
											}
										}
										if(PN.FlowControl !=null)
										{
											if(PN.FlowControl.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials_Velan(PN.FlowControl.Trim(),"WEB_Specials_Velan_Pricing_TableV1",PN.ValveSize.ToString().Trim(),PN.Bore_Size.ToString().Trim());
												total =t1 + (t1 * (spindex /100));  
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.FlowControl.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.FlowControl.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"7");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(PN.Tubing !=null)
										{
											if(PN.Tubing.Trim() !="")
											{
												t1 =db.SelectAddersPrice_Specials_Velan(PN.Tubing.Trim(),"WEB_Specials_Velan_Pricing_TableV1",PN.ValveSize.ToString().Trim(),PN.Bore_Size.ToString().Trim());
												total =t1 + (t1 * (spindex /100));  
												sptotal +=total;
												total=Decimal.Round(total,2);
												if(total ==0)
												{
													result1 =true;
												}	
												string des="";
												des=db.SelectValue("Description","WEB_Specials_Std_TableV1","Code",PN.Tubing.Trim());
												string sp1 ="";
												int tt1=Convert.ToInt32(db.SelectLastSpNo());
												sp1=(tt1+1).ToString();
												db.InsertCustomerSpecials(sp1.Trim(),quot.QuoteNo.Trim(),LblPartNo.Text.Trim(),PN.Tubing.Trim(),des.Trim(),total.ToString(),"1","0",total.ToString(),"8");
												db.InsertSpecialCount(sp1.Trim());
											}
										}
										if(result1 ==true)
										{
											lblstatus.Text= "true";
										}
									}
									else
									{
										Qitem.Special_ID="";
									}
								}
								else
								{
									Qitem.Special_ID="";
								}
							}
						}						
						decimal appoptprice=0.00m;
						if(PN.Application_Opt.Count >0)
						{
							for(int cont=0;cont < PN.Application_Opt.Count; cont++)
							{
								bool price=false;
								decimal app=0.00m;
								if(PN.Application_Opt[cont].ToString().Trim() =="G1")
								{
									app =db.SelectAddersPrice("G1",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									if(app ==0)
									{
										price =true;
									}
								}
								else if(PN.Application_Opt[cont].ToString().Trim() =="G2")
								{
									app =db.SelectAddersPrice("G2",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									if(app ==0)
									{
										price =true;
									}
								}
								else if(PN.Application_Opt[cont].ToString().Trim() =="G3")
								{
									app =db.SelectAddersPrice("G3",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									if(app ==0)
									{
										price =true;
									}
								}
								else if(PN.Application_Opt[cont].ToString().Trim() =="G4")
								{
									app =db.SelectAddersPrice("G4",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									if(app ==0)
									{
										price =true;
									}
								}
								else if(PN.Application_Opt[cont].ToString().Trim() =="G5")
								{
									app =db.SelectAddersPrice("G5",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									if(app ==0)
									{
										price =true;
									}
								}
								else if(PN.Application_Opt[cont].ToString().Trim() =="G6")
								{
									app =db.SelectAddersPrice("G6",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									if(app ==0)
									{
										price =true;
									}
								}
								else if(PN.Application_Opt[cont].ToString().Trim() =="G7")
								{
									app =db.SelectAddersPrice("G7",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									if(app ==0)
									{
										price =true;
									}
								}
								else if(PN.Application_Opt[cont].ToString().Trim() =="GR1")
								{
								
									app =db.SelectAddersPrice("GR1",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									if(app ==0)
									{
										price =true;
									}
								
								}
								else if(PN.Application_Opt[cont].ToString().Trim() =="GR2")
								{
									if(PN.Seal_Comp =="F")
									{
										app =db.SelectAddersPrice("FGR2",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
										if(app ==0)
										{
											price =true;
										}
									}
									else
									{	
										app =db.SelectAddersPrice("GR2",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
										if(app ==0)
										{
											price =true;
										}
									}
								}
								else if(PN.Application_Opt[cont].ToString().Trim() =="GT2")
								{
								
									app =db.SelectAddersPrice("GT2",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									if(app ==0)
									{
										price =true;
									}
								
								}
								//issue #104 start
								else if(PN.Application_Opt[cont].ToString().Trim() =="GT3")
								{
								
									app =db.SelectAddersPrice("GT3",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									if(app ==0)
									{
										price =true;
									}
								
								}
								//issue #104 end
								else if(PN.Application_Opt[cont].ToString().Trim() =="P1")
								{
									app =db.SelectAddersPrice("P1",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									if(app ==0)
									{
										price =true;
									}
								}
								else if(PN.Application_Opt[cont].ToString().Trim() =="P2")
								{
									app =db.SelectAddersPrice("P2",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									if(app ==0)
									{
										price =true;
									}
								}
								else if(PN.Application_Opt[cont].ToString().Trim() =="P3")
								{
									app =db.SelectAddersPrice("P3",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									if(app ==0)
									{
										price =true;
									}
								}
								else if(PN.Application_Opt[cont].ToString().Trim() =="P4")
								{
									app =db.SelectAddersPrice("P4",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									if(app ==0)
									{
										price =true;
									}
								}
								else if(PN.Application_Opt[cont].ToString().Trim() =="S1")
								{
									app =db.SelectAddersPrice("S1",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									if(app ==0)
									{
										price =true;
									}
								}
							
								else if(PN.Application_Opt[cont].ToString().Trim() =="S2")
								{
									app =db.SelectAddersPrice("S2",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									if(app ==0)
									{
										price =true;
									}
								}
								else if(PN.Application_Opt[cont].ToString().Trim() =="S3")
								{
									app =db.SelectAddersPrice("S3",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									if(app ==0)
									{
										price =true;
									}
								}
								else if(PN.Application_Opt[cont].ToString().Trim() =="S4")
								{
									app =db.SelectAddersPrice("S4",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									if(app ==0)
									{
										price =true;
									}
								}
								else if(PN.Application_Opt[cont].ToString().Trim() =="S5")
								{
									app =db.SelectAddersPrice("S5",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									if(app ==0)
									{
										price =true;
									}
								}
								else if(PN.Application_Opt[cont].ToString().Substring(0,2) =="RM" || PN.Application_Opt[cont].ToString().Substring(0,2) =="RX" || PN.Application_Opt[cont].ToString().Substring(0,2) =="RY")
								{							
								
									app =db.SelectAddersPrice("MetricRodEnd",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									if(app ==0)
									{
										price =true;
									}
								}
								else if(PN.Application_Opt[cont].ToString().Trim() =="W1")
								{
									decimal t1,t2,t3=0.00m;
									t1 =db.SelectAddersPrice("M3Base",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									t2 =db.SelectAddersPrice("M3PerInch",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									t3 = Convert.ToDecimal(PN.Stroke.Trim());
									app= (t1 + (t2 * t3)+ 275);
									if(app ==0  || app ==275)
									{
										price =true;
									}
								}
								//issue #90 start
								else if(PN.Application_Opt[cont].ToString().Trim() =="CS")
								{							
									//app= 391m;
									app =db.SelectAddersPrice_ByBore("CS",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim());
									if(app ==0  )
									{
										price =true;
									}
								}
								//issue #90 end
								if(app ==0 && price ==true)
								{
									lblstatus.Text = "true";
									
								}	
								appoptprice +=app;
							}
							ArrayList temp1 =new ArrayList();
							for(int p=0;p< PN.Application_Opt.Count;p++)
							{
								temp1.Add(db.SelectAppopts(PN.Application_Opt[p].ToString().Trim()));
							}
							for(int q=0;q< temp1.Count;q++)
							{
								db.InsertItemApplicationOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.Application_Opt[q].ToString(),temp1[q].ToString(),"0.00");
							}
							Qitem.ApplicationOpt_Id=quot.QuoteNo.Trim()+LblPartNo.Text.Trim();
						}
						else
						{
							Qitem.ApplicationOpt_Id="";
						}
						decimal pop=0.00m;
						if(PN.Popular_Opt.Count >0)
						{
							for(int cont=0;cont < PN.Popular_Opt.Count; cont++)
							{
								decimal total =0.00m;
								decimal t1,t2,t3 =0.00m;
								bool result1=false;
								if(PN.Popular_Opt[cont].ToString().Substring(0,1) =="A")
								{
									if(PN.Popular_Opt[cont].ToString().Substring(0,2) =="AD")
									{
										t1 =db.SelectAddersPrice("A",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
										t2=Convert.ToDecimal(PN.Popular_Opt[cont].ToString().Substring(2).ToString());
										total = t1 * t2;
										if(total ==0)
										{
											result1 =true;
										}
									}
									else
									{
										t1 =db.SelectAddersPrice("A",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
										t2 =Convert.ToDecimal(PN.Popular_Opt[cont].ToString().Substring(1).ToString());
										total = t1 * t2;
										if(total ==0)
										{
											result1 =true;
										}
									}
								}
								else if(PN.Popular_Opt[cont].ToString().Substring(0,1) =="W" && PN.Popular_Opt[cont].ToString().Length  > 2 )
								{
									if(PN.Popular_Opt[cont].ToString().Substring(0,2) =="WD")
									{
										t1 =db.SelectAddersPrice("W",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
										t2 =Convert.ToDecimal(PN.Popular_Opt[cont].ToString().Substring(2).ToString());
										total = t1 * t2;
										if(total ==0)
										{
											result1 =true;
										}
									}
									else
									{
										t1=db.SelectAddersPrice("W",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
										t2=Convert.ToDecimal(PN.Popular_Opt[cont].ToString().Substring(1).ToString());
										total = t1 * t2;
										if(total ==0)
										{
											result1 =true;
										}
									}
								}
								else if(PN.Popular_Opt[cont].ToString().Substring(0,2) =="BB" && PN.Popular_Opt[cont].ToString().Length  > 3 )
								{
									if(PN.Popular_Opt[cont].ToString().Substring(0,3) =="BBC")
									{								
										t1=db.SelectAddersPrice("BBBase",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
										t2=db.SelectAddersPrice("BBPerInch",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
										t3=Convert.ToDecimal(PN.Popular_Opt[cont].ToString().Substring(3));
										total = t1 +( t2 * t3);
										if(total ==0)
										{
											result1 =true;
										}
									}
									else
									{
										t1=db.SelectAddersPrice("BBBase",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
										t2=db.SelectAddersPrice("BBPerInch",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
										t3=Convert.ToDecimal(PN.Popular_Opt[cont].ToString().Substring(3));
										total = t1 +( t2 * t3);
										if(total ==0)
										{
											result1 =true;
										}
									}
								
								}
								else if(PN.Popular_Opt[cont].ToString().Substring(0,1) =="S")
								{
									PN.StopTube =PN.Popular_Opt[cont].ToString();
									t1=db.SelectAddersPrice("STBase",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									t2=db.SelectAddersPrice("STPerInch",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									t3=Convert.ToDecimal(PN.Popular_Opt[cont].ToString().Substring(2).ToString());
									total = t1 +( t2 * t3);
									if(total ==0)
									{
										result1 =true;
									}
								}
								else if(PN.Popular_Opt[cont].ToString().Substring(0,2) =="DS")
								{
									PN.StopTube =PN.Popular_Opt[cont].ToString();
									t1 =db.SelectAddersPrice("DSTBase",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
									total =t1;
									if(total ==0)
									{
										result1 =true;
									}
								}
								else if(PN.Popular_Opt[cont].ToString().Trim().Substring(0,1) =="T" && PN.Popular_Opt[cont].ToString().Trim().Substring(0,2) !="TC" && PN.Popular_Opt[cont].ToString().Trim().Substring(0,2) !="TB")
								{
									string companyid="";
									if(PN.CompanyID !=null)
									{
										if(PN.CompanyID.ToString().Trim() !="")
										{
											companyid=PN.CompanyID.ToString().Trim();
										}
									}
									if(companyid.ToString().Trim() =="1003")
									{
										total=0;						
										if(total ==0)
										{
											result1 =true;
										}
									}
									else
									{										
										if(PN.Popular_Opt[cont].ToString().Length >2)
										{
											if(PN.Popular_Opt[cont].ToString().Trim().Substring(2,1) =="S")
											{
												decimal ts1=0.00m;
												decimal ts2=0.00m;
												decimal ts3=0.00m;
												decimal ts4=0.00m;
												decimal strk=0.00m;
												decimal res=0.00m;
												strk=Convert.ToDecimal(PN.Stroke.Trim());
												ts1=db.SelectAddersPrice(PN.Popular_Opt[cont].ToString().Substring(0,3),commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												if(PN.Popular_Opt[cont].ToString().Substring(0,3) =="T1S" ||PN.Popular_Opt[cont].ToString().Substring(0,2) =="T1")
												{
													ts2=db.SelectAddersPrice("T1Adder",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												}
												else
												{
													ts2=db.SelectAddersPrice("TAdder",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												}
												if(PN.Popular_Opt[cont].ToString().Substring(2,1) =="S")
												{
													ts3=db.SelectAddersPrice("TSBase",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													ts4=db.SelectAddersPrice("TSPerInch",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
													res=ts3 + (ts4 * strk);
												}
												total =ts1 + ts2 +res;

											}
											else if(PN.Popular_Opt[cont].ToString().Trim().Substring(2,1) =="R")
											{
									
												decimal ts1=0.00m;
												decimal ts2=0.00m;
												decimal strk=0.00m;
												decimal res=0.00m;
												strk=Convert.ToDecimal(PN.Stroke.Trim());
												ts1=db.SelectAddersPrice(PN.Popular_Opt[cont].ToString().Substring(0,2),commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												if(PN.Popular_Opt[cont].ToString().Substring(0,3) =="T1R" ||PN.Popular_Opt[cont].ToString().Substring(0,2) =="T1")
												{
													ts2=db.SelectAddersPrice("T1Adder",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												}
												else
												{
													ts2=db.SelectAddersPrice("TAdder",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												}
												res=144.35m;
												total =ts1 + ts2 - res;
											}
											else if(PN.Popular_Opt[cont].ToString().Trim().Substring(1,1) =="1" || PN.Popular_Opt[cont].ToString().Trim().Substring(1,1) =="2"
												||PN.Popular_Opt[cont].ToString().ToString().Trim().Substring(1,1) =="3" || PN.Popular_Opt[cont].ToString().Trim().Substring(1,1) =="4"
												|| PN.Popular_Opt[cont].ToString().Trim().Substring(1,1) =="5")
											{
												decimal ts1=0.00m;
												decimal ts2=0.00m;
												ts1=db.SelectAddersPrice(PN.Popular_Opt[cont].ToString().Substring(0,2),commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												if(PN.Popular_Opt[cont].ToString().Substring(0,2) =="T1")
												{
													ts2=db.SelectAddersPrice("T1Adder",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												}
												else
												{
													ts2=db.SelectAddersPrice("TAdder",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
												}
												total =ts1 + ts2;

											}
										}
										else
										{
											decimal ts1=0.00m;
											decimal ts2=0.00m;
											ts1=db.SelectAddersPrice(PN.Popular_Opt[cont].ToString().Substring(0,2),commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
											if(PN.Popular_Opt[cont].ToString().Substring(0,2) =="T1")
											{
												ts2=db.SelectAddersPrice("T1Adder",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
											}
											else
											{
												ts2=db.SelectAddersPrice("TAdder",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
											}
											total =ts1 + ts2;
										}
									}
									if(total ==0)
									{
										result1 =true;
									}
								}
								else
								{
									if(PN.Popular_Opt[cont].ToString() =="M1")
									{
										t1 =db.SelectAddersPrice("M1Base",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
										t2 =db.SelectAddersPrice("M1PerInch",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
										t3 =Convert.ToDecimal(PN.Stroke.Trim());
										total =-(t1 + (t2 * t3)); 
										if(total ==0)
										{
											result1 =true;
										}
									}
									else if(PN.Popular_Opt[cont].ToString() =="M2")
									{
										t1 =db.SelectAddersPrice("M2Base",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
										t2 =db.SelectAddersPrice("M2PerInch",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
										t3 =Convert.ToDecimal(PN.Stroke.Trim());
										total =t1 + (t2 * t3); 
										if(total ==0)
										{
											result1 =true;
										}
									}
									else if(PN.Popular_Opt[cont].ToString() =="M3")
									{
										t1 =db.SelectAddersPrice("M3Base",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
										t2 =db.SelectAddersPrice("M3PerInch",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
										t3 =Convert.ToDecimal(PN.Stroke.Trim());
										total =t1 + (t2 * t3); 
										if(total ==0)
										{
											result1 =true;
										}
									}
									else if(PN.Popular_Opt[cont].ToString() =="M4")
									{
										t1 =db.SelectAddersPrice("M4Base",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
										t2 =db.SelectAddersPrice("M4PerInch",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
										t3 =Convert.ToDecimal(PN.Stroke.Trim());
										total =t1 + (t2 * t3); 
										if(total ==0)
										{
											result1 =true;
										}
									}
									else if(PN.Popular_Opt[cont].ToString() =="M5")
									{
										t1 =db.SelectAddersPrice("M5Base",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
										t2 =db.SelectAddersPrice("M5PerInch",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
										t3 =Convert.ToDecimal(PN.Stroke.Trim());
										total =t1 + (t2 * t3); 
										if(total ==0)
										{
											result1 =true;
										}
									}
									else if(PN.Popular_Opt[cont].ToString() =="M7")
									{
										t1 =db.SelectAddersPrice("M7Base",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
										t2 =db.SelectAddersPrice("M7PerInch",commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
										t3 =Convert.ToDecimal(PN.Stroke.Trim());
										total =t1 + (t2 * t3); 
										if(total ==0)
										{
											result1 =true;
										}
									}
									else if(PN.Popular_Opt[cont].ToString().Trim()=="TB")
									{
										decimal transducer=0;
										if(PN.Series.Trim()=="L" || PN.Series.Trim()=="ML")
										{										
											decimal tbbase=0;
											decimal tbstroke=0;
											tbbase =db.SelectAddersPrice("TB_Base","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
											tbstroke=db.SelectAddersPrice_Transducer("TB_Series"+PN.Series.Trim(),PN.Stroke.ToString().Trim());
											transducer=tbbase +tbstroke;
											total=transducer;
										}
										if(total ==0)
										{
											result1 =true;
										}
									}
									else if(PN.Popular_Opt[cont].ToString().Substring(0,1) =="P")
									{
										t1 =db.SelectAddersPrice("A"+PN.Popular_Opt[cont].ToString().Trim(),commadd.Trim()+"CommAdders_TableV1",PN.Bore_Size.ToString().Trim(), PN.Rod_Diamtr.ToString().Trim());
										total =t1; 
										if(total ==0)
										{
											result1 =true;
										}
									}
									else if(PN.Popular_Opt[cont].ToString().Trim() =="C1")
									{
										t1=0;
										if(PN.CompanyID !=null)
										{
											if(PN.CompanyID.Trim() =="1003")
											{
												t1 =db.SelectAddersPrice_Specials("C1","WEB_Specials_EV_Pricing_TableV1",PN.ValveSize.ToString().Trim());
											}
											if(PN.CompanyID.Trim() =="1004")
											{
												t1 =db.SelectAddersPrice_Specials("C1","WEB_Specials_Velan_Pricing_TableV1",PN.ValveSize.ToString().Trim());
											}
										}
										total =t1; 
										if(total ==0)
										{
											result1 =true;
										}
									}
								}
								if(result1 ==true)
								{
									lblstatus.Text ="true";
								}
								pop +=total;
							}
							decimal totaltandem =0.00m;
							for(int cont=0;cont < PN.Popular_Opt.Count; cont++)
							{
								bool result1=false;
								if(PN.Popular_Opt[cont].ToString().Substring(0,2) =="DC")
								{
									result1 =true;
									if(result1 ==true)
									{
										lblstatus.Text ="true";
									}
								}
								else if(PN.Popular_Opt[cont].ToString().Substring(0,2) =="BC")
								{
									result1 =true;
									if(result1 ==true)
									{
										lblstatus.Text ="true";
									}
								}
								else if(PN.Popular_Opt[cont].ToString().Substring(0,2) =="TC")
								{
									result1 =true;
									if(result1 ==true)
									{
										lblstatus.Text ="true";
									}
								}
							}
							decimal dcc1,dcc2=0.00m;
							dcc1 =Convert.ToDecimal(lbltotal.Text);
							dcc2 =dcc1 + totaltandem;
							lbltotal.Text=dcc2.ToString();
							ArrayList temp2 =new ArrayList();
							for(int p=0;p<PN.Popular_Opt.Count;p++)
							{
								if(PN.Popular_Opt[p].ToString().Substring(0,1) =="A")
								{
									if(PN.Popular_Opt[p].ToString().Substring(0,2) =="AD")
									{
										temp2.Add("Second thread extension AD="+PN.Popular_Opt[p].ToString().Substring(2).ToString()+"\"");
									}
									else
									{
										temp2.Add("Thread extension A="+PN.Popular_Opt[p].ToString().Substring(1).ToString()+"\"");
									}
								}
								else if(PN.Popular_Opt[p].ToString().Substring(0,1) =="W" && PN.Popular_Opt[p].ToString().Length  > 2)
								{
									if(PN.Popular_Opt[p].ToString().Substring(0,2) =="WD")
									{
										temp2.Add("Second rod extension WD="+PN.Popular_Opt[p].ToString().Substring(2).ToString()+"\"");
									}
									else
									{
										temp2.Add("Rod extension W="+PN.Popular_Opt[p].ToString().Substring(1).ToString()+"\"");
									}
								}
								else if(PN.Popular_Opt[p].ToString().Substring(0,2) =="BB" && PN.Popular_Opt[p].ToString().Length  > 3)
								{
									if(PN.Popular_Opt[p].ToString().Substring(0,3) =="BBC")
									{
										temp2.Add("Special Tie rod Extension (Cap End)BB="+PN.Popular_Opt[p].ToString().Substring(3).ToString()+"\"");
									}
									else
									{
										temp2.Add("Special Tie rod Extension (Head End)BB="+PN.Popular_Opt[p].ToString().Substring(3).ToString()+"\"");
									}
								}
								else if(PN.Popular_Opt[p].ToString().Substring(0,1) =="S")
								{
										
									temp2.Add("Stop Tube ST="+PN.Popular_Opt[p].ToString().Substring(2).ToString()+"\"");
										
								}
								else if(PN.Popular_Opt[p].ToString().Substring(0,2) =="DS")
								{
								
									temp2.Add("Stop Tube Double Piston Design DST="+PN.Popular_Opt[p].ToString().Substring(3).ToString()+"\"");
								
								}
								else if(PN.Popular_Opt[p].ToString().Substring(0,2) =="DC")
								{
								
									temp2.Add("Duplex Inline.(Rods not Attached) Second Stroke DC= "+PN.Popular_Opt[p].ToString().Substring(2).ToString()+"\"");
								
								}
								else if(PN.Popular_Opt[p].ToString().Substring(0,2) =="BC")
								{
								
									temp2.Add("Back To Back Cylinder. Second Stroke BC= "+PN.Popular_Opt[p].ToString().Substring(2).ToString()+"\"");
								
								}
								else if(PN.Popular_Opt[p].ToString().Substring(0,1) =="X")
								{
										
									temp2.Add("Intermediate trunnions XI="+PN.Popular_Opt[p].ToString().Substring(2).ToString()+"\"");
										
								}
								else if(PN.Popular_Opt[p].ToString().Substring(0,1) =="T" && PN.Popular_Opt[p].ToString().Substring(0,2) !="TC" )
								{
									string companyid="";
									if(PN.CompanyID !=null)
									{
										if(PN.CompanyID.ToString().Trim() !="")
										{
											companyid=PN.CompanyID.ToString().Trim();
										}
									}
									//issue #118 start
									//backup
									//if(companyid.ToString().Trim() =="1002" || companyid.ToString().Trim() =="1015")
									//update
									if(companyid.ToString().Trim() =="1002" || companyid.ToString().Trim() =="1015" || companyid.ToString().Trim() =="1021")
									//issue #118 end
									{
										string desc="";
										desc=db.SelectPopopts(PN.Popular_Opt[p].ToString().Trim());
										temp2.Add(desc.Trim());
									}
									else if(companyid.ToString().Trim() =="1003")
									{
										if(PN.Popular_Opt[p].ToString().Trim() !="")
										{
											string tt="";	
											if(PN.Popular_Opt[p].ToString().Trim() =="T2")
											{
												ArrayList trlist=new ArrayList();
												trlist=db.SelectTransducerDisc(PN.Popular_Opt[p].ToString().Trim());		
												for(int h=0;h<trlist.Count;h++)
												{
													tt +=trlist[h].ToString();
													if(trlist[h].ToString()!=null && h < trlist.Count -1)
													{
														tt +=", ";
													}
												}
											}
											else
											{
												if(PN.Popular_Opt[p].ToString().Length == 3)
												{
													ArrayList trlist=new ArrayList();
													trlist=db.SelectTransducerDisc(PN.Popular_Opt[p].ToString().Substring(0,3));		
													for(int h=0;h<trlist.Count;h++)
													{
														tt +=trlist[h].ToString();
														if(trlist[h].ToString()!=null && h < trlist.Count -1)
														{
															if(trlist[h].ToString().Trim() !="")
															{
																tt +=", ";
															}
														}
													}
												}
												else if(PN.Popular_Opt[p].ToString().Length > 3)
												{
													if(PN.Popular_Opt[p].ToString().Substring(2,1) =="R" || PN.Popular_Opt[p].ToString().Substring(2,1) =="P" 
														|| PN.Popular_Opt[p].ToString().Substring(2,1) =="C" || PN.Popular_Opt[p].ToString().Substring(2,1) =="Q"
														|| PN.Popular_Opt[p].ToString().Substring(2,1) =="D")
													{
														ArrayList trlist=new ArrayList();
														trlist=db.SelectTransducerDisc(PN.Popular_Opt[p].ToString().Substring(0,3));		
														for(int h=0;h<trlist.Count;h++)
														{
															tt +=trlist[h].ToString();
															if(trlist[h].ToString()!=null && h < trlist.Count -1)
															{
																if(trlist[h].ToString().Trim() !="")
																{
																	tt +=", ";
																}
															}
														}
														if(PN.Popular_Opt[p].ToString().ToString().Trim().Length >3)
														{
															tt =tt.Replace("10K",PN.Popular_Opt[p].ToString().ToString().Substring(3)+"K");
														}
													}
													else
													{
														ArrayList trlist=new ArrayList();
														trlist=db.SelectTransducerDisc(PN.Popular_Opt[p].ToString().ToString().Substring(0,2));		
														for(int h=0;h<trlist.Count;h++)
														{
															tt +=trlist[h].ToString();
															if(trlist[h].ToString()!=null && h < trlist.Count -1)
															{
																if(trlist[h].ToString().Trim() !="")
																{
																	tt +=", ";
																}
															}
														}
														if(PN.Popular_Opt[p].ToString().ToString().Trim().Length >2)
														{
															tt =tt.Replace("10K",PN.Popular_Opt[p].ToString().Substring(2)+"K");
														}
													}
												}
											}
											temp2.Add(tt.Trim());	
										}
									}
									else
									{
										string tt="";	
										if(PN.Popular_Opt[p].ToString().Length >2)
										{
											if(PN.Popular_Opt[p].ToString().Trim().Substring(2,1) =="S")
											{
									
												ArrayList trlist=new ArrayList();
												trlist=db.SelectTransducerDisc(PN.Popular_Opt[p].ToString().Substring(0,3));		
												for(int h=0;h<trlist.Count;h++)
												{
													tt +=trlist[h].ToString();
													if(trlist[h].ToString()!=null && h < trlist.Count -1)
													{
														tt +=", ";
													}
												}
												if(PN.Popular_Opt[p].ToString().Trim().Length >3)
												{
													tt =tt.Replace("10K",PN.Popular_Opt[p].ToString().Substring(3)+"K");
												}
											}
											else if(PN.Popular_Opt[p].ToString().Trim().Substring(2,1) =="R")
											{
									
												ArrayList trlist=new ArrayList();
												trlist=db.SelectTransducerDisc(PN.Popular_Opt[p].ToString().Substring(0,3));		
												//issue #262
												//back
												//for(int h=0;h<3;h++)
												//update
												for(int h=0;h<trlist.Count;h++)
											    {
													tt +=trlist[h].ToString();
													//backup
													//if(trlist[h].ToString()!=null && h < 3)
													//update
													if(trlist[h].ToString()!=null && h < trlist.Count)
													//issue #262 end
													{
														tt +=", ";
													}
												}
												if(PN.Popular_Opt[p].ToString().Trim().Length >3)
												{
													tt =tt.Replace("10K",PN.Popular_Opt[p].ToString().Substring(3)+"K");
												}
											}
											//issue #262 start
											//backup
											//if(PN.Popular_Opt[p].ToString().Trim().Substring(2,1) !="S" && PN.Popular_Opt[p].ToString().Trim().Substring(2,1) =="R")
											//update
											if(PN.Popular_Opt[p].ToString().Trim().Substring(2,1) !="S" && PN.Popular_Opt[p].ToString().Trim().Substring(2,1) !="R")
										    //issue #262 end
											{
												if(PN.Popular_Opt[p].ToString().Trim().Substring(1,1) =="1" ||PN.Popular_Opt[p].ToString().Trim().Substring(1,1) =="2"
													||PN.Popular_Opt[p].ToString().Trim().Substring(1,1) =="3" || PN.Popular_Opt[p].ToString().Trim().Substring(1,1) =="4"
													|| PN.Popular_Opt[p].ToString().Trim().Substring(1,1) =="5")
												{
										
													ArrayList trlist=new ArrayList();
													trlist=db.SelectTransducerDisc(PN.Popular_Opt[p].ToString().Substring(0,2));		
													for(int h=0;h<trlist.Count;h++)
													{
														tt +=trlist[h].ToString();
														if(trlist[h].ToString()!=null && h < trlist.Count -1)
														{
															tt +=", ";
														}
													}
													if(PN.Popular_Opt[p].ToString().Trim().Length >2)
													{
														tt =tt.Replace("10K",PN.Popular_Opt[p].ToString().Substring(2)+"K");
													}
												}
											}
										}
										else
										{
											ArrayList trlist=new ArrayList();
											trlist=db.SelectTransducerDisc(PN.Popular_Opt[p].ToString().Substring(0,2));		
											for(int h=0;h<trlist.Count;h++)
											{
												tt +=trlist[h].ToString();
												if(trlist[h].ToString()!=null && h < trlist.Count -1)
												{
													tt +=", ";
												}
											}
										}
										temp2.Add(tt.Trim());
									}		
								}
								else
								{
									temp2.Add(db.SelectPopopts(PN.Popular_Opt[p].ToString().Trim()));
								}
							}
						
							for(int t=0;t< temp2.Count;t++)
							{
								db.InsertItemPopularOpts(quot.QuoteNo.Trim(),LblPartNo.Text.Trim(), PN.Popular_Opt[t].ToString(),temp2[t].ToString(),"0.00");
							}
							Qitem.PopularOpt_Id=quot.QuoteNo.Trim()+LblPartNo.Text.Trim();
						}
						else
						{
							Qitem.PopularOpt_Id="";
						}
						decimal d1,d2=0.00m;
						d1 =Convert.ToDecimal(lbltotal.Text);
						string p_index="";
						//issue #118
						//bacuup
						//if(lst[6].ToString().Trim() =="1002" )
						//update
						if(lst[6].ToString().Trim() =="1002" ||lst[6].ToString().Trim() =="1021" )
						//issue #118 end
						{
							if(PN.Series.Trim() =="A")
							{
								p_index=db.SelectPriceIndex("AT");
							}
							else
							{
								if(PN.Series.ToString().Trim()=="ML" &&( PN.Bore_Size.ToString().Trim()=="C" || PN.Bore_Size.ToString().Trim()=="D" || PN.Bore_Size.ToString().Trim()=="E"))
								{			
									p_index=db.SelectPriceIndex("MLS");
								}
								else
								{
									p_index=db.SelectPriceIndex(PN.Series.Trim());
								}
							}
						}
						else
						{
							if(PN.Series.ToString().Trim()=="ML" &&( PN.Bore_Size.ToString().Trim()=="C" || PN.Bore_Size.ToString().Trim()=="D" || PN.Bore_Size.ToString().Trim()=="E"))
							{			
								p_index=db.SelectPriceIndex("MLS");
							}
							else
							{
								p_index=db.SelectPriceIndex(PN.Series.Trim());
							}							
						}
						decimal pindex=0.00m;
						pindex=Convert.ToDecimal(p_index.ToString());
						appoptprice =appoptprice + appoptprice * (pindex /100);
						pop =pop + pop * (pindex /100);
						//issue #315 start
						if(PN.Series.ToString().Trim()=="A" || PN.Series.ToString().Trim()=="AC" || PN.Series.ToString().Trim()=="AT" || PN.Series.ToString().Trim()=="AS" || PN.Series.ToString().Trim()=="SA")
						{
							decimal discount = discount=Convert.ToDecimal(lblD.Text);
							appoptprice = appoptprice*(1 - (discount/100));
							pop = pop*(1 - (discount/100));
                            sptotal= sptotal*(1 - (discount/100));
						}
						//issue #315 end
						d2 =d1 + appoptprice + pop + sptotal;
						lbltotal.Text=d2.ToString();
						if(TxtSpecialReq.Text.ToString().Trim() !="" )
						{
							lblstatus.Text ="true";
						}

						if(Session["Accessories"] !=null)
						{
							ArrayList acclist=new ArrayList();
							acclist=(ArrayList)Session["Accessories"];
							Others oth=new Others();
							decimal adc1=0.00m;
							decimal adc2=0.00m;
							decimal adc3=0.00m;
							decimal adc4=0.00m;
							decimal adc5=0.00m;
							for(int q=0;q<acclist.Count;q++)
							{
								oth=new Others();
								oth=(Others)acclist[q];
								string price="";
								price=db.SelectValue("ListPrice","WEB_Accessories_TableV11","PartNo",oth.Ids.Trim());
								p_index=db.SelectPriceIndex("Accessories");
								decimal apindex=0.00m;
								apindex=Convert.ToDecimal(pindex);
								adc2 = Convert.ToDecimal(oth.Qty.Trim()); 
								adc3 = Convert.ToDecimal(oth.Discount.Trim()); 
								if(price.Trim() !="")
								{
									adc1 = Convert.ToDecimal(price);
									adc1=adc1 + adc1 * (apindex /100);
									adc4 = adc1 * (1 - (adc3 / 100));
									adc5= adc4 * adc2;
									string str=db.InsertAccessories(quot.QuoteNo.Trim(),oth.Desc.Trim(), oth.Ids.Trim(),oth.Desc1.Trim(),adc4.ToString(),adc2.ToString(),adc3.ToString(),adc5.ToString());
					
								}
								else
								{
									string str=db.InsertAccessories(quot.QuoteNo.Trim(),oth.Desc.Trim(), oth.Ids.Trim(),oth.Desc1.Trim(),"0.00",adc2.ToString(),adc3.ToString(),"0.00");
									lblstatus.Text="true";
								}
							}
						
						}
								
						decimal dc1 ,dc2,dc3=0.00m;
						dc1=Convert.ToDecimal(TxtQty.Text.ToString());
						dc2=Convert.ToDecimal(lbltotal.Text.ToString());
						dc3= dc1 * dc2 ;
						//currency
						if(lblstatus.Text.ToLower().Trim() =="true")
						{
							Qitem.UnitPrice ="0.00";
							Qitem.TotalPrice="0.00";
						}
						else
						{
							Qitem.UnitPrice=lbltotal.Text.ToString();
							Qitem.TotalPrice=dc3.ToString();
							//currency
							//issue #251 start
							decimal curencya=1.00m;
							if(quot.Currency.ToString().Trim() =="US $")	
							{	
								curencya=Convert.ToDecimal(db.SelectValue("USD_Q","Dollar_ExchangeRate_TableV1","Slno","1"));
								Qitem.UnitPrice=Convert.ToString(Convert.ToDecimal(Qitem.UnitPrice)*curencya);
								Qitem.TotalPrice=Convert.ToString(Convert.ToDecimal(Qitem.TotalPrice)*curencya);
											
							}
							//issue #251 end
						}
						Qitem.Special_ID="";
						Qitem.PartNo=LblPartNo.Text.ToString();
						if(PN.Series.Trim() !="")
						{
							string weight="";
							decimal c1,c2,c3,c22,c5=0.00m;
							decimal c4=0.00m;
							if(PN.Series.Trim()== "A" )
							{
								//issue weight start backup
								//								if(PN.DoubleRod.ToUpper().Trim() =="YES")
								//								{
								//									c1=db.SelectAddersPrice("DWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								//									c2=db.SelectAddersPrice("DStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								//									c3=Convert.ToDecimal(PN.Stroke.Trim());
								//								}
								//								else
								//								{
								//									c1=db.SelectAddersPrice("SWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								//									c2=db.SelectAddersPrice("SStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								//									c3=Convert.ToDecimal(PN.Stroke.Trim());
								//								}
								//								c4=c1 + (c2 * c3);
								//								c4=Decimal.Round(c4,0);
								//issue weight end backup
								//issue weight start update
								if(PN.TandemDuplex ==null && PN.DoubleRod.ToUpper().Trim() =="NO")
								{
									c1=db.SelectAddersPrice("SWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									c2=db.SelectAddersPrice("SStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									c4=c1+c2*Convert.ToDecimal(PN.Stroke.Trim());
									c4=Decimal.Round(c4,0);
								}
								if(PN.TandemDuplex ==null && PN.DoubleRod.ToUpper().Trim() =="YES")
								{
									c1=db.SelectAddersPrice("DWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									c2=db.SelectAddersPrice("DStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									c4=c1+c2*Convert.ToDecimal(PN.Stroke.Trim());
									c4=Decimal.Round(c4,0);
								}
								if(PN.TandemDuplex !=null && PN.DoubleRod.ToUpper().Trim() =="NO")
								{
									c1=db.SelectAddersPrice("SWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									c2=db.SelectAddersPrice("SStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									c22=db.SelectAddersPrice("DStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									c4=c1*1.75m+(c2+c22)*Convert.ToDecimal(PN.Stroke.Trim());
									c4=Decimal.Round(c4,0);
								}
								if(PN.TandemDuplex !=null && PN.DoubleRod.ToUpper().Trim() =="YES")
								{
									c1=db.SelectAddersPrice("DWeight1","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									c22=db.SelectAddersPrice("DStroke","WEB_SeriesA_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									c4=c1*1.75m+2m*c22*Convert.ToDecimal(PN.Stroke.Trim());
									c4=Decimal.Round(c4,0);
								}
								//issue weight end update
								
							}
							else if( series.Trim()== "AT")
							{
								if(PN.DoubleRod.ToUpper().Trim() =="YES")
								{
									c1=db.SelectAddersPrice("DWeight1","WEB_SeriesAT_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									c2=db.SelectAddersPrice("DStroke","WEB_SeriesAT_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									c3=Convert.ToDecimal(PN.Stroke.Trim());
								}
								else
								{
									c1=db.SelectAddersPrice("SWeight1","WEB_SeriesAT_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									c2=db.SelectAddersPrice("SStroke","WEB_SeriesAT_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									c3=Convert.ToDecimal(PN.Stroke.Trim());
								}
								c4=c1 + (c2 * c3);
								c4=Decimal.Round(c4,0);
							}
							else if(PN.Series.Trim()== "M" || PN.Series.Trim()== "ML" || PN.Series.Trim()== "L")
							{
								if(PN.Bore_Size.Trim() =="P" || PN.Bore_Size.Trim() =="R" || PN.Bore_Size.Trim() =="S" || PN.Bore_Size.Trim() =="T" || PN.Bore_Size.Trim() =="W" || PN.Bore_Size.Trim() =="X")
								{
									if(PN.DoubleRod.ToUpper().Trim() =="YES")
									{
										c1=db.SelectAddersPrice("DWeight","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										c2=db.SelectAddersPrice("DStroke","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										
										if(PN.Mount.Substring(0,2) =="T1" || PN.Mount.Substring(0,2) =="T2")
										{
											c5=db.SelectAddersPrice("SWeight1","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										}
										else if(PN.Mount.Substring(0,2) =="E5" || PN.Mount.Substring(0,2) =="E6" || PN.Mount.Substring(0,2) =="T4")
										{
											c5=db.SelectAddersPrice("SWeight2","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										}
										else if(PN.Mount.Substring(0,2) =="F5" || PN.Mount.Substring(0,2) =="F6" )
										{
											c5=db.SelectAddersPrice("SWeight3","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										}
										else if(PN.Mount.Substring(0,2) =="P1" || PN.Mount.Substring(0,2) =="S2" || PN.Mount.Substring(0,2) =="S3")
										{
											c5=db.SelectAddersPrice("SWeight4","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										}
										else
										{
											c5=db.SelectAddersPrice("SWeight1","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										}
										c3=Convert.ToDecimal(PN.Stroke.Trim());
										c4= c1 + c5 + (c2 * c3);
										c4=Decimal.Round(c4,0);	
									}
									else
									{
										c2=db.SelectAddersPrice("SStroke","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										
										if(PN.Mount.Substring(0,2) =="T1" || PN.Mount.Substring(0,2) =="T2")
										{
											c5=db.SelectAddersPrice("SWeight1","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										}
										else if(PN.Mount.Substring(0,2) =="E5" || PN.Mount.Substring(0,2) =="E6" || PN.Mount.Substring(0,2) =="T4")
										{
											c5=db.SelectAddersPrice("SWeight2","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										}
										else if(PN.Mount.Substring(0,2) =="F5" || PN.Mount.Substring(0,2) =="F6" )
										{
											c5=db.SelectAddersPrice("SWeight3","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										}
										else if(PN.Mount.Substring(0,2) =="P1" || PN.Mount.Substring(0,2) =="S2" || PN.Mount.Substring(0,2) =="S3")
										{
											c5=db.SelectAddersPrice("SWeight4","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										}
										else
										{
											c5=db.SelectAddersPrice("SWeight1","WEB_SeriesHydraulic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										}	
										c3=Convert.ToDecimal(PN.Stroke.Trim());
										c4=c5 + (c2 * c3);
										c4=Decimal.Round(c4,0);								
									}
								}
								else if(PN.Bore_Size.Trim() !="P" && PN.Bore_Size.Trim() !="R" && PN.Bore_Size.Trim() !="S" && PN.Bore_Size.Trim() !="T" && PN.Bore_Size.Trim() !="W" && PN.Bore_Size.Trim() !="X")
								{
									if(PN.DoubleRod.ToUpper().Trim() =="YES")
									{
										if(PN.Mount.Substring(0,1) =="P" || PN.Mount.Substring(0,1) =="T" || PN.Mount.Substring(0,1) =="E" || PN.Mount.Substring(0,1) =="S")
										{
											c1=db.SelectAddersPrice("DWeight2","WEB_SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										}
										else
										{
											c1=db.SelectAddersPrice("DWeight1","WEB_SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										}
										c2=db.SelectAddersPrice("DStroke","WEB_SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else
									{
										if(PN.Mount.Substring(0,1) =="P" || PN.Mount.Substring(0,1) =="T" || PN.Mount.Substring(0,1) =="E" || PN.Mount.Substring(0,1) =="S")
										{
											c1=db.SelectAddersPrice("SWeight2","WEB_SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										}
										else
										{
											c1=db.SelectAddersPrice("SWeight1","WEB_SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
										}
										c2=db.SelectAddersPrice("SStroke","WEB_SeriesHydraulics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									c3=Convert.ToDecimal(PN.Stroke.Trim());
									c4=c1 + (c2 * c3);
									c4=Decimal.Round(c4,0);
								}
							}
							else if(PN.Series.Trim()== "PS" || PN.Series.Trim()== "N" || PN.Series.Trim()== "PC")
							{
								if(PN.DoubleRod.ToUpper().Trim() =="YES")
								{
									if(PN.Mount.Substring(0,1) =="P" || PN.Mount.Substring(0,1) =="T" || PN.Mount.Substring(0,1) =="E" || PN.Mount.Substring(0,1) =="S")
									{
										c1=db.SelectAddersPrice("DWeight2","WEB_SeriesPneumatics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else
									{
										c1=db.SelectAddersPrice("DWeight1","WEB_SeriesPneumatics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									c2=db.SelectAddersPrice("DStroke","WEB_SeriesPneumatics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								else
								{
									if(PN.Mount.Substring(0,1) =="P" || PN.Mount.Substring(0,1) =="T" || PN.Mount.Substring(0,1) =="E" || PN.Mount.Substring(0,1) =="S")
									{
										c1=db.SelectAddersPrice("SWeight2","WEB_SeriesPneumatics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else
									{
										c1=db.SelectAddersPrice("SWeight1","WEB_SeriesPneumatics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									c2=db.SelectAddersPrice("SStroke","WEB_SeriesPneumatics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
								}
								c3=Convert.ToDecimal(PN.Stroke.Trim());
								c4=c1 + c2 * c3;
								c4=Decimal.Round(c4,0);
							}
							else if(PN.Series.Trim()== "PA")
							{
								decimal c6=0.00m;
								if(PN.DoubleRod.ToUpper().Trim() =="YES")
								{
									c1=db.SelectAddersPrice("DWeight","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									c2=db.SelectAddersPrice("SWeight1","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									c5=db.SelectAddersPrice("DStroke","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									if(PN.Mount.Substring(0,1) =="X")
									{
										c3=db.SelectAddersPrice("SWeight1","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else if(PN.Mount.Substring(0,2) =="F1" || PN.Mount.Substring(0,2) =="F2")
									{
										c3=db.SelectAddersPrice("SWeight2","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else if(PN.Mount.Substring(0,2) =="F5" || PN.Mount.Substring(0,2) =="F6")
									{
										c3=db.SelectAddersPrice("SWeight3","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else if(PN.Mount.Substring(0,2) =="T1" || PN.Mount.Substring(0,2) =="T2")
									{
										c3=db.SelectAddersPrice("SWeight4","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else if(PN.Mount.Substring(0,2) =="P1" || PN.Mount.Substring(0,2) =="P3")
									{
										c3=db.SelectAddersPrice("SWeight5","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else if(PN.Mount.Substring(0,2) =="S1" || PN.Mount.Substring(0,2) =="S7")
									{
										c3=db.SelectAddersPrice("SWeight6","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else if(PN.Mount.Substring(0,2) =="T4" )
									{
										c3=db.SelectAddersPrice("SWeight7","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else if(PN.Mount.Substring(0,2) =="P4" )
									{
										c3=db.SelectAddersPrice("SWeight8","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else if(PN.Mount.Substring(0,2) =="S2" )
									{
										c3=db.SelectAddersPrice("SWeight9","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else if(PN.Mount.Substring(0,2) =="P2" )
									{
										c3=db.SelectAddersPrice("SWeight10","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else
									{
										c3=db.SelectAddersPrice("DWeight","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									c6=Convert.ToDecimal(PN.Stroke.Trim());
									c4=(c3 - c2)+ c1 + (c5 * c6);
									c4=Decimal.Round(c4,0);
								}
								else
								{
									c5=db.SelectAddersPrice("SStroke","WEB_SeriesPneumatics_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									if(PN.Mount.Substring(0,1) =="X")
									{
										c3=db.SelectAddersPrice("SWeight1","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else if(PN.Mount.Substring(0,2) =="F1" || PN.Mount.Substring(0,2) =="F2")
									{
										c3=db.SelectAddersPrice("SWeight2","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else if(PN.Mount.Substring(0,2) =="F5" || PN.Mount.Substring(0,2) =="F6")
									{
										c3=db.SelectAddersPrice("SWeight3","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else if(PN.Mount.Substring(0,2) =="T1" || PN.Mount.Substring(0,2) =="T2")
									{
										c3=db.SelectAddersPrice("SWeight4","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else if(PN.Mount.Substring(0,2) =="P1" || PN.Mount.Substring(0,2) =="P3")
									{
										c3=db.SelectAddersPrice("SWeight5","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else if(PN.Mount.Substring(0,2) =="S1" || PN.Mount.Substring(0,2) =="S7")
									{
										c3=db.SelectAddersPrice("SWeight6","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else if(PN.Mount.Substring(0,2) =="T4" )
									{
										c3=db.SelectAddersPrice("SWeight7","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else if(PN.Mount.Substring(0,2) =="P4" )
									{
										c3=db.SelectAddersPrice("SWeight8","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else if(PN.Mount.Substring(0,2) =="S2" )
									{
										c3=db.SelectAddersPrice("SWeight9","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else if(PN.Mount.Substring(0,2) =="P2" )
									{
										c3=db.SelectAddersPrice("SWeight10","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									else
									{
										c3=db.SelectAddersPrice("DWeight","WEB_SeriesPneumatic2_Weight_TableV1",PN.Bore_Size.Trim(),PN.Rod_Diamtr.Trim());
									}
									c6=Convert.ToDecimal(PN.Stroke.Trim());
									c4= c3 + (c5 * c6);
									c4=Decimal.Round(c4,0);
								}
							}
							if(c4 !=0)
							{
								//issue #242 start
								//back
								//weight="Approximate cylinder weight (does not include specials)= "+c4.ToString()+" LBS";
								//update
								weight="Approximate cylinder weight (does not include accessories)= "+c4.ToString()+" LBS";
								//issue #242 end
								//issue #233 start
								weight +=";Cylinder Displacement = " + Cyl_Displacement(PN) + " in" + Convert.ToChar(0179).ToString();
								//issue #233 end
							}
							else
							{
								weight="";
							}
							Qitem.Weight=weight.ToString();
						}
						string strd="";
						Qitem.Note="";
						//currency
						strd= db.InsertItems(Qitem);
						if(strd.ToString().Trim() =="1")
						{
							if(PN.Style !=null)
							{
								if(PN.Style.ToString().Trim() !="")
								{
									Opts opts=new Opts();
									string ff="";
									if(PN.Style.Trim() =="FC")
									{
										ff="Fail Close\r\n";
									}
									else if(PN.Style.Trim() =="FO")
									{
										ff="Fail Open\r\n";
									}
									else
									{
										ff="Style: Double Acting \r\n";
									}
									if(PN.ValveSize !=null)
									{
										ff +="  Valve Size: "+PN.ValveSize.ToString().Trim()+"\r\n";
									}
									if(PN.LinearPressure !=null)
									{
										ff +="  Line Pressure : "+PN.LinearPressure.ToString().Trim()+"\r\n";
									}
									if(PN.MinAirSupply !=null)
									{
										ff +="  Min Air Supply : "+PN.MinAirSupply.ToString().Trim()+"\r\n";
									}
									if(PN.SeatingTrust !=null)
									{
										ff +="  Seating Thrust : "+PN.SeatingTrust.ToString().Trim()+"\r\n";
									}
									if(PN.PackingFriction !=null)
									{
										ff +="  Packing Friction : "+PN.PackingFriction.ToString().Trim()+"\r\n";
									}
									if(PN.SaftyFactor !=null)
									{
										ff +="  Requested Safety Factor : "+PN.SaftyFactor.ToString().Trim()+"\r\n";
									}
									if(PN.ActualSaftyFactor !=null)
									{
										ff +="  Actual Safety Factor : "+PN.ActualSaftyFactor.ToString().Trim()+"\r\n";
									}
									if(cd1.ClosingTime !=null)
									{
										ff +="  Closing Time : "+cd1.ClosingTime.ToString().Trim()+"\r\n";
									}
									opts.Opt =ff.Trim();
									opts.Code=quot.QuoteNo.Trim();
									string temp=db.Insertcomments(opts);
								}
							}
							if(lblstatus.Text.ToLower().Trim() !="true" && TxtSpecialReq.Text.ToString().Trim() ==""  )
							{
								lblgenerateprice.Text ="Your Quote is created !!";
								lblqno.Text=quot.QuoteNo.Trim();
								LBGenerate.Visible=false;
								if(TxtSpecialReq.Text.ToString().Trim() !="" || lblstatus.Text.ToLower().Trim() =="true")
								{ 
									quot.Finish="0";
								}
								else
								{
									quot.Finish="1";
								}
								string str =db.SelectCustomerQno(quot.QuoteNo.ToString().Trim());
								if(str.ToString() !="")
								{
									string s =db.UpdateCustomerQuote(quot,str.Trim());
								}
								else
								{
									string s =db.InsertCustomerQuote(quot);
								}
								lblstatus.Text="";
								Session["Accessories"]=null;
								Response.Redirect("ManageQ.aspx?id="+quot.QuoteNo.Trim()+"&pn="+Qitem.PartNo.Trim());		
							}
							else
							{
								PSend.Visible=true;
								Session["Quote"] = quot;
								Session["Part"] =Qitem;
								if(TxtSpecialReq.Text.ToString().Trim() !="" ||  lblstatus.Text.ToLower().Trim() =="true")
								{ 
									quot.Finish="0";
									quot.Note ="This Quote require assitance from the factory.Please Contact Cowan Dynamics";
									if(TxtSpecialReq.Text.ToString().Trim() !="" )
									{
										Opts opts=new Opts();
										opts.Code=quot.QuoteNo.Trim();
										opts.Opt =TxtSpecialReq.Text.ToString().Trim();
										string temp=db.Insertcomments(opts);
									}
									db.Insert_NewRFQ_ICYL(lst[0].ToString().Trim(),quot.QuoteNo.ToString(),quot.Customer.ToString()
										,lst[0].ToString().Trim(),TxtSpecialReq.Text.ToString(),"",DateTime.Today.ToShortDateString(),DateTime.Today.AddDays(0).ToShortDateString()
										,"Select","0","1.00");
								}
								else
								{
									quot.Finish="1";
								}
								string str =db.SelectCustomerQno(quot.QuoteNo.ToString().Trim());
								if(str.ToString() !="")
								{
									string s =db.UpdateCustomerQuote(quot,str.Trim());
								}
								else
								{
									string s =db.InsertCustomerQuote(quot);
								}
								lblstatus.Text="";
								LBGenerate.Visible=false;
							
							}
						}
					}
					catch( Exception ex)
					{
						string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
						s=s.Replace("'"," ");
						LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
						lblgenerateprice.Text=ex.Message.ToString();
					}
					lblgenerateprice.Visible =true;
				}
				else
				{
					lblQty.Visible =true;
				}
			}
			catch(Exception ex)
			{
				if(ex.Message.ToString().Trim() !="Thread was being aborted.")
				{
					MailMessage mail=new MailMessage();
					SmtpMail.SmtpServer ="k2smtpout.secureserver.net"; 
					mail.From= "admin@cowandynamics.com";
					mail.To="admin@cowandynamics.com";
					mail.BodyFormat =MailFormat.Html;
					mail.Subject=   "icylinder error";
					mail.Priority =MailPriority.High;
					mail.Body =	ex.Message+ex.InnerException;
					//issue #582 start
					mail.Body += csSignature.Get_Admin();
					//issue #582 end
					SmtpMail.Send(mail);
				}
			}
		}
		public void MLPricing()
		{
			try
			{
				lblmgrp.Text="MLSMount_Grp";
				lbltable.Text="WEB_MLPrice_Master_TableV1";

				coder PN= (coder)Session["Coder"];
				if(PN.Application_Opt.Count >0 || PN.Popular_Opt.Count >0)
				{
					for(int p =0;p<PN.Application_Opt.Count;p++)
					{
						if(PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="D" || PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="R" )
						{
							lblmgrp.Text="MLDMount_Grp";
							lbltable.Text="WEB_MLDPrice_Master_TableV1";
						
						}
						
					}
					for(int p =0;p<PN.Popular_Opt.Count;p++)
					{
						if(PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="AD" || PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="WD" )
						{
							lblmgrp.Text="MLDMount_Grp";
							lbltable.Text="WEB_MLDPrice_Master_TableV1";
						}
					}
				}
				else
				{
					lblmgrp.Text="MLSMount_Grp";
					lbltable.Text="WEB_MLPrice_Master_TableV1";
				}
				//stroke price
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal cprice =0.00m;
				decimal seal=0.00m;
				decimal rodend=0.00m;

				st =Convert.ToDecimal(PN.Stroke);
				sp =db.SelectStrokePriceML(lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				sp =Decimal.Round(sp,2);
				decimal temp =sp * st;
				decimal index =0.00m;
				if(PN.Bore_Size.ToString().Trim()=="C" || PN.Bore_Size.ToString().Trim()=="D" || PN.Bore_Size.ToString().Trim()=="E")
				{			
					index =Convert.ToDecimal(db.SelectPriceIndex("MLS").ToString());
				}
				else
				{
					index =Convert.ToDecimal(db.SelectPriceIndex("ML").ToString());
				}
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				ArrayList lst=new ArrayList();
				lst=(ArrayList)Session["User"];
				lblD.Text=db.SelectDiscount(lst[1].ToString().Trim(),"ML");
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100)); 
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text =sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
				//mount price
				string mgrp ="";
				decimal mp =0.00m;
				try
				{
					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
					mp =db.SelectMountPriceML(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}
				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100)); 
				mprice = Decimal.Round(mp,2);
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}
				// cushin price
				decimal cp =0.00m;
				try
				{
					cp =db.SelectCushionPriceML(lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}
				cp =cp + cp * (index /100);
				
				
				if(PN.Cushions.ToString() =="5")
				{
					decimal temp1 =cp * 2;
					temp1 = temp1*(1 - (discount/100));
					cprice =Decimal.Round(temp1,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
					
				}
				else if(PN.Cushions.ToString() =="8")
				{
					cprice =0.00m;
					cprice =Decimal.Round(cprice,2);
				}
				else
				{
					cprice=cp;
					cprice = cprice*(1 - (discount/100)); 
					cprice =Decimal.Round(cprice,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
			
				cprice =Decimal.Round(cprice,2);
				
				if(PN.Seal_Comp.ToString().Trim() =="L")
				{
					seal =db.SelectAddersPrice("SL","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				
				}
				else if(PN.Seal_Comp.ToString().Trim() =="F")
				{
					seal =db.SelectAddersPrice("SF","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim()=="W")
				{
					seal =db.SelectAddersPrice("SW","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="E")
				{
					seal =db.SelectAddersPrice("SE","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				seal = Decimal.Round(seal,2);
				if(PN.Rod_End.Trim().Substring(0,1) !="N" && PN.Rod_End.Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="True";
					}
				}
				seal =seal + seal * (index /100);
				rodend =rodend + rodend * (index /100);
				decimal total=0.00m;
				total=sprice + mprice +cprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
				
		}
		public void PAPricing()
		{
			try
			{
				coder PN= (coder)Session["Coder"];
				if(PN.Application_Opt.Count >0 || PN.Popular_Opt.Count >0)
				{
					for(int p =0;p<PN.Application_Opt.Count;p++)
					{
						if(PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="D" || PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="R" )
						{
							char[] asst ;
							asst = PN.Bore_Size.ToString().ToCharArray(0,1);
							if( (int)asst[0] ==78)
							{
								lblmgrp.Text="PAD8MountSMC_Grp";
								lbltable.Text="WEB_PAPriceMaster_D8_TableV1";
							}
							else
							{
								lblmgrp.Text="PADMountSMC_Grp";
								lbltable.Text="WEB_PAPriceMaster_D_TableV1";
							}
						}
					}
					for(int p =0;p<PN.Popular_Opt.Count;p++)
					{
						if(PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="AD" || PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="WD" )
						{
							char[] asst ;
							asst = asst = PN.Bore_Size.ToString().ToCharArray(0,1);
							if( (int)asst[0] ==78)
							{
								lblmgrp.Text="PAD8MountSMC_Grp";
								lbltable.Text="WEB_PAPriceMaster_D8_TableV1";
							}
							else
							{
								lblmgrp.Text="PADMountSMC_Grp";
								lbltable.Text="WEB_PAPriceMaster_D_TableV1";
							}							
						}
					}
				}
				else
				{
					char[] asst ;
					asst = PN.Bore_Size.ToString().ToCharArray(0,1);
					if( (int)asst[0] ==78)
					{
						lblmgrp.Text="PAS8MountSMC_Grp";
						lbltable.Text="WEB_PAPriceMaster_SP_TableV1";
					}
					else
					{
						lblmgrp.Text="PASMountSMC_Grp";
						lbltable.Text="WEB_PAPriceMaster_TableV1";
					}
				}
				//stroke price
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal cprice =0.00m;
				decimal seal=0.00m;
				decimal rodend=0.00m;

				st =Convert.ToDecimal(PN.Stroke);
				try
				{
					sp =db.SelectStrokeSMCPricePA(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}
				sp =Decimal.Round(sp,2);
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(db.SelectPriceIndex("PA").ToString());
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				ArrayList lst=new ArrayList();
				lst=(ArrayList)Session["User"];
				lblD.Text=db.SelectDiscount(lst[1].ToString().Trim(),"PA");
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100)); 
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text =sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
				//mount price
				decimal mp =0.00m;
				string mgrp ="";
				try
				{ 
					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
					mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}

				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100)); 
				
				mprice= Decimal.Round(mp,2);
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}

				// cushin price
				string col ="";
				if(PN.Cushions.ToString().Trim() == "2" || PN.Cushions.ToString().Trim() == "3" || PN.Cushions.ToString().Trim() == "4")
				{
					col="PACushion_Per_InchN";
				} 
				else
				{
					col="PACushion_Per_InchA";
				}
				decimal cp =0.00m;
				cp =db.SelectCushionPrice(col.ToString(), lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				cp =cp + cp * (index /100);
				
				if(PN.Cushions.ToString().Trim() =="5" || PN.Cushions.ToString() =="2")
				{
					decimal temp1 =cp * 2;
					temp1 = temp1*(1 - (discount/100)); 
					cprice =Decimal.Round(temp1,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Cushions.ToString().Trim() =="8")
				{
					cprice =0.00m;
					cprice =Decimal.Round(cprice,2);
				}
				else
				{
					cprice=cp;
					cprice = cprice*(1 - (discount/100)); 
					cprice =Decimal.Round(cprice,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				cprice =Decimal.Round(cprice,2);

				
				if(PN.Seal_Comp.ToString().Trim() =="L")
				{
					seal =db.SelectAddersPrice("SL","WEB_SeriesPACommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim()  =="F")
				{
					seal =db.SelectAddersPrice("SF","WEB_SeriesPACommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				seal =Decimal.Round(seal,2);
				if(PN.Rod_End.ToString().Trim().Substring(0,1) !="N" && PN.Rod_End.ToString().Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd","WEB_SeriesPACommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="True";
					}
				}
				rodend =Decimal.Round(rodend,2);
				seal =seal + seal * (index /100);
				rodend =rodend + rodend * (index /100);
				decimal total=0.00m;
				total=sprice + mprice +cprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}
		//issue #644 start
		public void PAPricing_SMC()
		{
			try
			{
				coder PN= (coder)Session["Coder"];
				if(PN.Application_Opt.Count >0 || PN.Popular_Opt.Count >0)
				{
					for(int p =0;p<PN.Application_Opt.Count;p++)
					{
						if(PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="D" || PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="R" )
						{
							char[] asst ;
							asst = PN.Bore_Size.ToString().ToCharArray(0,1);
							if( (int)asst[0] ==78)
							{
								lblmgrp.Text="PAD8MountSMC_Grp";
								//issue #644 update
								lbltable.Text="PAPriceSMC_D8_Table";
							}
							else
							{
								lblmgrp.Text="PADMountSMC_Grp";
								//issue #644 update
								lbltable.Text="PAPriceSMC_D_Table";
							}
						}
					}
					for(int p =0;p<PN.Popular_Opt.Count;p++)
					{
						if(PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="AD" || PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="WD" )
						{
							char[] asst ;
							asst = asst = PN.Bore_Size.ToString().ToCharArray(0,1);
							if( (int)asst[0] ==78)
							{
								lblmgrp.Text="PAD8MountSMC_Grp";
								//issue #644 update
								lbltable.Text="PAPriceSMC_D8_Table";
							}
							else
							{
								lblmgrp.Text="PADMountSMC_Grp";
								//issue #644 update
								lbltable.Text="PAPriceSMC_D_Table";
							}							
						}
					}
				}
				else
				{
					char[] asst ;
					asst = PN.Bore_Size.ToString().ToCharArray(0,1);
					if( (int)asst[0] ==78)
					{
						lblmgrp.Text="PAS8MountSMC_Grp";
						//issue #644 update
						lbltable.Text="PAPriceSMC_SP_Table";
					}
					else
					{
						lblmgrp.Text="PASMountSMC_Grp";
						//issue #644 update
						lbltable.Text="PAPriceSMC_Table";
					}
				}
				//stroke price
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal cprice =0.00m;
				decimal seal=0.00m;
				decimal rodend=0.00m;

				st =Convert.ToDecimal(PN.Stroke);
				try
				{
					sp =db.SelectStrokeSMCPricePA(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}
				sp =Decimal.Round(sp,2);
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(db.SelectPriceIndex_SMC("PA").ToString());
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				ArrayList lst=new ArrayList();
				lst=(ArrayList)Session["User"];
				lblD.Text=db.SelectDiscount(lst[1].ToString().Trim(),"PA");
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100)); 
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text =sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
				//mount price
				decimal mp =0.00m;
				string mgrp ="";
				try
				{ 
					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
					mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}

				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100)); 
				
				mprice= Decimal.Round(mp,2);
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}

				// cushin price
				string col ="";
				if(PN.Cushions.ToString().Trim() == "2" || PN.Cushions.ToString().Trim() == "3" || PN.Cushions.ToString().Trim() == "4")
				{
					col="PACushion_Per_InchN";
				} 
				else
				{
					col="PACushion_Per_InchA";
				}
				decimal cp =0.00m;
				cp =db.SelectCushionPrice(col.ToString(), lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				cp =cp + cp * (index /100);
				
				if(PN.Cushions.ToString().Trim() =="5" || PN.Cushions.ToString() =="2")
				{
					decimal temp1 =cp * 2;
					temp1 = temp1*(1 - (discount/100)); 
					cprice =Decimal.Round(temp1,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Cushions.ToString().Trim() =="8")
				{
					cprice =0.00m;
					cprice =Decimal.Round(cprice,2);
				}
				else
				{
					cprice=cp;
					cprice = cprice*(1 - (discount/100)); 
					cprice =Decimal.Round(cprice,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				cprice =Decimal.Round(cprice,2);

				
				if(PN.Seal_Comp.ToString().Trim() =="L")
				{
					//issue #644 update
					//seal =db.SelectAddersPrice("SL","SMCSeriesPACommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					seal =db.SelectAddersPrice("L","SMCSeriesPACommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim()  =="F")
				{
					//issue #644 update
					//seal =db.SelectAddersPrice("SF","SMCSeriesPACommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					seal =db.SelectAddersPrice("F","SMCSeriesPACommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				seal =Decimal.Round(seal,2);
				if(PN.Rod_End.ToString().Trim().Substring(0,1) !="N" && PN.Rod_End.ToString().Trim().Substring(0,1) !="A")
				{
					//issue #644 update
					rodend =db.SelectAddersPrice("MetricRodEnd","SMCSeriesPACommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="True";
					}
				}
				rodend =Decimal.Round(rodend,2);
				seal =seal + seal * (index /100);
				rodend =rodend + rodend * (index /100);
				decimal total=0.00m;
				total=sprice + mprice +cprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}
		//issue #644 end
		public void PSPricing()
		{
			try
			{
				lblmgrp.Text="PSSMountSMC_Grp";
				lbltable.Text="WEB_PSPriceMaster_TableV1";

				coder PN= (coder)Session["Coder"];
				if(PN.Application_Opt.Count >0 || PN.Popular_Opt.Count >0)
				{
					for(int p =0;p<PN.Application_Opt.Count;p++)
					{
						if(PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="D" || PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="R" )
						{
							char[] asst ;
							asst = PN.Bore_Size.ToString().ToCharArray(0,1);
							if( (int)asst[0] >= 78)
							{
								lblmgrp.Text="PSDMountSMC_GrpSp";
								lbltable.Text="WEB_PSPriceMaster_DSP_TableV1";
							}
							else
							{
								lblmgrp.Text="PSDMountSMC_Grp";
								lbltable.Text="WEB_PSPriceMaster_D_TableV1";
							}
						}
					
					}
					for(int p =0;p<PN.Popular_Opt.Count;p++)
					{
						if(PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="AD" || PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="WD" )
						{
							char[] asst ;
							asst = asst = PN.Bore_Size.ToString().ToCharArray(0,1);
							if( (int)asst[0] >= 78)
							{
								lblmgrp.Text="PSDMountSMC_GrpSp";
								lbltable.Text="WEB_PSPriceMaster_DSP_TableV1";
							}
							else
							{
								lblmgrp.Text="PSDMountSMC_Grp";
								lbltable.Text="WEB_PSPriceMaster_D_TableV1";
							}
						}
					}
				}
				else
				{
					char[] asst ;
					asst = asst = PN.Bore_Size.ToString().ToCharArray(0,1);
					if( (int)asst[0] >=78)
					{
						lblmgrp.Text="PSSMountSMC_GrpSp";
						lbltable.Text="WEB_PSPriceMaster_SP_TableV1";
					}
					else
					{
						lblmgrp.Text="PSSMountSMC_Grp";
						lbltable.Text="WEB_PSPriceMaster_TableV1";
					}
					
				}
				if(PN.Mount.ToString().Trim() =="MT1+" )
				{
					lblmgrp.Text="PSSMountMT1_Grp";
					lbltable.Text="WEB_PSPriceSMC_MT1_TableV1";
				}
				
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal cprice =0.00m;	
				decimal seal=0.00m;

				st =Convert.ToDecimal(PN.Stroke);
				sp =db.SelectStrokeSMCPricePS(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				sp =Decimal.Round(sp,2);
				
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(db.SelectPriceIndex("PS").ToString());
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				ArrayList lst=new ArrayList();
				lst=(ArrayList)Session["User"];
				lblD.Text=db.SelectDiscount(lst[1].ToString().Trim(),"PS");
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100));
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text =sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
				//mount price
				decimal mp =0.00m;
				string mgrp ="";
				try
				{
					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
					mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}
				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100));
				
				mprice=Decimal.Round(mp,2);
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}

				// cushin price
				string col ="";
				if(PN.Cushions.Trim() == "2" || PN.Cushions.Trim() == "3" || PN.Cushions.Trim() == "4")
				{
					col="CushionPerInchN";
				} 
				else
				{
					col="CushionPerInchA";
				}
				decimal cp =0.00m;
				cp =db.SelectCushionPrice(col.ToString(), lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				cp =cp + cp * (index /100);
				
				if(PN.Cushions.Trim() =="5" || PN.Cushions.Trim() =="2")
				{
					decimal temp1 =cp * 2;
					temp1 = temp1*(1 - (discount/100));
					cprice =Decimal.Round(temp1,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Cushions.Trim() =="8")
				{
					cprice =0.00m;
					cprice =Decimal.Round(cprice,2);
				}
				else
				{
					cprice=cp;
					cprice = cprice*(1 - (discount/100)); 
					cprice =Decimal.Round(cprice,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				cprice =Decimal.Round(cprice,2);
				
				if(PN.Seal_Comp.Trim()  =="L")
				{
					seal =db.SelectAddersPrice("SL","SWEB_eriesPSCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.Trim()  =="F")
				{
					seal =db.SelectAddersPrice("SF","WEB_SeriesPSCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				seal =Decimal.Round(seal,2);

				decimal rodend=0.00m;
				if(PN.Rod_End.Trim().Substring(0,1) !="N" && PN.Rod_End.Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd","WEB_SeriesPSCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="True";
					}
				}
				rodend =Decimal.Round(rodend,2);
				seal =seal + seal * (index /100);
				rodend =rodend + rodend * (index /100);
				decimal total=0.00m;
				total=sprice + mprice +cprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}
		//issue #644 start
		public void PSPricing_SMC()
		{
			try
			{
				lblmgrp.Text="PSSMountSMC_Grp";
				lbltable.Text="WEB_PSPriceMaster_TableV1";

				coder PN= (coder)Session["Coder"];
				if(PN.Application_Opt.Count >0 || PN.Popular_Opt.Count >0)
				{
					for(int p =0;p<PN.Application_Opt.Count;p++)
					{
						if(PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="D" || PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="R" )
						{
							char[] asst ;
							asst = PN.Bore_Size.ToString().ToCharArray(0,1);
							if( (int)asst[0] >= 78)
							{
								lblmgrp.Text="PSDMountSMC_GrpSp";
								//issue #644 update
								lbltable.Text="PSPriceSMC_DSP_Table";
							}
							else
							{
								lblmgrp.Text="PSDMountSMC_Grp";
								//issue #644 update
								lbltable.Text="PSPriceSMC_D_Table";
							}
						}
					
					}
					for(int p =0;p<PN.Popular_Opt.Count;p++)
					{
						if(PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="AD" || PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="WD" )
						{
							char[] asst ;
							asst = asst = PN.Bore_Size.ToString().ToCharArray(0,1);
							if( (int)asst[0] >= 78)
							{
								lblmgrp.Text="PSDMountSMC_GrpSp";
								//issue #644 update
								lbltable.Text="PSPriceSMC_DSP_Table";
							}
							else
							{
								lblmgrp.Text="PSDMountSMC_Grp";
								//issue #644 update
								lbltable.Text="PSPriceSMC_D_Table";
							}
						}
					}
				}
				else
				{
					char[] asst ;
					asst = asst = PN.Bore_Size.ToString().ToCharArray(0,1);
					if( (int)asst[0] >=78)
					{
						lblmgrp.Text="PSSMountSMC_GrpSp";
						//issue #644 update
						lbltable.Text="PSPriceSMC_SP_Table";
					}
					else
					{
						lblmgrp.Text="PSSMountSMC_Grp";
						//issue #644 update
						lbltable.Text="PSPriceSMC_Table";
					}
					
				}
				if(PN.Mount.ToString().Trim() =="MT1+" )
				{
					lblmgrp.Text="PSSMountMT1_Grp";
					//issue #644 update
					lbltable.Text="PSPriceSMC_MT1_Table";
				}
				
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal cprice =0.00m;	
				decimal seal=0.00m;

				st =Convert.ToDecimal(PN.Stroke);
				sp =db.SelectStrokeSMCPricePS(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				sp =Decimal.Round(sp,2);
				
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(db.SelectPriceIndex_SMC("PS").ToString());
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				ArrayList lst=new ArrayList();
				lst=(ArrayList)Session["User"];
				lblD.Text=db.SelectDiscount(lst[1].ToString().Trim(),"PS");
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100));
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text =sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
				//mount price
				decimal mp =0.00m;
				string mgrp ="";
				try
				{
					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
					mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}
				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100));
				
				mprice=Decimal.Round(mp,2);
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}

				// cushin price
				string col ="";
				if(PN.Cushions.Trim() == "2" || PN.Cushions.Trim() == "3" || PN.Cushions.Trim() == "4")
				{
					col="CushionPerInchN";
				} 
				else
				{
					col="CushionPerInchA";
				}
				decimal cp =0.00m;
				cp =db.SelectCushionPrice(col.ToString(), lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				cp =cp + cp * (index /100);
				
				if(PN.Cushions.Trim() =="5" || PN.Cushions.Trim() =="2")
				{
					decimal temp1 =cp * 2;
					temp1 = temp1*(1 - (discount/100));
					cprice =Decimal.Round(temp1,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Cushions.Trim() =="8")
				{
					cprice =0.00m;
					cprice =Decimal.Round(cprice,2);
				}
				else
				{
					cprice=cp;
					cprice = cprice*(1 - (discount/100)); 
					cprice =Decimal.Round(cprice,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				cprice =Decimal.Round(cprice,2);
				
				if(PN.Seal_Comp.Trim()  =="L")
				{
					//issue #644 update
					//seal =db.SelectAddersPrice("SL","SMCSeriesPSCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					seal =db.SelectAddersPrice("L","SMCSeriesPSCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.Trim()  =="F")
				{
					//issue #644 update
					//seal =db.SelectAddersPrice("SF","SMCSeriesPSCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					seal =db.SelectAddersPrice("F","SMCSeriesPSCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				seal =Decimal.Round(seal,2);

				decimal rodend=0.00m;
				if(PN.Rod_End.Trim().Substring(0,1) !="N" && PN.Rod_End.Trim().Substring(0,1) !="A")
				{
					//issue #644 update
					rodend =db.SelectAddersPrice("MetricRodEnd","SMCSeriesPSCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="True";
					}
				}
				rodend =Decimal.Round(rodend,2);
				seal =seal + seal * (index /100);
				rodend =rodend + rodend * (index /100);
				decimal total=0.00m;
				total=sprice + mprice +cprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}
		//issue #644 end

		public void PCPricing()
		{
			try
			{
				coder PN= (coder)Session["Coder"];
				if(PN.Application_Opt.Count >0 || PN.Popular_Opt.Count >0)
				{
					for(int p =0;p<PN.Application_Opt.Count;p++)
					{
						if(PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="D" || PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="R" )
						{
							lblstatus.Text ="True";
							return;
						}
				
					}
					for(int p =0;p<PN.Popular_Opt.Count;p++)
					{
						if(PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="AD" || PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="WD" )
						{
							lblstatus.Text ="True";
							return;
						}
					}
				}
				lblmgrp.Text="PCMount_Grp";
				lbltable.Text="WEB_PCPriceSMC_Table";
				//stroke price
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal cprice =0.00m;
				st =Convert.ToDecimal(PN.Stroke);
				try
				{
					sp =db.SelectStrokePriceL(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}

				sp =Decimal.Round(sp,2);
				
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(db.SelectPriceIndex("PC").ToString());
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				ArrayList lst=new ArrayList();
				lst=(ArrayList)Session["User"];
				lblD.Text=db.SelectDiscount(lst[1].ToString().Trim(),"PC");
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100)); 
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text=sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
				//mount price
				decimal mp =0.00m;
				string mgrp ="";
				try
				{ 
					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
					mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}

				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100)); 
				
				mprice=Decimal.Round(mp,2);
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}
				// cushin price
				string col ="";
				if(PN.Cushions.Trim() == "2" || PN.Cushions.Trim() == "3" || PN.Cushions.Trim() == "4")
				{
					col="CushionPerInchN";
				} 
				else
				{
					col="CushionPerInchA";
				}
				decimal cp =0.00m;
				cp =db.SelectCushionPrice(col.ToString(), lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				cp =cp + cp * (index /100);
				
				if(PN.Cushions.Trim() =="5" || PN.Cushions.Trim() =="2")
				{
					decimal temp1 =cp * 2;
					temp1 = temp1*(1 - (discount/100)); 
					cprice =Decimal.Round(temp1,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Cushions.Trim() =="8")
				{
					cprice =0.00m;
					cprice =Decimal.Round(cprice,2);
				}
				else
				{
					cprice=cp;
					cprice = cprice*(1 - (discount/100)); 
					cprice =Decimal.Round(cprice,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				cprice =Decimal.Round(cprice,2);
				decimal seal=0.00m;
				if(PN.Seal_Comp.Trim() =="L")
				{
					seal =db.SelectAddersPrice("SL","WEB_SeriesPCCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.Trim() =="F")
				{
					seal =db.SelectAddersPrice("SF","WEB_SeriesPCCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				
				}
				seal =Decimal.Round(seal,2);

				decimal rodend=0.00m;
				if(PN.Rod_End.Trim().Substring(0,1) !="N" && PN.Rod_End.Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd","WEB_SeriesPCCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="True";
					}
				}
				rodend =Decimal.Round(rodend,2);
				seal =seal + seal * (index /100);
				rodend =rodend + rodend * (index /100);
				decimal total=0.00m;
				total=sprice + mprice +cprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		
		}
		//issue #644 start
		public void PCPricing_SMC()
		{
			try
			{
				coder PN= (coder)Session["Coder"];
				if(PN.Application_Opt.Count >0 || PN.Popular_Opt.Count >0)
				{
					for(int p =0;p<PN.Application_Opt.Count;p++)
					{
						if(PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="D" || PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="R" )
						{
							lblstatus.Text ="True";
							return;
						}
				
					}
					for(int p =0;p<PN.Popular_Opt.Count;p++)
					{
						if(PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="AD" || PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="WD" )
						{
							lblstatus.Text ="True";
							return;
						}
					}
				}
				lblmgrp.Text="PCMount_Grp";
				//issue #644 update
				lbltable.Text="PCPriceSMC_Table";
				//stroke price
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal cprice =0.00m;
				st =Convert.ToDecimal(PN.Stroke);
				try
				{
					sp =db.SelectStrokePriceL(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}

				sp =Decimal.Round(sp,2);
				
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(db.SelectPriceIndex_SMC("PC").ToString());
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				ArrayList lst=new ArrayList();
				lst=(ArrayList)Session["User"];
				lblD.Text=db.SelectDiscount(lst[1].ToString().Trim(),"PC");
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100)); 
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text=sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
				//mount price
				decimal mp =0.00m;
				string mgrp ="";
				try
				{ 
					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
					mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}

				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100)); 
				
				mprice=Decimal.Round(mp,2);
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}
				// cushin price
				string col ="";
				if(PN.Cushions.Trim() == "2" || PN.Cushions.Trim() == "3" || PN.Cushions.Trim() == "4")
				{
					col="CushionPerInchN";
				} 
				else
				{
					col="CushionPerInchA";
				}
				decimal cp =0.00m;
				cp =db.SelectCushionPrice(col.ToString(), lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				cp =cp + cp * (index /100);
				
				if(PN.Cushions.Trim() =="5" || PN.Cushions.Trim() =="2")
				{
					decimal temp1 =cp * 2;
					temp1 = temp1*(1 - (discount/100)); 
					cprice =Decimal.Round(temp1,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Cushions.Trim() =="8")
				{
					cprice =0.00m;
					cprice =Decimal.Round(cprice,2);
				}
				else
				{
					cprice=cp;
					cprice = cprice*(1 - (discount/100)); 
					cprice =Decimal.Round(cprice,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				cprice =Decimal.Round(cprice,2);
				decimal seal=0.00m;
				if(PN.Seal_Comp.Trim() =="L")
				{
					//issue #644 update
					//seal =db.SelectAddersPrice("SL","SMCSeriesPCCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					seal =db.SelectAddersPrice("L","SMCSeriesPCCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.Trim() =="F")
				{
					//issue #644 update
					//seal =db.SelectAddersPrice("SF","SMCSeriesPCCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					seal =db.SelectAddersPrice("F","SMCSeriesPCCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				
				}
				seal =Decimal.Round(seal,2);

				decimal rodend=0.00m;
				if(PN.Rod_End.Trim().Substring(0,1) !="N" && PN.Rod_End.Trim().Substring(0,1) !="A")
				{
					//issue #644 update
					rodend =db.SelectAddersPrice("MetricRodEnd","SMCSeriesPCCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="True";
					}
				}
				rodend =Decimal.Round(rodend,2);
				seal =seal + seal * (index /100);
				rodend =rodend + rodend * (index /100);
				decimal total=0.00m;
				total=sprice + mprice +cprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		
		}
		//issue #644 end

		public void MPricing()
		{
			try
			{
				lblmgrp.Text="MLSMount_Grp";
				lbltable.Text="WEB_MLPrice_Master_TableV1";
				coder PN= (coder)Session["Coder"];
				if(PN.Application_Opt.Count >0 || PN.Popular_Opt.Count >0)
				{
					for(int p =0;p<PN.Application_Opt.Count;p++)
					{
						if(PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="D" || PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="R" )
						{
							lblmgrp.Text="MLDMount_Grp";
							lbltable.Text="WEB_MLDPrice_Master_TableV1";
						
						}
						
					}
					for(int p =0;p<PN.Popular_Opt.Count;p++)
					{
						if(PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="AD" || PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="WD" )
						{
							lblmgrp.Text="MLDMount_Grp";
							lbltable.Text="WEB_MLDPrice_Master_TableV1";
						}
					}
				}
				else
				{
					lblmgrp.Text="MLSMount_Grp";
					lbltable.Text="WEB_MLPrice_Master_TableV1";
				}
				//stroke price
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal cprice =0.00m;
				decimal seal=0.00m;
				decimal rodend=0.00m;

				st =Convert.ToDecimal(PN.Stroke);
				sp =db.SelectStrokePriceML(lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				sp =Decimal.Round(sp,2);
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(db.SelectPriceIndex("ML").ToString());
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				ArrayList lst=new ArrayList();
				lst=(ArrayList)Session["User"];
				lblD.Text=db.SelectDiscount(lst[1].ToString().Trim(),"M");
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100)); 
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text =sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
				//mount price
				string mgrp ="";
				decimal mp =0.00m;
				try
				{
					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
					mp =db.SelectMountPriceML(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}
				decimal mlm=4.00m;
				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100)); 
				mp=mp*(1 - (mlm/100)); 
				mprice = Decimal.Round(mp,2);
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}
				// cushin price
				decimal cp =0.00m;
				try
				{
					cp =db.SelectCushionPriceML(lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}
				cp =cp + cp * (index /100);
				
				
				if(PN.Cushions.ToString() =="5")
				{
					decimal temp1 =cp * 2;
					temp1 = temp1*(1 - (discount/100));
					cprice =Decimal.Round(temp1,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
					
				}
				else if(PN.Cushions.ToString() =="8")
				{
					cprice =0.00m;
					cprice =Decimal.Round(cprice,2);
				}
				else
				{
					cprice=cp;
					cprice = cprice*(1 - (discount/100)); 
					cprice =Decimal.Round(cprice,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
			
				cprice =Decimal.Round(cprice,2);
				
				if(PN.Seal_Comp.ToString().Trim() =="L")
				{
					seal =db.SelectAddersPrice("SL","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				
				}
				else if(PN.Seal_Comp.ToString().Trim() =="F")
				{
					seal =db.SelectAddersPrice("SF","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim()=="W")
				{
					seal =db.SelectAddersPrice("SW","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="E")
				{
					seal =db.SelectAddersPrice("SE","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				seal = Decimal.Round(seal,2);
				if(PN.Rod_End.Trim().Substring(0,1) !="N" && PN.Rod_End.Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd","WEB_SeriesMLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="True";
					}
				}
				seal =seal + seal * (index /100);
				rodend =rodend + rodend * (index /100);
				decimal total=0.00m;
				total=sprice + mprice +cprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
				
		}
		public void NPricing()
		{
			try
			{
				coder PN= (coder)Session["Coder"];
				if(PN.Application_Opt.Count >0 || PN.Popular_Opt.Count >0)
				{
					for(int p =0;p<PN.Application_Opt.Count;p++)
					{
						if(PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="D" || PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="R" )
						{
							lblstatus.Text ="True";
							////Added by Kourosh
							lbltotal.Text="0";
							return;
						}
				
					}
					for(int p =0;p<PN.Popular_Opt.Count;p++)
					{
						if(PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="AD" || PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="WD" )
						{
							lblstatus.Text ="True";
							return;
						}
					}
				}
				lblmgrp.Text="NMount_Grp";
				lbltable.Text="WEB_NPrice_Master_TableV1";
		
				//stroke price
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal cprice =0.00m;
				decimal seal=0.00m;
				st =Convert.ToDecimal(PN.Stroke);
				sp =db.SelectStrokePriceL(lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				sp =Decimal.Round(sp,2);
				
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(db.SelectPriceIndex("N").ToString());
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				ArrayList lst=new ArrayList();
				lst=(ArrayList)Session["User"];
				lblD.Text=db.SelectDiscount(lst[1].ToString().Trim(),"N");
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100)); 
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text=sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
				//mount price
				string mgrp ="";
				decimal mp =0.00m;
				try
				{
					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
					if(mgrp.ToString() !="" || mgrp.ToString() !=null)
					{
						mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					}
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}
				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100)); 
				
				mprice=Decimal.Round(mp,2);
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}
				// cushin price
				decimal cp =0.00m;
				try
				{
					cp =db.SelectCushionPrice("CushionPerEnd",lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}
				cp =cp + cp * (index /100);
				
				if(PN.Cushions.ToString().Trim() =="5")
				{
					decimal temp1 =cp * 2;
					temp1 = temp1*(1 - (discount/100));
					cprice =Decimal.Round(temp1,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Cushions.ToString().Trim() =="8")
				{
					cprice =0.00m;
					cprice =Decimal.Round(cprice,2);
				}
				else
				{
					cprice=cp;
					cprice = cprice*(1 - (discount/100)); 
					cprice =Decimal.Round(cprice,2);
					if(cprice ==0)
					{
						lblstatus.Text ="True";
					}
				}
				cprice =Decimal.Round(cprice,2);
				
				if(PN.Seal_Comp.ToString().Trim() =="L")
				{
					seal =db.SelectAddersPrice("SL","WEB_SeriesNCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="F")
				{
					seal =db.SelectAddersPrice("SF","WEB_SeriesNCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="W")
				{
					seal =db.SelectAddersPrice("SW","WEB_SeriesNCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="E")
				{
					seal =db.SelectAddersPrice("SE","WEB_SeriesNCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				seal =Decimal.Round(seal,2);

				decimal rodend=0.00m;
				if(PN.Rod_End.ToString().Trim().Substring(0,1) !="N" && PN.Rod_End.ToString().Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd","WEB_SeriesNCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="True";
					}
				}
				rodend =Decimal.Round(rodend,2);
				seal =seal + seal * (index /100);
				rodend =rodend + rodend * (index /100);
				decimal total=0.00m;
				total=sprice + mprice +cprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
				
		}
		public void LPricing()
		{
			try
			{
				coder PN= (coder)Session["Coder"];
				if(PN.Application_Opt.Count >0 || PN.Popular_Opt.Count >0)
				{
					for(int p =0;p<PN.Application_Opt.Count;p++)
					{
						if(PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="D" || PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="R" )
						{
							lblstatus.Text ="True";
							return;
						}
				
					}
					for(int p =0;p<PN.Popular_Opt.Count;p++)
					{
						if(PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="AD" || PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="WD" )
						{
							lblstatus.Text ="True";
							return;
						}
					}
				}
				lblmgrp.Text="LMount_Grp";
				lbltable.Text="WEB_LPrice_Master_TableV1";
				//stroke price
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal cprice =0.00m;
				decimal seal=0.00m;
				decimal rodend=0.00m;

				st =Convert.ToDecimal(PN.Stroke);
				sp =db.SelectStrokeLprice(lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				sp =Decimal.Round(sp,2);
				
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(db.SelectPriceIndex("L").ToString());
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				ArrayList lst=new ArrayList();
				lst=(ArrayList)Session["User"];
				lblD.Text=db.SelectDiscount(lst[1].ToString().Trim(),"L");
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100)); 
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text=sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
			
				//mount price
				string mgrp ="";
				decimal mp =0.00m;
				try
				{
					mgrp=db.SelectMountGrp(PN.Mount.ToString(),lblmgrp.Text.ToString().Trim());
					if(mgrp.ToString() !="" || mgrp.ToString() !=null)
					{
						mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					}
				}
				catch(Exception ex) 
				{
					Response.Write("<script language='javascript'>alert( ' "+ex.ToString()+" ' )</script>");
				}
				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100)); 
				
				mprice=Decimal.Round(mp,2);
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}				
				
				if(PN.Seal_Comp.ToString().Trim() =="L")
				{
					seal =db.SelectAddersPrice("SL","WEB_SeriesLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				
				}
				else if(PN.Seal_Comp.ToString().Trim() =="F")
				{
					seal =db.SelectAddersPrice("SF","WEB_SeriesLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim()=="W")
				{
					seal =db.SelectAddersPrice("SW","WEB_SeriesLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="E")
				{
					seal =db.SelectAddersPrice("SE","WEB_SeriesLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				seal = Decimal.Round(seal,2);
				if(PN.Rod_End.Trim().Substring(0,1) !="N" && PN.Rod_End.Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd","WEB_SeriesLCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="True";
					}
				}
				seal =seal + seal * (index /100);
				rodend =rodend + rodend * (index /100);
				decimal total=0.00m;
				total=sprice + mprice +cprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}
		public void MHPricing()
		{}
		public void RPricing()
		{}
		public void APricing()
		{
			try
			{
				lblmgrp.Text="AP_Base";
				lbltable.Text="WEB_APrice_Master_TableV1";
				string commn="WEB_SeriesACommAdders_TableV1";
				string ind="0";
				ArrayList lst =new ArrayList();
				lst =(ArrayList)Session["User"];
				//issue #118 start
				//backup
				//if(lst[6].ToString().Trim() =="1002")
				//update
				if(lst[6].ToString().Trim() =="1002" || lst[6].ToString().Trim() =="1021")
				//isuue #118 end
				{
					lblmgrp.Text="AP_Base";
					lbltable.Text="WEB_APrice_SVC_TableV1";
					commn="WEB_SVCSeriesACommAdders_TableV1";
					ind=db.SelectPriceIndex("SA").ToString();
				}
				else
				{
					lblmgrp.Text="AP_Base";
					lbltable.Text="WEB_APrice_Master_TableV1";
					commn="WEB_SeriesACommAdders_TableV1";
					ind=db.SelectPriceIndex("A").ToString();
				}
				coder PN= (coder)Session["Coder"];
				if(PN.Application_Opt.Count >0 || PN.Popular_Opt.Count >0)
				{
					for(int p =0;p<PN.Application_Opt.Count;p++)
					{
						if(PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="D" || PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="R" )
						{
							lblmgrp.Text="AP_Base";
							lbltable.Text="WEB_ADPrice_Master_TableV1";
						}
				
					}
					for(int p =0;p<PN.Popular_Opt.Count;p++)
					{
						if(PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="AD" || PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="WD" )
						{
							lblmgrp.Text="AP_Base";
							lbltable.Text="WEB_ADPrice_Master_TableV1";
						}
					}
				}
				if(lst[6].ToString().Trim() =="1004")
				{
					ind=db.SelectPriceIndex("VA").ToString();
					commn="WEB_SeriesA_Velan_CommAdders_TableV1";
					lblmgrp.Text="AP_Base";
					lbltable.Text="WEB_APrice_Velan_TableV1";
					if(PN.Application_Opt.Count >0 || PN.Popular_Opt.Count >0)
					{
						for(int p =0;p<PN.Application_Opt.Count;p++)
						{
							if(PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="D" || PN.Application_Opt[p].ToString().Trim().Substring(0,1) =="R" )
							{
								lblmgrp.Text="AP_Base";
								lbltable.Text="WEB_ADPrice_Velan_TableV1";
							}
						}
						for(int p =0;p<PN.Popular_Opt.Count;p++)
						{
							if(PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="AD" || PN.Popular_Opt[p].ToString().Trim().Substring(0,2) =="WD" )
							{
								lblmgrp.Text="AP_Base";
								lbltable.Text="WEB_ADPrice_Velan_TableV1";
							}
						}
					}
				}
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal seal=0.00m;

				st =Convert.ToDecimal(PN.Stroke);
				sp =db.SelectStrokeA(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				sp =Decimal.Round(sp,2);
				
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(ind.Trim());
				temp =temp + temp * (index /100);
				decimal discount =0.00m;

				lblD.Text=db.SelectDiscount(lst[1].ToString().Trim(),"A");
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100));
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text=sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
				//mount price
				decimal mp =0.00m;
				string mgrp = lblmgrp.Text.ToString();
				mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100));
				mprice=Decimal.Round(mp,2); 
				if(PN.Mount.Trim() =="NX1" ||PN.Mount.Trim() =="NX3")
				{
					mprice=mprice + 5.00m;
				}
				if(PN.Mount.Trim().Substring(0,1) =="I" || PN.Mount.Trim().Substring(0,1) =="M" )
				{
					decimal a1=0.00m;
					a1 =db.SelectISSMSSPrice(PN.Mount.Trim());
					if(a1 >0)
					{
						a1 =a1 + a1 * (index /100);
						a1= a1*(1 - (discount/100)); 
						a1 =Decimal.Round(a1,2);
						mprice +=a1;
					}
				}
				else if(PN.Mount.Trim().Substring(0,2) =="GR" )
				{
					decimal a1=0.00m;
					a1 =db.SelectGRPrice(PN.Mount.Trim());
					if(a1 >0)
					{
						a1 =a1 + a1 * (index /100);
						a1= a1*(1 - (discount/100)); 
						a1 =Decimal.Round(a1,2);
						mprice +=a1;
					}
				}
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}

				if(PN.Seal_Comp.ToString().Trim() =="L")
				{
					seal =db.SelectAddersPrice("SL",commn.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="F")
				{
					seal =db.SelectAddersPrice("SF",commn.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="E")
				{
					seal =db.SelectAddersPrice("SE",commn.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				seal=Decimal.Round(seal,2);

				decimal rodend=0.00m;
				if(PN.Rod_End.ToString().Substring(0,1) !="N" &&PN.Rod_End.ToString().Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd",commn.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="True";
					}
				}
				rodend=Decimal.Round(rodend,2);
				seal =seal + seal * (index /100);
				rodend =rodend + rodend * (index /100);
				//issue #315 start
				seal = seal*(1 - (discount/100));
				seal = Decimal.Round(seal,2);
				rodend = rodend*(1 - (discount/100));
				rodend = Decimal.Round(rodend,2);
				//issue #315 end
				decimal total=0.00m;
				total=sprice + mprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}
		public void ATPricing()
		{
			try
			{
				ArrayList lst =new ArrayList();
				lst =(ArrayList)Session["User"];
				string comtable="";
				//issue #118 start
				//backup
				//if(lst[6].ToString().Trim() =="1002")
				//update
				if(lst[6].ToString().Trim() =="1002" || lst[6].ToString().Trim() =="1021")
				//issue #118 end
				{
					lblmgrp.Text="AP_Base";
					lbltable.Text="WEB_ATPrice_SVC_TableV1";
					comtable="WEB_SVCSeriesATCommAdders_TableV1";
				}
				else
				{
					lblmgrp.Text="AP_Base";
					lbltable.Text="WEB_ATPrice_Master_TableV1";
					comtable="WEB_SeriesATCommAdders_TableV1";
				}		
				coder PN= (coder)Session["Coder"];
											
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal seal=0.00m;

				st =Convert.ToDecimal(PN.Stroke);
				sp =db.SelectStrokeA(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				sp =Decimal.Round(sp,2);
				
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(db.SelectPriceIndex("AT").ToString());
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				lblD.Text=db.SelectDiscount(lst[1].ToString().Trim(),"AT");
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100));
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text=sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
				//mount price
				decimal mp =0.00m;
				string mgrp = lblmgrp.Text.ToString();
				mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100)); 
				
				mprice=Decimal.Round(mp,2);
				if(PN.Mount.Trim().Substring(0,1) =="I" || PN.Mount.Trim().Substring(0,1) =="M" )
				{
					decimal a1=0.00m;
					a1 =db.SelectISSMSSPrice(PN.Mount.Trim());
					if(a1 >0)
					{
						a1 =a1 + a1 * (index /100);
						a1= a1*(1 - (discount/100)); 
						a1 =Decimal.Round(a1,2);
						mprice +=a1;
					}
				}
				else if(PN.Mount.Trim().Substring(0,2) =="GR" )
				{
					decimal a1=0.00m;
					a1 =db.SelectGRPrice(PN.Mount.Trim());
					if(a1 >0)
					{
						a1 =a1 + a1 * (index /100);
						a1= a1*(1 - (discount/100)); 
						a1 =Decimal.Round(a1,2);
						mprice +=a1;
					}
				}
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}

				if(PN.Seal_Comp.ToString().Trim() =="L")
				{
					seal =db.SelectAddersPrice("SL",comtable,PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="F")
				{
					seal =db.SelectAddersPrice("SF",comtable,PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="E")
				{
					seal =db.SelectAddersPrice("SE",comtable,PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				seal=Decimal.Round(seal,2);

				decimal rodend=0.00m;
				if(PN.Rod_End.ToString().Substring(0,1) !="N" &&PN.Rod_End.ToString().Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd",comtable,PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="True";
					}
				}
				rodend=Decimal.Round(rodend,2);
				seal =seal + seal * (index /100);
				rodend =rodend + rodend * (index /100);
				//issue #315 start
				seal = seal*(1 - (discount/100));
				seal = Decimal.Round(seal,2);
				rodend = rodend*(1 - (discount/100));
				rodend = Decimal.Round(rodend,2);
				//issue #315 end
				decimal total=0.00m;
				total=sprice + mprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s=s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}
		public void SAPricing()
		{
			try
			{
				lblmgrp.Text="AP_Base";
				lbltable.Text="WEB_APrice_SVC_TableV1";
				coder PN= (coder)Session["Coder"];
											
				decimal sp =0.00m;
				decimal st =0.00m;
				decimal sprice =0.00m;
				decimal mprice =0.00m;
				decimal seal=0.00m;

				st =Convert.ToDecimal(PN.Stroke);
				sp =db.SelectStrokeA(lbltable.Text.Trim(), PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				sp =Decimal.Round(sp,2);
				decimal temp =sp * st;
				decimal index =0.00m;
				index =Convert.ToDecimal(db.SelectPriceIndex("SA").ToString());
				//issue #644 start
				//issue #644 end
				temp =temp + temp * (index /100);
				decimal discount =0.00m;
				ArrayList lst=new ArrayList();
				lst=(ArrayList)Session["User"];
				lblD.Text=db.SelectDiscount(lst[1].ToString().Trim(),"A");
				discount=Convert.ToDecimal(lblD.Text);
				sprice =temp*(1 - (discount/100));
				sprice =Decimal.Round(sprice,2);
				lblstroke.Text=sprice.ToString();
				if(sprice ==0)
				{
					lblstatus.Text ="True";
				}
				//mount price
				decimal mp =0.00m;
				string mgrp = lblmgrp.Text.ToString();
				mp =db.SelectMountPrice(mgrp.ToString().Trim(),lbltable.Text.Trim(),PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
				mp =mp + mp * (index /100);
				mp= mp*(1 - (discount/100)); 
				if(PN.Mount.Trim().Substring(0,1) =="I" || PN.Mount.Trim().Substring(0,1) =="M" )
				{
					decimal a1=0.00m;
					a1 =db.SelectISSMSSPrice(PN.Mount.Trim());
					if(a1 >0)
					{
						a1 =a1 + a1 * (index /100);
						a1= a1*(1 - (discount/100)); 
						a1 =Decimal.Round(a1,2);
						mprice +=a1;
					}
				}
				mprice=Decimal.Round(mp,2);
				if(mprice ==0)
				{
					lblstatus.Text ="True";
				}

				if(PN.Seal_Comp.ToString().Trim() =="L")
				{
					seal =db.SelectAddersPrice("SL","WEB_SVCSeriesATCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="F")
				{
					seal =db.SelectAddersPrice("SF","WEB_SVCSeriesATCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				else if(PN.Seal_Comp.ToString().Trim() =="E")
				{
					seal =db.SelectAddersPrice("SE","WEB_SVCSeriesATCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(seal ==0)
					{
						lblstatus.Text ="True";
					}
				}
				seal=Decimal.Round(seal,2);

				decimal rodend=0.00m;
				if(PN.Rod_End.ToString().Substring(0,1) !="N" &&PN.Rod_End.ToString().Trim().Substring(0,1) !="A")
				{
					rodend =db.SelectAddersPrice("MetricRodEnd","WEB_SVCSeriesATCommAdders_TableV1",PN.Bore_Size.ToString().Trim(),PN.Rod_Diamtr.ToString().Trim());
					if(rodend ==0)
					{
						lblstatus.Text ="True";
					}
				}
				rodend=Decimal.Round(rodend,2);
				seal =seal + seal * (index /100);
				rodend =rodend + rodend * (index /100);
				decimal total=0.00m;
				//issue #315 start
				seal = seal*(1 - (discount/100));
				seal = Decimal.Round(seal,2);
				rodend = rodend*(1 - (discount/100));
				rodend = Decimal.Round(rodend,2);
				//issue #315 end
				total=sprice + mprice +seal + rodend ;
				lbltotal.Text=total.ToString();
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}
		private void LBSend_Click(object sender, System.EventArgs e)
		{
			if(Page.IsValid)
			{
				lblgenerateprice.Visible=true;
				QItems qitem=new QItems();
				qitem =(QItems)Session["Part"];
				Quotation quote=new Quotation();
				quote =(Quotation)Session["Quote"];

				ArrayList lst1 =new ArrayList();
				lst1 =(ArrayList)Session["User"];
				MailMessage mail=new MailMessage();
				SmtpMail.SmtpServer ="k2smtpout.secureserver.net"; 
				mail.From= "admin@cowandynamics.com";
				//#588
				if(quote.CompanyID.ToString().Trim()=="1003" || quote.CompanyID.ToString().Trim()=="1015"|| quote.CompanyID.ToString().Trim()=="1001" 
					|| quote.CompanyID.ToString().Trim()=="1009" || quote.CompanyID.ToString().Trim()=="1011" || quote.CompanyID.ToString().Trim()=="1012" || quote.CompanyID.ToString().Trim()=="1019" || quote.CompanyID.ToString().Trim()=="1020"  || quote.CompanyID.ToString().Trim()=="1027" || quote.CompanyID.ToString().Trim()=="1028" || quote.CompanyID.ToString().Trim()=="1029" || quote.CompanyID.ToString().Trim()=="1030")
				{
					//mail.To = "matt@cowandynamics.com";
					mail.To = "jenny.l@cowandynamics.com";
					mail.Cc="dtaranu@cowandynamics.com;jbehara@cowandynamics.com";
				}
				//issue #118 start
				//backup
				//else if(quote.CompanyID.ToString().Trim()=="1004" || quote.CompanyID.ToString().Trim()=="1002" || quote.CompanyID.ToString().Trim()=="1016")
				//update
				else if(quote.CompanyID.ToString().Trim()=="1004" || quote.CompanyID.ToString().Trim()=="1002" || quote.CompanyID.ToString().Trim()=="1016"  || quote.CompanyID.ToString().Trim()=="1021"  || quote.CompanyID.ToString().Trim()=="1026")
				//issue #118 end
				{
					mail.To = "dtaranu@cowandynamics.com";
					//mail.Cc="matt@cowandynamics.com;jbehara@cowandynamics.com";
					mail.Cc="jenny.l@cowandynamics.com;jbehara@cowandynamics.com";
				}
				else if(quote.CompanyID.ToString().Trim()=="1008" || quote.CompanyID.ToString().Trim()=="1000")
				{
					mail.To = "dtaranu@cowandynamics.com";
					//mail.Cc="matt@cowandynamics.com;clemire@cowandynamics.com;jbehara@cowandynamics.com;";
					mail.Cc="jenny.l@cowandynamics.com;clemire@cowandynamics.com;jbehara@cowandynamics.com;";
				}
				else
				{
					mail.To = "dtaranu@cowandynamics.com";
					mail.Cc="jbehara@cowandynamics.com";
					mail.Bcc="admin@cowandynamics.com";
				}
//				if(qitem.Cusomer_ID.Trim()=="Reseau CB/DIV. CAN Bearings (Qu�bec)" || qitem.Cusomer_ID.Trim()=="Engrenage Provincial Inc")
//				{
//					mail.Cc="jbehara@cowandynamics.com;clemire@cowandynamics.com;";
//				}
//				else
//				{
//					mail.Cc="jbehara@cowandynamics.com";
//				}
				mail.BodyFormat =MailFormat.Html;
				mail.Subject=   "["+DDLPriority.SelectedItem.Text.Trim() +"] New Quote from "+quote.Customer.Trim();
				if(DDLPriority.SelectedIndex ==0)
				{
					mail.Priority =MailPriority.Normal;
				}
				else if(DDLPriority.SelectedIndex ==1)
				{
					mail.Priority =MailPriority.High;
				}
				mail.Body =						
					"<hr color='#FF0000'>Bonjour, <br> "+lst1[0].ToString()+" a une nouvelle demande de prix:"
					+"<TABLE id='Table1' borderColor='#0000ff' cellSpacing='1' cellPadding='1' align='left' border='1'>"
					+"<TR><TD noWrap>Date de demande :</TD><TD noWrap>"+quote.Quotedate.Trim()+"</TD></TR>"
					+"<TR><TD noWrap>No de demande :</TD><TD noWrap>"+quote.QuoteNo.ToString().Trim()+"</TD></TR>"
					+"<TR><TD noWrap>Part No :</TD><TD noWrap>"+qitem.PartNo.ToString().Trim()+"</TD></TR>"
					+"<TR><TD noWrap>Special Request Note :</TD><TD noWrap>"+TxtSpecialReq.Text.Trim()+"</TD></TR></TABLE>"
					+"<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>Clic sur le lien ci-dessous � r�pondre: <a href='http://172.16.0.252/'>Quote System</a>"
					+"<br><br><hr> Merci <br>I-Cylinder<br><hr color='#FF0000'><br>"								
					+"<hr color='#FF0000'>Hi, <br> "+lst1[0].ToString()+" has entered a new request for a quote:"
					+"<TABLE id='Table1' borderColor='#0000ff' cellSpacing='1' cellPadding='1' align='left' border='1'>"
					+"<TR><TD noWrap>Quote Date :</TD><TD noWrap>"+quote.Quotedate.Trim()+"</TD></TR>"
					+"<TR><TD noWrap>Quote No :</TD><TD noWrap>"+quote.QuoteNo.ToString().Trim()+"</TD></TR>"
					+"<TR><TD noWrap>Part No :</TD><TD noWrap>"+qitem.PartNo.ToString().Trim()+"</TD></TR>"
					+"<TR><TD noWrap>Special Request Note :</TD><TD style='width:200' noWrap>"+TxtSpecialReq.Text.Trim()+"</TD></TR></TABLE>"
					+"<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>Please log into <a href='http://172.16.0.252/'>Quote System</a> to complete the Quote."
					+"<br><br><hr> Thanks <br>I-Cylinder<br><hr color='#FF0000'>";
				if(qitem.DWG.Trim() !="")
				{
					MailAttachment attach;
					attach=new MailAttachment(Request.PhysicalApplicationPath+"icyl_drawing/"+ qitem.DWG.ToString().Trim());
					mail.Attachments.Add(attach);
				}					
				//issue #582 start
				mail.Body += csSignature.Get_Admin();
				//issue #582 end
				SmtpMail.Send(mail);
				//				Message message = new Message();
				//				message.From.Email = "admin@cowandynamics.com";
				//				message.To.Add( "dtaranu@cowandynamics.com" );
				//				message.Cc.Add( "rwenker@cowandynamics.com" );
				//				message.Cc.Add( "jbehara@cowandynamics.com" );
				//				message.Bcc.Add( "admin@cowandynamics.com" );
				//				message.Subject ="["+DDLPriority.SelectedItem.Text.Trim() +"] New Quote from "+quote.Customer.Trim();
				//				message.Charset = System.Text.Encoding.GetEncoding("iso-8859-7");
				//				if(DDLPriority.SelectedIndex ==0)
				//				{
				//					message.Priority =Priority.Medium;
				//				}
				//				else if(DDLPriority.SelectedIndex ==1)
				//				{
				//					message.Priority =Priority.Highest;
				//				}
				//				message.BodyHtml ="<hr color='#FF0000'>Bonjour, <br> "+lst1[0].ToString()+" a une nouvelle demande de prix:"
				//					+"<TABLE id='Table1' borderColor='#0000ff' cellSpacing='1' cellPadding='1' align='left' border='1'>"
				//					+"<TR><TD noWrap>Date de demande :</TD><TD noWrap>"+quote.Quotedate.Trim()+"</TD></TR>"
				//					+"<TR><TD noWrap>No de demande :</TD><TD noWrap>"+quote.QuoteNo.ToString().Trim()+"</TD></TR>"
				//					+"<TR><TD noWrap>Part No :</TD><TD noWrap>"+qitem.PartNo.ToString().Trim()+"</TD></TR>"
				//					+"<TR><TD noWrap>Special Request Note :</TD><TD noWrap>"+TxtSpecialReq.Text.Trim()+"</TD></TR></TABLE>"
				//					+"<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>Clic sur le lien ci-dessous � r�pondre: <a href='http://172.16.0.252/'>Quote System</a>"
				//					+"<br><br><hr> Merci <br>I-Cylinder<br><hr color='#FF0000'><br>"								
				//					+"<hr color='#FF0000'>Hi, <br> "+lst1[0].ToString()+" has entered a new request for a quote:"
				//					+"<TABLE id='Table1' borderColor='#0000ff' cellSpacing='1' cellPadding='1' align='left' border='1'>"
				//					+"<TR><TD noWrap>Quote Date :</TD><TD noWrap>"+quote.Quotedate.Trim()+"</TD></TR>"
				//					+"<TR><TD noWrap>Quote No :</TD><TD noWrap>"+quote.QuoteNo.ToString().Trim()+"</TD></TR>"
				//					+"<TR><TD noWrap>Part No :</TD><TD noWrap>"+qitem.PartNo.ToString().Trim()+"</TD></TR>"
				//					+"<TR><TD noWrap>Special Request Note :</TD><TD style='width:200' noWrap>"+TxtSpecialReq.Text.Trim()+"</TD></TR></TABLE>"
				//					+"<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>Please log into <a href='http://172.16.0.252/'>Quote System</a> to complete the Quote."
				//					+"<br><br><hr> Thanks <br>I-Cylinder<br><hr color='#FF0000'>";
				//				Smtp.Send( message, "smtpout.secureserver.net", 80, GetDomain( message.From.Email ),SmtpAuthentication.Login,"admin@cowandynamics.com","hockey13" );
				//				//				Smtp.Send( message, "smtp10.bellnet.ca", 25, GetDomain( message.From.Email ) );
				////				Session["Coder"]=null;
				Session["Accessories"]=null;
				Session["Style"] =null;
				Response.Redirect("Result.aspx?id="+quote.QuoteNo.Trim());		
			}
		}
		private string GetDomain( string email )
		{
			int index = email.IndexOf( '@' );
			return email.Substring( index + 1 );
		}

		private void LBBack_Click(object sender, System.EventArgs e)
		{
			ArrayList lst1 =new ArrayList();
			lst1 =(ArrayList)Session["User"];
			cd1= (coder)Session["Coder"];
			if(cd1.Series.Trim() =="SA")
			{
				Response.Redirect("SlurryfloSeriesA.aspx");
			}
				//issue #244 start
			else if(cd1.CompanyID =="1017")
			
			{
					if(cd1.CompanyID.Trim() =="1017")
				  {
					  if(cd1.Series.Trim() =="A" || cd1.Series.Trim() =="AT" )
					  {
						  Response.Redirect("Sistag_Page2.aspx");
					  }
				  }
			}
				//issue #244 end
			else if(cd1.Series.Trim() =="AT")
			{
				if(cd1.CompanyID !=null)
				{
					if(cd1.CompanyID.ToString() =="1003")
					{
						Response.Redirect("EV_SeriesAT_Trans.aspx");
					}
					else
					{
						Response.Redirect("Icylinder_page_Trans.aspx");
					}
				}
				else
				{
					Response.Redirect("Icylinder_page_Trans.aspx");
				}
			}
			else if(cd1.Series.Trim() =="R")
			{
				Response.Redirect("Icylinder_Page4.aspx");
			}
			else if(cd1.Specials !=null)
			{
				if(lst1[6].ToString().Trim() =="1004")
				{
					if(cd1.CompanyID !=null)
					{
						if(cd1.CompanyID.Trim() =="1004")
						{
							if(cd1.Specials.Trim() =="YES")
							{
								Response.Redirect("Velan_Page2.aspx");
							}
							else if(cd1.Specials.Trim() =="SPECIAL")
							{
								Response.Redirect("Velan_SeriesA_Specials.aspx");
							}
							else if(cd1.Specials.Trim() =="STD")
							{
								Response.Redirect("Velan_Page1.aspx");
							}
							else if(cd1.Specials.Trim() =="ADV")
							{
								Response.Redirect("Velan_SeriesA.aspx");
							}
						}
					}
				}
				else if(lst1[6].ToString().Trim() =="1003")
				{
					if(cd1.CompanyID !=null)
					{
						if(cd1.CompanyID.Trim() =="1003")
						{
							if(cd1.Specials.Trim() =="YES")
							{
								Response.Redirect("EV_SeriesA_Specials.aspx");
							}
							else if(cd1.Specials.Trim() =="NO")
							{
								Response.Redirect("EV_SeriesA.aspx");
							}
							else
							{
								Response.Redirect("EV_SeriesA.aspx");
							}
						}
					}
				}
				else if(lst1[6].ToString().Trim() =="1006")
				{
					if(cd1.CompanyID !=null)
					{
						if(cd1.CompanyID.Trim() =="1006")
						{
							if(cd1.Specials.Trim() =="YES")
							{
								Response.Redirect("ART_SeriesA_Page1.aspx");
							}
							if(cd1.Specials.Trim() =="NO")
							{
								Response.Redirect("ART_SeriesA_Page2.aspx");
							}
						}
					}
				}
			}
			else
			{
				Response.Redirect("Icylinder_PageAcc.aspx");
			}
		}
		private string UploadFile(object Sender,EventArgs E)
		{
			string file="";
			if (UploadDwg.PostedFile !=null) //Checking for valid file
			{	
				string StrFileName ="ICyl"+DateTime.Now.Ticks.ToString()+"_CustomerDwg.pdf" ;
				string StrFileType = UploadDwg.PostedFile.ContentType ;
				int IntFileSize =UploadDwg.PostedFile.ContentLength;
				if (IntFileSize <=0)
				{
					lblstatus.Text="Uploading of file " + StrFileName + " failed ";
				}
				else
				{
					UploadDwg.PostedFile.SaveAs(Server.MapPath("./icyl_drawing/" + StrFileName));
					file=StrFileName.ToString();
				}
			}
			return file;
		}
		private string UploadFileTemp(object Sender,EventArgs E)
		{
			string file="";
			if (UploadDwg.PostedFile !=null) //Checking for valid file
			{	
				string StrFileName = "ICyl"+DateTime.Now.Ticks.ToString()+"_CustomerDwg.pdf" ;
				string StrFileType = UploadDwg.PostedFile.ContentType ;
				int IntFileSize =UploadDwg.PostedFile.ContentLength;
				if (IntFileSize <=0)
				{
					lblstatus.Text="Uploading of file " + StrFileName + " failed ";
				}
				else
				{
					UploadDwg.PostedFile.SaveAs(Server.MapPath("./temp_drawing/" + StrFileName));
					lblfilename.Text=UploadDwg.Value.ToString();
					file=StrFileName.ToString();
				}
			}
			return file;
		}
		private void LbPreview_Click(object sender, System.EventArgs e)
		{
			if(UploadDwg.Value.Trim() !="")
			{
				if(UploadDwg.PostedFile.ContentType.ToString() =="application/pdf")
				{
					lblstatus.Text="";
					string upload="";
					if(UploadDwg.Value.Trim()!="")
					{
						upload=UploadFileTemp(sender,e);
						cbupload.Visible=true;
					}
					if(upload.Trim() !="")
					{
						lblfile.Text=upload.Trim();
						LblInfo.Text="<script language='javascript'>window.open('temp_drawing/"+upload.Trim()+"','_blank','toolbar=yes,width=1000,height=750,resizable=yes,top=0,left=0')</script>";						
					}
				}
				else
				{
					LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Please select a PDF file!!!')</script>";
				}
			}
			else
			{
				LblInfo.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Please select a file to preview!!!')</script>";
			}
		}
	}
}
