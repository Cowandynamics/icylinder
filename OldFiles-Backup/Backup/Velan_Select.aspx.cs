using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for Velan_Select.
	/// </summary>
	public class Velan_Select : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.ImageButton IBRod;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Panel Panelnew;
		protected System.Web.UI.WebControls.LinkButton LBcap;
		protected System.Web.UI.WebControls.ImageButton IBCap;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Panel PanelCassic;
		protected System.Web.UI.WebControls.LinkButton LBBatch;
		protected System.Web.UI.WebControls.Label Label2;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["User"] ==null)
			{
				Response.Redirect("Login.aspx");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.IBCap.Click += new System.Web.UI.ImageClickEventHandler(this.IBCap_Click);
			this.LBcap.Click += new System.EventHandler(this.LBcap_Click);
			this.IBRod.Click += new System.Web.UI.ImageClickEventHandler(this.IBRod_Click);
			this.LBBatch.Click += new System.EventHandler(this.LBBatch_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void LBcap_Click(object sender, System.EventArgs e)
		{
			//issue #137 start
			//backup
			//Response.Redirect("Velan_Page1.aspx?p=new");
			//update
			Response.Redirect("Velan_SeriesA.aspx?p=new");

		}

		private void IBCap_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			Response.Redirect("Velan_Page1.aspx?p=new");
		}

		private void LBRod_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Velan_SeriesA.aspx?p=new");
		}

		private void IBRod_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			Response.Redirect("Velan_BachQuote.aspx");
		}

		private void LBBatch_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Velan_BachQuote.aspx");
		}
	}
}
