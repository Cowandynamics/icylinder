using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for ViewUploadedFiles.
	/// </summary>
	public class ViewUploadedFiles : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lbl;
		protected System.Web.UI.WebControls.Label lblspec;
		protected System.Web.UI.WebControls.Label Lblerror;
		protected System.Web.UI.WebControls.Label Lblerr;
		protected System.Web.UI.WebControls.DataGrid DGView;
		public string AccessExcel(string name)
		{
			string str="";
			string constr="Provider=Microsoft.Jet.OLEDB.4.0;Data Source="+Request.PhysicalApplicationPath+"dbfile/"+name.ToString().Trim()
				+";Extended Properties='Excel 8.0;HDR=Yes;IMEX=2'";
			OleDbConnection myConnection = new OleDbConnection(constr);
			try
			{
				//string CommandText = "select [Line No#],[Required Qty],[Style],[Series],[Cylinder Stroke (in)],[Cylinder Bore (in)],[Thrust (lbs)],[Safety Factor (%)],[Minimum Operating Pressure (psi)],[Customer Ref#] from [CylinderData$] Where [Line No#] >0 ";  
				string CommandText = "select * from [CylinderData$] Where [Line No#] >0 ";  
				OleDbCommand myCommand = new OleDbCommand(CommandText, myConnection);    
				myConnection.Open(); 
				DataSet ds=new DataSet();
				OleDbDataAdapter oledad =new OleDbDataAdapter();
				oledad.SelectCommand =myCommand;
				oledad.Fill(ds);
				//remove extra columns as F14 ..
				int intColumns =ds.Tables[0].Columns.Count;
				for(int i = 0;i<intColumns;i++)
				{
					if(ds.Tables[0].Columns[intColumns-i-1].Caption.Substring(0,1)=="F") ds.Tables[0].Columns.RemoveAt(intColumns-i-1);

				}

				DGView.DataSource = ds;
				DGView.DataBind();   
				myConnection.Close();		
			}
			catch(Exception ex)
			{
				str=ex.Message;
			}
			finally
			{
				myConnection.Close();
			}
			return str;
		}
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			try
			{
				Lblerror.Text="";
				if(Request.QueryString["file"] !=null)
				{
					string str="";
					str=AccessExcel(Request.QueryString["file"].ToString());
					if(str.Trim() !="")
					{
						Lblerror.Text="<script language='javascript'>" + Environment.NewLine +"window.alert('"+str+"')</script>";
					}
					lbl.Text="<br><a href='dbfile/"+Request.QueryString["file"].ToString().Trim()+"' target='_blank'>Click here to download (excel format)</a>";					
				}	
				if(Request.QueryString["qno"] !=null)
				{
					string specsheet="";
					DBClass db=new DBClass();
					specsheet=db.SelectSpecSheet(Request.QueryString["qno"].ToString().Trim());
					if(specsheet.Trim() !="")
					{
						lblspec.Text="<br><a href='specsheet/"+specsheet.ToString().Trim()+"' target='_blank'>Click here to download  Spec sheet</a>";
					}
					else
					{
						lblspec.Text="There is no Spec sheet attached with this item";
					}
				}
			}
			catch(Exception ex)
			{
				Lblerror.Text="<script language='javascript'>" + Environment.NewLine +"window.alert('"+ex.Message+"')</script>";
			}
		}
        
		public void DGView_Item_Command(Object o, DataGridItemEventArgs e) 
		{
			
			if((e.Item.ItemType ==ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem) ) 
			{ 
				
			
			} 
		}





		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
