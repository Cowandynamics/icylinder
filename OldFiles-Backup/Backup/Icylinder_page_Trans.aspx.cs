using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for Icylinder_page_Trans.
	/// </summary>
	public class Icylinder_page_Trans : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblseries;
		protected System.Web.UI.WebControls.LinkButton BtnNext;
		protected System.Web.UI.WebControls.Label omega;
		protected System.Web.UI.WebControls.TextBox Txtxi;
		protected System.Web.UI.WebControls.Label lblxi;
		protected System.Web.UI.WebControls.LinkButton LBBack;
		protected System.Web.UI.WebControls.RadioButtonList RBLFittings;
		protected System.Web.UI.WebControls.RadioButtonList RBLTrans;
		protected System.Web.UI.WebControls.Label Label2;
		coder cd1=new coder();
		protected System.Web.UI.WebControls.Label lbrange;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.RadioButtonList RBLRemote;
		DBClass db=new DBClass();
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["User"] !=null)
			{
				if(! IsPostBack)
				{
					if(	Session["Coder"] !=null)
					{
						ListItem litem=new ListItem();
						cd1=(coder)Session["Coder"];
						if (cd1.Series.Trim() =="AT")
						{
							RBLTrans.Items.Clear();
							if(cd1.Bore_Size.Trim() !="K" && cd1.Bore_Size.Trim() !="L" && cd1.Bore_Size.Trim() !="M")
							{
								litem =new ListItem("T1-Analog Output (Ratiometric) With Discrete Switches<br /><img alt='Analog Output (Ratiometric) With Discrete Switches' src='images\\T1.jpg'>", "T1");
								RBLTrans.Items.Add(litem);
							}
							litem =new ListItem("T2-Analog Output (Ratiometric)<br /><img alt='Analog Output (Ratiometric)' src='images\\T2.jpg'>","T2");
							RBLTrans.Items.Add(litem);
							litem =new ListItem("T3-Analog Output (0 - 20 mA)<br /><img alt='Analog Output (0 - 20 mA)' src='images\\T3.jpg'>", "T3");
							RBLTrans.Items.Add(litem);
							litem =new ListItem("T4-Analog Output (4 - 20 mA)<br /><img alt='Analog Output (4 - 20 mA)' src='images\\T4.jpg'>", "T4");
							RBLTrans.Items.Add(litem);
							litem =new ListItem("T5-Analog Output (0 -5/0 - 10 Vdc)<br /><img alt='Analog Output (0 -5/0 - 10 Vdc)' src='images\\T5.jpg'>", "T5");
							RBLTrans.Items.Add(litem);
							RBLTrans.SelectedIndex=0;
							RBLTrans_SelectedIndexChanged(sender,e);
						}
						if(	Session["Coder"] !=null)
						{
							cd1=new coder();
							cd1=(coder)Session["Coder"];
							if(cd1.Transducer !=null)
							{
								if(cd1.Transducer.Trim() !="")
								{
									RBLTrans.SelectedValue=cd1.Transducer.Substring(0,2);
									RBLTrans_SelectedIndexChanged(sender,e);
									if(cd1.Transducer.Trim().Length >2)
									{
										Txtxi.Text="10";
										if(cd1.Transducer.Substring(2,1) =="S")
										{
											//issue #371 start
											//back
											//RBLFittings.SelectedIndex=1;
											//update
											RBLFittings.SelectedValue="S";
											//issue #371 end
											Txtxi.Text=cd1.Transducer.Substring(3);
										}
										else if(cd1.Transducer.Substring(2,1) =="R")
										{
											RBLFittings.Enabled=false;
											Txtxi.Text=cd1.Transducer.Substring(3);
											RBLRemote.SelectedIndex=0;
										}
										else
										{
											RBLFittings.SelectedIndex=0;
											RBLRemote.SelectedIndex=1;
											Txtxi.Text=cd1.Transducer.Substring(2);
										}
									}
									else if(RBLTrans.SelectedIndex <2)
									{
										Txtxi.Text="10";
									}
									
								}
							}
						}
					}
				}
			}
			else
			{
				Response.Redirect("UserLogin.aspx");
			}
		}
		private void Txtxi_TextChanged(object sender, System.EventArgs e)
		{
			try
			{
				if(Txtxi.Text.ToString().Trim() !="" && IsNumeric(Txtxi.Text.ToString().Trim()) ==true )
				{
			
					Txtxi.Text =String.Format("{0:00}",Convert.ToDecimal(Txtxi.Text));
				}
				else
				{
					Txtxi.Text="";
				}
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s.Replace("'"," ");
				lblseries.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}
		public static bool IsNumeric(string strInteger) 
		{
			try 
			{
				int intTemp =0;
				if(strInteger.ToString().StartsWith(".") == true)
				{
					for(int i=1; i< strInteger.Length;i++)
					{
						intTemp = Int32.Parse( strInteger.Substring(i,1) );
					}
				}
				else
				{
					for(int i=0; i< strInteger.Length;i++)
					{
						if (strInteger.ToString().Substring(i,1) !=".")
						{
							intTemp = Int32.Parse( strInteger.Substring(i,1) );
						}
					}
				}
				return true;
			} 
			catch (FormatException) 
			{
				return false;
			}    
		}

		private void RBLTrans_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(RBLTrans.SelectedIndex >-1)
			{
				if(RBLTrans.SelectedValue.Trim() !="T1" && RBLTrans.SelectedValue.Trim() !="T2")
				{
					lblxi.Visible=false;
					Txtxi.Visible=false;
					omega.Visible=false;
					lbrange.Visible=false;
				}
				else
				{
					lblxi.Visible=true;
					Txtxi.Visible=true;
					omega.Visible=true;
					lbrange.Visible=true;
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.RBLTrans.SelectedIndexChanged += new System.EventHandler(this.RBLTrans_SelectedIndexChanged);
			this.RBLRemote.SelectedIndexChanged += new System.EventHandler(this.RBLRemote_SelectedIndexChanged);
			this.LBBack.Click += new System.EventHandler(this.LBBack_Click);
			this.BtnNext.Click += new System.EventHandler(this.BtnNext_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void BtnNext_Click(object sender, System.EventArgs e)
		{
			if(RBLTrans.SelectedIndex >-1)
			{
				cd1=new coder();
				cd1 =(coder)Session["Coder"];
				string trans="";
				if(RBLTrans.SelectedItem.Value.ToString().Trim() !="T1" && RBLTrans.SelectedItem.Value.ToString().Trim() !="T2")
				{
					trans =RBLTrans.SelectedValue.ToString();
					if(RBLRemote.SelectedIndex ==0)
					{
						cd1.Transducer = trans.Trim()+"R";
					}
					else
					{
						cd1.Transducer = trans.Trim()+"S";
					}
				}
				else if(RBLTrans.SelectedItem.Value.ToString().Trim() =="T1" || RBLTrans.SelectedItem.Value.ToString().Trim()=="T2")
				{
					string trans1="";
					if(Txtxi.Text.Trim() !="10")
					{
						trans1=Txtxi.Text.Trim();
					}
					string tt=RBLTrans.SelectedValue.ToString().Trim()+trans1.Trim();
					if(RBLRemote.SelectedIndex ==0)
					{
						cd1.Transducer=tt.Insert(2,"R");
					}
					else
					{
//						if(RBLFittings.SelectedIndex ==1)
//						{
							cd1.Transducer=tt.Insert(2,"S");
//						}
//						else
//						{
//							cd1.Transducer=tt.Trim();
//						}
					}
				}
				Session["Coder"] =cd1;
				Response.Redirect("Icylinder_page6.aspx");

			}
		}

		private void LBBack_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("SlurryfloSeriesA.aspx");
		}

		private void RBLRemote_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(RBLRemote.SelectedIndex ==0)
			{
				RBLFittings.Enabled=false;
			}
			else
			{
				RBLFittings.Enabled=true;
			}
		}
	}
}
