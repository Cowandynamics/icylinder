<%@ Page language="c#" Codebehind="Icylinder_Page4.aspx.cs" AutoEventWireup="false" Inherits="iCylinderV1.Icylinder_Page4" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Icylinder_Page4</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 1000px; HEIGHT: 487px" cellSpacing="0" cellPadding="0"
				width="1000" bgColor="lightgrey" border="0">
				<TR>
					<TD width="50" height="20"></TD>
					<TD align="center" height="20"><asp:label id="Label2" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
							Font-Size="Small">Ports, Cushions & Seals</asp:label></TD>
					<TD width="50" height="20"></TD>
				</TR>
				<TR>
					<TD width="50"></TD>
					<TD align="center" bgColor="gainsboro">
						<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="899" border="1" borderColor="white">
							<TR>
								<TD align="center" width="449">
									<TABLE id="Table3" borderColor="#e3e1dc" cellSpacing="0" cellPadding="0" width="400" border="1">
										<TR>
											<TD style="HEIGHT: 18px" align="center" colSpan="2"><asp:label id="Label5" runat="server" Font-Underline="True" Font-Size="Smaller" Font-Bold="True"
													ForeColor="Maroon">Ports</asp:label></TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 141px" align="left" colSpan="2"><asp:radiobuttonlist id="RBLPorts" runat="server" Font-Size="Smaller" BorderStyle="None" RepeatLayout="Flow"
													RepeatColumns="1" RepeatDirection="Horizontal" AutoPostBack="True"></asp:radiobuttonlist></TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 18px" align="center" colSpan="2"><asp:label id="Label9" runat="server" Font-Underline="True" Font-Size="Smaller" Font-Bold="True"
													ForeColor="Maroon">Port Positions</asp:label></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 150px; HEIGHT: 18px" align="center"><asp:label id="Label1" runat="server" Font-Underline="True" Font-Size="Smaller">Head End</asp:label></TD>
											<TD style="WIDTH: 143px; HEIGHT: 18px" align="center"><asp:label id="Label3" runat="server" Font-Underline="True" Font-Size="Smaller">Cap End</asp:label></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 150px" align="center"><asp:radiobuttonlist id="RBLPP1" runat="server" Font-Size="Smaller" BorderStyle="None" RepeatColumns="2"
													RepeatDirection="Horizontal" AutoPostBack="True" Width="195px">
													<asp:ListItem Value="1">Position 1</asp:ListItem>
													<asp:ListItem Value="2">Position 2</asp:ListItem>
													<asp:ListItem Value="3">Position 3</asp:ListItem>
													<asp:ListItem Value="4">Position 4</asp:ListItem>
												</asp:radiobuttonlist></TD>
											<TD style="WIDTH: 143px" align="center"><asp:radiobuttonlist id="RBLPP2" runat="server" Font-Size="Smaller" BorderStyle="None" RepeatColumns="2"
													RepeatDirection="Horizontal" AutoPostBack="True" Width="195px">
													<asp:ListItem Value="1">Position 1</asp:ListItem>
													<asp:ListItem Value="2">Position 2</asp:ListItem>
													<asp:ListItem Value="3">Position 3</asp:ListItem>
													<asp:ListItem Value="4">Position 4</asp:ListItem>
												</asp:radiobuttonlist></TD>
										</TR>
									</TABLE>
								</TD>
								<TD align="center">
									<TABLE id="Table2" borderColor="#e3e1dc" cellSpacing="0" cellPadding="0" width="400" border="1">
										<TR>
											<TD style="HEIGHT: 18px" align="center" colSpan="2"><asp:label id="Label13" runat="server" Font-Underline="True" Font-Size="Smaller" Font-Bold="True"
													ForeColor="Maroon" Width="59px">Cushions</asp:label></TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 141px" align="left" colSpan="2"><asp:radiobuttonlist id="RBLCusions" runat="server" Font-Size="Smaller" BorderStyle="None" RepeatLayout="Flow"
													RepeatColumns="1" RepeatDirection="Horizontal" AutoPostBack="True" CellPadding="0"></asp:radiobuttonlist></TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 18px" align="center" colSpan="2"><asp:label id="Label12" runat="server" Font-Underline="True" Font-Size="Smaller" Font-Bold="True"
													ForeColor="Maroon">Cushion Positions</asp:label></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 150px; HEIGHT: 18px" align="center"><asp:label id="Label11" runat="server" Font-Underline="True" Font-Size="Smaller">Head End</asp:label></TD>
											<TD style="WIDTH: 143px; HEIGHT: 18px" align="center"><asp:label id="Label10" runat="server" Font-Underline="True" Font-Size="Smaller">Cap End</asp:label></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 150px" align="center"><asp:radiobuttonlist id="RBLCC1" runat="server" Font-Size="Smaller" BorderStyle="None" RepeatColumns="2"
													RepeatDirection="Horizontal" AutoPostBack="True" Width="195px">
													<asp:ListItem Value="1">Position 1</asp:ListItem>
													<asp:ListItem Value="2">Position 2</asp:ListItem>
													<asp:ListItem Value="3">Position 3</asp:ListItem>
													<asp:ListItem Value="4">Position 4</asp:ListItem>
												</asp:radiobuttonlist></TD>
											<TD style="WIDTH: 143px" align="center"><asp:radiobuttonlist id="RBLCC2" runat="server" Font-Size="Smaller" BorderStyle="None" RepeatColumns="2"
													RepeatDirection="Horizontal" AutoPostBack="True" Width="195px">
													<asp:ListItem Value="1">Position 1</asp:ListItem>
													<asp:ListItem Value="2">Position 2</asp:ListItem>
													<asp:ListItem Value="3">Position 3</asp:ListItem>
													<asp:ListItem Value="4">Position 4</asp:ListItem>
												</asp:radiobuttonlist></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
						<asp:label id="lbl" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
							Font-Size="Smaller" ForeColor="Red" Visible="False"></asp:label></TD>
					<TD width="50"></TD>
				</TR>
				<TR>
					<TD width="50"></TD>
					<TD align="center" bgColor="#dcdcdc"><asp:label id="lblxi" runat="server" Font-Underline="True" Font-Size="Smaller" Font-Bold="True"
							ForeColor="Maroon">Seals</asp:label><asp:radiobuttonlist id="RBLSeal" runat="server" Font-Size="Smaller" BorderStyle="None" RepeatColumns="5"
							RepeatDirection="Horizontal" CellPadding="0"></asp:radiobuttonlist></TD>
					<TD width="50"></TD>
				</TR>
				<TR>
					<TD align="right" width="50" height="30"><asp:linkbutton id="LBBack" runat="server" Font-Size="Smaller" Font-Bold="True">Back</asp:linkbutton></TD>
					<TD align="center" bgColor="#dcdcdc" height="30"><asp:linkbutton id="LBAdvanced" runat="server" Font-Size="9pt">Would you like to customize cylinder with more  advanced options?</asp:linkbutton></TD>
					<TD width="50" height="30"><asp:linkbutton id="BtnNext" runat="server" Font-Size="Smaller" Font-Bold="True">Next</asp:linkbutton></TD>
				</TR>
				<TR>
					<TD width="50" height="20"></TD>
					<TD align="center" height="20"><asp:label id="lblseries" runat="server" BackColor="Transparent" BorderColor="Transparent"
							Font-Size="XX-Small" ForeColor="Red" Visible="False">Please select/enter all option!</asp:label></TD>
					<TD width="50" height="20"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
