using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for Icylinder_page3.
	/// </summary>
	public class Icylinder_page3 : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.LinkButton BtnNext;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label lblxi;
		protected System.Web.UI.WebControls.TextBox Txtxi;
		protected System.Web.UI.WebControls.LinkButton LBBack;
		protected System.Web.UI.WebControls.TextBox TxtBBC;
		protected System.Web.UI.WebControls.Label lblt1;
		protected System.Web.UI.WebControls.TextBox TxtBBH;
		protected System.Web.UI.WebControls.Label lblt2;
		coder cd1=new coder();
		protected System.Web.UI.WebControls.RadioButtonList RBLMount;
		protected System.Web.UI.WebControls.Label lblseries;
		DBClass db=new DBClass();
		private void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["User"] !=null)
			{
				if(! IsPostBack)
				{
					if(	Session["Coder"] !=null)
					{
						ArrayList blist=new ArrayList();
						ArrayList blist1=new ArrayList();
						ArrayList blist2=new ArrayList();
						ArrayList blist3=new ArrayList();
						ListItem litem=new ListItem();
						cd1=(coder)Session["Coder"];
						switch (cd1.Series.Trim())
						{
							case "A":
								if(cd1.DoubleRod.Trim() =="Yes")
								{
									blist =db.SelectOptions_Mounts("Mount_Code", "Mount_t1", "Mount_t2","WEB_MountA_TableV1","SeriesAD",cd1.Bore_Size.Trim());
								}
								else
								{
									blist =db.SelectOptions_Mounts("Mount_Code", "Mount_t1", "Mount_t2","WEB_MountA_TableV1","SeriesA",cd1.Bore_Size.Trim());
								}
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								blist3 =(ArrayList)blist[2];
								for(int i=0;i<blist2.Count;i++)
								{
								
									litem =new ListItem((String.Format(blist2[i].ToString()+"<br /><img alt='"+blist3[i].ToString() +"' src='{0}'>", "mounts\\M"+blist1[i].ToString().Trim()+".jpg")), blist1[i].ToString());
									RBLMount.Items.Add(litem);
								}
								break;
							case "PA":
								if(cd1.DoubleRod.Trim() =="Yes")
								{
									blist =db.SelectOptions_Mounts("Mount_Code", "Mount_t1", "Mount_t2","WEB_MountPA_TableV1","SeriesPAD",cd1.Bore_Size.Trim());
								}
								else
								{
									blist =db.SelectOptions_Mounts("Mount_Code", "Mount_t1", "Mount_t2","WEB_MountPA_TableV1","SeriesPA",cd1.Bore_Size.Trim());
								}
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								blist3 =(ArrayList)blist[2];
								for(int i=0;i<blist2.Count;i++)
								{
								
									litem =new ListItem((String.Format(blist2[i].ToString()+"<br /><img alt='"+blist3[i].ToString() +"' src='{0}'>", "mounts\\M"+blist1[i].ToString().Trim()+".jpg")), blist1[i].ToString());
									RBLMount.Items.Add(litem);
								}
								break;
							case "PS":
								if(cd1.DoubleRod.Trim() =="Yes")
								{
									blist =db.SelectOptions_Mounts("Mount_Code", "Mount_t1", "Mount_t2","WEB_MountPS_TableV1","SeriesPSD",cd1.Bore_Size.Trim());
								}
								else
								{
									blist =db.SelectOptions_Mounts("Mount_Code", "Mount_t1", "Mount_t2","WEB_MountPS_TableV1","SeriesPS",cd1.Bore_Size.Trim());
								}
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								blist3 =(ArrayList)blist[2];
								for(int i=0;i<blist2.Count;i++)
								{
								
									litem =new ListItem((String.Format(blist2[i].ToString()+"<br /><img alt='"+blist3[i].ToString() +"' src='{0}'>", "mounts\\M"+blist1[i].ToString().Trim()+".jpg")), blist1[i].ToString());
									RBLMount.Items.Add(litem);
								}
								break;
							case "PC":
								if(cd1.DoubleRod.Trim() =="Yes")
								{
									blist =db.SelectOptions_Mounts("Mount_Code", "Mount_t1", "Mount_t2","WEB_MountPC_TableV1","SeriesPCD",cd1.Bore_Size.Trim());
								}
								else
								{
									blist =db.SelectOptions_Mounts("Mount_Code", "Mount_t1", "Mount_t2","WEB_MountPC_TableV1","SeriesPC",cd1.Bore_Size.Trim());
								}
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								blist3 =(ArrayList)blist[2];
								for(int i=0;i<blist2.Count;i++)
								{
								
									litem =new ListItem((String.Format(blist2[i].ToString()+"<br /><img alt='"+blist3[i].ToString() +"' src='{0}'>", "mounts\\M"+blist1[i].ToString().Trim()+".jpg")), blist1[i].ToString());
									RBLMount.Items.Add(litem);
								}
								break;
							case "N":
								if(cd1.DoubleRod.Trim() =="Yes")
								{
									blist =db.SelectOptions_Mounts("Mount_Code", "Mount_t1", "Mount_t2","WEB_MountN_TableV1","SeriesND",cd1.Bore_Size.Trim());
								}
								else
								{
									blist =db.SelectOptions_Mounts("Mount_Code", "Mount_t1", "Mount_t2","WEB_MountN_TableV1","SeriesN",cd1.Bore_Size.Trim());
								}
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								blist3 =(ArrayList)blist[2];
								for(int i=0;i<blist2.Count;i++)
								{
								
									litem =new ListItem((String.Format(blist2[i].ToString()+"<br /><img alt='"+blist3[i].ToString() +"' src='{0}'>", "mounts\\M"+blist1[i].ToString().Trim()+".jpg")), blist1[i].ToString());
									RBLMount.Items.Add(litem);
								}
								break;
							case "M":
								if(cd1.DoubleRod.Trim() =="Yes")
								{
									blist =db.SelectOptions_Mounts("Mount_Code", "Mount_t1", "Mount_t2","WEB_MountM_TableV1","SeriesMD",cd1.Bore_Size.Trim());
								}
								else
								{
									blist =db.SelectOptions_Mounts("Mount_Code", "Mount_t1", "Mount_t2","WEB_MountM_TableV1","SeriesM",cd1.Bore_Size.Trim());
								}
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								blist3 =(ArrayList)blist[2];
								for(int i=0;i<blist2.Count;i++)
								{
								
									litem =new ListItem((String.Format(blist2[i].ToString()+"<br /><img alt='"+blist3[i].ToString() +"' src='{0}'>", "mounts\\M"+blist1[i].ToString().Trim()+".jpg")), blist1[i].ToString());
									RBLMount.Items.Add(litem);
								}
								break;
							case "ML":
								if(cd1.DoubleRod.Trim() =="Yes")
								{
									blist =db.SelectOptions_Mounts("Mount_Code", "Mount_t1", "Mount_t2","WEB_MountML_TableV1","SeriesMLD",cd1.Bore_Size.Trim());
								}
								else
								{
									blist =db.SelectOptions_Mounts("Mount_Code", "Mount_t1", "Mount_t2","WEB_MountML_TableV1","SeriesML",cd1.Bore_Size.Trim());
								}
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								blist3 =(ArrayList)blist[2];
								for(int i=0;i<blist2.Count;i++)
								{
								
									litem =new ListItem((String.Format(blist2[i].ToString()+"<br /><img alt='"+blist3[i].ToString() +"' src='{0}'>", "mounts\\M"+blist1[i].ToString().Trim()+".jpg")), blist1[i].ToString());
									RBLMount.Items.Add(litem);
								}
								break;
							case "R":
								if(cd1.DoubleRod.Trim() =="Yes")
								{
									blist =db.SelectOptions_Mounts("Mount_Code", "Mount_t1", "Mount_t2","WEB_MountR_TableV1","SeriesRD",cd1.Bore_Size.Trim());
								}
								else
								{
									blist =db.SelectOptions_Mounts("Mount_Code", "Mount_t1", "Mount_t2","WEB_MountR_TableV1","SeriesR",cd1.Bore_Size.Trim());
								}
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								blist3 =(ArrayList)blist[2];
								for(int i=0;i<blist2.Count;i++)
								{
								
									litem =new ListItem((String.Format(blist2[i].ToString()+"<br /><img alt='"+blist3[i].ToString() +"' src='{0}'>", "mounts\\M"+blist1[i].ToString().Trim()+".jpg")), blist1[i].ToString());
									RBLMount.Items.Add(litem);
								}
								break;
							case "RP":
								if(cd1.DoubleRod.Trim() =="Yes")
								{
									blist =db.SelectOptions_Mounts("Mount_Code", "Mount_t1", "Mount_t2","WEB_MountRP_TableV1","SeriesRPD",cd1.Bore_Size.Trim());
								}
								else
								{
									blist =db.SelectOptions_Mounts("Mount_Code", "Mount_t1", "Mount_t2","WEB_MountRP_TableV1","SeriesRP",cd1.Bore_Size.Trim());
								}
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								blist3 =(ArrayList)blist[2];
								for(int i=0;i<blist2.Count;i++)
								{
								
									litem =new ListItem((String.Format(blist2[i].ToString()+"<br /><img alt='"+blist3[i].ToString() +"' src='{0}'>", "mounts\\M"+blist1[i].ToString().Trim()+".jpg")), blist1[i].ToString());
									RBLMount.Items.Add(litem);
								}
								break;
							case "L":
								if(cd1.DoubleRod.Trim() =="Yes")
								{
									blist =db.SelectOptions_Mounts("Mount_Code", "Mount_t1", "Mount_t2","WEB_MountL_TableV1","SeriesLD",cd1.Bore_Size.Trim());
								}
								else
								{
									blist =db.SelectOptions_Mounts("Mount_Code", "Mount_t1", "Mount_t2","WEB_MountL_TableV1","SeriesL",cd1.Bore_Size.Trim());
								}
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								blist3 =(ArrayList)blist[2];
								for(int i=0;i<blist2.Count;i++)
								{
								
									litem =new ListItem((String.Format(blist2[i].ToString()+"<br /><img alt='"+blist3[i].ToString() +"' src='{0}'>", "mounts\\M"+blist1[i].ToString().Trim()+".jpg")), blist1[i].ToString());
									RBLMount.Items.Add(litem);
								}
								break;
						}
					
						RBLMount.SelectedIndex =0;
						if(	Session["Coder"] !=null)
						{
							cd1=(coder)Session["Coder"];
							if(cd1.Mount !=null )
							{
								if(cd1.Mount !="" )
								{
									RBLMount.SelectedValue =cd1.Mount.Trim();
									RBLMount_SelectedIndexChanged(sender,e);
									if(cd1.Trunnions !=null)
									{
										Txtxi.Text= cd1.Trunnions.Trim().Substring(2);
									}
									if(cd1.BBH !=null)
									{
										TxtBBH.Text= cd1.BBH.Trim().Substring(3);
									}
									if(cd1.BBC !=null)
									{
										TxtBBC.Text= cd1.BBC.Trim().Substring(3);
									}
								}
						
							}
						}
					}
				}
			}
			else
			{
				Response.Redirect("Login.aspx");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.BtnNext.Click += new System.EventHandler(this.BtnNext_Click);
			this.LBBack.Click += new System.EventHandler(this.LBBack_Click);
			this.Txtxi.TextChanged += new System.EventHandler(this.Txtxi_TextChanged);
			this.RBLMount.SelectedIndexChanged += new System.EventHandler(this.RBLMount_SelectedIndexChanged);
			this.TxtBBC.TextChanged += new System.EventHandler(this.TxtBBC_TextChanged);
			this.TxtBBH.TextChanged += new System.EventHandler(this.TxtBBH_TextChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void RBLMount_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Txtxi.Visible =false;
			lblxi.Visible=false;
			TxtBBC.Visible =false;
			lblt1.Visible=false;
			TxtBBH.Visible =false;
			lblt2.Visible=false;

			if(RBLMount.SelectedItem.Value.Trim() =="T4" || RBLMount.SelectedItem.Value.Trim() =="TR4")
			{
				Txtxi.Visible =true;
				lblxi.Visible=true;
			}
			if(RBLMount.SelectedItem.Value.Trim() =="X1")
			{
				TxtBBC.Visible =true;
				lblt1.Visible=true;
				TxtBBH.Visible =true;
				lblt2.Visible=true;
			}
			if(RBLMount.SelectedItem.Value.Trim() =="X2")
			{
				TxtBBC.Visible =true;
				lblt1.Visible=true;
				
			}
			if(RBLMount.SelectedItem.Value.Trim() =="X3")
			{
				TxtBBH.Visible =true;
				lblt2.Visible=true;
			}
		
		}

		private void Txtxi_TextChanged(object sender, System.EventArgs e)
		{
			if(Txtxi.Text.ToString().Trim() !="" && IsNumeric(Txtxi.Text.ToString().Trim()) ==true )
			{
			
				Txtxi.Text =String.Format("{0:##0.00}",Convert.ToDecimal(Txtxi.Text));
			}
			else
			{
				Txtxi.Text="";
			}
		}

		private void TxtBBC_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtBBC.Text.ToString().Trim() !="" && IsNumeric(TxtBBC.Text.ToString().Trim()) ==true )
			{
			
				TxtBBC.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TxtBBC.Text));
			}
			else
			{
				TxtBBC.Text="";
			}
		}
		public static bool IsNumeric(string strInteger) 
		{
			try 
			{
					int intTemp =0;
				if(strInteger.ToString().StartsWith(".") == true)
				{
					for(int i=1; i< strInteger.Length;i++)
					{
						intTemp = Int32.Parse( strInteger.Substring(i,1) );
					}
				}
				else
				{
					for(int i=0; i< strInteger.Length;i++)
					{
						if (strInteger.ToString().Substring(i,1) !=".")
						{
							intTemp = Int32.Parse( strInteger.Substring(i,1) );
						}
					}
				}
				return true;
			} 
			catch (FormatException) 
			{
				return false;
			}    
		}
		private void TxtBBH_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtBBH.Text.ToString().Trim() !="" && IsNumeric(TxtBBH.Text.ToString().Trim()) ==true )
			{
			
				TxtBBH.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TxtBBH.Text));
			}
			else
			{
				TxtBBH.Text="";
			}
		}

		private void BtnNext_Click(object sender, System.EventArgs e)
		{
			if(RBLMount.SelectedIndex !=-1 && RBLMount.SelectedItem.Value.Trim() !="T4" && RBLMount.SelectedItem.Value.Trim() !="X1" && RBLMount.SelectedItem.Value.Trim() !="X2" && RBLMount.SelectedItem.Value.Trim() !="X3" )
			{
				cd1=new coder();
				cd1 =(coder)Session["Coder"];
				Session["Coder"] =null;
				cd1.Mount =RBLMount.SelectedItem.Value.Trim();
				Session["Coder"] =cd1;
				Response.Redirect("Icylinder_Page4.aspx");
			}
			else if(RBLMount.SelectedIndex !=-1 && RBLMount.SelectedItem.Value.Trim() =="T4" && Txtxi.Text.Trim() !="")
			{
				cd1=new coder();
				cd1 =(coder)Session["Coder"];
				Session["Coder"] =null;
				cd1.Mount =RBLMount.SelectedItem.Value.Trim();
				cd1.Trunnions="XI"+Txtxi.Text.Trim();
				Session["Coder"] =cd1;
				Response.Redirect("Icylinder_Page4.aspx");
			
			}
			else if(RBLMount.SelectedIndex !=-1 && RBLMount.SelectedItem.Value.Trim() =="TR4" && Txtxi.Text.Trim() !="")
			{
				cd1=new coder();
				cd1 =(coder)Session["Coder"];
				Session["Coder"] =null;
				cd1.Mount =RBLMount.SelectedItem.Value.Trim();
				cd1.Trunnions="XI"+Txtxi.Text.Trim();
				Session["Coder"] =cd1;
				Response.Redirect("Icylinder_Page4.aspx");
			
			}
			else if(RBLMount.SelectedIndex !=-1 && RBLMount.SelectedItem.Value.Trim() =="X1" || RBLMount.SelectedItem.Value.Trim() =="X2"|| RBLMount.SelectedItem.Value.Trim() =="X3" )
			{
				cd1=new coder();
				cd1 =(coder)Session["Coder"];
				Session["Coder"] =null;
				cd1.Mount =RBLMount.SelectedItem.Value.Trim();
				if(TxtBBC.Text.Trim() !="")
				{
					cd1.BBC="BBC"+TxtBBC.Text.Trim();
				}
				else
				{
					cd1.BBC="";
				}
				if(TxtBBH.Text.Trim() !="")
				{
					cd1.BBH="BBH"+TxtBBH.Text.Trim();
				}
				else
				{
					cd1.BBH="";
				}
				
				Session["Coder"] =cd1;
				Response.Redirect("Icylinder_Page4.aspx");
			
			}
			else
			{
				lblseries.Visible=true;
			}
		}

		private void LBBack_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Icylinder_Page2.aspx");
		}
	}
}
