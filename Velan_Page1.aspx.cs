using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for Velan_Page1.
	/// </summary>
	public partial class Velan_Page1 : System.Web.UI.Page
	{
		coder cd1=new coder();
		DBClass db=new DBClass();
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["User"] !=null)
			{
				if(! IsPostBack)
				{
					lblhidden.Text="";
					ArrayList blist=new ArrayList();
					ArrayList blist1=new ArrayList();
					ArrayList blist2=new ArrayList();
					ListItem litem=new ListItem();
					lblhidden.Text ="A";
					blist =db.SelectOptions_Bore("Bore_Code", "Bore_Size","WEB_Bore_TableV1","SeriesA","Bore_ID");
					blist1 =(ArrayList)blist[0];
					blist2 =(ArrayList)blist[1];
					for(int i=0;i<blist2.Count;i++)
					{
						litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
						RBLBore.Items.Add(litem);
					}
					if(Request.QueryString["p"] !=null)
					{
						if(Request.QueryString["p"].ToString() =="new")
						{
							Session["Coder"] =null;
						}
					}
					RBLBore.SelectedIndex =0;
					RBLBore_SelectedIndexChanged(sender,e);
					if(	Session["Coder"] !=null)
					{
						cd1=(coder)Session["Coder"];
						if(cd1.Bore_Size !=null)
						{
							RBLBore.SelectedValue =cd1.Bore_Size.Trim();
							RBLBore_SelectedIndexChanged(sender,e);
						}
						if(cd1.Rod_Diamtr !=null)
						{
							RBL1stRodSize.SelectedValue =cd1.Rod_Diamtr.Trim();
						}
						if(cd1.DoubleRod !=null)
						{
							RBLDoubleOrSingle.SelectedValue=cd1.DoubleRod.ToString();
							RBLDoubleOrSingle_SelectedIndexChanged(sender,e);
						}
						if(cd1.SecRodDiameter !=null)
						{
							RBL2ndRodSize.SelectedValue =cd1.SecRodDiameter.Trim().Substring(1,1);
						}
						if(cd1.Rod_End !=null)
						{
							if(cd1.Rod_End =="A4")
							{
								RBL1Rodend1.SelectedIndex=0;
							}
						}
						if(cd1.SecRodEnd !=null)
						{
							if(cd1.SecRodEnd =="RA4")
							{
								RBL1Rodend1.SelectedIndex=0;
							}
						}
						if(cd1.Stroke !=null)
						{
							TxtStroke.Text =cd1.Stroke.Trim();
						}
						if(cd1.StopTube !=null)
						{
							PStoptube.Visible =true;
							if(cd1.StopTube.Trim().Substring(0,1)=="D")
							{
								TxtStopTube.Text=cd1.StopTube.Trim().Substring(3);
							}
							else
							{
								TxtStopTube.Text=cd1.StopTube.Trim().Substring(2);
							}
							decimal dc1,dc2,dc3=0.00m;
							dc1=Convert.ToDecimal(TxtStopTube.Text);
							dc2=Convert.ToDecimal(TxtStroke.Text);
							dc3 =dc2 - dc1;
							TxtEffectiveStrok.Text=String.Format("{0:##0.00}", dc3);
						
						}
						if(cd1.RodEx !=null || cd1.ThreadEx !=null  )
						{
							P1stAdvanced.Visible =true;
							if(cd1.RodEx !=null)
							{
								TXTRodEx.Text=cd1.RodEx.Trim().Substring(1);
							}
							if(cd1.ThreadEx !=null)
							{
								TXTThread.Text=cd1.ThreadEx.Trim().Substring(1);
							}
						}
						if(cd1.SecRodEx !=null || cd1.SecRodThreadx !=null  )
						{
							P2ndAdvanced.Visible =true;
							if(cd1.SecRodEx !=null)
							{
								TxtSecRodEx.Text=cd1.SecRodEx.Trim().Substring(2);
							}
							if(cd1.SecRodThreadx !=null)
							{
								TxtSecThreadEx.Text=cd1.SecRodThreadx.Trim().Substring(2);
							}
						}
						if(cd1.Seal_Comp !=null)
						{
							RBLSeal.SelectedValue =cd1.Seal_Comp.Trim();
						}
					}
				}
			}
			else
			{
				Response.Redirect("Login.aspx");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
		protected void BtnAdvanced_Click(object sender, System.EventArgs e)
		{
			if(PStoptube.Visible ==true)
			{
				PStoptube.Visible =false;
				BtnAdvanced.Text ="Click here for Stop Tube";
			}	
			else
			{
				PStoptube.Visible =true;
				BtnAdvanced.Text ="No Stop Tube please!";
			}
		}
		protected void RBLBore_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(	RBLBore.SelectedIndex > -1)
			{
				RBL1stRodSize.Items.Clear();
				RBL2ndRodSize.Items.Clear();
				ArrayList blist=new ArrayList();
				ArrayList blist1=new ArrayList();
				ArrayList blist2=new ArrayList();
				ArrayList rdlist=new ArrayList();
				ListItem litem=new ListItem();
				if(RBLDoubleOrSingle.SelectedIndex ==1)
				{
					P2ndRod.Visible =true;
					P2ndRodnd.Visible =true;
					blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerA_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
					blist1 =(ArrayList)blist[0];
					blist2 =(ArrayList)blist[1];
					for(int i=0;i<blist2.Count;i++)
					{
						litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
						RBL1stRodSize.Items.Add(litem);
						RBL2ndRodSize.Items.Add(litem);
					}
					RBL1stRodSize.SelectedIndex =0;
					RBL1Rodend1.SelectedIndex=0;
					RBL2Rodend2.SelectedIndex=0;
				}
				else
				{
					P2ndRod.Visible =false;
					P2ndRodnd.Visible =false;
					P2ndAdvanced.Visible=false;
					blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerA_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
					blist1 =(ArrayList)blist[0];
					blist2 =(ArrayList)blist[1];
					for(int i=0;i<blist2.Count;i++)
					{
						litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
						RBL1stRodSize.Items.Add(litem);
					}
					RBL1stRodSize.SelectedIndex =0;
					RBL1Rodend1.SelectedIndex=0;
				}
			}
		}

		private void LBBack_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Velan_Select.aspx");
		}

		protected void LB1stRodAdvanced_Click(object sender, System.EventArgs e)
		{
			if(P1stAdvanced.Visible ==true)
			{
				P1stAdvanced.Visible =false;
				LB1stRodAdvanced.Text ="Advaced Options";
			}	
			else
			{
				P1stAdvanced.Visible =true;
				LB1stRodAdvanced.Text ="No Advanced Options !";
			}
		}

		protected void LB2ndRodAdvanced_Click(object sender, System.EventArgs e)
		{
			if(P2ndAdvanced.Visible ==true)
			{
				P2ndAdvanced.Visible =false;
				LB2ndRodAdvanced.Text ="Advaced Options";
			}	
			else
			{
				P2ndAdvanced.Visible =true;
				LB2ndRodAdvanced.Text ="No Advanced Options !";
			}
		}

		public static bool IsNumeric(string strInteger) 
		{
			try 
			{
				int intTemp =0;
				if(strInteger.ToString().StartsWith(".") == true)
				{
					for(int i=1; i< strInteger.Length;i++)
					{
						intTemp = Int32.Parse( strInteger.Substring(i,1) );
					}
				}
				else
				{
					for(int i=0; i< strInteger.Length;i++)
					{
						if (strInteger.ToString().Substring(i,1) !=".")
						{
							intTemp = Int32.Parse( strInteger.Substring(i,1) );
						}
					}
				}
				return true;
			} 
			catch (FormatException) 
			{
				return false;
			}    
		}

		protected void TxtStroke_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtStroke.Text.ToString().Trim() !="" && IsNumeric(TxtStroke.Text.ToString().Trim()) ==true  )
			{
			
				TxtStroke.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TxtStroke.Text));
				
			}
			else
			{
				TxtStroke.Text="";
			}
		}

		protected void TxtStopTube_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtStopTube.Text.ToString().Trim() !="" && IsNumeric(TxtStopTube.Text.ToString().Trim()) ==true )
			{
			
				TxtStopTube.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TxtStopTube.Text));
			}
			else
			{
				TxtStopTube.Text="";
			}
			if(TxtEffectiveStrok.Text.ToString().Trim() !="" && TxtStopTube.Text.ToString().Trim() !="")
			{
				decimal dc1,dc2,dc3=0.00m;
				dc1=Convert.ToDecimal(TxtStopTube.Text);
				dc2=Convert.ToDecimal(TxtEffectiveStrok.Text);
				dc3 =dc1 +dc2;
				TxtStroke.Text =String.Format("{0:##0.00}", dc3);
			}
		}

		protected void TxtEffectiveStrok_TextChanged(object sender, System.EventArgs e)
		{
			if(IsNumeric(TxtEffectiveStrok.Text.ToString().Trim()) ==true )
			{
			
				TxtEffectiveStrok.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TxtEffectiveStrok.Text));
			}
			else
			{
				TxtEffectiveStrok.Text="";
			}
			if(TxtEffectiveStrok.Text.ToString().Trim() !="" && TxtStopTube.Text.ToString().Trim() !="")
			{
				decimal dc1,dc2,dc3=0.00m;
				dc1=Convert.ToDecimal(TxtStopTube.Text);
				dc2=Convert.ToDecimal(TxtEffectiveStrok.Text);
				dc3 =dc1 +dc2;
				TxtStroke.Text =String.Format("{0:##0.00}", dc3);
			}
		}

		protected void TXTThread_TextChanged(object sender, System.EventArgs e)
		{
			if(TXTThread.Text.ToString().Trim() !="" && IsNumeric(TXTThread.Text.ToString().Trim()) ==true )
			{
			
				TXTThread.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TXTThread.Text));
			}
			else
			{
				TXTThread.Text="";
			}
		
		}

		protected void TXTRodEx_TextChanged(object sender, System.EventArgs e)
		{
			if(TXTRodEx.Text.ToString().Trim() !="" && IsNumeric(TXTRodEx.Text.ToString().Trim()) ==true )
			{
			
				TXTRodEx.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TXTRodEx.Text));
				
			}
			else
			{
				TXTRodEx.Text="";
			}
			
		}

		protected void TxtSecThreadEx_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtSecThreadEx.Text.ToString().Trim() !="" && IsNumeric(TxtSecThreadEx.Text.ToString().Trim()) ==true )
			{
			
				TxtSecThreadEx.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TxtSecThreadEx.Text));
			}
			else
			{
				TxtSecThreadEx.Text="";
			}
		}

		protected void TxtSecRodEx_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtSecRodEx.Text.ToString().Trim() !="" && IsNumeric(TxtSecRodEx.Text.ToString().Trim()) ==true )
			{
			
				TxtSecRodEx.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TxtSecRodEx.Text));
			}
			else
			{
				TxtSecRodEx.Text="";
			}
		}

		protected void RBLDoubleOrSingle_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(RBLDoubleOrSingle.SelectedIndex >-1 && RBLBore.SelectedIndex >-1)
			{
				RBL1stRodSize.Items.Clear();
				RBL2ndRodSize.Items.Clear();
				ArrayList blist=new ArrayList();
				ArrayList blist1=new ArrayList();
				ArrayList blist2=new ArrayList();
				ArrayList rdlist=new ArrayList();
				ListItem litem=new ListItem();
				if(RBLDoubleOrSingle.SelectedIndex ==1)
				{
					P2ndRod.Visible =true;
					P2ndRodnd.Visible =true;
					blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerA_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
					blist1 =(ArrayList)blist[0];
					blist2 =(ArrayList)blist[1];
					for(int i=0;i<blist2.Count;i++)
					{
						litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
						RBL1stRodSize.Items.Add(litem);
						RBL2ndRodSize.Items.Add(litem);
					}
					RBL1stRodSize.SelectedIndex =0;
					RBL1Rodend1.SelectedIndex=0;
					RBL2Rodend2.SelectedIndex=0;
				}
				else
				{
					P2ndRod.Visible =false;
					P2ndRodnd.Visible =false;
					P2ndAdvanced.Visible=false;
					blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerA_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
					blist1 =(ArrayList)blist[0];
					blist2 =(ArrayList)blist[1];
					for(int i=0;i<blist2.Count;i++)
					{
						litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
						RBL1stRodSize.Items.Add(litem);
					}
					RBL1stRodSize.SelectedIndex =0;
					RBL1Rodend1.SelectedIndex=0;
				}
			}
		}

		protected void BtnNext_Click(object sender, System.EventArgs e)
		{
			if(RBLBore.SelectedIndex >= 0 && TxtStroke.Text.Trim() !="" && RBL1stRodSize.SelectedIndex >=0)
			{
				cd1=new coder();
				cd1.Series="A";
				cd1.Bore_Size =RBLBore.SelectedItem.Value.Trim();
				cd1.Stroke =TxtStroke.Text.Trim();
				cd1.DoubleRod=RBLDoubleOrSingle.SelectedItem.Value.Trim();
				if(RBLDoubleOrSingle.SelectedIndex ==0)
				{
					cd1.Rod_Diamtr = RBL1stRodSize.SelectedItem.Value.Trim();
					cd1.Rod_End ="A4";
					if(P1stAdvanced.Visible ==true)
					{
						if(TXTRodEx.Text.Trim() !="")
						{
							cd1.RodEx ="W"+TXTRodEx.Text.Trim();
						}
						if(TXTThread.Text.Trim() !="")
						{
							cd1.ThreadEx ="A"+TXTThread.Text.Trim();
						}
						
					}
				}
				else
				{
					cd1.Rod_Diamtr = RBL1stRodSize.SelectedItem.Value.Trim();
					cd1.SecRodDiameter = "D"+ RBL2ndRodSize.SelectedItem.Value.Trim()+"2";
					cd1.Rod_End="A4";
					cd1.SecRodEnd ="RA4";
					if(P1stAdvanced.Visible ==true)
					{
						if(TXTRodEx.Text.Trim() !="")
						{
							cd1.RodEx ="W"+TXTRodEx.Text.Trim();
						}
						if(TXTThread.Text.Trim() !="")
						{
							cd1.ThreadEx ="A"+TXTThread.Text.Trim();
						}
					}
					if(P2ndAdvanced.Visible ==true)
					{
						if(TxtSecRodEx.Text.Trim() !="")
						{
							cd1.SecRodEx ="WD"+TxtSecRodEx.Text.Trim();
						}
						if(TxtSecThreadEx.Text.Trim() !="")
						{
							cd1.SecRodThreadx ="AD"+TxtSecThreadEx.Text.Trim();
						}
					}
				}
				if(PStoptube.Visible ==true)
				{
					if(RBStrokeType.SelectedIndex == 0)
					{
						cd1.StopTube="ST"+TxtStopTube.Text.Trim();
					}
					else
					{
						cd1.StopTube="DST"+TxtStopTube.Text.Trim();
					}
				}
				if(RBLSeal.SelectedIndex!=-1)
				{
					cd1.Seal_Comp=RBLSeal.SelectedItem.Value.Trim();
				}
				cd1.Cushions="8";
				cd1.CushionPosition="";
				cd1.Port_Type="N";
				cd1.Port_Pos="11";
				cd1.Mount="X0";
				cd1.Specials="STD";
				Session["Coder"] =cd1;
				Response.Redirect("Velan_Page2.aspx");
			}
		}
	}
}
