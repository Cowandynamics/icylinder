<%@ Page language="c#" Codebehind="Icylinder_page5.aspx.cs" AutoEventWireup="True" Inherits="iCylinderV1.Icylinder_page5" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Icylinder_page5</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE style="WIDTH: 1000px; HEIGHT: 487px" id="Table1" border="0" cellSpacing="0" cellPadding="0"
				width="1000" bgColor="lightgrey">
				<TR>
					<TD height="20" width="50"></TD>
					<TD height="20" align="center"><asp:label id="Label2" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
							Font-Size="Small">Advanced Options</asp:label></TD>
					<TD height="20" width="50"></TD>
				</TR>
				<TR>
					<TD width="50"></TD>
					<TD bgColor="gainsboro" align="center">
						<TABLE id="Table4" border="0" cellSpacing="0" cellPadding="0" width="899">
							<TR>
								<TD align="center"><asp:panel id="Panel2" runat="server" BorderColor="White" Width="234px" Height="350px" BorderStyle="Solid"
										BorderWidth="1px">
            <TABLE style="WIDTH: 234px; HEIGHT: 285px" id="Table3" border="1" cellSpacing="0" borderColor="#e3e1dc"
											cellPadding="0" width="238">
											<TR>
												<TD style="HEIGHT: 55px">
													<asp:label id="Label5" runat="server" Font-Size="Smaller" BackColor="Transparent" Height="19"
														Width="224px">Piston Rod material:</asp:label>
													<asp:DropDownList id="DDLPRodMat" runat="server" Font-Size="XX-Small" Width="136px"></asp:DropDownList></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 66px">
													<asp:label id="Label3" runat="server" Font-Size="Smaller" BackColor="Transparent" Height="19"
														Width="224px">Piston Seal Material:</asp:label>
													<asp:RadioButtonList id="RBLPsealmat" runat="server" Font-Size="Smaller" RepeatLayout="Flow"></asp:RadioButtonList></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 68px">
													<asp:label id="Label4" runat="server" Font-Size="Smaller" BackColor="Transparent" Height="19"
														Width="224px">Magnet on Piston:</asp:label>
													<asp:RadioButtonList id="RBLMagnet" runat="server" Font-Size="Smaller" RepeatLayout="Flow" RepeatDirection="Horizontal">
														<asp:ListItem Value="P2">Yes</asp:ListItem>
														<asp:ListItem Value=" " Selected="True">No</asp:ListItem>
													</asp:RadioButtonList></TD>
											</TR>
											<TR>
												<TD>
													<asp:label id="Label1" runat="server" Font-Size="Smaller" BackColor="Transparent" Height="19"
														Width="224px">Piston Rod Seal Material:</asp:label>
													<asp:DropDownList id="RBLProdSealmat" runat="server" Font-Size="XX-Small" Width="136px"></asp:DropDownList></TD>
											</TR>
											<TR>
												<TD>
													<asp:label id="Label13" runat="server" Font-Size="Smaller" BackColor="Transparent" Height="19"
														Width="224px">Stainless Steel Tie Rod:</asp:label>
													<asp:RadioButtonList id="RBLSStierod" runat="server" Font-Size="Smaller" RepeatLayout="Flow" RepeatDirection="Horizontal">
														<asp:ListItem Value="P2">Yes</asp:ListItem>
														<asp:ListItem Value=" " Selected="True">No</asp:ListItem>
													</asp:RadioButtonList></TD>
											</TR>
										</TABLE>&nbsp;</asp:panel></TD>
								<TD align="center"><asp:panel id="Panel1" runat="server" BorderColor="White" Width="234px" Height="350px" BorderStyle="Solid"
										BorderWidth="1px">
            <TABLE style="WIDTH: 234px; HEIGHT: 285px" id="Table2" border="1" cellSpacing="0" borderColor="#e3e1dc"
											cellPadding="0" width="238">
											<TR>
												<TD style="HEIGHT: 7px">
													<asp:label id="Label10" runat="server" Font-Size="Smaller" BackColor="Transparent" Height="19"
														Width="224px">Metal Scrapper:</asp:label>
													<asp:RadioButtonList id="RBLMetalScrapper" runat="server" Font-Size="Smaller" RepeatLayout="Flow">
														<asp:ListItem Value="None" Selected="True">None</asp:ListItem>
														<asp:ListItem Value="GT3">Severe Service</asp:ListItem>
													</asp:RadioButtonList></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 39px">
													<asp:label id="Label9" runat="server" Font-Size="Smaller" BackColor="Transparent" Height="19"
														Width="232px">Gland Drain:</asp:label>
													<asp:RadioButtonList id="RBLGlanddrain" runat="server" Font-Size="Smaller" RepeatLayout="Flow" RepeatDirection="Horizontal">
														<asp:ListItem Value="G1">Yes</asp:ListItem>
														<asp:ListItem Value=" " Selected="True">No</asp:ListItem>
													</asp:RadioButtonList></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 68px">
													<asp:label id="Label8" runat="server" Font-Size="Smaller" BackColor="Transparent" Height="19"
														Width="224px">Bushing Material:</asp:label>
													<asp:DropDownList id="RBLBushing" runat="server" Font-Size="XX-Small" Width="136px"></asp:DropDownList></TD>
											</TR>
											<TR>
												<TD>
													<asp:label id="Label7" runat="server" Font-Size="Smaller" BackColor="Transparent" Height="19"
														Width="224px">Air Bleeds:</asp:label>
													<asp:RadioButtonList id="RBLAirbleeds" runat="server" Font-Size="Smaller" RepeatColumns="2">
														<asp:ListItem Selected="True">None</asp:ListItem>
														<asp:ListItem Value="P1">Both Ends</asp:ListItem>
														<asp:ListItem Value="P2">Head End</asp:ListItem>
														<asp:ListItem Value="P3">Cap End</asp:ListItem>
													</asp:RadioButtonList></TD>
											</TR>
										</TABLE>&nbsp;</asp:panel></TD>
								<TD align="center"><asp:panel id="Panel3" runat="server" BorderColor="White" Width="234px" Height="219px" BorderStyle="Solid"
										BorderWidth="1px">
            <TABLE style="WIDTH: 234px; HEIGHT: 160px" id="Table5" border="1" cellSpacing="0" borderColor="#e3e1dc"
											cellPadding="0" width="238">
											<TR>
												<TD style="HEIGHT: 7px">
													<asp:label id="Label15" runat="server" Font-Size="Smaller" BackColor="Transparent" Height="19"
														Width="224px">Coating:</asp:label>
													<asp:DropDownList id="DDLCoating" runat="server" Font-Size="XX-Small" Width="128px"></asp:DropDownList></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 14px">
													<asp:label id="Label14" runat="server" Font-Size="Smaller" BackColor="Transparent" Height="19"
														Width="224px">Barrel:</asp:label>
													<asp:RadioButtonList id="RBLBarrel" runat="server" Font-Size="Smaller" RepeatColumns="2"></asp:RadioButtonList></TD>
											</TR>
										</TABLE>&nbsp;&nbsp;</asp:panel>
									<asp:panel id="Panel4" runat="server" BorderColor="White" Width="234px" Height="125px" BorderStyle="Solid"
										BorderWidth="1px">
            <TABLE style="WIDTH: 234px; HEIGHT: 80px" id="Table7" border="1" cellSpacing="0" borderColor="#e3e1dc"
											cellPadding="0" width="238">
											<TR>
												<TD style="HEIGHT: 7px">
													<asp:RadioButtonList id="RBLTandem" runat="server" Font-Size="Smaller" RepeatLayout="Flow" AutoPostBack="True" onselectedindexchanged="RBLTandem_SelectedIndexChanged">
														<asp:ListItem Value=" " Selected="True">None</asp:ListItem>
														<asp:ListItem Value="TC">Tandem Cylinder(Rods Attached)</asp:ListItem>
														<asp:ListItem Value="DC">Duplex Inline(Rods not attached)</asp:ListItem>
														<asp:ListItem Value="BC">Back to Back Cylinder</asp:ListItem>
													</asp:RadioButtonList></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 14px">
													<asp:label id="lblsecstk" runat="server" Font-Size="Smaller" BackColor="Transparent" Height="19"
														Width="88px" Visible="False">Second Stroke:</asp:label>
													<asp:textbox id="TXTstroke" runat="server" Font-Size="XX-Small" Height="20px" Width="52px" AutoPostBack="True"
														Visible="False" ontextchanged="TXTstroke_TextChanged"></asp:textbox></TD>
											</TR>
										</TABLE>&nbsp;</asp:panel></TD>
							</TR>
						</TABLE>
					</TD>
					<TD width="50"></TD>
				</TR>
				<TR>
					<TD height="30" width="50" align="right"><asp:linkbutton id="LBBack" runat="server" Font-Size="Smaller" Font-Bold="True" onclick="LBBack_Click">Back</asp:linkbutton></TD>
					<TD bgColor="#dcdcdc" height="30" align="center"></TD>
					<TD height="30" width="50"><asp:linkbutton id="LBNext" runat="server" Font-Size="Smaller" Font-Bold="True" onclick="LBNext_Click">Next</asp:linkbutton></TD>
				</TR>
				<TR>
					<TD height="20" width="50"></TD>
					<TD height="20" align="center"></TD>
					<TD height="20" width="50"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
