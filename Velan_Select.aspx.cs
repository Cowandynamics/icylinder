using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for Velan_Select.
	/// </summary>
	public partial class Velan_Select : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["User"] ==null)
			{
				Response.Redirect("Login.aspx");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void LBcap_Click(object sender, System.EventArgs e)
		{
			//issue #137 start
			//backup
			//Response.Redirect("Velan_Page1.aspx?p=new");
			//update
			Response.Redirect("Velan_SeriesA.aspx?p=new");

		}

		protected void IBCap_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			Response.Redirect("Velan_Page1.aspx?p=new");
		}

		private void LBRod_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Velan_SeriesA.aspx?p=new");
		}

		protected void IBRod_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			Response.Redirect("Velan_BachQuote.aspx");
		}

		protected void LBBatch_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Velan_BachQuote.aspx");
		}
	}
}
