<%@ Page language="c#" Codebehind="ManageQ.aspx.cs" AutoEventWireup="True" Inherits="iCylinderV1.ManageQ" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ManageQ</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="JavaScript" src="calendar_us.js"></script>
		<LINK rel="stylesheet" href="calendar.css">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE style="Z-INDEX: 100; POSITION: absolute; WIDTH: 1000px; TOP: 0px; LEFT: 0px" id="Table2"
				border="1" cellSpacing="0" borderColor="gray" cellPadding="0" width="1000">
				<TR>
					<TD noWrap align="center"><asp:label id="Label1" runat="server" ForeColor="Brown" Font-Size="Smaller" Font-Underline="True"
							Font-Bold="True">Quote Management</asp:label></TD>
				</TR>
				<TR>
					<TD noWrap>
						<TABLE id="Table1" border="0" cellSpacing="0" cellPadding="0" width="996">
							<TR>
								<TD style="WIDTH: 92px" bgColor="lightgrey" height="18" noWrap></TD>
								<TD style="WIDTH: 233px" bgColor="darkgray" height="18" noWrap align="left"><asp:label id="Label2" runat="server" Font-Size="Smaller" Font-Bold="True">Select Database  :</asp:label></TD>
								<TD style="WIDTH: 259px" bgColor="lightgrey" height="18" noWrap><asp:label id="Label5" runat="server" Font-Size="Smaller" Font-Bold="True">Search By :</asp:label></TD>
								<TD style="WIDTH: 283px" bgColor="darkgray" height="18" noWrap><asp:label id="lblinfo" runat="server" Font-Size="XX-Small" Width="8px" Height="20px"></asp:label></TD>
								<TD bgColor="lightgrey" height="18" noWrap></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 92px" bgColor="darkgray" noWrap><asp:label id="Label4" runat="server" ForeColor="Red" Font-Size="Smaller" Font-Underline="True"
										Font-Bold="True" Width="96px">Search</asp:label></TD>
								<TD style="WIDTH: 233px" bgColor="lightgrey" height="30" noWrap align="left"><asp:radiobuttonlist id="RBLOptions" runat="server" Font-Size="Smaller" AutoPostBack="True" onselectedindexchanged="RBLOptions_SelectedIndexChanged">
										<asp:ListItem Value="Finished Quote" Selected="True">Finished Quote</asp:ListItem>
										<asp:ListItem Value="RFQ Sent To Cowan">RFQ Sent To Cowan</asp:ListItem>
									</asp:radiobuttonlist></TD>
								<TD style="WIDTH: 259px" bgColor="darkgray" noWrap><asp:radiobuttonlist id="RBLSearch" runat="server" Font-Size="Smaller" Width="241px" AutoPostBack="True" onselectedindexchanged="RBLSearch_SelectedIndexChanged">
										<asp:ListItem Value="Date" Selected="True">Date</asp:ListItem>
										<asp:ListItem Value="Quote No">Quote No</asp:ListItem>
										<asp:ListItem Value="Part No">Part No</asp:ListItem>
									</asp:radiobuttonlist></TD>
								<TD bgColor="lightgrey" vAlign="middle" noWrap><asp:panel id="pdate" runat="server" Width="280px" BackColor="LightGray" Visible="False">
										<TABLE style="Z-INDEX: 0" id="Table5" border="0" cellSpacing="1" cellPadding="1" width="100%">
											<TR>
												<TD style="WIDTH: 18px">
													<asp:Label style="Z-INDEX: 0" id="Label7" runat="server" Font-Size="Smaller">From :</asp:Label></TD>
												<TD style="WIDTH: 103px">
													<asp:TextBox style="Z-INDEX: 0" id="TxtDate1" runat="server" Font-Size="XX-Small" Height="16px"
														Width="72px" ReadOnly="True"></asp:TextBox>
													<SCRIPT language="JavaScript">new tcal({'formname': 'Form1','controlname': 'TxtDate1'});</SCRIPT>
												</TD>
												<TD style="WIDTH: 4px">
													<asp:Label style="Z-INDEX: 0" id="Label6" runat="server" Font-Size="Smaller">To :</asp:Label></TD>
												<TD>
													<asp:TextBox style="Z-INDEX: 0" id="TxtDate2" runat="server" Font-Size="XX-Small" Height="16px"
														Width="72px" ReadOnly="True"></asp:TextBox>
													<SCRIPT language="JavaScript">new tcal({'formname': 'Form1','controlname': 'TxtDate2'});</SCRIPT>
												</TD>
											</TR>
										</TABLE>
									</asp:panel>
									<P style="Z-INDEX: 0"><asp:panel id="pqno" runat="server" Width="280px" BackColor="LightGray" Visible="False">
											<TABLE style="WIDTH: 280px" id="Table8" border="0" cellSpacing="0" cellPadding="0" width="280">
												<TR>
													<TD style="WIDTH: 81px" noWrap>
														<asp:Label id="Label10" runat="server" Font-Size="Smaller" Width="96px">Enter Quote No :</asp:Label></TD>
													<TD noWrap>
														<asp:TextBox id="TxtQno" runat="server" Font-Size="XX-Small" Height="20px" Width="120px"></asp:TextBox></TD>
												</TR>
											</TABLE>
										</asp:panel></P>
									<P><asp:panel id="ppno" runat="server" Width="280px" BackColor="LightGray" Visible="False">
											<TABLE style="WIDTH: 264px; HEIGHT: 22px" id="Table7" border="0" cellSpacing="0" cellPadding="0"
												width="264">
												<TR>
													<TD style="WIDTH: 81px" noWrap>
														<asp:Label id="Label11" runat="server" Font-Size="Smaller">Enter No:</asp:Label></TD>
													<TD noWrap>
														<asp:TextBox id="TxtPno" runat="server" Font-Size="XX-Small" Height="20px" Width="192px"></asp:TextBox></TD>
												</TR>
											</TABLE>
										</asp:panel></P>
								</TD>
								<TD bgColor="darkgray" noWrap><asp:button id="BtnSearh" runat="server" Font-Size="XX-Small" Font-Underline="True" Width="120px"
										Height="24px" BackColor="Gray" BorderStyle="Inset" Text="Search" onclick="BtnSearh_Click"></asp:button></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD noWrap><asp:panel id="PArchive" runat="server" Width="995px" BorderStyle="None" BorderColor="SlateGray">
							<TABLE style="WIDTH: 992px; HEIGHT: 22px" id="Table4" border="0" cellSpacing="0" cellPadding="0"
								width="992">
								<TR>
									<TD style="WIDTH: 84px" noWrap>
										<asp:DropDownList id="DDLAContacts" runat="server" Font-Size="XX-Small" Width="160px" AutoPostBack="True" onselectedindexchanged="DDLAContacts_SelectedIndexChanged"></asp:DropDownList></TD>
									<TD style="WIDTH: 74px" width="74" noWrap>
										<asp:DropDownList id="DDLACreatedate" runat="server" Font-Size="XX-Small" Width="80px" AutoPostBack="True" onselectedindexchanged="DDLACreatedate_SelectedIndexChanged"></asp:DropDownList></TD>
									<TD style="WIDTH: 80px" width="80" noWrap>
										<asp:DropDownList id="DDLAQNo" runat="server" Font-Size="XX-Small" Width="120px" AutoPostBack="True" onselectedindexchanged="DDLAQNo_SelectedIndexChanged"></asp:DropDownList></TD>
									<TD style="WIDTH: 199px" width="199" noWrap>
										<asp:DropDownList id="DDLAPartNo" runat="server" Font-Size="XX-Small" Width="216px" AutoPostBack="True" onselectedindexchanged="DDLAPartNo_SelectedIndexChanged"></asp:DropDownList></TD>
									<TD colSpan="2" noWrap></TD>
								</TR>
							</TABLE>
							<asp:DataGrid id="DGArchive" runat="server" Font-Size="Smaller" ForeColor="Gray" Height="144px"
								Width="994px" BackColor="White" BorderStyle="None" BorderColor="Gainsboro" AutoGenerateColumns="False"
								AllowPaging="True" BorderWidth="1px" CellPadding="0" AllowSorting="True" OnSortCommand="DGArchive_SortCommand"
								OnItemCommand="DGArchive_Command" OnPageIndexChanged="DGArchive_PageChanger">
								<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
								<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
								<ItemStyle ForeColor="#000066"></ItemStyle>
								<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="DarkGray"></HeaderStyle>
								<Columns>
									<asp:BoundColumn DataField="Contact" SortExpression="Contact" HeaderText="Contact">
										<HeaderStyle HorizontalAlign="Center" Width="150px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="QuoteDate" SortExpression="QuoteDate" HeaderText="Creation Date" DataFormatString="{0:MM/dd/yyyy}">
										<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:HyperLinkColumn Target="_blank" DataNavigateUrlField="QuoteNo" DataNavigateUrlFormatString="print.aspx?id={0}&amp;type=icylinder"
										DataTextField="QuoteNo" SortExpression="QuoteNo" HeaderText="Quote No" DataTextFormatString="{0:c}">
										<HeaderStyle HorizontalAlign="Center" Width="120px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:HyperLinkColumn>
									<asp:BoundColumn Visible="False" DataField="QuoteNo1" SortExpression="QuoteNo1" HeaderText="QuoteNo1">
										<HeaderStyle Width="10px"></HeaderStyle>
									</asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="QuoteNo2" SortExpression="QuoteNo2" HeaderText="QuoteNo2"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="PartNo" SortExpression="partNo" HeaderText="Part No1">
										<HeaderStyle HorizontalAlign="Center" Width="200px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:ButtonColumn DataTextField="PartNo" SortExpression="PartNo" HeaderText="Part No" CommandName="Dwg_Command">
										<HeaderStyle HorizontalAlign="Center" Width="200px" VerticalAlign="Middle"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
									</asp:ButtonColumn>
									<asp:BoundColumn DataField="TotalPrice" HeaderText="Total Price" DataFormatString="{0:$#,###,###.00}">
										<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Note" HeaderText="Note">
										<HeaderStyle HorizontalAlign="Center" Width="200px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:ButtonColumn Text="Copy" HeaderText="Quote" CommandName="CopyCommand">
										<HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:ButtonColumn>
									<asp:ButtonColumn Text="Request" HeaderText="Lead Time" CommandName="LeadTimeCommand">
										<HeaderStyle HorizontalAlign="Center" Width="70px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:ButtonColumn>
									<asp:ButtonColumn DataTextField="File" HeaderText="File" CommandName="ViewFile_Command">
										<HeaderStyle HorizontalAlign="Center" Width="30px" VerticalAlign="Middle"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
									</asp:ButtonColumn>
									<asp:BoundColumn Visible="False" DataField="FileName" HeaderText="FileName"></asp:BoundColumn>
									<asp:ButtonColumn Visible="False" DataTextField="Dwg" HeaderText="Dwg" CommandName="ViewDWG_Command">
										<HeaderStyle HorizontalAlign="Center" Width="30px" VerticalAlign="Middle"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
									</asp:ButtonColumn>
									<asp:BoundColumn Visible="False" DataField="Dwg1" HeaderText="Dwg"></asp:BoundColumn>
								</Columns>
								<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
							</asp:DataGrid>
						</asp:panel><asp:panel id="PUnfinshed" runat="server" Width="995px" BorderStyle="None" BorderColor="SlateGray">
							<TABLE style="WIDTH: 992px; HEIGHT: 22px" id="Table3" border="0" cellSpacing="0" cellPadding="0"
								width="992">
								<TR>
									<TD style="WIDTH: 100px" width="100" noWrap>
										<asp:DropDownList id="DDLUContacts" runat="server" Font-Size="XX-Small" Width="168px" AutoPostBack="True" onselectedindexchanged="DDLUContacts_SelectedIndexChanged"></asp:DropDownList></TD>
									<TD style="WIDTH: 74px" width="74" noWrap>
										<asp:DropDownList id="DDLUCreateDate" runat="server" Font-Size="XX-Small" Width="80px" AutoPostBack="True" onselectedindexchanged="DDLUCreateDate_SelectedIndexChanged"></asp:DropDownList></TD>
									<TD style="WIDTH: 89px" width="89" noWrap>
										<asp:DropDownList id="DDLUQuoteNO" runat="server" Font-Size="XX-Small" Width="104px" AutoPostBack="True" onselectedindexchanged="DDLUQuoteNO_SelectedIndexChanged"></asp:DropDownList></TD>
									<TD style="WIDTH: 183px" width="183" noWrap>
										<asp:DropDownList id="DDLUPartNo" runat="server" Font-Size="XX-Small" Width="200px" AutoPostBack="True" onselectedindexchanged="DDLUPartNo_SelectedIndexChanged"></asp:DropDownList></TD>
									<TD colSpan="2" noWrap></TD>
								</TR>
							</TABLE>
							<asp:DataGrid id="DGUnfinished" runat="server" Font-Size="Smaller" Height="144px" Width="994px"
								BackColor="White" BorderStyle="None" BorderColor="Gainsboro" AutoGenerateColumns="False" AllowPaging="True"
								BorderWidth="1px" CellPadding="0" AllowSorting="True" OnSortCommand="DGUnfinished_SortCommand"
								OnItemCommand="DGUnfinished_Command" OnPageIndexChanged="DGUnfinished_PageChanger">
								<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
								<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
								<ItemStyle ForeColor="#000066"></ItemStyle>
								<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="DarkGray"></HeaderStyle>
								<Columns>
									<asp:BoundColumn DataField="Contact" SortExpression="Contact" HeaderText="Contact">
										<HeaderStyle HorizontalAlign="Center" Width="150px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="QuoteDate" SortExpression="QuoteDate" HeaderText="Creation Date" DataFormatString="{0:MM/dd/yyyy}">
										<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:HyperLinkColumn Target="_blank" DataNavigateUrlField="QuoteNo" DataNavigateUrlFormatString="print.aspx?id={0}&amp;type=icylinder"
										DataTextField="QuoteNo" HeaderText="Ref No" DataTextFormatString="{0:c}">
										<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:HyperLinkColumn>
									<asp:BoundColumn Visible="False" DataField="QuoteNo1" HeaderText="QuoteNo1"></asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="QuoteNo2" SortExpression="QuoteNo2" HeaderText="QuoteNo2"></asp:BoundColumn>
									<asp:BoundColumn DataField="PartNo" SortExpression="partNo" HeaderText="Part No">
										<HeaderStyle HorizontalAlign="Center" Width="200px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Specialrequest" HeaderText="Special Request">
										<HeaderStyle HorizontalAlign="Center" Width="230px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="FinishedDate" HeaderText="Finished Date">
										<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:HyperLinkColumn Target="_blank" DataNavigateUrlField="CowanQno" DataNavigateUrlFormatString="print.aspx?id={0}&amp;type=icylinder"
										DataTextField="CowanQno" HeaderText="Cowan QuoteNo">
										<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:HyperLinkColumn>
									<asp:ButtonColumn DataTextField="File" HeaderText="File" CommandName="ViewFile_Command">
										<HeaderStyle HorizontalAlign="Center" Width="30px" VerticalAlign="Middle"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
									</asp:ButtonColumn>
									<asp:BoundColumn Visible="False" DataField="FileName" HeaderText="FileName"></asp:BoundColumn>
									<asp:ButtonColumn DataTextField="Dwg" HeaderText="Dwg" CommandName="ViewDWG_Command">
										<HeaderStyle HorizontalAlign="Center" Width="30px" VerticalAlign="Middle"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
									</asp:ButtonColumn>
									<asp:BoundColumn Visible="False" DataField="Dwg1" HeaderText="Dwg1"></asp:BoundColumn>
								</Columns>
								<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
							</asp:DataGrid>
						</asp:panel><asp:panel id="PLeedtime" runat="server" Visible="False" BorderStyle="None" BorderColor="SlateGray">
							<TABLE style="WIDTH: 784px; HEIGHT: 67px" id="Table6" border="0" cellSpacing="0" cellPadding="0"
								width="784">
								<TR>
									<TD style="WIDTH: 177px"></TD>
									<TD style="WIDTH: 36px"></TD>
									<TD style="WIDTH: 440px"></TD>
									<TD style="WIDTH: 37px"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 177px">
										<asp:Label id="Label17" runat="server" Font-Bold="True" Font-Underline="True" Font-Size="X-Small"
											ForeColor="Red">Lead Time Request</asp:Label></TD>
									<TD style="WIDTH: 505px" bgColor="lightgrey" colSpan="2">
										<asp:Label id="Label21" runat="server" Font-Size="Smaller" Width="192px">Enter The following information:</asp:Label></TD>
									<TD style="WIDTH: 37px">
										<asp:Label id="lblrfldate" runat="server" Visible="False"></asp:Label></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 177px"></TD>
									<TD style="WIDTH: 36px" bgColor="lightgrey">
										<asp:Label id="Label18" runat="server" Font-Size="Smaller">To :</asp:Label></TD>
									<TD style="WIDTH: 440px" bgColor="lightgrey">
										<asp:TextBox id="TxtRLTTo" runat="server" Font-Size="XX-Small" Height="20px" Width="184px" ReadOnly="True"></asp:TextBox></TD>
									<TD style="WIDTH: 37px">
										<asp:Label id="lblrflqno" runat="server" Visible="False"></asp:Label></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 177px"></TD>
									<TD style="WIDTH: 36px" bgColor="lightgrey">
										<asp:Label id="Label20" runat="server" Font-Size="Smaller">Subject :</asp:Label></TD>
									<TD style="WIDTH: 440px" bgColor="lightgrey">
										<asp:TextBox id="TxtRLTSubject" runat="server" Font-Size="XX-Small" Height="20px" Width="272px"
											ReadOnly="True"></asp:TextBox></TD>
									<TD style="WIDTH: 37px">
										<asp:Label id="lblrflpno" runat="server" Visible="False"></asp:Label></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 177px; HEIGHT: 20px"></TD>
									<TD style="WIDTH: 36px; HEIGHT: 20px" bgColor="lightgrey">
										<asp:Label id="Label22" runat="server" Font-Size="Smaller">Message :</asp:Label></TD>
									<TD style="WIDTH: 440px; HEIGHT: 20px" bgColor="lightgrey">
										<asp:TextBox id="TxtRLTMessage" runat="server" Font-Size="Smaller" Height="41px" Width="408px"
											TextMode="MultiLine"></asp:TextBox></TD>
									<TD style="WIDTH: 37px; HEIGHT: 20px"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 177px"></TD>
									<TD style="WIDTH: 36px" bgColor="lightgrey"></TD>
									<TD style="WIDTH: 440px" bgColor="lightgrey">
										<asp:Button id="BtnSend" runat="server" Font-Underline="True" Font-Size="XX-Small" Height="18px"
											BackColor="LightSlateGray" Text="Send Request" BorderStyle="None" onclick="BtnSend_Click"></asp:Button></TD>
									<TD style="WIDTH: 37px"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 177px"></TD>
									<TD style="WIDTH: 36px" bgColor="lightgrey"></TD>
									<TD style="WIDTH: 440px" bgColor="lightgrey">
										<asp:Label id="Label19" runat="server" Height="20px" Visible="False"></asp:Label></TD>
									<TD style="WIDTH: 37px"></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:label id="Lblmail" runat="server" ForeColor="Red" Font-Size="Smaller"></asp:label><asp:label id="lblpage" runat="server"></asp:label></TD>
				</TR>
			</TABLE>
			<asp:label style="Z-INDEX: 105; POSITION: absolute; TOP: 136px; LEFT: 1016px" id="dwg" runat="server"></asp:label><asp:label style="Z-INDEX: 101; POSITION: absolute; TOP: 64px; LEFT: 1024px" id="Label3" runat="server"></asp:label><asp:label style="Z-INDEX: 102; POSITION: absolute; TOP: 24px; LEFT: 1024px" id="lbl" runat="server"
				Visible="False"></asp:label><asp:label style="Z-INDEX: 104; POSITION: absolute; TOP: 96px; LEFT: 1024px" id="err" runat="server"></asp:label></form>
	</body>
</HTML>
