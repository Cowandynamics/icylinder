using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Web.Security;
using System.ComponentModel;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Data.OleDb;
using System.Configuration;
namespace iCylinderV1
{
////	/ <summary>
////	/ Summary description for DBClass.
////	/ </summary>
	public class DBClass
	{
        private static string constr = ConfigurationSettings.AppSettings["QuoteSystemICylinder"];
        public static string ICylDBServer = ConfigurationSettings.AppSettings["QICylReportServer"];
        public static string ICylDBLogin = ConfigurationSettings.AppSettings["QICylReportLogin"];
        public static string ICylDBPassword = ConfigurationSettings.AppSettings["QICylReportPassword"];
        public static string ICylDB = ConfigurationSettings.AppSettings["QICylReportDatabase"];
        public static string ICylRoot = ConfigurationSettings.AppSettings["QICylRoot"];

		private static   SqlConnection SqlCon;
		private static readonly object m_Lock = new object();
		public static SqlConnection GetConnection()
		{
			if (SqlCon==null || SqlCon.State == ConnectionState.Closed )
			{
				SqlCon = new SqlConnection();
				try
				{
					SqlCon.ConnectionString = constr;
				}
				catch (Exception)
				{
					if(SqlCon.State !=ConnectionState.Closed)
					{
						SqlCon.Close();
					}
					SqlCon.Dispose();
				}
				lock (m_Lock)
				{   
					SqlCon.Open();
				}
			}
			
			return SqlCon;
		}
		public static void Close()
		{
			if (SqlCon!=null && SqlCon.State == ConnectionState.Open)
				SqlCon.Close();
		}
		public DBClass()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		//issue #624 start
		public string ATSeries_Get_BoreCat(string bcode)
		{  
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.StoredProcedure;
			command.CommandText= @"usp_Select_ATSeries_BoreCat";
			command.Parameters.Add("@bcode",bcode.Trim());
			SqlDataReader reader =null;
			string strResult="0";
			try
			{
				IDisposable connection=null;connection = GetConnection();command.Connection = (SqlConnection)connection;
				reader = command.ExecuteReader(CommandBehavior.CloseConnection);
				if(reader.Read())
				{
					strResult=reader.GetValue(0).ToString();
					
				}
				reader.Close();
				Close();
			}
			finally
			{
				if (reader != null)
				{
					reader.Close();
				}
				Close();
			}
			return strResult;
		}
		public string ATSeries_Get_T(string quoteno, string partno)
		{  
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.StoredProcedure;
			command.CommandText= @"usp_Select_ATSeries_T";
			command.Parameters.Add("@qno",quoteno.Trim());
			command.Parameters.Add("@pno",partno.Trim());
			SqlDataReader reader =null;
			string strResult="0";
			try
			{
				IDisposable connection=null;connection = GetConnection();command.Connection = (SqlConnection)connection;
				reader = command.ExecuteReader(CommandBehavior.CloseConnection);
				if(reader.Read())
				{
					strResult=reader.GetValue(0).ToString();
					
				}
				reader.Close();
				Close();
			}
			finally
			{
				if (reader != null)
				{
					reader.Close();
				}
				Close();
			}
			return strResult;
		}
		
		//issue #624 end
		//issue #137 start
		public string ASSeries_Has_Tandem(string quoteno, string partno)
		{  
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.Text;
			command.CommandText= "select PopoptsCode From WEB_Popular_Opts_Items_TableV1 Where QuoteNo=@quoteno AND PartNo=@partno AND PopoptsCode=@tandem";
			command.Parameters.Add("@quoteno", quoteno);
			command.Parameters.Add("@partno", partno);
			command.Parameters.Add("@tandem", "TC");
			DataTable dt = new DataTable();
			string strResult="";
			try
			{
				IDisposable connection=null;connection = GetConnection();command.Connection = (SqlConnection)connection;
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;
				oledad.Fill(dt);
				Close();
				if(dt.Rows.Count>0) strResult = dt.Rows[0]["PopoptsCode"].ToString();
			}
			finally
			{
				Close();
			}
			return strResult;
		}
		public string ASSeries_Has_RodEnd_Male(string rodend)
		{  
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.Text;
			command.CommandText= "select type From WEB_RodEndKK_TableV1 Where RodEnd_Code=@rodend ";
			command.Parameters.Add("@rodend", rodend);
			DataTable dt = new DataTable();
			string strResult="";
			try
			{
				IDisposable connection=null;connection = GetConnection();command.Connection = (SqlConnection)connection;
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;
				oledad.Fill(dt);
				Close();
				if(dt.Rows.Count>0) strResult = dt.Rows[0]["type"].ToString();
			}
			finally
			{
				Close();
			}
			return strResult;
		}
		public string Select_Series_QItem(string quoteno, string partno)
		{  
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.Text;
			command.CommandText= "select S_Code from WEB_Quotation_Items_TableV1 where ItemNo=@quoteno AND PartNo =@partno ";
			command.Parameters.Add("@quoteno",quoteno);
			command.Parameters.Add("@partno",partno);
			SqlDataReader reader =null;
			string strResult="0";
			try
			{
				IDisposable connection=null;connection = GetConnection();command.Connection = (SqlConnection)connection;
				reader = command.ExecuteReader(CommandBehavior.CloseConnection);
				if(reader.Read())
				{
					strResult=reader.GetValue(0).ToString();
					
				}
				reader.Close();
				Close();
			}
			finally
			{
				if (reader != null)
				{
					reader.Close();
				}
				Close();
			}
			return strResult;
		}
		
		//public string  InsertCanisters(string quoteno,string partno,string failmode,string canisterno,string preload ,string etc,string eto,string bto,string airpressure,string packingfriction,string springrate,string etcvalvetrust,string price,string canistercode, string btc)
		public string  InsertCanisters(string quoteno,string partno,string failmode,string canisterno,string preload ,string etc,string eto,string bto,string airpressure,string packingfriction,string springrate,string etcvalvetrust,string price,string canistercode, string btc, string btoreq)
			{  
			if(packingfriction=="") packingfriction="0";
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.StoredProcedure;
			command.CommandType = CommandType.StoredProcedure;
			command.CommandText= "usp_InsertCanisters";
			command.Parameters.Add("@quoteno",quoteno.Trim());
			command.Parameters.Add("@partno",partno.Trim());
			command.Parameters.Add("@failmode",failmode.Trim());
			command.Parameters.Add("@canisterno",canisterno.Trim());
			command.Parameters.Add("@etc",etc.Trim());
			command.Parameters.Add("@eto",eto.Trim());
			command.Parameters.Add("@bto",bto.Trim());
			command.Parameters.Add("@btc",btc.Trim());
			command.Parameters.Add("@airpressure",airpressure.Trim());
			command.Parameters.Add("@packingfriction",packingfriction.Trim());
			command.Parameters.Add("@springrate",springrate.Trim());
			command.Parameters.Add("@etcvalvetrust",etcvalvetrust.Trim());
			command.Parameters.Add("@price",price.Trim());
			command.Parameters.Add("@canistercode",canistercode.Trim());
			command.Parameters.Add("@preload",preload.Trim());
			//btoreq
			command.Parameters.Add("@btoreq",btoreq.Trim());
			string s="";
			try
			{
				IDisposable connection=null;connection = GetConnection();command.Connection = (SqlConnection)connection;
				s=command.ExecuteNonQuery().ToString();
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		
		
		public decimal SelectOneValueByAllinfo(string col1,string tab,string col2,string val1,string col3,string val2)
		{  		
			decimal s =0.00m;
			if(col1.Trim() !="" && col2.Trim() !="" && val1.Trim() !="" && col3.Trim() !="" && val2.Trim() !="")
			{
				string query="select  "+col1.ToString().Trim()+"  from   "+tab.ToString().Trim()+"  Where  "+col2.ToString().Trim()+" = '"+val1.ToString().Trim()+"' and "+col3.ToString().Trim()+" = '"+val2.ToString().Trim()+"'"; 
				SqlCommand command =new SqlCommand();
				command.CommandText = query;
				command.CommandType = CommandType.Text;	
				SqlDataReader reader =null;
				string str="";
				try
				{
					IDisposable connection=null;connection = GetConnection();command.Connection = (SqlConnection)connection;
					reader = command.ExecuteReader(CommandBehavior.CloseConnection);
					if(reader.Read())
					{
						str=reader.GetValue(0).ToString();
					}
					reader.Close();
					Close();
					if(str.Trim() !="")
					{
						s=Convert.ToDecimal(str.ToString());
					}
				}
					//issue #82 start
				catch(Exception ex)
				{
					//s=0;
					//throw(ex);
					
				}
				
				finally
				{
					if (reader != null)
					{
						reader.Close();
					}
					Close();
					
				}
				//issue #82 end
			}
			return s;
		}
		
		public blCanisterPN ASSeries_Select_FC(decimal etc,decimal stroke,decimal airpressure, decimal packingfriction)
		{  
			int intResult=0;
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.StoredProcedure;
			//command.CommandText= "usp_ASSeries_Select_FC_V5";
			command.CommandText= "usp_ASSeries_Select_FC_V7";
			//command.Parameters.Add("@etc",etc);
			command.Parameters.Add("@etcpreload",etc);
			command.Parameters.Add("@stroke",stroke);
			command.Parameters.Add("@airpressure",airpressure);
			command.Parameters.Add("@packingfriction",packingfriction);
			SqlParameter result = new SqlParameter("@result", SqlDbType.Int);
			result.Direction = ParameterDirection.Output;
			command.Parameters.Add(result);
			blCanisterPN blcanister = new blCanisterPN();
			string strResult=intResult.ToString();
			try
			{
				DataTable dt = new DataTable();
				IDisposable connection=null;connection = GetConnection();command.Connection = (SqlConnection)connection;
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;
				oledad.Fill(dt);
				Close();
				if(Convert.ToInt32(result.Value.ToString())>0)
				{
					blcanister.ETO =dt.Rows[0]["ETO"].ToString();
					blcanister.ETC=dt.Rows[0]["ETC"].ToString();
					blcanister.BTO=dt.Rows[0]["BTO"].ToString();
					blcanister.CanisterNo=dt.Rows[0]["CanisterNo"].ToString();
					blcanister.CanisterBoreSize=dt.Rows[0]["CanisterBoreValue"].ToString();
					blcanister.SpringRate=dt.Rows[0]["SpringRate"].ToString();
					blcanister.CylinderBoreNo=dt.Rows[0]["CylinderNo"].ToString();
					blcanister.CylinderBoreValue=dt.Rows[0]["CylinderBoreValue"].ToString();
					blcanister.Tandem=dt.Rows[0]["Tandem"].ToString();
					blcanister.FailMode=dt.Rows[0]["FailMode"].ToString();
					blcanister.Preload=dt.Rows[0]["Preload"].ToString();
					blcanister.BTC=dt.Rows[0]["BTC"].ToString();
                    blcanister.EtcValveTrust=etc.ToString();
				}
				else
				{
					blcanister=null;
				}
				
			}
			finally
			{
				
				Close();
			}
			return blcanister;
		}

		//public blCanisterPN ASSeries_Select_FO(decimal etcvalvetrust, decimal eto,decimal stroke,decimal airpressure)
		public blCanisterPN ASSeries_Select_FO(decimal etcvalvetrust, decimal eto,decimal stroke,decimal airpressure, decimal btoreq)
		{  
			int intResult = 0;
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.StoredProcedure;
			//command.CommandText= "usp_ASSeries_Select_FO_V5";
			command.CommandText= "usp_ASSeries_Select_FO_V7";
			command.Parameters.Add("@etcvalvetrust",etcvalvetrust);
			//command.Parameters.Add("@eto",eto);
			command.Parameters.Add("@etopreload",eto);
			command.Parameters.Add("@stroke",stroke);
			command.Parameters.Add("@airpressure",airpressure);
			command.Parameters.Add("@btorequired",btoreq);
			SqlParameter result = new SqlParameter("@result", SqlDbType.Int);
			result.Direction = ParameterDirection.Output;
			command.Parameters.Add(result);
			blCanisterPN blcanister = new blCanisterPN();
			string strResult="";
			strResult=intResult.ToString();
			try
			{
				DataTable dt = new DataTable();
				IDisposable connection=null;connection = GetConnection();command.Connection = (SqlConnection)connection;
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;
				oledad.Fill(dt);
				Close();
				if(Convert.ToInt32(result.Value.ToString())>0)
				{
					blcanister.ETO =dt.Rows[0]["ETO"].ToString();
					blcanister.ETC=dt.Rows[0]["ETC"].ToString();
					blcanister.BTO=dt.Rows[0]["BTO"].ToString();
					blcanister.CanisterNo=dt.Rows[0]["CanisterNo"].ToString();
					blcanister.CanisterBoreSize=dt.Rows[0]["CanisterBoreValue"].ToString();
					blcanister.SpringRate=dt.Rows[0]["SpringRate"].ToString();
					blcanister.CylinderBoreNo=dt.Rows[0]["CylinderBoreNo"].ToString();
					blcanister.CylinderBoreValue=dt.Rows[0]["CylinderBoreValue"].ToString();
					blcanister.Tandem=dt.Rows[0]["Tandem"].ToString();
					blcanister.FailMode=dt.Rows[0]["FailMode"].ToString();
					blcanister.Preload=dt.Rows[0]["Preload"].ToString();
					blcanister.BTC=dt.Rows[0]["BTC"].ToString();
                    blcanister.EtcValveTrust=etcvalvetrust.ToString();
				}
				else
				{
					blcanister=null;
				}

			}
			finally
			{
				Close();
			}
			return blcanister;
		}
		//issue #242 start
		public string Select_BoreMetric_ByBore(string bore)
		{  
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.Text;
			string query = "select Bore_Metric from Bore_TableV1 Where [Bore_Code]= @borevalue ";
			command.CommandText= query;
			command.Parameters.Add("@borevalue",bore.Trim());
			SqlDataReader reader =null;
			string strResult="";
			try
			{
				IDisposable connection=null;connection = GetConnection();command.Connection = (SqlConnection)connection;
				reader = command.ExecuteReader(CommandBehavior.CloseConnection);
				if(reader.Read())
				{
					strResult=reader.GetValue(0).ToString().Trim();
					
				}
				reader.Close();
				Close();
			}
			finally
			{
				if (reader != null)
				{
					reader.Close();
				}
				Close();
			}
			return strResult;
		}
		public string Select_RodMetric_ByRod(string rod)
		{  
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.Text;
			string query = "select Rod_Metric from RodSerAS_TableV1 Where Rod_Code=@rod order by Rod_SLNO";
			command.Parameters.Add("@rod",rod.Trim());
			command.CommandText= query;
			SqlDataReader reader =null;
			string strResult="";
			try
			{
				IDisposable connection=null;connection = GetConnection();command.Connection = (SqlConnection)connection;
				reader = command.ExecuteReader(CommandBehavior.CloseConnection);
				if(reader.Read())
				{
					strResult=reader.GetValue(0).ToString().Trim();
					
				}
				reader.Close();
				Close();
			}
			finally
			{
				if (reader != null)
				{
					reader.Close();
				}
				Close();
			}
			return strResult;
		}
		//issue #242 end
		public string Select_BoreCode_ByBoreValue(string borevalue)
		{  
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.Text;
			string query = "select Bore_Code, Bore_Size from Bore_TableV1 Where [Bore_Value]= @borevalue ";
			command.CommandText= query;
			command.Parameters.Add("@borevalue",borevalue.Trim());
			SqlDataReader reader =null;
			string strResult="";
			try
			{
				IDisposable connection=null;connection = GetConnection();command.Connection = (SqlConnection)connection;
				reader = command.ExecuteReader(CommandBehavior.CloseConnection);
				if(reader.Read())
				{
					strResult=reader.GetValue(0).ToString().Trim()+"|";
					strResult+=reader.GetValue(1).ToString();
				}
				reader.Close();
				Close();
			}
			finally
			{
				if (reader != null)
				{
					reader.Close();
				}
				Close();
			}
			return strResult;
		}
		public string Select_RodDependency_ByBore(string bore)
		{  
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.Text;
			string query = "select Rod_Code,Rod_Size from RodSerAS_TableV1 Where ["+bore.ToString()+"]= 2 order by Rod_SLNO";
			command.CommandText= query;
			SqlDataReader reader =null;
			string strResult="";
			try
			{
				IDisposable connection=null;connection = GetConnection();command.Connection = (SqlConnection)connection;
				reader = command.ExecuteReader(CommandBehavior.CloseConnection);
				if(reader.Read())
				{
					strResult=reader.GetValue(0).ToString().Trim()+"|";
					strResult+=reader.GetValue(1).ToString();
				}
				reader.Close();
				Close();
			}
			finally
			{
				if (reader != null)
				{
					reader.Close();
				}
				Close();
			}
			return strResult;
		}
        
		public string  AS_Insertcomments(string quoteno, string comment)
		{  
			string query = "INSERT INTO WEB_Comment_TableV1(QuoteNo, Comments)Values ('"+quoteno+"', '"+comment+"')";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string s="";
			try
			{
				command.Connection = GetConnection();
			
				s=command.ExecuteNonQuery().ToString();
				Close();
			}
			catch (Exception ex)
			{		
				s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}

		//issue #137 end
		
		public ArrayList SelectOptions_Bore(string col1,string col2,string tab, string series,string order)
		{  
		
			//SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select "+col1.Trim()+","+col2.Trim()+" from "+tab.Trim()+" Where "+series.ToString()+"= 1 order by "+order.Trim()+"";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list1 = new ArrayList();
			ArrayList list2 = new ArrayList();
			ArrayList list = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			 
				while(reader.Read())
				{
					list1.Add(reader.GetValue(0));
					list2.Add(reader.GetValue(1));
		
				}
				reader.Close();
				Close();
				list.Add(list1);
				list.Add(list2);
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectOptions(string col1,string col2,string tab, string series,string sort)
		{  
		
			//SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select "+col1.Trim()+","+col2.Trim()+" from "+tab.Trim()+" Where "+series.ToString()+"= 1 order by "+sort.Trim()+"";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list1 = new ArrayList();
			ArrayList list2 = new ArrayList();
			ArrayList list = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			 
				while(reader.Read())
				{
					list1.Add(reader.GetValue(0));
					list2.Add(reader.GetValue(1));
		
				}
				reader.Close();
				Close();
				list.Add(list1);
				list.Add(list2);
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectOptions_Mounts(string col1,string col2,string col3,string tab, string series,string bore)
		{  
		
			//SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select "+col1.Trim()+","+col2.Trim()+","+col3.Trim()+" from "+tab.Trim()+" Where "+series.ToString()+"= 1 and "+bore.Trim()+"=1   order by "+col1.Trim()+"";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list1 = new ArrayList();
			ArrayList list2 = new ArrayList();
			ArrayList list3 = new ArrayList();
			ArrayList list = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			 
				while(reader.Read())
				{
					list1.Add(reader.GetValue(0));
					list2.Add(reader.GetValue(1));
					list3.Add(reader.GetValue(2));
		
				}
				reader.Close();
				Close();
				list.Add(list1);
				list.Add(list2);
				list.Add(list3);
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectValues(string col1,string tab, string series)
		{  
		
			//SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select distinct "+col1.Trim()+" from "+tab.Trim()+" Where "+series.ToString()+"= 1";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			 
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
				}
				reader.Close();
				Close();
			
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		
		public ArrayList SelectValues_List(string col1,string tab, string col2,string val)
		{  
			string query ="";
			if( val.ToString().Trim() =="Z" ||val.ToString().Trim() =="")
			{
				query = "select distinct ValveSize from WEB_Specials_Velan_Pricing_TableV1";
			}
			else
			{
				 query = "select distinct "+col1.Trim()+" from "+tab.Trim()+" Where  "+col2.ToString()+" = '"+val.Trim()+"'";
			}
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			 
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
				}
				reader.Close();
				Close();
			
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectValues_Rodend(string col1,string tab, string sys,string type, string rod)
		{  
		
			//SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select  "+col1.Trim()+" from "+tab.Trim()+" Where system='"+sys.ToString()+"' and type='"+type.Trim() +"' and "+rod.Trim()+"= 1";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			 
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
				}
				reader.Close();
				Close();
			
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectValues_RodendKK(string tab)
		{  
		
			//SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select KK from "+tab.Trim()+" order by Slno asc";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			 
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
				}
				reader.Close();
				Close();
			
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectValues_RodendKK_CD(string tab, string kk)
		{  
		
			//SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select distinct  CD from "+tab.Trim()+" Where KK='"+kk.ToString()+"' ";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			 
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
				}
				reader.Close();
				Close();
			
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectValues_CD(string tab, string bore)
		{  
		
			//SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select distinct  CD from "+tab.Trim()+" Where "+bore.Trim()+"=1";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			 
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
				}
				reader.Close();
				Close();
			
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public string SelectValues_Accessories(string col,string tab, string col2,string val)
		{  
		
			//SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select "+col.Trim()+" from "+tab.Trim()+" Where "+col2.Trim()+"='"+val.Trim() +"'";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string str="";
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
				
				if(reader.Read())
				{
					str=(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			
			}
			catch (Exception ex)
			{		
				str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return str;
		}
		public ArrayList SelectAccessories_Discription(string tab,string accessory, string kk,string cd)
		{  
		
			//SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select PartNo,Description from "+tab.Trim()+" Where Accessory='"+accessory.ToString()+"' and KK='"+kk.ToString()+"'  and CD='"+cd.ToString() +"'";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList();
			ArrayList list1 = new ArrayList();
			ArrayList list2 = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			 
				while(reader.Read())
				{
					list1.Add(reader.GetValue(0));
					list2.Add(reader.GetValue(1));
				}
				list.Add(list1);
				list.Add(list2);
				reader.Close();
				Close();
			
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectAccessories_Discription1(string tab,string accessory,string cd)
		{  
		
			//SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select PartNo,Description from "+tab.Trim()+" Where Accessory='"+accessory.ToString()+"' and CD='"+cd.ToString() +"'";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList();
			ArrayList list1 = new ArrayList();
			ArrayList list2 = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			 
				while(reader.Read())
				{
					list1.Add(reader.GetValue(0));
					list2.Add(reader.GetValue(1));
				}
				list.Add(list1);
				list.Add(list2);
				reader.Close();
				Close();
			
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectAccessories_Discription2(string tab,string accessory, string kk)
		{  
		
			//SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select PartNo,Description from "+tab.Trim()+" Where Accessory='"+accessory.ToString()+"' and KK='"+kk.ToString()+"'";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList();
			ArrayList list1 = new ArrayList();
			ArrayList list2 = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			 
				while(reader.Read())
				{
					list1.Add(reader.GetValue(0));
					list2.Add(reader.GetValue(1));
				}
				list.Add(list1);
				list.Add(list2);
				reader.Close();
				Close();
			
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public string SelectValue_REnd(string col1,string tab, string sys,string type, string kk)
		{  
		
			//SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select  "+col1.Trim()+" from "+tab.Trim()+" Where system='"+sys.ToString()+"' and type='"+type.Trim() +"' and kk= '"+kk.Trim()+"'";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string str="";
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			 
				if(reader.Read())
				{
					str=(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			
			}
			catch (Exception ex)
			{		
				str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return str;
		}
		public string SelectOneValue(string col1,string tab, string series,string val)
		{  
		
			//SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select "+col1.Trim()+" from "+tab.Trim()+" Where "+series.ToString()+"="+val.Trim() ;
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string str="";
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			 
				if(reader.Read())
				{
					str=(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			
			}
			catch (Exception ex)
			{		
				str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return str;
		}
		public string SelectValue(string  pname ,string ptab,string pcode , string  data)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select "+pname.ToString().Trim()+" from "+ptab.ToString().Trim()+"  where "+pcode.ToString().Trim()+"  ='"+data.ToString().Trim()+"'";
			string str ="";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
				if(reader.Read())
				{
					str =(reader.GetValue(0)).ToString();
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return str;
		}	
		public string SelectValue_Rotork(string  pname ,string ptab,string pcode , string  data,string order)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select "+pname.ToString().Trim()+" from "+ptab.ToString().Trim()+"  where "+pcode.ToString().Trim()+"  ='"+data.ToString().Trim()+"' Order By "+order.Trim()+" ASC;";
			string str ="";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
				if(reader.Read())
				{
					str =(reader.GetValue(0)).ToString();
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return str;
		}
		public string SelectBoreValue_Rotork(string  bore)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select BoreValue from WEB_Bore_TableV1  where BoreValue >'"+bore.ToString().Trim()+"' Order By BoreValue ASC;";
			string str ="";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
				if(reader.Read())
				{
					str =(reader.GetValue(0)).ToString();
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return str;
		}
		public string  SelectPortSizeNo(string bore,string port,string tab)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select "+port.Trim() +" from "+tab.Trim() +" where Bore ='"+bore.ToString().Trim() +"'";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			string s="";
			command.CommandType = CommandType.Text;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				if(reader.Read())
				{
					s=(reader.GetValue(0).ToString());
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public string  SelectRRD(string rrcode)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select RodEnd_Dimension from WEB_RodEndDiamension_TableV1 where RR_Code ='"+rrcode.ToString().Trim() +"'";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			string s="";
			command.CommandType = CommandType.Text;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				if(reader.Read())
				{
					s=(reader.GetValue(0).ToString());
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public  ArrayList SelectContactName(string custmerid)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select CompanyName,LastName from WEB_Customers_TableV1 where CustomerID=@custmerid "; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@custmerid",custmerid.Trim());
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				if(reader.Read())
				{ 
					list.Add(reader.GetValue(0).ToString());
					list.Add(reader.GetValue(1).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectSeries_WithCode()
		{  
	
			string query = "select Series_Code,Series_Name from WEB_Series_TableV1 order by Series_SLNO";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list1 = new ArrayList();
			ArrayList list2 = new ArrayList();
			ArrayList list3 = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
				
				while(reader.Read())
				{
					list1.Add(reader.GetValue(0));
					list2.Add(reader.GetValue(1));
		
				}
				list3.Add(list1);
				list3.Add(list2);
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list1.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list3;
		}
		public  ArrayList SelectAvailableSeries_details(string companyid ,string contactid)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			//issue #191 start
			//back
			//string query="select PA,PS,PC,M,N,L,ML,MH,R,A,SA,AT,RP,AC,R5 from WEB_Customers_TableV1 where CompanyID=@company and CustomerID=@contact"; 
			//update
			string query="select PA,PS,PC,M,N,L,ML,MH,R,A,SA,AT,RP,AC,R5, [AS] from WEB_Customers_TableV1 where CompanyID=@company and CustomerID=@contact"; 
			//issue #191 end
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@company",companyid.Trim());
			command.Parameters.Add("@contact",contactid.Trim());
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{ 
					list.Add(reader.GetValue(0).ToString());
					list.Add(reader.GetValue(1).ToString());
					list.Add(reader.GetValue(2).ToString());
					list.Add(reader.GetValue(3).ToString());
					list.Add(reader.GetValue(4).ToString());
					list.Add(reader.GetValue(5).ToString());
					list.Add(reader.GetValue(6).ToString());
					list.Add(reader.GetValue(7).ToString());
					list.Add(reader.GetValue(8).ToString());
					list.Add(reader.GetValue(9).ToString());
					list.Add(reader.GetValue(10).ToString());
					list.Add(reader.GetValue(11).ToString());
					string strtest= reader.GetValue(11).ToString();
					list.Add(reader.GetValue(12).ToString());
					list.Add(reader.GetValue(13).ToString());
					list.Add(reader.GetValue(14).ToString());
                    //issue #318 start
					//back
					//list.Add(reader.GetValue(15).ToString());
					//update
					list.Add("");
					//issue #318 end
				}
				reader.Close();
				Close();
				
			}
			
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList SelectRodEndReverse(string rodend)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select system,type,kk from WEB_RodEndKK_TableV1 where RodEnd_Code=@rodend "; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@rodend",rodend.Trim());
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				if(reader.Read())
				{ 
					list.Add(reader.GetValue(0).ToString());
					list.Add(reader.GetValue(1).ToString());
					list.Add(reader.GetValue(2).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public string  InsertQuoteNo(string qno)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "INSERT INTO WEB_Pricing_Customer_TableV1 (QuoteNo) values( '"+qno.Trim()+"')";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string s="";
			try
			{
				command.Connection = GetConnection();
			
				s=command.ExecuteNonQuery().ToString();
				Close();
			}
			catch (Exception ex)
			{		
				s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public void InsertQuoteCount(string j)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="Update WEB_QuoteCount_TableV1 set QuoteNo='"+j.Trim()+"' where SLNO =1";
				SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			try
			{
				command.Connection = GetConnection();
			
				command.ExecuteNonQuery().ToString();
				Close();
			}
			catch (Exception ex)
			{		
				string str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
		}
		public void InsertSpecialCount(string j)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="Update WEB_Special_Count_TableV1 set Spno='"+j.Trim()+"' where Slno =1";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			try
			{
				command.Connection = GetConnection();
				command.ExecuteNonQuery().ToString();
				Close();
			}
			catch (Exception ex)
			{		
				string str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
		}
		public  ArrayList SelectContactdetails(string companyid,string customerid)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select * from WEB_Customers_TableV1 where CustomerID=@customerid  and CompanyID=@companyid"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@customerid",customerid.Trim());
			command.Parameters.Add("@companyid",companyid.Trim());
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{ 
					list.Add(reader.GetValue(3).ToString());
					list.Add(reader.GetValue(4).ToString());
					list.Add(reader.GetValue(5).ToString());
					list.Add(reader.GetValue(6).ToString());
					list.Add(reader.GetValue(7).ToString());
					list.Add(reader.GetValue(8).ToString());
					list.Add(reader.GetValue(9).ToString());
					list.Add(reader.GetValue(10).ToString());
					list.Add(reader.GetValue(11).ToString());
					list.Add(reader.GetValue(12).ToString());
					list.Add(reader.GetValue(13).ToString());
					list.Add(reader.GetValue(14).ToString());
					list.Add(reader.GetValue(15).ToString());
					list.Add(reader.GetValue(16).ToString());
					list.Add(reader.GetValue(17).ToString());
					list.Add(reader.GetValue(18).ToString());
					list.Add(reader.GetValue(21).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList SelectCompanyterms(string customerid,string companyid)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select  SalesmenCode,currency,Terms,Office,Lang,CompanyID from WEB_Customers_TableV1 where CustomerID=@customerid  and CompanyID=@companyid" ; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@customerid",customerid.Trim());
			command.Parameters.Add("@companyid",companyid.Trim());
			ArrayList list =new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				if(reader.Read())
				{ 
					list.Add(reader.GetValue(0).ToString());
					list.Add(reader.GetValue(1).ToString());
					list.Add(reader.GetValue(2).ToString());
					list.Add(reader.GetValue(3).ToString());
					list.Add(reader.GetValue(4).ToString());
					list.Add(reader.GetValue(5).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public decimal SelectAddersPrice(string l,string tab,string bore,string rod)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select  "+l.ToString().Trim()+"  from   "+tab.ToString().Trim()+"  Where BoreSize= '"+bore.ToString().Trim()+"' and RodSize= '"+rod.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;	
			decimal s =0.00m;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
		
				if(reader.Read())
				{
					s=Convert.ToDecimal(reader.GetValue(0));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				string str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public decimal SelectAddersPrice_Specials_ByBore(string l,string tab,string bore)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select  "+l.ToString().Trim()+"  from   "+tab.ToString().Trim()+"  Where BoreSize =  '"+bore.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;	
			decimal s =0.00m;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
		
				if(reader.Read())
				{
					s=Convert.ToDecimal(reader.GetValue(0));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				string str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public decimal SelectAddersPrice_Specials(string l,string tab,string valve)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select  "+l.ToString().Trim()+"  from   "+tab.ToString().Trim()+"  Where ValveSize =  '"+valve.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;	
			decimal s =0.00m;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
		
				if(reader.Read())
				{
					s=Convert.ToDecimal(reader.GetValue(0));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				string str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public decimal SelectAddersPrice_Specials_Velan(string l,string tab,string valve,string bore)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select  "+l.ToString().Trim()+"  from   "+tab.ToString().Trim()+"  Where ValveSize like  '%"+valve.ToString().Trim()+"%' and  BoreSize= '"+bore.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;	
			decimal s =0.00m;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
		
				if(reader.Read())
				{
					s=Convert.ToDecimal(reader.GetValue(0));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				string str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public decimal SelectAddersPrice_Specials_VelanBore(string l,string tab,string bore)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select  "+l.ToString().Trim()+"  from   "+tab.ToString().Trim()+"  Where BoreSize= '"+bore.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;	
			decimal s =0.00m;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
		
				if(reader.Read())
				{
					s=Convert.ToDecimal(reader.GetValue(0));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				string str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public decimal SelectAddersPrice_Specials_Velan(string col,string tab,string pcode)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select  "+col.ToString().Trim()+"  from   "+tab.ToString().Trim()+"  Where PortCode= '"+pcode.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;	
			decimal s =0.00m;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
		
				if(reader.Read())
				{
					s=Convert.ToDecimal(reader.GetValue(0));		
				}
				reader.Close();
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public decimal SelectAddersPrice_Transducer(string col,string pcode)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select  "+col.ToString().Trim()+"  from   [WEB_Transducer_Price_TableV1]  Where [Stroke] >= '"+pcode.ToString().Trim()+"'  Order by [Stroke] ASC"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;	
			decimal s =0.00m;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();		
				if(reader.Read())
				{
					s=Convert.ToDecimal(reader.GetValue(0));		
				}
				reader.Close();
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public  string SelectAppopts(string code)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select  Description from WEB_ApplicationOpt_TableV1 where Application_Code='"+code.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string s ="";
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					s=reader.GetValue(0).ToString();
				
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public string  InsertItemApplicationOpts(string qno,string pno,string appoptcode,string desc,string price)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "INSERT INTO WEB_Application_Opts_Items_TableV1(QuoteNo,PartNo, AppOptCode, AppOpts, AppOptPrice) VALUES ('"+qno.ToString()+"','"+pno.ToString()+"','"+appoptcode.ToString()+"','"+desc.ToString()+"', '"+price.ToString()+"')";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string s="";
			try
			{
				command.Connection = GetConnection();
			
				s=command.ExecuteNonQuery().ToString();
				Close();
			}
			catch (Exception ex)
			{		
				s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public  string SelectPopopts(string code)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select  Description from WEB_Popular_Opt_TableV1 where Popular_Code='"+code.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string s ="";
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					s=reader.GetValue(0).ToString();
				
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public string  InsertItemPopularOpts(string qno,string pno,string popoptscode,string desc,string popoptprice)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "INSERT INTO WEB_Popular_Opts_Items_TableV1( QuoteNo,PartNo,PopOptsCode,PopOpts, PopoptPrice) VALUES ('"+qno.ToString()+"','"+pno.ToString()+"','"+popoptscode.ToString()+"','"+desc.ToString()+"', '"+popoptprice.ToString()+"')";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string s="";
			try
			{
				command.Connection = GetConnection();
			
				s=command.ExecuteNonQuery().ToString();
				Close();
			}
			catch (Exception ex)
			{		
				s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		//metricqitem
		public string  InsertItems(QItems q)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			//issue #242 start
			//back
//			string query = "INSERT INTO WEB_Quotation_Items_TableV1(ItemNo, PartNo, S_Code, Series, S_Price, B_Code, Bore, B_Price, R_Code, Rod, R_Price, StrokeCode, Stroke, Stroke_Price, M_code, Mount, M_Price, RE_Code,RodEnd,RE_Price,Cu_Code,Cushion,Cu_Price,CushionPos_Code,CushionPos,"+
//				"CushionPos_Price,ApplicationOPtID,Sel_Code,Seal,Sel_Price,Port_Code,Port,Port_Price,PP_Code,PortPos,PP_Price,PopularOptID,Special_ID,Quantity,Discount,UnitPrice,TotalPrice,Cusomer_ID,Quotation_No,Q_Date,User_ID,PriceList,Finish,AWeight,Note,Dwg) VALUES "+
//				"(@ItemNo,@PartNo,@S_Code,@Series,@S_Price, @B_Code, @Bore, @B_Price,"+
//				"@R_Code,@Rod,@R_Price,@Stroke_Code,@Stroke,@Stroke_Price,@M_code,@Mount,"+
//				"@M_Price, @RE_Code,@RodEnd, @RE_Price,@Cu_Code,@Cushion, @Cu_Price,"+
//				"@CushionPos_Code,@CushionPos, @CushionPos_Price, @ApplicationOpt_Id, @Sel_Code,"+
//				"@Seal, @Sel_Price, @Port_Code, @Port, @Port_Price, @PP_Code, @PortPos, @PP_Price, @PopularOpt_Id,"+
//				"@Special_ID, @Quantity, @Discount, @UnitPrice, @TotalPrice, @Cusomer_ID, @Quotation_No, @Q_Date, @User_ID,@PriceList,@finish,@aweight,@note,@dwg)";
			//update
			string query = "INSERT INTO WEB_Quotation_Items_TableV1(ItemNo, PartNo, S_Code, Series, S_Price, B_Code, Bore, B_Price, R_Code, Rod, R_Price, StrokeCode, Stroke, Stroke_Price, M_code, Mount, M_Price, RE_Code,RodEnd,RE_Price,Cu_Code,Cushion,Cu_Price,CushionPos_Code,CushionPos,"+
				"CushionPos_Price,ApplicationOPtID,Sel_Code,Seal,Sel_Price,Port_Code,Port,Port_Price,PP_Code,PortPos,PP_Price,PopularOptID,Special_ID,Quantity,Discount,UnitPrice,TotalPrice,Cusomer_ID,Quotation_No,Q_Date,User_ID,PriceList,Finish,AWeight,Note,Dwg,Units) VALUES "+
				"(@ItemNo,@PartNo,@S_Code,@Series,@S_Price, @B_Code, @Bore, @B_Price,"+
				"@R_Code,@Rod,@R_Price,@Stroke_Code,@Stroke,@Stroke_Price,@M_code,@Mount,"+
				"@M_Price, @RE_Code,@RodEnd, @RE_Price,@Cu_Code,@Cushion, @Cu_Price,"+
				"@CushionPos_Code,@CushionPos, @CushionPos_Price, @ApplicationOpt_Id, @Sel_Code,"+
				"@Seal, @Sel_Price, @Port_Code, @Port, @Port_Price, @PP_Code, @PortPos, @PP_Price, @PopularOpt_Id,"+
				"@Special_ID, @Quantity, @Discount, @UnitPrice, @TotalPrice, @Cusomer_ID, @Quotation_No, @Q_Date, @User_ID,@PriceList,@finish,@aweight,@note,@dwg,@units)";
			//issue #242 end
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@ItemNo",q.ItemNo.Trim());
			command.Parameters.Add("@PartNo",q.PartNo.Trim());
			command.Parameters.Add("@S_Code",q.S_Code.Trim());
			command.Parameters.Add("@Series",q.Series.Trim());
			command.Parameters.Add("@S_Price",q.S_Price.Trim());
			command.Parameters.Add("@B_Code",q.B_Code.Trim());
			command.Parameters.Add("@Bore",q.Bore.Trim());
			command.Parameters.Add("@B_Price",q.B_Price.Trim());
			command.Parameters.Add("@R_Code",q.R_Code.Trim());
			command.Parameters.Add("@Rod",q.Rod.Trim());
			command.Parameters.Add("@R_Price",q.R_Price.Trim());
			command.Parameters.Add("@Stroke_Code",q.Stroke_Code.Trim());
			command.Parameters.Add("@Stroke",q.Stroke.Trim());
			command.Parameters.Add("@Stroke_Price",q.Stroke_Price.Trim());
			command.Parameters.Add("@M_code",q.M_code.Trim());
			command.Parameters.Add("@Mount",q.Mount.Trim());
			command.Parameters.Add("@M_Price",q.M_Price.Trim());
			command.Parameters.Add("@RE_Code",q.RE_Code.Trim());
			command.Parameters.Add("@RodEnd",q.RodEnd.Trim());
			command.Parameters.Add("@RE_Price",q.RE_Price.Trim());
			command.Parameters.Add("@Cu_Code",q.Cu_Code.Trim());
			command.Parameters.Add("@Cushion",q.Cushion.Trim());
			command.Parameters.Add("@Cu_Price",q.Cu_Price.Trim());
			command.Parameters.Add("@CushionPos_Code",q.CushionPos_Code.Trim());
			command.Parameters.Add("@CushionPos",q.CushionPos.Trim());
			command.Parameters.Add("@CushionPos_Price",q.CushionPos_Price.Trim());
			command.Parameters.Add("@ApplicationOpt_Id",q.ApplicationOpt_Id.Trim());
			command.Parameters.Add("@Sel_Code",q.Sel_Code.Trim());
			command.Parameters.Add("@Seal",q.Seal.Trim());
			command.Parameters.Add("@Sel_Price",q.Sel_Price.Trim());
			command.Parameters.Add("@Port_Code",q.Port_Code.Trim());
			command.Parameters.Add("@Port",q.Port.Trim());
			command.Parameters.Add("@Port_Price",q.Port_Price.Trim());
			command.Parameters.Add("@PP_Code",q.PP_Code.Trim());
			command.Parameters.Add("@PortPos",q.PortPos.Trim());
			command.Parameters.Add("@PP_Price",q.PP_Price.Trim());
			command.Parameters.Add("@PopularOpt_Id",q.PopularOpt_Id.Trim());
			command.Parameters.Add("@Special_ID",q.Special_ID.Trim());
			command.Parameters.Add("@Quantity",q.Quantity.Trim());
			command.Parameters.Add("@Discount",q.Discount.Trim());
			command.Parameters.Add("@UnitPrice",q.UnitPrice.Trim());
			command.Parameters.Add("@TotalPrice",q.TotalPrice.Trim());
			command.Parameters.Add("@Cusomer_ID",q.Cusomer_ID.Trim());
			command.Parameters.Add("@Quotation_No",q.Quotation_No.Trim());
			command.Parameters.Add("@Q_Date",q.Q_Date.Trim());
			command.Parameters.Add("@User_ID",q.User_ID.Trim());
			command.Parameters.Add("@PriceList",q.PriceList.Trim());
			command.Parameters.Add("@finish",q.SpecialReq.Trim());
			command.Parameters.Add("@aweight",q.Weight.Trim());
			command.Parameters.Add("@note",q.Note.Trim());
			command.Parameters.Add("@dwg",q.DWG.Trim());
			//issue #242 start
			command.Parameters.Add("@units",q.Units.Trim());
			//issue #242 end
			string s="";
			try
			{
				command.Connection = GetConnection();
				s=command.ExecuteNonQuery().ToString();
				Close();
			}
			catch (Exception ex)
			{		
				s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public  string SelectCustomerQno(string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select SL_NO from WEB_Pricing_Customer_TableV1 where QuoteNo='"+qno.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string str="";
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					str =reader.GetValue(0).ToString();
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return str;
		}
		public string  UpdateCustomerQuote_UpLoadFile(string  file,string slno)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "set DateFormat mdy; Update WEB_Pricing_Customer_TableV1 set UpLoadFile=@file  where SL_NO=@slno";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@file",file.Trim());
			command.Parameters.Add("@slno",slno.Trim());
			string s="";
			try
			{
				command.Connection = GetConnection();
				s=command.ExecuteNonQuery().ToString();
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public string  UpdateCustomerQuote(Quotation q,string slno)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "set DateFormat mdy; Update WEB_Pricing_Customer_TableV1 set Office=@Office, Customer=@Customer, Contact=@Contact, AttnTo1=@AttnTo1, AttnTo2=@AttnTo2, AttnTo3= @AttnTo3, BillTo1=@BillTo1, BillTo2=@BillTo2, BillTo3=@BillTo3, ShipTo1=@ShipTo1, ShipTo2= @ShipTo2, ShipTo3= @ShipTo3, QuoteDate=@Quotedate, ExpiryDate=@ExpiryDate, PrepairedBy=@PrepairedBy, Code=@Code, Lang=@Langu, Terms=@Terms, Delivery=@Delivery, Currency=@Currency, Note=@Note, Items=@Items, Finished=@Finish , FinishedDate=@Finishdate ,CowanQno=@cowanQno,SearchDate=@searchdate,CompanyID=@CompanyId,SpecSheet=@specsheet  where QuoteNo=@QuoteNo and SL_NO=@slno";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@Office",q.Office.Trim());
			command.Parameters.Add("@Customer",q.Customer.Trim());
			command.Parameters.Add("@Contact",q.Contact.Trim());
			command.Parameters.Add("@AttnTo1",q.AttnTo1.Trim());
			command.Parameters.Add("@AttnTo2",q.AttnTo2.Trim());
			command.Parameters.Add("@AttnTo3",q.AttnTo3.Trim());
			command.Parameters.Add("@BillTo1",q.BillTo1.Trim());
			command.Parameters.Add("@BillTo2",q.BillTo2.Trim());
			command.Parameters.Add("@BillTo3",q.BillTo3.Trim());
			command.Parameters.Add("@ShipTo1",q.ShipTo1.Trim());
			command.Parameters.Add("@ShipTo2",q.ShipTo2.Trim());
			command.Parameters.Add("@ShipTo3",q.ShipTo3.Trim());
			command.Parameters.Add("@QuoteNo",q.QuoteNo.Trim());
			command.Parameters.Add("@Quotedate",q.Quotedate.Trim());
			command.Parameters.Add("@ExpiryDate",q.ExpiryDate.Trim());
			command.Parameters.Add("@PrepairedBy",q.PrepairedBy.Trim());
			command.Parameters.Add("@Code",q.Code.Trim());
			command.Parameters.Add("@Langu",q.Langu.Trim());
			command.Parameters.Add("@Terms",q.Terms.Trim());
			command.Parameters.Add("@Delivery",q.Delivery.Trim());
			command.Parameters.Add("@Currency",q.Currency.Trim());
			command.Parameters.Add("@Items",q.Items.Trim());
			command.Parameters.Add("@Note",q.Note.Trim());
			command.Parameters.Add("@Finish",q.Finish.Trim());
			command.Parameters.Add("@FinishDate",q.FinishedDate);
			command.Parameters.Add("@CowanQno",q.CowanQno);
			command.Parameters.Add("@CompanyId",q.CompanyID);
			command.Parameters.Add("@searchdate",q.Quotedate.Trim());
			command.Parameters.Add("@specsheet",q.SpecSheet.Trim());
			command.Parameters.Add("@slno",slno.Trim());
			string s="";
			try
			{
				command.Connection = GetConnection();			
				s=command.ExecuteNonQuery().ToString();
				Close();
			}
			catch (Exception ex)
			{		
				s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public string  InsertCustomerQuote(Quotation q)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "set DateFormat mdy; INSERT INTO WEB_Pricing_Customer_TableV1( Office, Customer, Contact, AttnTo1, AttnTo2, AttnTo3, BillTo1, BillTo2, BillTo3, ShipTo1, ShipTo2, ShipTo3, QuoteNo, QuoteDate, ExpiryDate, PrepairedBy, Code, Lang, Terms, Delivery, Currency, Note, Items,Finished, FinishedDate,CowanQno,SearchDate,CompanyID,SpecSheet) VALUES (@Office,@Customer,@Contact,@AttnTo1,@AttnTo2, @AttnTo3,@BillTo1,@BillTo2,@BillTo3,@ShipTo1,@ShipTo2,@ShipTo3,@QuoteNo,@Quotedate,@ExpiryDate,@PrepairedBy,@Code,@Langu,@Terms,@Delivery,@Currency,@Note,@Items,@Finish, @FinishDate,@CowanQno,@SearchDate,@CompanyId,@specsheet)";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@Office",q.Office.Trim());
			command.Parameters.Add("@Customer",q.Customer.Trim());
			command.Parameters.Add("@Contact",q.Contact.Trim());
			command.Parameters.Add("@AttnTo1",q.AttnTo1.Trim());
			command.Parameters.Add("@AttnTo2",q.AttnTo2.Trim());
			command.Parameters.Add("@AttnTo3",q.AttnTo3.Trim());
			command.Parameters.Add("@BillTo1",q.BillTo1.Trim());
			command.Parameters.Add("@BillTo2",q.BillTo2.Trim());
			command.Parameters.Add("@BillTo3",q.BillTo3.Trim());
			command.Parameters.Add("@ShipTo1",q.ShipTo1.Trim());
			command.Parameters.Add("@ShipTo2",q.ShipTo2.Trim());
			command.Parameters.Add("@ShipTo3",q.ShipTo3.Trim());
			command.Parameters.Add("@QuoteNo",q.QuoteNo.Trim());
			command.Parameters.Add("@Quotedate",q.Quotedate.Trim());
			command.Parameters.Add("@ExpiryDate",q.ExpiryDate.Trim());
			command.Parameters.Add("@PrepairedBy",q.PrepairedBy.Trim());
			command.Parameters.Add("@Code",q.Code.Trim());
			command.Parameters.Add("@Langu",q.Langu.Trim());
			command.Parameters.Add("@Terms",q.Terms.Trim());
			command.Parameters.Add("@Delivery",q.Delivery.Trim());
			command.Parameters.Add("@Currency",q.Currency.Trim());
			command.Parameters.Add("@Items",q.Items.Trim());
			command.Parameters.Add("@Note",q.Note.Trim());
			command.Parameters.Add("@Finish",q.Finish.Trim());
			command.Parameters.Add("@FinishDate",q.FinishedDate);
			command.Parameters.Add("@CowanQno",q.CowanQno);
			command.Parameters.Add("@SearchDate",q.Quotedate.Trim());
			command.Parameters.Add("@CompanyId",q.CompanyID.Trim());
			command.Parameters.Add("@specsheet",q.SpecSheet.Trim());
			string s="";
			try
			{
				command.Connection = GetConnection();
				s=command.ExecuteNonQuery().ToString();
				Close();
			}
			catch (Exception ex)
			{		
				s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public string  Insertcomments(Opts opt)
		{  
			string query = "INSERT INTO WEB_Comment_TableV1(QuoteNo, Comments)Values ('"+opt.Code+"', '"+opt.Opt+"')";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string s="";
			try
			{
				command.Connection = GetConnection();
			
				s=command.ExecuteNonQuery().ToString();
				Close();
			}
			catch (Exception ex)
			{		
				s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}

		public decimal SelectStrokePriceML(string tab,string bore,string rod)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select MLStroke_PerINch  from  "+tab.ToString().Trim()+"  Where MLBore_Size= '"+bore.ToString().Trim()+"' and MLRod_Size= '"+rod.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			decimal s =0.00m;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				if(reader.Read())
				{
					s=(reader.GetSqlMoney(0).ToDecimal());
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{	
				string str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public decimal SelectMountPriceML(string mgrp,string tab,string bore,string rod)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select "+mgrp.ToString().Trim() +"  from  "+tab.ToString().Trim() +"  Where MLBore_Size= '"+bore.ToString().Trim()+"' and MLRod_Size= '"+rod.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;	
			decimal s =0.00m;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
		
				if(reader.Read())
				{
					s=(reader.GetSqlMoney(0).ToDecimal());
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				string str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public  string SelectPriceIndex(string code)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select PriceIndex from WEB_PriceIndex_TableV1 where Series='"+code.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string s ="";
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					s=reader.GetValue(0).ToString();
				
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		//issue #644 start
		public  string SelectPriceIndex_SMC(string code)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select PriceIndex from PriceIndex_Table where Series='"+code.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string s ="";
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					s=reader.GetValue(0).ToString();
				
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		//issue #644 end
		public  string SelectDiscount(string email,string seris)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select "+seris.Trim()+" from WEB_Customers_TableV1 where Email=@email " ; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@email",email.Trim());
			string s1="0";
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				if(reader.Read())
				{ 
					if(reader.GetValue(0).ToString()!="")
					{
						s1= reader.GetValue(0).ToString();
					}
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				s1=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s1;
		}
		public string SelectMountGrp(string mcode,string grpname)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select "+grpname.ToString().Trim() +"  from WEB_MountGrp_TableV1 Where Mount_Code= '"+mcode.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string s ="";
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				if(reader.Read())
				{
					s=(reader.GetValue(0).ToString());
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public decimal SelectCushionPriceML(string tab,string bore,string rod)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select MLCushion_PerINch  from  "+tab.ToString().Trim()+"  Where MLBore_Size= '"+bore.ToString().Trim()+"' and MLRod_Size= '"+rod.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			decimal s =0.00m;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				if(reader.Read())
				{
					s=(reader.GetSqlMoney(0).ToDecimal());
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				string str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public decimal SelectStrokeSMCPricePA(string tab,string bore,string rod)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select PAStroke_Per_Inch  from  "+tab.ToString().Trim()+"  Where Bore_Size= '"+bore.ToString().Trim()+"' and Rod_Size= '"+rod.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			decimal s =0.00m;
			try
			{
			command.Connection = GetConnection();
			SqlDataReader reader = command.ExecuteReader();
			
			if(reader.Read())
			{
				s=(reader.GetSqlMoney(0).ToDecimal());
		
			}
			reader.Close();
			Close();
			}
			catch (Exception ex)
			{		
				string str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public decimal SelectStrokeSMCPricePS(string tab,string bore,string rod)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select PSPrice_Stroke_PerInch  from  "+tab.ToString().Trim()+"  Where Bore_Size= '"+bore.ToString().Trim()+"' and Rod_Size= '"+rod.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			decimal s =0.00m;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				if(reader.Read())
				{
					s=(reader.GetSqlMoney(0).ToDecimal());
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				string str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public decimal SelectStrokeLprice(string tab,string bore,string rod)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select StrokePerInch  from  "+tab.ToString().Trim()+"  Where Bore_Size= '"+bore.ToString().Trim()+"' and Rod_Size= '"+rod.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			decimal s =0.00m;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				if(reader.Read())
				{
					s=(reader.GetSqlMoney(0).ToDecimal());
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				string str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			} 
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public decimal SelectStrokeA(string tab,string bore,string rod)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select AP_StrokePerInch  from  "+tab.ToString().Trim()+"  Where Bore_Size= '"+bore.ToString().Trim()+"' and Rod_Size= '"+rod.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			decimal s =0.00m;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				if(reader.Read())
				{
					s=(reader.GetSqlMoney(0).ToDecimal());
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				string str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public decimal SelectMountPrice(string mgrp,string tab,string bore,string rod)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select "+mgrp.ToString().Trim() +"  from  "+tab.ToString().Trim() +"  Where Bore_Size= '"+bore.ToString().Trim()+"' and Rod_Size= '"+rod.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			decimal s =0.00m;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				if(reader.Read())
				{
					s=(reader.GetSqlMoney(0).ToDecimal());
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				string str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			
			return s;
		}
		public decimal SelectCushionPrice(string col,string tab,string bore,string rod)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select  "+col.ToString().Trim()+"   from   "+tab.ToString().Trim()+"  Where Bore_Size= '"+bore.ToString().Trim()+"' and Rod_Size= '"+rod.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;	
			decimal s =0.00m;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
		
				if(reader.Read())
				{
					s=(reader.GetSqlMoney(0).ToDecimal());
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				string str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public decimal SelectStrokePriceL(string tab,string bore,string rod)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select StrokePerINch  from  "+tab.ToString().Trim()+"  Where Bore_Size= '"+bore.ToString().Trim()+"' and Rod_Size= '"+rod.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			decimal s =0.00m;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				if(reader.Read())
				{
					s=(reader.GetSqlMoney(0).ToDecimal());
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				string str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public string PassHashValue(string pass)
		{
			string encryptpass=FormsAuthentication.HashPasswordForStoringInConfigFile(pass,"SHA1");
			return encryptpass;
		
		}
		
		public ArrayList  SelectUser(string user,string pass)
		{  
			string encpass=FormsAuthentication.HashPasswordForStoringInConfigFile(pass,"SHA1");
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select * from WEB_Users_TableV1 where User_Ids =@UsrID and User_Password =@Passwd";
			SqlCommand command = new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@UsrID",user.Trim());
			command.Parameters.Add("@Passwd",encpass);
			ArrayList list =new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				if(reader.Read())
				{
					list.Add(reader.GetValue(1).ToString());//username
					list.Add(reader.GetValue(2).ToString());//email
					list.Add(reader.GetValue(3).ToString());//pass
					list.Add(reader.GetValue(4).ToString());//code
					list.Add(reader.GetValue(5).ToString());//date
					list.Add(reader.GetValue(6).ToString());//customerid
					list.Add(reader.GetValue(7).ToString());//companyid
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:" + ex.Message.ToString());
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList  SelectUser_ValidEmail(string user)
		{  
			string query = "select * from WEB_Users_TableV1 where User_Ids =@UsrID";
			SqlCommand command = new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@UsrID",user.Trim());
			ArrayList list =new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				if(reader.Read())
				{
					list.Add(reader.GetValue(1).ToString());
					list.Add(reader.GetValue(2).ToString());
					list.Add(reader.GetValue(3).ToString());
					list.Add(reader.GetValue(4).ToString());
					list.Add(reader.GetValue(5).ToString());
					list.Add(reader.GetValue(6).ToString());
					list.Add(reader.GetValue(7).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:" + ex.Message.ToString());
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  Quotation SelectCustomerDetails(string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select  * from WEB_Pricing_Customer_TableV1 where QuoteNo='"+qno.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			Quotation  list = new Quotation(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{ 
					list.Office=(reader.GetValue(1).ToString());
					list.Customer=(reader.GetValue(2).ToString());
					list.Contact=(reader.GetValue(3).ToString());
					list.AttnTo1=(reader.GetValue(4).ToString());
					list.AttnTo2=(reader.GetValue(5).ToString());
					list.AttnTo3=(reader.GetValue(6).ToString());
					list.BillTo1=(reader.GetValue(7).ToString());
					list.BillTo2=(reader.GetValue(8).ToString());
					list.BillTo3=(reader.GetValue(9).ToString());
					list.ShipTo1=(reader.GetValue(10).ToString());
					list.ShipTo2=(reader.GetValue(11).ToString());
					list.ShipTo3=(reader.GetValue(12).ToString());
					list.QuoteNo=(reader.GetValue(13).ToString());
					list.Quotedate=(reader.GetValue(14).ToString());
					list.ExpiryDate=(reader.GetValue(15).ToString());
					list.PrepairedBy=(reader.GetValue(16).ToString());
					list.Code=(reader.GetValue(17).ToString());
					list.Langu=(reader.GetValue(18).ToString());
					list.Terms=(reader.GetValue(19).ToString());
					list.Delivery=(reader.GetValue(20).ToString());
					list.Currency=(reader.GetValue(21).ToString());
					list.Note=(reader.GetValue(22).ToString());
					list.Items=(reader.GetValue(23).ToString());
	
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				string str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList Select_Z101_ByQno(string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select * from WEB_Z101_TableV1 Where QuoteNo='"+qno.Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list=new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
				while(reader.Read())
				{
					Others oth=new Others();
					oth.Code=reader.GetValue(1).ToString();
					oth.Desc=reader.GetValue(3).ToString();
					oth.UnitP=reader.GetValue(4).ToString();
					oth.Qty=reader.GetValue(5).ToString();
					oth.Price=reader.GetValue(6).ToString();
					list.Add(oth);
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				string sa=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList FindItemsTOQuote(string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select PartNo from WEB_Quotation_Items_TableV1 where Quotation_No='"+qno.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList temp=new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					temp.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				temp.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return temp;
		}
		public  ArrayList SelectAccessory(string code)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select  PartNo,Description,ListPrice,Quantity,TotalPrice from WEB_Accessory_Item_TableV1 where AccessoryID='"+code.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					Others other=new Others();
					other.Code =reader.GetValue(0).ToString();
					other.Desc =reader.GetValue(1).ToString();
					other.UnitP  =reader.GetValue(2).ToString();
					other.Qty =reader.GetValue(3).ToString();
					other.Price =reader.GetValue(4).ToString();
					list.Add(other);
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList SelectRepairkit(string code)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select  PartNo,RepairKit,Description1,Description2,Description3,ListPrice,Quantity,TotalPrice from WEB_RepairKit_Item_TableV1 where RepairKitID='"+code.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					Others other=new Others();
					other.Code =reader.GetValue(0).ToString();
					other.Desc =reader.GetValue(1).ToString();
					other.Desc1 =reader.GetValue(2).ToString();
					other.Desc2 =reader.GetValue(3).ToString();
					other.Desc3 =reader.GetValue(4).ToString();
				
					other.UnitP  =reader.GetValue(5).ToString();
					other.Qty =reader.GetValue(6).ToString();
					other.Price =reader.GetValue(7).ToString();
					list.Add(other);
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		//metricqitem
		public  QItems SelectAllItems(string code,string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			//issue #242 start
			//back
			//string query="select  PartNo,Series,B_Code, Bore, R_Code, Rod, StrokeCode, Stroke, M_code, Mount, RE_Code,RodEnd,Cu_Code,Cushion,CushionPos_Code,CushionPos,ApplicationOPtID,Sel_Code,Seal,Port_Code,Port,PP_Code,PortPos,PopularOptID,Special_ID,Quantity,UnitPrice,TotalPrice,Cusomer_ID,Quotation_No,Q_Date,User_ID from WEB_Quotation_Items_TableV1 where PartNo='"+code.ToString().Trim()+"'  and  Quotation_No='"+qno.ToString().Trim()+"' order by SL_No"; 
			//update
			string query="select  PartNo,Series,B_Code, Bore, R_Code, Rod, StrokeCode, Stroke, M_code, Mount, RE_Code,RodEnd,Cu_Code,Cushion,CushionPos_Code,CushionPos,ApplicationOPtID,Sel_Code,Seal,Port_Code,Port,PP_Code,PortPos,PopularOptID,Special_ID,Quantity,UnitPrice,TotalPrice,Cusomer_ID,Quotation_No,Q_Date,User_ID,Units from WEB_Quotation_Items_TableV1 where PartNo='"+code.ToString().Trim()+"'  and  Quotation_No='"+qno.ToString().Trim()+"' order by SL_No"; 
			//issue #242 end
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			QItems Qitem=new QItems();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				if(reader.Read())
				{
				
					Qitem.PartNo=reader.GetValue(0).ToString();
					Qitem.Series=reader.GetValue(1).ToString();
					Qitem.Bore="("+reader.GetValue(2).ToString()+") "+reader.GetValue(3).ToString();
					Qitem.Rod="("+reader.GetValue(4).ToString()+") "+reader.GetValue(5).ToString();
					Qitem.Stroke="("+reader.GetValue(6).ToString()+") "+reader.GetValue(7).ToString();
					Qitem.Mount="("+reader.GetValue(8).ToString()+") "+reader.GetValue(9).ToString();
					Qitem.RodEnd="("+reader.GetValue(10).ToString()+") "+reader.GetValue(11).ToString();
					Qitem.Cushion="("+reader.GetValue(12).ToString()+") "+reader.GetValue(13).ToString();
					Qitem.CushionPos="("+reader.GetValue(14).ToString()+") "+reader.GetValue(15).ToString();
					Qitem.ApplicationOpt_Id =reader.GetValue(16).ToString();
					Qitem.Seal="("+reader.GetValue(17).ToString()+") "+reader.GetValue(18).ToString();
					Qitem.Port="("+reader.GetValue(19).ToString()+") "+reader.GetValue(20).ToString();
					Qitem.PortPos="("+reader.GetValue(21).ToString()+") "+reader.GetValue(22).ToString();
					Qitem.PopularOpt_Id=reader.GetValue(23).ToString();
					Qitem.Special_ID=reader.GetValue(24).ToString();
					Qitem.Quantity=reader.GetValue(25).ToString();
					Qitem.UnitPrice=reader.GetValue(26).ToString();
					Qitem.TotalPrice=reader.GetValue(27).ToString();
					Qitem.Cusomer_ID=reader.GetValue(28).ToString();
					Qitem.Quotation_No=reader.GetValue(29).ToString();
					Qitem.Q_Date=reader.GetValue(30).ToString();
					Qitem.User_ID=reader.GetValue(31).ToString();
				    //issue #242 start
					Qitem.Units=reader.GetValue(32).ToString();
					//issue #242 end
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				string s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return Qitem;
		}
		public string  InsertAccessories(string accessid,string acces,string partno,string desc,string unitp,string qty,string dic,string total )
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "INSERT INTO WEB_Accessory_Item_TableV1(AccessoryID,Accesory,PartNo,Description,ListPrice,Quantity,Discount,TotalPrice)Values ('"+accessid.Trim()+"','"+acces.Trim()+"','"+partno.Trim()+"','"+desc.Trim()+"','"+unitp.Trim()+"','"+qty.Trim()+"','"+dic.Trim()+"', '"+total.Trim()+"')";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string s="";
			try
			{
				command.Connection = GetConnection();
		
				s=command.ExecuteNonQuery().ToString();
				Close();
			}
			catch (Exception ex)
			{		
				s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}

		//copied from db manager

		public ArrayList SelectApplicationopts(string qno,string pno)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select AppoptCode,AppOpts from WEB_Application_Opts_Items_TableV1 where QuoteNo ='"+qno.ToString().Trim()+"' and  PartNo ='"+pno.ToString().Trim()+"'  order by SLNO";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			 
				while(reader.Read())
				{
					list.Add("("+reader.GetValue(0)+") "+reader.GetValue(1));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList  SelectQtyBreaks(string pno,string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select * from WEB_Qty_Breaks_TableV1 where  PartNo= '"+pno.Trim()+"' and QuoteNo='"+qno.Trim()+"' order by slno"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList temp=new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					Opts opt=new Opts();
					opt.Opt=reader.GetValue(0).ToString();
					opt.Code =reader.GetValue(3).ToString();
					opt.Free =reader.GetValue(4).ToString();
					temp.Add(opt);
				
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				string str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return temp;
		}
		public ArrayList SelectPopularoptions(string qno,string pno)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select PopoptsCode,PopOpts from WEB_Popular_Opts_Items_TableV1 where QuoteNo ='"+qno.ToString().Trim()+"' and  PartNo ='"+pno.ToString().Trim()+"' order by SLNO";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add("("+reader.GetValue(0)+") "+reader.GetValue(1));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList SelectSpecial(string pno,string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select  Special_Description from WEB_Specials_TableV1 where PartNo='"+pno.ToString().Trim()+"' and QuoteNo='"+qno.ToString().Trim()+"' order by SpNo"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList  Selectcomments(string qno)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "Select Comments from WEB_Comment_TableV1 where QuoteNo='"+qno.Trim()+"'";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList lst =new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					lst.Add(reader.GetValue(0).ToString());
				
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				lst.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return lst;
		}
		public  string SelectLastQno()
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select QuoteNo from WEB_QuoteCount_TableV1 Where SLNo=1 "; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string s ="";
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					s=reader.GetValue(0).ToString();
				
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		// manage quote
		public  ArrayList GetContacts1(string customerid,string usr,string finish,string dt1,string dt2)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "set DateFormat mdy; select distinct p.contact from WEB_Pricing_Customer_TableV1 as p  where p.CompanyID='"+customerid.Trim()+"' and  p.PrepairedBy  like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and  p.searchDate between '"+dt1.Trim()+"' and  '"+dt2.Trim()+"'";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList GetContacts2(string customerid,string usr,string finish,string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "select distinct p.contact from WEB_Pricing_Customer_TableV1 as p join WEB_Quotation_Items_TableV1 as q on p.quoteno = q.quotation_No  where p.CompanyID='"+customerid.Trim()+"' and  p.PrepairedBy  like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and q.quotation_no like '%"+qno.Trim()+"%' ";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList GetContacts3(string customerid,string usr,string finish,string pno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "select distinct p.contact from WEB_Pricing_Customer_TableV1 as p join WEB_Quotation_Items_TableV1 as q on p.quoteno = q.quotation_No  where p.CompanyID='"+customerid.Trim()+"' and  p.PrepairedBy  like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and q.PartNo like '%"+pno.Trim()+"%' ";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList GetContacts(string customerid,string usr,string finish)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "select distinct p.contact from WEB_Pricing_Customer_TableV1 as p  where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy  like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' ";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList Getquotedate1(string customerid,string usr,string finish,string contact,string dt1,string dt2)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "set DateFormat mdy; select quotedate from WEB_Pricing_Customer_TableV1 where  CompanyID='"+customerid.Trim()+"' and PrepairedBy like '%"+usr.Trim()+"%' and  Finished='"+finish.Trim()+"' and contact like '%"+contact.Trim()+"%' and searchDate between '"+dt1.Trim()+"' and  '"+dt2.Trim()+"' group by quotedate order by max(SL_NO) DESC";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			 
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList Getquotedate2(string customerid,string usr,string finish,string contact,string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "select p.quotedate from WEB_Pricing_Customer_TableV1 as p join WEB_Quotation_Items_TableV1 as q on p.quoteno = q.quotation_No  where  p.CompanyID='"+customerid.Trim()+"' and  p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and p.contact like '%"+contact.Trim()+"%' and q.quotation_no like '%"+qno.Trim()+"%'  group by quotedate order by max(p.SL_NO) DESC";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			 
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList Getquotedate3(string customerid,string usr,string finish,string contact,string pno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "select p.quotedate from WEB_Pricing_Customer_TableV1 as p join WEB_Quotation_Items_TableV1 as q on p.quoteno = q.quotation_No  where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and p.contact like '%"+contact.Trim()+"%' and q.PartNo like '%"+pno.Trim()+"%'  group by p.quotedate order by max(p.SL_NO) DESC";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			 
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList Getquotedate(string customerid,string usr,string finish,string contact)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "select quotedate from WEB_Pricing_Customer_TableV1 where  CompanyID='"+customerid.Trim()+"' and PrepairedBy like '%"+usr.Trim()+"%' and  Finished='"+finish.Trim()+"' and contact like '%"+contact.Trim()+"%' group by quotedate order by max(SL_NO) DESC";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			 
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList GetQno1(string customerid,string usr,string finish,string contact,string dt1,string dt2)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "set DateFormat mdy; select  p.quoteno from WEB_Pricing_Customer_TableV1 as p join WEB_Quotation_Items_TableV1 as q on p.quoteno = q.quotation_No   where  p.CompanyID='"+customerid.Trim()+"' and  p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"'  and p.contact like '%"+contact.Trim()+"%' and  p.searchDate between '"+dt1.Trim()+"' and  '"+dt2.Trim()+"'  group by p.QuoteNo order by max(p.SL_NO) DESC";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList GetQno2(string customerid,string usr,string finish,string contact,string qdate,string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "select  p.quoteno from WEB_Pricing_Customer_TableV1 as p join WEB_Quotation_Items_TableV1 as q on p.quoteno = q.quotation_No   where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"'  and p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"+qdate.Trim()+"%' and q.quotation_no like '%"+qno.Trim()+"%' group by p.QuoteNo order by max(p.SL_NO) DESC";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList GetQno3(string customerid,string usr,string finish,string contact,string qdate,string pno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "select  p.quoteno from WEB_Pricing_Customer_TableV1 as p join WEB_Quotation_Items_TableV1 as q on p.quoteno = q.quotation_No   where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"'  and p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"+qdate.Trim()+"%' and q.PartNo like '%"+pno.Trim()+"%' group by p.QuoteNo order by max(p.SL_NO) DESC";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList GetQno(string customerid,string usr,string finish,string contact,string qdate)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "select  quoteno from WEB_Pricing_Customer_TableV1  where  CompanyID='"+customerid.Trim()+"' and PrepairedBy like '%"+usr.Trim()+"%' and  Finished='"+finish.Trim()+"'  and contact like '%"+contact.Trim()+"%' and quotedate like '%"+qdate.Trim()+"%' group by QuoteNo order by max(SL_NO) DESC";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList GetZno_1(string customerid,string usr,string finish,string contact,string dt1,string dt2,string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "set DateFormat mdy; select distinct z.Zno from WEB_Pricing_Customer_TableV1 as p join WEB_Z101_TableV1 as z on p.quoteno = z.QuoteNo  where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and  p.contact like '%"+contact.Trim()+"%'  and p.quoteno like '%"+qno.Trim()+"%' and p.searchDate between '"+dt1.Trim()+"' and  '"+dt2.Trim()+"'  ";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList GetZno_2(string customerid,string usr,string finish,string contact,string qdate,string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "select distinct z.Zno from WEB_Pricing_Customer_TableV1 as p join WEB_Z101_TableV1 as z on p.quoteno = z.QuoteNo where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and  p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"+qdate.Trim()+"%' and p.quoteno like '%"+qno.Trim()+"%' ";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList GetZno_3(string customerid,string usr,string finish,string contact,string qdate,string qno,string pno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "select distinct z.Zno from WEB_Pricing_Customer_TableV1 as p join WEB_Z101_TableV1 as z on p.quoteno = z.QuoteNo where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and  p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"+qdate.Trim()+"%' and p.quoteno like '%"+qno.Trim()+"%'and z.Zno like '%"+pno.Trim()+"%' ";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList GetPno1(string customerid,string usr,string finish,string contact,string dt1,string dt2,string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "set DateFormat mdy; select distinct q.PartNo from WEB_Pricing_Customer_TableV1 as p join WEB_Quotation_Items_TableV1 as q on p.quoteno = q.quotation_No where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and  p.contact like '%"+contact.Trim()+"%'  and p.quoteno like '%"+qno.Trim()+"%' and p.searchDate between '"+dt1.Trim()+"' and  '"+dt2.Trim()+"'  ";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList GetPno2(string customerid,string usr,string finish,string contact,string qdate,string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "select distinct q.PartNo from WEB_Pricing_Customer_TableV1 as p join WEB_Quotation_Items_TableV1 as q on p.quoteno = q.quotation_No where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and  p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"+qdate.Trim()+"%' and p.quoteno like '%"+qno.Trim()+"%' ";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList GetPno3(string customerid,string usr,string finish,string contact,string qdate,string qno,string pno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "select distinct q.PartNo from WEB_Pricing_Customer_TableV1 as p join WEB_Quotation_Items_TableV1 as q on p.quoteno = q.quotation_No where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and  p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"+qdate.Trim()+"%' and p.quoteno like '%"+qno.Trim()+"%'and q.PartNo like '%"+pno.Trim()+"%' ";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList GetPno(string customerid,string usr,string finish,string contact,string qdate,string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "select distinct q.PartNo from WEB_Pricing_Customer_TableV1 as p join WEB_Quotation_Items_TableV1 as q on p.quoteno = q.quotation_No where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and  p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"+qdate.Trim()+"%' and p.quoteno like '%"+qno.Trim()+"%' ";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList GetAno(string customerid,string usr,string finish,string contact,string qdate,string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "select distinct q.PartNo from WEB_Pricing_Customer_TableV1 as p join WEB_Accessory_Item_TableV1 as q on p.quoteno = q.AccessoryID where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and  p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"+qdate.Trim()+"%' and p.quoteno like '%"+qno.Trim()+"%'";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList GetAno1(string customerid,string usr,string finish,string contact,string dt1,string dt2,string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "set DateFormat mdy; select distinct q.PartNo from WEB_Pricing_Customer_TableV1 as p join WEB_Accessory_Item_TableV1 as q on p.quoteno = q.AccessoryID where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and  p.contact like '%"+contact.Trim()+"%'  and p.quoteno like '%"+qno.Trim()+"%' and p.searchDate between '"+dt1.Trim()+"' and  '"+dt2.Trim()+"'";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList GetAno2(string customerid,string usr,string finish,string contact,string qdate,string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "select distinct q.PartNo from WEB_Pricing_Customer_TableV1 as p join WEB_Accessory_Item_TableV1 as q on p.quoteno = q.AccessoryID where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and  p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"+qdate.Trim()+"%' and p.quoteno like '%"+qno.Trim()+"%'";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList GetAno3(string customerid,string usr,string finish,string contact,string qdate,string qno,string pno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "select distinct q.PartNo from WEB_Pricing_Customer_TableV1 as p join WEB_Accessory_Item_TableV1 as q on p.quoteno = q.AccessoryID where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and  p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"+qdate.Trim()+"%' and p.quoteno like '%"+qno.Trim()+"%'and q.PartNo like '%"+pno.Trim()+"%' ";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList GetRno(string customerid,string usr,string finish,string contact,string qdate,string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "select distinct q.PartNo from WEB_Pricing_Customer_TableV1 as p join WEB_RepairKit_Item_TableV1 as q on p.quoteno = q.RepairKitID where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and  p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"+qdate.Trim()+"%' and p.quoteno like '%"+qno.Trim()+"%'";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList GetRno1(string customerid,string usr,string finish,string contact,string dt1,string dt2,string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "set DateFormat mdy; select distinct q.PartNo from WEB_Pricing_Customer_TableV1 as p join WEB_RepairKit_Item_TableV1 as q on p.quoteno = q.RepairKitID where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and  p.contact like '%"+contact.Trim()+"%'  and p.quoteno like '%"+qno.Trim()+"%' and p.searchDate between '"+dt1.Trim()+"' and  '"+dt2.Trim()+"'";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList GetRno2(string customerid,string usr,string finish,string contact,string qdate,string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "select distinct q.PartNo from WEB_Pricing_Customer_TableV1 as p join WEB_RepairKit_Item_TableV1 as q on p.quoteno = q.RepairKitID where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and  p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"+qdate.Trim()+"%' and p.quoteno like '%"+qno.Trim()+"%'";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList GetRno3(string customerid,string usr,string finish,string contact,string qdate,string qno,string pno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "select distinct q.PartNo from WEB_Pricing_Customer_TableV1 as p join WEB_RepairKit_Item_TableV1 as q on p.quoteno = q.RepairKitID where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and  p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"+qdate.Trim()+"%' and p.quoteno like '%"+qno.Trim()+"%'and q.PartNo like '%"+pno.Trim()+"%'";
			SqlCommand command =new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public DataSet GetRifinedQuotes(string customerid,string usr,string finish,string contact,string qdate,string qno,string pno)
		{
			string dml ="";
			//#588
			if(customerid.Trim() =="1012"  || customerid.Trim() =="1027"  || customerid.Trim() =="1028" || customerid.Trim() =="1029" || customerid.Trim() =="1030")
			{
				dml="select p.contact,p.quotedate,p.quoteno,q.partno,q.totalprice,p.Note,p.UploadFile,q.Dwg from WEB_Pricing_Customer_TableV1  as p "
					+" join WEB_Quotation_Items_TableV1 as q on p.quoteno = q.quotation_No where  ( p.CompanyID='"+customerid.Trim()+"' or p.CompanyID='1013') and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()
					+"' and p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"+qdate.Trim()
					+"%' and p.quoteno like '%"+qno.Trim()+"%' and q.partno like '%"+pno.Trim()+"%' order by p.SL_NO DESC";
			}
			else
			{
				dml="select p.contact,p.quotedate,p.quoteno,q.partno,q.totalprice,p.Note,p.UploadFile,q.Dwg from WEB_Pricing_Customer_TableV1  as p "
					+" join WEB_Quotation_Items_TableV1 as q on p.quoteno = q.quotation_No where  p.CompanyID='"+customerid.Trim()
					+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()
					+"' and p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"+qdate.Trim()
					+"%' and p.quoteno like '%"+qno.Trim()+"%' and q.partno like '%"+pno.Trim()+"%' order by p.SL_NO DESC";
			}
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;
				oledad.Fill(ds);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public DataSet GetRifinedQuotes_Project(string customerid,string usr,string finish,string contact,string qdate,string qno,string pno,string project)
		{
			string dml ="";
			//#588
			if(customerid.Trim() =="1012" || customerid.Trim() =="1027" || customerid.Trim() =="1028" || customerid.Trim() =="1029" || customerid.Trim() =="1030")
			{
				dml = "select p.contact,p.quotedate,p.quoteno,q.partno,q.totalprice,p.Note,p.UploadFile,q.Dwg from WEB_Pricing_Customer_TableV1  as p join WEB_Quotation_Items_TableV1 as q on p.quoteno = q.quotation_No where  ( p.CompanyID='"
					+customerid.Trim()+"'  or p.CompanyID='1013') and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"
					+qdate.Trim()+"%' and p.quoteno like '%"+qno.Trim()+"%' and q.partno like '%"+pno.Trim()+"%'  and p.Note like '%"+project.Trim()+"%' order by p.SL_NO DESC";
			}
			else
			{
				dml = "select p.contact,p.quotedate,p.quoteno,q.partno,q.totalprice,p.Note,p.UploadFile,q.Dwg from WEB_Pricing_Customer_TableV1  as p join WEB_Quotation_Items_TableV1 as q on p.quoteno = q.quotation_No where  p.CompanyID='"
					+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"
					+qdate.Trim()+"%' and p.quoteno like '%"+qno.Trim()+"%' and q.partno like '%"+pno.Trim()+"%'  and p.Note like '%"+project.Trim()+"%' order by p.SL_NO DESC";
			}
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;
				oledad.Fill(ds);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public DataSet GetRifinedQuotes1(string customerid,string usr,string finish,string contact,string qdate,string qno,string pno)
		{
			string dml ="";
			//#588
			if(customerid.Trim() =="1012"  || customerid.Trim() =="1027"  || customerid.Trim() =="1028" || customerid.Trim() =="1029" || customerid.Trim() =="1030")
			{
				dml = "select p.contact,p.quotedate,p.quoteno,q.partno,p.Note,q.Finish,p.FinishedDate,p.CowanQno,p.UploadFile,q.Dwg from WEB_Pricing_Customer_TableV1  as p join WEB_Quotation_Items_TableV1 as q on p.quoteno = q.quotation_No where"
					+" ( p.CompanyID='"+customerid.Trim()+"'  or p.CompanyID='1013') and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"+qdate.Trim()+"%' and p.quoteno like '%"+qno.Trim()+"%' and q.partno like '%"+pno.Trim()+"%' order by p.SL_NO DESC";
			}
			else
			{
				dml = "select p.contact,p.quotedate,p.quoteno,q.partno,p.Note,q.Finish,p.FinishedDate,p.CowanQno,p.UploadFile,q.Dwg from WEB_Pricing_Customer_TableV1  as p join WEB_Quotation_Items_TableV1 as q on p.quoteno = q.quotation_No where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"+qdate.Trim()+"%' and p.quoteno like '%"+qno.Trim()+"%' and q.partno like '%"+pno.Trim()+"%' order by p.SL_NO DESC";
			}
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;
				oledad.Fill(ds);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public DataSet GetRifinedQuotes_ProjectNo(string customerid,string usr,string finish,string contact,string qdate,string qno,string pno,string projectno)
		{
			string dml ="";
			//#588
			if(customerid.Trim() =="1012"  || customerid.Trim() =="1027" || customerid.Trim() =="1028" || customerid.Trim() =="1029" || customerid.Trim() =="1030")
			{
				dml = "select p.contact,p.quotedate,p.quoteno,q.partno,p.Note,q.Finish,p.FinishedDate,p.CowanQno,p.UploadFile,q.Dwg from WEB_Pricing_Customer_TableV1  as p join WEB_Quotation_Items_TableV1 as q on p.quoteno = q.quotation_No "
					+" where  ( p.CompanyID='"+customerid.Trim()+"'  or p.CompanyID='1013') and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"
					+qdate.Trim()+"%' and p.quoteno like '%"+qno.Trim()+"%' and q.partno like '%"+pno.Trim()+"%' and p.Note like '%"+projectno.Trim()+"%' order by p.SL_NO DESC";
			}
			else
			{
				dml = "select p.contact,p.quotedate,p.quoteno,q.partno,p.Note,q.Finish,p.FinishedDate,p.CowanQno,p.UploadFile,q.Dwg from WEB_Pricing_Customer_TableV1  as p join WEB_Quotation_Items_TableV1 as q on p.quoteno = q.quotation_No where  p.CompanyID='"
					+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"
					+qdate.Trim()+"%' and p.quoteno like '%"+qno.Trim()+"%' and q.partno like '%"+pno.Trim()+"%' and p.Note like '%"+projectno.Trim()+"%' order by p.SL_NO DESC";
			}
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;
				oledad.Fill(ds);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public DataSet GetRifinedZ101(string customerid,string usr,string finish,string contact,string qdate,string qno,string pno)
		{
			string dml = "select p.contact,p.quotedate,p.quoteno,z.Zno,z.TotalPrice,p.Note from WEB_Pricing_Customer_TableV1  as p join WEB_Z101_TableV1 as z on p.quoteno = z.QuoteNo where p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"'  and p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"+qdate.Trim()+"%' and p.quoteno like '%"+qno.Trim()+"%' and z.Zno like '%"+pno.Trim()+"%' order by p.SL_NO DESC";
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;
				oledad.Fill(ds);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public DataSet GetRifinedZ1011(string customerid,string usr,string finish,string contact,string qdate,string qno,string pno)
		{
			//			SqlConnection SqlCon =new SqlConnection(constr1);
			string dml = "select p.contact,p.quotedate,p.quoteno,z.Zno,p.Note,p.FinishedDate,p.CowanQno from WEB_Pricing_Customer_TableV1  as p join WEB_Z101_TableV1 as z on p.quoteno = z.QuoteNo where p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"'  and p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"+qdate.Trim()+"%' and p.quoteno like '%"+qno.Trim()+"%' and z.Zno like '%"+pno.Trim()+"%' order by p.SL_NO DESC";
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;
			
				oledad.Fill(ds);
				Close();
			}
			catch (Exception ex)
			{		
				string s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public DataSet GetRifinedZ1013(string customerid, string usr,string finish,string contact,string qdate,string qdate2,string qno,string pno)
		{
			//			SqlConnection SqlCon =new SqlConnection(constr1);
			string dml = "set DateFormat mdy; select p.contact,p.quotedate,p.quoteno,z.Zno,z.TotalPrice,p.Note from WEB_Pricing_Customer_TableV1  as p join WEB_Z101_TableV1 as z on p.quoteno = z.QuoteNo where p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"'  and p.contact like '%"+contact.Trim()+"%' and p.quoteno like '%"+qno.Trim()+"%' and z.Zno like '%"+pno.Trim()+"%' and p.searchDate between '"+qdate.Trim()+"' and  '"+qdate2.Trim()+"' order by p.SL_NO DESC";
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;
			
				oledad.Fill(ds);
				Close();
			}
			catch (Exception ex)
			{		
				string s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public DataSet GetRifinedZ1014(string customerid, string usr,string finish,string contact,string qdate,string qdate2,string qno,string pno)
		{
			//			SqlConnection SqlCon =new SqlConnection(constr1);
			string dml = "set DateFormat mdy; select  p.contact,p.quotedate,p.quoteno,z.Zno,p.Note,p.FinishedDate,p.CowanQno from WEB_Pricing_Customer_TableV1  as p join WEB_Z101_TableV1 as z on p.quoteno = z.QuoteNo where p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"'  and p.contact like '%"+contact.Trim()+"%' and p.quoteno like '%"+qno.Trim()+"%' and z.Zno like '%"+pno.Trim()+"%' and p.searchDate between '"+qdate.Trim()+"' and  '"+qdate2.Trim()+"' order by p.SL_NO DESC";
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;
			
				oledad.Fill(ds);
				Close();
			}
			catch (Exception ex)
			{		
				string s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public DataSet GetRifinedQuotes3(string customerid, string usr,string finish,string contact,string qdate,string qdate2,string qno,string pno)
		{
			string dml ="";
			//#588
			if(customerid.Trim() =="1012" || customerid.Trim() =="1027" || customerid.Trim() =="1028" || customerid.Trim() =="1029" || customerid.Trim() =="1030")
			{
				dml = "set DateFormat mdy; select p.contact,p.quotedate,p.quoteno,q.partno,q.totalprice,p.Note,p.UploadFile,q.Dwg from WEB_Pricing_Customer_TableV1  as p join WEB_Quotation_Items_TableV1 as q on p.quoteno = q.quotation_No "
					+" where ( p.CompanyID='"+customerid.Trim()+"' or p.CompanyID='1013') and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and p.contact like '%"+contact.Trim()+"%'  and p.quoteno like '%"+qno.Trim()+"%' and q.partno like '%"+pno.Trim()+"%' and p.searchDate between '"+qdate.Trim()+"' and  '"+qdate2.Trim()+"' order by p.SL_NO DESC";
			}
			else
			{
				dml = "set DateFormat mdy; select p.contact,p.quotedate,p.quoteno,q.partno,q.totalprice,p.Note,p.UploadFile,q.Dwg from WEB_Pricing_Customer_TableV1  as p join WEB_Quotation_Items_TableV1 as q on p.quoteno = q.quotation_No where p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and p.contact like '%"+contact.Trim()+"%'  and p.quoteno like '%"+qno.Trim()+"%' and q.partno like '%"+pno.Trim()+"%' and p.searchDate between '"+qdate.Trim()+"' and  '"+qdate2.Trim()+"' order by p.SL_NO DESC";
			}
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;
			
				oledad.Fill(ds);
				Close();
			}
			catch (Exception ex)
			{		
				string s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public DataSet GetRifinedQuotes4(string customerid,string usr,string finish,string contact,string qdate,string qdate2,string qno,string pno)
		{
			string dml ="";
			//#588
			if(customerid.Trim() =="1012"  || customerid.Trim() =="1027" || customerid.Trim() =="1028" || customerid.Trim() =="1029" || customerid.Trim() =="1030")
			{
				dml = "set DateFormat mdy; select  p.contact,p.quotedate,p.quoteno,q.partno,p.Note,q.Finish,p.FinishedDate,p.CowanQno,p.UploadFile,q.Dwg  from WEB_Pricing_Customer_TableV1  as p join WEB_Quotation_Items_TableV1 as q on p.quoteno = q.quotation_No "
					+" where ( p.CompanyID='"+customerid.Trim()+"' or p.CompanyID='1013')  and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and p.contact like '%"+contact.Trim()+"%'  and p.quoteno like '%"+qno.Trim()+"%' and q.partno like '%"+pno.Trim()+"%' and p.searchDate between '"+qdate.Trim()+"' and  '"+qdate2.Trim()+"' order by p.SL_NO DESC";
			}
			else
			{
				dml = "set DateFormat mdy; select  p.contact,p.quotedate,p.quoteno,q.partno,p.Note,q.Finish,p.FinishedDate,p.CowanQno,p.UploadFile,q.Dwg  from WEB_Pricing_Customer_TableV1  as p join WEB_Quotation_Items_TableV1 as q on p.quoteno = q.quotation_No where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and p.contact like '%"+contact.Trim()+"%'  and p.quoteno like '%"+qno.Trim()+"%' and q.partno like '%"+pno.Trim()+"%' and p.searchDate between '"+qdate.Trim()+"' and  '"+qdate2.Trim()+"' order by p.SL_NO DESC";
			}
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;
			
				oledad.Fill(ds);
				Close();
			}
			catch (Exception ex)
			{		
				string s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public DataSet GetRifinedAccesories(string customerid,string usr,string finish,string contact,string qdate,string qno,string pno)
		{
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "select p.contact,p.quotedate,p.quoteno,a.partno,a.totalprice,p.Note from WEB_Pricing_Customer_TableV1  as p join WEB_Accessory_Item_TableV1 as a on p.quoteno = a.AccessoryID where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"'  and p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"+qdate.Trim()+"%' and p.quoteno like '%"+qno.Trim()+"%' and a.PartNo like '%"+pno.Trim()+"%' order by p.SL_NO DESC";
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;
			
				oledad.Fill(ds);
				Close();
			}
			catch (Exception ex)
			{		
				string s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public DataSet GetRifinedAccesories1(string customerid,string usr,string finish,string contact,string qdate,string qno,string pno)
		{
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "select p.contact,p.quotedate,p.quoteno,a.partno,p.Note,p.FinishedDate,p.CowanQno from WEB_Pricing_Customer_TableV1  as p join WEB_Accessory_Item_TableV1 as a on p.quoteno = a.AccessoryID where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"+qdate.Trim()+"%' and p.quoteno like '%"+qno.Trim()+"%' and a.PartNo like '%"+pno.Trim()+"%' order by p.SL_NO DESC";
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;
			
				oledad.Fill(ds);
				Close();
			}
			catch (Exception ex)
			{		
				string s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public DataSet GetRifinedAccesories3(string customerid,string usr,string finish,string contact,string qdate,string qdate2,string qno,string pno)
		{
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "set DateFormat mdy; select p.contact,p.quotedate,p.quoteno,a.partno,a.totalprice,p.Note from WEB_Pricing_Customer_TableV1  as p join WEB_Accessory_Item_TableV1 as a on p.quoteno = a.AccessoryID where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and p.contact like '%"+contact.Trim()+"%'  and p.quoteno like '%"+qno.Trim()+"%' and a.partno like '%"+pno.Trim()+"%' and p.searchDate between '"+qdate.Trim()+"' and  '"+qdate2.Trim()+"' order by p.SL_NO DESC";
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;
			
				oledad.Fill(ds);
				Close();
			}
			catch (Exception ex)
			{		
				string s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public DataSet GetRifinedAccesories4(string customerid,string usr,string finish,string contact,string qdate,string qdate2,string qno,string pno)
		{
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "set DateFormat mdy; select  p.contact,p.quotedate,p.quoteno,a.partno,p.Note,p.FinishedDate,p.CowanQno from WEB_Pricing_Customer_TableV1  as p join WEB_Accessory_Item_TableV1 as a on p.quoteno = a.AccessoryID where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and p.contact like '%"+contact.Trim()+"%'  and p.quoteno like '%"+qno.Trim()+"%' and a.partno like '%"+pno.Trim()+"%' and p.searchDate between '"+qdate.Trim()+"' and  '"+qdate2.Trim()+"' order by p.SL_NO DESC";
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;
			
				oledad.Fill(ds);
				Close();
			}
			catch (Exception ex)
			{		
				string s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public DataSet GetRifinedRepairKit(string customerid,string usr,string finish,string contact,string qdate,string qno,string pno)
		{
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "select p.contact,p.quotedate,p.quoteno,a.partno,a.totalprice,p.Note from WEB_Pricing_Customer_TableV1  as p join WEB_RepairKit_Item_TableV1 as a on p.quoteno = a.RepairKitID where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"'  and p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"+qdate.Trim()+"%' and p.quoteno like '%"+qno.Trim()+"%' and a.PartNo like '%"+pno.Trim()+"%' order by p.SL_NO DESC";
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;
			
				oledad.Fill(ds);
				Close();
			}
			catch (Exception ex)
			{		
				string s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public DataSet GetRifinedRepairKit1(string customerid,string usr,string finish,string contact,string qdate,string qno,string pno)
		{
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "select p.contact,p.quotedate,p.quoteno,a.partno,p.Note,p.FinishedDate,p.CowanQno from WEB_Pricing_Customer_TableV1  as p join WEB_RepairKit_Item_TableV1 as a on p.quoteno = a.RepairKitID where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"'  and p.contact like '%"+contact.Trim()+"%' and p.quotedate like '%"+qdate.Trim()+"%' and p.quoteno like '%"+qno.Trim()+"%' and a.PartNo like '%"+pno.Trim()+"%' order by p.SL_NO DESC";
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;
			
				oledad.Fill(ds);
				Close();
			}
			catch (Exception ex)
			{		
				string s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public DataSet GetRifinedRepairKit3(string customerid,string usr,string finish,string contact,string qdate,string qdate2,string qno,string pno)
		{
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "set DateFormat mdy; select p.contact,p.quotedate,p.quoteno,a.partno,a.totalprice,p.Note from WEB_Pricing_Customer_TableV1  as p join WEB_RepairKit_Item_TableV1 as a on p.quoteno = a.RepairKitID where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and p.contact like '%"+contact.Trim()+"%'  and p.quoteno like '%"+qno.Trim()+"%' and a.partno like '%"+pno.Trim()+"%' and p.searchDate between '"+qdate.Trim()+"' and  '"+qdate2.Trim()+"' order by p.SL_NO DESC";
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;
			
				oledad.Fill(ds);
				Close();
			}
			catch (Exception ex)
			{		
				string s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public DataSet GetRifinedRepairKit4(string customerid,string usr,string finish,string contact,string qdate,string qdate2,string qno,string pno)
		{
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string dml = "set DateFormat mdy; select  p.contact,p.quotedate,p.quoteno,a.partno,p.Note,p.FinishedDate,p.CowanQno from WEB_Pricing_Customer_TableV1  as p join WEB_RepairKit_Item_TableV1 as a on p.quoteno = a.RepairKitID where  p.CompanyID='"+customerid.Trim()+"' and p.PrepairedBy like '%"+usr.Trim()+"%' and  p.Finished='"+finish.Trim()+"' and p.contact like '%"+contact.Trim()+"%'  and p.quoteno like '%"+qno.Trim()+"%' and a.partno like '%"+pno.Trim()+"%' and p.searchDate between '"+qdate.Trim()+"' and  '"+qdate2.Trim()+"' order by p.SL_NO DESC";
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;
			
				oledad.Fill(ds);
				Close();
			}
			catch (Exception ex)
			{		
				string s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}

		public  string SelectCustomerDelivery(string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select Delivery from WEB_Pricing_Customer_TableV1 where QuoteNo='"+qno.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string str="";
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					str =reader.GetValue(0).ToString();
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return str;
		}
		public string  UpdateDeliveryLeadTime(string qno,string slno)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "Update WEB_Pricing_Customer_TableV1 set Delivery ='under review' where QuoteNo= '"+qno.Trim()+"' and SL_NO="+slno.Trim();
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string s="";
			try
			{
				command.Connection = GetConnection();
			
				s=command.ExecuteNonQuery().ToString();
				Close();
			}
			catch (Exception ex)
			{		
				s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}

		//part number generator

		public ArrayList SelectSeries()
		{  
	
			string query = "select Series_Name from WEB_Series_TableV1 order by Series_SLNO";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
				
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		//metricqitem
		public  QItems SelectItemsToManage(string code,string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select *  from WEB_Quotation_Items_TableV1 where PartNo='"+code.ToString().Trim()+"'  and Quotation_No='"+qno.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			QItems Qitem=new QItems();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				if(reader.Read())
				{
				
					Qitem.PartNo=reader.GetValue(2).ToString();
					Qitem.S_Code=reader.GetValue(3).ToString();
					Qitem.Series=reader.GetValue(4).ToString();
					Qitem.S_Price=reader.GetValue(5).ToString();
					Qitem.B_Code=reader.GetValue(6).ToString();
					Qitem.Bore=reader.GetValue(7).ToString();
					Qitem.B_Price=reader.GetValue(8).ToString();
					Qitem.R_Code=reader.GetValue(9).ToString();
					Qitem.Rod=reader.GetValue(10).ToString();
					Qitem.R_Price=reader.GetValue(11).ToString();
					Qitem.Stroke_Code=reader.GetValue(12).ToString();
					Qitem.Stroke=reader.GetValue(13).ToString();
					Qitem.Stroke_Price=reader.GetValue(14).ToString();
					Qitem.M_code=reader.GetValue(15).ToString();
					Qitem.Mount=reader.GetValue(16).ToString();
					Qitem.M_Price=reader.GetValue(17).ToString();
					Qitem.RE_Code=reader.GetValue(18).ToString();
					Qitem.RodEnd=reader.GetValue(19).ToString();
					Qitem.RE_Price=reader.GetValue(20).ToString();
					Qitem.Cu_Code=reader.GetValue(21).ToString();
					Qitem.Cushion=reader.GetValue(22).ToString();
					Qitem.Cu_Price=reader.GetValue(23).ToString();
					Qitem.CushionPos_Code=reader.GetValue(24).ToString();
					Qitem.CushionPos=reader.GetValue(25).ToString();
					Qitem.CushionPos_Price=reader.GetValue(26).ToString();
					Qitem.ApplicationOpt_Id =reader.GetValue(27).ToString();
					Qitem.Sel_Code=reader.GetValue(28).ToString();
					Qitem.Seal=reader.GetValue(29).ToString();
					Qitem.Sel_Price=reader.GetValue(30).ToString();
					Qitem.Port_Code=reader.GetValue(31).ToString();
					Qitem.Port=reader.GetValue(32).ToString();
					Qitem.Port_Price=reader.GetValue(33).ToString();
					Qitem.PP_Code=reader.GetValue(34).ToString();
					Qitem.PortPos=reader.GetValue(35).ToString();
					Qitem.PP_Price=reader.GetValue(36).ToString();
					Qitem.PopularOpt_Id=reader.GetValue(37).ToString();
					Qitem.Special_ID=reader.GetValue(38).ToString();
					Qitem.Quantity=reader.GetValue(39).ToString();
					Qitem.Discount=reader.GetValue(40).ToString();
					Qitem.UnitPrice=reader.GetValue(41).ToString();
					Qitem.TotalPrice=reader.GetValue(42).ToString();
					Qitem.Cusomer_ID=reader.GetValue(43).ToString();
					Qitem.Quotation_No=reader.GetValue(44).ToString();
					Qitem.Q_Date=reader.GetValue(45).ToString();
					Qitem.User_ID=reader.GetValue(46).ToString();
					Qitem.PriceList=reader.GetValue(47).ToString();
					Qitem.SpecialReq=reader.GetValue(48).ToString();
					Qitem.Note=reader.GetValue(49).ToString();
					Qitem.Weight=reader.GetValue(50).ToString();
					Qitem.DWG=reader.GetValue(55).ToString();
					//issue #242 start
					Qitem.Units=reader.GetValue(56).ToString();
					//issue #242 end
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				string s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return Qitem;
		}
		public  ArrayList SelectPartNosByQuoteNo(string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select PartNo from WEB_Quotation_Items_TableV1 where  Quotation_No='"+qno.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList qlist=new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();			
				while(reader.Read())
				{				
					qlist.Add(reader.GetValue(0).ToString());					
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				string s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return qlist;
		}
		public ArrayList SelectAopts(string qno,string pno)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select AppoptCode,AppOpts,AppOptPrice from WEB_Application_Opts_Items_TableV1 where QuoteNo ='"+qno.ToString().Trim()+"'  and   PartNo ='"+pno.ToString().Trim()+"' order by SLNO";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					Opts opt =new Opts();
					opt.Code =reader.GetValue(0).ToString();
					opt.Opt=reader.GetValue(1).ToString();
					opt.Free=reader.GetValue(2).ToString();
					list.Add(opt);
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectPopts(string qno,string pno)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select PopoptsCode,PopOpts,PopOptPrice from WEB_Popular_Opts_Items_TableV1 where QuoteNo='"+qno.ToString().Trim()+"'  and   PartNo='"+pno.ToString().Trim()+"' order by SLNO";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					Opts opt =new Opts();
					opt.Code =reader.GetValue(0).ToString();
					opt.Opt=reader.GetValue(1).ToString();
					opt.Free=reader.GetValue(2).ToString();
					list.Add(opt);
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public string SelectSeriesCode(string s)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select Series_code from WEB_Series_TableV1 where Series_name ='"+ s.ToString()+"'";
			string str ="";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
				if(reader.Read())
				{
					str =(reader.GetValue(0)).ToString();
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return str;
		}
		public ArrayList SelectBore(string series)
		{  
		
			//SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select Bore_Size from WEB_Bore_TableV1 Where "+series.ToString()+"= 1";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			 
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectCushion(string series)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select Cushion_Type from WEB_Cushion_TableV1 where "+series.ToString()+"= 1";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectSeal(string series)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select seal_Type from WEB_Seal_TableV1 where "+series.ToString()+"= 1";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try 
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectPortType(string series)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			//UMI Start adding ORDER BY PortType_SLNO
			string query = "select PortType_Type from WEB_PortType_TableV1 where "+series.ToString()+"= 1 ORDER BY PortType_SLNO";
			//UMI end
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			 
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public string SelectBoreCode(string s)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select Bore_code from WEB_Bore_TableV1 where Bore_Size='"+ s.ToString()+"'";
			string str ="";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
				if(reader.Read())
				{
					str =(reader.GetValue(0)).ToString();
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return str;
		}
		//issue #263 start
		public string SelectDelivery(string series, string borecode, decimal qty)
		{  
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.StoredProcedure;
			command.CommandText= "usp_Select_Delivery";
			command.Parameters.Add("@series",series);
			command.Parameters.Add("@bore_code",borecode);
			command.Parameters.Add("@qty",qty);
			string str="TBA";
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
				if(reader.Read())
				{
					str =(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return str;
		}
		//issue #263 end
		//issue #233 start
		public string SelectBoreValue(string s)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select BoreValue from WEB_Bore_TableV1 where Bore_Code='"+ s.ToString()+"'";
			string str ="0";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
				if(reader.Read())
				{
					str =(reader.GetValue(0)).ToString();
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return str;
		}

		public string SelectRodValue(string s)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select Rod_Value from WEB_Rod_TableV1 where Rod_Code='"+ s.ToString()+"'";
			string str ="0";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
				if(reader.Read())
				{
					str =(reader.GetValue(0)).ToString();
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return str;
		}
		//issue #233 end
		public ArrayList SelectRod(string tab,string bore)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select Rod_Size from "+tab.ToString().Trim()+" Where "+bore.ToString()+"= 1";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
				
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
		
				}
				reader.Close();
				Close();
			
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public string SelectCode(string pcode,string ptab,string pname, string data)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select "+pcode.ToString().Trim()+" from "+ptab.ToString().Trim()+"  where "+pname.ToString().Trim()+"  ='"+data.ToString().Trim()+" '";
			string str ="";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
				if(reader.Read())
				{
					str =(reader.GetValue(0)).ToString();
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return str;
		}
		public ArrayList SelectSSProd(string series)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select SSPRod_Type from WEB_SSPRod_TableV1 where "+series.ToString()+"= 1";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectRodSealConf(string series)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select sealConf_Conf from WEB_RodSealConf_TableV1 where  "+series.Trim()+"= 1"; ;
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectPistonSealConf(string series)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select sealConf_Conf from WEB_PistonSealConf_TableV1 where  "+series.Trim()+"= 1"; ;
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList MetalScrap(string series)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select MetalScrapper from WEB_MetalScrapper_TableV1 where "+series.ToString()+"= 1";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectGlandBushing(string series)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select Bush_Type from WEB_GlandBushing_TableV1 where  "+series.Trim()+"= 1"; ;
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectMount(string series,string bore,string table)
		{  
			string query = "select Mount_type from "+table.Trim()+" Where "+series.ToString()+"= 1  and "+bore.ToString()+"= 1 order by mount_slno";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			 	while(reader.Read())
				{
					list.Add(reader.GetValue(0));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectSecRodEnd(string rod)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select SecRodEnd_Type from WEB_SecRodEnd_TableV1 where "+rod.Trim()+"=1";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectDMount(string series,string bore,string table)
		{  
			string query = "select Mount_type from "+table.Trim()+" Where "+series.ToString()+"= 1  and "+bore.ToString()+"= 1";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			 
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectCoating(string series)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select Coating_type from WEB_Coating_TableV1 where "+series.ToString()+"= 1";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectBarrelMaterial(string series)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select Description from WEB_BarrelMaterial_TableV1 where "+series.ToString()+"= 1";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectCushionPos()
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select CushionPos_Pos from WEB_CushionPos_TableV1";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectPortPos()
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select PortPos_Position from WEB_PortPosition_TableV1";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectRodEnd(string rod,string series)
		{  
			string query = "select RodEnd_shape from WEB_RodEndKK_TableV1 where "+rod.Trim()+"=1 and "+series.Trim()+"=1  order by RodEnd_SLNo";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectPortSize(string series,string port)
		{  
			string query = "select PortSize_Type from WEB_PortSize_TableV1 where "+series.ToString()+"= 1 and Porttype = "+port.ToString();
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			ArrayList list = new ArrayList(); 
			command.CommandType = CommandType.Text;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList SelectAirBleeds(string series)
		{  
			string query = "select AirBleed_Type from WEB_AirBleed_TableV1 where "+series.ToString()+"= 1";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public String UpdatePass(string pass,string email)
		{  	
			string query="Update WEB_Users_TableV1 set User_Password='"+pass+"',PassExdate='"+DateTime.Today.ToShortDateString()+"'  where User_IDs ='"+email.ToString().Trim()+"'";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string s="";
			try
			{
				command.Connection = GetConnection();
				int i=command.ExecuteNonQuery();
				s=i.ToString();
				Close();
			}
			catch (Exception ex)
			{		
				s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s; 
		}
		public String ResetPassword(string pass,string email)
		{  	
			string encpass=FormsAuthentication.HashPasswordForStoringInConfigFile(pass,"SHA1");
			DateTime dates=new DateTime(2007,10,10);
			string query="Update WEB_Users_TableV1 set User_Password='"+encpass+"',PassExdate='"+dates.ToShortDateString()+"'  where User_IDs ='"+email.ToString().Trim()+"'";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string s="";
			try
			{
				command.Connection = GetConnection();
				int i=command.ExecuteNonQuery();
				s=i.ToString();
				Close();
			}
			catch (Exception ex)
			{		
				s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s; 
		}
		public ArrayList SelectTransducerDisc(string pcode)
		{  		
			string query="select * from WEB_Transducer_Disc_TableV1  where Code ='"+pcode.ToString().Trim()+" '";
			ArrayList lst=new ArrayList();
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
				if(reader.Read())
				{
					lst.Add(reader.GetValue(2));
					lst.Add(reader.GetValue(3));
					lst.Add(reader.GetValue(4));
					lst.Add(reader.GetValue(5));
					lst.Add(reader.GetValue(6));
					lst.Add(reader.GetValue(7));
					
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				string str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return lst;
		}
		public string FillDataToCrystalReport_ICylinder(string qno)
		{
			string str="";
			try
			{
				string str1="";
				ArrayList cylinderlist=new ArrayList();
				ArrayList appoptlist=new ArrayList();
				ArrayList popoptlist=new ArrayList();
				ArrayList splist=new ArrayList();
				ArrayList z101list=new ArrayList();
				ArrayList acceslist=new ArrayList();
				ArrayList repairlist=new ArrayList();
				ArrayList qtybrkslist=new ArrayList();
				ArrayList commentlist=new ArrayList();
				QItems qitems=new QItems();
				Opts opt=new Opts();
				CrystalReportClass crc=new CrystalReportClass();
				str=FindQuoteFromCrystalReort_WEB(qno.Trim());
				if(str =="true")
				{
					Delete_FromCrystalReoprtTable_WEB(qno.Trim());
					Delete_FromCrystalReoprtCommentTable_WEB(qno.Trim());
					cylinderlist =FindAllCylinders_WEB(qno.Trim());
					if(cylinderlist.Count >0)
					{
						//issue #670 start
						CrystalReportClass crcSurch=new CrystalReportClass();
						ArrayList dicSurch = new ArrayList();
						ArrayList dicSurchDet = new ArrayList();
						crcSurch.QuoteNo = qno.Trim();
						crcSurch.PartNo = "Z101-SURCHARGE";
						crcSurch.Desc = "Raw Material Surcharge";
						crcSurch.Qty = "1";
						crcSurch.UnitPrice = "0";
						crcSurch.Itemprice = crc.UnitPrice;
						//crc.Price = crc.UnitPrice;
						crcSurch.Qtybreaks_qty = "";
						crcSurch.Qtybreaks_uprice="";
						crcSurch.Qtybreaks_tprice = "";
						crcSurch.Itemprice = "";
						crcSurch.Discount="";
						crcSurch.Note="";
						crcSurch.Weight="";
						//issue #670 end
						for(int i=0;i<cylinderlist.Count;i++)
						{
							crc=new CrystalReportClass();
							qitems=new QItems();
							qitems=(QItems)cylinderlist[i];
							crc.QuoteNo=qno.Trim();
							crc.PartNo=qitems.PartNo.Trim();
							string cushion="";
							string application="";
							string popular="";
							string special="";
							string qtybrks_qty="";
							string qtybrks_uprice="";
							string qtybrks_tprice="";
							if(qitems.Cushion.Substring(1,1) =="5" || qitems.Cushion.Substring(1,1) =="6" || qitems.Cushion.Substring(1,1) =="7")
							{
								cushion=";"+qitems.CushionPos.Trim();
							}
							appoptlist=FindAllAppicationOptions_WEB(qno.Trim(),qitems.PartNo.Trim());
							if(appoptlist.Count >0)
							{
								for(int j=0;j<appoptlist.Count;j++)
								{
									application +=";"+appoptlist[j].ToString().Trim();
								}
							}
							popoptlist=FindAllPopularOptions_WEB(qno.Trim(),qitems.PartNo.Trim());
							if(popoptlist.Count >0)
							{
								for(int k=0;k<popoptlist.Count;k++)
								{
									string ppp="";
									if(popoptlist[k].ToString().Trim().Substring(0,2) =="(T")
									{
										ppp=popoptlist[k].ToString().Trim().Replace(",",";");
									}
									else
									{
										ppp=popoptlist[k].ToString().Trim();
									}
									popular +=";"+ppp.Trim();
								}
							}
							splist=FindAllSpecials_WEB(qno.Trim(),qitems.PartNo.Trim());
							if(splist.Count >0)
							{
								for(int l=0;l<splist.Count;l++)
								{
									special +=";"+splist[l].ToString().Trim();
								}
							}
							//metricbore
							//CrystalReport
							//issue #242
							string strBoreMetric = Select_BoreMetric_ByBore(qitems.B_Code);
							string strRodMetric = Select_RodMetric_ByRod(qitems.R_Code);
							crc.Desc=qitems.Series.Trim()+";"+qitems.Bore.Trim()+";"+qitems.Rod.Trim()+";"+qitems.RodEnd.Trim()+";"
								+qitems.Cushion.Trim()+cushion.Trim()+application.Trim()+";"+qitems.Seal.Trim()+";"+qitems.Port.Trim()+";"+
								qitems.PortPos.Trim()+";"+qitems.Mount.Trim()+";"+qitems.Stroke.Trim()+popular.Trim()+special.Trim();
							if(qitems.Units=="metric")
							{
								crc.Desc=qitems.Series.Trim()+";"+qitems.Bore.Trim()+" ("+strBoreMetric+" mm)"+";"+qitems.Rod.Trim()+" ("+strRodMetric+" mm);"+qitems.RodEnd.Trim()+";"
									+qitems.Cushion.Trim()+cushion.Trim()+application.Trim()+";"+qitems.Seal.Trim()+";"+qitems.Port.Trim()+";"+
									qitems.PortPos.Trim()+";"+qitems.Mount.Trim()+";"+qitems.Stroke.Trim()+" ("+Convert.ToString(Math.Round(Convert.ToDecimal(qitems.Stroke_Code)*25.4m,2))+" mm)"+popular.Trim()+special.Trim();
							}
							//issue #137 start
							if(qitems.S_Code=="AS")
							{
								//issue #242 start
								if(qitems.Units=="metric")
								{
									if(application.Trim()=="")
										crc.Desc=qitems.Series.Trim()+";"+qitems.Bore.Trim()+" ("+strBoreMetric+" mm)"+";"+qitems.Rod.Trim()+" ("+strRodMetric+" mm);"+qitems.RodEnd.Trim()+";"
											+qitems.Seal.Trim()+";"+qitems.Port.Trim()+";"+
											qitems.PortPos.Trim()+";"+qitems.Mount.Trim()+";"+qitems.Stroke.Trim()+" ("+Convert.ToString(Math.Round(Convert.ToDecimal(qitems.Stroke_Code)*25.4m,2))+" mm)"+popular.Trim().Replace("FO","O").Replace("FC","C")+special.Trim();
									else 
										crc.Desc=qitems.Series.Trim()+";"+qitems.Bore.Trim()+" ("+strBoreMetric+" mm)"+";"+qitems.Rod.Trim()+";"+qitems.RodEnd.Trim()+" ("+strRodMetric+" mm);"+qitems.RodEnd.Trim()+";"
											+application.Trim()+";"+qitems.Seal.Trim()+";"+qitems.Port.Trim()+";"+
											qitems.PortPos.Trim()+";"+qitems.Mount.Trim()+";"+qitems.Stroke.Trim()+" ("+Convert.ToString(Math.Round(Convert.ToDecimal(qitems.Stroke_Code)*25.4m,2))+" mm)"+popular.Trim().Replace("FO","O").Replace("FC","C")+special.Trim();
								}
								else 
								{
									if(application.Trim()=="")
										crc.Desc=qitems.Series.Trim()+";"+qitems.Bore.Trim()+";"+qitems.Rod.Trim()+";"+qitems.RodEnd.Trim()+";"
											+qitems.Seal.Trim()+";"+qitems.Port.Trim()+";"+
											qitems.PortPos.Trim()+";"+qitems.Mount.Trim()+";"+qitems.Stroke.Trim()+popular.Trim().Replace("FO","O").Replace("FC","C")+special.Trim();
									else 
										crc.Desc=qitems.Series.Trim()+";"+qitems.Bore.Trim()+";"+qitems.Rod.Trim()+";"+qitems.RodEnd.Trim()+";"
											+application.Trim()+";"+qitems.Seal.Trim()+";"+qitems.Port.Trim()+";"+
											qitems.PortPos.Trim()+";"+qitems.Mount.Trim()+";"+qitems.Stroke.Trim()+popular.Trim().Replace("FO","O").Replace("FC","C")+special.Trim();
								}
							}
							//issue #137 end
							crc.Qty=String.Format("{0:###}",Convert.ToDecimal(qitems.Quantity.Trim()));
							crc.UnitPrice=qitems.UnitPrice.Trim();
							crc.Price=qitems.TotalPrice.Trim();
						
							qtybrkslist =FindALlQtyBreaks_WEB(qitems.PartNo.Trim(),qno.Trim());
							if(qtybrkslist.Count >0)
							{
								if(qtybrkslist[0] !=null)
								{
									ArrayList temp1=new ArrayList();
									temp1=(ArrayList)qtybrkslist[0];
									for(int m=0;m<temp1.Count;m++)
									{
										qtybrks_qty +=temp1[m].ToString().Trim();
										if(m !=temp1.Count -1)
										{
											qtybrks_qty +=";";
										}
									}
								}
								if(qtybrkslist[1] !=null)
								{
									ArrayList temp1=new ArrayList();
									temp1=(ArrayList)qtybrkslist[1];
									for(int m=0;m<temp1.Count;m++)
									{
										qtybrks_uprice +=temp1[m].ToString().Trim();
										if(m !=temp1.Count -1)
										{
											qtybrks_uprice +=";";
										}
									}
								}
								if(qtybrkslist[2] !=null)
								{
									ArrayList temp1=new ArrayList();
									temp1=(ArrayList)qtybrkslist[2];
									for(int m=0;m<temp1.Count;m++)
									{
										qtybrks_tprice +=temp1[m].ToString().Trim();
										if(m !=temp1.Count -1)
										{
											qtybrks_tprice +=";";
										}
									}
								}
							}
							crc.Qtybreaks_qty=qtybrks_qty.Trim();
							crc.Qtybreaks_uprice=qtybrks_uprice.Trim();
							crc.Qtybreaks_tprice=qtybrks_tprice.Trim();
							crc.Itemprice="";
							crc.Discount="";
							crc.Note=qitems.Note.Trim();
							crc.Weight=qitems.Weight.ToString();
							//metricinput
							//issue #242 start
							crc.Units = qitems.Units;
							//issue #242 end
							str1=Insert_IntoCrystalReoprtTable_WEB(crc);
							//issue #670 start
							decimal dcSurchCyl = (Surcharge_Select_Percentage_Series(qitems.S_Code,qitems.B_Code)*Convert.ToDecimal(qitems.TotalPrice)/100);
							dicSurchDet = new ArrayList();
							dicSurchDet.Add(qitems.PartNo);
							dicSurchDet.Add(dcSurchCyl);
							dicSurch.Add(dicSurchDet);
							decimal dcsurchargeCan = 0m;
							if(qitems.S_Code=="AS")
							{
								string canPrice="0";
								string canName="0";
								popoptlist=SelectAppPopOpts_ICyl(qitems.PartNo.Trim(),qno.Trim(),"sp_SelectPopts_WEB");
								blCanisterPN blcanCop = ASSeries_Select_Canister_ICyl(qitems.Quotation_No, qitems.PartNo);
								if(blcanCop.CanisterNo.Trim()=="4" || blcanCop.CanisterNo.Trim()=="5" || blcanCop.CanisterNo.Trim()=="6" || blcanCop.CanisterNo.Trim()=="7" || blcanCop.CanisterNo.Trim()=="8" || blcanCop.CanisterNo.Trim()=="9" || blcanCop.CanisterNo.Trim()=="10")
								{
									dcsurchargeCan = 9;

								}
								else
								{
									dcsurchargeCan = 14;
								}
								if(popoptlist.Count >0)
								{
									ExtraClass xtra=new ExtraClass();
									for(int k=0;k<popoptlist.Count;k++)
									{
										xtra=new ExtraClass();
										xtra=(ExtraClass)popoptlist[k];
										if(xtra.Code.ToString().Trim() =="FC" || xtra.Code.ToString().Trim() =="FO")
										{
											canPrice=xtra.Price.Trim();
										}
									
									}
								}
								dcsurchargeCan = (dcsurchargeCan*Convert.ToDecimal(qitems.Quantity)*Convert.ToDecimal(canPrice)/100);
								dicSurchDet = new ArrayList();
								dicSurchDet.Add(qitems.PartNo+"_"+canName);
								dicSurchDet.Add(dcsurchargeCan);
								dicSurch.Add(dicSurchDet);
							}
                            //issue #670 end
							
						}
						//issue #670 start
						decimal dcSurchTot = 0m;
						foreach(ArrayList arrDet in dicSurch)
						{
							string strCyl = arrDet[0].ToString();
							decimal dcCyl = Convert.ToDecimal(arrDet[1].ToString());
							dcSurchTot += dcCyl;
						}
						crcSurch.UnitPrice = dcSurchTot.ToString();
						crcSurch.Price = dcSurchTot.ToString();
						//issue #670 to be published
						//Insert_IntoCrystalReoprtTable_ICyl_Metric(crcSurch,"sp_Insert_IntoCrystalReoprtTable_WEB_Metric"););
						crcSurch = null;
						//issue #670 end
					}
					z101list=FindAllZ101_WEB(qno.Trim());
					if(z101list.Count >0)
					{
						for(int n=0;n<z101list.Count;n++)
						{
							string qtybrks_qty="";
							string qtybrks_uprice="";
							string qtybrks_tprice="";
							crc =new CrystalReportClass();
							Others oth=new Others();
							oth=(Others)z101list[n];
							crc.QuoteNo=qno.Trim();
							crc.PartNo=oth.Code.Trim();
							crc.Desc=oth.Desc.Replace(";",";");
							crc.Qty=oth.Qty.Trim();
							crc.UnitPrice=oth.UnitP.Trim();
							crc.Price=oth.Price.Trim();
							qtybrkslist=new ArrayList();
							qtybrkslist =FindALlQtyBreaks_WEB(crc.PartNo.Trim(),qno.Trim());
							if(qtybrkslist.Count >0)
							{
								if(qtybrkslist[0] !=null)
								{
									ArrayList temp1=new ArrayList();
									temp1=(ArrayList)qtybrkslist[0];
									for(int m=0;m<temp1.Count;m++)
									{
										qtybrks_qty +=temp1[m].ToString().Trim();
										if(m !=temp1.Count -1)
										{
											qtybrks_qty +=";";
										}
									}
								}
								if(qtybrkslist[1] !=null)
								{
									ArrayList temp1=new ArrayList();
									temp1=(ArrayList)qtybrkslist[1];
									for(int m=0;m<temp1.Count;m++)
									{
										qtybrks_uprice +=temp1[m].ToString().Trim();
										if(m !=temp1.Count -1)
										{
											qtybrks_uprice +=";";
										}
									}
								}
								if(qtybrkslist[2] !=null)
								{
									ArrayList temp1=new ArrayList();
									temp1=(ArrayList)qtybrkslist[2];
									for(int m=0;m<temp1.Count;m++)
									{
										qtybrks_tprice +=temp1[m].ToString().Trim();
										if(m !=temp1.Count -1)
										{
											qtybrks_tprice +=";";
										}
									}
								}
							}
							crc.Qtybreaks_qty=qtybrks_qty.Trim();
							crc.Qtybreaks_uprice=qtybrks_uprice.Trim();
							crc.Qtybreaks_tprice=qtybrks_tprice.Trim();
							crc.Itemprice="";
							crc.Discount="";
							crc.Note="";
							crc.Weight="";
							//metricinput
							//issue #242 start
							crc.Units = qitems.Units;
							//issue #242 end
							str1=Insert_IntoCrystalReoprtTable_WEB(crc);
						}
					}
					acceslist=FindAllAccessories_WEB(qno.Trim());
					if(acceslist.Count >0)
					{
						for(int n=0;n<acceslist.Count;n++)
						{
							string qtybrks_qty="";
							string qtybrks_uprice="";
							string qtybrks_tprice="";
							crc =new CrystalReportClass();
							Others oth=new Others();
							oth=(Others)acceslist[n];
							crc.QuoteNo=qno.Trim();
							crc.PartNo=oth.Code.Trim();
							crc.Desc=oth.Desc.Replace(";",";");
							crc.Qty=oth.Qty.Trim();
							crc.UnitPrice=oth.UnitP.Trim();
							crc.Price=oth.Price.Trim();
							crc.Qtybreaks_qty=qtybrks_qty.Trim();
							crc.Qtybreaks_uprice=qtybrks_uprice.Trim();
							crc.Qtybreaks_tprice=qtybrks_tprice.Trim();
							crc.Itemprice="";
							crc.Discount="";
							crc.Note="";
							crc.Weight="";
							//metricinput
							//issue #242 start
							crc.Units = qitems.Units;
							//issue #242 end
							str1=Insert_IntoCrystalReoprtTable_WEB(crc);
						}
					}
					repairlist=FindAllRepairkits_WEB(qno.Trim());
					if(repairlist.Count >0)
					{
						for(int n=0;n<repairlist.Count;n++)
						{
							string qtybrks_qty="";
							string qtybrks_uprice="";
							string qtybrks_tprice="";
							crc =new CrystalReportClass();
							Rkits oth=new Rkits();
							oth=(Rkits)repairlist[n];
							crc.QuoteNo=qno.Trim();
							crc.PartNo=oth.PartNo.Trim();
							crc.Desc=oth.Series.ToString().Trim()+";";
							if(oth.PartNo.Trim().Substring(0,2) =="95")
							{
								crc.Desc +="("+oth.B_Code.ToString().Trim()+") "+oth.Bore.ToString().Trim()+";"
									+"("+oth.R_Code.ToString().Trim()+") "+oth.Rod.ToString().Trim()+";";
								if(oth.C_Code.ToString().Trim()  !="")
								{
									crc.Desc += "("+oth.C_Code.ToString().Trim()+") "+oth.Cushion.ToString().Trim()+";";
								}	
								if(oth.SecR_Code.ToString().Trim()  !="")
								{
									crc.Desc += "("+oth.SecR_Code.ToString().Trim()+") "+oth.SecRod.ToString().Trim()+";";
								}
								if(oth.RodS_Code.ToString().Trim()  !="")
								{
									crc.Desc += "("+oth.RodS_Code.ToString().Trim()+") "+oth.RodSeal.ToString().Trim()+";";
								}
								if(oth.PistonS_Code.ToString().Trim()  !="")
								{
									crc.Desc += "("+oth.PistonS_Code.ToString().Trim()+") "+oth.PistonSeal.ToString().Trim()+";";
								}
								if(oth.MS_Code.ToString().Trim()  !="")
								{
									crc.Desc += "("+oth.MS_Code.ToString().Trim()+") "+oth.MetalScrapper.ToString().Trim()+";";
								}
								if(oth.GD_Code.ToString().Trim()  !="")
								{
									crc.Desc += "("+oth.GD_Code.ToString().Trim()+") "+oth.GlandDrain.ToString().Trim()+";";
								}
								crc.Desc += "("+oth.Sel_Code.ToString().Trim()+") "+oth.Seal.ToString().Trim()+";";
								if(oth.Multi_code.ToString().Trim()  !="")
								{
									crc.Desc += "("+oth.Multi_code.ToString().Trim()+") "+oth.MultiCylinder.ToString().Trim()+";";
								}
								int bs=2;
								int pp=2;
								int gp=1;
								int gpb=1;
								int gs=1;
								int gsb=1;
								int rw=1;
								int cp=2;
								if(oth.SecR_Code.ToString().Trim() !="" )
								{
									bs=2;
									pp=2;
									gp=2;
									gpb=2;
									gs=2;
									gsb=2;
									rw=2;
									cp=4;
								}
								else if(oth.Multi_code.ToString().Trim() !="")
								{
									bs=4;
									pp=4;
									gp=2;
									gpb=2;
									gs=2;
									gsb=2;
									rw=2;
									cp=4;
								}
								crc.Desc +="Standard Repair Kit contains:-;"+
									"Description					Qty;"+
									"Barrel Seal					"+bs.ToString()+";"+
									"Piston Packing				"+pp.ToString()+";"+
									"Gland Packing				"+gp.ToString()+";"+
									"Gland Packing Back-up		"+gpb.ToString()+";"+
									"Gland Seal					"+gs.ToString()+";"+
									"Gland Seal Back-up			"+gsb.ToString()+";"+
									"Rod Wiper					"+rw.ToString()+";"+
									"Cushion Packing				"+cp.ToString();
							}
							crc.Qty=oth.Qty.Trim();
							crc.UnitPrice=oth.NetPrice.Trim();
							crc.Price=oth.TPrice.Trim();
							crc.Qtybreaks_qty=qtybrks_qty.Trim();
							crc.Qtybreaks_uprice=qtybrks_uprice.Trim();
							crc.Qtybreaks_tprice=qtybrks_tprice.Trim();
							crc.Itemprice="";
							crc.Discount="";
							crc.Note=oth.Note.ToString();
							crc.Weight="";
							//metricinput
							//issue #242 start
							crc.Units = qitems.Units;
							//issue #242 end
							str1=Insert_IntoCrystalReoprtTable_WEB(crc);
						}
					}
					
					commentlist=FindAllComments_WEB(qno.Trim());
					if(commentlist.Count >0)
					{
						string comment="";
						int p=1;
						for(int n=0;n<commentlist.Count;n++)
						{
							
							comment +=p.ToString()+". "+commentlist[n].ToString()+";";
							p++;
						}
						str1=Insert_CommentsIntoCrystalReoprtTable_WEB(qno.Trim(),comment.Trim());
					}
					else
					{
						str1=Insert_CommentsIntoCrystalReoprtTable_WEB(qno.Trim(),"");
					}
					
				}
				else if(str =="false")
				{
					cylinderlist =FindAllCylinders_WEB(qno.Trim());
					if(cylinderlist.Count >0)
					{
						for(int i=0;i<cylinderlist.Count;i++)
						{
							crc=new CrystalReportClass();
							qitems=new QItems();
							qitems=(QItems)cylinderlist[i];
							crc.QuoteNo=qno.Trim();
							crc.PartNo=qitems.PartNo.Trim();
							string cushion="";
							string application="";
							string popular="";
							string special="";
							string qtybrks_qty="";
							string qtybrks_uprice="";
							string qtybrks_tprice="";
							if(qitems.Cushion.Trim() =="(5) Cushioned Both Ends" || qitems.Cushion.Trim() =="(6) Head End Cushion" ||
								qitems.Cushion.Trim() =="(7) Cap End Cushion")
							{
								cushion=";"+qitems.CushionPos.Trim();
							}
							//popswap
							appoptlist=FindAllAppicationOptions_WEB(qno.Trim(),qitems.PartNo.Trim());
							if(appoptlist.Count >0)
							{
								for(int j=0;j<appoptlist.Count;j++)
								{
									application +=";"+appoptlist[j].ToString().Trim();
								}
							}
							popoptlist=FindAllPopularOptions_WEB(qno.Trim(),qitems.PartNo.Trim());
							if(popoptlist.Count >0)
							{
								for(int k=0;k<popoptlist.Count;k++)
								{
									string ppp="";
									if(popoptlist[k].ToString().Trim().Substring(0,2) =="(T")
									{
										ppp=popoptlist[k].ToString().Trim().Replace(",",";");
									}
									else
									{
										ppp=popoptlist[k].ToString().Trim();
									}
									popular +=";"+ppp.Trim();
								}
							}
							splist=FindAllSpecials_WEB(qno.Trim(),qitems.PartNo.Trim());
							if(splist.Count >0)
							{
								for(int l=0;l<splist.Count;l++)
								{
									special +=";"+splist[l].ToString().Trim();
								}
							}
							//CrystalReport
							//metricbore
							string strBoreMetric = Select_BoreMetric_ByBore(qitems.B_Code);
							string strRodMetric = Select_RodMetric_ByRod(qitems.R_Code);
							crc.Desc=qitems.Series.Trim()+";"+qitems.Bore.Trim()+";"+qitems.Rod.Trim()+";"+qitems.RodEnd.Trim()+";"
								+qitems.Cushion.Trim()+cushion.Trim()+application.Trim()+";"+qitems.Seal.Trim()+";"+qitems.Port.Trim()+";"+
								qitems.PortPos.Trim()+";"+qitems.Mount.Trim()+";"+qitems.Stroke.Trim()+popular.Trim()+special.Trim();
					        if(qitems.Units=="metric")
								crc.Desc=qitems.Series.Trim()+";"+qitems.Bore.Trim()+" ("+strBoreMetric+" mm)"+";"+qitems.Rod.Trim()+" ("+strRodMetric+" mm);"+qitems.RodEnd.Trim()+";"
									+qitems.Cushion.Trim()+cushion.Trim()+application.Trim()+";"+qitems.Seal.Trim()+";"+qitems.Port.Trim()+";"+
									qitems.PortPos.Trim()+";"+qitems.Mount.Trim()+";"+qitems.Stroke.Trim()+" ("+Convert.ToString(Math.Round(Convert.ToDecimal(qitems.Stroke_Code)*25.4m,2))+" mm)"+popular.Trim()+special.Trim();
					        
							//issue #137 start
							if(qitems.S_Code=="AS")
							{
								//issue #242 start
								//update
								if(qitems.Units=="metric")
								{
									if(application.Trim()=="")
										crc.Desc=qitems.Series.Trim()+";"+qitems.Bore.Trim()+" ("+strBoreMetric+" mm)"+";"+qitems.Rod.Trim()+" ("+strRodMetric+" mm);"+qitems.RodEnd.Trim()+";"
											+qitems.Seal.Trim()+";"+qitems.Port.Trim()+";"+
											qitems.PortPos.Trim()+";"+qitems.Mount.Trim()+";"+qitems.Stroke.Trim()+" ("+Convert.ToString(Math.Round(Convert.ToDecimal(qitems.Stroke_Code)*25.4m,2))+" mm)"+popular.Trim().Replace("FO","O").Replace("FC","C")+special.Trim();
									else 
										crc.Desc=qitems.Series.Trim()+";"+qitems.Bore.Trim()+" ("+strBoreMetric+" mm)"+";"+qitems.Rod.Trim()+";"+qitems.RodEnd.Trim()+" ("+strRodMetric+" mm);"+qitems.RodEnd.Trim()+";"
											+application.Trim()+";"+qitems.Seal.Trim()+";"+qitems.Port.Trim()+";"+
											qitems.PortPos.Trim()+";"+qitems.Mount.Trim()+";"+qitems.Stroke.Trim()+" ("+Convert.ToString(Math.Round(Convert.ToDecimal(qitems.Stroke_Code)*25.4m,2))+popular.Trim().Replace("FO","O").Replace("FC","C")+special.Trim();
								}
								else
								{
								//back
									if(application.Trim()=="")
										crc.Desc=qitems.Series.Trim()+";"+qitems.Bore.Trim()+";"+qitems.Rod.Trim()+";"+qitems.RodEnd.Trim()+";"
											+qitems.Seal.Trim()+";"+qitems.Port.Trim()+";"+
											qitems.PortPos.Trim()+";"+qitems.Mount.Trim()+";"+qitems.Stroke.Trim()+popular.Trim().Replace("FO","O").Replace("FC","C")+special.Trim();
									else 
										crc.Desc=qitems.Series.Trim()+";"+qitems.Bore.Trim()+";"+qitems.Rod.Trim()+";"+qitems.RodEnd.Trim()+";"
											+application.Trim()+";"+qitems.Seal.Trim()+";"+qitems.Port.Trim()+";"+
											qitems.PortPos.Trim()+";"+qitems.Mount.Trim()+";"+qitems.Stroke.Trim()+popular.Trim().Replace("FO","O").Replace("FC","C")+special.Trim();
								}
							}
							//issue #137 end
							crc.Qty=String.Format("{0:###}",Convert.ToDecimal(qitems.Quantity.Trim()));
							crc.UnitPrice=qitems.UnitPrice.Trim();
							crc.Price=qitems.TotalPrice.Trim();
						
							qtybrkslist =FindALlQtyBreaks_WEB(qitems.PartNo.Trim(),qno.Trim());
							if(qtybrkslist.Count >0)
							{
								if(qtybrkslist[0] !=null)
								{
									ArrayList temp1=new ArrayList();
									temp1=(ArrayList)qtybrkslist[0];
									for(int m=0;m<temp1.Count;m++)
									{
										qtybrks_qty +=temp1[m].ToString().Trim();
										if(m !=temp1.Count -1)
										{
											qtybrks_qty +=";";
										}
									}
								}
								if(qtybrkslist[1] !=null)
								{
									ArrayList temp1=new ArrayList();
									temp1=(ArrayList)qtybrkslist[1];
									for(int m=0;m<temp1.Count;m++)
									{
										qtybrks_uprice +=temp1[m].ToString().Trim();
										if(m !=temp1.Count -1)
										{
											qtybrks_uprice +=";";
										}
									}
								}
								if(qtybrkslist[2] !=null)
								{
									ArrayList temp1=new ArrayList();
									temp1=(ArrayList)qtybrkslist[2];
									for(int m=0;m<temp1.Count;m++)
									{
										qtybrks_tprice +=temp1[m].ToString().Trim();
										if(m !=temp1.Count -1)
										{
											qtybrks_tprice +=";";
										}
									}
								}
							}
							crc.Qtybreaks_qty=qtybrks_qty.Trim();
							crc.Qtybreaks_uprice=qtybrks_uprice.Trim();
							crc.Qtybreaks_tprice=qtybrks_tprice.Trim();
							crc.Itemprice="";
							crc.Discount="";
							crc.Note=qitems.Note.Trim();
							crc.Weight=qitems.Weight.ToString();
							//metricinput
							//issue #242 start
							crc.Units = qitems.Units;
							//issue #242 end
							str1=Insert_IntoCrystalReoprtTable_WEB(crc);
						
						}
					}
					z101list=FindAllZ101_WEB(qno.Trim());
					if(z101list.Count >0)
					{
						for(int n=0;n<z101list.Count;n++)
						{
							string qtybrks_qty="";
							string qtybrks_uprice="";
							string qtybrks_tprice="";
							crc =new CrystalReportClass();
							Others oth=new Others();
							oth=(Others)z101list[n];
							crc.QuoteNo=qno.Trim();
							crc.PartNo=oth.Code.Trim();
							crc.Desc=oth.Desc.Replace(";",";");
							crc.Qty=oth.Qty.Trim();
							crc.UnitPrice=oth.UnitP.Trim();
							crc.Price=oth.Price.Trim();
							qtybrkslist=new ArrayList();
							qtybrkslist =FindALlQtyBreaks_WEB(crc.PartNo.Trim(),qno.Trim());
							if(qtybrkslist.Count >0)
							{
								if(qtybrkslist[0] !=null)
								{
									ArrayList temp1=new ArrayList();
									temp1=(ArrayList)qtybrkslist[0];
									for(int m=0;m<temp1.Count;m++)
									{
										qtybrks_qty +=temp1[m].ToString().Trim();
										if(m !=temp1.Count -1)
										{
											qtybrks_qty +=";";
										}
									}
								}
								if(qtybrkslist[1] !=null)
								{
									ArrayList temp1=new ArrayList();
									temp1=(ArrayList)qtybrkslist[1];
									for(int m=0;m<temp1.Count;m++)
									{
										qtybrks_uprice +=temp1[m].ToString().Trim();
										if(m !=temp1.Count -1)
										{
											qtybrks_uprice +=";";
										}
									}
								}
								if(qtybrkslist[2] !=null)
								{
									ArrayList temp1=new ArrayList();
									temp1=(ArrayList)qtybrkslist[2];
									for(int m=0;m<temp1.Count;m++)
									{
										qtybrks_tprice +=temp1[m].ToString().Trim();
										if(m !=temp1.Count -1)
										{
											qtybrks_tprice +=";";
										}
									}
								}
							}
							crc.Qtybreaks_qty=qtybrks_qty.Trim();
							crc.Qtybreaks_uprice=qtybrks_uprice.Trim();
							crc.Qtybreaks_tprice=qtybrks_tprice.Trim();
							crc.Itemprice="";
							crc.Discount="";
							crc.Note="";
							crc.Weight="";
							//metricinput
							//issue #242 start
							crc.Units = qitems.Units;
							//issue #242 end
							str1=Insert_IntoCrystalReoprtTable_WEB(crc);
						}
					}
					acceslist=FindAllAccessories_WEB(qno.Trim());
					if(acceslist.Count >0)
					{
						for(int n=0;n<acceslist.Count;n++)
						{
							string qtybrks_qty="";
							string qtybrks_uprice="";
							string qtybrks_tprice="";
							crc =new CrystalReportClass();
							Others oth=new Others();
							oth=(Others)acceslist[n];
							crc.QuoteNo=qno.Trim();
							crc.PartNo=oth.Code.Trim();
							crc.Desc=oth.Desc.Replace(";",";");
							crc.Qty=oth.Qty.Trim();
							crc.UnitPrice=oth.UnitP.Trim();
							crc.Price=oth.Price.Trim();
							crc.Qtybreaks_qty=qtybrks_qty.Trim();
							crc.Qtybreaks_uprice=qtybrks_uprice.Trim();
							crc.Qtybreaks_tprice=qtybrks_tprice.Trim();
							crc.Itemprice="";
							crc.Discount="";
							crc.Note="";
							crc.Weight="";
							//metricinput
							//issue #242 start
							crc.Units = qitems.Units;
							//issue #242 end
							str1=Insert_IntoCrystalReoprtTable_WEB(crc);
						}
					}
					repairlist=FindAllRepairkits_WEB(qno.Trim());
					if(repairlist.Count >0)
					{
						for(int n=0;n<repairlist.Count;n++)
						{
							string qtybrks_qty="";
							string qtybrks_uprice="";
							string qtybrks_tprice="";
							crc =new CrystalReportClass();
							Others oth=new Others();
							oth=(Others)repairlist[n];
							crc.QuoteNo=qno.Trim();
							crc.PartNo=oth.Code.Trim();
							crc.Desc=oth.Desc.Replace(";",";")+";"+oth.Desc1.Replace(";","chr(13)")+";"+oth.Desc2.Replace(";",";")+";"+oth.Desc3.Replace(";",";");
							crc.Qty=oth.Qty.Trim();
							crc.UnitPrice=oth.UnitP.Trim();
							crc.Price=oth.Price.Trim();
							crc.Qtybreaks_qty=qtybrks_qty.Trim();
							crc.Qtybreaks_uprice=qtybrks_uprice.Trim();
							crc.Qtybreaks_tprice=qtybrks_tprice.Trim();
							crc.Itemprice="";
							crc.Discount="";
							crc.Note="";
							crc.Weight="";
							//metricinput
							//issue #242 start
							crc.Units = qitems.Units;
							//issue #242 end
							str1=Insert_IntoCrystalReoprtTable_WEB(crc);
						}
					}
					commentlist=FindAllComments_WEB(qno.Trim());
					if(commentlist.Count >0)
					{
						string comment="";
						int p=1;
						for(int n=0;n<commentlist.Count;n++)
						{
							
							comment +=p.ToString()+". "+commentlist[n].ToString()+";";
							p++;
						}
						str1=Insert_CommentsIntoCrystalReoprtTable_WEB(qno.Trim(),comment.Trim());
					}
					else
					{
						str1=Insert_CommentsIntoCrystalReoprtTable_WEB(qno.Trim(),"");
					}
					
				}
				str="success";
			}
			catch (Exception ex)
			{		
				string sr=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return str;
		}
		//issue #670 start
		public decimal Surcharge_Select_Percentage_Series(string  series,string bore)
		{  		
			string query="";
			query="usp_Surcharge_GetPercentage_BySeries";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.StoredProcedure;
			command.Parameters.Add("@series",series.Trim());
			command.Parameters.Add("@bore",bore.Trim());
			SqlDataReader reader =null;
			decimal dcPercentage = 0m;
			//DBManager_Access db=new DBManager_Access();
			try
			{
				IDisposable connection=null;connection = GetConnection();command.Connection = (SqlConnection)connection;
				reader = command.ExecuteReader(CommandBehavior.CloseConnection);
				if(reader.Read())
				{
					if(IsNumeric(reader.GetValue(0).ToString()))
					{
						dcPercentage = Convert.ToDecimal(reader.GetValue(0).ToString());
					}
				}
				reader.Close();
				Close();
			}
			finally
			{
				if (reader != null)
				{
					reader.Close();
				}
				Close();
			}
			return dcPercentage;
		}
		public blCanisterPN ASSeries_Select_Canister_ICyl(string quoteno, string partno)
		{ 
			
			int intResult = 0;
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.Text;
			//issue #229 start
			//backup
			//			command.CommandText= " Select [QuoteNo], [PartNo], [CanisterNo], [CanisterCode], [FailMode], [Preload], [ETC], [ETO], [BTO], [AirPressure], "+
			//				" [PackingFriction], [SpringRate], [EtcValveTrust] , [Price], [BTC]  from Canister_Items_TableV1 " +
			//				" WHERE QuoteNo= @quoteno AND PartNo = @partno";
			//update
			command.CommandText= " Select [QuoteNo], [PartNo], [CanisterNo], [CanisterCode], [FailMode], [Preload], [ETC], [ETO], [BTO], [AirPressure], "+
				" [PackingFriction], [SpringRate], [EtcValveTrust] , [Price], [BTC], [BTOReq] from Canister_Items_TableV1 " +
				" WHERE QuoteNo= @quoteno AND PartNo = @partno";
			//issue #229 end
			command.Parameters.Add("@quoteno", quoteno);
			command.Parameters.Add("@partno", partno);
			SqlParameter result = new SqlParameter("@result", SqlDbType.Int);
			result.Direction = ParameterDirection.Output;
			command.Parameters.Add(result);
			blCanisterPN blcanister = new blCanisterPN();
			string strResult="";
			strResult=intResult.ToString();
			try
			{
				DataTable dt = new DataTable();
				IDisposable connection=null;connection = GetConnection();command.Connection = (SqlConnection)connection;
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;
				intResult=oledad.Fill(dt);
				Close();
				if(intResult>0)
				{
					blcanister.CanisterNo=dt.Rows[0]["CanisterNo"].ToString();
					blcanister.FailMode=dt.Rows[0]["FailMode"].ToString();
					blcanister.Preload=dt.Rows[0]["Preload"].ToString();
					blcanister.ETC=dt.Rows[0]["ETC"].ToString();
					blcanister.ETO=dt.Rows[0]["ETO"].ToString();
					blcanister.BTO=dt.Rows[0]["BTO"].ToString();
					blcanister.BTC=dt.Rows[0]["BTC"].ToString();
					blcanister.AirPressure=dt.Rows[0]["AirPressure"].ToString();
					if(dt.Rows[0]["AirPressure"].ToString()=="") blcanister.AirPressure="0";
					blcanister.PackingFriction=dt.Rows[0]["PackingFriction"].ToString();
					blcanister.SpringRate=dt.Rows[0]["SpringRate"].ToString();
					blcanister.EtcValveTrust=dt.Rows[0]["EtcValveTrust"].ToString();
					blcanister.Price=dt.Rows[0]["Price"].ToString();
					blcanister.Preload=dt.Rows[0]["Preload"].ToString();
					//issue #229 start
					//blcanister.BTOReq=dt.Rows[0]["BTOReq"].ToString();
					//issue #229 end
				}
				else
				{
					blcanister=null;
				}

			}
			finally
			{
				Close();
			}
			return blcanister;
            
		}
		public  bool IsNumeric(string strInteger) 
		{
			try 
			{
				int intTemp =0;
				if(strInteger.ToString().StartsWith(".") == true)
				{
					for(int i=1; i< strInteger.Length;i++)
					{
						intTemp = Int32.Parse( strInteger.Substring(i,1) );
					}
				}
				else
				{
					for(int i=0; i< strInteger.Length;i++)
					{
						if (strInteger.ToString().Substring(i,1) !=".")
						{
							intTemp = Int32.Parse( strInteger.Substring(i,1) );
						}
					}
				}
				return true;
			} 
			catch (FormatException) 
			{
				return false;
			}    
		}
		//issue #670 end
		public  string FindQuoteFromCrystalReort_WEB(string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select * from WEB_CrystalReport_Template_TableV1 Where QuoteNo='"+qno.Trim()+"'";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string res="false";
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
				if(reader.Read())
				{
					res="true";
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				string sa=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return res;
		}
		public string  Delete_FromCrystalReoprtTable_WEB(string qno)
		{  
	
			string query = "Delete from WEB_CrystalReport_Template_TableV1 where QuoteNo='"+qno.Trim()+"'";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string s="";
			try
			{
				command.Connection = GetConnection();
				s=command.ExecuteNonQuery().ToString();
				Close();
			}
			catch (Exception ex)
			{		
				string sr=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public string  Delete_FromCrystalReoprtCommentTable_WEB(string qno)
		{  
	
			string query = "Delete from WEB_CrystalReport_Comment_TabeV1 where QuoteNo='"+qno.Trim()+"'";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string s="";
			try
			{
				command.Connection = GetConnection();
				s=command.ExecuteNonQuery().ToString();
				Close();
			}
			catch (Exception ex)
			{		
				string sr=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		//metricqitem
		public  ArrayList FindAllCylinders_WEB(string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			//issue #137 start
			//backup
			//string query="select  PartNo,Series,B_Code, Bore, R_Code, Rod, StrokeCode, Stroke, M_code, Mount, RE_Code,RodEnd,Cu_Code,Cushion,CushionPos_Code,CushionPos,ApplicationOPtID,Sel_Code,Seal,Port_Code,Port,PP_Code,PortPos,PopularOptID,Special_ID,Quantity,UnitPrice,TotalPrice,Cusomer_ID,Quotation_No,Q_Date,User_ID,Note,AWeight from WEB_Quotation_Items_TableV1 where Quotation_No='"+qno.ToString().Trim()+"' order by SL_No"; 
			//update
			//issue #242 start
			//backup
			//string query="select  PartNo,Series,B_Code, Bore, R_Code, Rod, StrokeCode, Stroke, M_code, Mount, RE_Code,RodEnd,Cu_Code,Cushion,CushionPos_Code,CushionPos,ApplicationOPtID,Sel_Code,Seal,Port_Code,Port,PP_Code,PortPos,PopularOptID,Special_ID,Quantity,UnitPrice,TotalPrice,Cusomer_ID,Quotation_No,Q_Date,User_ID,Note,AWeight,S_Code from WEB_Quotation_Items_TableV1 where Quotation_No='"+qno.ToString().Trim()+"' order by SL_No"; 
			//update
			string query="select  PartNo,Series,B_Code, Bore, R_Code, Rod, StrokeCode, Stroke, M_code, Mount, RE_Code,RodEnd,Cu_Code,Cushion,CushionPos_Code,CushionPos,ApplicationOPtID,Sel_Code,Seal,Port_Code,Port,PP_Code,PortPos,PopularOptID,Special_ID,Quantity,UnitPrice,TotalPrice,Cusomer_ID,Quotation_No,Q_Date,User_ID,Note,AWeight,S_Code,Units, B_Code, R_Code, StrokeCode from WEB_Quotation_Items_TableV1 where Quotation_No='"+qno.ToString().Trim()+"' order by SL_No"; 
			//issue #242 end
			//issue #137 end
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list=new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
				
				while(reader.Read())
				{
					QItems Qitem=new QItems();
					Qitem.PartNo=reader.GetValue(0).ToString();
					Qitem.Series=reader.GetValue(1).ToString();
					Qitem.B_Code=reader.GetValue(2).ToString();
					Qitem.Bore="("+reader.GetValue(2).ToString()+") "+reader.GetValue(3).ToString();
					Qitem.Rod="("+reader.GetValue(4).ToString()+") "+reader.GetValue(5).ToString();
					Qitem.Stroke="("+reader.GetValue(6).ToString()+") "+reader.GetValue(7).ToString();
					Qitem.Mount="("+reader.GetValue(8).ToString()+") "+reader.GetValue(9).ToString();
					Qitem.RodEnd="("+reader.GetValue(10).ToString()+") "+reader.GetValue(11).ToString();
					Qitem.Cushion="("+reader.GetValue(12).ToString()+") "+reader.GetValue(13).ToString();
					Qitem.CushionPos="("+reader.GetValue(14).ToString()+") "+reader.GetValue(15).ToString();
					Qitem.ApplicationOpt_Id =reader.GetValue(16).ToString();
					Qitem.Seal="("+reader.GetValue(17).ToString()+") "+reader.GetValue(18).ToString();
					Qitem.Port="("+reader.GetValue(19).ToString()+") "+reader.GetValue(20).ToString();
					Qitem.PortPos="("+reader.GetValue(21).ToString()+") "+reader.GetValue(22).ToString();
					Qitem.PopularOpt_Id=reader.GetValue(23).ToString();
					Qitem.Special_ID=reader.GetValue(24).ToString();
					Qitem.Quantity=reader.GetValue(25).ToString();
					Qitem.UnitPrice=reader.GetValue(26).ToString();
					Qitem.TotalPrice=reader.GetValue(27).ToString();
					Qitem.Cusomer_ID=reader.GetValue(28).ToString();
					Qitem.Quotation_No=reader.GetValue(29).ToString();
					Qitem.Q_Date=reader.GetValue(30).ToString();
					Qitem.User_ID=reader.GetValue(31).ToString();
					Qitem.Note=reader.GetValue(32).ToString();
					Qitem.Weight=reader.GetValue(33).ToString();
					//issue #137 start
					Qitem.S_Code=reader.GetValue(34).ToString();
					//issue #137 end
					//issue #242 start
					Qitem.Units=reader.GetValue(35).ToString();
                    Qitem.B_Code=reader.GetValue(36).ToString();
					Qitem.R_Code=reader.GetValue(37).ToString();
					Qitem.Stroke_Code=reader.GetValue(38).ToString();
					//issue #242 end
					list.Add(Qitem);
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				string s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList FindAllAppicationOptions_WEB(string qno,string pno)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select AppoptCode,AppOpts from WEB_Application_Opts_Items_TableV1 where QuoteNo ='"+qno.ToString().Trim()+"' and  PartNo ='"+pno.ToString().Trim()+"'  order by AppoptCode";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			 
				while(reader.Read())
				{
					list.Add("("+reader.GetValue(0)+") "+reader.GetValue(1));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList FindAllPopularOptions_WEB(string qno,string pno)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "select PopoptsCode,PopOpts from WEB_Popular_Opts_Items_TableV1 where QuoteNo ='"+qno.ToString().Trim()+"' and  PartNo ='"+pno.ToString().Trim()+"' order by PopoptsCode";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add("("+reader.GetValue(0)+") "+reader.GetValue(1));
		
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		//issue #670 start
		public ArrayList SelectAppPopOpts_ICyl(string pno,string qno,string spname)
		{  
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.StoredProcedure;
			command.CommandText= spname.Trim();
			command.Parameters.Add("@parm1",pno.Trim());
			command.Parameters.Add("@parm2",qno.Trim());
			SqlDataReader reader =null;
			ArrayList list = new ArrayList(); 
			try
			{
				IDisposable connection=null;connection = GetConnection();command.Connection = (SqlConnection)connection;
				reader = command.ExecuteReader(CommandBehavior.CloseConnection);
				while(reader.Read())
				{
					ExtraClass opt =new ExtraClass();
					opt.Code =reader.GetValue(0).ToString();
					opt.Desc=reader.GetValue(1).ToString();
					opt.Price=reader.GetValue(2).ToString();
					list.Add(opt);
				}
				reader.Close();
				Close();
			}
			finally
			{
				if (reader != null)
				{
					reader.Close();
				}
				Close();
			}
			return list;
		}
		//issue #670 end
		public  ArrayList FindAllSpecials_WEB(string qno,string pno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select  Special_Description from WEB_Specials_TableV1 where PartNo='"+pno.ToString().Trim()+"' and QuoteNo='"+qno.ToString().Trim()+"' order by SpNo"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
				
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList  FindALlQtyBreaks_WEB(string pno,string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select * from WEB_Qty_Breaks_TableV1 where  PartNo= '"+pno.Trim()+"' and QuoteNo='"+qno.Trim()+"' order by slno"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList temp=new ArrayList();
			ArrayList t1=new ArrayList();
			ArrayList t2=new ArrayList();
			ArrayList t3=new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
				decimal dc1=0.00m;
				decimal dc2=0.00m;
				decimal dc3=0.00m;
				while(reader.Read())
				{
					dc1=Convert.ToDecimal(reader.GetValue(3).ToString());
					dc2=Convert.ToDecimal(reader.GetValue(4).ToString());
					dc3=dc1 * dc2;
					t1.Add(String.Format("{0:###}", dc1));
					t2.Add(String.Format("{0:$###,###.00}", dc2));
					t3.Add(String.Format("{0:$###,###.00}", dc3));
				}
				temp.Add(t1);
				temp.Add(t2);
				temp.Add(t3);
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				string str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return temp;
		}
		//metricinput
		public string  Insert_IntoCrystalReoprtTable_WEB(CrystalReportClass crc)
		{  
			//issue #242 start
			//back
//			string query = "INSERT INTO WEB_CrystalReport_Template_TableV1 (QuoteNo,PartNo,Description,Quantity,UnitPrice,TotalPrice,QtyBreaks_Qty,QtyBreaks_Uprice,QtyBreaks_Tprice,ItemPrice,Discount,Note,Weight) values"
//				+"(@qno,@pno,@desc,@qty,@unitp,@total,@qtybrks1,@qtybrks2,@qtybrks3,@itemp,@discount,@note,@weight)";
			//update
			string query = "INSERT INTO WEB_CrystalReport_Template_TableV1 (QuoteNo,PartNo,Description,Quantity,UnitPrice,TotalPrice,QtyBreaks_Qty,QtyBreaks_Uprice,QtyBreaks_Tprice,ItemPrice,Discount,Note,Weight,Units) values"
				+"(@qno,@pno,@desc,@qty,@unitp,@total,@qtybrks1,@qtybrks2,@qtybrks3,@itemp,@discount,@note,@weight,@units)";
			//issue #242 end
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@qno",crc.QuoteNo.Trim());
			command.Parameters.Add("@pno",crc.PartNo.Trim());
			command.Parameters.Add("@desc",crc.Desc.Trim());
			command.Parameters.Add("@unitp",crc.UnitPrice.Trim());
			command.Parameters.Add("@qty",crc.Qty.Trim());
			command.Parameters.Add("@total",crc.Price.Trim());
			command.Parameters.Add("@qtybrks1",crc.Qtybreaks_qty.Trim());
			command.Parameters.Add("@qtybrks2",crc.Qtybreaks_uprice.Trim());
			command.Parameters.Add("@qtybrks3",crc.Qtybreaks_tprice.Trim());
			command.Parameters.Add("@itemp",crc.Itemprice.Trim());
			command.Parameters.Add("@discount",crc.Discount.Trim());
			command.Parameters.Add("@note",crc.Note.Trim());
			command.Parameters.Add("@weight",crc.Weight.Trim());
			//issue #242 start
			command.Parameters.Add("@units",crc.Units.Trim());
			//issue #242 end
			string s="";
			try
			{
				command.Connection = GetConnection();
				s=command.ExecuteNonQuery().ToString();
				Close();
			}
			catch (Exception ex)
			{		
				string sr=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public ArrayList  FindAllComments_WEB(string qno)
		{  
		
			string query = "Select Comments from WEB_Comment_TableV1 where QuoteNo='"+qno.Trim()+"' order by SlNo ASC";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList lst =new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					lst.Add(reader.GetValue(0).ToString());
				
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				lst.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			
			return lst;
		}
		public  ArrayList FindAllZ101_WEB(string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select * from WEB_Z101_TableV1 Where QuoteNo='"+qno.Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list=new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
				while(reader.Read())
				{
					Others oth=new Others();
					oth.Code=reader.GetValue(1).ToString();
					oth.Desc=reader.GetValue(3).ToString();
					oth.UnitP=reader.GetValue(4).ToString();
					oth.Qty=reader.GetValue(5).ToString();
					oth.Price=reader.GetValue(6).ToString();
					list.Add(oth);
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				string sa=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public string  Insert_CommentsIntoCrystalReoprtTable_WEB(string qno,string comment)
		{  
	
			string query = "INSERT INTO WEB_CrystalReport_Comment_TabeV1 (QuoteNo,Comments) values(@qno,@comment)";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@qno",qno.Trim());
			command.Parameters.Add("@comment",comment.Trim());
			string s="";
			try
			{
				command.Connection = GetConnection();
				s=command.ExecuteNonQuery().ToString();
				Close();
			}
			catch (Exception ex)
			{		
				string sr=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public  ArrayList FindAllRepairkits_WEB(string code)
		{
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.StoredProcedure;
			command.CommandText= "sp_Select_RkitsByQno_WEB";
			command.Parameters.Add("@parm1",code.Trim());
			ArrayList list = new ArrayList();
			try
			{
				Rkits other=new Rkits();
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
				while(reader.Read())
				{
					other=new Rkits();
					other.Slno=reader.GetValue(0).ToString();
					other.Qno=reader.GetValue(1).ToString();
					other.PartNo =reader.GetValue(2).ToString();
					other.S_Code =reader.GetValue(3).ToString();
					other.Series =reader.GetValue(4).ToString();
					other.S_Price =reader.GetValue(5).ToString();
					other.B_Code =reader.GetValue(6).ToString();
					other.Bore  =reader.GetValue(7).ToString();
					other.B_Price  =reader.GetValue(8).ToString();
					other.R_Code =reader.GetValue(9).ToString();
					other.Rod=reader.GetValue(10).ToString();
					other.R_Price  =reader.GetValue(11).ToString();
					other.C_Code =reader.GetValue(12).ToString();
					other.Cushion =reader.GetValue(13).ToString();
					other.C_Price  =reader.GetValue(14).ToString();
					other.Sel_Code =reader.GetValue(15).ToString();
					other.Seal =reader.GetValue(16).ToString();
					other.Sel_price  =reader.GetValue(17).ToString();
					other.SecR_Code =reader.GetValue(18).ToString();
					other.SecRod =reader.GetValue(19).ToString();
					other.SecR_Price  =reader.GetValue(20).ToString();
					other.RodS_Code =reader.GetValue(21).ToString();
					other.RodSeal =reader.GetValue(22).ToString();
					other.RodS_Price  =reader.GetValue(23).ToString();
					other.PistonS_Code =reader.GetValue(24).ToString();
					other.PistonSeal =reader.GetValue(25).ToString();
					other.PistonS_Price  =reader.GetValue(26).ToString();
					other.MS_Code =reader.GetValue(27).ToString();
					other.MetalScrapper =reader.GetValue(28).ToString();
					other.MetalS_Price  =reader.GetValue(29).ToString();
					other.GD_Code =reader.GetValue(30).ToString();
					other.GlandDrain =reader.GetValue(31).ToString();
					other.Gland_Price  =reader.GetValue(32).ToString();
					other.Multi_code =reader.GetValue(33).ToString();
					other.MultiCylinder =reader.GetValue(34).ToString();
					other.Multi_Price  =reader.GetValue(35).ToString();
					other.NetPrice =reader.GetValue(36).ToString();
					other.Qty =reader.GetValue(37).ToString();
					other.Discount =reader.GetValue(38).ToString();
					other.TPrice =reader.GetValue(39).ToString();
					other.Note =reader.GetValue(40).ToString();
					list.Add(other);
				}
				reader.Close();
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public  ArrayList FindAllAccessories_WEB(string qno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select  PartNo,Description,ListPrice,Quantity,TotalPrice,Discount from WEB_Accessory_Item_TableV1 where AccessoryID='"+qno.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					Others other=new Others();
					other.Code =reader.GetValue(0).ToString();
					other.Desc =reader.GetValue(1).ToString();
					other.UnitP  =reader.GetValue(2).ToString();
					other.Qty =reader.GetValue(3).ToString();
					other.Price =reader.GetValue(4).ToString();
					other.Discount =reader.GetValue(5).ToString();
					list.Add(other);
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public void RemoveFiles(string strPath)
		{			 
			System.IO.DirectoryInfo di = new DirectoryInfo(strPath);
			FileInfo[] fiArr = di.GetFiles();
			foreach (FileInfo fri in fiArr)
			{
				if(fri.Extension.ToString() ==".pdf")
				{
					fri.Delete();
				}
			}
           
		}
		public  decimal SelectISSMSSPrice(string mount)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select Price from WEB_Aprice_IsoMssAdder_TableV1 where Mount='"+mount.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			decimal str=0.00m;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				if(reader.Read())
				{
					str =Convert.ToDecimal(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{	
				string str1= ex.Message.ToString();
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return str;
		}
		public  decimal SelectGRPrice(string mount)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select Price from WEB_Aprice_GRAdder_TableV1 where Mount='"+mount.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			decimal str=0.00m;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				if(reader.Read())
				{
					str =Convert.ToDecimal(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{	
				string str1= ex.Message.ToString();
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return str;
		}
		public  string SelectSeriesNPressure(string code,string mount)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select  "+mount.Trim()+"  from  WEB_SeriesNPressure_TableV1 where Code='"+code.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string str="";
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				if(reader.Read())
				{ 
					str =reader.GetValue(0).ToString();
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return str;
		}
		public  string SelectSeriesMMLPressure(string code,string mount)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select  "+mount.Trim()+"  from  WEB_SeriesMMLPressure_TableV1 where Code='"+code.ToString().Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string str="";
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				if(reader.Read())
				{ 
					str =(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				str=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return str;
		}
		public  string SelectLastSpNo()
		{  		
			string query="select SPNo from WEB_Special_Count_TableV1 where Slno=1"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string s ="";
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				if(reader.Read())
				{
					s=reader.GetValue(0).ToString();
				}
				reader.Close();
				Close();
			}
			catch (Exception ex)
			{		
				s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public string  InsertCustomerSpecials(string code,string qno,string pno,string  pname,string desc,string unitcost,string qty,string markup,string total,string order)
		{  
			string query = "INSERT INTO WEB_Specials_TableV1( SpNo,QuoteNo,PartNo, Special_PartName, Special_Description, UnitCost, Quantity, MarkUp, TotalCost,Ordr)"
				+"VALUES (@spno,@qno,@pno,@spname,@desc,@uprice,@qty,@markup,@total,@ordr)";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@spno",code.Trim());
			command.Parameters.Add("@qno",qno.Trim());
			command.Parameters.Add("@pno",pno.Trim());
			command.Parameters.Add("@spname",pname.Trim());
			command.Parameters.Add("@desc",desc.Trim());
			command.Parameters.Add("@uprice",unitcost.Trim());
			command.Parameters.Add("@qty",qty.Trim());
			command.Parameters.Add("@markup",markup.Trim());
			command.Parameters.Add("@total",total.Trim());
			command.Parameters.Add("@ordr",order.Trim());
			string s="";
			try
			{
				command.Connection = GetConnection();
				s=command.ExecuteNonQuery().ToString();
				Close();
			}
			catch (Exception ex)
			{		
				s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public  string  SelectSTDDrawing(string qno,string pno)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select Drawing from WEB_Quotation_Items_TableV1 where Quotation_No='"+qno.Trim()+"'  and PartNo='"+pno.Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string ss="";
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
				if(reader.Read())
				{
					ss =reader.GetValue(0).ToString();
				}
				reader.Close();
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return ss;
		}
		//issue #107 start
		public  string  SelectQno_BySoNo_ByPaNo(string sono,string pano)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select QuoteNo from WEB_Backlog_TableV1 where SoNo='"+sono.Trim()+"'  and PartNo='"+pano.Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string ss="";
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
				if(reader.Read())
				{
					ss =reader.GetValue(0).ToString();
				}
				reader.Close();
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return ss;
		}
		//issue #107 end
		public  string  SelectDimDrawing(string tab,string bore,string rod)
		{  		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query="select A from "+tab.Trim()+" where BoreSize='"+bore.Trim()+"'  and RodSize='"+rod.Trim()+"'"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string ss="";
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
				if(reader.Read())
				{
					ss =reader.GetValue(0).ToString();
				}
				reader.Close();
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return ss;
		}
		public string  UpdateDrawing(string qno,string pno)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = "Update WEB_Quotation_Items_TableV1 set Drawing =1 where Quotation_No= '"+qno.Trim()+"' and PartNo='"+pno.Trim()+"'";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			string s="";
			try
			{
				command.Connection = GetConnection();
				s=command.ExecuteNonQuery().ToString();
				Close();
			}
			catch (Exception ex)
			{		
				s=("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public string SelectOneValueFunction(string parm1,string spname)
		{  
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.StoredProcedure;
			command.CommandText= spname.Trim();
			command.Parameters.Add("@parm1",parm1.Trim());
			string str="";
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
				if(reader.Read())
				{
					str =(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return str;
		}
		public ArrayList Select_Mount_Thrust(string query1)
		{  
		
			//  SqlConnection SqlCon =new SqlConnection(constr);
			string query = query1.ToString();
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			ArrayList list = new ArrayList(); 
			ArrayList list1 = new ArrayList(); 
			ArrayList list2 = new ArrayList(); 
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
			
				while(reader.Read())
				{
					list1.Add(reader.GetValue(0));
					list2.Add(reader.GetValue(1));
		
				}
				reader.Close();
				Close();
				list.Add(list1);
				list.Add(list2);
			}
			catch (Exception ex)
			{		
				list.Add("Error:  " + ex.Message.ToString() + "  :: " + ex.StackTrace);
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return list;
		}
		public ArrayList Select_DataFrom_ImportFile(string filename)
		{  
			string constr="Provider=Microsoft.Jet.OLEDB.4.0;Data Source="+filename.ToString().Trim()
				+";Extended Properties='Excel 8.0;HDR=Yes;IMEX=2'";
			//issue #137 start
			//backup
//			string CommandText = "select [Line No#],[Velan Tag#],[Required Qty],[Style],[Cyl# Stroke],[Minimum Air Supply (Psi)],[Break To Open],[Run To Open]"
//				+" ,[End To Open],[Break To Close],[Run To Close],[End To Close],[Safety Factor (%)],[Seals],[Paint],[Mount],[Ratings],[Pneumatic Control Package]"
//				+" ,[Limit Switch],[Manual Override],[Closing Time],[Notes]  from [CylinderData$] Where [Line No#] >0";    
			//update
//			string CommandText = "select [Line No#],[Velan Tag#],[Required Qty],[Style],[Cyl# Stroke],[Minimum Air Supply (Psi)],[Break To Open],[Run To Open]"
//				+" ,[End To Open],[Break To Close],[Run To Close],[End To Close],[Seals],[Paint],[Mount],[Ratings],[Pneumatic Control Package]"
//				+" ,[Limit Switch],[Manual Override],[Closing Time],[Notes]  from [CylinderData$] Where [Line No#] >0";    
//			string CommandText = "select [Line No#],[Tag#],[Required Qty],[Style],[Cyl# Stroke],[Minimum Air Supply (Psi)],[Break To Open],[Run To Open]"
//				+" ,[End To Open],[Break To Close],[Run To Close],[End To Close],[Safety Factor (%)],[Seals],[Paint],[Mount],[Ratings],[Pneumatic Control Package]"
//				+" ,[Limit Switch],[Manual Override],[Closing Time],[Notes]  from [CylinderData$] Where [Line No#] >0";    
			string CommandText = "select [Line No#],[Tag#],[Required Qty],[Style],[Cyl# Stroke (in)],[Minimum Air Supply (psi)],[Break To Open (lbs)],[Run To Open (lbs)]"
				+" ,[End To Open (lbs)],[Break To Close (lbs)],[Run To Close (lbs)],[End To Close (lbs)],[Safety Factor (%)],[Seals],[Paint],[Mount],[Ratings],[Pneumatic Control Package]"
				+" ,[Limit Switch],[Manual Override],[Closing Time (s)],[Notes]  from [CylinderData$] Where [Line No#] >0";    
			//issue #137 end
			OleDbConnection myConnection = new OleDbConnection(constr);
			OleDbCommand myCommand = new OleDbCommand(CommandText, myConnection);    			
			ArrayList list = new ArrayList(); 
			VelanClass pn=new VelanClass();
			try
			{
				myConnection.Open();    
				OleDbDataReader reader = myCommand.ExecuteReader();			
				while(reader.Read())
				{
					pn=new VelanClass();
					pn.Slno=reader.GetValue(0).ToString();
					pn.VelanTag=reader.GetValue(1).ToString();
					pn.Qty=reader.GetValue(2).ToString();
					pn.Style=reader.GetValue(3).ToString();
					pn.Stroke=reader.GetValue(4).ToString();
					pn.MinAirSupply=reader.GetValue(5).ToString();
					pn.BTO=reader.GetValue(6).ToString();
					pn.RTO=reader.GetValue(7).ToString();
					pn.ETO=reader.GetValue(8).ToString();
					pn.BTC=reader.GetValue(9).ToString();
					pn.RTC=reader.GetValue(10).ToString();
					pn.ETC=reader.GetValue(11).ToString();
					pn.SaftyFactor=reader.GetValue(12).ToString();
					pn.Seals=reader.GetValue(13).ToString();
					pn.Paint=reader.GetValue(14).ToString();
					pn.Mount=reader.GetValue(15).ToString();
					pn.Ratings=reader.GetValue(16).ToString();
					pn.PneumaticP=reader.GetValue(17).ToString();
					pn.LimitSwitch=reader.GetValue(18).ToString();					
					pn.ManualOverride=reader.GetValue(19).ToString();
					pn.ClosingTime=reader.GetValue(20).ToString();
					pn.Notes=reader.GetValue(21).ToString();
//					pn.Seals=reader.GetValue(12).ToString();
//					pn.Paint=reader.GetValue(13).ToString();
//					pn.Mount=reader.GetValue(14).ToString();
//					pn.Ratings=reader.GetValue(15).ToString();
//					pn.PneumaticP=reader.GetValue(16).ToString();
//					pn.LimitSwitch=reader.GetValue(17).ToString();					
//					pn.ManualOverride=reader.GetValue(18).ToString();
//					pn.ClosingTime=reader.GetValue(19).ToString();
//					pn.Notes=reader.GetValue(20).ToString();
					pn.EtcValveTrust=pn.ETC;
					list.Add(pn);
				}
				reader.Close();
				myConnection.Close();
			}
			finally
			{
				myConnection.Close();
			}
			return list;
		}
		//issue #242 start
		public DataSet ExcelValidation_Get_AS(string filename)
		{
			string strVersion = ExcelVersion_Get(filename);
			string dml = "SELECT CoderHdr, ASValidation "
				+" FROM [Excel_Template_Dic] Where Version = @version Order by DicId;";
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@version",Convert.ToInt32(strVersion));
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;			
				oledad.Fill(ds);
				Close();
			}			
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public DataSet ExcelValidation_Get_A(string filename)
		{
			string strVersion = ExcelVersion_Get(filename);
			string dml = "SELECT CoderHdr, AValidation "
				+" FROM [Excel_Template_Dic] Where Version = @version Order by DicId;";
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@version",Convert.ToInt32(strVersion));
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;			
				oledad.Fill(ds);
				Close();
			}			
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public DataSet ExcelValidation_Get(string filename)
		{
			string strVersion = ExcelVersion_Get(filename);
			string dml = "SELECT CoderHdr, Validation "
				+" FROM [Excel_Template_Dic] Where Version = @version Order by DicId;";
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@version",Convert.ToInt32(strVersion));
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;			
				oledad.Fill(ds);
				Close();
			}			
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public DataSet ExcelHdr_Get(string version)
		{
			string dml = "SELECT ExcelHdr, CoderHdr, Units "
				+" FROM [Excel_Template_Dic] Where Version=@version Order by DicId;";
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@version",Convert.ToInt32(version));
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;			
				oledad.Fill(ds);
				Close();
			}			
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public string ExcelVersion_Get(string filename)
		{
			string strVersion = "0";
			string constr="Provider=Microsoft.Jet.OLEDB.4.0;Data Source="+filename.ToString().Trim()
				+";Extended Properties='Excel 8.0;HDR=Yes;IMEX=2'";
			string CommandText = "SELECT Version FROM [DataBase$]";    
			OleDbConnection myConnection = new OleDbConnection(constr);
			OleDbCommand myCommand = new OleDbCommand(CommandText, myConnection);    	
			try
			{
				myConnection.Open();    
				OleDbDataReader reader = myCommand.ExecuteReader();			
				if(reader.Read())
				{
					strVersion=reader.GetValue(0).ToString();
				}
				reader.Close();
				myConnection.Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return strVersion;
		}
		public DataSet ExcelData_Get(string filename)
		{
			string strVersion = "0";
			string constr="Provider=Microsoft.Jet.OLEDB.4.0;Data Source="+filename.ToString().Trim()
				+";Extended Properties='Excel 8.0;HDR=Yes;IMEX=2'";
			string CommandText = "SELECT * FROM [CylinderData$]  Where [Line No#] >0";    
			OleDbDataAdapter dad = new OleDbDataAdapter(CommandText, constr);
			DataSet ds =new DataSet();
			try
			{
				dad.Fill(ds);
				Close();
			}			
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public ArrayList Select_DataFrom_ImportFile_All(string filename)
		{  
			string strVersion = "0";
			string constr="Provider=Microsoft.Jet.OLEDB.4.0;Data Source="+filename.ToString().Trim()
				+";Extended Properties='Excel 8.0;HDR=Yes;IMEX=2'";
			//			string CommandText = "SELECT *  FROM [Database$];SELECT * FROM [CylinderData$]";    
			string CommandText1 = "SELECT Version FROM [Database$]";    
			string CommandText2 = "SELECT * FROM [CylinderData$]  Where [Line No#] >0";    
			OleDbDataAdapter dad1 = new OleDbDataAdapter(CommandText1, constr);
			OleDbDataAdapter dad2 = new OleDbDataAdapter(CommandText2, constr);
			DataSet ds1 =new DataSet();
			DataSet ds2 =new DataSet();
			try
			{
				dad1.Fill(ds1);
				dad2.Fill(ds2);
				//Close();
			}			
//			finally
//			{
//				//SqlCon.Close();
//				//Close();
//			}
			
			finally
			{
				
			}
			DataSet dsDic = ExcelHdr_Get(ds1.Tables[0].Rows[0][0].ToString());
			ArrayList list = new ArrayList(); 
			VelanClass pn=new VelanClass();
			foreach(DataRow drExcel in ds2.Tables[0].Rows)
			{
				pn=new VelanClass();
				foreach(DataRow dr in dsDic.Tables[0].Rows)
				{
					switch(dr[1].ToString())
					{
						
						case "Slno":
						    pn.Slno = drExcel[dr[0].ToString()].ToString();
						    break;
						case "VelanTag":
							pn.VelanTag = drExcel[dr[0].ToString()].ToString();
							break; 
						case "Qty":
							pn.Qty = drExcel[dr[0].ToString()].ToString();
							break; 
						case "Style":
							pn.Style = drExcel[dr[0].ToString()].ToString();
							break; 
						case "Stroke":
							pn.Stroke = drExcel[dr[0].ToString()].ToString();
							break; 
						case "MinAirSupply":
							pn.MinAirSupply = drExcel[dr[0].ToString()].ToString();
							break; 
						case "BTO":
							pn.BTO = drExcel[dr[0].ToString()].ToString();
							break; 
						case "RTO":
							pn.RTO = drExcel[dr[0].ToString()].ToString();
							break; 
						case "ETO":
							pn.ETO = drExcel[dr[0].ToString()].ToString();
							break; 
						case "BTC":
							pn.BTC = drExcel[dr[0].ToString()].ToString();
							break; 
						case "RTC":
							pn.RTC = drExcel[dr[0].ToString()].ToString();
							break; 
						case "ETC":
							pn.ETC = drExcel[dr[0].ToString()].ToString();
							break; 
						case "SaftyFactor":
							pn.SaftyFactor = drExcel[dr[0].ToString()].ToString();
							break; 
						case "Seals":
							pn.Seals = drExcel[dr[0].ToString()].ToString();
							break; 
						case "Paint":
							pn.Paint = drExcel[dr[0].ToString()].ToString();
							break; 
						case "Mount":
							pn.Mount = drExcel[dr[0].ToString()].ToString();
							break; 
						case "Ratings":
							pn.Ratings = drExcel[dr[0].ToString()].ToString();
							break; 
						case "PneumaticP":
							pn.PneumaticP = drExcel[dr[0].ToString()].ToString();
							break; 
						case "LimitSwitch":				
							pn.LimitSwitch = drExcel[dr[0].ToString()].ToString();
							break; 
						case "ManualOverride":
							pn.ManualOverride = drExcel[dr[0].ToString()].ToString();
							break; 
						case "ClosingTime":
							pn.ClosingTime = drExcel[dr[0].ToString()].ToString();
							break; 
						case "Notes":
							pn.Notes = drExcel[dr[0].ToString()].ToString();
							break;
						case "PistonRod":
							pn.PistonRod = drExcel[dr[0].ToString()].ToString();
							break;
					}
				}
				pn.Units = dsDic.Tables[0].Rows[0][2].ToString();
				if(pn.Units=="metric")
				{
					//if(pn.Stroke.ToString().Trim()!="") pn.Stroke = Convert.ToString(Convert.ToDecimal(pn.Stroke)*.0394m);
					if(pn.Stroke.ToString().Trim()!="") pn.Stroke = Convert.ToString(Convert.ToDecimal(pn.Stroke)*.039369m);
					if(pn.MinAirSupply.ToString().Trim()!="") pn.MinAirSupply=Convert.ToString(Convert.ToDecimal(pn.MinAirSupply)*14.5m);
//					if(pn.BTO.ToString().Trim()!="") pn.BTO=Convert.ToString(Math.Round(Convert.ToDecimal(pn.BTO)*0.2248m));
//                    if(pn.RTO.ToString().Trim()!="") pn.RTO=Convert.ToString(Math.Round(Convert.ToDecimal(pn.RTO)*0.2248m));
//					if(pn.ETO.ToString().Trim()!="") pn.ETO=Convert.ToString(Math.Round(Convert.ToDecimal(pn.ETO)*0.2248m));
//					if(pn.BTC.ToString().Trim()!="") pn.BTC=Convert.ToString(Math.Round(Convert.ToDecimal(pn.BTC)*0.2248m));
//					if(pn.RTC.ToString().Trim()!="") pn.RTC=Convert.ToString(Math.Round(Convert.ToDecimal(pn.RTC)*0.2248m));
//					if(pn.ETC.ToString().Trim()!="") pn.ETC=Convert.ToString(Math.Round(Convert.ToDecimal(pn.ETC)*0.2248m));
					if(pn.BTO.ToString().Trim()!="") pn.BTO=Convert.ToString(Convert.ToDecimal(pn.BTO)*0.2248m);
					if(pn.RTO.ToString().Trim()!="") pn.RTO=Convert.ToString(Convert.ToDecimal(pn.RTO)*0.2248m);
					if(pn.ETO.ToString().Trim()!="") pn.ETO=Convert.ToString(Convert.ToDecimal(pn.ETO)*0.2248m);
					if(pn.BTC.ToString().Trim()!="") pn.BTC=Convert.ToString(Convert.ToDecimal(pn.BTC)*0.2248m);
					if(pn.RTC.ToString().Trim()!="") pn.RTC=Convert.ToString(Convert.ToDecimal(pn.RTC)*0.2248m);
					if(pn.ETC.ToString().Trim()!="") pn.ETC=Convert.ToString(Convert.ToDecimal(pn.ETC)*0.2248m);

				}
				pn.EtcValveTrust=pn.ETC;
				list.Add(pn);
			}
			return list;
		}

		public string  Insert_Excel_Template_Hdr(string version, string excelhdr)
		{  
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.Text;
			command.CommandText= " set DateFormat mdy; "
				+" INSERT INTO Excel_Template_Dic (Version, ExcelHdr) "
				+" values(@version,@excelhdr) ";
			command.Parameters.Add("@version",Convert.ToInt16(version));
			command.Parameters.Add("@excelhdr",excelhdr.Trim());
			string s="";
			try
			{
				IDisposable connection=null;connection = GetConnection();command.Connection = (SqlConnection)connection;
				s=command.ExecuteNonQuery().ToString();
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public void Excel_Template_Save_Hdr(string filename)
		{
			string strVersion = "0";
			string constr="Provider=Microsoft.Jet.OLEDB.4.0;Data Source="+filename.ToString().Trim()
				+";Extended Properties='Excel 8.0;HDR=Yes;IMEX=2'";
//			string CommandText = "SELECT *  FROM [Database$];SELECT * FROM [CylinderData$]";    
			string CommandText1 = "SELECT Version FROM [Database$]";    
			string CommandText2 = "SELECT * FROM [CylinderData$]";    
			OleDbDataAdapter dad1 = new OleDbDataAdapter(CommandText1, constr);
			OleDbDataAdapter dad2 = new OleDbDataAdapter(CommandText2, constr);
			DataSet ds1 =new DataSet();
			DataSet ds2 =new DataSet();
			try
			{
				dad1.Fill(ds1);
				dad2.Fill(ds2);
				Close();
			}			
			finally
			{
				SqlCon.Close();
			}
			foreach(DataColumn dc in ds2.Tables[0].Columns)
			{
              Insert_Excel_Template_Hdr(ds1.Tables[0].Rows[0][0].ToString(), dc.ColumnName.ToString().Trim());

			}

			
		}
		//issue #242 end
		public ArrayList Select_DataFrom_ImportFile_Rotork(string filename)
		{  
			string constr="Provider=Microsoft.Jet.OLEDB.4.0;Data Source="+filename.ToString().Trim()
				+";Extended Properties='Excel 8.0;HDR=Yes;IMEX=2'";
			string CommandText = "select [Line No#],[Required Qty],[Style],[Hydraulic Or Pneumatic],[Cyl# Stroke],[Cyl# Bore],[Minimum Operating Pressure (Psi)],[Break To Open (LBS)],[Run To Open (LBS)]"
				+" ,[End To Open (LBS)],[Break To Close (LBS)],[Run To Close (LBS)],[End To Close (LBS)],[Safety Factor (%)],[Seals],[Rod End],[Rod Boot],[Paint],[Manual Override],[Lifting Lugs],[Customer Ref#] from [CylinderData$] Where [Line No#] >0 ";    
			OleDbConnection myConnection = new OleDbConnection(constr);
			OleDbCommand myCommand = new OleDbCommand(CommandText, myConnection);    			
			ArrayList list = new ArrayList(); 
			VelanClass pn=new VelanClass();
			try
			{
				myConnection.Open();    
				OleDbDataReader reader = myCommand.ExecuteReader();			
				while(reader.Read())
				{
					pn=new VelanClass();
					pn.Slno=reader.GetValue(0).ToString();
					pn.Qty=reader.GetValue(1).ToString();
					pn.Style=reader.GetValue(2).ToString();
					pn.PneumaticHydraulic=reader.GetValue(3).ToString();
					pn.Stroke=reader.GetValue(4).ToString();
					pn.Bore=reader.GetValue(5).ToString();
					pn.MinAirSupply=reader.GetValue(6).ToString();
					pn.BTO=reader.GetValue(7).ToString();
					pn.RTO=reader.GetValue(8).ToString();
					pn.ETO=reader.GetValue(9).ToString();
					pn.BTC=reader.GetValue(10).ToString();
					pn.RTC=reader.GetValue(11).ToString();
					pn.ETC=reader.GetValue(12).ToString();
					pn.SaftyFactor=reader.GetValue(13).ToString();
					pn.Seals=reader.GetValue(14).ToString();
					pn.RodEnd=reader.GetValue(15).ToString();
					pn.RodBoot=reader.GetValue(16).ToString();
					pn.Paint=reader.GetValue(17).ToString();
					pn.ManualOverride=reader.GetValue(18).ToString();
					pn.LiftingLugs=reader.GetValue(19).ToString();
					pn.Notes=reader.GetValue(20).ToString();	
					list.Add(pn);
				}
				reader.Close();
				myConnection.Close();
			}
			catch (OleDbException ex)
			{
				string str="";
				str=ex.Message;
			}
			finally
			{
				myConnection.Close();
			}
			return list;
		}
		public ArrayList Select_DataFrom_ImportFile_SVC_Summit(string filename)
		{  
			string constr="Provider=Microsoft.Jet.OLEDB.4.0;Data Source="+filename.ToString().Trim()
				+";Extended Properties='Excel 8.0;HDR=Yes;IMEX=2'";
			//issue #137 start
			//backup
			//string CommandText = "select [Line No#],[Required Qty],[Style],[Series],[Cylinder Stroke],[Cylinder Bore],[Thrust],[Safety Factor (%)],[Minimum Operating Pressure (Psi)],[Metallic Rod Wiper],[Customer Ref#] from [CylinderData$] Where [Line No#] >0 ";  
			//update
			//string CommandText = "select [Line No#],[Required Qty],[Style],[Series],[Cylinder Stroke],[Cylinder Bore],[Thrust],[Safety Factor (%)],[Minimum Operating Pressure (Psi)],[Metallic Rod Wiper],[Customer Ref#],[Packing Friction],[End To Open],[Break To Open] from [CylinderData$] Where [Line No#] >0 ";  
			//string CommandText = "select [Line No#],[Required Qty],[Style],[Series],[Cylinder Stroke (in)],[Cylinder Bore (in)],[Thrust (lbs)],[Safety Factor (%)],[Minimum Operating Pressure (psi)],[Metallic Rod Wiper],[Customer Ref#] from [CylinderData$] Where [Line No#] >0 ";  
			string CommandText = "select [Line No#],[Required Qty],[Style],[Series],[Cylinder Stroke (in)],[Cylinder Bore (in)],[Thrust (lbs)],[Safety Factor (%)],[Minimum Operating Pressure (psi)],[Customer Ref#] from [CylinderData$] Where [Line No#] >0 ";  
			//issue #137 end
			OleDbConnection myConnection = new OleDbConnection(constr);
			OleDbCommand myCommand = new OleDbCommand(CommandText, myConnection);    			
			ArrayList list = new ArrayList(); 
			VelanClass pn=new VelanClass();
			try
			{
				myConnection.Open();    
				OleDbDataReader reader = myCommand.ExecuteReader();			
				while(reader.Read())
				{
					pn=new VelanClass();
					pn.Slno=reader.GetValue(0).ToString();
					pn.Qty=reader.GetValue(1).ToString();
					pn.Style=reader.GetValue(2).ToString();
					pn.PneumaticHydraulic=reader.GetValue(3).ToString();
					pn.Stroke=reader.GetValue(4).ToString();
					pn.Bore=reader.GetValue(5).ToString();
					pn.Thrust=reader.GetValue(6).ToString();
					pn.SaftyFactor=reader.GetValue(7).ToString();
					pn.MinAirSupply=reader.GetValue(8).ToString();
//					pn.RodWiper=reader.GetValue(9).ToString();
//					pn.Notes=reader.GetValue(10).ToString();				
					pn.Notes=reader.GetValue(9).ToString();				
					//issue #137 start
//					pn.PackingFriction=reader.GetValue(11).ToString();
//					pn.ETO=reader.GetValue(12).ToString();
//					pn.BTO=reader.GetValue(13).ToString();				
					//issue #137 end
					list.Add(pn);
				}
				reader.Close();
				myConnection.Close();
			}
			catch(OleDbException ex)
			{
				string sss="";
				sss=ex.Message;
			}
			finally
			{
				myConnection.Close();
			}
			return list;
		}
		///Red Valve Start
		public ArrayList Select_DataFrom_ImportFile_RedValve(string filename)
		{  
			string constr="Provider=Microsoft.Jet.OLEDB.4.0;Data Source="+filename.ToString().Trim()
				+";Extended Properties='Excel 8.0;HDR=Yes;IMEX=2'";
			string CommandText = "select [Line No#],[Required Qty],[Style],[Series],[Cylinder Stroke],[Cylinder Bore],[Thrust],[Safety Factor (%)],[Minimum Operating Pressure (Psi)],[Seals],[Metallic Rod Wiper],[Customer Ref#] from [CylinderData$] Where [Line No#] >0 ";  
			OleDbConnection myConnection = new OleDbConnection(constr);
			OleDbCommand myCommand = new OleDbCommand(CommandText, myConnection);    			
			ArrayList list = new ArrayList(); 
			VelanClass pn=new VelanClass();
			try
			{
				myConnection.Open();    
				OleDbDataReader reader = myCommand.ExecuteReader();			
				while(reader.Read())
				{
					pn=new VelanClass();
					pn.Slno=reader.GetValue(0).ToString();
					pn.Qty=reader.GetValue(1).ToString();
					pn.Style=reader.GetValue(2).ToString();
					pn.PneumaticHydraulic=reader.GetValue(3).ToString();
					pn.Stroke=reader.GetValue(4).ToString();
					pn.Bore=reader.GetValue(5).ToString();
					pn.Thrust=reader.GetValue(6).ToString();
					pn.SaftyFactor=reader.GetValue(7).ToString();
					pn.MinAirSupply=reader.GetValue(8).ToString();
					pn.Seals=reader.GetValue(9).ToString();
					pn.RodWiper=reader.GetValue(10).ToString();
					pn.Notes=reader.GetValue(11).ToString();
					list.Add(pn);
				}
				reader.Close();
				myConnection.Close();
			}
			catch(OleDbException ex)
			{
				string sss="";
				sss=ex.Message;
			}
			finally
			{
				myConnection.Close();
			}
			return list;
		}
		///Red Valve End
		public ArrayList Select_DataFrom_ImportFile_Way(string filename)
		{  
			string constr="Provider=Microsoft.Jet.OLEDB.4.0;Data Source="+filename.ToString().Trim()
				+";Extended Properties='Excel 8.0;HDR=Yes;IMEX=2'";
			//issue #137 start
			//backup
			//string CommandText = "select [Line No#],[Required Qty],[Style],[Hydraulic Or Pneumatic],[Cyl# Stroke],[Cyl# Bore],[Seals],[Paint],[Customer Ref#] from [CylinderData$] Where [Line No#] >0 ";    
			//update
			//issue #267 start
			//backup
			//string CommandText = "select [Line No#],[Required Qty],[Style],[Hydraulic Or Pneumatic],[Cyl# Stroke],[Cyl# Bore],[Seals],[Paint],[Customer Ref#], [Thrust (lbs)], [Packing Friction (lbs)], [End To Open (lbs)], [Break To Open (lbs)], [Minimum Operating Pressure (psi)] from [CylinderData$] Where [Line No#] >0 ";    
			//update
			string CommandText = "select [Line No#],[Required Qty],[Style],[Hydraulic Or Pneumatic],[Cyl# Stroke],[Cyl# Bore],[Seals],[Paint],[Customer Ref#], [Thrust (lbs)], [Packing Friction (lbs)], [End To Open (lbs)], [Break To Open (lbs)], [Minimum Operating Pressure (psi)], [Safety Factor (%)] from [CylinderData$] Where [Line No#] >0 ";    
			//issue #267 end
			//issue #137 end
			OleDbConnection myConnection = new OleDbConnection(constr);
			OleDbCommand myCommand = new OleDbCommand(CommandText, myConnection);    			
			ArrayList list = new ArrayList(); 
			VelanClass pn=new VelanClass();
			try
			{
				myConnection.Open();    
				OleDbDataReader reader = myCommand.ExecuteReader();			
				while(reader.Read())
				{
					pn=new VelanClass();
					pn.Slno=reader.GetValue(0).ToString();
					pn.Qty=reader.GetValue(1).ToString();
					pn.Style=reader.GetValue(2).ToString();
					pn.PneumaticHydraulic=reader.GetValue(3).ToString();
					pn.Stroke=reader.GetValue(4).ToString();
					pn.Bore=reader.GetValue(5).ToString();					
					pn.Seals=reader.GetValue(6).ToString();				
					pn.Paint=reader.GetValue(7).ToString();					
					pn.Notes=reader.GetValue(8).ToString();	
					//issue #137 start
					pn.ETC=reader.GetValue(9).ToString();	
					pn.PackingFriction=reader.GetValue(10).ToString();	
					pn.ETO=reader.GetValue(11).ToString();	
					pn.BTO=reader.GetValue(12).ToString();	
					pn.AirPressure=reader.GetValue(13).ToString();	
					//issue 263 start
					pn.EtcValveTrust=pn.ETC;
					pn.MinAirSupply=reader.GetValue(13).ToString();
					//issue 263 end
					//issue #137 end
					//issue #267 start
					pn.SaftyFactor=reader.GetValue(14).ToString();
					//issue #267 end
					list.Add(pn);
				}
				reader.Close();
				myConnection.Close();
			}
			catch (OleDbException ex)
			{
				string str="";
				str=ex.Message;
			}
			finally
			{
				myConnection.Close();
			}
			return list;
		}
		public string SelectSpecSheet(string qno)
		{  
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.Text;
			command.CommandText= "Select SpecSheet from WEB_Pricing_Customer_TableV1 where quoteno=@parm1";
			command.Parameters.Add("@parm1",qno.Trim());
			string str="";
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
				if(reader.Read())
				{
					str =(reader.GetValue(0).ToString());
				}
				reader.Close();
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return str;
		}
	
		public ArrayList SelectOptionsWithCode(string col1,string col2,string tab, string series,string sort)
		{  
			string query = "select "+col1.Trim()+","+col2.Trim()+" from "+tab.Trim()+" Where ["+series.ToString()+"]= 1 order by "+sort.Trim()+"";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			SqlDataReader reader =null;
			ArrayList list1 = new ArrayList();
			ArrayList list2 = new ArrayList();
			ArrayList list = new ArrayList();
			try
			{
				command.Connection = GetConnection();
				reader = command.ExecuteReader(CommandBehavior.CloseConnection);
				while(reader.Read())
				{
					list1.Add(reader.GetValue(0));
					list2.Add(reader.GetValue(1));
				}				
				list.Add(list1);
				list.Add(list2);
				reader.Close();
				Close();
			}
			finally
			{
				if (reader != null)
				{
					reader.Close();
				}
				Close();
			}
			return list;
		}
		public ArrayList Select_TransducerDisc(string parm1)
		{  
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.Text ;
			command.CommandText= "select * from WEB_Transducer_Disc_TableV1 where Code =@parm1";
			command.Parameters.Add("@parm1",parm1.Trim());
			SqlDataReader reader =null;
			ArrayList list =new ArrayList();
			try
			{
				command.Connection = GetConnection();
				reader = command.ExecuteReader(CommandBehavior.CloseConnection);
				if(reader.Read())
				{
					list.Add(reader.GetValue(2).ToString());
					list.Add(reader.GetValue(3).ToString());
					list.Add(reader.GetValue(4).ToString());
					list.Add(reader.GetValue(5).ToString());
					list.Add(reader.GetValue(6).ToString());
					list.Add(reader.GetValue(7).ToString());
				}
				reader.Close();
				Close();
			}
			finally
			{
				if (reader != null)
				{
					reader.Close();
				}
				Close();
			}
			return list;
		}
		//issue #105 start
		public DataSet BackLogReport_PartNo(string customercode1,string customercode2,string partno)
		{
			string dml = "SELECT Top 40 [Slno],[PoNo],[PartNo],[Qty],[UnitPrice],[SoNo],[EntryDate],[DeliveryDate],[CustomerRef],[OrderStatus],[CustomerCode],[Status],[ProjectRef] "
				+" FROM [WEB_Backlog_TableV1] Where ([CustomerCode] =@customercode1 or [CustomerCode] =@customercode2) and [PartNo] like @partno and Status = '0' Order by [DeliveryDate] DESC;";
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@customercode1",customercode1.ToString());
			command.Parameters.Add("@customercode2",customercode2.ToString());
			command.Parameters.Add("@partno",partno.ToString()+"%");
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;			
				oledad.Fill(ds);
				Close();
			}			
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		//issue #105 end
		
		public DataSet BackLogReport(string customercode1,string customercode2)
		{
			//  SqlConnection SqlCon =new SqlConnection(constr);
			//issue #101 start
			//backup
//			string dml = "SELECT Top 20 [Slno],[PoNo],[PartNo],[Qty],[UnitPrice],[SoNo],[EntryDate],[DeliveryDate],[CustomerRef],[OrderStatus],[CustomerCode],[Status],[ProjectRef] "
//				+" FROM [WEB_Backlog_TableV1] Where ([CustomerCode] =@customercode1 or [CustomerCode] =@customercode2) Order by [DeliveryDate] DESC;";
			//update
//			string dml = "SELECT Top 40 [Slno],[PoNo],[PartNo],[Qty],[UnitPrice],[SoNo],[EntryDate],[DeliveryDate],[CustomerRef],[OrderStatus],[CustomerCode],[Status],[ProjectRef] "
//				+" FROM [WEB_Backlog_TableV1] Where ([CustomerCode] =@customercode1 or [CustomerCode] =@customercode2) and OrderStatus not like '%Order Cancelled%' and OrderStatus not like '%Shipped%' Order by [DeliveryDate] DESC;";
			//issue #101 end
			//issue #103 start
            //issue #225 start
			//backup
//			string dml = "SELECT Top 40 [Slno],[PoNo],[PartNo],[Qty],[UnitPrice],[SoNo],[EntryDate],[DeliveryDate],[CustomerRef],[OrderStatus],[CustomerCode],[Status],[ProjectRef] "
//				+" FROM [WEB_Backlog_TableV1] Where ([CustomerCode] =@customercode1 or [CustomerCode] =@customercode2) and Status = '0' Order by [DeliveryDate] DESC;";
			//update
			string dml = "SELECT  [Slno],[PoNo],[PartNo],[Qty],[UnitPrice],[SoNo],[EntryDate],[DeliveryDate],[CustomerRef],[OrderStatus],[CustomerCode],[Status],[ProjectRef] "
				+" FROM [WEB_Backlog_TableV1] Where ([CustomerCode] =@customercode1 or [CustomerCode] =@customercode2) and Status = '0' Order by [DeliveryDate] DESC;";
			//issue #103 end
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@customercode1",customercode1.ToString());
			command.Parameters.Add("@customercode2",customercode2.ToString());
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;			
				oledad.Fill(ds);
				Close();
			}			
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public DataSet BackLogReport_PoNo(string customercode1,string customercode2,string pono)
		{
			//  SqlConnection SqlCon =new SqlConnection(constr);
			//issue #101 start
			//backup
//			string dml = "SELECT Top 20 [Slno],[PoNo],[PartNo],[Qty],[UnitPrice],[SoNo],[EntryDate],[DeliveryDate],[CustomerRef],[OrderStatus],[CustomerCode],[Status],[ProjectRef] "
//				+" FROM [WEB_Backlog_TableV1] Where ([CustomerCode] =@customercode1 or [CustomerCode] =@customercode2) and [PoNo] like @pono Order by [DeliveryDate] DESC;";
			//update
//			string dml = "SELECT Top 40 [Slno],[PoNo],[PartNo],[Qty],[UnitPrice],[SoNo],[EntryDate],[DeliveryDate],[CustomerRef],[OrderStatus],[CustomerCode],[Status],[ProjectRef] "
//				+" FROM [WEB_Backlog_TableV1] Where ([CustomerCode] =@customercode1 or [CustomerCode] =@customercode2) and [PoNo] like @pono and OrderStatus not like '%Order Cancelled%' and OrderStatus not like '%Shipped%' Order by [DeliveryDate] DESC;";
			//issue #101 end
			//issue #103 start
			//issue #225 start
			//backup
//			string dml = "SELECT Top 40 [Slno],[PoNo],[PartNo],[Qty],[UnitPrice],[SoNo],[EntryDate],[DeliveryDate],[CustomerRef],[OrderStatus],[CustomerCode],[Status],[ProjectRef] "
//				+" FROM [WEB_Backlog_TableV1] Where ([CustomerCode] =@customercode1 or [CustomerCode] =@customercode2) and [PoNo] like @pono and Status = '0' Order by [DeliveryDate] DESC;";
			//update
			string dml = "SELECT  [Slno],[PoNo],[PartNo],[Qty],[UnitPrice],[SoNo],[EntryDate],[DeliveryDate],[CustomerRef],[OrderStatus],[CustomerCode],[Status],[ProjectRef] "
				+" FROM [WEB_Backlog_TableV1] Where ([CustomerCode] =@customercode1 or [CustomerCode] =@customercode2) and [PoNo] like @pono and Status = '0' Order by [DeliveryDate] DESC;";
			
			//issue #103 end
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@customercode1",customercode1.ToString());
			command.Parameters.Add("@customercode2",customercode2.ToString());
			command.Parameters.Add("@pono",pono.ToString()+"%");
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;			
				oledad.Fill(ds);
				Close();
			}			
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public DataSet BackLogReport_Project(string customercode1,string customercode2,string project)
		{
			//  SqlConnection SqlCon =new SqlConnection(constr);
			//issue #101 start
			//backup
//			string dml = "SELECT Top 20 [Slno],[PoNo],[PartNo],[Qty],[UnitPrice],[SoNo],[EntryDate],[DeliveryDate],[CustomerRef],[OrderStatus],[CustomerCode],[Status],[ProjectRef] "
//				+" FROM [WEB_Backlog_TableV1] Where ([CustomerCode] =@customercode1 or [CustomerCode] =@customercode2) and [ProjectRef] like @project Order by [DeliveryDate] DESC;";
			//update
//			string dml = "SELECT Top 40 [Slno],[PoNo],[PartNo],[Qty],[UnitPrice],[SoNo],[EntryDate],[DeliveryDate],[CustomerRef],[OrderStatus],[CustomerCode],[Status],[ProjectRef] "
//				+" FROM [WEB_Backlog_TableV1] Where ([CustomerCode] =@customercode1 or [CustomerCode] =@customercode2) and [ProjectRef] like @project and OrderStatus not like '%Order Cancelled%' and OrderStatus not like '%Shipped%' Order by [DeliveryDate] DESC;";
			//issue #101 end
			//issue #103 start
			//issue #225 start
			//backup
//			string dml = "SELECT Top 40 [Slno],[PoNo],[PartNo],[Qty],[UnitPrice],[SoNo],[EntryDate],[DeliveryDate],[CustomerRef],[OrderStatus],[CustomerCode],[Status],[ProjectRef] "
//				+" FROM [WEB_Backlog_TableV1] Where ([CustomerCode] =@customercode1 or [CustomerCode] =@customercode2) and [ProjectRef] like @project and Status = '0' Order by [DeliveryDate] DESC;";
			//update
			string dml = "SELECT  [Slno],[PoNo],[PartNo],[Qty],[UnitPrice],[SoNo],[EntryDate],[DeliveryDate],[CustomerRef],[OrderStatus],[CustomerCode],[Status],[ProjectRef] "
				+" FROM [WEB_Backlog_TableV1] Where ([CustomerCode] =@customercode1 or [CustomerCode] =@customercode2) and [ProjectRef] like @project and Status = '0' Order by [DeliveryDate] DESC;";
			//issue #103 end
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@customercode1",customercode1.ToString());
			command.Parameters.Add("@customercode2",customercode2.ToString());
			command.Parameters.Add("@project",project.ToString()+"%");
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;			
				oledad.Fill(ds);
				Close();
			}			
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public DataSet BackLogReport_CustomerRef(string customercode1,string customercode2,string customerref)
		{
			//  SqlConnection SqlCon =new SqlConnection(constr);
			//issue #101 start
			//start
//			string dml = "SELECT Top 20 [Slno],[PoNo],[PartNo],[Qty],[UnitPrice],[SoNo],[EntryDate],[DeliveryDate],[CustomerRef],[OrderStatus],[CustomerCode],[Status],[ProjectRef] "
//				+" FROM [WEB_Backlog_TableV1] Where ([CustomerCode] =@customercode1 or [CustomerCode] =@customercode2) and [CustomerRef] like @customerref Order by [DeliveryDate] DESC;";
			//update
//			string dml = "SELECT Top 40 [Slno],[PoNo],[PartNo],[Qty],[UnitPrice],[SoNo],[EntryDate],[DeliveryDate],[CustomerRef],[OrderStatus],[CustomerCode],[Status],[ProjectRef] "
//				+" FROM [WEB_Backlog_TableV1] Where ([CustomerCode] =@customercode1 or [CustomerCode] =@customercode2) and [CustomerRef] like @customerref and OrderStatus not like '%Order Cancelled%' and OrderStatus not like '%Shipped%' Order by [DeliveryDate] DESC;";
			//issue #101 end
			//issue #103 start
			//issue #225 start
			//backup
//			string dml = "SELECT Top 40 [Slno],[PoNo],[PartNo],[Qty],[UnitPrice],[SoNo],[EntryDate],[DeliveryDate],[CustomerRef],[OrderStatus],[CustomerCode],[Status],[ProjectRef] "
//				+" FROM [WEB_Backlog_TableV1] Where ([CustomerCode] =@customercode1 or [CustomerCode] =@customercode2) and [CustomerRef] like @customerref and Status = '0' Order by [DeliveryDate] DESC;";
			//update
			string dml = "SELECT [Slno],[PoNo],[PartNo],[Qty],[UnitPrice],[SoNo],[EntryDate],[DeliveryDate],[CustomerRef],[OrderStatus],[CustomerCode],[Status],[ProjectRef] "
				+" FROM [WEB_Backlog_TableV1] Where ([CustomerCode] =@customercode1 or [CustomerCode] =@customercode2) and [CustomerRef] like @customerref and Status = '0' Order by [DeliveryDate] DESC;";
			
			//issue #103 end
			SqlCommand command = new SqlCommand();
			command.CommandText = dml;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@customercode1",customercode1.ToString());
			command.Parameters.Add("@customercode2",customercode2.ToString());
			command.Parameters.Add("@customerref",customerref.ToString()+"%");
			DataSet ds =new DataSet();
			try
			{
				command.Connection = GetConnection();
				SqlDataAdapter oledad =new SqlDataAdapter();
				oledad.SelectCommand =command;			
				oledad.Fill(ds);
				Close();
			}			
			finally
			{
				SqlCon.Close();
			}
			return ds;
		}
		public string  Update_AuthotrizeQuote_SMC(string qno)
		{  
			string query = "Update WEB_Pricing_Customer_TableV1 Set Finished='1' where QuoteNo=@qno;";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@qno",qno.Trim());		
			string s="";
			try
			{
				command.Connection = GetConnection();
				s=command.ExecuteNonQuery().ToString();
				Close();
			}		
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public string  Update_AuthotrizeQuote_ICYL(string qno)
		{  
			string query = "Update Pricing_Customer_Table Set Finished='1' where QuoteNo=@qno;";
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;
			command.Parameters.Add("@qno",qno.Trim());		
			string s="";
			try
			{
				command.Connection = GetConnection();
				s=command.ExecuteNonQuery().ToString();
				Close();
			}		
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		public ArrayList Select_BoreByBoreArea(string table,string type,string area)
		{  
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.Text ;
			command.CommandText= "select Top 1 BCode,RCode,"+type+" from "+table+" where "+type+" >='"+area+"' Order By "+type+" ASC";
			SqlDataReader reader =null;
			ArrayList list =new ArrayList();
			try
			{
				command.Connection = GetConnection();
				reader = command.ExecuteReader(CommandBehavior.CloseConnection);
				if(reader.Read())
				{
					list.Add(reader.GetValue(0).ToString());
					list.Add(reader.GetValue(1).ToString());
					list.Add(reader.GetValue(2).ToString());
				}
				reader.Close();
				Close();
			}
			finally
			{
				if (reader != null)
				{
					reader.Close();
				}
				Close();
			}
			return list;
		}
		public decimal SelectOneValueByAllinfo_Rod(string col1,string tab,string col2,string val1)
		{  		
			decimal s =0.00m;
			string query="select  "+col1.ToString().Trim()+"  from   "+tab.ToString().Trim()+"  Where  "+col2.ToString().Trim()+" = '"+val1.ToString().Trim()+"';"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;	
			SqlDataReader reader =null;
			string str="";
			try
			{
				IDisposable connection=null;
				connection = GetConnection();
				command.Connection = (SqlConnection)connection;
				reader = command.ExecuteReader(CommandBehavior.CloseConnection);
				if(reader.Read())
				{
					str=reader.GetValue(0).ToString();
				}
				reader.Close();
				Close();
				if(str.Trim() !="")
				{
					s=Convert.ToDecimal(str.ToString());
				}
			}
			catch(Exception ex)
			{
				throw(ex);
			}
			finally
			{
				if (reader != null)
				{
					reader.Close();
				}
				Close();
			}		
			return s;
		}
		//issue #90 start
		public decimal SelectAddersPrice_ByBore(string l,string tab,string bore)
		{  		
			string query="select  "+l.ToString().Trim()+"  from   "+tab.ToString().Trim()+"  Where BoreSize= '"+bore.ToString().Trim()+"' "; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;	
			decimal s =0.00m;
			try
			{
				command.Connection = GetConnection();
				SqlDataReader reader = command.ExecuteReader();
				if(reader.Read())
				{
					s=Convert.ToDecimal(reader.GetValue(0));
				}
				reader.Close();
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
		//issue #90 end

		//issue #230 start
		public decimal AS_Select_CylArea(string bore, string tandem)
		{  		
			decimal s =0.00m;
			string query="SELECT ActualArea FROM AS_Area_Bore_Index WHERE BoreCode = @bore AND Tandem = @tandem"; 
			SqlCommand command =new SqlCommand();
			command.CommandText = query;
			command.CommandType = CommandType.Text;	
			command.Parameters.Add("@bore",bore.Trim());		
			command.Parameters.Add("@tandem",tandem.Trim());		
			SqlDataReader reader =null;
			string str="";
			try
			{
				IDisposable connection=null;
				connection = GetConnection();
				command.Connection = (SqlConnection)connection;
				reader = command.ExecuteReader(CommandBehavior.CloseConnection);
				if(reader.Read())
				{
					str=reader.GetValue(0).ToString();
				}
				reader.Close();
				Close();
				if(str.Trim() !="")
				{
					s=Convert.ToDecimal(str.ToString());
				}
			}
			catch(Exception ex)
			{
				throw(ex);
			}
			finally
			{
				if (reader != null)
				{
					reader.Close();
				}
				Close();
			}		
			return s;
		}
		//issue #230 end

		public string  Insert_NewRFQ_ICYL(string originator, string crfqno,string customer,string contact,string objrfq,string drawings,string cdate,string pdate,string aperson,string finished,string ptime)
		{  
			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.Text;
			command.CommandText= " set DateFormat mdy; "
				+" INSERT INTO RFQ_TableV1 (RDate,Originator,CRFQNo,Customer,Contact,ObjectRFQ,Drawings,CDate,PDate,AssignPerson,Finished,PTime) "
				+" values(@rdate,@originator,@crfqno,@customer,@contact,@objectrfq,@drawings,@cdate,@pdate,@aperson,@finished,@ptime) ";
			command.Parameters.Add("@rdate",DateTime.Today.ToShortDateString().Trim());
			command.Parameters.Add("@originator",originator.Trim());
			command.Parameters.Add("@crfqno",crfqno.Trim());
			command.Parameters.Add("@customer",customer.Trim());
			command.Parameters.Add("@contact",contact.Trim());
			command.Parameters.Add("@objectrfq",objrfq.Trim());
			command.Parameters.Add("@drawings",drawings.Trim());
			command.Parameters.Add("@cdate",cdate.Trim());
			command.Parameters.Add("@pdate",pdate.Trim());
			command.Parameters.Add("@aperson",aperson.Trim());
			command.Parameters.Add("@finished",finished.Trim());
			command.Parameters.Add("@ptime",ptime.Trim());
			string s="";
			try
			{
				IDisposable connection=null;connection = GetConnection();command.Connection = (SqlConnection)connection;
				s=command.ExecuteNonQuery().ToString();
				Close();
			}
			finally
			{
				SqlCon.Close();
			}
			return s;
		}
	}
}
