using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for Result.
	/// </summary>
	public partial class Result : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(! IsPostBack)
			{
				if(Session["User"] !=null)
				{
					ArrayList lst =new ArrayList();
					lst =(ArrayList)Session["User"];
					string qno =Request.QueryString["id"].Trim();
					//#588
					if(lst[6].ToString().Trim()=="1003" || lst[6].ToString().Trim()=="1015"|| lst[6].ToString().Trim()=="1001" 
						|| lst[6].ToString().Trim()=="1009" || lst[6].ToString().Trim()=="1011" || lst[6].ToString().Trim()=="1012" || lst[6].ToString().Trim()=="1019"|| lst[6].ToString().Trim()=="1020"  || lst[6].ToString().Trim()=="1027" || lst[6].ToString().Trim()=="1028" || lst[6].ToString().Trim()=="1029" || lst[6].ToString().Trim()=="1030")
					{
						//LBLSeries.Text="The options you selected require assistance from the factory.<br>Your Quote Ref # is "+qno.Trim()+" <br>Pricing will be sent to you by email.<br>For more information please contact <a href='mailto:matt@cowandynamics.com'> matt@cowandynamics.com</a>";
						LBLSeries.Text="The options you selected require assistance from the factory.<br>Your Quote Ref # is "+qno.Trim()+" <br>Pricing will be sent to you by email.<br>For more information please contact <a href='jenny.l@cowandynamics.com'> jenny.l@cowandynamics.com</a>";
						lblinfo.Text="<script language='javascript'>window.open('print.aspx?id="+qno.Trim()+"&type=icylinder','blank','toolbar=yes,width=800,height=800,resizable=yes,top=0,left=0')</script>";
					}
					//issue #118 start
				    //else if(lst[6].ToString().Trim()=="1004" || lst[6].ToString().Trim()=="1002" || lst[6].ToString().Trim()=="1016")
					//backup
					//update
					else if(lst[6].ToString().Trim()=="1004" || lst[6].ToString().Trim()=="1002" || lst[6].ToString().Trim()=="1016" ||  lst[6].ToString().Trim()=="1021" ||  lst[6].ToString().Trim()=="1026")
					//issue #118 end
					{
						LBLSeries.Text="The options you selected require assistance from the factory.<br>Your Quote Ref # is "+qno.Trim()+" <br>Pricing will be sent to you by email.<br>For more information please contact <a href='mailto:dtaranu@cowandynamics.com'> dtaranu@cowandynamics.com</a>";
						lblinfo.Text="<script language='javascript'>window.open('print.aspx?id="+qno.Trim()+"&type=icylinder','blank','toolbar=yes,width=800,height=800,resizable=yes,top=0,left=0')</script>";
					}
					else if(lst[6].ToString().Trim()=="1008" || lst[6].ToString().Trim()=="1000")
					{
						LBLSeries.Text="The options you selected require assistance from the factory.<br>Your Quote Ref # is "+qno.Trim()+" <br>Pricing will be sent to you by email.<br>For more information please contact <a href='mailto:dtaranu@cowandynamics.com'> dtaranu@cowandynamics.com</a>";
						lblinfo.Text="<script language='javascript'>window.open('print.aspx?id="+qno.Trim()+"&type=icylinder','blank','toolbar=yes,width=800,height=800,resizable=yes,top=0,left=0')</script>";
					}
					else
					{
						LBLSeries.Text="The options you selected require assistance from the factory.<br>Your Quote Ref # is "+qno.Trim()+" <br>Pricing will be sent to you by email.<br>For more information please contact <a href='mailto:dtaranu@cowandynamics.com'> dtaranu@cowandynamics.com</a>";
						lblinfo.Text="<script language='javascript'>window.open('print.aspx?id="+qno.Trim()+"&type=icylinder','blank','toolbar=yes,width=800,height=800,resizable=yes,top=0,left=0')</script>";
					}
				}
			}
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
		
	}
}
