<%@ Page language="c#" Codebehind="Velan_Page2.aspx.cs" AutoEventWireup="True" Inherits="iCylinderV1.Velan_Page2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Velan_Page2</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 1000px; HEIGHT: 487px" cellSpacing="0" cellPadding="0"
				width="1000" bgColor="lightgrey" border="0">
				<TR>
					<TD width="50" height="20"></TD>
					<TD style="WIDT: 484px" align="center" height="20"></TD>
					<TD align="center" height="20"><asp:label id="Label1" runat="server" Font-Size="Small" Font-Underline="True" BorderColor="Transparent"
							BackColor="Transparent">Series  A - Velan Cylinder Special Options</asp:label></TD>
					<TD align="center" height="20"></TD>
					<TD width="50" height="20"></TD>
				</TR>
				<TR>
					<TD width="50"></TD>
					<TD language="300" align="center" width="299" bgColor="#dcdcdc"><asp:panel id="Panel2" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
							<TABLE id="Table6" borderColor="silver" cellSpacing="0" cellPadding="0" width="299" border="0">
								<TR>
									<TD align="center" colSpan="2">
										<asp:label id="Label8" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
											Font-Size="Smaller" ForeColor="Maroon" Font-Bold="True">Mounting Kit</asp:label></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 161px" align="right">
										<asp:image id="ImageMK" runat="server" ImageUrl="specials/kgt.gif"></asp:image></TD>
									<TD align="center">
										<asp:Label id="LblMKDesc" runat="server" Font-Size="8pt" ForeColor="Black" Width="125px"></asp:Label></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 161px; HEIGHT: 20px" align="right">
										<asp:Label id="Label7" runat="server" Font-Size="9pt">Valve Size :</asp:Label></TD>
									<TD style="HEIGHT: 20px" align="left">
										<asp:DropDownList id="DDLValveSize" runat="server" Font-Size="8pt" Width="57px" AutoPostBack="True" onselectedindexchanged="DDLValveSize_SelectedIndexChanged"></asp:DropDownList></TD>
								</TR>
								<TR>
									<TD align="center" colSpan="2">
										<asp:RadioButtonList id="RBLMKit" runat="server" Font-Size="Smaller" AutoPostBack="True" onselectedindexchanged="RBLMKit_SelectedIndexChanged">
											<asp:ListItem Value="KG" Selected="True">Standard Knife-Gate</asp:ListItem>
											<asp:ListItem Value="BT">Bonnet Type</asp:ListItem>
											<asp:ListItem Value="None">None</asp:ListItem>
										</asp:RadioButtonList>
										<asp:label id="lblstroke" runat="server" BackColor="Transparent" BorderColor="Transparent"
											Font-Size="9pt" ForeColor="Red" Width="258px"></asp:label></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="Panel6" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
							<TABLE id="Table13" style="HEIGHT: 8px" borderColor="silver" cellSpacing="0" cellPadding="0"
								width="299" border="0">
								<TR>
								</TR>
								<TR>
								</TR>
								<TR>
								</TR>
								<TR>
								</TR>
							</TABLE>
							<TABLE id="Table10" borderColor="silver" cellSpacing="0" cellPadding="0" width="299" border="0">
								<TR>
									<TD align="center" colSpan="2">
										<asp:label id="Label12" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
											Font-Size="Smaller" ForeColor="Maroon" Font-Bold="True">Limit Switch</asp:label></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 154px" align="right">
										<asp:image id="Image1" runat="server" ImageUrl="specials/ls.gif"></asp:image></TD>
									<TD align="center">
										<asp:Label id="LblLSDesc" runat="server" Font-Size="8pt" ForeColor="Black" Width="114px"></asp:Label>
										<asp:textbox id="TxtLSDesc" runat="server" Font-Size="8pt" Width="144px" Height="54px" TextMode="MultiLine"
											MaxLength="100"></asp:textbox>
										<asp:RequiredFieldValidator id="RFVLS" runat="server" Font-Size="8pt" Width="103px" ControlToValidate="TxtLSDesc"
											ErrorMessage="Enter Specification" Visible="False"></asp:RequiredFieldValidator></TD>
								</TR>
								<TR>
									<TD align="center" colSpan="2">
										<asp:RadioButtonList id="RBLLS" runat="server" Font-Size="Smaller" AutoPostBack="True" onselectedindexchanged="RBLLS_SelectedIndexChanged">
											<asp:ListItem Value="LS1">Standard Limit Switch</asp:ListItem>
											<asp:ListItem Value="LS2">Special  Limit Switch</asp:ListItem>
											<asp:ListItem Value="None" Selected="True">None</asp:ListItem>
										</asp:RadioButtonList></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
					<TD align="center" width="299" bgColor="#dcdcdc">
						<TABLE id="Table2" style="HEIGHT: 35px" borderColor="silver" cellSpacing="0" cellPadding="0"
							width="299" border="0">
						</TABLE>
						<asp:panel id="Panel9" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
							<TABLE id="Table14" style="HEIGHT: 8px" borderColor="silver" cellSpacing="0" cellPadding="0"
								width="299" border="0">
								<TR>
									<TD align="center">
										<asp:label id="Label18" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
											Font-Size="Smaller" ForeColor="Maroon" Font-Bold="True">Manual Override</asp:label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:image id="ImageMO" runat="server" ImageUrl="specials/mo.gif"></asp:image></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:RadioButtonList id="RBLMO" runat="server" Font-Size="Smaller" RepeatDirection="Horizontal">
											<asp:ListItem Value="MO">Yes</asp:ListItem>
											<asp:ListItem Selected="True">No</asp:ListItem>
										</asp:RadioButtonList></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="PanelSV" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
							<TABLE id="Table8" borderColor="silver" cellSpacing="0" cellPadding="0" width="299" border="0">
								<TR>
									<TD style="WIDTH: 152px" align="right">
										<asp:label id="Label9" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
											Font-Size="Smaller" ForeColor="Maroon" Font-Bold="True">Solenoid Valve</asp:label></TD>
									<TD align="left">
										<asp:DropDownList id="DDLSVType" runat="server" Font-Size="8pt" Width="72px" AutoPostBack="True" onselectedindexchanged="DDLSVType_SelectedIndexChanged"></asp:DropDownList></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 152px" align="right">
										<asp:image id="Image3" runat="server" ImageUrl="specials/sv.gif"></asp:image></TD>
									<TD align="center">
										<asp:Label id="LblSVDesc" runat="server" Font-Size="8pt" ForeColor="Black" Width="125px"></asp:Label></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 152px" align="right">
										<asp:Label id="lblclassi" runat="server" Font-Size="9pt">Classification :</asp:Label></TD>
									<TD align="left">
										<asp:textbox id="TxtClassification" runat="server" Font-Size="8pt" Width="144px"></asp:textbox></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 152px" align="center"></TD>
									<TD align="left">
										<asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" Font-Size="8pt" Width="103px" ControlToValidate="TxtClassification"
											ErrorMessage="Enter Classification" Visible="False"></asp:RequiredFieldValidator></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="PanelTB" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px"
							>
							<TABLE id="Table5" borderColor="silver" cellSpacing="0" cellPadding="0" width="299" border="0">
								<TR>
									<TD align="center">
										<asp:label id="Label4" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
											Font-Size="Smaller" ForeColor="Maroon" Font-Bold="True">Tubing & Fittings</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:RadioButtonList id="RBLTubing" runat="server" Font-Size="Smaller" AutoPostBack="True" onselectedindexchanged="RBLTubing_SelectedIndexChanged">
											<asp:ListItem Value="T1" Selected="True">Copper Tubing &amp; Brass Fittings</asp:ListItem>
											<asp:ListItem Value="T2">Stainless Steel Tubing &amp; Fittings (No Brand)</asp:ListItem>
											<asp:ListItem Value="T3">Stainless Steel Tubing &amp; Fittings (Swagelock)</asp:ListItem>
										</asp:RadioButtonList>
										<asp:label id="lbltubing" runat="server" BackColor="Transparent" BorderColor="Transparent"
											Font-Size="9pt" ForeColor="Red" Width="234px" Visible="False">you can select only Stainless Steel options</asp:label></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
					<TD align="center" width="299" bgColor="gainsboro"><asp:panel id="PanelFR" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
							<TABLE id="Table4" style="HEIGHT: 35px" borderColor="silver" cellSpacing="0" cellPadding="0"
								width="299" border="0">
								<TR>
									<TD align="center">
										<asp:label id="Label3" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
											Font-Size="Smaller" ForeColor="Maroon" Font-Bold="True">Filter Regulator</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:image id="Image5" runat="server" ImageUrl="specials/fr.gif"></asp:image></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:RadioButtonList id="RBLFR" runat="server" Font-Size="Smaller" AutoPostBack="True" onselectedindexchanged="RBLFR_SelectedIndexChanged">
											<asp:ListItem Value="FR1">General Purpose Filter Regulator</asp:ListItem>
											<asp:ListItem Value="FR2">Stainless Steel Filter Regulator</asp:ListItem>
											<asp:ListItem Selected="True">None</asp:ListItem>
										</asp:RadioButtonList>
										<asp:label id="lblfr" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Size="9pt"
											ForeColor="Red" Width="234px" Visible="False">you can select only Stainless Steel option</asp:label></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="PanelFC" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
							<TABLE id="Table3" style="HEIGHT: 35px" borderColor="silver" cellSpacing="0" cellPadding="0"
								width="299" border="0">
								<TR>
									<TD align="center">
										<asp:label id="Label2" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
											Font-Size="Smaller" ForeColor="Maroon" Font-Bold="True">Flow Control</asp:label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 120px" align="center">
										<asp:image id="Image4" runat="server" ImageUrl="specials/fc.gif"></asp:image></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:RadioButtonList id="RBLFC" runat="server" Font-Size="Smaller" AutoPostBack="True" onselectedindexchanged="RBLFC_SelectedIndexChanged">
											<asp:ListItem Value="F1">Inline Flow Controls</asp:ListItem>
											<asp:ListItem Value="F2">Inline Stainless Flow Controls</asp:ListItem>
											<asp:ListItem Selected="True">None</asp:ListItem>
										</asp:RadioButtonList>
										<asp:label id="lblfc" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Size="9pt"
											ForeColor="Red" Width="234px" Visible="False">you can select only Stainless Steel option</asp:label></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
					<TD width="50"></TD>
				</TR>
				<TR>
					<TD align="right" width="50" height="30"><asp:linkbutton id="BtnBack" runat="server" Font-Size="Smaller" Font-Bold="True" onclick="BtnBack_Click">Back</asp:linkbutton></TD>
					<TD language="300" align="center" height="30"><asp:label id="lblhidden" runat="server" Font-Size="XX-Small" Visible="False"></asp:label></TD>
					<TD align="center" height="30"><asp:label id="lblpage" runat="server" Font-Size="9pt" BorderColor="Transparent" BackColor="Transparent"
							ForeColor="Red" Width="234px" Visible="False">Enter / Select  all values</asp:label></TD>
					<TD align="center" height="30"></TD>
					<TD width="50" height="30"><asp:linkbutton id="BtnNext" runat="server" Font-Size="Smaller" Font-Bold="True" onclick="BtnNext_Click">Next</asp:linkbutton></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
