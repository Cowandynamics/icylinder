using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for Icylinder_SelectV.
	/// </summary>
	public partial class Icylinder_SelectV : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["User"] !=null)
			{
				if(! IsPostBack)
				{
					ArrayList lst =new ArrayList();
					lst =(ArrayList)Session["User"];
					//issue #118 start
					//backup
					//if(lst[6].ToString().Trim() =="1002"  || lst[6].ToString().Trim() =="1015" || lst[6].ToString().Trim() =="1018" )
					//update
					//if(lst[6].ToString().Trim() =="1002"  || lst[6].ToString().Trim() =="1015" || lst[6].ToString().Trim() =="1018"  || lst[6].ToString().Trim() =="1021")
					//issue #267 start
					//back
					//if(lst[6].ToString().Trim() =="1002"  || lst[6].ToString().Trim() =="1015" || lst[6].ToString().Trim() =="1018"  || lst[6].ToString().Trim() =="1021"  || lst[6].ToString().Trim() =="998")
					//update
					if(lst[6].ToString().Trim() =="1002"  || lst[6].ToString().Trim() =="1015" || lst[6].ToString().Trim() =="1021"  || lst[6].ToString().Trim() =="998")
					//issue #267 end
					//issue #118 end
					{
						Response.Redirect("Icylinder_Page1.aspx");
					}
					else if(lst[6].ToString().Trim() =="1003")
					{
						Response.Redirect("EV_Series_Select.aspx");
					}
					//issue #137 start
						//back
					//else if(lst[6].ToString().Trim() =="1004")
					//update
					else if(lst[6].ToString().Trim() =="1004" || lst[6].ToString().Trim() =="999")
					//issue #137 end
					{
						//Response.Redirect("Velan_Select.aspx");
						Response.Redirect("Velan_BachQuote.aspx");
					}
					else if(lst[6].ToString().Trim() =="1006")
					{
						Response.Redirect("ART_SeriesA_Page1.aspx?p=new");
					}
					//issue #137 start
					//backup
					//else if(lst[6].ToString().Trim() =="1009" || lst[6].ToString().Trim() =="1011")
					//update
					else if(lst[6].ToString().Trim() =="1009" || lst[6].ToString().Trim() =="1011" || lst[6].ToString().Trim() =="996")
					//issue #137 end
					{
						//Response.Redirect("Rotork_BachQuote.aspx");
						Response.Redirect("Velan_BachQuote.aspx");
					}
					else if(lst[6].ToString().Trim() =="1014")
					{
						Response.Redirect("Icylinder_Page1.aspx");
					}
//					else if(lst[6].ToString().Trim() =="1015")
//					{
//						Response.Redirect("SVC_Summit_BatchQuote.aspx");
//					}
					//issue #137 start
					//backup
					//else if(lst[6].ToString().Trim() =="1016" )
					//update
					//issue #267 start
//					else if(lst[6].ToString().Trim() =="1018"  )
//					{
//						Response.Redirect("Icylinder_ML_A.aspx");
//					}
						//back
					//else if(lst[6].ToString().Trim() =="1016"  || lst[6].ToString().Trim() =="997")
					//update
					//issue #395
					else if(lst[6].ToString().Trim() =="1016"  || lst[6].ToString().Trim() =="997" || lst[6].ToString().Trim() =="1018"  || lst[6].ToString().Trim() =="1024" || lst[6].ToString().Trim() =="1025"  || lst[6].ToString().Trim() =="1026")
					//issue #267 end
					//issue #137 end
					{
						Response.Redirect("WayValve_BatchQuote.aspx");
					}
					//issue #137 start
					//backup
					//else if(lst[6].ToString().Trim() =="1017")
					//update
					else if(lst[6].ToString().Trim() =="1017" || lst[6].ToString().Trim() =="995")
					//issue #137 end
					{
						Response.Redirect("Sistag_Page1.aspx");
					}
					//issue #204 start
					else if(lst[6].ToString().Trim() =="1022" || lst[6].ToString().Trim() =="1023")
					{
						Response.Redirect("Velan_BachQuote.aspx");
					}
					//issue #204 end
				    //issue #660 start
					else if(lst[6].ToString().Trim() =="1030")
					{
						Response.Redirect("PNOAdvanced.aspx");
					}
				}
			}
			else
			{
				Response.Redirect("Login.aspx");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void LBcap_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("PNOAdvanced.aspx");
		}

		protected void LBRod_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Icylinder_Page1.aspx");
		}

		protected void IBCap_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			Response.Redirect("PNOAdvanced.aspx");
		}

		protected void IBRod_Click(object sender, System.Web.UI.ImageClickEventArgs e)
		{
			Response.Redirect("Icylinder_Page1.aspx");
		}
	}
}
