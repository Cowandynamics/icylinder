using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for Drawing.
	/// </summary>
	public partial class Drawing : System.Web.UI.Page
	{
		DBClass db=new DBClass();
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(Request.QueryString["qno"] !=null && Request.QueryString["pno"] !=null && Session["User"] !=null)
			{
				string qno=Request.QueryString["qno"].ToString();
				string pno=Request.QueryString["pno"].ToString();
				string dwg="";
				//issue #137 start
				string series=db.Select_Series_QItem(qno,pno);// Request.QueryString["series"].ToString();
				//issue #137 end
				dwg=db.SelectSTDDrawing(qno.ToString(),pno.ToString());
				if(dwg.ToString().Trim() =="")
				{
					if(pno.Substring(0,1) =="P")
					{
						SystemDrawing_SeriesP(qno.Trim(),pno.Trim());
					}
					else if(pno.Substring(0,1) =="M")
					{
						SystemDrawing_SeriesML(qno.Trim(),pno.Trim());
					}
					//issue #137 sart
					//update
					//else if(pno.Substring(0,1) =="A")
					//backup
					else if(pno.Substring(0,1) =="A" || series=="AS")
					//issue #137 end
					{
						//issue #137 start
						if(series=="AS") SystemDrawing_SeriesAS(qno.Trim(),pno.Trim());
							//issue #278 start
						else if(series=="AT") 
						{
							//issue #624 start
							SystemDrawing_SeriesAT(qno.Trim(),pno.Trim());
							//issue #624 end
							//err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
						}
							//issue #278 end
						else
							//issue #137 end
							if(pno.ToString() !="AT")
						{
							SystemDrawing_SeriesA(qno.Trim(),pno.Trim());
						}
					}
					else if(pno.Substring(0,1) =="N")
					{
						SystemDrawing_SeriesN(qno.Trim(),pno.Trim());
					}
					else
					{
						err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
					}
				}
				else if(dwg.ToString().Trim() =="1")
				{
					if(pno.Trim().Substring(0,1) =="Z")
					{
						if(pno.Substring(1,1) =="P")
						{
							SystemDrawing_SeriesP(qno.Trim(),pno.Trim());
						}
						else if(pno.Substring(1,1) =="M")
						{
							SystemDrawing_SeriesML(qno.Trim(),pno.Trim());
						}
						else if(pno.Substring(1,1) =="A")
						{
							//issue #137 start
							if(series=="AS") SystemDrawing_SeriesAS(qno.Trim(),pno.Trim());
								//issue #278 start
							else if(series=="AT")
							{
								//issue #624 start
								SystemDrawing_SeriesAT(qno.Trim(),pno.Trim());
								//issue #624 end
								//err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
							}	
								//issue #278 end
						
							else
								
								//issue #137 end
								if(pno.ToString() !="AT")
							{
								SystemDrawing_SeriesA(qno.Trim(),pno.Trim());
							}
						}
						else if(pno.Substring(0,1) =="N")
						{
							SystemDrawing_SeriesN(qno.Trim(),pno.Trim());
						}
						else
						{
							err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
						}
					}
					else
					{
						if(pno.Substring(0,1) =="P")
						{
							SystemDrawing_SeriesP(qno.Trim(),pno.Trim());
						}
						else if(pno.Substring(0,1) =="A")
						{
							//issue #137 start
							if(series=="AS") SystemDrawing_SeriesAS(qno.Trim(),pno.Trim());
								//issue #278 start
							else if(series=="AT") 
							{
								//issue #624 start
								SystemDrawing_SeriesAT(qno.Trim(),pno.Trim());
								//issue #624 end
								//err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
							}
								//issue #278 end
						
							else
								//issue #137 end
								if(pno.ToString() !="AT")
							{
								SystemDrawing_SeriesA(qno.Trim(),pno.Trim());
							}
						}
						else if(pno.Substring(0,1) =="M")
						{
							SystemDrawing_SeriesML(qno.Trim(),pno.Trim());
						}
						else if(pno.Substring(0,1) =="N")
						{
							SystemDrawing_SeriesN(qno.Trim(),pno.Trim());
						}
						else
						{
							err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
						}
					}
				}
				else if(dwg.ToString().Trim() =="2")
				{
					err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
				}
				else
				{
					err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
				}
			}
		}
		//page_load test
		private void Page_Load_test(object sender, System.EventArgs e)
		{
			if(1==1 && 1==1)
			{
				string qno="T09060104-R1";
				string pno="ATKEA48NN11I074.25T2";
				string dwg="";
				//issue #137 start
				string series=db.Select_Series_QItem(qno,pno);// Request.QueryString["series"].ToString();
				//issue #137 end
				dwg=db.SelectSTDDrawing(qno.ToString(),pno.ToString());
				if(dwg.ToString().Trim() =="")
				{
					if(pno.Substring(0,1) =="P")
					{
						SystemDrawing_SeriesP(qno.Trim(),pno.Trim());
					}
					else if(pno.Substring(0,1) =="M")
					{
						SystemDrawing_SeriesML(qno.Trim(),pno.Trim());
					}
						//issue #137 sart
						//update
						//else if(pno.Substring(0,1) =="A")
						//backup
					else if(pno.Substring(0,1) =="A" || series=="AS")
						//issue #137 end
					{
						//issue #137 start
						if(series=="AS") SystemDrawing_SeriesAS(qno.Trim(),pno.Trim());
							//issue #278 start
						else if(series=="AT") 
						{
							//issue #624 start
							SystemDrawing_SeriesAT(qno.Trim(),pno.Trim());
							//issue #624 end
							//err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
						}
							//issue #278 end
						else
							//issue #137 end
							if(pno.ToString() !="AT")
						{
							SystemDrawing_SeriesA(qno.Trim(),pno.Trim());
						}
					}
					else if(pno.Substring(0,1) =="N")
					{
						SystemDrawing_SeriesN(qno.Trim(),pno.Trim());
					}
					else
					{
						err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
					}
				}
				else if(dwg.ToString().Trim() =="1")
				{
					if(pno.Trim().Substring(0,1) =="Z")
					{
						if(pno.Substring(1,1) =="P")
						{
							SystemDrawing_SeriesP(qno.Trim(),pno.Trim());
						}
						else if(pno.Substring(1,1) =="M")
						{
							SystemDrawing_SeriesML(qno.Trim(),pno.Trim());
						}
						else if(pno.Substring(1,1) =="A")
						{
							//issue #137 start
							if(series=="AS") SystemDrawing_SeriesAS(qno.Trim(),pno.Trim());
								//issue #278 start
							else if(series=="AT")
							{
								//issue #624 start
								SystemDrawing_SeriesAT(qno.Trim(),pno.Trim());
								//issue #624 end
								//err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
							}	
								//issue #278 end
						
							else
								
								//issue #137 end
								if(pno.ToString() !="AT")
							{
								SystemDrawing_SeriesA(qno.Trim(),pno.Trim());
							}
						}
						else if(pno.Substring(0,1) =="N")
						{
							SystemDrawing_SeriesN(qno.Trim(),pno.Trim());
						}
						else
						{
							err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
						}
					}
					else
					{
						if(pno.Substring(0,1) =="P")
						{
							SystemDrawing_SeriesP(qno.Trim(),pno.Trim());
						}
						else if(pno.Substring(0,1) =="A")
						{
							//issue #137 start
							if(series=="AS") SystemDrawing_SeriesAS(qno.Trim(),pno.Trim());
								//issue #278 start
							else if(series=="AT") 
							{
								//issue #624 start
								SystemDrawing_SeriesAT(qno.Trim(),pno.Trim());
								//issue #624 end
								//err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
							}
								//issue #278 end
						
							else
								//issue #137 end
								if(pno.ToString() !="AT")
							{
								SystemDrawing_SeriesA(qno.Trim(),pno.Trim());
							}
						}
						else if(pno.Substring(0,1) =="M")
						{
							SystemDrawing_SeriesML(qno.Trim(),pno.Trim());
						}
						else if(pno.Substring(0,1) =="N")
						{
							SystemDrawing_SeriesN(qno.Trim(),pno.Trim());
						}
						else
						{
							err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
						}
					}
				}
				else if(dwg.ToString().Trim() =="2")
				{
					err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
				}
				else
				{
					err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
				}
			}
		}
		private void SystemDrawing_SeriesP(string qno,string pno)
		{
			string material="";
			string finish="";
			string appopts="";
			string popopts="";
			QItems qitem=new QItems();
			qitem=db.SelectItemsToManage(pno.Trim(),qno.Trim());
			if(qitem.S_Code.Substring(0,1) =="P")
			{
				if(qitem.S_Code.Trim() =="PA")
				{
					material="Aluminum";
				}
				else if(qitem.S_Code.Trim() =="PS")
				{
					material="Steel";
				}
				else if(qitem.S_Code.Trim() =="PC")
				{
					material="Stainless Steel";
				}
				if(qitem.B_Code.Trim() !="W" &&  qitem.B_Code.Trim() !="V" && qitem.B_Code.Trim() !="Q" && 
					qitem.B_Code.Trim() !="T" && qitem.B_Code.Trim() !="X" &&  qitem.B_Code.Trim() !="A" &&
					qitem.B_Code.Trim() !="Y" && qitem.B_Code.Trim() !="W" && qitem.B_Code.Trim() !="B" && 
					qitem.B_Code.Trim() !="F" && qitem.B_Code.Trim() !="I" &&  qitem.B_Code.Trim() !="J" && qitem.B_Code.Trim() !="Z" )
				{
					string xi="";
					string bbc="0";
					string bbh="0";
					string a="";
					string w="";
					string ad="";
					string wd="";
					bool tandem=false;
					ArrayList poplist=new ArrayList();
					Opts opt=new Opts();
					poplist=db.SelectPopts(qno.ToString().Trim(),pno.ToString().Trim());
					if(poplist.Count >0)
					{
						for(int i=0;i<poplist.Count;i++)
						{
							opt=new Opts();
							opt=(Opts)poplist[i];
							if(opt.Code.Substring(0,1)== "M")
							{
								popopts +=opt.Opt.Trim()+", ";
							}
							if(opt.Code.Substring(0,1)== "P")
							{
								popopts +=opt.Opt.Trim()+", ";
							}
							if(opt.Code.Substring(0,2)== "ST")
							{
								decimal dc1=0.00m;
								decimal dc2=0.00m;
								decimal dc3=0.00m;
								dc1=Convert.ToDecimal(qitem.Stroke_Code.Trim());
								dc2=Convert.ToDecimal(opt.Code.Trim().Substring(2));
								dc3= dc1 - dc2;
								popopts +=opt.Opt.Trim()+" ( Effective Stroke="+Decimal.Round(dc3,2)+"\" ), ";
							}
							if(opt.Code.Substring(0,2)== "DS")
							{
								decimal dc1=0.00m;
								decimal dc2=0.00m;
								decimal dc3=0.00m;
								dc1=Convert.ToDecimal(qitem.Stroke_Code.Trim());
								dc2=Convert.ToDecimal(opt.Code.Trim().Substring(3));
								dc3= dc1 - dc2;
								popopts +=opt.Opt.Trim()+" ( Effective Stroke="+Decimal.Round(dc3,2)+"\" ), ";
							}
							if(opt.Code.Substring(0,1)== "C")
							{
								finish=opt.Opt.Trim();
							}
							if(opt.Code.Substring(0,2)=="XI")
							{
								xi=opt.Code.Substring(2);
							}
							if(opt.Code.Substring(0,2)=="BB")
							{
								if(opt.Code.Substring(0,3)=="BBH")
								{
									bbh=opt.Code.Substring(3);
								}
								if(opt.Code.Substring(0,3)=="BBC")
								{
									bbc=opt.Code.Substring(3);
								}
							}
							if(opt.Code.Substring(0,1)=="A" )
							{
								if(opt.Code.Substring(0,2) =="AD")
								{
									ad=opt.Code.Substring(2);
								}
								else
								{
									a=opt.Code.Substring(1);
								}
							}
							if(opt.Code.Substring(0,1)=="W" )
							{
								if(opt.Code.Substring(0,2) =="WD")
								{
									wd=opt.Code.Substring(2);
								}
								else
								{
									w=opt.Code.Substring(1);
								}
							}
							if(opt.Code.Substring(0,2)=="BC" || opt.Code.Substring(0,2)=="TC" || opt.Code.Substring(0,2)=="DC")
							{
								tandem=true;
							}
						}
					}
					if(tandem ==false)
					{
						if( qitem.M_code.Substring(0,2)!="X5"  && qitem.M_code.Substring(0,2)!="X6" && qitem.M_code.Substring(0,2)!="X7")
						{
							if(qitem.M_code.Substring(0,1)=="P" || qitem.M_code.Substring(0,1)=="F"|| qitem.M_code.Substring(0,1)=="S" || qitem.M_code.Substring(0,1)=="E" || qitem.M_code.Substring(0,1)=="T" || qitem.M_code.Substring(0,1)=="X" )
							{
								CrystalDecisions.CrystalReports.Engine.ReportDocument objReport = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
								string path;
								path = Request.PhysicalApplicationPath;
								bool drod=false;
								string secrod="";
								string rendcode="";
								string secrend="";
								ArrayList list=new ArrayList();
								list=db.SelectAopts(qno.ToString().Trim(),pno.ToString().Trim());
								if(list.Count >0)
								{
									for(int i=0;i<list.Count;i++)
									{
										Opts sss =new Opts();
										sss=(Opts)list[i];
										if(sss.Code.Substring(0,1) =="G")
										{
											appopts +=sss.Opt.ToString()+", ";
										}
										if(sss.Code.Substring(0,1) =="P")
										{
											appopts +=sss.Opt.ToString()+", ";
										}
										if(sss.Code.Substring(0,1) =="S")
										{
											appopts +=sss.Opt.ToString()+", ";
										}
										if(sss.Code.Substring(0,1) =="W")
										{
											appopts +=sss.Opt.ToString()+", ";
										}
										if(sss.Code.Substring(0,1) =="R")
										{	
											rendcode=sss.Code.Trim();															
											secrend=sss.Opt.Trim();
											drod=true;
										}
										if(sss.Code.Substring(0,1) =="D")
										{	
											secrod=sss.Code.Trim();	
											appopts +=sss.Opt.ToString()+", ";
											drod=true;
										}
									}
									//								if(secrod.Trim() !="" && rendcode.Trim() !="")
									//								{
									//									string s=secrod.Trim().Substring(1,1)+rendcode.Replace("R","");
									//									string kkval=db.SelectValue("RodEnd_Dimension","WEB_RodEndDiamension_TableV1","RR_Code",s.Trim());
									//									if(kkval.ToString().Trim() !="")
									//									{
									//										kk =" KK="+kk.Trim();
									//									}
									//								}
								}
								string check="";
								if(drod ==false)
								{
									if(qitem.M_code.Substring(0,1) =="P")
									{
										objReport.Load(path +"/crtfiles/SMCDrawings_SeriesP_MP.rpt");
									}
									else if(qitem.M_code.Substring(0,1) =="T")
									{
										objReport.Load(path +"/crtfiles/SMCDrawings_SeriesP_MT.rpt");
									}
									else if(qitem.M_code.Substring(0,1) =="F")
									{
										objReport.Load(path +"/crtfiles/SMCDrawings_SeriesP_MF.rpt");
									}
									else if(qitem.M_code.Substring(0,1) =="S")
									{
										objReport.Load(path +"/crtfiles/SMCDrawings_SeriesP_MS.rpt");
									}
									else if(qitem.M_code.Substring(0,1) =="X")
									{
										objReport.Load(path +"/crtfiles/SMCDrawings_SeriesP_MX.rpt");
									}
									else if(qitem.M_code.Substring(0,1) =="E")
									{
										objReport.Load(path +"/crtfiles/SMCDrawings_SeriesP_ME.rpt");
									}
									check=db.SelectDimDrawing("DWG_Series"+qitem.S_Code.Substring(0,1)+"_"+qitem.M_code.Substring(0,1)+"_TableV1",qitem.B_Code.Trim(),qitem.R_Code.Trim());

								}
								else if(drod ==true)
								{
									if(qitem.M_code.Substring(0,1) =="T")
									{
										objReport.Load(path +"/crtfiles/SMCDrawings_SeriesP_MT_DBL.rpt");
									}
									else if(qitem.M_code.Substring(0,1) =="F")
									{
										objReport.Load(path +"/crtfiles/SMCDrawings_SeriesP_MF_DBL.rpt");
									}
									else if(qitem.M_code.Substring(0,1) =="S")
									{
										objReport.Load(path +"/crtfiles/SMCDrawings_SeriesP_MS_DBL.rpt");
									}
									else if(qitem.M_code.Substring(0,1) =="X")
									{
										objReport.Load(path +"/crtfiles/SMCDrawings_SeriesP_MX_DBL.rpt");
									}
									else if(qitem.M_code.Substring(0,1) =="E")
									{
										objReport.Load(path +"/crtfiles/SMCDrawings_SeriesP_ME_DBL.rpt");
									}
									check=db.SelectDimDrawing("DWG_Series"+qitem.S_Code.Substring(0,1)+"_DBL_"+qitem.M_code.Substring(0,1)+"_TableV1",qitem.B_Code.Trim(),qitem.R_Code.Trim());
								}
								if(check.Trim() !="")
								{
									string strCurrentDir = Server.MapPath(".") + "/quote/";
									//									fn.RemoveFiles(strCurrentDir);
									objReport.Refresh();
                                    objReport.SetDatabaseLogon(DBClass.ICylDBLogin, DBClass.ICylDBPassword, DBClass.ICylDBServer, DBClass.ICylDB);
									objReport.Refresh(); 
									objReport.SetParameterValue("@parm1",qitem.B_Code.Trim());
									objReport.SetParameterValue("@parm2",qitem.R_Code.Trim());
									objReport.SetParameterValue("@stroke",qitem.Stroke_Code.Trim());
									objReport.SetParameterValue("@pno",qitem.PartNo.Trim());
									objReport.SetParameterValue("@qno",qitem.Quotation_No.Trim());
									objReport.SetParameterValue("@material",material.Trim());
									objReport.SetParameterValue("@finish",finish.Trim());
									objReport.SetParameterValue("@customerref",qitem.Note.Trim());
									objReport.SetParameterValue("@ports",qitem.Port.Trim()+" ("+qitem.PortPos.Trim()+")");
									string cushions="";
									if(qitem.Cu_Code.Trim().Substring(0,1) !="8")
									{
										cushions=qitem.Cushion.Trim()+" ("+qitem.CushionPos.Trim()+")";
									}
									else
									{
										cushions=qitem.Cushion.Trim();
									}
									objReport.SetParameterValue("@cushions",cushions.Trim());
									objReport.SetParameterValue("@rodend",qitem.RodEnd.Trim());
									objReport.SetParameterValue("@mount",qitem.M_code.Trim());
									objReport.SetParameterValue("@desc",qitem.Bore.Trim().Replace("Size","").ToUpper()+" - "+ qitem.Rod.Trim().Replace("Size","").ToUpper()+" - "+qitem.Stroke_Code.Trim()+"\" STROKE");
									objReport.SetParameterValue("@weight",Regex.Replace(qitem.Weight.Trim(), "[^0-9.]", "") + " LBS" );
									string rodendtype="";
									if(qitem.RE_Code.Trim() =="N1" || qitem.RE_Code.Trim() =="M1" || qitem.RE_Code.Trim() =="N5" || qitem.RE_Code.Trim().Substring(0,1) =="Y" || qitem.RE_Code.Trim().Substring(0,1) =="Z" )
									{
										rodendtype="sm";
									}
									else if(qitem.RE_Code.Trim() =="N4"|| qitem.RE_Code.Trim() =="M4" || qitem.RE_Code.Trim() =="N6" || qitem.RE_Code.Trim().Substring(0,1) =="X" || qitem.RE_Code.Trim().Substring(0,1) =="W")
									{
										rodendtype="sf";
									}
									else if(qitem.RE_Code.Trim() =="N3" || qitem.RE_Code.Trim() =="M3" || qitem.RE_Code.Trim() =="N2" || qitem.RE_Code.Trim() =="M2")
									{
										rodendtype="sm";
									}
									else
									{
										rodendtype="sm";
									}
									string sec_rodendtype="";
									if(secrod.Trim() !="" && rendcode.Trim() !="")
									{
										if(rendcode.Substring(1,2) =="N1" || rendcode.Substring(1,2) =="M1" || rendcode.Substring(1,2) =="N5"  || rendcode.Substring(1,1) =="Y" || rendcode.Substring(1,1) =="Z")
										{
											sec_rodendtype="sm";
										}
										else if(rendcode.Substring(1,2) =="N4"|| rendcode.Substring(1,2) =="M4" || rendcode.Substring(1,2) =="N6" || rendcode.Substring(1,1) =="X" || rendcode.Substring(1,1) =="W")
										{
											sec_rodendtype="sf";
										}
										else if(rendcode.Substring(1,2) =="N3" || rendcode.Substring(1,2) =="M3" || rendcode.Substring(1,2) =="N2" || rendcode.Substring(1,2) =="M2")
										{
											sec_rodendtype="sm";
										}
										else
										{
											sec_rodendtype="sm";
										}
									}
									string bore="";
									if(qitem.B_Code.Trim() =="C" || qitem.B_Code.Trim() =="D" || qitem.B_Code.Trim() =="E" || qitem.B_Code.Trim() =="G"
										|| qitem.B_Code.Trim() =="H" || qitem.B_Code.Trim() =="J" || qitem.B_Code.Trim() =="K" || qitem.B_Code.Trim() =="L" || qitem.B_Code.Trim() =="M")
									{
										bore="no";
									}
									else
									{
										bore="yes";
									}
									objReport.SetParameterValue("@bore",bore.Trim());
									objReport.SetParameterValue("@rodendtype",rodendtype.Trim());
									if(drod ==true)
									{
										objReport.SetParameterValue("@rodend",qitem.RodEnd.Trim()+", "+secrend.Trim());
										objReport.SetParameterValue("@sec_rodendtype",sec_rodendtype.Trim());
										if(ad.Trim() !="")
										{
											objReport.SetParameterValue("@dim_ad",ad.Trim());	
										}
										else
										{
											objReport.SetParameterValue("@dim_ad","0");	
										}
										if(wd.Trim() !="")
										{
											objReport.SetParameterValue("@dim_wd",wd.Trim());	
										}
										else
										{
											objReport.SetParameterValue("@dim_wd","0");	
										}
									}
									if(a.Trim() !="")
									{
										objReport.SetParameterValue("@dim_a",a.Trim());	
									}
									else
									{
										objReport.SetParameterValue("@dim_a","0");	
									}
									if(w.Trim() !="")
									{
										objReport.SetParameterValue("@dim_w",w.Trim());	
										decimal wdim=0.00m;
										wdim=db.SelectAddersPrice("W1","STD_W_Values_TableV1",qitem.B_Code.Trim(),qitem.R_Code.Trim());
										objReport.SetParameterValue("@std_dim_w",wdim.ToString().Trim());	
									}
									else
									{
										objReport.SetParameterValue("@dim_w","0");	
										objReport.SetParameterValue("@std_dim_w","0");	
									}
									if(qitem.M_code.Substring(0,1)=="T")
									{
										if(qitem.M_code.Substring(0,2)=="T4")
										{
											objReport.SetParameterValue("@xi",xi.ToString());
										}
										else
										{
											objReport.SetParameterValue("@xi","0");
										}
									}
									if(qitem.M_code.Substring(0,1)=="X")
									{
										objReport.SetParameterValue("@bbh",bbh.ToString());
										objReport.SetParameterValue("@bbc",bbc.ToString());
									}
									ArrayList splist=new ArrayList();
									splist=db.FindAllSpecials_WEB(qno.ToString().Trim(),pno.Trim());
									if(splist.Count >0)
									{
										string tem="";
										Specials sp=new Specials();
										for(int l=0;l<splist.Count;l++)
										{
											sp=new Specials();
											sp=(Specials)splist[l];
											tem +=sp.SPDesc.ToString();
											if(l < splist.Count -1)
											{
												tem +=", ";
											}
										}
										objReport.SetParameterValue("@specials",appopts.ToString()+popopts.ToString()+ tem.ToString());
									}
									else
									{
										objReport.SetParameterValue("@specials",appopts.ToString()+popopts.ToString());
									}
									ArrayList selist =new ArrayList();
									selist =(ArrayList)Session["User"];
									//issue #644 test start
									for(int i=0;i<selist.Count;i++)
									{
										string strTest = (String)selist[i];
                                        strTest = (String)selist[i];
									}
									//issue #644 test end
									objReport.SetParameterValue("@customer",selist[6].ToString().Trim());
									string psi="";
									psi=db.SelectOneValueFunction(qitem.S_Code.ToString().Trim(),"sp_SelectSeriesPSI");
									if(psi.Trim() !="")
									{
										objReport.SetParameterValue("@seriespsi",psi.ToString().Trim());
									}
									CrystalDecisions.Shared.DiskFileDestinationOptions diskOpts = new CrystalDecisions.Shared.DiskFileDestinationOptions();
									objReport.ExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
									objReport.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
									diskOpts.DiskFileName = path+"/quote/"+pno.Trim().Replace("/","")+".pdf";
									objReport.ExportOptions.DestinationOptions = diskOpts;
									objReport.Export();
									Response.Redirect("quote/"+pno.Trim().Replace("/","")+".pdf");
								}
								else
								{
									err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
								}
							}
							else
							{
								err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
							}
						}
						else
						{
							err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
						}
					}
					else
					{
						err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
					}
				}
				else
				{
					err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
				}
			}
			else
			{
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
			}
		}
		private void SystemDrawing_SeriesML(string qno,string pno)
		{
			string material="";
			string finish="";
			string appopts="";
			string popopts="";
			QItems qitem=new QItems();
			qitem=db.SelectItemsToManage(pno.Trim(),qno.Trim());
			//issue #107 start
			if(qitem.S_Code!=null)
			{
			//issue #107 end
			//issue #114 start
			//backup
				if(qitem.S_Code.Substring(0,1) =="M")
            //update
			    //if(qitem.S_Code.Substring(0,1) =="M" && qitem.RodEnd!="Plain Rod end(with out wrench flats)")
			//issue #114 end
				{
					material="";
					if(qitem.B_Code.Trim() !="W" &&  qitem.B_Code.Trim() !="V" && qitem.B_Code.Trim() !="Q" && 
						qitem.B_Code.Trim() !="T" && qitem.B_Code.Trim() !="X" &&  qitem.B_Code.Trim() !="A" &&
						qitem.B_Code.Trim() !="Y" && qitem.B_Code.Trim() !="W" && qitem.B_Code.Trim() !="B" && 
						qitem.B_Code.Trim() !="F" && qitem.B_Code.Trim() !="I" &&  qitem.B_Code.Trim() !="J" && qitem.B_Code.Trim() !="Z" &&
						qitem.RodEnd!="Plain Rod end(with out wrench flats)"
						)
					{
						string xi="";
						string bbc="0";
						string bbh="0";
						string a="";
						string w="";
						string ad="";
						string wd="";
						bool tandem=false;
						ArrayList poplist=new ArrayList();
						Opts opt=new Opts();
						poplist=db.SelectPopts(qno.ToString().Trim(),pno.ToString().Trim());
						if(poplist.Count >0)
						{
							for(int i=0;i<poplist.Count;i++)
							{
								opt=new Opts();
								opt=(Opts)poplist[i];
								if(opt.Code.Substring(0,1)== "M")
								{
									popopts +=opt.Opt.Trim()+", ";
								}
								if(opt.Code.Substring(0,1)== "P")
								{
									popopts +=opt.Opt.Trim()+", ";
								}
								if(opt.Code.Substring(0,2)== "ST")
								{
									decimal dc1=0.00m;
									decimal dc2=0.00m;
									decimal dc3=0.00m;
									dc1=Convert.ToDecimal(qitem.Stroke_Code.Trim());
									dc2=Convert.ToDecimal(opt.Code.Trim().Substring(2));
									dc3= dc1 - dc2;
									popopts +=opt.Opt.Trim()+" ( Effective Stroke="+Decimal.Round(dc3,2)+"\" ), ";
								}
								if(opt.Code.Substring(0,2)== "DS")
								{
									decimal dc1=0.00m;
									decimal dc2=0.00m;
									decimal dc3=0.00m;
									dc1=Convert.ToDecimal(qitem.Stroke_Code.Trim());
									dc2=Convert.ToDecimal(opt.Code.Trim().Substring(3));
									dc3= dc1 - dc2;
									popopts +=opt.Opt.Trim()+" ( Effective Stroke="+Decimal.Round(dc3,2)+"\" ), ";
								}
								if(opt.Code.Substring(0,1)== "C")
								{
									finish=opt.Opt.Trim();
								}
								if(opt.Code.Substring(0,2)=="XI")
								{
									xi=opt.Code.Substring(2);
								}
								if(opt.Code.Substring(0,2)=="BB")
								{
									if(opt.Code.Substring(0,3)=="BBH")
									{
										bbh=opt.Code.Substring(3);
									}
									if(opt.Code.Substring(0,3)=="BBC")
									{
										bbc=opt.Code.Substring(3);
									}
								}
								if(opt.Code.Substring(0,1)=="A" )
								{
									if(opt.Code.Substring(0,2) =="AD")
									{
										ad=opt.Code.Substring(2);
									}
									else
									{
										a=opt.Code.Substring(1);
									}
								}
								if(opt.Code.Substring(0,1)=="W" )
								{
									if(opt.Code.Substring(0,2) =="WD")
									{
										wd=opt.Code.Substring(2);
									}
									else
									{
										w=opt.Code.Substring(1);
									}
								}
								if(opt.Code.Substring(0,2)=="BC" || opt.Code.Substring(0,2)=="TC" || opt.Code.Substring(0,2)=="DC")
								{
									tandem=true;
								}
							}
						}
						if(tandem ==false)
						{
							if(qitem.M_code.Substring(0,2)!="SH" )
							{
								if(qitem.M_code.Substring(0,1)=="P" || qitem.M_code.Substring(0,1)=="F"|| qitem.M_code.Substring(0,1)=="S" || qitem.M_code.Substring(0,1)=="E" || qitem.M_code.Substring(0,1)=="T" || qitem.M_code.Substring(0,1)=="X")
								{
									CrystalDecisions.CrystalReports.Engine.ReportDocument objReport = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
									string path;
									path = Request.PhysicalApplicationPath;
									bool drod=false;
									string secrod="";
									string rendcode="";
									string secrend="";
									ArrayList list=new ArrayList();
									list=db.SelectAopts(qno.ToString().Trim(),pno.ToString().Trim());
									if(list.Count >0)
									{
										for(int i=0;i<list.Count;i++)
										{
											Opts sss =new Opts();
											sss=(Opts)list[i];
											if(sss.Code.Substring(0,1) =="G")
											{
												appopts +=sss.Opt.ToString()+", ";
											}
											if(sss.Code.Substring(0,1) =="P")
											{
												appopts +=sss.Opt.ToString()+", ";
											}
											if(sss.Code.Substring(0,1) =="S")
											{
												appopts +=sss.Opt.ToString()+", ";
											}
											if(sss.Code.Substring(0,1) =="W")
											{
												appopts +=sss.Opt.ToString()+", ";
											}
											if(sss.Code.Substring(0,1) =="R")
											{	
												rendcode=sss.Code.Trim();															
												secrend=sss.Opt.Trim();
												drod=true;
											}
											if(sss.Code.Substring(0,1) =="D")
											{	
												secrod=sss.Code.Trim();	
												appopts +=sss.Opt.ToString()+", ";
												drod=true;
											}
										}
										//								if(secrod.Trim() !="" && rendcode.Trim() !="")
										//								{
										//									string s=secrod.Trim().Substring(1,1)+rendcode.Replace("R","");
										//									string kkval=db.SelectValue("RodEnd_Dimension","WEB_RodEndDiamension_TableV1","RR_Code",s.Trim());
										//									if(kkval.ToString().Trim() !="")
										//									{
										//										kk =" KK="+kk.Trim();
										//									}
										//								}
									}
									string check="";
									if(drod ==false)
									{
										if(qitem.M_code.Substring(0,1) =="P")
										{
											objReport.Load(path +"/crtfiles/Drawings_SeriesML_MP.rpt");
										}
										else if(qitem.M_code.Substring(0,1) =="T")
										{
											objReport.Load(path +"/crtfiles/Drawings_SeriesML_MT.rpt");
										}
										else if(qitem.M_code.Substring(0,1) =="F")
										{
											objReport.Load(path +"/crtfiles/Drawings_SeriesML_MF.rpt");
										}
										else if(qitem.M_code.Substring(0,1) =="S")
										{
											objReport.Load(path +"/crtfiles/Drawings_SeriesML_MS.rpt");
										}
										else if(qitem.M_code.Substring(0,1) =="X")
										{
											objReport.Load(path +"/crtfiles/Drawings_SeriesML_MX.rpt");
										}
										else if(qitem.M_code.Substring(0,1) =="E")
										{
											objReport.Load(path +"/crtfiles/Drawings_SeriesML_ME.rpt");
										}
										check=db.SelectDimDrawing("DWG_Series"+qitem.S_Code.Substring(0,2)+"_"+qitem.M_code.Substring(0,1)+"_TableV1",qitem.B_Code.Trim(),qitem.R_Code.Trim());

									}
									//							else if(drod ==true)
									//							{
									//								if(qitem.M_code.Substring(0,1) =="T")
									//								{
									//									objReport.Load(path +"/crtfiles/SMCDrawings_SeriesP_MT_DBL.rpt");
									//								}
									//								else if(qitem.M_code.Substring(0,1) =="F")
									//								{
									//									objReport.Load(path +"/crtfiles/SMCDrawings_SeriesP_MF_DBL.rpt");
									//								}
									//								else if(qitem.M_code.Substring(0,1) =="S")
									//								{
									//									objReport.Load(path +"/crtfiles/SMCDrawings_SeriesP_MS_DBL.rpt");
									//								}
									//								else if(qitem.M_code.Substring(0,1) =="X")
									//								{
									//									objReport.Load(path +"/crtfiles/SMCDrawings_SeriesP_MX_DBL.rpt");
									//								}
									//								else if(qitem.M_code.Substring(0,1) =="E")
									//								{
									//									objReport.Load(path +"/crtfiles/SMCDrawings_SeriesP_ME_DBL.rpt");
									//								}
									//								check=db.SelectDimDrawing("DWG_Series"+qitem.S_Code.Substring(0,1)+"_DBL_"+qitem.M_code.Substring(0,1)+"_TableV1",qitem.B_Code.Trim(),qitem.R_Code.Trim());
									//							}
									if(check.Trim() !="")
									{
										string strCurrentDir = Server.MapPath(".") + "/quote/";
										//									fn.RemoveFiles(strCurrentDir);
										objReport.Refresh();
                                        objReport.SetDatabaseLogon(DBClass.ICylDBLogin, DBClass.ICylDBPassword, DBClass.ICylDBServer, DBClass.ICylDB);
										objReport.Refresh(); 
										objReport.SetParameterValue("@parm1",qitem.B_Code.Trim());
										objReport.SetParameterValue("@parm2",qitem.R_Code.Trim());
										objReport.SetParameterValue("@stroke",qitem.Stroke_Code.Trim());
										objReport.SetParameterValue("@pno",qitem.PartNo.Trim());
										objReport.SetParameterValue("@qno",qitem.Quotation_No.Trim());
										objReport.SetParameterValue("@material",material.Trim());
										objReport.SetParameterValue("@finish",finish.Trim());
										objReport.SetParameterValue("@customerref",qitem.Note.Trim());
										objReport.SetParameterValue("@ports",qitem.Port.Trim()+" ("+qitem.PortPos.Trim()+")");
										string cushions="";
										if(qitem.Cu_Code.Trim().Substring(0,1) !="8")
										{
											cushions=qitem.Cushion.Trim()+" ("+qitem.CushionPos.Trim()+")";
										}
										else
										{
											cushions=qitem.Cushion.Trim();
										}
										objReport.SetParameterValue("@cushions",cushions.Trim());
										objReport.SetParameterValue("@rodend",qitem.RodEnd.Trim());
										objReport.SetParameterValue("@mount",qitem.M_code.Trim());
										objReport.SetParameterValue("@desc",qitem.Bore.Trim().Replace("Size","").ToUpper()+" - "+ qitem.Rod.Trim().Replace("Size","").ToUpper()+" - "+qitem.Stroke_Code.Trim()+"\" STROKE");
                                        objReport.SetParameterValue("@weight", Regex.Replace(qitem.Weight.Trim(), "[^0-9.]", "") + " LBS");
										string rodendtype="";
										if(qitem.RE_Code.Trim() =="N1" || qitem.RE_Code.Trim() =="M1" || qitem.RE_Code.Trim() =="N5" || qitem.RE_Code.Trim().Substring(0,1) =="Y" || qitem.RE_Code.Trim().Substring(0,1) =="Z" )
										{
											rodendtype="sm";
										}
										else if(qitem.RE_Code.Trim() =="N4"|| qitem.RE_Code.Trim() =="M4" || qitem.RE_Code.Trim() =="N6" || qitem.RE_Code.Trim().Substring(0,1) =="X" || qitem.RE_Code.Trim().Substring(0,1) =="W")
										{
											rodendtype="sf";
										}
										else if(qitem.RE_Code.Trim() =="N3" || qitem.RE_Code.Trim() =="M3" || qitem.RE_Code.Trim() =="N2" || qitem.RE_Code.Trim() =="M2")
										{
											rodendtype="sm";
										}
										else
										{
											rodendtype="sm";
										}
										string sec_rodendtype="";
										if(secrod.Trim() !="" && rendcode.Trim() !="")
										{
											if(rendcode.Substring(1,2) =="N1" || rendcode.Substring(1,2) =="M1" || rendcode.Substring(1,2) =="N5"  || rendcode.Substring(1,1) =="Y" || rendcode.Substring(1,1) =="Z")
											{
												sec_rodendtype="sm";
											}
											else if(rendcode.Substring(1,2) =="N4"|| rendcode.Substring(1,2) =="M4" || rendcode.Substring(1,2) =="N6" || rendcode.Substring(1,1) =="X" || rendcode.Substring(1,1) =="W")
											{
												sec_rodendtype="sf";
											}
											else if(rendcode.Substring(1,2) =="N3" || rendcode.Substring(1,2) =="M3" || rendcode.Substring(1,2) =="N2" || rendcode.Substring(1,2) =="M2")
											{
												sec_rodendtype="sm";
											}
											else
											{
												sec_rodendtype="sm";
											}
										}
										string bore="";
										if(qitem.B_Code.Trim() =="C" || qitem.B_Code.Trim() =="D" || qitem.B_Code.Trim() =="E" || qitem.B_Code.Trim() =="G"
											|| qitem.B_Code.Trim() =="H" || qitem.B_Code.Trim() =="J" || qitem.B_Code.Trim() =="K" || qitem.B_Code.Trim() =="L" )
										{
											bore="no";
										}
										else if(qitem.B_Code.Trim() =="M" || qitem.B_Code.Trim() =="N")
										{
											bore="yn";
										}
										else
										{
											bore="yes";
										}
										objReport.SetParameterValue("@bore",bore.Trim());
										objReport.SetParameterValue("@rodendtype",rodendtype.Trim());
										if(drod ==true)
										{
											objReport.SetParameterValue("@rodend",qitem.RodEnd.Trim()+", "+secrend.Trim());
											objReport.SetParameterValue("@sec_rodendtype",sec_rodendtype.Trim());
											if(ad.Trim() !="")
											{
												objReport.SetParameterValue("@dim_ad",ad.Trim());	
											}
											else
											{
												objReport.SetParameterValue("@dim_ad","0");	
											}
											if(wd.Trim() !="")
											{
												objReport.SetParameterValue("@dim_wd",wd.Trim());	
											}
											else
											{
												objReport.SetParameterValue("@dim_wd","0");	
											}
										}
										if(a.Trim() !="")
										{
											objReport.SetParameterValue("@dim_a",a.Trim());	
										}
										else
										{
											objReport.SetParameterValue("@dim_a","0");	
										}
										if(w.Trim() !="")
										{
											objReport.SetParameterValue("@dim_w",w.Trim());	
											decimal wdim=0.00m;
											wdim=db.SelectAddersPrice("W","STD_WH_Values_TableV1",qitem.B_Code.Trim(),qitem.R_Code.Trim());
											objReport.SetParameterValue("@std_dim_w",wdim.ToString().Trim());	
										}
										else
										{
											objReport.SetParameterValue("@dim_w","0");	
											objReport.SetParameterValue("@std_dim_w","0");	
										}
										if(qitem.M_code.Substring(0,1)=="T")
										{
											if(qitem.M_code.Substring(0,2)=="T4")
											{
												//issue #649
												objReport.SetParameterValue("@xi",xi.ToString());
											}
											else
											{
												objReport.SetParameterValue("@xi","0");
											}
										}
										if(qitem.M_code.Substring(0,1)=="X")
										{
											objReport.SetParameterValue("@bbh",bbh.ToString());
											objReport.SetParameterValue("@bbc",bbc.ToString());
										}
										string comment="";
										if(qitem.Cusomer_ID.Trim()=="Rotork" || qitem.Cusomer_ID.Trim()=="Rotork - USA" )
										{										
											comment=qitem.SpecialReq.Trim();
										}
										ArrayList splist=new ArrayList();
										splist=db.FindAllSpecials_WEB(qno.ToString().Trim(),pno.Trim());
										if(splist.Count >0)
										{
											string tem="";
											Specials sp=new Specials();
											for(int l=0;l<splist.Count;l++)
											{
												sp=new Specials();
												sp=(Specials)splist[l];
												tem +=sp.SPDesc.ToString();
												if(l < splist.Count -1)
												{
													tem +=", ";
												}
											}
											objReport.SetParameterValue("@specials",appopts.ToString()+popopts.ToString()+ tem.ToString()+comment.Trim());
										}
										else
										{
											objReport.SetParameterValue("@specials",appopts.ToString()+popopts.ToString()+comment.Trim());
										}									
										string psi="";
										psi=db.SelectOneValueFunction(qitem.S_Code.ToString().Trim(),"sp_SelectSeriesPSI");
										if(psi.Trim() !="")
										{
											objReport.SetParameterValue("@seriespsi",psi.ToString().Trim());
										}
										//issue #646 start
										//backu
										objReport.SetParameterValue("@customer",qitem.Cusomer_ID.Trim());
										//update
                                        //ArrayList selist =new ArrayList();
                                        //selist =(ArrayList)Session["User"];
                                        //if(selist[6].ToString().Trim()=="1030")
                                        //{
                                        //    objReport.SetParameterValue("@customer",selist[6].ToString().Trim());
                                        //}
										//issue #646 end 
										CrystalDecisions.Shared.DiskFileDestinationOptions diskOpts = new CrystalDecisions.Shared.DiskFileDestinationOptions();
										objReport.ExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
										objReport.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
										diskOpts.DiskFileName = path+"/quote/"+pno.Trim().Replace("/","")+".pdf";
										objReport.ExportOptions.DestinationOptions = diskOpts;
										objReport.Export();
										Response.Redirect("quote/"+pno.Trim().Replace("/","")+".pdf");
									}
									else
									{
										err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
									}
								}
								else
								{
									err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
								}
							}
							else
							{
								err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
							}
						}
						else
						{
							err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
						}
					}
					else
					{
						err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
					}
				}
				}
            //issue #114 start
//			else
//			{
//				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
//			}
			//issue #114 end

		}
		
		//issue #624 start
		private void SystemDrawing_SeriesAT(string qno,string pno)
		{
			try
			{
				QItems qitem=new QItems();
				qitem=db.SelectItemsToManage(pno.Trim(),qno.Trim());
				string strBoreCat = db.ATSeries_Get_BoreCat(qitem.B_Code);
				string strT = "";
				strT=db.ATSeries_Get_T(qno, pno);
				string strISO = "";
				string strX0="1";
				if(qitem.M_code.Substring(0,1)=="I")strISO="IM";
				else if(qitem.M_code.Substring(0,1)=="M")strISO="IM";
				else if(qitem.M_code.Substring(0,1)=="X")
				{
					strISO="XN";
					if(qitem.M_code=="X0")
					{
						strX0="0"; 
						strISO="X0";
					}
				}
				string path="";
				string strReport="";
				CrystalDecisions.CrystalReports.Engine.ReportDocument objReport = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
				path = Request.PhysicalApplicationPath;
				switch (strBoreCat)
				{
					case "lowBore":
					switch(strISO)
					{
						case "X0":
							strReport = "crtfiles/Drawings_SeriesAT_L_X0.rpt";
							break;
						case "XN":
							strReport = "crtfiles/Drawings_SeriesAT_L_XN.rpt";
							break;
						case "IM":
							strReport = "crtfiles/Drawings_SeriesAT_L_IM.rpt";
							break;
						
					}
						break;
					case "highBore":
					switch(strISO)
					{
						case "X0":
							strReport = "crtfiles/Drawings_SeriesAT_H_X0.rpt";
							break;
						case "XN":
							strReport = "crtfiles/Drawings_SeriesAT_H_XN.rpt";
							break;
						case "IM":
							strReport = "crtfiles/Drawings_SeriesAT_H_IM.rpt";
							break;
						
					}
						break;
					case "outOfRange":
						break;

				}
				if(strReport!="")
				{
					objReport.Load(path +strReport);
					objReport.Refresh();
                    objReport.SetDatabaseLogon(DBClass.ICylDBLogin, DBClass.ICylDBPassword, DBClass.ICylDBServer, DBClass.ICylDB);
					objReport.SetParameterValue("@bcode",qitem.B_Code);
					objReport.SetParameterValue("@stroke",qitem.Stroke_Code);
					objReport.SetParameterValue("@t",strT);
					objReport.SetParameterValue("@qno",qno.Trim());
					objReport.SetParameterValue("@pno",pno.Trim());
					objReport.SetParameterValue("@mount",qitem.M_code);
					objReport.SetParameterValue("@seriesdesc","");
					objReport.SetParameterValue("@bore",qitem.Bore.Replace("Size",""));
					objReport.SetParameterValue("@rod",qitem.Rod.Replace("Size",""));
					objReport.SetParameterValue("@strokedesc",qitem.Stroke);
					//if(strISO=="X") objReport.SetParameterValue("@x",strX0);
					CrystalDecisions.Shared.DiskFileDestinationOptions diskOpts = new CrystalDecisions.Shared.DiskFileDestinationOptions();
					objReport.ExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
					objReport.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
					//diskOpts.DiskFileName = path+"/quote/"+ pno.Trim().Replace("/","")+".pdf";
					diskOpts.DiskFileName = path+"/quote/"+pno.Trim().Replace("/","")+".pdf";
					objReport.ExportOptions.DestinationOptions = diskOpts;
					objReport.Export();
					Response.Redirect("quote/"+pno.Trim().Replace("/","")+".pdf");
				}
				else err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
			}
			catch(Exception ex)
			{
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+ex.Message+"')</script>";
			}

		}
		
		//issue #624 end
		//issue #137 start
		private void SystemDrawing_SeriesAS(string qno,string pno)
		{
			try
			{
				QItems qitem=new QItems();
				qitem=db.SelectItemsToManage(pno.Trim(),qno.Trim());
				string strTandem = db.ASSeries_Has_Tandem(qno, pno);
				string strRodEndG = "";
				strRodEndG=db.ASSeries_Has_RodEnd_Male(qitem.RE_Code);
				string strISO = "";
				string strX0="1";
				if(qitem.M_code.Substring(0,1)=="I")strISO="ISO";
				else if(qitem.M_code.Substring(0,1)=="M")strISO="MSS";
				else if(qitem.M_code.Substring(0,1)=="X")
				{
					strISO="X";
					if(qitem.M_code=="X0")strX0="0"; 
				}
				//issue #242 start
				else if(qitem.M_code=="Z07" || qitem.M_code=="Z10" || qitem.M_code=="Z14" || qitem.M_code=="Z16" || qitem.M_code=="Z25") strISO="Z";
				//issue #242 end
				string path="";
				string strReport="";
				CrystalDecisions.CrystalReports.Engine.ReportDocument objReport = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
				path = Request.PhysicalApplicationPath;
				switch (strTandem)
				{
					case "":
					switch(strISO)
					{
						case "ISO":
							strReport = "crtfiles/Drawings_SeriesAS_S_I.rpt";
							break;
						case "MSS":
							strReport = "crtfiles/Drawings_SeriesAS_S_M.rpt";
							break;
							//issue #242 start
						case "Z":
							strReport = "crtfiles/Drawings_SeriesAS_S_Z.rpt";
							break;
							//issue #242 end
						case "X":
							strReport = "crtfiles/Drawings_SeriesAS_S_X.rpt";
							//issue #242 start
							if(qitem.Port_Code!=null)
							{
								if(qitem.Port_Code=="B") strReport = "crtfiles/Drawings_SeriesAS_B_S_X.rpt";
							}
							//issue #242 end
							break;
					}
						break;
					case "TC":
					switch(strISO)
					{
						case "ISO":
							strReport = "crtfiles/Drawings_SeriesAS_T_I.rpt";
							break;
						case "MSS":
							strReport = "crtfiles/Drawings_SeriesAS_T_M.rpt";
							break;
						//issue #242 start
						case "Z":
							strReport = "crtfiles/Drawings_SeriesAS_T_Z.rpt";
							break;
						//issue #242 end
						case "X":
							strReport = "crtfiles/Drawings_SeriesAS_T_X.rpt";
							//issue #242 start
							if(qitem.Port_Code!=null)
							{
								if(qitem.Port_Code=="B") strReport = "crtfiles/Drawings_SeriesAS_B_T_X.rpt";
							}
							//issue #242 end
							break;
					}
						break;

				}
				if(strReport!="")
				{
					objReport.Load(path +strReport);
					objReport.Refresh();
                    objReport.SetDatabaseLogon(DBClass.ICylDBLogin, DBClass.ICylDBPassword, DBClass.ICylDBServer, DBClass.ICylDB);
					objReport.SetParameterValue("@quoteno",qno.Trim());
					objReport.SetParameterValue("@partno",pno.Trim());
					objReport.SetParameterValue("@rodendg",strRodEndG);
					if(strISO=="X") objReport.SetParameterValue("@x",strX0);
					//issue #242 start
//					if(qitem.Port_Code!=null)
//					{
//						if(qitem.Port_Code=="B") objReport.SetParameterValue("@portsize",qitem.Port);
//					}
					//issue #242 end
					CrystalDecisions.Shared.DiskFileDestinationOptions diskOpts = new CrystalDecisions.Shared.DiskFileDestinationOptions();
					objReport.ExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
					objReport.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
					//diskOpts.DiskFileName = path+"/quote/"+ pno.Trim()+".pdf";
					diskOpts.DiskFileName = path+"/quote/"+ pno.Trim().Replace("/","")+".pdf";
					objReport.ExportOptions.DestinationOptions = diskOpts;
					objReport.Export();
					Response.Redirect("quote/"+pno.Trim().Replace("/","")+".pdf");
				}
				else err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
			}
			catch(Exception ex)
			{
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+ex.Message+"')</script>";
			}

		}
		//issue #137 end
		
		
		
		
		private void SystemDrawing_SeriesA(string qno,string pno)
		{
			string material="";
			string finish="";
			string appopts="";
			string popopts="";
			QItems qitem=new QItems();
			qitem=db.SelectItemsToManage(pno.Trim(),qno.Trim());
			//issue #107 start
			if(qitem!=null && qitem.S_Code!=null)
			{
				string debug =qitem.S_Code;
				//issue #107 end 
				if(qitem.S_Code.Substring(0,1) =="A")
				{
					if(qitem.S_Code.Trim() =="A")
					{
						material="";
					}
					//issue #251 start
//					if(qitem.B_Code.Trim() !="W" &&  qitem.B_Code.Trim() !="V" && qitem.B_Code.Trim() !="Q" && 
//						qitem.B_Code.Trim() !="T" && qitem.B_Code.Trim() !="X" &&  qitem.B_Code.Trim() !="A" &&
//						qitem.B_Code.Trim() !="Y" && qitem.B_Code.Trim() !="W" && qitem.B_Code.Trim() !="B" && 
//						qitem.B_Code.Trim() !="F" && qitem.B_Code.Trim() !="I" &&  qitem.B_Code.Trim() !="J" && 
//						qitem.B_Code.Trim() !="Z" )
					//update
					if(qitem.B_Code.Trim() !="Z" &&  qitem.B_Code.Trim() !="V" && qitem.B_Code.Trim() !="Q")
					//issue #251 end
					{
						string xi="";
						string bbc="0";
						string bbh="0";
						string a="";
						string w="";
						string ad="";
						string wd="";
						bool tandem=false;
						bool tandemduplex=false;
						ArrayList poplist=new ArrayList();
						Opts opt=new Opts();
						poplist=db.SelectPopts(qno.ToString().Trim(),pno.ToString().Trim());
						if(poplist.Count >0)
						{
							for(int i=0;i<poplist.Count;i++)
							{
								opt=new Opts();
								opt=(Opts)poplist[i];
								if(opt.Code.Substring(0,1)== "M")
								{
									popopts +=opt.Opt.Trim()+", ";
								}
								if(opt.Code.Substring(0,1)== "P")
								{
									popopts +=opt.Opt.Trim()+", ";
								}
								if(opt.Code.Substring(0,2)== "ST")
								{
									decimal dc1=0.00m;
									decimal dc2=0.00m;
									decimal dc3=0.00m;
									dc1=Convert.ToDecimal(qitem.Stroke_Code.Trim());
									dc2=Convert.ToDecimal(opt.Code.Trim().Substring(2));
									dc3= dc1 - dc2;
									popopts +=opt.Opt.Trim()+" ( Effective Stroke="+Decimal.Round(dc3,2)+"\" ), ";
								}
								if(opt.Code.Substring(0,2)== "DS")
								{
									decimal dc1=0.00m;
									decimal dc2=0.00m;
									decimal dc3=0.00m;
									dc1=Convert.ToDecimal(qitem.Stroke_Code.Trim());
									dc2=Convert.ToDecimal(opt.Code.Trim().Substring(3));
									dc3= dc1 - dc2;
									popopts +=opt.Opt.Trim()+" ( Effective Stroke="+Decimal.Round(dc3,2)+"\" ), ";
								}
								if(opt.Code.Substring(0,1)== "C")
								{
									finish=opt.Opt.Trim();
								}
								if(opt.Code.Substring(0,2)=="XI")
								{
									xi=opt.Code.Substring(2);
								}
								if(opt.Code.Substring(0,2)=="BB")
								{
									if(opt.Code.Substring(0,3)=="BBH")
									{
										bbh=opt.Code.Substring(3);
									}
									if(opt.Code.Substring(0,3)=="BBC")
									{
										bbc=opt.Code.Substring(3);
									}
								}
								if(opt.Code.Substring(0,1)=="A" )
								{
									if(opt.Code.Substring(0,2) =="AD")
									{
										ad=opt.Code.Substring(2);
									}
									else
									{
										a=opt.Code.Substring(1);
									}
								}
								if(opt.Code.Substring(0,1)=="W" )
								{
									if(opt.Code.Substring(0,2) =="WD")
									{
										wd=opt.Code.Substring(2);
									}
									else
									{
										w=opt.Code.Substring(1);
									}
								}
								if(opt.Code.Substring(0,2)=="BC" ||  opt.Code.Substring(0,2)=="DC")
								{
									tandemduplex=true;
								}
								if( opt.Code.Substring(0,2)=="TC")
								{
									tandem=true;
								}
							}
						}
						if(tandemduplex ==false)
						{
							if(qitem.M_code.Substring(0,1)=="X" ||qitem.M_code.Substring(1,1)=="X" || qitem.M_code.Substring(0,1)=="I"|| qitem.M_code.Substring(0,1)=="M")
							{
								CrystalDecisions.CrystalReports.Engine.ReportDocument objReport = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
								string path;
								path = Request.PhysicalApplicationPath;
								bool drod=false;
								string secrod="";
								string rendcode="";
								string secrend="";
								string  kk="";
								ArrayList list=new ArrayList();
								list=db.SelectAopts(qno.ToString().Trim(),pno.ToString().Trim());
								if(list.Count >0)
								{
									for(int i=0;i<list.Count;i++)
									{
										Opts sss =new Opts();
										sss=(Opts)list[i];
										if(sss.Code.Substring(0,1) =="G")
										{
											appopts +=sss.Opt.ToString()+", ";
										}
										if(sss.Code.Substring(0,1) =="P")
										{
											appopts +=sss.Opt.ToString()+", ";
										}
										if(sss.Code.Substring(0,1) =="S")
										{
											appopts +=sss.Opt.ToString()+", ";
										}
										if(sss.Code.Substring(0,1) =="W")
										{
											appopts +=sss.Opt.ToString()+", ";
										}
										if(sss.Code.Substring(0,1) =="R")
										{	
											rendcode=sss.Code.Trim();															
											secrend=sss.Opt.Trim();
											drod=true;
										}
										if(sss.Code.Substring(0,1) =="D")
										{	
											secrod=sss.Code.Trim();	
											drod=true;
										}
									}
									string kkval="";
									kkval=db.SelectValue("RodEnd_Dimension","WEB_RodEndDiamension_TableV1","RR_Code",secrod.Trim().Replace("D","").Replace("2","")+rendcode.Replace("R",""));
									if(kkval.Trim() !="")
									{
										kk=", Second Rod KK = "+kkval.Trim();
									}
								}	
								if(qitem.M_code.Substring(0,1)=="X" || qitem.M_code.Substring(1,1)=="X" || qitem.M_code.Substring(0,1)=="I"|| qitem.M_code.Substring(0,1)=="M")
								{
									if(drod ==false && tandem == false)
									{
										objReport.Load(path +"/crtfiles/SMCDrawings_SeriesA_SingleRod.rpt");
									}
									else if(drod ==true && tandem == false)
									{
										objReport.Load(path +"/crtfiles/SMCDrawings_SeriesA_DoubleRod.rpt");
									}
									else if(tandem == true)
									{
										objReport.Load(path +"/crtfiles/SMCDrawings_SeriesA_Tandem.rpt");
									}
								}
								string check=db.SelectDimDrawing("DWG_SeriesA_Main_TableV1",qitem.B_Code.Trim(),qitem.R_Code.Trim());
								if(check.Trim() !="")
								{
									string strCurrentDir = Server.MapPath(".") + "/quote/";
									objReport.Refresh();
                                    objReport.SetDatabaseLogon(DBClass.ICylDBLogin, DBClass.ICylDBPassword, DBClass.ICylDBServer, DBClass.ICylDB);
									objReport.Refresh(); 
									objReport.SetParameterValue("@parm1",qitem.B_Code.Trim());
									objReport.SetParameterValue("@parm2",qitem.R_Code.Trim());
									objReport.SetParameterValue("@stroke",qitem.Stroke_Code.Trim());
									objReport.SetParameterValue("@pno",qitem.PartNo.Trim());
									objReport.SetParameterValue("@qno",qitem.Quotation_No.Trim());
									objReport.SetParameterValue("@material",material.Trim());
									objReport.SetParameterValue("@finish",finish.Trim());
									objReport.SetParameterValue("@customerref",qitem.Note.Trim());
									objReport.SetParameterValue("@ports",qitem.Port.Trim()+" ("+qitem.PortPos.Trim()+")");
									if(drod ==false)
									{
										objReport.SetParameterValue("@rodend",qitem.RodEnd.Trim());
									}
									else
									{
										objReport.SetParameterValue("@rodend",qitem.RodEnd.Trim() +kk.Trim());
									}
									if(qitem.M_code.Substring(0,2)=="NX")
									{
										objReport.SetParameterValue("@mount",qitem.M_code.Substring(1).Trim());
										objReport.SetParameterValue("@mount1",qitem.M_code.Substring(1).Trim());
									}
									else
									{
										objReport.SetParameterValue("@mount1",qitem.M_code.Trim());
										objReport.SetParameterValue("@mount",qitem.M_code.Trim());
									}
									objReport.SetParameterValue("@mountdesc",qitem.Mount.Substring(9));
								
									objReport.SetParameterValue("@mounttype",qitem.M_code.Substring(0,1).Trim());
									objReport.SetParameterValue("@desc",qitem.Bore.Trim().Replace("Size","").ToUpper()+" - "+ qitem.Rod.Trim().Replace("Size","").ToUpper()+" - "+qitem.Stroke_Code.Trim()+"\" STROKE");
                                    objReport.SetParameterValue("@weight", Regex.Replace(qitem.Weight.Trim(), "[^0-9.]", "") + " LBS");
									string bore="";
									if(qitem.B_Code.Trim() =="C" || qitem.B_Code.Trim() =="D" || qitem.B_Code.Trim() =="E" || qitem.B_Code.Trim() =="G"
										|| qitem.B_Code.Trim() =="H" || qitem.B_Code.Trim() =="J" || qitem.B_Code.Trim() =="K" || qitem.B_Code.Trim() =="L" || qitem.B_Code.Trim() =="M")
									{
										bore="no";
									}
									else
									{
										bore="yes";
									}
									objReport.SetParameterValue("@bore",bore.Trim());
									if(a.Trim() !="")
									{
										objReport.SetParameterValue("@dim_a",a.Trim());	
									}
									else
									{
										objReport.SetParameterValue("@dim_a","0");	
									}
									if(w.Trim() !="")
									{
										objReport.SetParameterValue("@dim_w",w.Trim());	
									}
									else
									{
										objReport.SetParameterValue("@dim_w","0");	
									}
									if(drod ==true)
									{
										if(ad.Trim() !="")
										{
											objReport.SetParameterValue("@dim_ad",ad.Trim());	
										}
										else
										{
											objReport.SetParameterValue("@dim_ad","0");	
										}
										if(wd.Trim() !="")
										{
											objReport.SetParameterValue("@dim_wd",wd.Trim());	
										}
										else
										{
											objReport.SetParameterValue("@dim_wd","0");	
										}
									}
									if(qitem.M_code.Substring(0,1)=="X")
									{
										objReport.SetParameterValue("@bbh",bbh.ToString());
										objReport.SetParameterValue("@bbc",bbc.ToString());
									}
									else
									{
										objReport.SetParameterValue("@bbh","0");
										objReport.SetParameterValue("@bbc","0");
									}
									string comment="";
									if(qitem.Cusomer_ID.Trim()=="Rotork" || qitem.Cusomer_ID.Trim()=="Rotork - USA" )
									{										
										comment=qitem.SpecialReq.Trim();
									}
									ArrayList splist=new ArrayList();
									splist=db.FindAllSpecials_WEB(qno.ToString().Trim(),pno.Trim());
									if(splist.Count >0)
									{
										string tem="";
										Specials sp=new Specials();
										for(int l=0;l<splist.Count;l++)
										{
											sp=new Specials();
											sp=(Specials)splist[l];
											tem +=sp.SPDesc.ToString();
											if(l < splist.Count -1)
											{
												tem +=", ";
											}
										}
										objReport.SetParameterValue("@specials",appopts.ToString()+popopts.ToString()+ tem.ToString()+comment.Trim());
									}
									else
									{
										objReport.SetParameterValue("@specials",appopts.ToString()+popopts.ToString()+comment.Trim());
									}
									string psi="";
									psi=db.SelectOneValueFunction(qitem.S_Code.ToString().Trim(),"sp_SelectSeriesPSI");
									if(psi.Trim() !="")
									{
										objReport.SetParameterValue("@seriespsi",psi.ToString().Trim());
									}
									ArrayList selist =new ArrayList();
									selist =(ArrayList)Session["User"];
									objReport.SetParameterValue("@customer",selist[6].ToString().Trim());
									CrystalDecisions.Shared.DiskFileDestinationOptions diskOpts = new CrystalDecisions.Shared.DiskFileDestinationOptions();
									objReport.ExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
									objReport.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
									diskOpts.DiskFileName = path+"/quote/"+pno.Trim().Replace("/","")+".pdf";
									objReport.ExportOptions.DestinationOptions = diskOpts;
									objReport.Export();
									Response.Redirect("quote/"+pno.Trim().Replace("/","")+".pdf");
								}
								else
								{
									err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
								}
							
							}
							else
							{
								err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
							}
						}
						else
						{
							err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
						}
					}
					else
					{
						err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
					}
				}
			}
			else
			{
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
			}
		}

		private void SystemDrawing_SeriesN(string qno,string pno)
		{
			string material="";
			string finish="";
			string appopts="";
			string popopts="";
			QItems qitem=new QItems();
			qitem=db.SelectItemsToManage(pno.Trim(),qno.Trim());
			if(qitem.S_Code.Substring(0,1) =="N")
			{
				material="Steel";
				if(qitem.B_Code.Trim() !="W" &&  qitem.B_Code.Trim() !="V" && qitem.B_Code.Trim() !="Q" && 
					qitem.B_Code.Trim() !="T" && qitem.B_Code.Trim() !="X" &&  qitem.B_Code.Trim() !="A" &&
					qitem.B_Code.Trim() !="Y" && qitem.B_Code.Trim() !="W" && qitem.B_Code.Trim() !="B" && 
					qitem.B_Code.Trim() !="F" && qitem.B_Code.Trim() !="I" &&  qitem.B_Code.Trim() !="J" && qitem.B_Code.Trim() !="Z" )
				{
					string xi="";
					string bbc="0";
					string bbh="0";
					string a="";
					string w="";
					string ad="";
					string wd="";
					bool tandem=false;
					ArrayList poplist=new ArrayList();
					Opts opt=new Opts();
					poplist=db.SelectPopts(qno.ToString().Trim(),pno.ToString().Trim());
					if(poplist.Count >0)
					{
						for(int i=0;i<poplist.Count;i++)
						{
							opt=new Opts();
							opt=(Opts)poplist[i];
							if(opt.Code.Substring(0,1)== "M")
							{
								popopts +=opt.Opt.Trim()+", ";
							}
							if(opt.Code.Substring(0,1)== "P")
							{
								popopts +=opt.Opt.Trim()+", ";
							}
							if(opt.Code.Substring(0,2)== "ST")
							{
								decimal dc1=0.00m;
								decimal dc2=0.00m;
								decimal dc3=0.00m;
								dc1=Convert.ToDecimal(qitem.Stroke_Code.Trim());
								dc2=Convert.ToDecimal(opt.Code.Trim().Substring(2));
								dc3= dc1 - dc2;
								popopts +=opt.Opt.Trim()+" ( Effective Stroke="+Decimal.Round(dc3,2)+"\" ), ";
							}
							if(opt.Code.Substring(0,2)== "DS")
							{
								decimal dc1=0.00m;
								decimal dc2=0.00m;
								decimal dc3=0.00m;
								dc1=Convert.ToDecimal(qitem.Stroke_Code.Trim());
								dc2=Convert.ToDecimal(opt.Code.Trim().Substring(3));
								dc3= dc1 - dc2;
								popopts +=opt.Opt.Trim()+" ( Effective Stroke="+Decimal.Round(dc3,2)+"\" ), ";
							}
							if(opt.Code.Substring(0,1)== "C")
							{
								finish=opt.Opt.Trim();
							}
							if(opt.Code.Substring(0,2)=="XI")
							{
								xi=opt.Code.Substring(2);
							}
							if(opt.Code.Substring(0,2)=="BB")
							{
								if(opt.Code.Substring(0,3)=="BBH")
								{
									bbh=opt.Code.Substring(3);
								}
								if(opt.Code.Substring(0,3)=="BBC")
								{
									bbc=opt.Code.Substring(3);
								}
							}
							if(opt.Code.Substring(0,1)=="A" )
							{
								if(opt.Code.Substring(0,2) =="AD")
								{
									ad=opt.Code.Substring(2);
								}
								else
								{
									a=opt.Code.Substring(1);
								}
							}
							if(opt.Code.Substring(0,1)=="W" )
							{
								if(opt.Code.Substring(0,2) =="WD")
								{
									wd=opt.Code.Substring(2);
								}
								else
								{
									w=opt.Code.Substring(1);
								}
							}
							if(opt.Code.Substring(0,2)=="BC" || opt.Code.Substring(0,2)=="TC" || opt.Code.Substring(0,2)=="DC")
							{
								tandem=true;
							}
						}
					}
					if(tandem ==false)
					{
						if( qitem.M_code.Substring(0,2)!="X5"  && qitem.M_code.Substring(0,2)!="X6" && qitem.M_code.Substring(0,2)!="X7")
						{
							if(qitem.M_code.Substring(0,1)=="P" || qitem.M_code.Substring(0,1)=="F"|| qitem.M_code.Substring(0,1)=="S" || qitem.M_code.Substring(0,1)=="E" || qitem.M_code.Substring(0,1)=="T" || qitem.M_code.Substring(0,1)=="X" )
							{
								CrystalDecisions.CrystalReports.Engine.ReportDocument objReport = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
								string path;
								path = Request.PhysicalApplicationPath;
								bool drod=false;
								string secrod="";
								string rendcode="";
								string secrend="";
								ArrayList list=new ArrayList();
								list=db.SelectAopts(qno.ToString().Trim(),pno.ToString().Trim());
								if(list.Count >0)
								{
									for(int i=0;i<list.Count;i++)
									{
										Opts sss =new Opts();
										sss=(Opts)list[i];
										if(sss.Code.Substring(0,1) =="G")
										{
											appopts +=sss.Opt.ToString()+", ";
										}
										if(sss.Code.Substring(0,1) =="P")
										{
											appopts +=sss.Opt.ToString()+", ";
										}
										if(sss.Code.Substring(0,1) =="S")
										{
											appopts +=sss.Opt.ToString()+", ";
										}
										if(sss.Code.Substring(0,1) =="W")
										{
											appopts +=sss.Opt.ToString()+", ";
										}
										if(sss.Code.Substring(0,1) =="R")
										{	
											rendcode=sss.Code.Trim();															
											secrend=sss.Opt.Trim();
											drod=true;
										}
										if(sss.Code.Substring(0,1) =="D")
										{	
											secrod=sss.Code.Trim();	
											appopts +=sss.Opt.ToString()+", ";
											drod=true;
										}
									}
									//								if(secrod.Trim() !="" && rendcode.Trim() !="")
									//								{
									//									string s=secrod.Trim().Substring(1,1)+rendcode.Replace("R","");
									//									string kkval=db.SelectValue("RodEnd_Dimension","WEB_RodEndDiamension_TableV1","RR_Code",s.Trim());
									//									if(kkval.ToString().Trim() !="")
									//									{
									//										kk =" KK="+kk.Trim();
									//									}
									//								}
								}
								string check="";
								if(drod ==false)
								{
									if(qitem.M_code.Substring(0,1) =="P")
									{
										objReport.Load(path +"/crtfiles/Drawings_SeriesN_MP.rpt");
									}
									else if(qitem.M_code.Substring(0,1) =="T")
									{
										objReport.Load(path +"/crtfiles/Drawings_SeriesN_MT.rpt");
									}
									else if(qitem.M_code.Substring(0,1) =="F")
									{
										objReport.Load(path +"/crtfiles/Drawings_SeriesN_MF.rpt");
									}
									else if(qitem.M_code.Substring(0,1) =="S")
									{
										objReport.Load(path +"/crtfiles/Drawings_SeriesN_MS.rpt");
									}
									else if(qitem.M_code.Substring(0,1) =="X")
									{
										objReport.Load(path +"/crtfiles/Drawings_SeriesN_MX.rpt");
									}
									else if(qitem.M_code.Substring(0,1) =="E")
									{
										objReport.Load(path +"/crtfiles/Drawings_SeriesN_ME.rpt");
									}
									check=db.SelectDimDrawing("DWG_SeriesP_"+qitem.M_code.Substring(0,1)+"_TableV1",qitem.B_Code.Trim(),qitem.R_Code.Trim());

								}
								else if(drod ==true)
								{
									if(qitem.M_code.Substring(0,1) =="T")
									{
										objReport.Load(path +"/crtfiles/Drawings_SeriesN_MT_DBL.rpt");
									}
									else if(qitem.M_code.Substring(0,1) =="F")
									{
										objReport.Load(path +"/crtfiles/Drawings_SeriesN_MF_DBL.rpt");
									}
									else if(qitem.M_code.Substring(0,1) =="S")
									{
										objReport.Load(path +"/crtfiles/Drawings_SeriesN_MS_DBL.rpt");
									}
									else if(qitem.M_code.Substring(0,1) =="X")
									{
										objReport.Load(path +"/crtfiles/Drawings_SeriesN_MX_DBL.rpt");
									}
									else if(qitem.M_code.Substring(0,1) =="E")
									{
										objReport.Load(path +"/crtfiles/Drawings_SeriesN_ME_DBL.rpt");
									}
									check=db.SelectDimDrawing("DWG_SeriesP_DBL_"+qitem.M_code.Substring(0,1)+"_TableV1",qitem.B_Code.Trim(),qitem.R_Code.Trim());
								}
								if(check.Trim() !="")
								{
									string strCurrentDir = Server.MapPath(".") + "/quote/";
									//									fn.RemoveFiles(strCurrentDir);
									objReport.Refresh();
                                    objReport.SetDatabaseLogon(DBClass.ICylDBLogin, DBClass.ICylDBPassword, DBClass.ICylDBServer, DBClass.ICylDB);
									objReport.Refresh(); 
									objReport.SetParameterValue("@parm1",qitem.B_Code.Trim());
									objReport.SetParameterValue("@parm2",qitem.R_Code.Trim());
									objReport.SetParameterValue("@stroke",qitem.Stroke_Code.Trim());
									objReport.SetParameterValue("@pno",qitem.PartNo.Trim());
									objReport.SetParameterValue("@qno",qitem.Quotation_No.Trim());
									objReport.SetParameterValue("@material",material.Trim());
									objReport.SetParameterValue("@finish",finish.Trim());
									objReport.SetParameterValue("@customerref",qitem.Note.Trim());
									objReport.SetParameterValue("@ports",qitem.Port.Trim()+" ("+qitem.PortPos.Trim()+")");
									string cushions="";
									if(qitem.Cu_Code.Trim().Substring(0,1) !="8")
									{
										cushions=qitem.Cushion.Trim()+" ("+qitem.CushionPos.Trim()+")";
									}
									else
									{
										cushions=qitem.Cushion.Trim();
									}
									objReport.SetParameterValue("@cushions",cushions.Trim());
									objReport.SetParameterValue("@rodend",qitem.RodEnd.Trim());
									objReport.SetParameterValue("@mount",qitem.M_code.Trim());
									objReport.SetParameterValue("@desc",qitem.Bore.Trim().Replace("Size","").ToUpper()+" - "+ qitem.Rod.Trim().Replace("Size","").ToUpper()+" - "+qitem.Stroke_Code.Trim()+"\" STROKE");
                                    objReport.SetParameterValue("@weight", Regex.Replace(qitem.Weight.Trim(), "[^0-9.]", "") + " LBS");
									string rodendtype="";
									if(qitem.RE_Code.Trim() =="N1" || qitem.RE_Code.Trim() =="M1" || qitem.RE_Code.Trim() =="N5" || qitem.RE_Code.Trim().Substring(0,1) =="Y" || qitem.RE_Code.Trim().Substring(0,1) =="Z" )
									{
										rodendtype="sm";
									}
									else if(qitem.RE_Code.Trim() =="N4"|| qitem.RE_Code.Trim() =="M4" || qitem.RE_Code.Trim() =="N6" || qitem.RE_Code.Trim().Substring(0,1) =="X" || qitem.RE_Code.Trim().Substring(0,1) =="W")
									{
										rodendtype="sf";
									}
									else if(qitem.RE_Code.Trim() =="N3" || qitem.RE_Code.Trim() =="M3" || qitem.RE_Code.Trim() =="N2" || qitem.RE_Code.Trim() =="M2")
									{
										rodendtype="sm";
									}
									else
									{
										rodendtype="sm";
									}
									string sec_rodendtype="";
									if(secrod.Trim() !="" && rendcode.Trim() !="")
									{
										if(rendcode.Substring(1,2) =="N1" || rendcode.Substring(1,2) =="M1" || rendcode.Substring(1,2) =="N5"  || rendcode.Substring(1,1) =="Y" || rendcode.Substring(1,1) =="Z")
										{
											sec_rodendtype="sm";
										}
										else if(rendcode.Substring(1,2) =="N4"|| rendcode.Substring(1,2) =="M4" || rendcode.Substring(1,2) =="N6" || rendcode.Substring(1,1) =="X" || rendcode.Substring(1,1) =="W")
										{
											sec_rodendtype="sf";
										}
										else if(rendcode.Substring(1,2) =="N3" || rendcode.Substring(1,2) =="M3" || rendcode.Substring(1,2) =="N2" || rendcode.Substring(1,2) =="M2")
										{
											sec_rodendtype="sm";
										}
										else
										{
											sec_rodendtype="sm";
										}
									}
									string bore="";
									if(qitem.B_Code.Trim() =="C" || qitem.B_Code.Trim() =="D" || qitem.B_Code.Trim() =="E" || qitem.B_Code.Trim() =="G"
										|| qitem.B_Code.Trim() =="H" || qitem.B_Code.Trim() =="J" || qitem.B_Code.Trim() =="K" || qitem.B_Code.Trim() =="L" || qitem.B_Code.Trim() =="M")
									{
										bore="no";
									}
									else
									{
										bore="yes";
									}
									objReport.SetParameterValue("@bore",bore.Trim());
									objReport.SetParameterValue("@rodendtype",rodendtype.Trim());
									if(drod ==true)
									{
										objReport.SetParameterValue("@rodend",qitem.RodEnd.Trim()+", "+secrend.Trim());
										objReport.SetParameterValue("@sec_rodendtype",sec_rodendtype.Trim());
										if(ad.Trim() !="")
										{
											objReport.SetParameterValue("@dim_ad",ad.Trim());	
										}
										else
										{
											objReport.SetParameterValue("@dim_ad","0");	
										}
										if(wd.Trim() !="")
										{
											objReport.SetParameterValue("@dim_wd",wd.Trim());	
										}
										else
										{
											objReport.SetParameterValue("@dim_wd","0");	
										}
									}
									if(a.Trim() !="")
									{
										objReport.SetParameterValue("@dim_a",a.Trim());	
									}
									else
									{
										objReport.SetParameterValue("@dim_a","0");	
									}
									if(w.Trim() !="")
									{
										objReport.SetParameterValue("@dim_w",w.Trim());	
										decimal wdim=0.00m;
										wdim=db.SelectAddersPrice("W1","STD_W_Values_TableV1",qitem.B_Code.Trim(),qitem.R_Code.Trim());
										objReport.SetParameterValue("@std_dim_w",wdim.ToString().Trim());	
									}
									else
									{
										objReport.SetParameterValue("@dim_w","0");	
										objReport.SetParameterValue("@std_dim_w","0");	
									}
									if(qitem.M_code.Substring(0,1)=="T")
									{
										if(qitem.M_code.Substring(0,2)=="T4")
										{
											objReport.SetParameterValue("@xi",xi.ToString());
										}
										else
										{
											objReport.SetParameterValue("@xi","0");
										}
									}
									if(qitem.M_code.Substring(0,1)=="X")
									{
										objReport.SetParameterValue("@bbh",bbh.ToString());
										objReport.SetParameterValue("@bbc",bbc.ToString());
									}
									ArrayList splist=new ArrayList();
									splist=db.FindAllSpecials_WEB(qno.ToString().Trim(),pno.Trim());
									if(splist.Count >0)
									{
										string tem="";
										Specials sp=new Specials();
										for(int l=0;l<splist.Count;l++)
										{
											sp=new Specials();
											sp=(Specials)splist[l];
											tem +=sp.SPDesc.ToString();
											if(l < splist.Count -1)
											{
												tem +=", ";
											}
										}
										objReport.SetParameterValue("@specials",appopts.ToString()+popopts.ToString()+ tem.ToString());
									}
									else
									{
										objReport.SetParameterValue("@specials",appopts.ToString()+popopts.ToString());
									}
									//objReport.SetParameterValue("@customer",qitem.Cusomer_ID.Trim());
									string psi="";
									psi=db.SelectOneValueFunction(qitem.S_Code.ToString().Trim(),"sp_SelectSeriesPSI");
									if(psi.Trim() !="")
									{
										objReport.SetParameterValue("@seriespsi",psi.ToString().Trim());
									}
									//issue #646 start
									ArrayList selist =new ArrayList();
									selist =(ArrayList)Session["User"];
									objReport.SetParameterValue("@customer",qitem.Cusomer_ID.Trim());
									//issue #646 end 
									CrystalDecisions.Shared.DiskFileDestinationOptions diskOpts = new CrystalDecisions.Shared.DiskFileDestinationOptions();
									objReport.ExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
									objReport.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
									diskOpts.DiskFileName = path+"/quote/"+pno.Trim().Replace("/","")+".pdf";
									objReport.ExportOptions.DestinationOptions = diskOpts;
									objReport.Export();
									Response.Redirect("quote/"+pno.Trim().Replace("/","")+".pdf");
								}
								else
								{
									err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
								}
							}
							else
							{
								err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
							}
						}
						else
						{
							err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
						}
					}
					else
					{
						err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
					}
				}
				else
				{
					err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
				}
			}
			else
			{
				err.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Drawing is not available. Please consult Cowan Engineering')</script>";
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
