using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
namespace iCylinderV1
{
	/// <summary>
	/// Summary description for UserLogin.
	/// </summary>
	public partial class Default : System.Web.UI.Page
	{
		DBClass db=new DBClass();
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			Page.RegisterStartupScript("SetInitialFocus", "<script>document.getElementById('" + txtUserName.ClientID + "').focus();</script>");
			
			if (Session["User"]!=null)
			{
				Session.Clear();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void btnLogin_Click(object sender, System.EventArgs e)
		{
			rfvPassword.Enabled =true;
			lblMessage.Text="";
			if(Page.IsValid)
			{
				
				ArrayList list =new ArrayList();
				list=db.SelectUser(txtUserName.Text.ToString().Trim(),txtPassword.Text.ToString().Trim());
				if(list.Count ==1)
				{
					string s=list[0].ToString().Replace("'"," ");
					s.Replace("\r\n"," ");
					lblMessage.Text ="<script language='javascript'> window.alert('"+s+"')</script>";
				}
				else if(list.Count > 1)
				{
					Session["User"]=list;
					DateTime dt=Convert.ToDateTime(list[4]);
					if(dt.AddDays(90) < DateTime.Today)
					{
						Response.Redirect("Profile.aspx");
					}
					else
					{
						//issue #341 start
						DateTime dtUpdatePrice = new DateTime(2015,06,02);
						if(DateTime.Now<dtUpdatePrice)
						{
							string strMessage = "Dear iCylinder User,\\r\\n" +
								"As you are aware, our industry has been challenged with rising costs on all fronts.\\r\\n"+
								"While we have attempted to offset those additional costs to minimize the effect to our customers, we have reached a point where we must pass on a small portion of those increases:\\r\\n"+
								"Effective June 1st there will be a 4% price increase on cylinders and accessories.\\r\\n"+
								"Repair kits will increase by 5%.\\r\\n"+
								"iCylinder will be updated on June 1st. \\r\\n";
//							string sJavaScript = "<script language=javascript>\n";        
//							sJavaScript += "var agree;\n";        
//							sJavaScript += "agree = confirm('"+strMessage+"');\n";        
//							sJavaScript += "if(agree)\n";        
//							sJavaScript += "window.location = \"Welcome.aspx\";\n";        
//							sJavaScript += "</script>";      
//							Response.Write(sJavaScript);
							string sJavaScript = "<script language=javascript>\n";        
							sJavaScript += "alert('"+strMessage+"');\n";        
							sJavaScript += "window.location = \"Welcome.aspx\";\n";        
							sJavaScript += "</script>";      
							Response.Write(sJavaScript);
						}
						else
						{
							Response.Redirect("Welcome.aspx");
						}
					}
				}
				else
				{
					rfvPassword.Enabled =false;
					lblMessage.Text="<script language='javascript'> " + Environment.NewLine +" window.alert('Invalid Login Try again !')</script>";
				}
			}
		}

		protected void LBForgotPass_Click(object sender, System.EventArgs e)
		{
			rfvPassword.Enabled =false;
			if(txtUserName.Text.Trim() !="" )
			{
				lblMessage.Text="";
				ArrayList elist=new ArrayList();
				elist=db.SelectUser_ValidEmail(txtUserName.Text.Trim());
				if(elist.Count >=1)
				{
					string guidResult = System.Guid.NewGuid().ToString();
					guidResult = guidResult.Replace("-", string.Empty);
					guidResult=guidResult.Substring(0,6);
					string s =db.ResetPassword(guidResult.Trim(),txtUserName.Text.Trim());
						if(s.Trim() =="1")
						{
                            MailService.SendMail(
                                txtUserName.Text.Trim(),
                                "jbehara@cowandynamics.com",
                                "Temporary password for i-Cylinder",
                                "Hi,<Br> Your temporary password is -<br>" + guidResult.Trim() + "<Br>Thanks<br> Admin"
                                ); 
                            lblMessage.Text = "New password is emailed to you";
						}
						else
						{
							lblMessage.Text="Error try again later!!";
						}
				}
				else
				{
					lblMessage.Text="You are not a valid user!!";
				}
			}
			else
			{
				lblMessage.Text="Please enter user name!!";
			}
		}
		private string GetDomain( string email )
		{
			int index = email.IndexOf( '@' );
			return email.Substring( index + 1 );
		}
	}
}
