<%@ Page language="c#" Codebehind="Manage_Rod_Acc.aspx.cs" AutoEventWireup="True" Inherits="iCylinderV1.Manage_Rod_Acc" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Manage_Rod_Acc</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 1000px; HEIGHT: 487px" cellSpacing="0" cellPadding="0"
				width="1000" bgColor="lightgrey" border="0">
				<TR>
					<TD width="50" height="20"></TD>
					<TD align="center" height="20">
						<asp:label id="Label1" runat="server" Font-Size="Smaller" Font-Underline="True" BackColor="Transparent"
							Width="229px" Font-Bold="True" Height="19">Rod End Accessories</asp:label></TD>
					<TD width="50" height="20"></TD>
				</TR>
				<TR>
					<TD width="50"></TD>
					<TD align="center" bgColor="gainsboro">
						<TABLE id="Table3" style="WIDTH: 238px; HEIGHT: 333px" borderColor="gray" cellSpacing="0"
							cellPadding="0" width="238" border="0">
							<TR>
								<TD align="center">
									<TABLE id="Table22" style="WIDTH: 217px; HEIGHT: 44px" borderColor="white" cellSpacing="0"
										cellPadding="0" width="217" bgColor="silver" border="1">
										<TR>
											<TD style="WIDTH: 156px">
												<asp:RadioButtonList id="RBLMaleFemale" runat="server" Font-Size="Smaller" RepeatLayout="Flow" AutoPostBack="True" onselectedindexchanged="RBLMaleFemale_SelectedIndexChanged">
													<asp:ListItem Value="Male" Selected="True">Male Rod End</asp:ListItem>
													<asp:ListItem Value="Female">Female Rod End</asp:ListItem>
												</asp:RadioButtonList></TD>
											<TD style="WIDTH: 92px">
												<asp:label id="Label2" runat="server" Font-Size="Smaller" BackColor="Transparent" Width="85px"
													Height="19">KK:</asp:label>
												<asp:DropDownList id="DDLKK" runat="server" Font-Size="XX-Small" Width="94px" AutoPostBack="True" onselectedindexchanged="DDLKK_SelectedIndexChanged"></asp:DropDownList></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD align="center">
									<TABLE id="Table24" style="WIDTH: 624px; HEIGHT: 65px" borderColor="whitesmoke" cellSpacing="0"
										cellPadding="0" width="624" border="1">
										<TR>
											<TD>
												<asp:Panel id="PanelRodeye" runat="server" Enabled="False">
													<TABLE id="Table16" border="0" cellSpacing="0" cellPadding="0" width="300">
														<TR>
															<TD style="WIDTH: 251px">
																<TABLE style="WIDTH: 192px; HEIGHT: 91px" id="Table17" border="0" cellSpacing="0" cellPadding="0"
																	width="192">
																	<TR>
																		<TD style="HEIGHT: 8px">
																			<asp:CheckBox id="CBRodeye" runat="server" Font-Size="Smaller" AutoPostBack="True" Text="Rod Eye"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:DropDownList id="DDLRodeye" runat="server" Width="190px" Font-Size="XX-Small" AutoPostBack="True"></asp:DropDownList></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:Label id="Label22" runat="server" Font-Size="Smaller">Quantity:</asp:Label>
																			<asp:TextBox id="TxtRodeye" runat="server" Width="35px" Font-Size="XX-Small" AutoPostBack="True" ontextchanged="TxtRodeye_TextChanged"></asp:TextBox></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="110" align="right">
																<asp:Image id="ImgRodeye" runat="server" ImageUrl="accessories\rodeye.jpg"></asp:Image></TD>
														</TR>
													</TABLE>
												</asp:Panel></TD>
											<TD>
												<asp:Panel id="PanelRodClevis" runat="server" Enabled="False">
													<TABLE id="Table6" border="0" cellSpacing="0" cellPadding="0" width="300">
														<TR>
															<TD style="WIDTH: 251px">
																<TABLE style="WIDTH: 192px; HEIGHT: 91px" id="Table12" border="0" cellSpacing="0" cellPadding="0"
																	width="192">
																	<TR>
																		<TD style="HEIGHT: 11px">
																			<asp:CheckBox id="CBRodclevis" runat="server" Font-Size="Smaller" AutoPostBack="True" Text="Rod Clevis"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:DropDownList id="DDLRodclevis" runat="server" Width="190px" Font-Size="XX-Small" AutoPostBack="True"></asp:DropDownList></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:Label id="Label5" runat="server" Font-Size="Smaller">Quantity:</asp:Label>
																			<asp:TextBox id="TxtRodclevis" runat="server" Width="35px" Font-Size="XX-Small" AutoPostBack="True" ontextchanged="TxtRodclevis_TextChanged"></asp:TextBox></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="110" align="right">
																<asp:Image id="ImgRodclevis" runat="server" ImageUrl="accessories\rodclevis.jpg"></asp:Image></TD>
														</TR>
													</TABLE>
												</asp:Panel></TD>
											<TD>
												<asp:Panel id="PanelPivotPin" runat="server" Enabled="False">
													<TABLE id="Table4" border="0" cellSpacing="0" cellPadding="0" width="300">
														<TR>
															<TD style="WIDTH: 251px">
																<TABLE style="WIDTH: 192px; HEIGHT: 91px" id="Table2" border="0" cellSpacing="0" cellPadding="0"
																	width="192">
																	<TR>
																		<TD style="HEIGHT: 9px">
																			<asp:CheckBox id="CBPivotpin" runat="server" Font-Size="Smaller" AutoPostBack="True" Text="Pivot Pin"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:DropDownList id="DDLPivotpin" runat="server" Width="190px" Font-Size="XX-Small" AutoPostBack="True"></asp:DropDownList></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:Label id="Label11" runat="server" Font-Size="Smaller">Quantity:</asp:Label>
																			<asp:TextBox id="txtPivotpin" runat="server" Width="35px" Font-Size="XX-Small" AutoPostBack="True" ontextchanged="txtPivotpin_TextChanged"></asp:TextBox></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="110" align="right">
																<asp:Image id="ImgPivotpin" runat="server" ImageUrl="accessories\pivotpin.jpg"></asp:Image></TD>
														</TR>
													</TABLE>
												</asp:Panel></TD>
										</TR>
										<TR>
											<TD>
												<asp:Panel id="PanelEyeBracket" runat="server" Enabled="False">
													<TABLE id="Table13" border="0" cellSpacing="0" cellPadding="0" width="300">
														<TR>
															<TD style="WIDTH: 251px">
																<TABLE style="WIDTH: 192px; HEIGHT: 91px" id="Table14" border="0" cellSpacing="0" cellPadding="0"
																	width="192">
																	<TR>
																		<TD style="HEIGHT: 1px">
																			<asp:CheckBox id="CBEyeb" runat="server" Font-Size="Smaller" AutoPostBack="True" Text="Eye Bracket"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:DropDownList id="DDLEyeb" runat="server" Width="190px" Font-Size="XX-Small" AutoPostBack="True"></asp:DropDownList></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:Label id="Label4" runat="server" Font-Size="Smaller">Quantity:</asp:Label>
																			<asp:TextBox id="TxtEyeb" runat="server" Width="35px" Font-Size="XX-Small" AutoPostBack="True" ontextchanged="TxtEyeb_TextChanged"></asp:TextBox></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="110" align="right">
																<asp:Image id="ImgEyeb" runat="server" ImageUrl="accessories\eyebracket.jpg"></asp:Image></TD>
														</TR>
													</TABLE>
												</asp:Panel></TD>
											<TD>
												<asp:Panel id="PanelClevisBracket" runat="server" Enabled="False">
													<TABLE id="Table15" border="0" cellSpacing="0" cellPadding="0" width="300">
														<TR>
															<TD style="WIDTH: 251px">
																<TABLE style="WIDTH: 192px; HEIGHT: 91px" id="Table18" border="0" cellSpacing="0" cellPadding="0"
																	width="192">
																	<TR>
																		<TD style="HEIGHT: 5px">
																			<asp:CheckBox id="CBClevisbracket" runat="server" Font-Size="Smaller" AutoPostBack="True" Text="Clevis Bracket"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:DropDownList id="DDLClevisbracket" runat="server" Width="190px" Font-Size="XX-Small" AutoPostBack="True"></asp:DropDownList></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:Label id="Label18" runat="server" Font-Size="Smaller">Quantity:</asp:Label>
																			<asp:TextBox id="TxtClevisbracket" runat="server" Width="35px" Font-Size="XX-Small" AutoPostBack="True" ontextchanged="TxtClevisbracket_TextChanged"></asp:TextBox></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="110" align="right">
																<asp:Image id="ImgClevisbracket" runat="server" ImageUrl="accessories\clevisbracket.jpg"></asp:Image></TD>
														</TR>
													</TABLE>
												</asp:Panel></TD>
											<TD>
												<asp:Panel id="PanelLinearCoupler" runat="server" Enabled="False">
													<TABLE id="Table9" border="0" cellSpacing="0" cellPadding="0" width="300">
														<TR>
															<TD style="WIDTH: 251px">
																<TABLE style="WIDTH: 192px; HEIGHT: 91px" id="Table10" border="0" cellSpacing="0" cellPadding="0"
																	width="192">
																	<TR>
																		<TD style="HEIGHT: 8px">
																			<asp:CheckBox id="CBLinearcoupler" runat="server" Font-Size="Smaller" AutoPostBack="True" Text="Linear Alignment Coupler"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:DropDownList id="DDLLinearcoupler" runat="server" Width="190px" Font-Size="XX-Small" AutoPostBack="True"></asp:DropDownList></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:Label id="Label24" runat="server" Font-Size="Smaller">Quantity:</asp:Label>
																			<asp:TextBox id="TxtLinearcoupler" runat="server" Width="35px" Font-Size="XX-Small" AutoPostBack="True" ontextchanged="TxtLinearcoupler_TextChanged"></asp:TextBox></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="110" align="right">
																<asp:Image id="ImgLinearcoupler" runat="server" ImageUrl="accessories\linearcoupler.jpg"></asp:Image></TD>
														</TR>
													</TABLE>
												</asp:Panel></TD>
										</TR>
										<TR>
											<TD>
												<asp:Panel id="PanelSphericalRodEye" runat="server" Enabled="False">
													<TABLE id="Table19" border="0" cellSpacing="0" cellPadding="0" width="300">
														<TR>
															<TD style="WIDTH: 251px">
																<TABLE style="WIDTH: 192px; HEIGHT: 91px" id="Table20" border="0" cellSpacing="0" cellPadding="0"
																	width="192">
																	<TR>
																		<TD style="HEIGHT: 13px">
																			<asp:CheckBox id="CBSphericalrodeye" runat="server" Font-Size="Smaller" AutoPostBack="True" Text="Spherical Rod Eye"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:DropDownList id="DDLSphericalrodeye" runat="server" Width="190px" Font-Size="XX-Small" AutoPostBack="True"></asp:DropDownList></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:Label id="Label20" runat="server" Font-Size="Smaller">Quantity:</asp:Label>
																			<asp:TextBox id="TxtSphericalrodeye" runat="server" Width="35px" Font-Size="XX-Small" AutoPostBack="True" ontextchanged="TxtSphericalrodeye_TextChanged"></asp:TextBox></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="110" align="right">
																<asp:Image id="ImgSphericalrodeye" runat="server" ImageUrl="accessories\sphericalrodeye.jpg"></asp:Image></TD>
														</TR>
													</TABLE>
												</asp:Panel></TD>
											<TD>
												<asp:Panel id="PanelSphericalClevisBracket" runat="server" Enabled="False">
													<TABLE id="Table8" border="0" cellSpacing="0" cellPadding="0" width="300">
														<TR>
															<TD style="WIDTH: 251px">
																<TABLE style="WIDTH: 192px; HEIGHT: 91px" id="Table11" border="0" cellSpacing="0" cellPadding="0"
																	width="192">
																	<TR>
																		<TD style="HEIGHT: 10px">
																			<asp:CheckBox id="CBSphericalclevisbracket" runat="server" Font-Size="Smaller" AutoPostBack="True"
																				Text="Spherical Clevis Bracket"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:DropDownList id="DDLSphericalclevisbracket" runat="server" Width="190px" Font-Size="XX-Small"
																				AutoPostBack="True"></asp:DropDownList></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:Label id="Label16" runat="server" Font-Size="Smaller">Quantity:</asp:Label>
																			<asp:TextBox id="TxtSphericalclevisbraket" runat="server" Width="35px" Font-Size="XX-Small" AutoPostBack="True" ontextchanged="TxtSphericalclevisbraket_TextChanged"></asp:TextBox></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="110" align="right">
																<asp:Image id="ImgSphericalclevisbraket" runat="server" ImageUrl="accessories\sphericalclevisbracket.jpg"></asp:Image></TD>
														</TR>
													</TABLE>
												</asp:Panel></TD>
											<TD>
												<asp:Panel id="PanelSphericalPivotpin" runat="server" Enabled="False">
													<TABLE id="Table5" border="0" cellSpacing="0" cellPadding="0" width="300">
														<TR>
															<TD style="WIDTH: 251px">
																<TABLE style="WIDTH: 192px; HEIGHT: 91px" id="Table7" border="0" cellSpacing="0" cellPadding="0"
																	width="192">
																	<TR>
																		<TD style="HEIGHT: 18px">
																			<asp:CheckBox id="CBSpericalpp" runat="server" Font-Size="Smaller" AutoPostBack="True" Text="Spherical Pivot Pin"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:DropDownList id="DDLSphericalpp" runat="server" Width="190px" Font-Size="XX-Small" AutoPostBack="True"></asp:DropDownList></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:Label id="Label14" runat="server" Font-Size="Smaller">Quantity:</asp:Label>
																			<asp:TextBox id="TxtSphericalpp" runat="server" Width="35px" Font-Size="XX-Small" AutoPostBack="True" ontextchanged="TxtSphericalpp_TextChanged"></asp:TextBox></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="110" align="right">
																<asp:Image id="ImgSphericalpp" runat="server" ImageUrl="accessories\sphericalpivotpin.jpg"></asp:Image></TD>
														</TR>
													</TABLE>
												</asp:Panel></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
						<asp:Label id="lblinfo" runat="server" Font-Size="Smaller" ForeColor="Red"></asp:Label>
					</TD>
					<TD width="50"></TD>
				</TR>
				<TR>
					<TD align="right" width="50" height="30"></TD>
					<TD align="center" bgColor="#dcdcdc" height="30">
						<asp:LinkButton id="LBHome" runat="server" Font-Size="Smaller" Width="111px" Font-Bold="True" onclick="LBHome_Click">Back</asp:LinkButton>
						<asp:LinkButton id="BtnNext" runat="server" Font-Size="Smaller" Width="134px" Font-Bold="True" onclick="BtnNext_Click">Generate Quote</asp:LinkButton>
						<asp:HyperLink id="HLPrint" runat="server" Font-Size="9pt" Font-Bold="True" Target="_blank" Visible="False"
							Width="131px">Print Quote</asp:HyperLink></TD>
					<TD width="50" height="30"></TD>
				</TR>
				<TR>
					<TD width="50" height="20"></TD>
					<TD align="center" height="20"></TD>
					<TD width="50" height="20"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
