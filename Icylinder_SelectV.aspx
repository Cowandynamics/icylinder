<%@ Page language="c#" Codebehind="Icylinder_SelectV.aspx.cs" AutoEventWireup="True" Inherits="iCylinderV1.Icylinder_SelectV" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Icylinder_SelectV</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 1000px; HEIGHT: 487px" cellSpacing="0" cellPadding="0"
				width="1000" bgColor="lightgrey" border="0">
				<TR>
					<TD width="50" height="20"></TD>
					<TD align="center" height="20">
						<asp:label id="Label2" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
							Font-Size="Small">Select Version of iCylinder</asp:label></TD>
					<TD width="50" height="20"></TD>
				</TR>
				<TR>
					<TD width="50"></TD>
					<TD align="center" bgColor="gainsboro">
						<TABLE id="Table2" borderColor="gray" height="400" cellSpacing="0" cellPadding="0" width="900"
							border="0">
							<TR>
								<TD align="center">
									<asp:Panel id="PanelCassic" runat="server" Width="343px">
										<TABLE style="WIDTH: 234px; HEIGHT: 48px" id="Table3" border="0" cellSpacing="0" borderColor="silver"
											cellPadding="0" width="234">
											<TR>
												<TD style="HEIGHT: 13px" align="center">
													<asp:label id="Label3" runat="server" Font-Size="Smaller" Font-Underline="True" BackColor="Transparent"
														Width="232px" Height="19">Classic Version</asp:label></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 13px" align="center">
													<asp:ImageButton id="IBCap" runat="server" ImageUrl="images\old-version.gif" onclick="IBCap_Click"></asp:ImageButton></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 66px" align="center">
													<asp:label id="Label4" runat="server" Font-Size="Smaller" BackColor="Transparent" Width="247px"
														Height="19">Please use this version if you are familiar with all the options of i-Cylinder. </asp:label>
													<asp:LinkButton id="LBcap" runat="server" Font-Size="Smaller" Width="392px" onclick="LBcap_Click">Click Here to Start Classic Version</asp:LinkButton></TD>
											</TR>
										</TABLE>
									</asp:Panel></TD>
								<TD align="center">
									<asp:Panel id="Panelnew" runat="server" Width="301px">
										<TABLE style="WIDTH: 234px; HEIGHT: 48px" id="Table4" border="0" cellSpacing="0" borderColor="silver"
											cellPadding="0" width="234">
											<TR>
												<TD style="HEIGHT: 13px" align="center">
													<asp:label id="Label1" runat="server" Font-Size="Smaller" Font-Underline="True" BackColor="Transparent"
														Width="232px" Height="19">New Version</asp:label></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 13px" align="center">
													<asp:ImageButton id="IBRod" runat="server" ImageUrl="images\new-version.gif" onclick="IBRod_Click"></asp:ImageButton></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 66px" align="center">
													<asp:label id="Label5" runat="server" Font-Size="Smaller" BackColor="Transparent" Width="247px"
														Height="19">Please use this version if you are not familiar with i-Cylinder. </asp:label>
													<asp:LinkButton id="LBRod" runat="server" Font-Size="Smaller" Width="386px" onclick="LBRod_Click">Click Here to Start New Version</asp:LinkButton></TD>
											</TR>
										</TABLE>
									</asp:Panel></TD>
							</TR>
						</TABLE>
					</TD>
					<TD width="50"></TD>
				</TR>
				<TR>
					<TD width="50" height="20"></TD>
					<TD align="center" height="20"></TD>
					<TD width="50" height="20"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
