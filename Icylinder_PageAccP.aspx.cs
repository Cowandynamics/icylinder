using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for Icylinder_PageAccP.
	/// </summary>
	public partial class Icylinder_PageAccP : System.Web.UI.Page
	{
		coder cd1=new coder();
		DBClass db=new DBClass();
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["User"] !=null)
			{
				if(! IsPostBack)
				{
					if(	Session["Coder"] !=null)
					{
						lblinfo.Text="";
						cd1=(coder)Session["Coder"];
						if(cd1.Series.Trim() !="R")
						{
							string malefemale="";
							malefemale =db.SelectValues_Accessories("Type","WEB_RodEndKK_TableV1","RodEnd_Code",cd1.Rod_End.Trim());
							lblmalefemale.Text=malefemale.ToString();
							string kk="";
							kk=db.SelectValues_Accessories("RodEnd_Dimension","WEB_RodEndDiamension_TableV1","RR_Code",cd1.Rod_Diamtr.Trim()+ cd1.Rod_End.Trim());
							if(kk.Trim() !="")
							{
								lblkk.Text=kk.ToString();
								string cd="";
								cd =db.SelectValue("CD","WEB_KK_TableV1","KK",kk.Trim());

								if(cd1.Mount.Trim() =="P1" || cd1.Mount.Trim() =="P2")
								{
									PanelRodeye.Enabled =false;
									PanelRodClevis.Enabled =false;
									PanelPivotPin.Enabled =false;
									PanelEyeBracket.Enabled =true;
									PanelClevisBracket.Enabled =false;
									PanelLinearCoupler.Enabled =false;
									PanelSphericalClevisBracket.Enabled=false;
									PanelSphericalPivotpin.Enabled=false;
									PanelSphericalRodEye.Enabled=false;
								
								}
								else if(cd1.Mount.Trim() =="P3" || cd1.Mount.Trim() =="P4")
								{
									PanelRodeye.Enabled =false;
									PanelRodClevis.Enabled =false;
									PanelPivotPin.Enabled =false;
									PanelEyeBracket.Enabled =false;
									PanelClevisBracket.Enabled =true;
									PanelLinearCoupler.Enabled =false;
									PanelSphericalClevisBracket.Enabled=false;
									PanelSphericalPivotpin.Enabled=false;
									PanelSphericalRodEye.Enabled=false;
								}
								else if(cd1.Mount.Trim() =="P5")
								{
									PanelRodeye.Enabled =false;
									PanelRodClevis.Enabled =false;
									PanelPivotPin.Enabled =false;
									PanelEyeBracket.Enabled =false;
									PanelClevisBracket.Enabled =false;
									PanelLinearCoupler.Enabled =false;
									PanelSphericalClevisBracket.Enabled=true;
									PanelSphericalPivotpin.Enabled=true;
									PanelSphericalRodEye.Enabled=false;
								}
								else
								{
									PanelRodeye.Enabled =false;
									PanelRodClevis.Enabled =false;
									PanelPivotPin.Enabled =false;
									PanelEyeBracket.Enabled =false;
									PanelClevisBracket.Enabled =false;
									PanelLinearCoupler.Enabled =false;
									PanelSphericalClevisBracket.Enabled=false;
									PanelSphericalPivotpin.Enabled=false;
									PanelSphericalRodEye.Enabled=false;
									lblinfo.Text="There is no standard accessory available for this cylinder";
								}
								if(PanelRodeye.Enabled ==true)
								{
									ArrayList plist=new ArrayList();
									plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Rod Eye",cd.Trim());
									DDLRodeye.Items.Clear();
									ArrayList list1 = new ArrayList();
									ArrayList list2 = new ArrayList();
									list1=(ArrayList)plist[0];
									list2=(ArrayList)plist[1];
									ListItem li=new ListItem();
									if(list1.Count >0)
									{
										for(int i=0;i<list1.Count;i++)
										{
											li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
											DDLRodeye.Items.Add(li);
										}
									}
									else
									{
										PanelRodeye.Enabled =false;
										DDLRodeye.Items.Clear();
										CBRodeye.Checked=false;
										ImgRodeye.ImageUrl="accessories/na.jpg";
									}
								}
								else
								{
									PanelRodeye.Enabled =false;
									DDLRodeye.Items.Clear();
									CBRodeye.Checked=false;
									ImgRodeye.ImageUrl="accessories/na.jpg";
								}
								if(PanelRodClevis.Enabled ==true)
								{
									ArrayList plist=new ArrayList();
									plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Rod Clevis",cd.Trim());
									DDLRodclevis.Items.Clear();
									ArrayList list1 = new ArrayList();
									ArrayList list2 = new ArrayList();
									list1=(ArrayList)plist[0];
									list2=(ArrayList)plist[1];
									ListItem li=new ListItem();
									if(list1.Count >0)
									{
										for(int i=0;i<list1.Count;i++)
										{
											li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
											DDLRodclevis.Items.Add(li);
										}
									}
									else
									{
										PanelRodClevis.Enabled =false;
										DDLRodclevis.Items.Clear();
										CBRodclevis.Checked=false;
										ImgRodclevis.ImageUrl="accessories/na.jpg";
									}
								}
								else
								{
									PanelRodClevis.Enabled =false;
									DDLRodclevis.Items.Clear();
									CBRodclevis.Checked=false;
									ImgRodclevis.ImageUrl="accessories/na.jpg";
								}
								if(PanelPivotPin.Enabled ==true)
								{
									ArrayList plist=new ArrayList();
									plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Pivot Pin",cd.Trim());
									DDLPivotpin.Items.Clear();
									ArrayList list1 = new ArrayList();
									ArrayList list2 = new ArrayList();
									list1=(ArrayList)plist[0];
									list2=(ArrayList)plist[1];
									ListItem li=new ListItem();
									if(list1.Count >0)
									{
										for(int i=0;i<list1.Count;i++)
										{
											li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
											DDLPivotpin.Items.Add(li);
										}
									}
									else
									{
										PanelPivotPin.Enabled =false;
										DDLPivotpin.Items.Clear();
										CBPivotpin.Checked=false;
										ImgPivotpin.ImageUrl="accessories/na.jpg";
									}
								}
								else
								{
									PanelPivotPin.Enabled =false;
									DDLPivotpin.Items.Clear();
									CBPivotpin.Checked=false;
									ImgPivotpin.ImageUrl="accessories/na.jpg";
								}
								if(PanelEyeBracket.Enabled ==true)
								{
									ArrayList plist=new ArrayList();
									plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Eye Bracket",cd.Trim());
									DDLEyeb.Items.Clear();
									ArrayList list1 = new ArrayList();
									ArrayList list2 = new ArrayList();
									list1=(ArrayList)plist[0];
									list2=(ArrayList)plist[1];
									ListItem li=new ListItem();
									if(list1.Count >0)
									{
										for(int i=0;i<list1.Count;i++)
										{
											li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
											DDLEyeb.Items.Add(li);
										}
									}
									else
									{
										PanelEyeBracket.Enabled =false;
										DDLEyeb.Items.Clear();
										CBEyeb.Checked=false;
										ImgEyeb.ImageUrl="accessories/na.jpg";
									}
								}
								else
								{
									PanelEyeBracket.Enabled =false;
									DDLEyeb.Items.Clear();
									CBEyeb.Checked=false;
									ImgEyeb.ImageUrl="accessories/na.jpg";
								}
								if(PanelClevisBracket.Enabled ==true)
								{
									ArrayList plist=new ArrayList();
									plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Clevis Bracket",cd.Trim());
									DDLClevisbracket.Items.Clear();
									ArrayList list1 = new ArrayList();
									ArrayList list2 = new ArrayList();
									list1=(ArrayList)plist[0];
									list2=(ArrayList)plist[1];
									ListItem li=new ListItem();
									if(list1.Count >0)
									{
										for(int i=0;i<list1.Count;i++)
										{
											li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
											DDLClevisbracket.Items.Add(li);
										}
									}
									else
									{
										PanelClevisBracket.Enabled =false;
										DDLClevisbracket.Items.Clear();
										CBClevisbracket.Checked=false;
										ImgClevisbracket.ImageUrl="accessories/na.jpg";
									}
								}
								else
								{
									PanelClevisBracket.Enabled =false;
									DDLClevisbracket.Items.Clear();
									CBClevisbracket.Checked=false;
									ImgClevisbracket.ImageUrl="accessories/na.jpg";
								}
								if(PanelLinearCoupler.Enabled ==true)
								{
									ArrayList plist=new ArrayList();
									plist =db.SelectAccessories_Discription2("WEB_Accessories_TableV11","Linear Alignment Coupler",kk.Trim());
									DDLLinearcoupler.Items.Clear();
									ArrayList list1 = new ArrayList();
									ArrayList list2 = new ArrayList();
									list1=(ArrayList)plist[0];
									list2=(ArrayList)plist[1];
									ListItem li=new ListItem();
									if(list1.Count >0)
									{
										for(int i=0;i<list1.Count;i++)
										{
											li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
											DDLLinearcoupler.Items.Add(li);
										}
									}
									else
									{
										PanelLinearCoupler.Enabled =false;
										DDLLinearcoupler.Items.Clear();
										CBLinearcoupler.Checked=false;
										ImgLinearcoupler.ImageUrl="accessories/na.jpg";
									}
								}
								else
								{
									PanelLinearCoupler.Enabled =false;
									DDLLinearcoupler.Items.Clear();
									CBLinearcoupler.Checked=false;
									ImgLinearcoupler.ImageUrl="accessories/na.jpg";
								}
								if(PanelSphericalRodEye.Enabled ==true)
								{
									ArrayList plist=new ArrayList();
									plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Spherical Rod Eye",cd.Trim());
									DDLSphericalrodeye.Items.Clear();
									ArrayList list1 = new ArrayList();
									ArrayList list2 = new ArrayList();
									list1=(ArrayList)plist[0];
									list2=(ArrayList)plist[1];
									ListItem li=new ListItem();
									if(list1.Count >0)
									{
										for(int i=0;i<list1.Count;i++)
										{
											li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
											DDLSphericalrodeye.Items.Add(li);
										}
									}
									else
									{
										PanelSphericalRodEye.Enabled =false;
										DDLSphericalrodeye.Items.Clear();
										CBSphericalrodeye.Checked=false;
										ImgSphericalrodeye.ImageUrl="accessories/na.jpg";
									}
								}
								else
								{
									PanelSphericalRodEye.Enabled =false;
									DDLSphericalrodeye.Items.Clear();
									CBSphericalrodeye.Checked=false;
									ImgSphericalrodeye.ImageUrl="accessories/na.jpg";
								}
								if(PanelSphericalClevisBracket.Enabled ==true)
								{
									ArrayList plist=new ArrayList();
									plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Spherical Clevis Bracket",cd.Trim());
									DDLSphericalclevisbracket.Items.Clear();
									ArrayList list1 = new ArrayList();
									ArrayList list2 = new ArrayList();
									list1=(ArrayList)plist[0];
									list2=(ArrayList)plist[1];
									ListItem li=new ListItem();
									if(list1.Count >0)
									{
										for(int i=0;i<list1.Count;i++)
										{
											li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
											DDLSphericalclevisbracket.Items.Add(li);
										}
									}
									else
									{
										PanelSphericalClevisBracket.Enabled =false;
										DDLSphericalclevisbracket.Items.Clear();
										CBSphericalclevisbracket.Checked=false;
										ImgSphericalclevisbraket.ImageUrl="accessories/na.jpg";
									}
								}
								else
								{
									PanelSphericalClevisBracket.Enabled =false;
									DDLSphericalclevisbracket.Items.Clear();
									CBSphericalclevisbracket.Checked=false;
									ImgSphericalclevisbraket.ImageUrl="accessories/na.jpg";
								}
								if(PanelSphericalPivotpin.Enabled ==true)
								{
									ArrayList plist=new ArrayList();
									plist =db.SelectAccessories_Discription1("WEB_Accessories_TableV11","Spherical Pivot Pin",cd.Trim());
									DDLSphericalpp.Items.Clear();
									ArrayList list1 = new ArrayList();
									ArrayList list2 = new ArrayList();
									list1=(ArrayList)plist[0];
									list2=(ArrayList)plist[1];
									ListItem li=new ListItem();
									if(list1.Count >0)
									{
										for(int i=0;i<list1.Count;i++)
										{
											li=new ListItem(list2[i].ToString().Trim(),list1[i].ToString().Trim());
											DDLSphericalpp.Items.Add(li);
										}
									}
									else
									{
										PanelSphericalPivotpin.Enabled =false;
										DDLSphericalpp.Items.Clear();
										CBSpericalpp.Checked=false;
										ImgSphericalpp.ImageUrl="accessories/na.jpg";
									}
								}
								else
								{
									PanelSphericalPivotpin.Enabled =false;
									DDLSphericalpp.Items.Clear();
									CBSpericalpp.Checked=false;
									ImgSphericalpp.ImageUrl="accessories/na.jpg";
								}
							}
							else
							{
								lblinfo.Text="There is no standard accessories avialable for this cylinder";
							}
						}
						else
						{
							Response.Redirect("Icylinder_page6.aspx");
						}
					}
				}
			}
			else
			{
				Response.Redirect("Login.aspx");
			}

		}

	

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void LBHome_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Icylinder_PageAcc.aspx");
		}

		protected void LBNext_Click(object sender, System.EventArgs e)
		{
			if(PanelRodeye.Enabled ==true || PanelRodClevis.Enabled ==true || PanelPivotPin.Enabled ==true || 
				PanelEyeBracket.Enabled ==true || PanelClevisBracket.Enabled ==true || PanelLinearCoupler.Enabled ==true ||
				PanelSphericalRodEye.Enabled ==true || PanelSphericalClevisBracket.Enabled ==true || PanelSphericalPivotpin.Enabled ==true)
			{
				
				if(CBRodeye.Checked ==true || CBRodclevis.Checked ==true || CBPivotpin.Checked ==true || CBEyeb.Checked ==true ||
					CBClevisbracket.Checked ==true || CBLinearcoupler.Checked ==true || CBSphericalrodeye.Checked ==true || 
					CBSphericalclevisbracket.Checked ==true ||	CBSpericalpp.Checked ==true)
				{
					ArrayList acceslist=new ArrayList();
					if(Session["Accessories"]!=null)
					{
						acceslist=(ArrayList)Session["Accessories"];
					}
					Others oth=new Others();
					ArrayList lst =new ArrayList();
					lst =(ArrayList)Session["User"];
					string discount="0";
					discount= db.SelectDiscount(lst[1].ToString(),"Accessories");
					decimal dc1=0.00m;
					decimal dc2=0.00m;
					decimal dc3=0.00m;
					decimal dc4=0.00m;
					decimal dc5=0.00m;
					lblinfo.Text="";
					if(CBRodeye.Checked ==true)
					{
						oth=new Others();
						string price="";
						price=db.SelectValue("ListPrice","WEB_Accessories_TableV11","PartNo",DDLRodeye.SelectedItem.Value.ToString().Trim());
						dc2 = 1m; 
						dc3 = Convert.ToDecimal(discount); 
						if(price.Trim() !="")
						{
							dc1 = Convert.ToDecimal(price);
							dc4 = dc1 * (1 - (dc3 / 100));
							dc5= dc4 * dc2;
							oth.Code="";
							oth.Desc="Rod Eye";
							oth.Ids=DDLRodeye.SelectedItem.Value.ToString().Trim();
							oth.Desc1="Rod Eye - "+DDLRodeye.SelectedItem.Text.Trim();
							oth.UnitP=dc4.ToString();
							oth.Qty=dc2.ToString();
							oth.Discount=dc3.ToString();
							oth.Price=dc5.ToString();
							acceslist.Add(oth);
						}
						else
						{
							oth.Code="";
							oth.Desc="Rod Eye";
							oth.Ids=DDLRodeye.SelectedItem.Value.ToString().Trim();
							oth.Desc1="Rod Eye - "+DDLRodeye.SelectedItem.Text.Trim();
							oth.UnitP="0.00";
							oth.Qty=dc2.ToString();
							oth.Discount=dc3.ToString();
							oth.Price="0.00";
							acceslist.Add(oth);
						}
					}
					if(CBRodclevis.Checked ==true)
					{
						oth=new Others();
						string price="";
						price=db.SelectValue("ListPrice","WEB_Accessories_TableV11","PartNo",DDLRodclevis.SelectedItem.Value.ToString().Trim());
						dc2 = 1m; 
						dc3 = Convert.ToDecimal(discount); 
						if(price.Trim() !="")
						{
							dc1 = Convert.ToDecimal(price);
							dc4 = dc1 * (1 - (dc3 / 100));
							dc5= dc4 * dc2;
							oth.Code="";
							oth.Desc="Rod Clevis";
							oth.Ids=DDLRodclevis.SelectedItem.Value.ToString().Trim();
							oth.Desc1="Rod Clevis - "+DDLRodclevis.SelectedItem.Text.Trim();
							oth.UnitP=dc4.ToString();
							oth.Qty=dc2.ToString();
							oth.Discount=dc3.ToString();
							oth.Price=dc5.ToString();
							acceslist.Add(oth);
							
						}
						else
						{
							oth.Code="";
							oth.Desc="Rod Clevis";
							oth.Ids=DDLRodclevis.SelectedItem.Value.ToString().Trim();
							oth.Desc1="Rod Clevis - "+DDLRodclevis.SelectedItem.Text.Trim();
							oth.UnitP="0.00";
							oth.Qty=dc2.ToString();
							oth.Discount=dc3.ToString();
							oth.Price="0.00";
							acceslist.Add(oth);
							
						}
					}
					if(CBPivotpin.Checked ==true)
					{
						oth=new Others();
						string price="";
						price=db.SelectValue("ListPrice","WEB_Accessories_TableV11","PartNo",DDLPivotpin.SelectedItem.Value.ToString().Trim());
						dc2 = 1m; 
						dc3 = Convert.ToDecimal(discount); 
						if(price.Trim() !="")
						{
							dc1 = Convert.ToDecimal(price);
							dc4 = dc1 * (1 - (dc3 / 100));
							dc5= dc4 * dc2;
							oth.Code="";
							oth.Desc="Pivot Pin";
							oth.Ids=DDLPivotpin.SelectedItem.Value.ToString().Trim();
							oth.Desc1="Pivot Pin - "+DDLPivotpin.SelectedItem.Text.Trim();
							oth.UnitP=dc4.ToString();
							oth.Qty=dc2.ToString();
							oth.Discount=dc3.ToString();
							oth.Price=dc5.ToString();
							acceslist.Add(oth);
						}
						else
						{
							oth.Code="";
							oth.Desc="Pivot Pin";
							oth.Ids=DDLPivotpin.SelectedItem.Value.ToString().Trim();
							oth.Desc1="Pivot Pin - "+DDLPivotpin.SelectedItem.Text.Trim();
							oth.UnitP="0.00";
							oth.Qty=dc2.ToString();
							oth.Discount=dc3.ToString();
							oth.Price="0.00";
							acceslist.Add(oth);
						
						}
					}
					if(CBEyeb.Checked ==true)
					{
						oth=new Others();
						string price="";
						price=db.SelectValue("ListPrice","WEB_Accessories_TableV11","PartNo",DDLEyeb.SelectedItem.Value.ToString().Trim());
						dc2 = 1m; 
						dc3 = Convert.ToDecimal(discount); 
						if(price.Trim() !="")
						{
							dc1 = Convert.ToDecimal(price);
							dc4 = dc1 * (1 - (dc3 / 100));
							dc5= dc4 * dc2;
							oth.Code="";
							oth.Desc="Eye Bracket";
							oth.Ids=DDLEyeb.SelectedItem.Value.ToString().Trim();
							oth.Desc1="Eye Bracket - "+DDLEyeb.SelectedItem.Text.Trim();
							oth.UnitP=dc4.ToString();
							oth.Qty=dc2.ToString();
							oth.Discount=dc3.ToString();
							oth.Price=dc5.ToString();
							acceslist.Add(oth);
						}
						else
						{
							oth.Code="";
							oth.Desc="Eye Bracket";
							oth.Ids=DDLEyeb.SelectedItem.Value.ToString().Trim();
							oth.Desc1="Eye Bracket - "+DDLEyeb.SelectedItem.Text.Trim();
							oth.UnitP="0.00";
							oth.Qty=dc2.ToString();
							oth.Discount=dc3.ToString();
							oth.Price="0.00";
							acceslist.Add(oth);
							
						}
					}
					if(CBClevisbracket.Checked ==true)
					{
						oth=new Others();
						string price="";
						price=db.SelectValue("ListPrice","WEB_Accessories_TableV11","PartNo",DDLClevisbracket.SelectedItem.Value.ToString().Trim());
						dc2 = 1m; 
						dc3 = Convert.ToDecimal(discount); 
						if(price.Trim() !="")
						{
							dc1 = Convert.ToDecimal(price);
							dc4 = dc1 * (1 - (dc3 / 100));
							dc5= dc4 * dc2;
							oth.Code="";
							oth.Desc="Clevis Bracket";
							oth.Ids=DDLClevisbracket.SelectedItem.Value.ToString().Trim();
							oth.Desc1="Clevis Bracket - "+DDLClevisbracket.SelectedItem.Text.Trim();
							oth.UnitP=dc4.ToString();
							oth.Qty=dc2.ToString();
							oth.Discount=dc3.ToString();
							oth.Price=dc5.ToString();
							acceslist.Add(oth);					
						}
						else
						{
							oth.Code="";
							oth.Desc="Clevis Bracket";
							oth.Ids=DDLClevisbracket.SelectedItem.Value.ToString().Trim();
							oth.Desc1="Clevis Bracket - "+DDLClevisbracket.SelectedItem.Text.Trim();
							oth.UnitP="0.00";
							oth.Qty=dc2.ToString();
							oth.Discount=dc3.ToString();
							oth.Price="0.00";
							acceslist.Add(oth);		
						
						}
					}
					if(CBLinearcoupler.Checked ==true)
					{
						oth=new Others();
						string price="";
						price=db.SelectValue("ListPrice","WEB_Accessories_TableV11","PartNo",DDLLinearcoupler.SelectedItem.Value.ToString().Trim());
						dc2 = 1m; 
						dc3 = Convert.ToDecimal(discount); 
						if(price.Trim() !="")
						{
							dc1 = Convert.ToDecimal(price);
							dc4 = dc1 * (1 - (dc3 / 100));
							dc5= dc4 * dc2;
							oth.Code="";
							oth.Desc="Linear Alignment Coupler";
							oth.Ids=DDLLinearcoupler.SelectedItem.Value.ToString().Trim();
							oth.Desc1="Linear Alignment Coupler - "+DDLLinearcoupler.SelectedItem.Text.Trim();
							oth.UnitP=dc4.ToString();
							oth.Qty=dc2.ToString();
							oth.Discount=dc3.ToString();
							oth.Price=dc5.ToString();
							acceslist.Add(oth);					
						}
						else
						{
							oth.Code="";
							oth.Desc="Linear Alignment Coupler";
							oth.Ids=DDLLinearcoupler.SelectedItem.Value.ToString().Trim();
							oth.Desc1="Linear Alignment Coupler - "+DDLLinearcoupler.SelectedItem.Text.Trim();
							oth.UnitP="0.00";
							oth.Qty=dc2.ToString();
							oth.Discount=dc3.ToString();
							oth.Price="0.00";
							acceslist.Add(oth);		
							
						}
					}
					if(CBSphericalrodeye.Checked ==true)
					{
						oth=new Others();
						string price="";
						price=db.SelectValue("ListPrice","WEB_Accessories_TableV11","PartNo",DDLSphericalrodeye.SelectedItem.Value.ToString().Trim());
						dc2 = 1m; 
						dc3 = Convert.ToDecimal(discount); 
						if(price.Trim() !="")
						{
							dc1 = Convert.ToDecimal(price);
							dc4 = dc1 * (1 - (dc3 / 100));
							dc5= dc4 * dc2;
							oth.Code="";
							oth.Desc="Spherical Rod Eye";
							oth.Ids=DDLSphericalrodeye.SelectedItem.Value.ToString().Trim();
							oth.Desc1="Spherical Rod Eye - "+DDLSphericalrodeye.SelectedItem.Text.Trim();
							oth.UnitP=dc4.ToString();
							oth.Qty=dc2.ToString();
							oth.Discount=dc3.ToString();
							oth.Price=dc5.ToString();
							acceslist.Add(oth);
						}
						else
						{
							oth.Code="";
							oth.Desc="Spherical Rod Eye";
							oth.Ids=DDLSphericalrodeye.SelectedItem.Value.ToString().Trim();
							oth.Desc1="Spherical Rod Eye - "+DDLSphericalrodeye.SelectedItem.Text.Trim();
							oth.UnitP="0.00";
							oth.Qty=dc2.ToString();
							oth.Discount=dc3.ToString();
							oth.Price="0.00";
							acceslist.Add(oth);
								
						}							
					}
					if(CBSphericalclevisbracket.Checked ==true)
					{
						oth=new Others();
						string price="";
						price=db.SelectValue("ListPrice","WEB_Accessories_TableV11","PartNo",DDLSphericalclevisbracket.SelectedItem.Value.ToString().Trim());
						dc2 = 1m; 
						dc3 = Convert.ToDecimal(discount); 
						if(price.Trim() !="")
						{
							dc1 = Convert.ToDecimal(price);
							dc4 = dc1 * (1 - (dc3 / 100));
							dc5= dc4 * dc2;
							oth.Code="";
							oth.Desc="Spherical Clevis Bracket";
							oth.Ids=DDLSphericalclevisbracket.SelectedItem.Value.ToString().Trim();
							oth.Desc1="Spherical Clevis Bracket - "+DDLSphericalclevisbracket.SelectedItem.Text.Trim();
							oth.UnitP=dc4.ToString();
							oth.Qty=dc2.ToString();
							oth.Discount=dc3.ToString();
							oth.Price=dc5.ToString();
							acceslist.Add(oth);
						}
						else
						{
							oth.Code="";
							oth.Desc="Spherical Clevis Bracket";
							oth.Ids=DDLSphericalclevisbracket.SelectedItem.Value.ToString().Trim();
							oth.Desc1="Spherical Clevis Bracket - "+DDLSphericalclevisbracket.SelectedItem.Text.Trim();
							oth.UnitP="0.00";
							oth.Qty=dc2.ToString();
							oth.Discount=dc3.ToString();
							oth.Price="0.00";
							acceslist.Add(oth);
								
						}
					}
					if(CBSpericalpp.Checked ==true)
					{
						oth=new Others();
						string price="";
						price=db.SelectValue("ListPrice","WEB_Accessories_TableV11","PartNo",DDLSphericalpp.SelectedItem.Value.ToString().Trim());
						dc2 = 1m; 
						dc3 = Convert.ToDecimal(discount); 
						if(price.Trim() !="")
						{
							dc1 = Convert.ToDecimal(price);
							dc4 = dc1 * (1 - (dc3 / 100));
							dc5= dc4 * dc2;
							oth.Code="";
							oth.Desc="Spherical Pivot Pin";
							oth.Ids=DDLSphericalclevisbracket.SelectedItem.Value.ToString().Trim();
							oth.Desc1="Spherical Pivot Pin - "+DDLSphericalclevisbracket.SelectedItem.Text.Trim();
							oth.UnitP=dc4.ToString();
							oth.Qty=dc2.ToString();
							oth.Discount=dc3.ToString();
							oth.Price=dc5.ToString();
							acceslist.Add(oth);
						}
						else
						{
							oth.Code="";
							oth.Desc="Spherical Pivot Pin";
							oth.Ids=DDLSphericalclevisbracket.SelectedItem.Value.ToString().Trim();
							oth.Desc1="Spherical Pivot Pin - "+DDLSphericalclevisbracket.SelectedItem.Text.Trim();
							oth.UnitP="0.00";
							oth.Qty=dc2.ToString();
							oth.Discount=dc3.ToString();
							oth.Price="0.00";
							acceslist.Add(oth);
							
						}
					}
					Session["Accessories"]=acceslist;
					Response.Redirect("Icylinder_Page6.aspx");
				
				}
				else
				{
					Response.Redirect("Icylinder_Page6.aspx");
				
				}
			}
			else
			{
				lblinfo.Text="There is no accessory available for the selected Rodend,KK or CD";
				Response.Redirect("Icylinder_Page6.aspx");
			
			}
		}
	}
}
