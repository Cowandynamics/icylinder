using System;

namespace iCylinderV1
{
	public class ExtraClass
	{
		public ExtraClass()
		{
			//issue #142 start
			    ids="";
				code="";
				desc="";
				desc1="";
				desc2="";
				desc3="";
				qty="";
				dicount="";
				unitp="";
				price="";
				ex1="";
				ex2="";
				ex3="";
				ex4="";
				ex5="";
			//issue #142 end
		}
		private string ids;
		private string code;
		private string desc;
		private string desc1;
		private string desc2;
		private string desc3;
		private string qty;
		private string dicount;
		private string unitp;
		private string price;
		private string ex1;
		private string ex2;
		private string ex3;
		private string ex4;
		private string ex5;
		public string Ids
		{
			get
			{
				return ids ;
			}
			set
			{
				ids  = value;
			}
		}

		public string Code
		{
			get
			{
				return code;
			}
			set
			{
				code = value;
			}
		}

		public string Desc
		{
			get
			{
				return desc;
			}
			set
			{
				desc = value;
			}
		}
		public string Desc1
		{
			get
			{
				return desc1;
			}
			set
			{
				desc1 = value;
			}
		}
		public string Desc2
		{
			get
			{
				return desc2;
			}
			set
			{
				desc2 = value;
			}
		}
		public string Desc3
		{
			get
			{
				return desc3;
			}
			set
			{
				desc3 = value;
			}
		}
		public string Qty
		{
			get
			{
				return qty;
			}
			set
			{
				qty = value;
			}
		}
		public string UnitP
		{
			get
			{
				return unitp;
			}
			set
			{
				unitp = value;
			}
		}
		public string Discount
		{
			get
			{
				return dicount;
			}
			set
			{
				dicount = value;
			}
		}
		public string Price
		{
			get
			{
				return price;
			}
			set
			{
				price = value;
			}
		}
		public string Ex1
		{
			get
			{
				return ex1;
			}
			set
			{
				ex1 = value;
			}
		}
		public string Ex2
		{
			get
			{
				return ex2;
			}
			set
			{
				ex2 = value;
			}
		}
		public string Ex3
		{
			get
			{
				return ex3;
			}
			set
			{
				ex3 = value;
			}
		}
		public string Ex4
		{
			get
			{
				return ex4;
			}
			set
			{
				ex4 = value;
			}
		}
		public string Ex5
		{
			get
			{
				return ex5;
			}
			set
			{
				ex5 = value;
			}
		}
	}
}
