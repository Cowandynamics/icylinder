using System;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for csSignature.
	/// </summary>
	/// 
	public class csSignature
	{
		private csSignature()
		{

		}
//		public csSignature()
//		{
//			//
//			// TODO: Add constructor logic here
//			//
//		}
		public static  string Get_Admin()
		{
			string strSignature = @"<br />"
				+ "<br />"
				+ "<br />Danny Cotton"
				+ "<br />Service a la clientele, Customer service"
				+ "<br />"
				+ "<br />Cowan Dynamics Inc."
				+ "<br />6194 Notre-Dame Ouest"
				+ "<br />Montreal, Quebec H4C 1V4"
				+ "<br />tel : 514-341-3415 x300"
				+ "<br />Fax : 514-341-0249"
				+ "<br />"
				+ "<br />e-mail : danny@cowandynamics.com"
				+ "<br />Web: www.cowandynamics.com";
				return strSignature;
		}
		public static  string Get_DWG()
		{
			string strSignature = @"<br />"
				+ "<br />"
				+ "<br />Azita Malek, Eng. M.Sc."
				+ "<br />Director, Engineering and R&D"
				+ "<br />"
				+ "<br />Cowan Dynamics Inc."
				+ "<br />6194 Notre-Dame Ouest"
				+ "<br />Montreal, Quebec H4C 1V4"
				+ "<br />tel : 514-341-3415 x323"
				+ "<br />Fax : 514-341-0249"
				+ "<br />"
				+ "<br />e-mail : amalek@cowandynamics.com"
				+ "<br />Web: www.cowandynamics.com";
				return strSignature;
		}
		public static  string Get_Dorin()
		{
			string strSignature = @"<br />"
				+ "<br />"
				+ "<br />Dorin Taranu"
				+ "<br />Project Manager Process Automation"
				+ "<br />"
				+ "<br />Cowan Dynamics Inc."
				+ "<br />6194 Notre-Dame Ouest"
				+ "<br />Montreal, Quebec H4C 1V4"
				+ "<br />tel : 514-341-3415 x302"
				+ "<br />Fax : 514-341-0249"
				+ "<br />"
				+ "<br />e-mail : dtaranu@cowandynamics.com"
				+ "<br />Web: www.cowandynamics.com";
			return strSignature;
		}
		public static  string Get_Jenny()
		{
			string strSignature = @"<br />"
				+ "<br />"
				+ "<br />Jenny Luo, P.Eng."
				+ "<br />Application Engineer"
				+ "<br />"
				+ "<br />Cowan Dynamics Inc."
				+ "<br />6194 Notre-Dame Ouest"
				+ "<br />Montreal, Quebec H4C 1V4"
				+ "<br />tel : 514-341-3415 x322"
				+ "<br />Fax : 514-341-0249"
				+ "<br />"
				+ "<br />e-mail : jenny.l@cowandynamics.com"
				+ "<br />Web: www.cowandynamics.com";
			return strSignature;
		}
		public static  string Get_Jean()
		{
			string strSignature = @"<br />"
				+ "<br />"
				+ "<br />Jean Behara P.Eng."
				+ "<br />VP, Business Development"
				+ "<br />"
				+ "<br />Cowan Dynamics Inc."
				+ "<br />211 Watline Ave, S205"
				+ "<br />Mississauga, Ontario, L4Z 1P3"
				+ "<br />P: 905-829-2910 x201"
				+ "<br />C: 905-208-0985"
				+ "<br />"
				+ "<br />e-mail : jbehara@cowandynamics.com"
				+ "<br />Web: www.cowandynamics.com";
			return strSignature;
		}
		public static  string Get_Stephane()
		{
			string strSignature = @"<br />"
				+ "<br />"
				+ "<br />Stephane Meunier, Eng., PMP"
				+ "<br />Director, International Business Development"
				+ "<br />"
				+ "<br />Cowan Dynamics Inc."
				+ "<br />6194 Notre-Dame Ouest"
				+ "<br />Montreal, Quebec H4C 1V4"
				+ "<br />tel : 514-341-3415 x305"
				+ "<br />Fax : 514-341-0249"
				+ "<br />"
				+ "<br />e-mail : smeunier@cowandynamics.com"
				+ "<br />Web: www.cowandynamics.com";
			return strSignature;
		}
		public static string Get_Agent(string email)
		{
			string strEmail= "admin@cowandynamics.com";
			switch(email)
			{
				case "dtaranu@cowandynamics.com":
					strEmail = Get_Dorin();
					break;
				case "jenny.l@cowandynamics.com":
					strEmail = Get_Jenny();
					break;
				case "jbehara@cowandynamics.com":
					strEmail=Get_Jean();
					break;
				case "smeunier@cowandynamics.com":
					strEmail = Get_Stephane();
					break;
				default:
					strEmail= Get_Admin();
					break;
			}
			return strEmail;
		}
	}
}
