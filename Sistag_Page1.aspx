<%@ Page language="c#" Codebehind="Sistag_Page1.aspx.cs" AutoEventWireup="True" Inherits="iCylinderV1.Sistag_Page1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Velan_Page1</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE style="WIDTH: 1000px; HEIGHT: 487px" id="Table1" border="0" cellSpacing="0" cellPadding="0"
				width="1000" bgColor="lightgrey">
				<TR>
					<TD height="20" width="50"></TD>
					<TD style="WIDT: 484px" height="20" colSpan="3" align="center">
						<P><asp:label id="Label1" runat="server" Font-Size="Small" Font-Underline="True" BorderColor="Transparent"
								BackColor="Transparent"> Cylinder Configurator</asp:label></P>
						<P><asp:linkbutton style="Z-INDEX: 0" id="LBBatchQuote" runat="server" Font-Size="Smaller" CausesValidation="False"
								Font-Bold="True" onclick="LBBatchQuote_Click">Click Here for Batch Quote</asp:linkbutton></P>
					</TD>
					<TD height="20" width="50"></TD>
				</TR>
				<TR>
					<TD width="50"></TD>
					<TD bgColor="#dcdcdc" align="center"><asp:panel id="Panel3" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
							<TABLE id="Table11" border="0" cellSpacing="0" borderColor="silver" cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label2" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Series</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:radiobuttonlist style="Z-INDEX: 0" id="RBLSeries" runat="server" Font-Size="Smaller" BorderColor="#404040"
											Height="8px" RepeatDirection="Horizontal">
											<asp:ListItem Value="A" Selected="True">Series A&lt;br&gt;&lt;img src='images\A.jpg' alt='A' /&gt;</asp:ListItem>
											<asp:ListItem Value="AT">Series AT&lt;br&gt;&lt;img src='images\AT.jpg' alt='AT' /&gt;</asp:ListItem>
										</asp:radiobuttonlist></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:radiobuttonlist id="RBLDoubleOrSingle" runat="server" Font-Size="Smaller" BorderColor="#404040"
											Height="8px" RepeatDirection="Horizontal" Width="200px" RepeatLayout="Flow" AutoPostBack="True" onselectedindexchanged="RBLDoubleOrSingle_SelectedIndexChanged">
											<asp:ListItem Value="No" Selected="True">Single Ended</asp:ListItem>
											<asp:ListItem Value="Yes">Double Ended</asp:ListItem>
										</asp:radiobuttonlist></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="Panel1" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
							<TABLE id="Table3" border="0" cellSpacing="0" borderColor="silver" cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label4" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Bore Size</asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:radiobuttonlist id="RBLBore" runat="server" Font-Size="Smaller" BorderStyle="None" RepeatDirection="Horizontal"
											AutoPostBack="True" RepeatColumns="6" onselectedindexchanged="RBLBore_SelectedIndexChanged"></asp:radiobuttonlist></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="Panel2" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
							<TABLE style="HEIGHT: 8px" id="Table5" border="0" cellSpacing="0" borderColor="silver"
								cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:label id="Label5" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Stroke</asp:label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:textbox id="TxtStroke" runat="server" Font-Size="XX-Small" Width="64px" AutoPostBack="True" ontextchanged="TxtStroke_TextChanged"></asp:textbox></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:label id="Label3" runat="server" Font-Size="XX-Small" BorderColor="Transparent" BackColor="Transparent"
											ForeColor="Brown">Min= 0.00, Max= 120.00(Consult Factory, if stroke >120)</asp:label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px" align="center">
										<asp:RequiredFieldValidator id="RequiredFieldValidator4" runat="server" Font-Size="8pt" ErrorMessage="Stroke Required"
											ControlToValidate="TxtStroke"></asp:RequiredFieldValidator></TD>
								</TR>
							</TABLE>
							<asp:LinkButton id="BtnAdvanced" runat="server" Font-Size="8pt" onclick="BtnAdvanced_Click">Click Here for Stop Tube</asp:LinkButton>
						</asp:panel><asp:panel id="PStoptube" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px"
							Visible="False">
							<TABLE id="Table6" border="0" cellSpacing="0" borderColor="silver" cellPadding="0" width="299">
								<TR>
									<TD align="center">
										<asp:RadioButtonList id="RBStrokeType" runat="server" Font-Size="Smaller" RepeatDirection="Horizontal"
											Width="216px" RepeatLayout="Flow">
											<asp:ListItem Value="Standard" Selected="True">Standard</asp:ListItem>
											<asp:ListItem Value="Double Piston Design">Double Piston Design</asp:ListItem>
										</asp:RadioButtonList></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:label id="Label47" runat="server" Font-Size="Smaller" BackColor="Transparent" Height="19"
											Width="128px">Stop Tube Length :</asp:label>
										<asp:TextBox id="TxtStopTube" runat="server" Font-Size="XX-Small" Width="53" AutoPostBack="True" ontextchanged="TxtStopTube_TextChanged"></asp:TextBox></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:Label id="Label7" runat="server" Font-Size="Smaller" Width="128px">Enter Effective Stroke :</asp:Label>
										<asp:TextBox id="TxtEffectiveStrok" runat="server" Font-Size="XX-Small" Width="53px" AutoPostBack="True" ontextchanged="TxtEffectiveStrok_TextChanged"></asp:TextBox></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:panel id="Panel10" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
							<TABLE id="Table18" border="0" cellSpacing="0" borderColor="silver" cellPadding="0" width="299">
								<TR>
								</TR>
								<TR>
								</TR>
								<TR>
								</TR>
							</TABLE>
							<TABLE id="Table19" border="0" cellSpacing="0" borderColor="silver" cellPadding="0" width="299">
								<TR>
									<TD colSpan="2" align="center">
										<asp:label id="Label24" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
											BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Mount</asp:label></TD>
								</TR>
								<TR>
									<TD colSpan="2" align="center">
										<asp:image id="Image2" runat="server" ImageUrl="mounts/MX0.jpg"></asp:image>
										<asp:Label id="Label20" runat="server" Font-Size="8pt" ForeColor="Black" Width="284px" Visible="False">MX0 Mount: No Mount</asp:Label></TD>
								</TR>
							</TABLE>
							<asp:RadioButtonList id="rblMounts" runat="server" Font-Size="Smaller" Height="32px" RepeatDirection="Horizontal"
								Width="269px">
								<asp:ListItem Value="X0" Selected="True">MX0 Mount No Mount</asp:ListItem>
								<asp:ListItem Value="X3">MX3 Mount</asp:ListItem>
								<asp:ListItem Value="I">ISO Mount</asp:ListItem>
							</asp:RadioButtonList>
						</asp:panel></TD>
					<TD bgColor="#dcdcdc" colSpan="2" align="center">
						<TABLE id="Table12" border="0" cellSpacing="0" cellPadding="0" width="300">
							<TR>
								<TD align="center"><asp:panel id="Panel4" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
										<TABLE style="HEIGHT: 35px" id="Table2" border="0" cellSpacing="0" borderColor="silver"
											cellPadding="0" width="299">
											<TR>
												<TD align="center">
													<asp:label id="Label6" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
														BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Rod</asp:label></TD>
											</TR>
											<TR>
												<TD align="center">
													<asp:radiobuttonlist id="RBL1stRodSize" runat="server" Font-Size="Smaller" BorderStyle="None" RepeatDirection="Horizontal"
														AutoPostBack="True" RepeatColumns="4"></asp:radiobuttonlist></TD>
											</TR>
										</TABLE>
									</asp:panel><asp:panel id="Panel5" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
										<TABLE id="Table8" border="0" cellSpacing="0" borderColor="silver" cellPadding="0" width="299">
											<TR>
												<TD align="center">
													<asp:label id="Label9" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
														BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Rod End</asp:label></TD>
											</TR>
											<TR>
												<TD align="center">
													<asp:image id="Img1stRodEnd" runat="server" Height="70px" Width="100px" ImageUrl="rodends\smallfemale.jpg"></asp:image></TD>
											</TR>
											<TR>
												<TD align="center">
													<asp:radiobuttonlist id="RBL1Rodend1" runat="server" Font-Size="Smaller" BorderStyle="None" Height="1px"
														RepeatDirection="Horizontal" Width="144px" RepeatLayout="Flow">
														<asp:ListItem Value="A4" Selected="True">Small Female (Series A)</asp:ListItem>
													</asp:radiobuttonlist></TD>
											</TR>
										</TABLE>
										<asp:LinkButton id="LB1stRodAdvanced" runat="server" Font-Size="8pt" Width="160" onclick="LB1stRodAdvanced_Click">Advanced Options</asp:LinkButton>
									</asp:panel><asp:panel id="P1stAdvanced" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px"
										Visible="False">
										<TABLE id="Table4" border="0" cellSpacing="0" borderColor="silver" cellPadding="0" width="299">
											<TR>
												<TD>
													<asp:label id="Label13" runat="server" Font-Size="Smaller" BackColor="Transparent" Height="19"
														Width="88px"> A Dimension :</asp:label>
													<asp:textbox id="TXTThread" runat="server" Font-Size="XX-Small" Height="20px" Width="52px" AutoPostBack="True" ontextchanged="TXTThread_TextChanged"></asp:textbox>
													<asp:Label id="Label68" runat="server" Font-Size="XX-Small" ForeColor="SlateGray">(Min = 0.00 & Max = 12.00)</asp:Label></TD>
											</TR>
											<TR>
												<TD>
													<asp:label id="Label14" runat="server" Font-Size="Smaller" BackColor="Transparent" Height="19"
														Width="88px"> W  Dimension :</asp:label>
													<asp:textbox id="TXTRodEx" runat="server" Font-Size="XX-Small" Height="20px" Width="52px" AutoPostBack="True" ontextchanged="TXTRodEx_TextChanged"></asp:textbox>
													<asp:Label id="lblw" runat="server" Font-Size="XX-Small" ForeColor="SlateGray">(Min = 0.00 & Max = 24.00)</asp:Label></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
								<TD align="center"><asp:panel id="P2ndRod" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px"
										Visible="False">
										<TABLE style="HEIGHT: 35px" id="Table7" border="0" cellSpacing="0" borderColor="silver"
											cellPadding="0" width="299">
											<TR>
												<TD align="center">
													<asp:label id="Label10" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
														BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">2nd Rod</asp:label></TD>
											</TR>
											<TR>
												<TD align="center">
													<asp:radiobuttonlist id="RBL2ndRodSize" runat="server" Font-Size="Smaller" BorderStyle="None" RepeatDirection="Horizontal"
														AutoPostBack="True" RepeatColumns="4"></asp:radiobuttonlist></TD>
											</TR>
										</TABLE>
									</asp:panel><asp:panel id="P2ndRodnd" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px"
										Visible="False">
										<TABLE id="Table9" border="0" cellSpacing="0" borderColor="silver" cellPadding="0" width="299">
											<TR>
												<TD align="center">
													<asp:label id="Label12" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
														BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">2nd Rod End</asp:label></TD>
											</TR>
											<TR>
												<TD align="center">
													<asp:image id="Img2ndRodend" runat="server" Height="70px" Width="100px" ImageUrl="rodends\smallfemale_double.jpg"></asp:image></TD>
											</TR>
											<TR>
												<TD style="HEIGHT: 20px" align="center">
													<asp:radiobuttonlist id="RBL2Rodend2" runat="server" Font-Size="Smaller" BorderStyle="None" RepeatDirection="Horizontal"
														Width="200px" RepeatLayout="Flow" RepeatColumns="2">
														<asp:ListItem Value="RA4" Selected="True">Small Female (Series A)</asp:ListItem>
													</asp:radiobuttonlist></TD>
											</TR>
										</TABLE>
										<asp:LinkButton id="LB2ndRodAdvanced" runat="server" Font-Size="8pt" Width="160px" onclick="LB2ndRodAdvanced_Click">Advanced Options</asp:LinkButton>
									</asp:panel><asp:panel id="P2ndAdvanced" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px"
										Visible="False">
										<TABLE id="Table10" border="0" cellSpacing="0" borderColor="silver" cellPadding="0" width="299">
											<TR>
												<TD>
													<asp:label id="Label18" runat="server" Font-Size="Smaller" BackColor="Transparent" Height="19"
														Width="88px"> A Dimension :</asp:label>
													<asp:textbox id="TxtSecThreadEx" runat="server" Font-Size="XX-Small" Height="20px" Width="53"
														AutoPostBack="True" ontextchanged="TxtSecThreadEx_TextChanged"></asp:textbox>
													<asp:Label id="Label17" runat="server" Font-Size="XX-Small" ForeColor="SlateGray">(Min = 0.00 & Max = 12.00)</asp:Label></TD>
											</TR>
											<TR>
												<TD>
													<asp:label id="Label16" runat="server" Font-Size="Smaller" BackColor="Transparent" Height="19"
														Width="88px"> W  Dimension :</asp:label>
													<asp:textbox id="TxtSecRodEx" runat="server" Font-Size="XX-Small" Height="20px" Width="53px"
														AutoPostBack="True" ontextchanged="TxtSecRodEx_TextChanged"></asp:textbox>
													<asp:Label id="lblsecminmax1" runat="server" Font-Size="XX-Small" ForeColor="SlateGray">(Min = 0.00 & Max = 24.00)</asp:Label></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
						</TABLE>
						<TABLE id="Table14" border="0" cellSpacing="0" cellPadding="0" width="300">
							<TR>
								<TD><asp:panel id="Panel6" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
										<TABLE id="Table13" border="0" cellSpacing="0" borderColor="silver" cellPadding="0" width="299">
											<TR>
												<TD align="center">
													<asp:label id="Label11" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
														BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Cushion</asp:label></TD>
											</TR>
											<TR>
												<TD align="center">
													<asp:Label id="Label8" runat="server" Font-Size="Smaller" Width="128px">No Cushions</asp:Label></TD>
											</TR>
										</TABLE>
									</asp:panel><asp:panel id="Panel7" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
										<TABLE id="Table15" border="0" cellSpacing="0" borderColor="silver" cellPadding="0" width="299">
											<TR>
												<TD align="center">
													<asp:label id="Label19" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
														BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Ports</asp:label></TD>
											</TR>
											<TR>
												<TD align="center">
													<asp:Label id="Label15" runat="server" Font-Size="Smaller" Width="128px" Visible="False">NPT Ports</asp:Label>
													<asp:RadioButtonList id="rblPorts" runat="server" Font-Size="Smaller" Height="24px" RepeatDirection="Horizontal"
														Width="166px">
														<asp:ListItem Value="N" Selected="True">NPT Ports</asp:ListItem>
														<asp:ListItem Value="B">BSPP Ports</asp:ListItem>
													</asp:RadioButtonList></TD>
											</TR>
										</TABLE>
									</asp:panel><asp:panel id="Panel9" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
										<TABLE id="Table17" border="0" cellSpacing="0" borderColor="silver" cellPadding="0" width="299">
											<TR>
												<TD align="center">
													<asp:label id="Label23" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
														BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Port Positions</asp:label></TD>
											</TR>
											<TR>
												<TD align="center">
													<asp:Label id="Label22" runat="server" Font-Size="Smaller" Width="128px">Position 1 both ends</asp:Label></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
								<TD><asp:panel id="Panel8" runat="server" BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
										<TABLE id="Table16" border="0" cellSpacing="0" borderColor="silver" cellPadding="0" width="299">
											<TR>
												<TD align="center">
													<asp:label id="Label21" runat="server" Font-Size="Smaller" Font-Underline="True" BorderColor="Transparent"
														BackColor="Transparent" Font-Bold="True" ForeColor="Maroon">Seals</asp:label></TD>
											</TR>
											<TR>
												<TD align="center">
													<asp:radiobuttonlist id="RBLSeal" runat="server" Font-Size="Smaller" BorderStyle="None" RepeatColumns="1">
														<asp:ListItem Value="N" Selected="True">Standard Seals</asp:ListItem>
														<asp:ListItem Value="F">High Temp Seals</asp:ListItem>
														<asp:ListItem Value="L">Low temp Seals</asp:ListItem>
													</asp:radiobuttonlist></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
						</TABLE>
					</TD>
					<TD width="50"></TD>
				</TR>
				<TR>
					<TD height="20" width="50"></TD>
					<TD language="300" height="20" align="center"></TD>
					<TD height="20" colSpan="2" align="center"><asp:label style="Z-INDEX: 0" id="LblView" runat="server" Font-Size="Smaller" BackColor="Transparent"
							Height="19"></asp:label><asp:label id="lblhidden" runat="server" Font-Size="XX-Small" Visible="False"></asp:label></TD>
					<TD height="20" width="50"><asp:linkbutton id="BtnNext" runat="server" Font-Size="Smaller" Font-Bold="True" onclick="BtnNext_Click">Next</asp:linkbutton></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
