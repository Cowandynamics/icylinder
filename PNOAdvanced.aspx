<%@ Page language="c#" Codebehind="PNOAdvanced.aspx.cs" AutoEventWireup="false" Inherits="iCylinderV1.PNOAdvanced" %>
<%@ Register TagPrefix="jlc" Namespace="JLovell.WebControls" Assembly="StaticPostBackPosition" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Part Number Generator</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE style="Z-INDEX: 102; BORDER-BOTTOM: lightgrey thin solid; POSITION: absolute; BORDER-LEFT: darkgray thin solid; WIDTH: 496px; BORDER-TOP: lightgrey thin solid; TOP: 0px; RIGHT: 0px; BORDER-RIGHT: darkgray thin solid; LEFT: 16px"
				id="Table11" border="0" cellSpacing="0" cellPadding="0" width="496" align="left">
				<TR>
					<TD width="498" align="center"><asp:label id="Label1" runat="server" ForeColor="Red" BackColor="White" Font-Underline="True"
							Font-Size="Smaller" Width="476px" Height="20px">I- Cylinder</asp:label></TD>
				</TR>
				<TR>
					<TD width="498" align="left"><asp:label id="Label42" runat="server" ForeColor="White" BackColor="Gray" Font-Size="Smaller"
							Width="496px" Height="18px">Select options from the dropdown box below.</asp:label></TD>
				</TR>
				<TR>
					<TD width="498"><asp:panel id="Panel1" runat="server" Width="488px" BorderColor="SlateBlue" BorderStyle="None">
							<TABLE style="WIDTH: 488px; HEIGHT: 228px" id="Table1" border="0" cellSpacing="0" cellPadding="0"
								width="488">
								<TR>
									<TD style="WIDTH: 13px" noWrap align="left"></TD>
									<TD bgColor="gainsboro" width="150" noWrap align="left">
										<asp:label id="Label2" runat="server" Height="19" Width="163px" Font-Size="Smaller" BackColor="Transparent">Select The Series :</asp:label></TD>
									<TD style="WIDTH: 1px" noWrap align="left"></TD>
									<TD width="200" noWrap align="left">
										<asp:dropdownlist id="DDLSeries" runat="server" Height="22" Width="316px" Font-Size="XX-Small" AutoPostBack="True">
											<asp:ListItem Value="Select Series">Select Series</asp:ListItem>
										</asp:dropdownlist></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 13px" noWrap align="left"></TD>
									<TD bgColor="gainsboro" width="150" noWrap align="left">
										<asp:label id="Label3" runat="server" Height="19" Width="163px" Font-Size="Smaller" BackColor="Transparent">Select The Bore Size :</asp:label></TD>
									<TD style="WIDTH: 1px" noWrap align="left"></TD>
									<TD width="200" noWrap align="left">
										<asp:dropdownlist id="DDLBore" runat="server" Height="22" Width="104px" Font-Size="XX-Small" AutoPostBack="True">
											<asp:ListItem Value="Select Bore Size" Selected="True">Select Bore Size</asp:ListItem>
										</asp:dropdownlist></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 13px" noWrap align="left"></TD>
									<TD bgColor="gainsboro" width="150" noWrap align="left">
										<asp:label id="Label4" runat="server" Height="19" Width="163px" Font-Size="Smaller" BackColor="Transparent">Select The Rod Size :</asp:label></TD>
									<TD style="WIDTH: 1px" noWrap align="left"></TD>
									<TD width="200" noWrap align="left">
										<asp:dropdownlist id="DDLRod" runat="server" Height="22" Width="104px" Font-Size="XX-Small" AutoPostBack="True">
											<asp:ListItem Value="Select Rod Size" Selected="True">Select Rod Size</asp:ListItem>
										</asp:dropdownlist></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 13px; HEIGHT: 12px" noWrap align="left"></TD>
									<TD style="HEIGHT: 12px" bgColor="gainsboro" width="150" noWrap align="left">
										<asp:label id="Label40" runat="server" Height="19" Width="163px" Font-Size="Smaller" BackColor="Transparent">Enter Stroke :</asp:label></TD>
									<TD style="WIDTH: 1px; HEIGHT: 12px" noWrap align="left"></TD>
									<TD width="300" noWrap align="left">
										<asp:textbox id="TxtStroke" runat="server" Height="18px" Width="52px" Font-Size="XX-Small" AutoPostBack="True"
											EnableViewState="False" MaxLength="7"></asp:textbox>
										<asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" Font-Size="XX-Small" ControlToValidate="TxtStroke"
											ErrorMessage="Enter Stroke (***.** Format)">*</asp:requiredfieldvalidator>
										<asp:Label id="Label41" runat="server" Width="232px" Font-Size="XX-Small" ForeColor="RoyalBlue">Min= 0.00, Max= 120.00(Consult Factory, Stroke>120)</asp:Label></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 13px" noWrap align="left"></TD>
									<TD bgColor="gainsboro" width="150" noWrap align="left">
										<asp:label id="Label9" runat="server" Height="19" Width="163px" Font-Size="Smaller" BackColor="Transparent">Select The Mount :</asp:label></TD>
									<TD style="WIDTH: 1px" noWrap align="left"></TD>
									<TD width="200" noWrap align="left">
										<asp:dropdownlist id="DDLMount" runat="server" Height="22" Width="256px" Font-Size="XX-Small" AutoPostBack="True"></asp:dropdownlist></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 13px" noWrap align="left"></TD>
									<TD bgColor="gainsboro" width="150" noWrap align="left">
										<asp:label id="Label5" runat="server" Height="19" Width="163px" Font-Size="Smaller" BackColor="Transparent">Select The Rod End  :</asp:label></TD>
									<TD style="WIDTH: 1px" noWrap align="left"></TD>
									<TD width="200" noWrap align="left">
										<asp:dropdownlist id="DDLRodEnd" runat="server" Height="22" Width="240px" Font-Size="XX-Small"></asp:dropdownlist></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 13px" noWrap align="left"></TD>
									<TD bgColor="gainsboro" width="150" noWrap align="left">
										<asp:label id="Label21" runat="server" Height="19" Width="163px" Font-Size="Smaller" BackColor="Transparent">Select The Ports :</asp:label></TD>
									<TD style="WIDTH: 1px" noWrap align="left"></TD>
									<TD width="200" noWrap align="left">
										<asp:dropdownlist id="DDLPorttype" runat="server" Height="22" Width="168px" Font-Size="XX-Small" AutoPostBack="True"></asp:dropdownlist></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 13px" noWrap align="left"></TD>
									<TD bgColor="gainsboro" width="150" noWrap align="left">
										<asp:label id="Label22" runat="server" Height="19" Width="163px" Font-Size="Smaller" BackColor="Transparent">Select The Ports Position :</asp:label></TD>
									<TD style="WIDTH: 1px" noWrap align="left"></TD>
									<TD width="200" noWrap align="left">
										<asp:dropdownlist id="DDLPortPos" runat="server" Height="22" Width="240px" Font-Size="XX-Small"></asp:dropdownlist></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 13px" noWrap align="left"></TD>
									<TD bgColor="gainsboro" width="150" noWrap align="left">
										<asp:label id="Label15" runat="server" Height="19" Width="163px" Font-Size="Smaller" BackColor="Transparent">Select The Cushions :</asp:label></TD>
									<TD style="WIDTH: 1px" noWrap align="left"></TD>
									<TD width="200" noWrap align="left">
										<asp:dropdownlist id="DDLCushions" runat="server" Height="22" Width="216px" Font-Size="XX-Small" AutoPostBack="True"></asp:dropdownlist></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 13px" noWrap align="left"></TD>
									<TD bgColor="gainsboro" width="150" noWrap align="left">
										<asp:label id="Label27" runat="server" Width="163px" Font-Size="Smaller" BackColor="Transparent">Select The Cushions Position :</asp:label></TD>
									<TD style="WIDTH: 1px" noWrap align="left"></TD>
									<TD width="200" noWrap align="left">
										<asp:dropdownlist id="DDLCushionPosition" runat="server" Height="22px" Width="200px" Font-Size="XX-Small"></asp:dropdownlist></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 13px" noWrap align="left"></TD>
									<TD bgColor="gainsboro" width="150" noWrap align="left">
										<asp:label id="Label18" runat="server" Height="19" Width="163px" Font-Size="Smaller" BackColor="Transparent">Select The Seal :</asp:label></TD>
									<TD style="WIDTH: 1px" noWrap align="left"></TD>
									<TD width="200" noWrap align="left">
										<asp:dropdownlist id="DDLSeal" runat="server" Height="22px" Width="168px" Font-Size="XX-Small"></asp:dropdownlist></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD width="498"><asp:label id="Label38" runat="server" ForeColor="White" BackColor="Gray" Font-Size="Smaller"
							Width="264px" Height="19px" Font-Bold="True">Rod</asp:label><asp:checkbox id="CBRod" runat="server" Font-Size="Smaller" AutoPostBack="True" Text="Edit"></asp:checkbox><asp:panel id="PanelRod" runat="server" Width="488px" BorderColor="SlateBlue" BorderStyle="None">
							<TABLE style="WIDTH: 496px; HEIGHT: 191px" id="Table2" border="0" cellSpacing="0" cellPadding="0"
								width="496">
								<TR>
									<TD style="HEIGHT: 7px" width="12" noWrap align="left"></TD>
									<TD style="WIDTH: 242px; HEIGHT: 7px" bgColor="gainsboro" width="242" noWrap align="left">
										<asp:label id="Label16" runat="server" Height="19" Width="233" Font-Size="Smaller" BackColor="Transparent">Select Piston Rod Material :</asp:label></TD>
									<TD style="WIDTH: 1px; HEIGHT: 7px" width="1" noWrap align="left"></TD>
									<TD style="WIDTH: 216px" width="216" noWrap align="left">
										<asp:DropDownList id="DDLSSpistinrod" runat="server" Width="208px" Font-Size="XX-Small"></asp:DropDownList></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 12px" width="12" noWrap align="left"></TD>
									<TD style="WIDTH: 242px; HEIGHT: 12px" bgColor="gainsboro" width="242" noWrap align="left">
										<asp:label id="Label13" runat="server" Height="19" Width="233px" Font-Size="Smaller" BackColor="Transparent">Enter The A Dimension :</asp:label></TD>
									<TD style="WIDTH: 1px; HEIGHT: 12px" width="1" noWrap align="left"></TD>
									<TD style="WIDTH: 216px; HEIGHT: 12px" width="216" noWrap align="left">
										<asp:textbox id="TXTThread" runat="server" Height="20px" Width="52px" Font-Size="XX-Small" AutoPostBack="True"></asp:textbox>
										<asp:RegularExpressionValidator id="RegularExpressionValidator2" runat="server" Font-Size="XX-Small" ControlToValidate="TXTThread"
											ErrorMessage="Enter Value  (**.** Format)" ValidationExpression="^[\d]?[\d]?\d\.\d\d$">*</asp:RegularExpressionValidator>
										<asp:Label id="Label68" runat="server" Font-Size="XX-Small" ForeColor="RoyalBlue">(Min = 0.00 & Max = 12.00)</asp:Label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 8px" width="12" noWrap align="left"></TD>
									<TD style="WIDTH: 242px; HEIGHT: 8px" bgColor="gainsboro" width="242" noWrap align="left">
										<asp:label id="Label14" runat="server" Height="19" Width="233" Font-Size="Smaller" BackColor="Transparent">Enter The W  Dimension :</asp:label></TD>
									<TD style="WIDTH: 1px; HEIGHT: 8px" width="1" noWrap align="left"></TD>
									<TD style="WIDTH: 216px; HEIGHT: 8px" width="216" noWrap align="left">
										<asp:textbox id="TXTRodEx" runat="server" Height="20px" Width="52px" Font-Size="XX-Small" AutoPostBack="True"></asp:textbox>
										<asp:RegularExpressionValidator id="RegularExpressionValidator3" runat="server" Font-Size="XX-Small" ControlToValidate="TXTRodEx"
											ErrorMessage="Enter Value  (**.** Format)" ValidationExpression="^[\d]?[\d]?\d\.\d\d$">*</asp:RegularExpressionValidator>
										<asp:Label id="lblw" runat="server" Font-Size="XX-Small" ForeColor="RoyalBlue">(Min = 0.00 & Max = 24.00)</asp:Label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 17px" width="12" noWrap align="left"></TD>
									<TD style="WIDTH: 242px; HEIGHT: 17px" bgColor="gainsboro" width="242" noWrap align="left">
										<asp:label id="Label17" runat="server" Height="19px" Width="233px" Font-Size="Smaller" BackColor="Transparent">Use Double Rod :</asp:label></TD>
									<TD style="WIDTH: 1px; HEIGHT: 17px" width="1" noWrap align="left"></TD>
									<TD style="WIDTH: 216px" width="216" noWrap align="left">
										<asp:radiobuttonlist id="RBLDoubleRod" runat="server" Height="26px" Width="53px" Font-Size="XX-Small"
											AutoPostBack="True" RepeatDirection="Horizontal">
											<asp:ListItem Value="Yes">Yes</asp:ListItem>
											<asp:ListItem Value="No" Selected="True">No</asp:ListItem>
										</asp:radiobuttonlist></TD>
								</TR>
								<TR>
									<TD width="12" noWrap align="left"></TD>
									<TD style="WIDTH: 242px" bgColor="gainsboro" width="242" noWrap align="left">
										<asp:label id="Label19" runat="server" Height="20px" Width="233px" Font-Size="Smaller" BackColor="Transparent">Diameter Of Second Rod :</asp:label></TD>
									<TD style="WIDTH: 1px" width="1" noWrap align="left"></TD>
									<TD style="WIDTH: 216px" width="216" noWrap align="left">
										<asp:dropdownlist id="DDLDiameterSec" runat="server" Height="22px" Width="144px" Font-Size="XX-Small"
											AutoPostBack="True"></asp:dropdownlist></TD>
								</TR>
								<TR>
									<TD width="12" noWrap align="left"></TD>
									<TD style="WIDTH: 242px" bgColor="gainsboro" width="242" noWrap align="left">
										<asp:label id="Label20" runat="server" Height="19" Width="233px" Font-Size="Smaller" BackColor="Transparent">Select Second Rod End :</asp:label></TD>
									<TD style="WIDTH: 1px" width="1" noWrap align="left"></TD>
									<TD style="WIDTH: 216px" width="216" noWrap align="left">
										<asp:dropdownlist id="DDLSecRodEnd" runat="server" Height="22px" Width="232px" Font-Size="XX-Small"></asp:dropdownlist></TD>
								</TR>
								<TR>
									<TD width="12" noWrap align="left"></TD>
									<TD style="WIDTH: 242px" bgColor="gainsboro" width="242" noWrap align="left">
										<asp:label id="Label23" runat="server" Height="19" Width="233px" Font-Size="Smaller" BackColor="Transparent">Enter The Second Rod A Dimension  :</asp:label></TD>
									<TD style="WIDTH: 1px" width="1" noWrap align="left"></TD>
									<TD style="WIDTH: 216px" width="216" noWrap align="left">
										<asp:textbox id="TxtSecThreadEx" runat="server" Height="20px" Width="53" Font-Size="XX-Small"
											AutoPostBack="True"></asp:textbox>
										<asp:RegularExpressionValidator id="RegularExpressionValidator4" runat="server" Font-Size="XX-Small" ControlToValidate="TxtSecThreadEx"
											ErrorMessage="Enter Value  (**.** Format)" ValidationExpression="^[\d]?[\d]?\d\.\d\d$">*</asp:RegularExpressionValidator>
										<asp:Label id="Label59" runat="server" Font-Size="XX-Small" ForeColor="RoyalBlue">(Min = 0.00 & Max = 12.00)</asp:Label></TD>
								</TR>
								<TR>
									<TD width="12" noWrap align="left"></TD>
									<TD style="WIDTH: 242px" bgColor="gainsboro" width="242" noWrap align="left">
										<asp:label id="Label24" runat="server" Height="19px" Width="233px" Font-Size="Smaller" BackColor="Transparent">Enter The  Second Rod W Dimension :</asp:label></TD>
									<TD style="WIDTH: 1px" width="1" noWrap align="left"></TD>
									<TD style="WIDTH: 216px" width="216" noWrap align="left">
										<asp:textbox id="TxtSecRodEx" runat="server" Height="20px" Width="53px" Font-Size="XX-Small"
											AutoPostBack="True"></asp:textbox>
										<asp:RegularExpressionValidator id="RegularExpressionValidator5" runat="server" Font-Size="XX-Small" ControlToValidate="TxtSecRodEx"
											ErrorMessage="Enter Value  (**.** Format)" ValidationExpression="^[\d]?[\d]?\d\.\d\d$">*</asp:RegularExpressionValidator>
										<asp:Label id="lblwd" runat="server" Font-Size="XX-Small" ForeColor="RoyalBlue">(Min = 0.00 & Max = 24.00)</asp:Label></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD width="498"><asp:label id="Label49" runat="server" ForeColor="White" BackColor="Gray" Font-Size="Smaller"
							Width="264px" Height="19" Font-Bold="True">Stroke</asp:label><asp:checkbox id="CBStroke" runat="server" Font-Size="Smaller" AutoPostBack="True" Text="Edit"></asp:checkbox><asp:panel id="PanelStroke" runat="server" Width="496px" BorderColor="SlateBlue" BorderStyle="None">
							<TABLE style="WIDTH: 496px; HEIGHT: 91px" id="Table3" border="0" cellSpacing="0" cellPadding="0"
								width="496">
								<TR>
									<TD style="WIDTH: 12px; HEIGHT: 3px" noWrap align="left"></TD>
									<TD style="WIDTH: 193px; HEIGHT: 3px" bgColor="gainsboro" width="193" noWrap align="left">
										<asp:label id="Label10" runat="server" Height="19" Width="225px" Font-Size="Smaller" BackColor="Transparent">Select Stop Tube Configuration :</asp:label></TD>
									<TD style="WIDTH: 1px; HEIGHT: 3px" width="1" noWrap align="left"></TD>
									<TD style="WIDTH: 200px" width="200" noWrap align="left">
										<asp:RadioButtonList id="RBStrokeType" runat="server" Width="184px" Font-Size="XX-Small" RepeatDirection="Horizontal">
											<asp:ListItem Value="Standard" Selected="True">Standard</asp:ListItem>
											<asp:ListItem Value="Double Piston Design">Double Piston Design</asp:ListItem>
										</asp:RadioButtonList></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 12px; HEIGHT: 3px" noWrap align="left"></TD>
									<TD style="WIDTH: 193px; HEIGHT: 3px" bgColor="gainsboro" width="193" noWrap align="left">
										<asp:label id="Label47" runat="server" Height="19" Width="136px" Font-Size="Smaller" BackColor="Transparent">Enter Stop Tube Length :</asp:label>
										<asp:RegularExpressionValidator id="RegularExpressionValidator6" runat="server" Font-Size="XX-Small" ControlToValidate="TxtStopTube"
											ErrorMessage="Enter (**.**)" ValidationExpression="^[\d]?[\d]?\d\.\d\d$">*</asp:RegularExpressionValidator></TD>
									<TD style="WIDTH: 1px; HEIGHT: 3px" width="1" noWrap align="left"></TD>
									<TD style="WIDTH: 200px" width="200" noWrap align="left">
										<asp:TextBox id="TxtStopTube" runat="server" Height="22" Width="53" Font-Size="XX-Small" AutoPostBack="True"></asp:TextBox>
										<asp:RequiredFieldValidator id="RequiredFieldValidator7" runat="server" Font-Size="XX-Small" ControlToValidate="TxtStopTube"
											ErrorMessage="Stop Tube " Enabled="False">*</asp:RequiredFieldValidator>
										<asp:CompareValidator id="CompareValidator1" runat="server" Font-Size="XX-Small" ControlToValidate="TxtStopTube"
											ErrorMessage="Always< Stroke" Enabled="False" ControlToCompare="TxtStroke" Operator="LessThan">*</asp:CompareValidator></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 12px; HEIGHT: 3px" noWrap align="left"></TD>
									<TD style="WIDTH: 193px; HEIGHT: 3px" bgColor="gainsboro" width="193" noWrap align="left">
										<asp:Label id="Label7" runat="server" Font-Size="Smaller">Enter Effective Stroke :</asp:Label>
										<asp:RegularExpressionValidator id="RegularExpressionValidator8" runat="server" Font-Size="XX-Small" ControlToValidate="TxtEffectiveStrok"
											ErrorMessage="Enter (**.**)" ValidationExpression="^[\d]?[\d]?\d\.\d\d$">*</asp:RegularExpressionValidator></TD>
									<TD style="WIDTH: 1px; HEIGHT: 3px" width="1" noWrap align="left"></TD>
									<TD style="WIDTH: 200px" width="200" noWrap align="left">
										<asp:TextBox id="TxtEffectiveStrok" runat="server" Height="20px" Width="53px" Font-Size="XX-Small"
											AutoPostBack="True"></asp:TextBox>
										<asp:RequiredFieldValidator id="RequiredFieldValidator4" runat="server" Font-Size="XX-Small" ControlToValidate="TxtEffectiveStrok"
											ErrorMessage="Effective Stroke" Enabled="False">*</asp:RequiredFieldValidator></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD width="498"><asp:label id="Label43" runat="server" ForeColor="White" BackColor="Gray" Font-Size="Smaller"
							Width="265" Height="19" Font-Bold="True">Seal</asp:label><asp:checkbox id="CBSeal" runat="server" Font-Size="Smaller" AutoPostBack="True" Text="Edit"></asp:checkbox><asp:panel id="PanelSeal" runat="server" Width="488px" BorderColor="SlateBlue" BorderStyle="None">
							<TABLE style="WIDTH: 496px; HEIGHT: 19px" id="Table5" border="0" cellSpacing="0" cellPadding="0"
								width="496">
								<TR>
									<TD style="HEIGHT: 17px" width="12" noWrap align="left"></TD>
									<TD style="HEIGHT: 17px" bgColor="gainsboro" width="200" noWrap align="left">
										<asp:label id="Label30" runat="server" Height="19" Width="225px" Font-Size="Smaller" BackColor="Transparent">Select Rod Seal Configuration :</asp:label></TD>
									<TD style="HEIGHT: 17px" width="2" noWrap align="left"></TD>
									<TD style="HEIGHT: 17px" width="200" noWrap align="left">
										<asp:DropDownList id="DDLRodSeal" runat="server" Width="199px" Font-Size="XX-Small"></asp:DropDownList></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD width="498"><asp:label id="Label44" runat="server" ForeColor="White" BackColor="Gray" Font-Size="Smaller"
							Width="265" Height="19px" Font-Bold="True">Port</asp:label><asp:checkbox id="CBPort" runat="server" Font-Size="Smaller" Width="32px" AutoPostBack="True"
							Text="Edit"></asp:checkbox><asp:panel id="PanelPort" runat="server" Width="488px" BorderColor="SlateBlue" BorderStyle="None">
							<TABLE style="WIDTH: 496px; HEIGHT: 44px" id="Table6" border="0" cellSpacing="0" cellPadding="0"
								width="496">
								<TR>
									<TD width="12" noWrap align="left"></TD>
									<TD style="WIDTH: 211px" bgColor="gainsboro" width="211" noWrap align="left">
										<asp:label id="Label33" runat="server" Width="241px" Font-Size="Smaller" BackColor="Transparent">Select Ports Size :</asp:label></TD>
									<TD style="WIDTH: 1px" width="1" noWrap align="left"></TD>
									<TD style="WIDTH: 194px" width="194" noWrap align="left">
										<asp:dropdownlist id="DDLPortSize" runat="server" Height="22px" Width="216px" Font-Size="XX-Small"
											Enabled="False"></asp:dropdownlist></TD>
								</TR>
								<TR>
									<TD width="12" noWrap align="left"></TD>
									<TD style="WIDTH: 211px" bgColor="gainsboro" width="211" noWrap align="left">
										<asp:label id="Label34" runat="server" Height="19px" Width="240px" Font-Size="Smaller" BackColor="Transparent">Select Air Bleeds :</asp:label></TD>
									<TD style="WIDTH: 1px" width="1" noWrap align="left"></TD>
									<TD style="WIDTH: 194px" width="194" noWrap align="left">
										<asp:dropdownlist id="DDLAirbleeds" runat="server" Height="22px" Width="216px" Font-Size="XX-Small"></asp:dropdownlist></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD width="498"><asp:label id="Label45" runat="server" ForeColor="White" BackColor="Gray" Font-Size="Smaller"
							Width="264px" Height="19px" Font-Bold="True">Mount</asp:label><asp:checkbox id="CBMount" runat="server" Font-Size="Smaller" AutoPostBack="True" Text="Edit"
							Enabled="False"></asp:checkbox><asp:panel id="PanelMount" runat="server" Width="488px" BorderColor="SlateBlue" BorderStyle="None">
							<asp:Panel id="PnlXI" runat="server" Width="496px">
								<TABLE style="WIDTH: 496px; HEIGHT: 22px" id="Table14" border="0" cellSpacing="0" cellPadding="0"
									width="496">
									<TR>
										<TD width="12" noWrap align="left"></TD>
										<TD style="WIDTH: 248px" bgColor="gainsboro" width="248" noWrap align="left">
											<asp:label id="lblxi" runat="server" Width="232px" Font-Size="Smaller" BackColor="Transparent">Enter Intermediate Trunnion Position :</asp:label></TD>
										<TD style="WIDTH: 2px" width="2" noWrap align="left"></TD>
										<TD noWrap align="left">
											<asp:textbox id="TXTIntere" runat="server" Height="20px" Width="52px" Font-Size="XX-Small" AutoPostBack="True"
												EnableViewState="False"></asp:textbox>
											<asp:RegularExpressionValidator id="RegularExpressionValidator7" runat="server" Width="8px" Font-Size="XX-Small"
												ControlToValidate="TXTIntere" ErrorMessage="Enter Value  (**.** Format)" ValidationExpression="^[\d]?[\d]?\d\.\d\d$">*</asp:RegularExpressionValidator>
											<asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" Font-Size="XX-Small" ControlToValidate="TXTIntere"
												ErrorMessage="Enter XI">*</asp:RequiredFieldValidator>
											<asp:Label id="lblx" runat="server" Font-Size="XX-Small" ForeColor="RoyalBlue">(Min = 0.00 & Max = 12.00)</asp:Label></TD>
									</TR>
								</TABLE>
							</asp:Panel>
							<asp:Panel id="PnlBBH" runat="server" Width="496px">
								<TABLE style="WIDTH: 496px; HEIGHT: 41px" id="Table16" border="0" cellSpacing="0" cellPadding="0"
									width="496">
									<TR>
										<TD width="12" noWrap align="left"></TD>
										<TD style="WIDTH: 247px" bgColor="#dcdcdc" width="247" noWrap align="left">
											<asp:label id="lblbb" runat="server" Width="240px" Font-Size="Smaller" BackColor="Transparent">Special Tie Rod Extention (BB):</asp:label></TD>
										<TD style="WIDTH: 2px" width="2" noWrap align="left"></TD>
										<TD noWrap align="left"></TD>
									</TR>
									<TR>
										<TD width="12" noWrap align="left"></TD>
										<TD style="WIDTH: 247px" bgColor="#dcdcdc" width="247" noWrap align="left">
											<asp:label id="lblbbh" runat="server" Width="240px" Font-Size="Smaller" BackColor="Transparent">Head End :</asp:label></TD>
										<TD style="WIDTH: 2px" width="2" noWrap align="left"></TD>
										<TD noWrap align="left">
											<asp:textbox id="TxtBBHead" runat="server" Height="20px" Width="52px" Font-Size="XX-Small" AutoPostBack="True"
												EnableViewState="False"></asp:textbox>
											<asp:RegularExpressionValidator id="RegularExpressionValidator11" runat="server" Font-Size="Smaller" ControlToValidate="TxtBBHead"
												ErrorMessage="Enter Value  (**.** Format)" ValidationExpression="^[\d]?[\d]?\d\.\d\d$">*</asp:RegularExpressionValidator>
											<asp:Label id="lblbbcc" runat="server" Font-Size="XX-Small" ForeColor="RoyalBlue">(Min = 0.00 & Max = 12.00)</asp:Label></TD>
									</TR>
								</TABLE>
							</asp:Panel>
							<asp:Panel id="PnlBBC" runat="server" Width="496px">
								<TABLE style="WIDTH: 496px; HEIGHT: 41px" id="Table17" border="0" cellSpacing="0" cellPadding="0"
									width="496">
									<TR>
										<TD width="12" noWrap align="left"></TD>
										<TD style="WIDTH: 247px" bgColor="#dcdcdc" width="247" noWrap align="left">
											<asp:label id="Label65" runat="server" Width="240px" Font-Size="Smaller" BackColor="Transparent">Special Tie Rod Extention (BB):</asp:label></TD>
										<TD style="WIDTH: 1px" width="1" noWrap align="left"></TD>
										<TD noWrap align="left"></TD>
									</TR>
									<TR>
										<TD width="12" noWrap align="left"></TD>
										<TD style="WIDTH: 247px" bgColor="#dcdcdc" width="247" noWrap align="left">
											<asp:label id="lblbbc" runat="server" Width="240px" Font-Size="Smaller" BackColor="Transparent">Cap End :</asp:label></TD>
										<TD style="WIDTH: 1px" width="1" noWrap align="left"></TD>
										<TD noWrap align="left">
											<asp:textbox id="TxtBBCap" runat="server" Height="20px" Width="52px" Font-Size="XX-Small" AutoPostBack="True"
												EnableViewState="False"></asp:textbox>
											<asp:RegularExpressionValidator id="RegularExpressionValidator10" runat="server" Font-Size="Smaller" ControlToValidate="TxtBBCap"
												ErrorMessage="Enter Value  (**.** Format)" ValidationExpression="^[\d]?[\d]?\d\.\d\d$">*</asp:RegularExpressionValidator>
											<asp:Label id="lblbbhh" runat="server" Font-Size="XX-Small" ForeColor="RoyalBlue">(Min = 0.00 & Max = 12.00)</asp:Label></TD>
									</TR>
								</TABLE>
							</asp:Panel>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD width="498"><asp:label id="Label6" runat="server" ForeColor="White" BackColor="Gray" Font-Size="Smaller"
							Width="264px" Height="19px" Font-Bold="True">Piston</asp:label><asp:checkbox id="CBPiston" runat="server" Font-Size="Smaller" AutoPostBack="True" Text="Edit"></asp:checkbox><asp:panel id="PanelPiston" runat="server" Width="496px" Height="39px" BorderColor="SlateBlue"
							BorderStyle="None">
							<TABLE style="WIDTH: 496px; HEIGHT: 64px" id="Table8" border="0" cellSpacing="0" cellPadding="0"
								width="496">
								<TR>
									<TD width="12" noWrap align="left"></TD>
									<TD bgColor="#dcdcdc" width="200" noWrap align="left">
										<asp:label id="Label54" runat="server" Width="224px" Font-Size="Smaller" BackColor="Transparent">Select Piston Seal Configuration :</asp:label></TD>
									<TD width="2" noWrap align="left"></TD>
									<TD width="200" noWrap align="left">
										<asp:DropDownList id="DDLPistonSealConfig" runat="server" Width="200px" Font-Size="XX-Small">
											<asp:ListItem Value="Standard">Standard</asp:ListItem>
										</asp:DropDownList></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 29px" width="12" noWrap align="left"></TD>
									<TD style="HEIGHT: 29px" bgColor="#dcdcdc" width="200" noWrap align="left">
										<asp:label id="Label55" runat="server" Width="225px" Font-Size="Smaller" BackColor="Transparent">Use Magnets on  the Pistion :</asp:label></TD>
									<TD style="HEIGHT: 29px" width="2" noWrap align="left"></TD>
									<TD style="HEIGHT: 29px" width="200" noWrap align="left">
										<asp:radiobuttonlist id="RBLMagnet" runat="server" Height="26px" Width="56px" Font-Size="XX-Small" RepeatDirection="Horizontal">
											<asp:ListItem Value="P2">Yes</asp:ListItem>
											<asp:ListItem Value="No" Selected="True">No</asp:ListItem>
										</asp:radiobuttonlist></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD width="498"><asp:label id="Label50" runat="server" ForeColor="White" BackColor="Gray" Font-Size="Smaller"
							Width="264px" Height="19px" Font-Bold="True"> Gland</asp:label><asp:checkbox id="CBGland" runat="server" Font-Size="Smaller" AutoPostBack="True" Text="Edit"></asp:checkbox><asp:panel id="PanelGland" runat="server" Width="496px" Height="46px" BorderColor="SlateBlue"
							BorderStyle="None">
							<TABLE style="WIDTH: 488px; HEIGHT: 83px" id="Table9" border="0" cellSpacing="0" cellPadding="0"
								width="488">
								<TR>
									<TD width="12" noWrap align="left"></TD>
									<TD bgColor="#dcdcdc" width="200" noWrap align="left">
										<asp:label id="Label51" runat="server" Width="224px" Font-Size="Smaller" BackColor="Transparent">Select  Metal Scrapper :</asp:label></TD>
									<TD width="2" noWrap align="left"></TD>
									<TD width="200" noWrap align="left">
										<asp:DropDownList id="DDLMetalScrappeer" runat="server" Width="169px" Font-Size="XX-Small"></asp:DropDownList></TD>
								</TR>
								<TR>
									<TD width="12" noWrap align="left"></TD>
									<TD bgColor="#dcdcdc" width="200" noWrap align="left">
										<asp:label id="Label52" runat="server" Width="224px" Font-Size="Smaller" BackColor="Transparent">Use Gland Drain :</asp:label></TD>
									<TD width="2" noWrap align="left"></TD>
									<TD width="200" noWrap align="left">
										<asp:radiobuttonlist id="RBDrain" runat="server" Width="32px" Font-Size="XX-Small" RepeatDirection="Horizontal">
											<asp:ListItem Value="G1">Yes</asp:ListItem>
											<asp:ListItem Value="No" Selected="True">No</asp:ListItem>
										</asp:radiobuttonlist></TD>
								</TR>
								<TR>
									<TD width="12" noWrap align="left"></TD>
									<TD bgColor="#dcdcdc" width="200" noWrap align="left">
										<asp:label id="Label25" runat="server" Height="3px" Width="224px" Font-Size="Smaller" BackColor="Transparent">Select Bushing Material :</asp:label></TD>
									<TD width="2" noWrap align="left"></TD>
									<TD width="200" noWrap align="left">
										<asp:DropDownList id="DDLBushing" runat="server" Width="169" Font-Size="XX-Small"></asp:DropDownList></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD width="498"><asp:label id="Label35" runat="server" ForeColor="White" BackColor="Gray" Font-Size="Smaller"
							Width="264px" Height="19px" Font-Bold="True">Other Options :</asp:label><asp:checkbox id="CbOther" runat="server" Font-Size="Smaller" AutoPostBack="True" Text="Yes"></asp:checkbox><asp:panel id="PanelOther" runat="server" Width="496px" BorderColor="SlateBlue" BorderStyle="None">
							<TABLE style="WIDTH: 488px; HEIGHT: 83px" id="Table10" border="0" cellSpacing="0" cellPadding="0"
								width="488">
								<TR>
									<TD width="12" noWrap align="left"></TD>
									<TD language="240" bgColor="#dcdcdc" width="200" noWrap align="left">
										<asp:label id="Label36" runat="server" Width="224px" Font-Size="Smaller" BackColor="Transparent">Select Coating :</asp:label></TD>
									<TD style="WIDTH: 1px" width="1" noWrap align="left"></TD>
									<TD width="200" noWrap align="left">
										<asp:dropdownlist id="DDLCoating" runat="server" Height="36px" Width="160px" Font-Size="XX-Small"></asp:dropdownlist></TD>
								</TR>
								<TR>
									<TD width="12" noWrap align="left"></TD>
									<TD language="240" bgColor="#dcdcdc" width="200" noWrap align="left">
										<asp:label id="Label48" runat="server" Height="19" Width="224px" Font-Size="Smaller" BackColor="Transparent">Select Barrel :</asp:label></TD>
									<TD style="WIDTH: 1px" width="1" noWrap align="left"></TD>
									<TD width="200" noWrap align="left">
										<asp:DropDownList id="DDLBarrel" runat="server" Width="160px" Font-Size="XX-Small">
											<asp:ListItem Value="Standard">Standard</asp:ListItem>
										</asp:DropDownList></TD>
								</TR>
								<TR>
									<TD width="12" noWrap align="left"></TD>
									<TD language="240" bgColor="#dcdcdc" width="200" noWrap align="left">
										<asp:label id="Label37" runat="server" Width="224px" Font-Size="Smaller" BackColor="Transparent">Use Stainless Steel Tie Rod :</asp:label></TD>
									<TD style="WIDTH: 1px" width="1" noWrap align="left"></TD>
									<TD width="200" noWrap align="left">
										<asp:radiobuttonlist id="RBLTieRod" runat="server" Width="72px" Font-Size="XX-Small" RepeatDirection="Horizontal">
											<asp:ListItem Value="M2">Yes</asp:ListItem>
											<asp:ListItem Value="No" Selected="True">No</asp:ListItem>
										</asp:radiobuttonlist></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD width="498"><asp:label id="Label8" runat="server" ForeColor="White" BackColor="Gray" Font-Size="Smaller"
							Width="264px" Height="19px" Font-Bold="True">Tandem/Duplex/Back To Back :</asp:label><asp:checkbox id="CBTandem" runat="server" Font-Size="Smaller" Width="30px" Height="20px" AutoPostBack="True"
							Text="Yes"></asp:checkbox><asp:panel id="PNTandem" runat="server" Width="496px" BorderColor="SlateBlue" BorderStyle="None">
							<TABLE style="WIDTH: 488px; HEIGHT: 41px" id="Table4" border="0" cellSpacing="0" cellPadding="0"
								width="488">
								<TR>
									<TD width="12" noWrap align="left"></TD>
									<TD language="240" bgColor="#dcdcdc" width="200" noWrap align="left">
										<asp:label id="Label12" runat="server" Width="240px" Font-Size="Smaller" BackColor="Transparent">Select Configuration :</asp:label></TD>
									<TD style="WIDTH: 1px" width="1" noWrap align="left"></TD>
									<TD width="200" noWrap align="left">
										<asp:dropdownlist id="DDLTandomConfig" runat="server" Height="36px" Width="216px" Font-Size="XX-Small"
											AutoPostBack="True">
											<asp:ListItem Value="TC">Tandem Cylinder.(Rods Attached)</asp:ListItem>
											<asp:ListItem Value="DC">Duplex lnline.(Rods Not Attached)</asp:ListItem>
											<asp:ListItem Value="BC">Back To Back Cylinder</asp:ListItem>
										</asp:dropdownlist></TD>
								</TR>
								<TR>
									<TD width="12" noWrap align="left"></TD>
									<TD language="240" bgColor="#dcdcdc" width="200" noWrap align="left">
										<asp:label id="Label11" runat="server" Height="19" Width="240px" Font-Size="Smaller" BackColor="Transparent">Enter Second Stroke :</asp:label></TD>
									<TD style="WIDTH: 1px" width="1" noWrap align="left"></TD>
									<TD width="200" noWrap align="left">
										<asp:textbox id="TxtSecondStroke" runat="server" Height="20px" Width="52px" Font-Size="XX-Small"
											AutoPostBack="True" EnableViewState="False"></asp:textbox>
										<asp:RequiredFieldValidator id="RFVtandem" runat="server" Font-Size="XX-Small" ControlToValidate="TxtSecondStroke"
											ErrorMessage="Enter Stroke" Enabled="False">*</asp:RequiredFieldValidator>
										<asp:Label id="lblSecstroke" runat="server" Font-Size="XX-Small" ForeColor="RoyalBlue">( Min = 0.00 & Max  <= Stroke)</asp:Label></TD>
								</TR>
							</TABLE>
						</asp:panel><asp:label style="Z-INDEX: 0" id="Label61" runat="server" ForeColor="White" BackColor="Gray"
							Font-Size="Smaller" Width="264px" Height="19px" Font-Bold="True">Transducer Options:</asp:label><asp:checkbox style="Z-INDEX: 0" id="CBTransducer" runat="server" Font-Size="Smaller" Width="30px"
							Height="20px" AutoPostBack="True" Text="Yes"></asp:checkbox><asp:panel style="Z-INDEX: 0" id="PNTransducer" runat="server" Width="496px" BorderColor="SlateBlue"
							BorderStyle="None">
							<TABLE style="WIDTH: 488px; HEIGHT: 41px" id="Table7" border="0" cellSpacing="0" cellPadding="0"
								width="488">
								<TR>
									<TD width="12" noWrap align="left"></TD>
									<TD language="240" bgColor="#dcdcdc" width="200" noWrap align="left">
										<asp:label id="Label66" runat="server" Width="240px" Font-Size="Smaller" BackColor="Transparent">Select Configuration :</asp:label></TD>
									<TD style="WIDTH: 1px" width="1" noWrap align="left"></TD>
									<TD width="200" noWrap align="left">
										<asp:dropdownlist id="DDLTransducer" runat="server" Height="36px" Width="216px" Font-Size="XX-Small"
											AutoPostBack="True"></asp:dropdownlist></TD>
								</TR>
								<TR>
									<TD width="12" noWrap align="left"></TD>
									<TD language="240" bgColor="#dcdcdc" width="200" noWrap align="left">
										<asp:label id="lblressist" runat="server" Height="19" Width="240px" Font-Size="Smaller" BackColor="Transparent">Enter Resistance :</asp:label></TD>
									<TD style="WIDTH: 1px" width="1" noWrap align="left"></TD>
									<TD width="200" noWrap align="left">
										<asp:textbox id="TxtTransducer" runat="server" Height="20px" Width="52px" Font-Size="XX-Small"
											AutoPostBack="True" EnableViewState="False"></asp:textbox>
										<asp:label style="Z-INDEX: 0" id="lblohm" runat="server" Height="19" Width="8px" BackColor="Transparent">&Omega;</asp:label>
										<asp:RequiredFieldValidator style="Z-INDEX: 0" id="RequiredFieldValidator5" runat="server" Font-Size="XX-Small"
											ControlToValidate="TxtSecondStroke" ErrorMessage="Enter Stroke" Enabled="False">*</asp:RequiredFieldValidator>
										<asp:Label style="Z-INDEX: 0" id="lblminmaxohm" runat="server" Font-Size="XX-Small" ForeColor="RoyalBlue">( Min = 0.00 & Max  <100)</asp:Label></TD>
								</TR>
								<TR>
									<TD width="12" noWrap align="left"></TD>
									<TD bgColor="#dcdcdc" width="200" noWrap align="left">
										<asp:label style="Z-INDEX: 0" id="Label62" runat="server" Height="19" Width="240px" Font-Size="Smaller"
											BackColor="Transparent">Remote Positioner :</asp:label></TD>
									<TD style="WIDTH: 1px" width="1" noWrap align="left"></TD>
									<TD width="200" noWrap align="left">
										<asp:radiobuttonlist style="Z-INDEX: 0" id="RBLRemote" runat="server" Width="78px" Font-Size="9pt" AutoPostBack="True"
											RepeatDirection="Horizontal" RepeatLayout="Flow">
											<asp:ListItem Value="P">Yes</asp:ListItem>
											<asp:ListItem Value="No" Selected="True">No</asp:ListItem>
										</asp:radiobuttonlist></TD>
								</TR>
								<TR>
									<TD width="12" noWrap align="left"></TD>
									<TD bgColor="#dcdcdc" width="200" noWrap align="left">
										<asp:label style="Z-INDEX: 0" id="Label67" runat="server" Height="19" Width="240px" Font-Size="Smaller"
											BackColor="Transparent">Fittings :</asp:label></TD>
									<TD style="WIDTH: 1px" width="1" noWrap align="left"></TD>
									<TD width="200" noWrap align="left">
										<asp:radiobuttonlist style="Z-INDEX: 0" id="RBLFittings" runat="server" Height="38px" Font-Size="9pt"
											RepeatLayout="Flow">
											<asp:ListItem Value="B" Selected="True">Brass fittings &amp; Copper tubing</asp:ListItem>
											<asp:ListItem Value="S">Stainless Steel fittings &amp; tubing</asp:ListItem>
										</asp:radiobuttonlist></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD width="498"></TD>
				</TR>
				<TR>
					<TD width="498" align="center"><asp:validationsummary id="ValidationSummary1" runat="server" Font-Size="XX-Small" Height="25px"></asp:validationsummary><asp:button id="BTNGenPartNo" runat="server" ForeColor="White" BackColor="Gray" Font-Size="XX-Small"
							Width="126px" BorderStyle="Ridge" Text="Generate Part Number"></asp:button></TD>
				</TR>
				<TR>
					<TD width="498" align="center"><asp:label style="TEXT-ALIGN: center" id="LblInfo" runat="server" ForeColor="Red" Font-Size="Smaller"
							Width="431px"></asp:label></TD>
				</TR>
			</TABLE>
			<TABLE style="Z-INDEX: 101; BORDER-BOTTOM: lightgrey thin solid; POSITION: absolute; BORDER-LEFT: darkgray thin solid; BORDER-TOP: lightgrey thin solid; TOP: 0px; BORDER-RIGHT: darkgray thin solid; LEFT: 520px"
				id="Table15" border="0" cellSpacing="0" borderColor="gainsboro" cellPadding="0" width="300">
				<TR>
					<TD align="center"><asp:label id="Label26" runat="server" ForeColor="Red" Font-Underline="True" Font-Size="Smaller"
							Width="88px" Height="22px">Cylinder Details</asp:label></TD>
				</TR>
				<TR>
					<TD><asp:label id="Label28" runat="server" ForeColor="White" BackColor="Gray" Font-Size="Smaller"
							Width="460px" Height="18px">The Part Number for your specifications is  given below :</asp:label></TD>
				</TR>
				<TR>
					<TD><asp:panel id="Panel2" runat="server" BackColor="#E0E0E0" Width="460px" BorderColor="SlateBlue"
							BorderStyle="None">
							<TABLE id="Table13" border="0" cellSpacing="0" cellPadding="0" width="450">
								<TR>
									<TD style="WIDTH: 53px; HEIGHT: 9px" colSpan="2" noWrap align="left">
										<asp:Label id="Label32" runat="server" Height="15px" Font-Size="XX-Small"></asp:Label></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 53px" noWrap align="left">
										<asp:label id="Label29" runat="server" Font-Size="Smaller" Font-Bold="True">Part No:</asp:label></TD>
									<TD width="250" noWrap align="left">
										<asp:label id="LBLPNo" runat="server" Width="380px" Font-Size="Smaller" Font-Italic="True"></asp:label></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 53px" noWrap align="left">
										<asp:Label id="Label39" runat="server" Height="15px" Font-Size="XX-Small"></asp:Label></TD>
									<TD width="250" noWrap align="left"></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD align="left"><asp:label id="Label31" runat="server" ForeColor="White" BackColor="Gray" Font-Size="Smaller"
							Width="460px" Height="18px">Detailed Description of your Cylinder :</asp:label></TD>
				</TR>
				<TR>
					<TD align="left"><asp:panel id="Panel3" runat="server" Width="460px" BorderColor="SlateBlue" BorderStyle="None">
							<asp:Label id="LBLSeries" runat="server" Width="456px" Font-Size="Smaller"></asp:Label>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD align="left"><asp:label id="Label46" runat="server" ForeColor="White" BackColor="Gray" Font-Size="Smaller"
							Width="460px" Height="18px">Do you have any Special Request :</asp:label><asp:radiobuttonlist id="RBSpecialReq" runat="server" Font-Size="XX-Small" Height="15px" AutoPostBack="True"
							RepeatDirection="Horizontal">
							<asp:ListItem Value="Yes">Yes</asp:ListItem>
							<asp:ListItem Value="No" Selected="True">No</asp:ListItem>
						</asp:radiobuttonlist><asp:panel id="PnlSpReg" runat="server" Width="461px" Height="32px" BorderColor="SlateBlue"
							BorderStyle="None">
							<asp:TextBox id="TxtSpecialReq" runat="server" Height="72px" Width="456px" TextMode="MultiLine"></asp:TextBox>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD align="left"><asp:label id="Label53" runat="server" ForeColor="White" BackColor="Gray" Font-Size="Smaller"
							Width="460px" Height="18px">Enter The Quantity :</asp:label></TD>
				</TR>
				<TR>
					<TD align="left"><asp:textbox id="TxtQty" runat="server" Font-Size="XX-Small" Width="56px" Height="16px"></asp:textbox><asp:label id="lblQty" runat="server" ForeColor="Red" Font-Size="Smaller" Visible="False">Please Enter Quantity !</asp:label></TD>
				</TR>
				<TR>
					<TD align="left"><asp:label style="Z-INDEX: 0" id="Label60" runat="server" Font-Underline="True" Font-Size="Smaller"
							Width="432px">Upload Drawing (pdf format):</asp:label><INPUT style="Z-INDEX: 0; WIDTH: 320px; HEIGHT: 22px; FONT-SIZE: 8pt" id="UploadDwg" size="34"
							type="file" name="File1" runat="server">
						<asp:linkbutton style="Z-INDEX: 0" id="LbPreview" runat="server" ForeColor="Black" BackColor="Silver"
							Font-Size="9pt" Width="48px" Height="20px" BorderColor="WhiteSmoke" BorderStyle="Outset" BorderWidth="2px">Preview</asp:linkbutton></TD>
				</TR>
				<TR>
					<TD align="left"><asp:checkbox style="Z-INDEX: 0" id="cbupload" runat="server" Font-Size="8pt" Text="Upload this File:"
							Visible="False" Checked="True" TextAlign="Left"></asp:checkbox><asp:label style="Z-INDEX: 0" id="lblfilename" runat="server" ForeColor="DimGray" BackColor="Transparent"
							Font-Size="8pt" Height="6px"></asp:label></TD>
				</TR>
				<TR>
					<TD align="left"><asp:label id="Label56" runat="server" ForeColor="White" BackColor="Gray" Font-Size="Smaller"
							Width="460px" Height="16px"> Please Click Generate for qoute.</asp:label></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 20px" align="center"><asp:button id="BtnGeneratePrice" runat="server" ForeColor="White" BackColor="Gray" Font-Size="XX-Small"
							Width="126px" BorderStyle="Ridge" Text="Generate Quote"></asp:button></TD>
				</TR>
				<TR>
					<TD align="center"><asp:label id="lblgenerateprice" runat="server" ForeColor="Blue" BackColor="Transparent" Font-Size="Smaller"
							Width="460px" Height="18px"></asp:label></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 2px" align="center"><asp:label id="lbltable" runat="server" Visible="False"></asp:label><asp:label id="lbl" runat="server" Visible="False"></asp:label><asp:label id="lblbor" runat="server" Visible="False"></asp:label><asp:label id="lblmgrp" runat="server" Visible="False"></asp:label><asp:label id="lbltotal" runat="server" Visible="False"></asp:label><asp:label id="lblqno" runat="server" Visible="False"></asp:label><asp:label id="lblstroke" runat="server" Visible="False"></asp:label><asp:label id="lblstatus" runat="server" Visible="False"></asp:label><asp:label id="lblD" runat="server" Visible="False"></asp:label></TD>
				</TR>
				<TR>
					<TD align="left"><asp:panel id="PPriority" runat="server" Width="461px" BorderColor="SlateBlue" BorderStyle="Dotted"
							Visible="False" BorderWidth="1px">
							<TABLE style="WIDTH: 456px; HEIGHT: 57px" id="Table12" border="0" cellSpacing="0" cellPadding="0"
								width="456">
								<TR>
									<TD>
										<asp:Label id="Label57" runat="server" Height="18px" Width="456px" Font-Size="Smaller" BackColor="Gray"
											ForeColor="Red">This Quote require assitance from the factory:</asp:Label></TD>
								</TR>
								<TR>
									<TD>
										<asp:Label id="Label58" runat="server" Width="240px" Font-Size="X-Small" ForeColor="#FF8000">Please Select the priority of this RFQ :</asp:Label>
										<asp:DropDownList id="DDLPriority" runat="server" Height="18px" Width="80px" Font-Size="XX-Small">
											<asp:ListItem Value="Normal" Selected="True">Normal</asp:ListItem>
											<asp:ListItem Value="Urgent">Urgent</asp:ListItem>
										</asp:DropDownList></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:Button id="BtnSendMail" runat="server" Width="126px" Font-Size="XX-Small" BackColor="Gray"
											ForeColor="White" BorderStyle="Groove" Text="Send"></asp:Button></TD>
								</TR>
							</TABLE>
							<asp:label style="Z-INDEX: 0" id="lblfile" runat="server" Visible="False"></asp:label>
						</asp:panel></TD>
				</TR>
			</TABLE>
			<jlc:staticpostbackposition id="StaticPostBackPosition1" runat="server"></jlc:staticpostbackposition><asp:label style="Z-INDEX: 103; POSITION: absolute; TOP: 528px; LEFT: 528px" id="dwg" runat="server"></asp:label></form>
	</body>
</HTML>
