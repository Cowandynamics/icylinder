<%@ Page language="c#" Codebehind="Icylinder_PageAcc.aspx.cs" AutoEventWireup="True" Inherits="iCylinderV1.Icylinder_PageAcc" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Icylinder_PageAcc</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 1000px; HEIGHT: 487px" cellSpacing="0" cellPadding="0"
				width="1000" bgColor="lightgrey" border="0">
				<TR>
					<TD width="50" height="20"></TD>
					<TD align="center" height="20"><asp:label id="Label1" runat="server" Height="19" Font-Bold="True" Width="268px" BackColor="Transparent"
							Font-Underline="True" Font-Size="Smaller"> Rod End Accessories</asp:label></TD>
					<TD width="50" height="20"></TD>
				</TR>
				<TR>
					<TD width="50"></TD>
					<TD align="center" bgColor="gainsboro">
						<TABLE id="Table3" style="WIDTH: 238px; HEIGHT: 333px" borderColor="gray" cellSpacing="0"
							cellPadding="0" width="238" border="1">
							<TR>
								<TD align="center">
									<TABLE id="Table24" style="WIDTH: 624px; HEIGHT: 65px" borderColor="whitesmoke" cellSpacing="0"
										cellPadding="0" width="624" border="1">
										<TR>
											<TD><asp:panel id="PanelRodeye" runat="server" Enabled="False">
													<TABLE id="Table16" cellSpacing="0" cellPadding="0" width="300" border="0">
														<TR>
															<TD style="WIDTH: 251px">
																<TABLE id="Table17" style="WIDTH: 192px; HEIGHT: 91px" cellSpacing="0" cellPadding="0"
																	width="192" border="0">
																	<TR>
																		<TD style="HEIGHT: 8px">
																			<asp:CheckBox id="CBRodeye" runat="server" Font-Size="Smaller" Text="Rod Eye" AutoPostBack="True" oncheckedchanged="CBRodeye_CheckedChanged"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:DropDownList id="DDLRodeye" runat="server" Font-Size="XX-Small" Width="190px" AutoPostBack="True"></asp:DropDownList></TD>
																	</TR>
																	<TR>
																		<TD></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD align="right" width="110">
																<asp:Image id="ImgRodeye" runat="server" ImageUrl="accessories\rodeye.jpg"></asp:Image></TD>
														</TR>
													</TABLE>
												</asp:panel></TD>
											<TD><asp:panel id="PanelRodClevis" runat="server" Enabled="False">
													<TABLE id="Table6" cellSpacing="0" cellPadding="0" width="300" border="0">
														<TR>
															<TD style="WIDTH: 251px">
																<TABLE id="Table12" style="WIDTH: 192px; HEIGHT: 91px" cellSpacing="0" cellPadding="0"
																	width="192" border="0">
																	<TR>
																		<TD style="HEIGHT: 11px">
																			<asp:CheckBox id="CBRodclevis" runat="server" Font-Size="Smaller" Text="Rod Clevis" AutoPostBack="True" oncheckedchanged="CBRodclevis_CheckedChanged"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:DropDownList id="DDLRodclevis" runat="server" Font-Size="XX-Small" Width="190px" AutoPostBack="True"></asp:DropDownList></TD>
																	</TR>
																	<TR>
																		<TD></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD align="right" width="110">
																<asp:Image id="ImgRodclevis" runat="server" ImageUrl="accessories\rodclevis.jpg"></asp:Image></TD>
														</TR>
													</TABLE>
												</asp:panel></TD>
											<TD><asp:panel id="PanelPivotPin" runat="server" Enabled="False">
													<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="300" border="0">
														<TR>
															<TD style="WIDTH: 251px">
																<TABLE id="Table2" style="WIDTH: 192px; HEIGHT: 91px" cellSpacing="0" cellPadding="0" width="192"
																	border="0">
																	<TR>
																		<TD style="HEIGHT: 9px">
																			<asp:CheckBox id="CBPivotpin" runat="server" Font-Size="Smaller" Text="Pivot Pin" AutoPostBack="True" oncheckedchanged="CBPivotpin_CheckedChanged"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:DropDownList id="DDLPivotpin" runat="server" Font-Size="XX-Small" Width="190px" AutoPostBack="True"></asp:DropDownList></TD>
																	</TR>
																	<TR>
																		<TD></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD align="right" width="110">
																<asp:Image id="ImgPivotpin" runat="server" ImageUrl="accessories\pivotpin.jpg"></asp:Image></TD>
														</TR>
													</TABLE>
												</asp:panel></TD>
										</TR>
										<TR>
											<TD><asp:panel id="PanelEyeBracket" runat="server" Enabled="False">
													<TABLE id="Table13" cellSpacing="0" cellPadding="0" width="300" border="0">
														<TR>
															<TD style="WIDTH: 251px">
																<TABLE id="Table14" style="WIDTH: 192px; HEIGHT: 91px" cellSpacing="0" cellPadding="0"
																	width="192" border="0">
																	<TR>
																		<TD style="HEIGHT: 1px">
																			<asp:CheckBox id="CBEyeb" runat="server" Font-Size="Smaller" Text="Eye Bracket" AutoPostBack="True" oncheckedchanged="CBEyeb_CheckedChanged"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:DropDownList id="DDLEyeb" runat="server" Font-Size="XX-Small" Width="190px" AutoPostBack="True"></asp:DropDownList></TD>
																	</TR>
																	<TR>
																		<TD></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD align="right" width="110">
																<asp:Image id="ImgEyeb" runat="server" ImageUrl="accessories\eyebracket.jpg"></asp:Image></TD>
														</TR>
													</TABLE>
												</asp:panel></TD>
											<TD><asp:panel id="PanelClevisBracket" runat="server" Enabled="False">
													<TABLE id="Table15" cellSpacing="0" cellPadding="0" width="300" border="0">
														<TR>
															<TD style="WIDTH: 251px">
																<TABLE id="Table18" style="WIDTH: 192px; HEIGHT: 91px" cellSpacing="0" cellPadding="0"
																	width="192" border="0">
																	<TR>
																		<TD style="HEIGHT: 5px">
																			<asp:CheckBox id="CBClevisbracket" runat="server" Font-Size="Smaller" Text="Clevis Bracket" AutoPostBack="True" oncheckedchanged="CBClevisbracket_CheckedChanged"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:DropDownList id="DDLClevisbracket" runat="server" Font-Size="XX-Small" Width="190px" AutoPostBack="True"></asp:DropDownList></TD>
																	</TR>
																	<TR>
																		<TD></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD align="right" width="110">
																<asp:Image id="ImgClevisbracket" runat="server" ImageUrl="accessories\clevisbracket.jpg"></asp:Image></TD>
														</TR>
													</TABLE>
												</asp:panel></TD>
											<TD><asp:panel id="PanelLinearCoupler" runat="server" Enabled="False">
													<TABLE id="Table9" cellSpacing="0" cellPadding="0" width="300" border="0">
														<TR>
															<TD style="WIDTH: 251px">
																<TABLE id="Table10" style="WIDTH: 192px; HEIGHT: 91px" cellSpacing="0" cellPadding="0"
																	width="192" border="0">
																	<TR>
																		<TD style="HEIGHT: 8px">
																			<asp:CheckBox id="CBLinearcoupler" runat="server" Font-Size="Smaller" Text="Linear Alignment Coupler"
																				AutoPostBack="True"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:DropDownList id="DDLLinearcoupler" runat="server" Font-Size="XX-Small" Width="190px" AutoPostBack="True"></asp:DropDownList></TD>
																	</TR>
																	<TR>
																		<TD></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD align="right" width="110">
																<asp:Image id="ImgLinearcoupler" runat="server" ImageUrl="accessories\linearcoupler.jpg"></asp:Image></TD>
														</TR>
													</TABLE>
												</asp:panel></TD>
										</TR>
										<TR>
											<TD><asp:panel id="PanelSphericalRodEye" runat="server" Enabled="False">
													<TABLE id="Table19" cellSpacing="0" cellPadding="0" width="300" border="0">
														<TR>
															<TD style="WIDTH: 251px">
																<TABLE id="Table20" style="WIDTH: 192px; HEIGHT: 91px" cellSpacing="0" cellPadding="0"
																	width="192" border="0">
																	<TR>
																		<TD style="HEIGHT: 13px">
																			<asp:CheckBox id="CBSphericalrodeye" runat="server" Font-Size="Smaller" Text="Spherical Rod Eye"
																				AutoPostBack="True"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:DropDownList id="DDLSphericalrodeye" runat="server" Font-Size="XX-Small" Width="190px" AutoPostBack="True"></asp:DropDownList></TD>
																	</TR>
																	<TR>
																		<TD></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD align="right" width="110">
																<asp:Image id="ImgSphericalrodeye" runat="server" ImageUrl="accessories\sphericalrodeye.jpg"></asp:Image></TD>
														</TR>
													</TABLE>
												</asp:panel></TD>
											<TD><asp:panel id="PanelSphericalClevisBracket" runat="server" Enabled="False">
													<TABLE id="Table8" cellSpacing="0" cellPadding="0" width="300" border="0">
														<TR>
															<TD style="WIDTH: 251px">
																<TABLE id="Table11" style="WIDTH: 192px; HEIGHT: 91px" cellSpacing="0" cellPadding="0"
																	width="192" border="0">
																	<TR>
																		<TD style="HEIGHT: 10px">
																			<asp:CheckBox id="CBSphericalclevisbracket" runat="server" Font-Size="Smaller" Text="Spherical Clevis Bracket"
																				AutoPostBack="True"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:DropDownList id="DDLSphericalclevisbracket" runat="server" Font-Size="XX-Small" Width="190px"
																				AutoPostBack="True"></asp:DropDownList></TD>
																	</TR>
																	<TR>
																		<TD></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD align="right" width="110">
																<asp:Image id="ImgSphericalclevisbraket" runat="server" ImageUrl="accessories\sphericalclevisbracket.jpg"></asp:Image></TD>
														</TR>
													</TABLE>
												</asp:panel></TD>
											<TD><asp:panel id="PanelSphericalPivotpin" runat="server" Enabled="False">
													<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="300" border="0">
														<TR>
															<TD style="WIDTH: 251px">
																<TABLE id="Table7" style="WIDTH: 192px; HEIGHT: 91px" cellSpacing="0" cellPadding="0" width="192"
																	border="0">
																	<TR>
																		<TD style="HEIGHT: 18px">
																			<asp:CheckBox id="CBSpericalpp" runat="server" Font-Size="Smaller" Text="Spherical Pivot Pin"
																				AutoPostBack="True"></asp:CheckBox></TD>
																	</TR>
																	<TR>
																		<TD>
																			<asp:DropDownList id="DDLSphericalpp" runat="server" Font-Size="XX-Small" Width="190px" AutoPostBack="True"></asp:DropDownList></TD>
																	</TR>
																	<TR>
																		<TD></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD align="right" width="110">
																<asp:Image id="ImgSphericalpp" runat="server" ImageUrl="accessories\sphericalpivotpin.jpg"></asp:Image></TD>
														</TR>
													</TABLE>
												</asp:panel></TD>
										</TR>
									</TABLE>
									<asp:label id="lblinfo" runat="server" Font-Size="Smaller" ForeColor="Red"></asp:label></TD>
							</TR>
						</TABLE>
						<asp:label id="lblkk" runat="server" Visible="False"></asp:label><asp:label id="lblmalefemale" runat="server" Visible="False"></asp:label></TD>
					<TD width="50"></TD>
				</TR>
				<TR>
					<TD align="right" width="50" height="30"><asp:linkbutton id="LBHome" runat="server" Font-Bold="True" Width="1px" Font-Size="Smaller" onclick="LBHome_Click">Back</asp:linkbutton></TD>
					<TD align="center" bgColor="#dcdcdc" height="30">
						<asp:Label id="Label2" runat="server" Font-Size="Smaller" ForeColor="#404040"> Select the rod end accessories for your cylinder and click next.</asp:Label></TD>
					<TD width="50" height="30"><asp:linkbutton id="LBNext" runat="server" Font-Bold="True" Width="1px" Font-Size="Smaller" onclick="LBNext_Click">Next</asp:linkbutton></TD>
				</TR>
				<TR>
					<TD width="50" height="20"></TD>
					<TD align="center" height="20"></TD>
					<TD width="50" height="20"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
