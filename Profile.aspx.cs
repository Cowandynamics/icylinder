using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;
namespace iCylinderV1
{
	/// <summary>
	/// Summary description for Profile.
	/// </summary>
	public partial class Profile : System.Web.UI.Page
	{
		DBClass db =new DBClass();
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if( ! IsPostBack)
			{
				if(Session["User"] !=null)
				{
					ArrayList list =new ArrayList();
					list=(ArrayList)Session["User"];
					TxtUserName.Text =list[0].ToString();
					TxtEmail.Text=list[1].ToString();
					DateTime dt=Convert.ToDateTime(list[4]);
					if(dt.AddDays(30) < DateTime.Today)
					{
						Session["temp"] =Session["User"];
						Session["User"] =null;
					}
				}
				else
				{
				Response.Redirect("Login.aspx");
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void BtnSave_Click(object sender, System.EventArgs e)
		{
			if(Session["User"] !=null)
			{

				ArrayList list =new ArrayList();
				list=(ArrayList)Session["User"];
				string encryptpass=FormsAuthentication.HashPasswordForStoringInConfigFile(TxtoldPass.Text.Trim(),"SHA1");
				if(encryptpass.Trim() == list[2].ToString())
				{
					string encryptpass1=FormsAuthentication.HashPasswordForStoringInConfigFile(Txtconfirm.Text.Trim(),"SHA1");
				
					string s =db.UpdatePass(encryptpass1.Trim(),TxtEmail.Text.Trim());
					if(s !="")
					{
						Session["User"] =null;
						LblInfo.Text ="Your password is changed !";
						ArrayList list1 =new ArrayList();
						list1=db.SelectUser(TxtEmail.Text.ToString().Trim(),Txtconfirm.Text.ToString().Trim());
						Session["User"]=list1;
						Response.Redirect("Welcome.aspx");
					}

				}
				else
				{
					
					LblInfo.Text ="Invalid Old password !!";
				}

			}
			else if(Session["temp"] !=null)
			{

				ArrayList list1 =new ArrayList();
				list1=(ArrayList)Session["temp"];
				string encryptpass1=FormsAuthentication.HashPasswordForStoringInConfigFile(TxtoldPass.Text.Trim(),"SHA1");
				if(encryptpass1.Trim() == list1[2].ToString())
				{
					string encryptpass11=FormsAuthentication.HashPasswordForStoringInConfigFile(Txtconfirm.Text.Trim(),"SHA1");
				
					string s =db.UpdatePass(encryptpass11.Trim(),TxtEmail.Text.Trim());
					if(s !="")
					{
						Session["User"] =null;
						LblInfo.Text ="Your password is changed !";
						ArrayList list11 =new ArrayList();
						list11=db.SelectUser(TxtEmail.Text.ToString().Trim(),Txtconfirm.Text.ToString().Trim());
						Session["User"]=list1;
						Response.Redirect("Welcome.aspx");
					}

				}
			}
		}
	}
}
