using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for print.
	/// </summary>
	public partial class print : System.Web.UI.Page
	{
	
		DBClass db=new DBClass();
		protected void Page_Load(object sender, System.EventArgs e)
		{
            try
            {
                if (Request.QueryString["id"] != null && Request.QueryString["type"] != null)
                {
                    string type = Request.QueryString["type"].Trim();
                    string qno = Request.QueryString["id"].Trim();
                    if (type.Trim() == "icylinder")
                    {
                        db.FillDataToCrystalReport_ICylinder(qno.Trim());
                        CrystalDecisions.CrystalReports.Engine.ReportDocument objReport = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
                        string path;
                        path = Request.PhysicalApplicationPath;
                        objReport.Load(path + "Quotation_Template_ICylinder.rpt");
                        string strCurrentDir = Server.MapPath(".") + "//quote//";
                        db.RemoveFiles(strCurrentDir);
                        objReport.Refresh();
                        objReport.SetDatabaseLogon(DBClass.ICylDBLogin, DBClass.ICylDBPassword, DBClass.ICylDBServer, DBClass.ICylDB);
                        objReport.SetParameterValue("@Qno", qno.Trim());
                        CrystalDecisions.Shared.DiskFileDestinationOptions diskOpts = new CrystalDecisions.Shared.DiskFileDestinationOptions();
                        objReport.ExportOptions.ExportFormatType = CrystalDecisions.Shared.ExportFormatType.PortableDocFormat;
                        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile;
                        diskOpts.DiskFileName = path + "//quote//" + qno.Trim() + ".pdf";
                        objReport.ExportOptions.DestinationOptions = diskOpts;
                        objReport.Export();
                        Response.Redirect("quote//" + qno.Trim() + ".pdf");
                    }
                }
            }
            catch (Exception ex)
            {
                MailService.SendMail("admin@cowandynamics.com", "", "email error- quote print", ex.Message + ex.InnerException);
            }
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
