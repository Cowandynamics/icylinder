<%@ Page language="c#" Codebehind="Icylinder_page3.aspx.cs" AutoEventWireup="True" Inherits="iCylinderV1.Icylinder_page3" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Icylinder_page3</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 1000px; HEIGHT: 487px" cellSpacing="0" cellPadding="0"
				width="1000" bgColor="lightgrey" border="0">
				<TR>
					<TD width="50" height="20"></TD>
					<TD align="center" height="20">
						<asp:label id="Label2" runat="server" BackColor="Transparent" BorderColor="Transparent" Font-Underline="True"
							Font-Size="Small">Mount</asp:label></TD>
					<TD width="50" height="20"></TD>
				</TR>
				<TR>
					<TD width="50"></TD>
					<TD align="center" bgColor="gainsboro">
						<asp:RadioButtonList id="RBLMount" runat="server" Font-Size="Smaller" RepeatColumns="8" RepeatDirection="Horizontal"
							AutoPostBack="True" onselectedindexchanged="RBLMount_SelectedIndexChanged"></asp:RadioButtonList></TD>
					<TD width="50"></TD>
				</TR>
				<TR>
					<TD align="right" width="50" height="30">
						<asp:LinkButton id="LBBack" runat="server" Font-Size="Smaller" Font-Bold="True" onclick="LBBack_Click">Back</asp:LinkButton></TD>
					<TD align="center" bgColor="#dcdcdc" height="30">
						<asp:label id="lblxi" runat="server" Font-Size="Smaller" Visible="False">XI</asp:label>
						<asp:textbox id="Txtxi" runat="server" Font-Size="XX-Small" Width="64px" Visible="False" AutoPostBack="True" ontextchanged="Txtxi_TextChanged"></asp:textbox>
						<asp:label id="lblt1" runat="server" Font-Size="Smaller" Visible="False">Tie Rod Extension Cap End</asp:label>
						<asp:textbox id="TxtBBC" runat="server" Font-Size="XX-Small" Width="64px" Visible="False" AutoPostBack="True" ontextchanged="TxtBBC_TextChanged"></asp:textbox>
						<asp:label id="lblt2" runat="server" Font-Size="Smaller" Visible="False">Tie Rod Extension Head End</asp:label>
						<asp:textbox id="TxtBBH" runat="server" Font-Size="XX-Small" Width="64px" Visible="False" AutoPostBack="True" ontextchanged="TxtBBH_TextChanged"></asp:textbox></TD>
					<TD width="50" height="30">
						<asp:LinkButton id="BtnNext" runat="server" Font-Size="Smaller" Font-Bold="True" onclick="BtnNext_Click">Next</asp:LinkButton></TD>
				</TR>
				<TR>
					<TD width="50" height="20"></TD>
					<TD align="center" height="20">
						<asp:label id="lblseries" runat="server" BackColor="Transparent" BorderColor="Transparent"
							Font-Size="XX-Small" Visible="False" ForeColor="Red">Please select/enter all option!</asp:label></TD>
					<TD width="50" height="20"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
