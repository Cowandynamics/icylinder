using System;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for Quotation.
	/// </summary>
	public class Quotation
	{
		public Quotation()
		{
			//issue #660 
			spec="";
		}
		private string office;
		private string customer;
		private string contact;
		private string attn1;
		private string attn2;
		private string attn3;
		private string shipto1;
		private string shipto2;
		private string shipto3;
		private string billto1;
		private string billto2;
		private string billto3;
		private string quoteno;
		private string quotedate;
		private string expirydate;
		private string prepairedby;
		private string code;
		private string lang;
		private string terms;
		private string delivery;
		private string currency;
		private string note;
		private string items;
		private string finish;
		private string cowanqno;
		private string finieddate;
		private string companyid;
		private string spec;
		 
		public string Office
		{
			get
			{
				return office;
			}
			set
			{
				office = value;
			}
		}

		public string Customer
		{
			get
			{
				return customer;
			}
			set
			{
				customer = value;
			}
		}

		
		public string Contact
		{
			get
			{
				return contact;
			}
			set
			{
				contact = value;
			}
		}
		public string AttnTo1
		{
			get
			{
				return attn1;
			}
			set
			{
				attn1 = value;
			}
		}
		public string AttnTo2
		{
			get
			{
				return attn2;
			}
			set
			{
				attn2 = value;
			}
		}
		public string AttnTo3
		{
			get
			{
				return attn3;
			}
			set
			{
				attn3 = value;
			}
		}
		public string BillTo1
		{
			get
			{
				return billto1;
			}
			set
			{
				billto1 = value;
			}
		}
		public string BillTo2
		{
			get
			{
				return billto2;
			}
			set
			{
				billto2 = value;
			}
		}
		public string BillTo3
		{
			get
			{
				return billto3;
			}
			set
			{
				billto3 = value;
			}
		}
		public string ShipTo1
		{
			get
			{
				return shipto1;
			}
			set
			{
				shipto1 = value;
			}
		}
		public string ShipTo2
		{
			get
			{
				return shipto2;
			}
			set
			{
				shipto2 = value;
			}
		}
		public string ShipTo3
		{
			get
			{
				return shipto3;
			}
			set
			{
				shipto3 = value;
			}

		}
		public string QuoteNo
		{
			get
			{
				return quoteno;
			}
			set
			{
				quoteno = value;
			}
		}

	 

		public string Quotedate
		{
			get
			{
				return quotedate;
			}
			set
			{
				quotedate = value;
			}
		}
		public string ExpiryDate
		{
			get
			{
				return expirydate;
			}
			set
			{
				expirydate = value;
			}
		}
		public string PrepairedBy
		{
			get
			{
				return prepairedby;
			}
			set
			{
				prepairedby = value;
			}
		}

		public string Code
		{
			get
			{
				return code;
			}
			set
			{
				code = value;
			}
		}
		public string Langu
		{
			get
			{
				return lang;
			}
			set
			{
				lang = value;
			}
		}
		public string Terms
		{
			get
			{
				return terms;
			}
			set
			{
				terms = value;
			}
		}
		public string Delivery
		{
			get
			{
				return delivery;
			}
			set
			{
				delivery = value;
			}
		}
		public string Currency
		{
			get
			{
				return currency;
			}
			set
			{
				currency = value;
			}
		}
		public string Note
		{
			get
			{
				return note;
			}
			set
			{
				note = value;
			}
		}
		public string Items
		{
			get
			{
				return items;
			}
			set
			{
				items = value;
			}
		}
		public string Finish
		{
			get
			{
				return finish;
			}
			set
			{
				finish = value;
			}
		}
		public string FinishedDate
		{
			get
			{
				return finieddate;
			}
			set
			{
				finieddate = value;
			}
		}
		public string CowanQno
		{
			get
			{
				return cowanqno;
			}
			set
			{
				cowanqno = value;
			}
		}
		public string CompanyID
		{
			get
			{
				return companyid;
			}
			set
			{
				companyid = value;
			}
		}
		public string SpecSheet
		{
			get
			{
				return spec;
			}
			set
			{
				spec = value;
			}
		}
	}
}
