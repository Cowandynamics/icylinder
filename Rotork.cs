using System;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for Rotork.
	/// </summary>
	public class Rotork
	{
		public Rotork()
		{			
		}
		private string series;
		private string action;
		private string bore;
		private string stroke;
		private string sealtemp;
		private string manufacturer;
		public string Series
		{
			get
			{
				return series;
			}
			set
			{
				series = value;
			}
		}
		public string Action
		{
			get
			{
				return action;
			}
			set
			{
				action = value;
			}
		}
		public string BoreSize
		{
			get
			{
				return bore;
			}
			set
			{
				bore = value;
			}
		}
		public string Stroke
		{
			get
			{
				return stroke;
			}
			set
			{
				stroke = value;
			}
		}
		public string SealTemp
		{
			get
			{
				return sealtemp;
			}
			set
			{
				sealtemp = value;
			}
		}
		public string Manufacturer
		{
			get
			{
				return manufacturer;
			}
			set
			{
				manufacturer = value;
			}
		}
	}
}
