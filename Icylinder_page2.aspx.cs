using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for Icylinder_page2.
	/// </summary>
	public partial class Icylinder_page2 : System.Web.UI.Page
	{
		DBClass db=new DBClass();
		coder cd1=new coder();
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["User"] !=null)
			{
				if(! IsPostBack)
				{
					if(	Session["Coder"] !=null)
					{
						lblhidden.Text="";
						ArrayList blist=new ArrayList();
						ArrayList blist1=new ArrayList();
						ArrayList blist2=new ArrayList();
						ListItem litem=new ListItem();
						cd1=(coder)Session["Coder"];
						switch (cd1.Series.Trim())
						{
							case "A":
								lblhidden.Text ="A";
								blist =db.SelectOptions_Bore("Bore_Code", "Bore_Size","WEB_Bore_TableV1","SeriesA","Bore_ID");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								break;
							case "PA":
								lblhidden.Text ="PA";
								blist =db.SelectOptions_Bore("Bore_Code", "Bore_Size","WEB_Bore_TableV1","SeriesPA","Bore_ID");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								break;
							case "PS":
								lblhidden.Text ="PS";
								blist =db.SelectOptions_Bore("Bore_Code", "Bore_Size","WEB_Bore_TableV1","SeriesPS","Bore_ID");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								break;
							case "PC":
								lblhidden.Text ="PC";
								blist =db.SelectOptions_Bore("Bore_Code", "Bore_Size","WEB_Bore_TableV1","SeriesPC","Bore_ID");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								break;
							case "N":
								lblhidden.Text ="N";
								blist =db.SelectOptions_Bore("Bore_Code", "Bore_Size","WEB_Bore_TableV1","SeriesN","Bore_ID");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								break;
							case "M":
								lblhidden.Text ="M";
								blist =db.SelectOptions_Bore("Bore_Code", "Bore_Size","WEB_Bore_TableV1","SeriesM","Bore_ID");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								break;
							case "ML":
								lblhidden.Text ="ML";
								blist =db.SelectOptions_Bore("Bore_Code", "Bore_Size","WEB_Bore_TableV1","SeriesML","Bore_ID");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								break;
							case "R":
								lblhidden.Text ="R";
								blist =db.SelectOptions_Bore("Bore_Code", "Bore_Size","WEB_Bore_TableV1","SeriesR","Bore_ID");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								break;
							case "RP":
								lblhidden.Text ="RP";
								blist =db.SelectOptions_Bore("Bore_Code", "Bore_Size","WEB_Bore_TableV1","SeriesRP","Bore_ID");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								break;
							case "L":
								ArrayList selist =new ArrayList();
								selist =(ArrayList)Session["User"];
								//issue #118 start
								//backup
								//if(selist[6].ToString().Trim() =="1002" || selist[6].ToString().Trim() =="1015" )
								//update
								if(selist[6].ToString().Trim() =="1002" || selist[6].ToString().Trim() =="1015" || selist[6].ToString().Trim() =="1021")
								//issue #118 end
								{
									lblhidden.Text ="L";
									blist =db.SelectOptions_Bore("Bore_Code", "Bore_Size","WEB_Bore_TableV1","SeriesML","Bore_ID");
									blist1 =(ArrayList)blist[0];
									blist2 =(ArrayList)blist[1];
								}
								else
								{
									lblhidden.Text ="L";
									blist =db.SelectOptions_Bore("Bore_Code", "Bore_Size","WEB_Bore_TableV1","SeriesL","Bore_ID");
									blist1 =(ArrayList)blist[0];
									blist2 =(ArrayList)blist[1];
								}
								break;
						}
								
						for(int i=0;i<blist2.Count;i++)
						{
							litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
							RBLBore.Items.Add(litem);
						}
						RBLBore.SelectedIndex =0;
						RBLBore_SelectedIndexChanged(sender,e);
					
						if(	Session["Coder"] !=null)
						{
							cd1=(coder)Session["Coder"];
							if(cd1.Bore_Size !=null)
							{
								RBLBore.SelectedValue =cd1.Bore_Size.Trim();
								RBLBore_SelectedIndexChanged(sender,e);
							}
							if(cd1.Rod_Diamtr !=null)
							{
								RBL1stRodSize.SelectedValue =cd1.Rod_Diamtr.Trim();
								RBL1stRodSize_SelectedIndexChanged(sender,e);
							}
							if(cd1.SecRodDiameter !=null)
							{
								RBL2ndRodSize.SelectedValue =cd1.SecRodDiameter.Trim().Substring(1,1);
								RBL2ndRodSize_SelectedIndexChanged(sender,e);
							}
							if(cd1.Rod_End !=null)
							{
								if(cd1.Rod_End =="A4")
								{
								
								}
								else
								{
									ArrayList rlist=new ArrayList();
									rlist=db.SelectRodEndReverse(cd1.Rod_End.Trim());
									RBLRodend1.SelectedValue =rlist[0].ToString().Trim();
									RBLRodend1_SelectedIndexChanged(sender,e);
									RBLRodend2.SelectedValue =rlist[1].ToString().Trim();
									RBLRodend2_SelectedIndexChanged(sender,e);
									DDLRodend.SelectedValue=rlist[2].ToString().Trim();
									DDLRodend_SelectedIndexChanged(sender,e);
								}
							}
							if(cd1.SecRodEnd !=null)
							{
								ArrayList rlist=new ArrayList();
								rlist=db.SelectRodEndReverse(cd1.SecRodEnd.Trim().Substring(1));
								RBL2Rodend1.SelectedValue =rlist[0].ToString().Trim();
								RBL2Rodend1_SelectedIndexChanged(sender,e);
								RBL2Rodend2.SelectedValue =rlist[1].ToString().Trim();
								RBL2Rodend2_SelectedIndexChanged(sender,e);
								DDL2Rodend.SelectedValue=rlist[2].ToString().Trim();
								DDL2Rodend_SelectedIndexChanged(sender,e);
							}
							if(cd1.Stroke !=null)
							{
								TxtStroke.Text =cd1.Stroke.Trim();
							}
							if(cd1.StopTube !=null)
							{
								PStoptube.Visible =true;
								if(cd1.StopTube.Trim().Substring(0,1)=="D")
								{
									TxtStopTube.Text=cd1.StopTube.Trim().Substring(3);
								}
								else
								{
									TxtStopTube.Text=cd1.StopTube.Trim().Substring(2);
								}
								decimal dc1,dc2,dc3=0.00m;
								dc1=Convert.ToDecimal(TxtStopTube.Text);
								dc2=Convert.ToDecimal(TxtStroke.Text);
								dc3 =dc2 - dc1;
								TxtEffectiveStrok.Text=String.Format("{0:##0.00}", dc3);
						
							}
							if(cd1.RodEx !=null || cd1.ThreadEx !=null  )
							{
								P1stAdvanced.Visible =true;
								if(cd1.RodEx !=null)
								{
									TXTRodEx.Text=cd1.RodEx.Trim().Substring(1);
								}
								if(cd1.ThreadEx !=null)
								{
									TXTThread.Text=cd1.ThreadEx.Trim().Substring(1);
								}
							}
							if(cd1.SecRodEx !=null || cd1.SecRodThreadx !=null  )
							{
								P2ndAdvanced.Visible =true;
								if(cd1.SecRodEx !=null)
								{
									TxtSecRodEx.Text=cd1.SecRodEx.Trim().Substring(2);
								}
								if(cd1.SecRodThreadx !=null)
								{
									TxtSecThreadEx.Text=cd1.SecRodThreadx.Trim().Substring(2);
								}
							}

						}
					}
				}
			}
			else
			{
				Response.Redirect("Login.aspx");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void BtnAdvanced_Click(object sender, System.EventArgs e)
		{
			if(PStoptube.Visible ==true)
			{
				PStoptube.Visible =false;
				BtnAdvanced.Text ="Click here for Stop Tube";
			}	
			else
			{
				PStoptube.Visible =true;
				BtnAdvanced.Text ="No Stop Tube please!";
			}
		}

		protected void RBLBore_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(	Session["Coder"] !=null)
			{
				
				RBL1stRodSize.Items.Clear();
				RBL2ndRodSize.Items.Clear();
				ArrayList blist=new ArrayList();
				ArrayList blist1=new ArrayList();
				ArrayList blist2=new ArrayList();
				ArrayList rdlist=new ArrayList();
				ListItem litem=new ListItem();
				cd1=(coder)Session["Coder"];
				switch (cd1.Series.Trim())
				{
					case "A":
						if(cd1.DoubleRod.Trim() =="Yes")
						{
							P2ndRod.Visible =true;
							P2ndRodnd.Visible =true;
							lbl2nd.Visible=true;
							Img1stRodEnd.ImageUrl = "rodends\\smallmale.jpg";
							Img2ndRodend.ImageUrl ="rodends\\smallmale_double.jpg";
							blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerA_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
							blist1 =(ArrayList)blist[0];
							blist2 =(ArrayList)blist[1];
							for(int i=0;i<blist2.Count;i++)
							{
								litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
								RBL1stRodSize.Items.Add(litem);
								RBL2ndRodSize.Items.Add(litem);
							}
							RBL1stRodSize.SelectedIndex =0;
							ListItem li=new ListItem();
							RBLRodend1.Items.Clear();
							RBL2Rodend1.Items.Clear();
							li =new ListItem("Standard","Standard");
							RBLRodend1.Items.Add(li);
							RBL2Rodend1.Items.Add(li);
							RBLRodend1.SelectedIndex=0;
							RBL2Rodend1.SelectedIndex=0;
							RBLRodend2.Items.Clear();
							RBL2Rodend2.Items.Clear();
							li =new ListItem("Female","Female");
							RBLRodend2.Items.Add(li);
							RBL2Rodend2.Items.Add(li);
							RBLRodend2.SelectedIndex=0;
							RBL2Rodend2.SelectedIndex=0;
							
							DDLRodend.Items.Clear();
							DDL2Rodend.Items.Clear();
							li =new ListItem("Small","Small");
							DDLRodend.Items.Add(li);
							DDL2Rodend.Items.Add(li);
							DDLRodend.SelectedIndex=0;
							DDL2Rodend.SelectedIndex=0;
						}
						else
						{
							P2ndRod.Visible =false;
							P2ndRodnd.Visible =false;
							P2ndAdvanced.Visible=false;
							lbl2nd.Visible=false;
							Img1stRodEnd.ImageUrl = "rodends\\smallmale.jpg";
							blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerA_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
							blist1 =(ArrayList)blist[0];
							blist2 =(ArrayList)blist[1];
							for(int i=0;i<blist2.Count;i++)
							{
								litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
								RBL1stRodSize.Items.Add(litem);
							}
							RBL1stRodSize.SelectedIndex =0;
							ListItem li=new ListItem();

							RBLRodend1.Items.Clear();
							li =new ListItem("Standard","Standard");
							RBLRodend1.Items.Add(li);
							RBLRodend1.SelectedIndex=0;

							RBLRodend2.Items.Clear();
							li =new ListItem("Female","Female");
							RBLRodend2.Items.Add(li);
							RBLRodend2.SelectedIndex=0;
							
							DDLRodend.Items.Clear();
							li =new ListItem("Small","Small");
							DDLRodend.Items.Add(li);
							DDLRodend.SelectedIndex=0;
							
						}
						
						break;
					case "PA":
						if(cd1.DoubleRod.Trim() =="Yes")
						{
							P2ndRod.Visible =true;
							P2ndRodnd.Visible =true;
							lbl2nd.Visible=true;
							Img1stRodEnd.ImageUrl = "rodends\\smallmale.jpg";
							Img2ndRodend.ImageUrl ="rodends\\smallmale_double.jpg";
							blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerP_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
							blist1 =(ArrayList)blist[0];
							blist2 =(ArrayList)blist[1];
							for(int i=0;i<blist2.Count;i++)
							{
								litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
								RBL1stRodSize.Items.Add(litem);
								RBL2ndRodSize.Items.Add(litem);
							}
							RBL1stRodSize.SelectedIndex =0;
							rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),RBL1stRodSize.SelectedItem.Value.Trim());
							DDLRodend.Items.Clear();
							for(int i=0;i<rdlist.Count;i++)
							{
								DDLRodend.Items.Add(rdlist[i].ToString());
								DDL2Rodend.Items.Add(rdlist[i].ToString());
							}
						}
						else
						{
							P2ndRod.Visible =false;
							P2ndRodnd.Visible =false;
							P2ndAdvanced.Visible =false;
							lbl2nd.Visible=false;
							Img1stRodEnd.ImageUrl = "rodends\\smallmale.jpg";
							blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerP_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
							blist1 =(ArrayList)blist[0];
							blist2 =(ArrayList)blist[1];
							for(int i=0;i<blist2.Count;i++)
							{
								litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
								RBL1stRodSize.Items.Add(litem);
							}
							RBL1stRodSize.SelectedIndex =0;
							rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),RBL1stRodSize.SelectedItem.Value.Trim());
							DDLRodend.Items.Clear();
							for(int i=0;i<rdlist.Count;i++)
							{
								DDLRodend.Items.Add(rdlist[i].ToString());
							}
						}
					
						break;
					case "PS":
						if(cd1.DoubleRod.Trim() =="Yes")
						{
							P2ndRod.Visible =true;
							P2ndRodnd.Visible =true;
							lbl2nd.Visible=true;
							Img1stRodEnd.ImageUrl = "rodends\\smallmale.jpg";
							Img2ndRodend.ImageUrl ="rodends\\smallmale_double.jpg";
							blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerP_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
							blist1 =(ArrayList)blist[0];
							blist2 =(ArrayList)blist[1];
							for(int i=0;i<blist2.Count;i++)
							{
								litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
								RBL1stRodSize.Items.Add(litem);
								RBL2ndRodSize.Items.Add(litem);
							}
							RBL1stRodSize.SelectedIndex =0;
							rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),RBL1stRodSize.SelectedItem.Value.Trim());
							DDLRodend.Items.Clear();
							for(int i=0;i<rdlist.Count;i++)
							{
								DDLRodend.Items.Add(rdlist[i].ToString());
								DDL2Rodend.Items.Add(rdlist[i].ToString());
							}
						}
						else
						{
							P2ndRod.Visible =false;
							P2ndRodnd.Visible =false;
							P2ndAdvanced.Visible=false;
							lbl2nd.Visible=false;
							Img1stRodEnd.ImageUrl = "rodends\\smallmale.jpg";
							blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerP_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
							blist1 =(ArrayList)blist[0];
							blist2 =(ArrayList)blist[1];
							for(int i=0;i<blist2.Count;i++)
							{
								litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
								RBL1stRodSize.Items.Add(litem);
							}
							RBL1stRodSize.SelectedIndex =0;
							rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),RBL1stRodSize.SelectedItem.Value.Trim());
							DDLRodend.Items.Clear();
							for(int i=0;i<rdlist.Count;i++)
							{
								DDLRodend.Items.Add(rdlist[i].ToString());
							}
						}
					
						break;
					case "PC":
						if(cd1.DoubleRod.Trim() =="Yes")
						{
							P2ndRod.Visible =true;
							P2ndRodnd.Visible =true;
							lbl2nd.Visible=true;
							Img1stRodEnd.ImageUrl = "rodends\\smallmale.jpg";
							Img2ndRodend.ImageUrl ="rodends\\smallmale_double.jpg";
							blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerP_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
							blist1 =(ArrayList)blist[0];
							blist2 =(ArrayList)blist[1];
							for(int i=0;i<blist2.Count;i++)
							{
								litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
								RBL1stRodSize.Items.Add(litem);
								RBL2ndRodSize.Items.Add(litem);
							}
							RBL1stRodSize.SelectedIndex =0;
							rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),RBL1stRodSize.SelectedItem.Value.Trim());
							DDLRodend.Items.Clear();
							for(int i=0;i<rdlist.Count;i++)
							{
								DDLRodend.Items.Add(rdlist[i].ToString());
								DDL2Rodend.Items.Add(rdlist[i].ToString());
							}
						}
						else
						{
							P2ndRod.Visible =false;
							P2ndRodnd.Visible =false;
							P2ndAdvanced.Visible=false;
							lbl2nd.Visible=false;
							Img1stRodEnd.ImageUrl = "rodends\\smallmale.jpg";
							blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerP_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
							blist1 =(ArrayList)blist[0];
							blist2 =(ArrayList)blist[1];
							for(int i=0;i<blist2.Count;i++)
							{
								litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
								RBL1stRodSize.Items.Add(litem);
							}
							RBL1stRodSize.SelectedIndex =0;
							rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),RBL1stRodSize.SelectedItem.Value.Trim());
							DDLRodend.Items.Clear();
							for(int i=0;i<rdlist.Count;i++)
							{
								DDLRodend.Items.Add(rdlist[i].ToString());
							}
						}
						
						break;
					case "N":
						if(cd1.DoubleRod.Trim() =="Yes")
						{
							P2ndRod.Visible =true;
							P2ndRodnd.Visible =true;
							lbl2nd.Visible=true;
							Img1stRodEnd.ImageUrl = "rodends\\smallmale.jpg";
							Img2ndRodend.ImageUrl ="rodends\\smallmale_double.jpg";
							blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerN_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
							blist1 =(ArrayList)blist[0];
							blist2 =(ArrayList)blist[1];
							for(int i=0;i<blist2.Count;i++)
							{
								litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
								RBL1stRodSize.Items.Add(litem);
								RBL2ndRodSize.Items.Add(litem);
							}
							RBL1stRodSize.SelectedIndex =0;
							rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),RBL1stRodSize.SelectedItem.Value.Trim());
							DDLRodend.Items.Clear();
							for(int i=0;i<rdlist.Count;i++)
							{
								DDLRodend.Items.Add(rdlist[i].ToString());
								DDL2Rodend.Items.Add(rdlist[i].ToString());
							}
						}
						else
						{
							P2ndRod.Visible =false;
							P2ndRodnd.Visible =false;
							P2ndAdvanced.Visible =false;
							lbl2nd.Visible=false;
							Img1stRodEnd.ImageUrl = "rodends\\smallmale.jpg";
							blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerN_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
							blist1 =(ArrayList)blist[0];
							blist2 =(ArrayList)blist[1];
							for(int i=0;i<blist2.Count;i++)
							{
								litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
								RBL1stRodSize.Items.Add(litem);
							}
							RBL1stRodSize.SelectedIndex =0;
							rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),RBL1stRodSize.SelectedItem.Value.Trim());
							DDLRodend.Items.Clear();
							for(int i=0;i<rdlist.Count;i++)
							{
								DDLRodend.Items.Add(rdlist[i].ToString());
							}
						}
					
						break;
					case "M":
						if(cd1.DoubleRod.Trim() =="Yes")
						{
							P2ndRod.Visible =true;
							P2ndRodnd.Visible =true;
							lbl2nd.Visible=true;
							Img1stRodEnd.ImageUrl = "rodends\\smallmale.jpg";
							Img2ndRodend.ImageUrl ="rodends\\smallmale_double.jpg";
							blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerM_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
							blist1 =(ArrayList)blist[0];
							blist2 =(ArrayList)blist[1];
							for(int i=0;i<blist2.Count;i++)
							{
								litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
								RBL1stRodSize.Items.Add(litem);
								RBL2ndRodSize.Items.Add(litem);
							}
							RBL1stRodSize.SelectedIndex =0;
							rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),RBL1stRodSize.SelectedItem.Value.Trim());
							DDLRodend.Items.Clear();
							for(int i=0;i<rdlist.Count;i++)
							{
								DDLRodend.Items.Add(rdlist[i].ToString());
								DDL2Rodend.Items.Add(rdlist[i].ToString());
							}
						}
						else
						{
							P2ndRod.Visible =false;
							P2ndRodnd.Visible =false;
							P2ndAdvanced.Visible =false;
							lbl2nd.Visible=false;
							Img1stRodEnd.ImageUrl = "rodends\\smallmale.jpg";
							blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerM_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
							blist1 =(ArrayList)blist[0];
							blist2 =(ArrayList)blist[1];
							for(int i=0;i<blist2.Count;i++)
							{
								litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
								RBL1stRodSize.Items.Add(litem);
							}
							RBL1stRodSize.SelectedIndex =0;
							rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),RBL1stRodSize.SelectedItem.Value.Trim());
							DDLRodend.Items.Clear();
							for(int i=0;i<rdlist.Count;i++)
							{
								DDLRodend.Items.Add(rdlist[i].ToString());
							}
						}
						
						break;
					case "ML":
						if(cd1.DoubleRod.Trim() =="Yes")
						{
							P2ndRod.Visible =true;
							P2ndRodnd.Visible =true;
							lbl2nd.Visible=true;
							Img1stRodEnd.ImageUrl = "rodends\\smallmale.jpg";
							Img2ndRodend.ImageUrl ="rodends\\smallmale_double.jpg";
							blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerM_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
							blist1 =(ArrayList)blist[0];
							blist2 =(ArrayList)blist[1];
							for(int i=0;i<blist2.Count;i++)
							{
								litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
								RBL1stRodSize.Items.Add(litem);
								RBL2ndRodSize.Items.Add(litem);
							}
							RBL1stRodSize.SelectedIndex =0;
							rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),RBL1stRodSize.SelectedItem.Value.Trim());
							DDLRodend.Items.Clear();
							for(int i=0;i<rdlist.Count;i++)
							{
								DDLRodend.Items.Add(rdlist[i].ToString());
								DDL2Rodend.Items.Add(rdlist[i].ToString());
							}
						}
						else
						{
							P2ndRod.Visible =false;
							P2ndRodnd.Visible =false;
							P2ndAdvanced.Visible =false;
							lbl2nd.Visible=false;
							Img1stRodEnd.ImageUrl = "rodends\\smallmale.jpg";
							blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerM_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
							blist1 =(ArrayList)blist[0];
							blist2 =(ArrayList)blist[1];
							for(int i=0;i<blist2.Count;i++)
							{
								litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
								RBL1stRodSize.Items.Add(litem);
							}
							RBL1stRodSize.SelectedIndex =0;
							rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),RBL1stRodSize.SelectedItem.Value.Trim());
							DDLRodend.Items.Clear();
							for(int i=0;i<rdlist.Count;i++)
							{
								DDLRodend.Items.Add(rdlist[i].ToString());
							}
						}
						
						break;
					case "R":
						if(cd1.DoubleRod.Trim() =="Yes")
						{
							P2ndRod.Visible =true;
							P2ndRodnd.Visible =true;
							lbl2nd.Visible=true;
							Img1stRodEnd.ImageUrl = "rodends\\smallmale.jpg";
							Img2ndRodend.ImageUrl ="rodends\\smallmale_double.jpg";
							blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerR_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
							blist1 =(ArrayList)blist[0];
							blist2 =(ArrayList)blist[1];
							for(int i=0;i<blist2.Count;i++)
							{
								litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
								RBL1stRodSize.Items.Add(litem);
								RBL2ndRodSize.Items.Add(litem);
							}
							RBL1stRodSize.SelectedIndex =0;
							rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),RBL1stRodSize.SelectedItem.Value.Trim());
							DDLRodend.Items.Clear();
							for(int i=0;i<rdlist.Count;i++)
							{
								DDLRodend.Items.Add(rdlist[i].ToString());
								DDL2Rodend.Items.Add(rdlist[i].ToString());
							}
						}
						else
						{
							P2ndRod.Visible =false;
							P2ndRodnd.Visible =false;
							P2ndAdvanced.Visible =false;
							lbl2nd.Visible=false;
							Img1stRodEnd.ImageUrl = "rodends\\smallmale.jpg";
							blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerR_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
							blist1 =(ArrayList)blist[0];
							blist2 =(ArrayList)blist[1];
							for(int i=0;i<blist2.Count;i++)
							{
								litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
								RBL1stRodSize.Items.Add(litem);
							}
							RBL1stRodSize.SelectedIndex =0;
							rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),RBL1stRodSize.SelectedItem.Value.Trim());
							DDLRodend.Items.Clear();
							for(int i=0;i<rdlist.Count;i++)
							{
								DDLRodend.Items.Add(rdlist[i].ToString());
							}
						}
					
						break;
					case "RP":
						if(cd1.DoubleRod.Trim() =="Yes")
						{
							P2ndRod.Visible =true;
							P2ndRodnd.Visible =true;
							lbl2nd.Visible=true;
							Img1stRodEnd.ImageUrl = "rodends\\smallmale.jpg";
							Img2ndRodend.ImageUrl ="rodends\\smallmale_double.jpg";
							blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerRP_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
							blist1 =(ArrayList)blist[0];
							blist2 =(ArrayList)blist[1];
							for(int i=0;i<blist2.Count;i++)
							{
								litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
								RBL1stRodSize.Items.Add(litem);
								RBL2ndRodSize.Items.Add(litem);
							}
							RBL1stRodSize.SelectedIndex =0;
							rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),RBL1stRodSize.SelectedItem.Value.Trim());
							DDLRodend.Items.Clear();
							for(int i=0;i<rdlist.Count;i++)
							{
								DDLRodend.Items.Add(rdlist[i].ToString());
								DDL2Rodend.Items.Add(rdlist[i].ToString());
							}
						}
						else
						{
							P2ndRod.Visible =false;
							P2ndRodnd.Visible =false;
							P2ndAdvanced.Visible =false;
							lbl2nd.Visible=false;
							Img1stRodEnd.ImageUrl = "rodends\\smallmale.jpg";
							blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerRP_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
							blist1 =(ArrayList)blist[0];
							blist2 =(ArrayList)blist[1];
							for(int i=0;i<blist2.Count;i++)
							{
								litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
								RBL1stRodSize.Items.Add(litem);
							}
							RBL1stRodSize.SelectedIndex =0;
							rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),RBL1stRodSize.SelectedItem.Value.Trim());
							DDLRodend.Items.Clear();
							for(int i=0;i<rdlist.Count;i++)
							{
								DDLRodend.Items.Add(rdlist[i].ToString());
							}
						}					
						break;
					case "L":
						bool mlprice=false;
						ArrayList selist =new ArrayList();
						selist =(ArrayList)Session["User"];
						//issue #118 start
						//backup
						//if(selist[6].ToString().Trim() =="1002" || selist[6].ToString().Trim() =="1015" )
						//update
						if(selist[6].ToString().Trim() =="1002" || selist[6].ToString().Trim() =="1015" || selist[6].ToString().Trim() =="1021" )
						//issue #118 end
						{
							if(cd1.Series.Trim()=="L")
							{
								string brval="";
								brval=db.SelectValue("BoreValue","WEB_Bore_TableV1","Bore_Code",RBLBore.SelectedItem.Value.Trim());
								decimal bdc=0;
								bdc=Convert.ToDecimal(brval);							
								if(bdc >6)
								{
									mlprice=true;
								}
							}
						}
						if(mlprice ==true)
						{
							if(cd1.DoubleRod.Trim() =="Yes")
							{
								P2ndRod.Visible =true;
								P2ndRodnd.Visible =true;
								lbl2nd.Visible=true;
								Img1stRodEnd.ImageUrl = "rodends\\smallmale.jpg";
								Img2ndRodend.ImageUrl ="rodends\\smallmale_double.jpg";
								blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerM_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
									RBL1stRodSize.Items.Add(litem);
									RBL2ndRodSize.Items.Add(litem);
								}
								RBL1stRodSize.SelectedIndex =0;
								rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),RBL1stRodSize.SelectedItem.Value.Trim());
								DDLRodend.Items.Clear();
								for(int i=0;i<rdlist.Count;i++)
								{
									DDLRodend.Items.Add(rdlist[i].ToString());
									DDL2Rodend.Items.Add(rdlist[i].ToString());
								}
							}
							else
							{
								P2ndRod.Visible =false;
								P2ndRodnd.Visible =false;
								P2ndAdvanced.Visible =false;
								lbl2nd.Visible=false;
								Img1stRodEnd.ImageUrl = "rodends\\smallmale.jpg";
								blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerM_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
									RBL1stRodSize.Items.Add(litem);
								}
								RBL1stRodSize.SelectedIndex =0;
								rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),RBL1stRodSize.SelectedItem.Value.Trim());
								DDLRodend.Items.Clear();
								for(int i=0;i<rdlist.Count;i++)
								{
									DDLRodend.Items.Add(rdlist[i].ToString());
								}
							}
						}
						else
						{
							if(cd1.DoubleRod.Trim() =="Yes")
							{
								P2ndRod.Visible =true;
								P2ndRodnd.Visible =true;
								lbl2nd.Visible=true;
								Img1stRodEnd.ImageUrl = "rodends\\smallmale.jpg";
								Img2ndRodend.ImageUrl ="rodends\\smallmale_double.jpg";
								blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerL_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
									RBL1stRodSize.Items.Add(litem);
									RBL2ndRodSize.Items.Add(litem);
								}
								RBL1stRodSize.SelectedIndex =0;
								rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),RBL1stRodSize.SelectedItem.Value.Trim());
								DDLRodend.Items.Clear();
								for(int i=0;i<rdlist.Count;i++)
								{
									DDLRodend.Items.Add(rdlist[i].ToString());
									DDL2Rodend.Items.Add(rdlist[i].ToString());
								}
							}
							else
							{
								P2ndRod.Visible =false;
								P2ndRodnd.Visible =false;
								P2ndAdvanced.Visible =false;
								lbl2nd.Visible=false;
								Img1stRodEnd.ImageUrl = "rodends\\smallmale.jpg";
								blist =db.SelectOptions("Rod_Code", "Rod_Size","WEB_RodSerL_TableV1",RBLBore.SelectedItem.Value.Trim(),"Rod_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(),blist1[i].ToString().Trim());
									RBL1stRodSize.Items.Add(litem);
								}
								RBL1stRodSize.SelectedIndex =0;
								rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),RBL1stRodSize.SelectedItem.Value.Trim());
								DDLRodend.Items.Clear();
								for(int i=0;i<rdlist.Count;i++)
								{
									DDLRodend.Items.Add(rdlist[i].ToString());
								}
							}
						}
						break;
				}				
			}
		}

		protected void LBBack_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Icylinder_Page1.aspx");
		}

		protected void LB1stRodAdvanced_Click(object sender, System.EventArgs e)
		{
			if(P1stAdvanced.Visible ==true)
			{
				P1stAdvanced.Visible =false;
				LB1stRodAdvanced.Text ="Advaced Options";
			}	
			else
			{
				P1stAdvanced.Visible =true;
				LB1stRodAdvanced.Text ="No Advanced Options !";
			}
		}

		protected void LB2ndRodAdvanced_Click(object sender, System.EventArgs e)
		{
			if(P2ndAdvanced.Visible ==true)
			{
				P2ndAdvanced.Visible =false;
				LB2ndRodAdvanced.Text ="Advaced Options";
			}	
			else
			{
				P2ndAdvanced.Visible =true;
				LB2ndRodAdvanced.Text ="No Advanced Options !";
			}
		}

		protected void RBL1stRodSize_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ArrayList rdlist=new ArrayList();
			rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),RBL1stRodSize.SelectedItem.Value.Trim());
			DDLRodend.Items.Clear();
			for(int i=0;i<rdlist.Count;i++)
			{
				DDLRodend.Items.Add(rdlist[i].ToString());
			}
		}

		protected void RBL2ndRodSize_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ArrayList rdlist=new ArrayList();
			rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBL2Rodend1.SelectedItem.Text.Trim(),RBL2Rodend2.SelectedItem.Text.Trim(),RBL2ndRodSize.SelectedItem.Value.Trim());
			DDL2Rodend.Items.Clear();
			for(int i=0;i<rdlist.Count;i++)
			{
				DDL2Rodend.Items.Add(rdlist[i].ToString());
			}
		}

		protected void RBLRodend1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(RBLRodend1.SelectedIndex ==0)
			{
				ListItem li=new ListItem();
				RBLRodend2.Items.Clear();
				li =new ListItem("Male","Male");
				RBLRodend2.Items.Add(li);
				li =new ListItem("Female","Female");
				RBLRodend2.Items.Add(li);
				li =new ListItem("Plain","Plain");
				RBLRodend2.Items.Add(li);
				li =new ListItem("Flange","Flange");
				RBLRodend2.Items.Add(li);
				RBLRodend2.SelectedIndex =0;
				ArrayList rdlist=new ArrayList();
				rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),RBL1stRodSize.SelectedItem.Value.Trim());
				DDLRodend.Items.Clear();
				for(int i=0;i<rdlist.Count;i++)
				{
					DDLRodend.Items.Add(rdlist[i].ToString());
				}
			}
			else
			{
				ListItem li=new ListItem();
				RBLRodend2.Items.Clear();
				li =new ListItem("Male","Male");
				RBLRodend2.Items.Add(li);
				li =new ListItem("Female","Female");
				RBLRodend2.Items.Add(li);
				RBLRodend2.SelectedIndex =0;
				ArrayList rdlist=new ArrayList();
				rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),RBL1stRodSize.SelectedItem.Value.Trim());
				DDLRodend.Items.Clear();
				for(int i=0;i<rdlist.Count;i++)
				{
					DDLRodend.Items.Add(rdlist[i].ToString());
				}
				
			}
		}

		protected void RBL2Rodend1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(RBL2Rodend1.SelectedIndex ==0)
			{
				ListItem li=new ListItem();
				RBL2Rodend2.Items.Clear();
				li =new ListItem("Male","Male");
				RBL2Rodend2.Items.Add(li);
				li =new ListItem("Female","Female");
				RBL2Rodend2.Items.Add(li);
				li =new ListItem("Plain","Plain");
				RBL2Rodend2.Items.Add(li);
				li =new ListItem("Flange","Flange");
				RBL2Rodend2.Items.Add(li);
				RBL2Rodend2.SelectedIndex =0;
				ArrayList rdlist=new ArrayList();
				rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBL2Rodend1.SelectedItem.Text.Trim(),RBL2Rodend2.SelectedItem.Text.Trim(),RBL2ndRodSize.SelectedItem.Value.Trim());
				DDL2Rodend.Items.Clear();
				for(int i=0;i<rdlist.Count;i++)
				{
					DDL2Rodend.Items.Add(rdlist[i].ToString());
				}
			}
			else
			{
				ListItem li=new ListItem();
				RBL2Rodend2.Items.Clear();
				li =new ListItem("Male","Male");
				RBL2Rodend2.Items.Add(li);
				li =new ListItem("Female","Female");
				RBL2Rodend2.Items.Add(li);
				RBL2Rodend2.SelectedIndex =0;
				ArrayList rdlist=new ArrayList();
				rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBL2Rodend1.SelectedItem.Text.Trim(),RBL2Rodend2.SelectedItem.Text.Trim(),RBL2ndRodSize.SelectedItem.Value.Trim());
				DDL2Rodend.Items.Clear();
				for(int i=0;i<rdlist.Count;i++)
				{
					DDL2Rodend.Items.Add(rdlist[i].ToString());
				}
				
			}
		}

		protected void RBLRodend2_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(RBLRodend2.SelectedIndex ==0)
			{
				Img1stRodEnd.ImageUrl = "rodends\\smallmale.jpg";
			}
			else if(RBLRodend2.SelectedIndex ==1)
			{
				Img1stRodEnd.ImageUrl = "rodends\\smallfemale.jpg";
			}
			else if(RBLRodend2.SelectedIndex ==2)
			{
				Img1stRodEnd.ImageUrl = "rodends\\plain.jpg";
			}
			else if(RBLRodend2.SelectedIndex ==3)
			{
				Img1stRodEnd.ImageUrl = "rodends\\flange.jpg";
			}
			ArrayList rdlist=new ArrayList();
			rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),RBL1stRodSize.SelectedItem.Value.Trim());
			DDLRodend.Items.Clear();
			for(int i=0;i<rdlist.Count;i++)
			{
				DDLRodend.Items.Add(rdlist[i].ToString());
			}
		}

		protected void RBL2Rodend2_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(RBL2Rodend2.SelectedIndex ==0)
			{
				Img2ndRodend.ImageUrl = "rodends\\smallmale_double.jpg";
			}
			else if(RBL2Rodend2.SelectedIndex ==1)
			{
				Img2ndRodend.ImageUrl = "rodends\\smallfemale_double.jpg";
			}
			else if(RBL2Rodend2.SelectedIndex ==2)
			{
				Img2ndRodend.ImageUrl = "rodends\\plain_double.jpg";
			}
			else if(RBL2Rodend2.SelectedIndex ==3)
			{
				Img2ndRodend.ImageUrl = "rodends\\flange_double.jpg";
			}
			ArrayList rdlist=new ArrayList();
			rdlist =db.SelectValues_Rodend("kk","WEB_RodEndKK_TableV1",RBL2Rodend1.SelectedItem.Text.Trim(),RBL2Rodend2.SelectedItem.Text.Trim(),RBL2ndRodSize.SelectedItem.Value.Trim());
			DDL2Rodend.Items.Clear();
			for(int i=0;i<rdlist.Count;i++)
			{
				DDL2Rodend.Items.Add(rdlist[i].ToString());
			}
		}

		protected void DDLRodend_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(DDLRodend.SelectedItem.Text.Trim() =="Small")
			{
				if(RBLRodend2.SelectedIndex ==0)
				{
					Img1stRodEnd.ImageUrl = "rodends\\smallmale.jpg";
				}
				else
				{
					Img1stRodEnd.ImageUrl = "rodends\\smallfemale.jpg";
				}
			}
			else if(DDLRodend.SelectedItem.Text.Trim() =="Intermediate")
			{
				Img1stRodEnd.ImageUrl = "rodends\\intermediate.jpg";
			}
			else if(DDLRodend.SelectedItem.Text.Trim() =="Full")
			{
				Img1stRodEnd.ImageUrl = "rodends\\full.jpg";
			}
			else 
			{
				Img1stRodEnd.ImageUrl = "rodends\\intermediate.jpg";
			}
		}

		protected void DDL2Rodend_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(DDL2Rodend.SelectedItem.Text.Trim() =="Small")
			{
				if(RBL2Rodend2.SelectedIndex ==0)
				{
					Img2ndRodend.ImageUrl = "rodends\\smallmale_double.jpg";
				}
				else
				{
					Img2ndRodend.ImageUrl = "rodends\\smallfemale_double.jpg";
				}
			}
			else if(DDL2Rodend.SelectedItem.Text.Trim() =="Intermediate")
			{
				Img2ndRodend.ImageUrl = "rodends\\intermediate_double.jpg";
			}
			else if(DDL2Rodend.SelectedItem.Text.Trim() =="Full")
			{
				Img2ndRodend.ImageUrl = "rodends\\full_double.jpg";
			}
			else 
			{
				Img2ndRodend.ImageUrl = "rodends\\intermediate_double.jpg";
			}
		}
		public static bool IsNumeric(string strInteger) 
		{
			try 
			{
				int intTemp =0;
				if(strInteger.ToString().StartsWith(".") == true)
				{
					for(int i=1; i< strInteger.Length;i++)
					{
						intTemp = Int32.Parse( strInteger.Substring(i,1) );
					}
				}
				else
				{
					for(int i=0; i< strInteger.Length;i++)
					{
						if (strInteger.ToString().Substring(i,1) !=".")
						{
							intTemp = Int32.Parse( strInteger.Substring(i,1) );
						}
					}
				}
				return true;
			} 
			catch (FormatException) 
			{
				return false;
			}    
		}

		protected void TxtStroke_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtStroke.Text.ToString().Trim() !="" && IsNumeric(TxtStroke.Text.ToString().Trim()) ==true  )
			{
			
				TxtStroke.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TxtStroke.Text));
				
				
			}
			else
			{
				TxtStroke.Text="";
			}
		}

		protected void TxtStopTube_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtStopTube.Text.ToString().Trim() !="" && IsNumeric(TxtStopTube.Text.ToString().Trim()) ==true )
			{
			
				TxtStopTube.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TxtStopTube.Text));
			}
			else
			{
				TxtStopTube.Text="";
			}
			if(TxtEffectiveStrok.Text.ToString().Trim() !="" && TxtStopTube.Text.ToString().Trim() !="")
			{
				decimal dc1,dc2,dc3=0.00m;
				dc1=Convert.ToDecimal(TxtStopTube.Text);
				dc2=Convert.ToDecimal(TxtEffectiveStrok.Text);
				dc3 =dc1 +dc2;
				TxtStroke.Text =String.Format("{0:##0.00}", dc3);
			}
		}

		protected void TxtEffectiveStrok_TextChanged(object sender, System.EventArgs e)
		{
			if(IsNumeric(TxtEffectiveStrok.Text.ToString().Trim()) ==true )
			{
			
				TxtEffectiveStrok.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TxtEffectiveStrok.Text));
			}
			else
			{
				TxtEffectiveStrok.Text="";
			}
			if(TxtEffectiveStrok.Text.ToString().Trim() !="" && TxtStopTube.Text.ToString().Trim() !="")
			{
				decimal dc1,dc2,dc3=0.00m;
				dc1=Convert.ToDecimal(TxtStopTube.Text);
				dc2=Convert.ToDecimal(TxtEffectiveStrok.Text);
				dc3 =dc1 +dc2;
				TxtStroke.Text =String.Format("{0:##0.00}", dc3);
			}
		}

		protected void TXTThread_TextChanged(object sender, System.EventArgs e)
		{
		if(TXTThread.Text.ToString().Trim() !="" && IsNumeric(TXTThread.Text.ToString().Trim()) ==true )
		 {
			
			 TXTThread.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TXTThread.Text));
		 }
		 else
		 {
			 TXTThread.Text="";
		 }
		
		}

		protected void TXTRodEx_TextChanged(object sender, System.EventArgs e)
		{
			if(TXTRodEx.Text.ToString().Trim() !="" && IsNumeric(TXTRodEx.Text.ToString().Trim()) ==true )
			{
			
				TXTRodEx.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TXTRodEx.Text));
				
			}
			else
			{
				TXTRodEx.Text="";
			}
			
		}

		protected void TxtSecThreadEx_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtSecThreadEx.Text.ToString().Trim() !="" && IsNumeric(TxtSecThreadEx.Text.ToString().Trim()) ==true )
			{
			
				TxtSecThreadEx.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TxtSecThreadEx.Text));
			}
			else
			{
				TxtSecThreadEx.Text="";
			}
		}

		protected void TxtSecRodEx_TextChanged(object sender, System.EventArgs e)
		{
			if(TxtSecRodEx.Text.ToString().Trim() !="" && IsNumeric(TxtSecRodEx.Text.ToString().Trim()) ==true )
			{
			
				TxtSecRodEx.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TxtSecRodEx.Text));
			}
			else
			{
				TxtSecRodEx.Text="";
			}
		}

		protected void BtnNext_Click(object sender, System.EventArgs e)
		{
			
				if(RBLBore.SelectedIndex >= 0 && TxtStroke.Text.Trim() !="" && RBL1stRodSize.SelectedIndex >=0)
				{
					lblstroke.Visible=false;
					cd1=new coder();
					cd1=(coder)Session["Coder"];
					cd1.MLPrice=false;
					cd1.Bore_Size =RBLBore.SelectedItem.Value.Trim();
					cd1.Stroke =TxtStroke.Text.Trim();
					//issue #90 start
					if(cd1.Series =="PA" || cd1.Series =="PC" || cd1.Series =="PS")
					{
						if (Convert.ToDecimal(TxtStroke.Text)>59.9m) cd1.TieRodeCentSupp = "CS";
						else cd1.TieRodeCentSupp = "";
					}
					//issue #90 end
					if(cd1.DoubleRod =="No")
					{
						cd1.Rod_Diamtr = RBL1stRodSize.SelectedItem.Value.Trim();
						string rend="";
						if(cd1.Series.Trim() !="A")
						{
							rend =db.SelectValue_REnd("RodEnd_Code","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),DDLRodend.SelectedItem.Text.Trim());
							cd1.Rod_End =rend.Trim();
						}
						else
						{	
							cd1.Rod_End ="A4";
						}
						if(P1stAdvanced.Visible ==true)
						{
							if(TXTRodEx.Text.Trim() !="")
							{
								cd1.RodEx ="W"+TXTRodEx.Text.Trim();
							}
							if(TXTThread.Text.Trim() !="")
							{
								cd1.ThreadEx ="A"+TXTThread.Text.Trim();
							}
						
						}
					}
					else
					{
						cd1.Rod_Diamtr = RBL1stRodSize.SelectedItem.Value.Trim();
						cd1.SecRodDiameter = "D"+ RBL2ndRodSize.SelectedItem.Value.Trim()+"2";
						string rend="";
						if(cd1.Series.Trim() !="A")
						{
							rend =db.SelectValue_REnd("RodEnd_Code","WEB_RodEndKK_TableV1",RBLRodend1.SelectedItem.Text.Trim(),RBLRodend2.SelectedItem.Text.Trim(),DDLRodend.SelectedItem.Text.Trim());
							cd1.Rod_End =rend.Trim();
							rend="";
							rend =db.SelectValue_REnd("RodEnd_Code","WEB_RodEndKK_TableV1",RBL2Rodend1.SelectedItem.Text.Trim(),RBL2Rodend2.SelectedItem.Text.Trim(),DDL2Rodend.SelectedItem.Text.Trim());
							cd1.SecRodEnd ="R"+rend.Trim();
						}
						else
						{
							cd1.Rod_End="A4";
							rend="";
							rend =db.SelectValue_REnd("RodEnd_Code","WEB_RodEndKK_TableV1",RBL2Rodend1.SelectedItem.Text.Trim(),RBL2Rodend2.SelectedItem.Text.Trim(),DDL2Rodend.SelectedItem.Text.Trim());
							cd1.SecRodEnd ="R"+rend.Trim();
						}
						if(P1stAdvanced.Visible ==true)
						{
							if(TXTRodEx.Text.Trim() !="")
							{
								cd1.RodEx ="W"+TXTRodEx.Text.Trim();
							}
							if(TXTThread.Text.Trim() !="")
							{
								cd1.ThreadEx ="A"+TXTThread.Text.Trim();
							}
						
						}
						if(P2ndAdvanced.Visible ==true)
						{
							if(TxtSecRodEx.Text.Trim() !="")
							{
								cd1.SecRodEx ="WD"+TxtSecRodEx.Text.Trim();
							}
							if(TxtSecThreadEx.Text.Trim() !="")
							{
								cd1.SecRodThreadx ="AD"+TxtSecThreadEx.Text.Trim();
							}
						}
					}
					if(PStoptube.Visible ==true)
					{
						if(RBStrokeType.SelectedIndex == 0)
						{
							cd1.StopTube="ST"+TxtStopTube.Text.Trim();
						}
						else
						{
							cd1.StopTube="DST"+TxtStopTube.Text.Trim();
						}
					}
					ArrayList selist =new ArrayList();
					selist =(ArrayList)Session["User"];
					//issue #118 start
					//backup
					//if(selist[6].ToString().Trim() =="1002" || selist[6].ToString().Trim() =="1015" )
					//update
					if(selist[6].ToString().Trim() =="1002" || selist[6].ToString().Trim() =="1015"  || selist[6].ToString().Trim() =="1021" )
					//issue #118 end
					{
						if(cd1.Series.Trim()=="L")
						{
							string brval="";
							brval=db.SelectValue("BoreValue","WEB_Bore_TableV1","Bore_Code",cd1.Bore_Size.Trim());
							decimal bdc=0;
							bdc=Convert.ToDecimal(brval);
							if(bdc >4 && bdc <=6)
							{
								cd1.MLPrice=true;
							}
							if(bdc >6)
							{
								cd1.Series="ML";
								cd1.Transducer="TB";
								cd1.MLPrice =true;
							}
						}
					}
					cd1.CompanyID=selist[6].ToString().Trim();
					Session["Coder"] =cd1;
					Response.Redirect("Icylinder_page3.aspx");				
				}
				else
				{
					lblstroke.Text="Please enter stroke !!";
					lblstroke.Visible=true;
				}
			
			
		}
	}
}
