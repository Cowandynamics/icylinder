using System;
using System.Collections;
namespace iCylinderV1
{
	/// <summary>
	/// Summary description for PartNumber.
	/// </summary>
	public class PartNumber
	{
		public PartNumber()
		{
			
		}
		
		private string series;
		private string bore;
		private string rod;
		private string rodend;
		private string cushions;
		private string cushionpos;
		private ArrayList applicationopt =new ArrayList();
		private string secroddiam;
		private string gdrain;
		private string metalscrap;
		private string magnet;
		private string secrodend;
		private string seal;
		private string pistonsealcof;
		private string rodsealcof;
		//issue #90 start
		private string tierodecentsupp;
		//issue #90 end
		private string tandemduplex;
		private string bushingcof;
		private string porttype;
		private string portsize;
		private string air;
		private string portpos;
		private string mount;
		private string stroke;
		private string doublestroke;
		private ArrayList popularopt =new ArrayList();
		private string threadex;
		private string secrodthreadex;
		private string coating;
		private string cfbarrel;
		private string sstierod;
		private string sspistionrod;
		private string stoptube;
		private string rodext;
		private string secrodex;
		private string trunnions;
		private string pno;
		private string doublerod; 
		private string bbc;
		private string bbh; 
		private string trans; 
		private string allss; 
		private string allssc; 
		private string thirdPartypn; 
		private string rodboot;
		public string Series
		{
			get
			{
				return series;
			}
			set
			{
				series = value;
			}
		}

		public string Bore_Size
		{
			get
			{
				return bore;
			}
			set
			{
				bore = value;
			}
		}

		
		public string Rod_Diamtr
		{
			get
			{
				return rod;
			}
			set
			{
				rod = value;
			}
		}
		public string Rod_End
		{
			get
			{
				return rodend;
			}
			set
			{
				rodend = value;
			}
		}
		public string Cushions
		{
			get
			{
				return cushions;
			}
			set
			{
				cushions = value;
			}
		}

		public ArrayList Application_Opt
		{
			get
			{
				return applicationopt;
			}
		}

		public string CushionPosition
		{
			get
			{
				return cushionpos;
			}
			set
			{
				cushionpos = value;
			}
		}
		public string SecRodDiameter
		{
			get
			{
				return secroddiam;
			}
			set
			{
				secroddiam = value;
			}
		}

		public string GlandDrain
		{
			get
			{
				return gdrain;
			}
			set
			{
				gdrain = value;
			}
		}
		public string MetalScrapper
		{
			get
			{
				return metalscrap;
			}
			set
			{
				metalscrap = value;
			}
		}
//		public string NonRotating
//		{
//			get
//			{
//				return nonrotate;
//			}
//			set
//			{
//				nonrotate = value;
//			}
//		}
//		public string CastIronPring
//		{
//			get
//			{
//				return castiron;
//			}
//			set
//			{
//				castiron = value;
//			}
//		}
		public string MagnetRing
		{
			get
			{
				return magnet;
			}
			set
			{
				magnet = value;
			}
		}
		public string SecRodEnd
		{
			get
			{
				return secrodend;
			}
			set
			{
				secrodend = value;
			}
		}
//		public string AWWCylinder
//		{
//			get
//			{
//				return awwacylinder;
//			}
//			set
//			{
//				awwacylinder = value;
//			}
//		}


		public string Seal_Comp
		{
			get
			{
				return seal;
			}
			set
			{
				seal = value;
			}
		}
		public string PistonSeal_Conf
		{
			get
			{
				return pistonsealcof;
			}
			set
			{
				pistonsealcof = value;
			}
		}
		public string RodSeal_Conf
		{
			get
			{
				return rodsealcof;
			}
			set
			{
				rodsealcof = value;
			}
		}
		//issue #90 start
		public string TieRodeCentSupp
		{
			get
			{
				return tierodecentsupp;
			}
			set
			{
				tierodecentsupp = value;
			}
		}
		//issue #90 end
		
		public string Bushing_Conf
		{
			get
			{
				return bushingcof;
			}
			set
			{
				bushingcof = value;
			}
		}
		
		public string Port_Type
		{
			get
			{
				return porttype;
			}
			set
			{
				porttype = value;
			}
		}
		public string PortSize
		{
			get
			{
				return portsize;
			}
			set
			{
				portsize = value;
			}
		}
		public string AirBleed
		{
			get
			{
				return air;
			}
			set
			{
				air = value;
			}
		}
		public string Port_Pos
		{
			get
			{
				return portpos;
			}
			set
			{
				portpos = value;
			}
		}
		public string Mount
		{
			get
			{
				return mount;
			}
			set
			{
				mount = value;
			}
		}
		public string Stroke
		{
			get
			{
				return stroke;
			}
			set
			{
				stroke = value;
			}
		}
		public string DoubleStroke
		{
			get
			{
				return doublestroke;
			}
			set
			{
				doublestroke = value;
			}
		}
		public ArrayList Popular_Opt
		{
			get
			{
				return popularopt;
			}
		}	
		public string ThreadEx
		{
			get
			{
				return threadex;
			}
			set
			{
				threadex = value;
			}
		}
		public string SecRodThreadx
		 {
			 get
			 {
				 return secrodthreadex;
			 }
			 set
			 {
				 secrodthreadex = value;
			 }
		 }
		public string Coating
		  {
			  get
			  {
				  return coating;
			  }
			  set
			  {
				  coating = value;
			  }
		  }
		public string CarbonFibBarrel
		   {
			   get
			   {
				   return cfbarrel;
			   }
			   set
			   {
				   cfbarrel = value;
			   }
		   }
		public string SSTieRod
			{
				get
				{
					return sstierod;
				}
				set
				{
					sstierod = value;
				}
			}
		public string SSPistionRod
			 {
				 get
				 {
					 return sspistionrod;
				 }
				 set
				 {
					 sspistionrod = value;
				 }
			 }
	
		public string StopTube
				{
					get
					{
						return stoptube;
					}
					set
					{
						stoptube = value;
					}
				}

		public string RodEx
				 {
					 get
					 {
						 return rodext;
					 }
					 set
					 {
						 rodext = value;
					 }
				 }
		public string SecRodEx
				  {
					  get
					  {
						  return secrodex;
					  }
					  set
					  {
						  secrodex = value;
					  }
				  }
		public string Trunnions
				   {
					   get
					   {
						   return trunnions;
					   }
					   set
					   {
						   trunnions = value;
					   }
				   }
		public string PNO
		{
			get
			{
				return pno;
			}
			set
			{
				pno = value;
			}
		}
		public string DoubleRod
		{
			get
			{
				return doublerod;
			}
			set
			{
				doublerod = value;
			}
		}
		public string TandemDuplex
		{
			get
			{
				return tandemduplex;
			}
			set
			{
				tandemduplex = value;
			}
		}
		public string BBC
		{
			get
			{
				return bbc;
			}
			set
			{
				bbc = value;
			}
		}
		public string BBH
		{
			get
			{
				return bbh;
			}
			set
			{
				bbh = value;
			}
		}
		public string Transducer
		{
			get
			{
				return trans;
			}
			set
			{
				trans = value;
			}
		}
		public string AllSS
		{
			get
			{
				return allss;
			}
			set
			{
				allss = value;
			}
		}
		public string AllSS_Carbon
		{
			get
			{
				return allssc;
			}
			set
			{
				allssc = value;
			}
		}
		public string ThirdPartyPN
		{
			get
			{
				return thirdPartypn;
			}
			set
			{
				thirdPartypn = value;
			}
		}
		public string RodBoot
		{
			get
			{
				return rodboot;
			}
			set
			{
				rodboot = value;
			}
		}
	}
}
