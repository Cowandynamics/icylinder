using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for EV_SeriesAT_Page1.
	/// </summary>
	public partial class EV_SeriesAT_Page1 : System.Web.UI.Page
	{
		DBClass db=new DBClass();
		coder cd1=new coder();
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["User"] !=null)
			{
				lblseries.Text="";
				if(! IsPostBack)
				{
					if(	Session["Coder"] !=null)
					{
						ListItem litem=new ListItem();
						cd1=(coder)Session["Coder"];
						if (cd1.Series.Trim() =="AT")
						{
							RBLTrans.Items.Clear();
							litem =new ListItem("T2-Analog Output (Ratiometric)<br /><img alt='Analog Output (Ratiometric)' src='images\\T2.jpg'>","T2");
							RBLTrans.Items.Add(litem);
							RBLTrans.SelectedIndex=0;
						}
						if(	Session["Coder"] !=null)
						{
							cd1=new coder();
							cd1=(coder)Session["Coder"];
							if(cd1.Transducer !=null)
							{
								if(cd1.Transducer.Trim() !="")
								{
									RBLTrans.SelectedValue=cd1.Transducer.Substring(0,2);
									if(cd1.Transducer.Trim() =="T2")
									{
										RBLRemote.SelectedValue="N";
										RBLRemote_SelectedIndexChanged(sender,e);
										RBLTubing.SelectedIndex=0;
										Txtxi.Text="10";
									}
									else
									{
										if(cd1.Transducer.Length == 3)
										{
											Txtxi.Text="10";
											if(cd1.Transducer.Substring(2,1) =="R" || cd1.Transducer.Substring(2,1) =="P" 
												|| cd1.Transducer.Substring(2,1) =="C")
											{	
												if(cd1.Transducer.Substring(2,1) =="C")
												{
													Txtcustom.Text=cd1.PositionerPno.ToString();
												}
												RBLRemote.SelectedValue=cd1.Transducer.Substring(2,1) ;
												RBLRemote_SelectedIndexChanged(sender,e);
												RBLTubing.SelectedIndex=0;
											}
											else if(cd1.Transducer.Substring(2,1) =="Q")
											{											
												RBLRemote.SelectedValue="P";
												RBLRemote_SelectedIndexChanged(sender,e);
												RBLTubing.SelectedIndex=1;
											} 
											else if(cd1.Transducer.Substring(2,1) =="D")
											{		
												Txtcustom.Text=cd1.PositionerPno.ToString();
												RBLRemote.SelectedValue="C";
												RBLRemote_SelectedIndexChanged(sender,e);
												RBLTubing.SelectedIndex=1;
											}
										}
										else if(cd1.Transducer.Length > 3)
										{
											if(cd1.Transducer.Substring(2,1) =="R" || cd1.Transducer.Substring(2,1) =="P" 
												|| cd1.Transducer.Substring(2,1) =="C")
											{	
												if(cd1.Transducer.Substring(2,1) =="C")
												{
													Txtcustom.Text=cd1.PositionerPno.ToString();
												}
												RBLRemote.SelectedValue=cd1.Transducer.Substring(2,1) ;
												RBLRemote_SelectedIndexChanged(sender,e);
												RBLTubing.SelectedIndex=0;
												Txtxi.Text=cd1.Transducer.Substring(3);
											}
											else if(cd1.Transducer.Substring(2,1) =="Q")
											{											
												RBLRemote.SelectedValue="P";
												RBLRemote_SelectedIndexChanged(sender,e);
												RBLTubing.SelectedIndex=1;
												Txtxi.Text=cd1.Transducer.Substring(3);
											} 
											else if(cd1.Transducer.Substring(2,1) =="D")
											{	
												Txtcustom.Text=cd1.PositionerPno.ToString();
												RBLRemote.SelectedValue="C";
												RBLRemote_SelectedIndexChanged(sender,e);
												RBLTubing.SelectedIndex=1;
												Txtxi.Text=cd1.Transducer.Substring(3);
											}
											else
											{
												RBLRemote.SelectedValue="N";
												RBLRemote_SelectedIndexChanged(sender,e);
												RBLTubing.SelectedIndex=0;
												Txtxi.Text=cd1.Transducer.Substring(2);
											}
										}
									}
								}
							}
						}
					}
				}
			}
			else
			{
				Response.Redirect("UserLogin.aspx");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void Txtxi_TextChanged(object sender, System.EventArgs e)
		{
			try
			{
				if(Txtxi.Text.ToString().Trim() !="" && IsNumeric(Txtxi.Text.ToString().Trim()) ==true )
				{
			
					Txtxi.Text =String.Format("{0:00}",Convert.ToDecimal(Txtxi.Text));
				}
				else
				{
					Txtxi.Text="";
				}
			}
			catch(Exception ex)
			{
				string s="Error:  " +ex.Message.ToString().Replace("\r\n"," ")+ "  :: " + ex.StackTrace.ToString().Replace("\r\n"," ");
				s=s.Replace("'"," ");
				lblseries.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('"+s+"')</script>";
			}
		}
		public static bool IsNumeric(string strInteger) 
		{
			try 
			{
				int intTemp =0;
				if(strInteger.ToString().StartsWith(".") == true)
				{
					for(int i=1; i< strInteger.Length;i++)
					{
						intTemp = Int32.Parse( strInteger.Substring(i,1) );
					}
				}
				else
				{
					for(int i=0; i< strInteger.Length;i++)
					{
						if (strInteger.ToString().Substring(i,1) !=".")
						{
							intTemp = Int32.Parse( strInteger.Substring(i,1) );
						}
					}
				}
				return true;
			} 
			catch (FormatException) 
			{
				return false;
			}    
		}

		protected void LBBack_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("EV_SeriesA.aspx");
		}

		protected void BtnNext_Click(object sender, System.EventArgs e)
		{
			if(RBLTrans.SelectedIndex >-1)
			{
				if(RBLRemote.SelectedValue.ToString() =="C" && Txtcustom.Text.Trim() =="")
				{
					lblseries.Text ="<script language='javascript'>" + Environment.NewLine +"window.alert('Please enter positioner part number')</script>";
				}
				else
				{
					cd1=new coder();
					cd1 =(coder)Session["Coder"];
					if(RBLTrans.SelectedItem.Value.ToString().Trim()=="T2")
					{
						string trans1="";
						if(Txtxi.Text.Trim() !="10")
						{
							trans1=Txtxi.Text.Trim();
						}
						string tt=RBLTrans.SelectedValue.ToString().Trim()+trans1.Trim();
						if(RBLRemote.SelectedItem.Value.ToString().Trim()  !="N")
						{
							string tr="";
							if(RBLRemote.SelectedItem.Value.ToString().Trim()  =="C")
							{
								cd1.Positioner="PO";
								cd1.PositionerPno=Txtcustom.Text.Trim();
								cd1.Specials="YES";
							}
							else
							{
								cd1.Positioner="";
								cd1.PositionerPno="";
								cd1.Specials="";
							}
							if(RBLRemote.SelectedItem.Value.ToString().Trim()  =="R")
							{
								tr=tt.Insert(2,RBLRemote.SelectedItem.Value.ToString().Trim());
							}
							else if(RBLRemote.SelectedItem.Value.ToString().Trim()  !="R")
							{
								if(RBLTubing.SelectedIndex ==0)
								{
									tr=tt.Insert(2,RBLRemote.SelectedItem.Value.ToString().Trim());
								}
								else if(RBLTubing.SelectedIndex ==1 && RBLRemote.SelectedItem.Value.ToString().Trim()  =="P")
								{
									tr=tt.Insert(2,"Q");
								}
								else if(RBLTubing.SelectedIndex ==1 && RBLRemote.SelectedItem.Value.ToString().Trim()  =="C")
								{
									tr=tt.Insert(2,"D");
								}
							}
							cd1.Transducer=tr.Trim();
						}
						else
						{
							cd1.Transducer=tt.Trim();					
						}
					}
					cd1.CompanyID="1003";
					Session["Coder"] =cd1;
					Response.Redirect("Icylinder_page6.aspx");
				}
			}
		}

		protected void RBLRemote_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(RBLRemote.SelectedIndex ==0)
			{
				RBLTubing.Enabled=false;
			}
			else
			{
				RBLTubing.Enabled=true;
			}
			if(RBLRemote.SelectedItem.Value.ToString().Trim() =="C")
			{
				Txtcustom.Visible=true;
			}
			else
			{
				Txtcustom.Visible=false;
			}
		}

	}
}
