using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for ViewUploadedFile.
	/// </summary>
	public partial class ViewUploadedFile_Rotork : System.Web.UI.Page
	{
		public string AccessExcel(string name)
		{
			string str="";
			string constr="Provider=Microsoft.Jet.OLEDB.4.0;Data Source="+Request.PhysicalApplicationPath+"dbfile/"+name.ToString().Trim()
					+";Extended Properties='Excel 8.0;HDR=Yes;IMEX=2'";
			OleDbConnection myConnection = new OleDbConnection(constr);
			try
			{
				string CommandText = "select [Line No#],[Required Qty],[Style],[Hydraulic Or Pneumatic],[Cyl# Stroke],[Cyl# Bore],[Minimum Operating Pressure (Psi)],[Break To Open (LBS)],[Run To Open (LBS)]"
					+" ,[End To Open (LBS)],[Break To Close (LBS)],[Run To Close (LBS)],[End To Close (LBS)],[Safety Factor (%)],[Seals],[Rod End],[Rod Boot],[Paint],[Manual Override],[Lifting Lugs],[Customer Ref#] from [CylinderData$] Where [Line No#] >0 ";    
				OleDbCommand myCommand = new OleDbCommand(CommandText, myConnection);    
				myConnection.Open(); 
				DataSet ds=new DataSet();
				OleDbDataAdapter oledad =new OleDbDataAdapter();
				oledad.SelectCommand =myCommand;
				oledad.Fill(ds);
				DGView.DataSource = ds;
				DGView.DataBind();    
				myConnection.Close();		
			}
			catch(Exception ex)
			{
				str=ex.Message;
			}
			finally
			{
				myConnection.Close();
			}
			return str;
		}
		public static bool IsNumeric(string strInteger) 
		{
			try 
			{
				int intTemp =0;
				if(strInteger.ToString().StartsWith(".") == true)
				{
					for(int i=1; i< strInteger.Length;i++)
					{
						intTemp = Int32.Parse( strInteger.Substring(i,1) );
					}
				}
				else
				{
					for(int i=0; i< strInteger.Length;i++)
					{
						if (strInteger.ToString().Substring(i,1) !=".")
						{
							intTemp = Int32.Parse( strInteger.Substring(i,1) );
						}
					}
				}
				return true;
			} 
			catch (FormatException) 
			{
				return false;
			}    
		}
		public void DGView_Item_Command(Object o, DataGridItemEventArgs e) 
		{
			if((e.Item.ItemType ==ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem) ) 
			{ 
				string error="";
				DBClass db=new DBClass();
				if (e.Item.Cells[1].Text.Trim() =="" || e.Item.Cells[1].Text.Trim() =="&nbsp;") 
				{ 
					e.Item.Cells[1].BackColor =Color.Red; 
					error +=" Qty cannot be blank <br>";
				} 
				else 
				{ 
					if(IsNumeric(e.Item.Cells[1].Text.Trim()) ==false)
					{
						e.Item.Cells[1].BackColor =Color.Red; 
						error +="Qty should be a numeric value > 0 <br>";
					}
				}
				if (e.Item.Cells[2].Text.Trim() =="" || e.Item.Cells[2].Text.Trim() =="&nbsp;")  
				{ 
					e.Item.Cells[2].BackColor =Color.Red; 
					error +="Style cannot be blank. Please select a value from dropdown list<br>";
				} 
				if (e.Item.Cells[3].Text.Trim() =="" || e.Item.Cells[3].Text.Trim() =="&nbsp;")  
				{ 
					e.Item.Cells[3].BackColor =Color.Red; 
					error +="Pneumatic or Hydraulic cannot be blank. Please select a value from dropdown list<br>";
				}
				if (e.Item.Cells[4].Text.Trim() =="" || e.Item.Cells[4].Text.Trim() =="&nbsp;") 
				{ 
					e.Item.Cells[4].BackColor =Color.Red; 
					error +="Stroke cannot be blank<br>";
				} 
				else 
				{ 
					if(IsNumeric(e.Item.Cells[4].Text.Trim()) ==false)
					{
						e.Item.Cells[4].BackColor =Color.Red; 
						error +="Stroke should be a numeric value > 0 <br>";
					}
				}
				bool bore=false;
				if (e.Item.Cells[5].Text.Trim().Replace("&nbsp;","") !="") 
				{ 
					if(IsNumeric(e.Item.Cells[5].Text.Trim()) ==true)
					{
						bore=true;
						if(Convert.ToDecimal(e.Item.Cells[5].Text.Trim().Replace("&nbsp;","")) <4.00m  && e.Item.Cells[3].Text.Trim().ToUpper() =="PNEUMATIC")
						{
							e.Item.Cells[5].BackColor =Color.Red; 
							error +="If Pneumatic, bore size should be >= 4.00 <br>";
						}
					}
				} 	
				if (e.Item.Cells[6].Text.Trim() ==""  || e.Item.Cells[6].Text.Trim() =="&nbsp;") 
				{ 
					e.Item.Cells[6].BackColor =Color.Red; 
					error +="Minimum Operating Pressure (Psi) should be a numeric value between 25  and 150 for Pneumatic Cyliders / between 25  and 3000 for Hydraulic Cyliders<br>";
				} 
				else 
				{ 
					if(IsNumeric(e.Item.Cells[6].Text.Trim()) ==false)
					{
						e.Item.Cells[6].BackColor =Color.Red; 
						error +="Minimum Operating Pressure (Psi) should be a numeric value between 25  and 150 for Pneumatic Cyliders / between 25  and 3000 for Hydraulic Cyliders<br>";
					}
				}
				if(bore ==false)
				{					
					if (e.Item.Cells[7].Text.Trim() ==""  || e.Item.Cells[7].Text.Trim() =="&nbsp;") 
					{ 
						e.Item.Cells[7].BackColor =Color.Red; 
						error +="Break to Open should be a numeric value <br>";
					} 
					else 
					{ 
						if(IsNumeric(e.Item.Cells[7].Text.Trim()) ==false)
						{
							e.Item.Cells[7].BackColor =Color.Red; 
							error +="Break to Open should be a numeric value <br>";
						}
					}
					if (e.Item.Cells[8].Text.Trim() ==""  || e.Item.Cells[8].Text.Trim() =="&nbsp;") 
					{ 
						e.Item.Cells[8].BackColor =Color.Red; 
						error +="Run to Open should be a numeric value <br>";
					} 
					else 
					{ 
						if(IsNumeric(e.Item.Cells[8].Text.Trim()) ==false)
						{
							e.Item.Cells[8].BackColor =Color.Red; 
							error +="Run to Open should be a numeric value <br>";
						}
					}
					if (e.Item.Cells[9].Text.Trim() ==""  || e.Item.Cells[9].Text.Trim() =="&nbsp;") 
					{ 
						e.Item.Cells[9].BackColor =Color.Red; 
						error +="End to Open should be a numeric value <br>";
					} 
					else 
					{ 
						if(IsNumeric(e.Item.Cells[9].Text.Trim()) ==false)
						{
							e.Item.Cells[9].BackColor =Color.Red; 
							error +="End to Open should be a numeric value <br>";
						}
					}
					if (e.Item.Cells[10].Text.Trim() ==""  || e.Item.Cells[10].Text.Trim() =="&nbsp;") 
					{ 
						e.Item.Cells[10].BackColor =Color.Red; 
						error +="Break to Close should be a numeric value <br>";
					} 
					else 
					{ 
						if(IsNumeric(e.Item.Cells[10].Text.Trim()) ==false)
						{
							e.Item.Cells[10].BackColor =Color.Red; 
							error +="Break to Close should be a numeric value <br>";
						}
					}
					if (e.Item.Cells[11].Text.Trim() ==""  || e.Item.Cells[11].Text.Trim() =="&nbsp;") 
					{ 
						e.Item.Cells[11].BackColor =Color.Red; 
						error +="Run to Close should be a numeric value > 0 <br>";
					} 
					else 
					{ 
						if(IsNumeric(e.Item.Cells[11].Text.Trim()) ==false)
						{
							e.Item.Cells[11].BackColor =Color.Red; 
							error +="Run to Close should be a numeric value > 0 <br>";
						}
					}
					if (e.Item.Cells[12].Text.Trim() ==""  || e.Item.Cells[12].Text.Trim() =="&nbsp;") 
					{ 
						e.Item.Cells[12].BackColor =Color.Red; 
						error +="End to Close should be a numeric value > 0 <br>";
					} 
					else 
					{ 
						if(IsNumeric(e.Item.Cells[12].Text.Trim()) ==false)
						{
							e.Item.Cells[12].BackColor =Color.Red; 
							error +="End to Close should be a numeric value > 0 <br>";
						}
					}			
					if (e.Item.Cells[13].Text.Trim() ==""  || e.Item.Cells[13].Text.Trim() =="&nbsp;") 
					{ 
						e.Item.Cells[13].BackColor =Color.Red; 
						error +="Safty Factor should be a numeric value > 0 <br>";
					} 
					else 
					{ 
						if(IsNumeric(e.Item.Cells[13].Text.Trim()) ==false)
						{
							e.Item.Cells[13].BackColor =Color.Red; 
							error +="Safty Factor should be a numeric value > 0 <br>";
						}
					}
				}
				if (e.Item.Cells[14].Text.Trim() ==""  || e.Item.Cells[14].Text.Trim() =="&nbsp;") 
				{ 
					e.Item.Cells[14].BackColor =Color.Red; 
					error +="Seal cannot be blank. Please select a value from dropdown list <br>";
				} 
				else
				{
					if(db.SelectValue("Seal_Code","WEB_Seal_TableV1","Seal_Type",e.Item.Cells[14].Text.Trim()).Trim() =="")
					{
						e.Item.Cells[14].BackColor =Color.OrangeRed; 
						error +="Seal data is not a valid selection. Please select a value from dropdown list <br>";
					}
				}
				if (e.Item.Cells[15].Text.Trim() ==""  || e.Item.Cells[15].Text.Trim() =="&nbsp;") 
				{ 
					e.Item.Cells[15].BackColor =Color.Red; 
					error +="Rod End cannot be blank. Please select a value from dropdown list <br>";
				}	
				if (e.Item.Cells[16].Text.Trim() ==""  || e.Item.Cells[16].Text.Trim() =="&nbsp;") 
				{ 
					e.Item.Cells[16].BackColor =Color.Red; 
					error +="Rod Boot cannot be blank. Please select a value from dropdown list <br>";
				}	
				if (e.Item.Cells[17].Text.Trim() ==""  || e.Item.Cells[17].Text.Trim() =="&nbsp;") 
				{ 
					e.Item.Cells[17].BackColor =Color.Red; 
					error +="Paint cannot be blank. Please select a value from dropdown list <br>";
				}				
				if(e.Item.Cells[18].Text.Trim() =="" || e.Item.Cells[18].Text.Trim() =="&nbsp;")
				{
					e.Item.Cells[18].BackColor =Color.Red; 
					error +="Manual Override data is not a valid selection. Please select a value from dropdown list <br>";
				}
				if (e.Item.Cells[19].Text.Trim() ==""  || e.Item.Cells[19].Text.Trim() =="&nbsp;") 
				{ 
					e.Item.Cells[19].BackColor =Color.Red; 
					error +="Lifting Lugs cannot be blank. Please select a value from dropdown list <br>";
				}	
				if(error.Trim() !="")
				{
					Lblerr.Visible=true;
					Lblerror.Text +="<br>Line "+e.Item.Cells[0].Text.Trim()+"<br> "+error.ToString();
				}
				if (Lblerror.Text.Trim() =="")
				{
					Lblerr.Visible=false;
				}
			} 
		}
		protected void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				Lblerror.Text="";
				if(Request.QueryString["file"] !=null)
				{
					string str="";
					str=AccessExcel(Request.QueryString["file"].ToString());
					if(str.Trim() !="")
					{
						Lblerror.Text="<script language='javascript'>" + Environment.NewLine +"window.alert('"+str+"')</script>";
					}
					lbl.Text="<br><a href='dbfile/"+Request.QueryString["file"].ToString().Trim()+"' target='_blank'>Click here to download (excel format)</a>";					
				}	
				if(Request.QueryString["qno"] !=null)
				{
					string specsheet="";
					DBClass db=new DBClass();
					specsheet=db.SelectSpecSheet(Request.QueryString["qno"].ToString().Trim());
					if(specsheet.Trim() !="")
					{
						lblspec.Text="<br><a href='specsheet/"+specsheet.ToString().Trim()+"' target='_blank'>Click here to download  Spec sheet</a>";
					}
					else
					{
						lblspec.Text="There is no Spec sheet attached with this item";
					}
				}
			}
			catch(Exception ex)
			{
				Lblerror.Text="<script language='javascript'>" + Environment.NewLine +"window.alert('"+ex.Message+"')</script>";
			}

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
