using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace iCylinderV1
{
	/// <summary>
	/// Summary description for Icylinder_page5.
	/// </summary>
	public partial class Icylinder_page5 : System.Web.UI.Page
	{
		coder cd1=new coder();
		DBClass db=new DBClass();
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if(Session["User"] !=null)
			{
				if(! IsPostBack)
				{
					if(	Session["Coder"] !=null)
					{
						ArrayList blist=new ArrayList();
						ArrayList blist1=new ArrayList();
						ArrayList blist2=new ArrayList();
					
						ListItem litem=new ListItem();
						cd1=(coder)Session["Coder"];
						//issue #104 start
						//backup
//						if(cd1.Seal_Comp == "F" || cd1.Seal_Comp == "L" || cd1.Seal_Comp == "E"  ) 
//						{
//							RBLMetalScrapper.Items.RemoveAt(2);
//                        }
						//issue #104 end
						switch (cd1.Series.Trim())
						{
							case "A":
								blist =db.SelectOptions("SSPRod_Code", "SSPRod_Type","WEB_SSPRod_TableV1","SeriesA","SSPRod_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								DDLPRodMat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									DDLPRodMat.Items.Add(litem);
								}
								DDLPRodMat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("sealConf_Code", "sealConf_Conf","WEB_PistonSealConf_TableV1","SeriesA","SealConf_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLPsealmat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLPsealmat.Items.Add(litem);
								}
								RBLPsealmat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("sealConf_Code", "sealConf_Conf","WEB_RodSealConf_TableV1","SeriesA","SealConf_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLProdSealmat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLProdSealmat.Items.Add(litem);
								}
								RBLProdSealmat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Bush_Code", "Bush_Type","WEB_GlandBushing_TableV1","SeriesA","SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLBushing.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLBushing.Items.Add(litem);
								}
								RBLBushing.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Coating_Code", "Coating_type","WEB_Coating_TableV1","SeriesA","Coating_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								DDLCoating.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									DDLCoating.Items.Add(litem);
								}
								DDLCoating.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Popular_Code", "Description","WEB_BarrelMaterial_TableV1","SeriesA","Popular_ID");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLBarrel.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLBarrel.Items.Add(litem);
								}
								RBLBarrel.SelectedIndex =0;
								RBLGlanddrain.Enabled=false;
								RBLAirbleeds.Enabled=false;

								break;
							case "PA":
								blist =db.SelectOptions("SSPRod_Code", "SSPRod_Type","WEB_SSPRod_TableV1","SeriesPA","SSPRod_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								DDLPRodMat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									DDLPRodMat.Items.Add(litem);
								}
								DDLPRodMat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("sealConf_Code", "sealConf_Conf","WEB_PistonSealConf_TableV1","SeriesPA","SealConf_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLPsealmat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLPsealmat.Items.Add(litem);
								}
								RBLPsealmat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("sealConf_Code", "sealConf_Conf","WEB_RodSealConf_TableV1","SeriesPA","SealConf_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLProdSealmat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLProdSealmat.Items.Add(litem);
								}
								RBLProdSealmat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Bush_Code", "Bush_Type","WEB_GlandBushing_TableV1","SeriesPA","SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLBushing.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLBushing.Items.Add(litem);
								}
								RBLBushing.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Coating_Code", "Coating_type","WEB_Coating_TableV1","SeriesPA","Coating_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								DDLCoating.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									DDLCoating.Items.Add(litem);
								}
								DDLCoating.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Popular_Code", "Description","WEB_BarrelMaterial_TableV1","SeriesPA","Popular_ID");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLBarrel.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLBarrel.Items.Add(litem);
								}
								RBLBarrel.SelectedIndex =0;
								RBLAirbleeds.Enabled=false;
								RBLGlanddrain.Enabled=false;
								break;
							case "PS":
								blist =db.SelectOptions("SSPRod_Code", "SSPRod_Type","WEB_SSPRod_TableV1","SeriesPS","SSPRod_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								DDLPRodMat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									DDLPRodMat.Items.Add(litem);
								}
								DDLPRodMat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("sealConf_Code", "sealConf_Conf","WEB_PistonSealConf_TableV1","SeriesPS","SealConf_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLPsealmat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLPsealmat.Items.Add(litem);
								}
								RBLPsealmat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("sealConf_Code", "sealConf_Conf","WEB_RodSealConf_TableV1","SeriesPS","SealConf_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLProdSealmat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLProdSealmat.Items.Add(litem);
								}
								RBLProdSealmat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Bush_Code", "Bush_Type","WEB_GlandBushing_TableV1","SeriesPS","SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLBushing.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLBushing.Items.Add(litem);
								}
								RBLBushing.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Coating_Code", "Coating_type","WEB_Coating_TableV1","SeriesPS","Coating_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								DDLCoating.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									DDLCoating.Items.Add(litem);
								}
								DDLCoating.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Popular_Code", "Description","WEB_BarrelMaterial_TableV1","SeriesPS","Popular_ID");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLBarrel.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLBarrel.Items.Add(litem);
								}
								RBLBarrel.SelectedIndex =0;
								RBLAirbleeds.Enabled=false;
								RBLGlanddrain.Enabled=false;
								break;
							case "PC":
								blist =db.SelectOptions("SSPRod_Code", "SSPRod_Type","WEB_SSPRod_TableV1","SeriesPC","SSPRod_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								DDLPRodMat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									DDLPRodMat.Items.Add(litem);
								}
								DDLPRodMat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("sealConf_Code", "sealConf_Conf","WEB_PistonSealConf_TableV1","SeriesPC","SealConf_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLPsealmat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLPsealmat.Items.Add(litem);
								}
								RBLPsealmat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("sealConf_Code", "sealConf_Conf","WEB_RodSealConf_TableV1","SeriesPC","SealConf_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLProdSealmat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLProdSealmat.Items.Add(litem);
								}
								RBLProdSealmat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Bush_Code", "Bush_Type","WEB_GlandBushing_TableV1","SeriesPC","SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLBushing.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLBushing.Items.Add(litem);
								}
								RBLBushing.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Coating_Code", "Coating_type","WEB_Coating_TableV1","SeriesPC","Coating_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								DDLCoating.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									DDLCoating.Items.Add(litem);
								}
								DDLCoating.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Popular_Code", "Description","WEB_BarrelMaterial_TableV1","SeriesPC","Popular_ID");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLBarrel.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLBarrel.Items.Add(litem);
								} 
								RBLBarrel.SelectedIndex =0;
								RBLAirbleeds.Enabled=false;
								RBLGlanddrain.Enabled=false;
								break;
							case "N":
								blist =db.SelectOptions("SSPRod_Code", "SSPRod_Type","WEB_SSPRod_TableV1","SeriesN","SSPRod_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								DDLPRodMat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									DDLPRodMat.Items.Add(litem);
								}
								DDLPRodMat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("sealConf_Code", "sealConf_Conf","WEB_PistonSealConf_TableV1","SeriesN","SealConf_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLPsealmat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLPsealmat.Items.Add(litem);
								}
								RBLPsealmat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("sealConf_Code", "sealConf_Conf","WEB_RodSealConf_TableV1","SeriesN","SealConf_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLProdSealmat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLProdSealmat.Items.Add(litem);
								}
								RBLProdSealmat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Bush_Code", "Bush_Type","WEB_GlandBushing_TableV1","SeriesN","SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLBushing.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLBushing.Items.Add(litem);
								}
								RBLBushing.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Coating_Code", "Coating_type","WEB_Coating_TableV1","SeriesN","Coating_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								DDLCoating.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									DDLCoating.Items.Add(litem);
								}
								DDLCoating.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Popular_Code", "Description","WEB_BarrelMaterial_TableV1","SeriesN","Popular_ID");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLBarrel.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLBarrel.Items.Add(litem);
								} 
								RBLBarrel.SelectedIndex =0;
								RBLMagnet.Enabled=false;
								RBLMetalScrapper.Enabled=false;
								break;
							case "M":
								blist =db.SelectOptions("SSPRod_Code", "SSPRod_Type","WEB_SSPRod_TableV1","SeriesM","SSPRod_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								DDLPRodMat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									DDLPRodMat.Items.Add(litem);
								}
								DDLPRodMat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("sealConf_Code", "sealConf_Conf","WEB_PistonSealConf_TableV1","SeriesM","SealConf_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLPsealmat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLPsealmat.Items.Add(litem);
								}
								RBLPsealmat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("sealConf_Code", "sealConf_Conf","WEB_RodSealConf_TableV1","SeriesM","SealConf_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLProdSealmat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLProdSealmat.Items.Add(litem);
								}
								RBLProdSealmat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Bush_Code", "Bush_Type","WEB_GlandBushing_TableV1","SeriesM","SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLBushing.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLBushing.Items.Add(litem);
								}
								RBLBushing.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Coating_Code", "Coating_type","WEB_Coating_TableV1","SeriesM","Coating_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								DDLCoating.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									DDLCoating.Items.Add(litem);
								}
								DDLCoating.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Popular_Code", "Description","WEB_BarrelMaterial_TableV1","SeriesM","Popular_ID");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLBarrel.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLBarrel.Items.Add(litem);
								} 
								RBLBarrel.SelectedIndex =0;
								RBLMagnet.Enabled=false;
								break;
							case "ML":
								blist =db.SelectOptions("SSPRod_Code", "SSPRod_Type","WEB_SSPRod_TableV1","SeriesML","SSPRod_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								DDLPRodMat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									DDLPRodMat.Items.Add(litem);
								}
								DDLPRodMat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("sealConf_Code", "sealConf_Conf","WEB_PistonSealConf_TableV1","SeriesML","SealConf_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLPsealmat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLPsealmat.Items.Add(litem);
								}
								RBLPsealmat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("sealConf_Code", "sealConf_Conf","WEB_RodSealConf_TableV1","SeriesML","SealConf_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLProdSealmat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLProdSealmat.Items.Add(litem);
								}
								RBLProdSealmat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Bush_Code", "Bush_Type","WEB_GlandBushing_TableV1","SeriesML","SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLBushing.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLBushing.Items.Add(litem);
								}
								RBLBushing.SelectedIndex=0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Coating_Code", "Coating_type","WEB_Coating_TableV1","SeriesML","Coating_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								DDLCoating.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									DDLCoating.Items.Add(litem);
								}
								DDLCoating.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Popular_Code", "Description","WEB_BarrelMaterial_TableV1","SeriesML","Popular_ID");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLBarrel.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLBarrel.Items.Add(litem);
								} 
								RBLBarrel.SelectedIndex =0;
								RBLMagnet.Enabled=false;
								break;
							case "R":
								blist =db.SelectOptions("SSPRod_Code", "SSPRod_Type","WEB_SSPRod_TableV1","SeriesR","SSPRod_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								DDLPRodMat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									DDLPRodMat.Items.Add(litem);
								}
								DDLPRodMat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("sealConf_Code", "sealConf_Conf","WEB_PistonSealConf_TableV1","SeriesR","SealConf_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLPsealmat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLPsealmat.Items.Add(litem);
								}
								RBLPsealmat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("sealConf_Code", "sealConf_Conf","WEB_RodSealConf_TableV1","SeriesR","SealConf_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLProdSealmat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLProdSealmat.Items.Add(litem);
								}
								RBLProdSealmat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Bush_Code", "Bush_Type","WEB_GlandBushing_TableV1","SeriesR","SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLBushing.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLBushing.Items.Add(litem);
								}
								RBLBushing.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Coating_Code", "Coating_type","WEB_Coating_TableV1","SeriesR","Coating_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								DDLCoating.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									DDLCoating.Items.Add(litem);
								}
								DDLCoating.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Popular_Code", "Description","WEB_BarrelMaterial_TableV1","SeriesR","Popular_ID");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLBarrel.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLBarrel.Items.Add(litem);
								} 
								RBLBarrel.SelectedIndex =0;
								RBLMagnet.Enabled=false;
								RBLMetalScrapper.Enabled=false;
								break;
							case "RP":
								blist =db.SelectOptions("SSPRod_Code", "SSPRod_Type","WEB_SSPRod_TableV1","SeriesRP","SSPRod_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								DDLPRodMat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									DDLPRodMat.Items.Add(litem);
								}
								DDLPRodMat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("sealConf_Code", "sealConf_Conf","WEB_PistonSealConf_TableV1","SeriesRP","SealConf_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLPsealmat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLPsealmat.Items.Add(litem);
								}
								RBLPsealmat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("sealConf_Code", "sealConf_Conf","WEB_RodSealConf_TableV1","SeriesRP","SealConf_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLProdSealmat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLProdSealmat.Items.Add(litem);
								}
								RBLProdSealmat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Bush_Code", "Bush_Type","WEB_GlandBushing_TableV1","SeriesRP","SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLBushing.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLBushing.Items.Add(litem);
								}
								RBLBushing.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Coating_Code", "Coating_type","WEB_Coating_TableV1","SeriesRP","Coating_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								DDLCoating.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									DDLCoating.Items.Add(litem);
								}
								DDLCoating.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Popular_Code", "Description","WEB_BarrelMaterial_TableV1","SeriesRP","Popular_ID");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLBarrel.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLBarrel.Items.Add(litem);
								} 
								RBLBarrel.SelectedIndex =0;
								RBLMagnet.Enabled=false;
								RBLMetalScrapper.Enabled=false;
								break;
							case "L":
								blist =db.SelectOptions("SSPRod_Code", "SSPRod_Type","WEB_SSPRod_TableV1","SeriesL","SSPRod_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								DDLPRodMat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									DDLPRodMat.Items.Add(litem);
								}
								DDLPRodMat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("sealConf_Code", "sealConf_Conf","WEB_PistonSealConf_TableV1","SeriesL","SealConf_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLPsealmat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLPsealmat.Items.Add(litem);
								}
								RBLPsealmat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("sealConf_Code", "sealConf_Conf","WEB_RodSealConf_TableV1","SeriesL","SealConf_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLProdSealmat.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLProdSealmat.Items.Add(litem);
								}
								RBLProdSealmat.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Bush_Code", "Bush_Type","WEB_GlandBushing_TableV1","SeriesL","SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLBushing.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLBushing.Items.Add(litem);
								}
								RBLBushing.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Coating_Code", "Coating_type","WEB_Coating_TableV1","SeriesL","Coating_SLNO");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								DDLCoating.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									DDLCoating.Items.Add(litem);
								}
								DDLCoating.SelectedIndex =0;
								blist=new ArrayList();
								blist1=new ArrayList();
								blist2=new ArrayList();
								blist =db.SelectOptions("Popular_Code", "Description","WEB_BarrelMaterial_TableV1","SeriesL","Popular_ID");
								blist1 =(ArrayList)blist[0];
								blist2 =(ArrayList)blist[1];
								RBLBarrel.Items.Add("Standard");
								for(int i=0;i<blist2.Count;i++)
								{
									litem =new ListItem(blist2[i].ToString(), blist1[i].ToString());
									RBLBarrel.Items.Add(litem);
								} 
								RBLBarrel.SelectedIndex =0;
								RBLMagnet.Enabled=false;
								RBLTandem.Enabled=false;
								RBLProdSealmat.Enabled=false;
								break;
						}
						if(Session["Coder"] !=null)
						{
							cd1 =(coder)Session["Coder"];
							if(cd1.SSTieRod !=null)
							{
								if(cd1.SSTieRod !="")
								{
									RBLSStierod.SelectedValue =cd1.SSTieRod.Trim();
								}
							}
							if(cd1.SSPistionRod !=null)
							{
								if(cd1.SSPistionRod !="")
								{
									DDLPRodMat.SelectedValue =cd1.SSPistionRod.Trim();
								}
							}
							if(cd1.PistonSeal_Conf !=null)
							{
								if(cd1.PistonSeal_Conf !="")
								{
									RBLPsealmat.SelectedValue =cd1.PistonSeal_Conf.Trim();
								}
							}
							if(cd1.RodSeal_Conf !=null)
							{
								if(cd1.RodSeal_Conf !="")
								{
									RBLProdSealmat.SelectedValue =cd1.RodSeal_Conf.Trim();
								}
							}
							if(cd1.Bushing_Conf !=null)
							{
								if(cd1.Bushing_Conf !="")
								{
									RBLBushing.SelectedValue =cd1.Bushing_Conf.Trim();
								}
							}
							if(cd1.Coating !=null)
							{
								if(cd1.Coating !="")
								{
									DDLCoating.SelectedValue =cd1.Coating.Trim();
								}
							}
							if(cd1.CarbonFibBarrel !=null)
							{
								if(cd1.CarbonFibBarrel !="")
								{
									RBLBarrel.SelectedValue =cd1.CarbonFibBarrel.Trim();
								}
							}
							if(cd1.AirBleed !=null)
							{
								if(cd1.AirBleed !="")
								{
									RBLAirbleeds.SelectedValue =cd1.AirBleed.Trim();
								}
							}
							if(cd1.MagnetRing !=null)
							{
								if(cd1.MagnetRing !="")
								{
									RBLMagnet.SelectedValue =cd1.MagnetRing.Trim();
								}
							}
							if(cd1.MetalScrapper !=null)
							{
								if(cd1.MetalScrapper !="")
								{
									if(cd1.Seal_Comp == "F" || cd1.Seal_Comp == "L" || cd1.Seal_Comp == "E"  ) 
									{
										if(cd1.MetalScrapper=="GR2")
										{
											cd1.MetalScrapper="";
											RBLMetalScrapper.SelectedValue = "None";
										}
										else RBLMetalScrapper.SelectedValue =cd1.MetalScrapper.Trim();
   									}
									else RBLMetalScrapper.SelectedValue =cd1.MetalScrapper.Trim();
								}
							}
							if(cd1.GlandDrain !=null)
							{
								if(cd1.GlandDrain !="")
								{
									RBLGlanddrain.SelectedValue =cd1.GlandDrain.Trim();
								}
							}
							if(cd1.TandemDuplex !=null)
							{
								if(cd1.TandemDuplex !="")
								{
									RBLTandem.SelectedValue =cd1.TandemDuplex.Trim().Substring(0,2);
									TXTstroke.Text=cd1.TandemDuplex.Trim().Substring(2);
								}
							}
						}
					}
				}
			}
			else
			{
				Response.Redirect("Login.aspx");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void LBBack_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("Icylinder_Page4.aspx");
		}

		protected void LBNext_Click(object sender, System.EventArgs e)
		{
			cd1=new coder();
			cd1 =(coder)Session["Coder"];
			Session["Coder"] =null;
			if(RBLAirbleeds.SelectedIndex >0)
			{
				cd1.AirBleed =RBLAirbleeds.SelectedItem.Value.Trim();
			}
			if(RBLBarrel.SelectedIndex >0)
			{
				cd1.CarbonFibBarrel =RBLBarrel.SelectedItem.Value.Trim();
			}
			if(RBLBushing.SelectedIndex >0)
			{
				cd1.Bushing_Conf =RBLBushing.SelectedItem.Value.Trim();
			}
			if(DDLCoating.SelectedIndex >0)
			{
				cd1.Coating =DDLCoating.SelectedItem.Value.Trim();
			}
			if(RBLGlanddrain.SelectedIndex == 0)
			{
				cd1.GlandDrain =RBLGlanddrain.SelectedItem.Value.Trim();
			}
			if(RBLMagnet.SelectedIndex == 0)
			{
				cd1.MagnetRing =RBLMagnet.SelectedItem.Value.Trim();
			}
			if(RBLMetalScrapper.SelectedIndex > 0)
			{
				cd1.MetalScrapper =RBLMetalScrapper.SelectedItem.Value.Trim();
			}
			//issue MetalScrapper start
			if(RBLMetalScrapper.SelectedIndex == 0)
			{
				cd1.MetalScrapper ="";
			}
			//issue MetalScrapper end
			if(DDLPRodMat.SelectedIndex > 0)
			{
				cd1.SSPistionRod =DDLPRodMat.SelectedItem.Value.Trim();
			}
			if(RBLProdSealmat.SelectedIndex > 0)
			{
				cd1.RodSeal_Conf =RBLProdSealmat.SelectedItem.Value.Trim();
			}
			if(RBLPsealmat.SelectedIndex > 0)
			{
				cd1.PistonSeal_Conf =RBLPsealmat.SelectedItem.Value.Trim();
			}
			if(RBLSStierod.SelectedIndex > 0)
			{
				cd1.SSTieRod =RBLSStierod.SelectedItem.Value.Trim();
			}
			if(RBLTandem.SelectedIndex > 0)
			{
				cd1.TandemDuplex =RBLTandem.SelectedItem.Value.Trim()+TXTstroke.Text.Trim();
			}
			Session["Coder"] =cd1;
			Response.Redirect("Icylinder_PageAcc.aspx");
		}

		protected void RBLTandem_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(RBLTandem.SelectedIndex >1)
			{
				lblsecstk.Visible=true;
				TXTstroke.Visible=true;
			}
			else
			{
				lblsecstk.Visible=false;
				TXTstroke.Visible=false;
				TXTstroke.Text="";
			}
		}
		public static bool IsNumeric(string strInteger) 
		{
			try 
			{
				int intTemp =0;
				if(strInteger.ToString().StartsWith(".") == true)
				{
					for(int i=1; i< strInteger.Length;i++)
					{
						intTemp = Int32.Parse( strInteger.Substring(i,1) );
					}
				}
				else
				{
					for(int i=0; i< strInteger.Length;i++)
					{
						if (strInteger.ToString().Substring(i,1) !=".")
						{
							intTemp = Int32.Parse( strInteger.Substring(i,1) );
						}
					}
				}
				return true;
			} 
			catch (FormatException) 
			{
				return false;
			}    
		}
		protected void TXTstroke_TextChanged(object sender, System.EventArgs e)
		{
			if(TXTstroke.Text.ToString().Trim() !="" && IsNumeric(TXTstroke.Text.ToString().Trim()) ==true  )
			{
			
				TXTstroke.Text =String.Format("{0:##0.00}",Convert.ToDecimal(TXTstroke.Text));
				
			}
			else
			{
				TXTstroke.Text="";
			}
		}
	}
}
